/*
 * Copyright à définir.....
 * 
 * class com.maths1p4p.NomDeLaClasse
 * 
 * @author Grégory Lardon
 * @version 1.0
 */
 
import com.maths1p4p.*

class com.maths1p4p.NomDeLaClasse{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var p_bUnBoolean:Boolean;
	public var p_sUnString:String;
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var p_nUnNombre:Number;
	private var p_aUnArray:Array;

	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public static function NomDeLaClasse():void	{
		
		// Quelques exemples de variables locales
		var iScore:Number; 			// i indique que le nombre sera un entier servant d'indice
		var nPlayers:Number; 		// n indique que le nombre sera un entier (nombre de...)
		var fExchangeRate:Number; 	// f indique un nombre réel (float)
		
		...
	}
	
	
	/**
	 * Renvois le tableau
	 * @return un tableau
	 */
	public static function getSomething():Array	{
		return p_aUnArray;
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
		
	/**
	 * Description de maMethodePrivee
	 * @param a un nombre
	 */
	private function myPrivateMethod(a:Number)	{
		...
	}
	
}
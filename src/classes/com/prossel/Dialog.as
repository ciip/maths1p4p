﻿/**
 * Dialogue générique.
 * La fonction statique show() provoque l'affichage du dialogue modal. 
 * 
 * Cette classe est destinée à être héritée pour chaque dialogue spécifique.
 * La classe dérivée doit redéfinir au moins les fonctions show() et init()
 * 
 * @author Pierre Rossel
 * @version 1.0
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 

import mx.containers.Window;
import mx.managers.PopUpManager;
//import mx.controls.Alert;
import mx.utils.Delegate;


class com.prossel.Dialog extends MovieClip {

	
	/**
	 * Affiche un dialogue de gestion des classes
	 * Le retour se fait de manière asynchrone via un objet écouteur
	 * 
	 * Utilisation:
	 * 
		
		var dlg = Dialog.show("myMovie.swf", ClassEditDlg, _root, {title: "Hello world"});
		

	 * @param   contentPath   chemin du movie externe à charger ou symbole de bibliothèque
	 * @param   oClass   Classe à attribuer dynamiquement au contenu chargé. Appelle ensuite init() quand tout est prêt.
	 * @param   parent   clip parent (_root si indéfini)
	 * @param   options  objet pour paramètrer plus spécifiquement le dialogue
	 *                   title: titre de la fenêtre ("")
	 *                   visible: cachée au départ et pendant le chargement (false)
	 *                   modal:  fenêtre modale (true)
	 *                   param:  paramètres passés au constructeur de la class oClass et à init()
	 * @return  la fenêtre créée
	 */
	static private function show (contentPath:Object, oClass:Object, parent:MovieClip, options:Object):Window
	{
		if (options == undefined)
			options = new Object();
		
		if (options.visible == undefined) options.visible = false;
		if (options.modal == undefined) options.modal = true;
		if (options.title == undefined) options.title = "";
		if (parent == undefined){parent = options.parent = _root}else{options.parent = parent};
		
		options.contentPath = contentPath;

		var dlg = PopUpManager.createPopUp(parent, mx.containers.Window, options.modal, options);
		if (dlg == undefined) {
			trace("Failed to create a new Window, probably because there is no Window in the Library");
		}

		var clp = dlg.createEmptyMovieClip("Dialog helper", dlg.getNextHighestDepth());
		clp.onEnterFrame = function () {
			//trace("dialog helper enterframe()");
			if (this.p_bDoInit) {
				this.doInit();
				return;
			}
			
			var content = _parent.content;
			if (content.getBytesTotal() > 0 && content.getBytesLoaded() == content.getBytesTotal()) {
				this.p_bDoInit = true;
			}
		}
		clp.doInit = function () {
			//trace("doInit");
			var content = this._parent.content;
			var mcDlg:MovieClip = content._parent;

			// change la classe du clip qu'on vient de charger
			content.__proto__ = oClass.prototype;
			oClass["apply"](content, options.param);
			
			// adapt to content size and center by default
			mcDlg.setSize(content._width + 5, content._height + 35);
			mcDlg.move(Stage.width / 2 - mcDlg._width / 2, Stage.height / 2 - mcDlg._height / 2);
			mcDlg._visible = true;
			
			content.init(options.param);
			
			this.removeMovieClip();
		}
		
		// regarde déjà si le clip est chargé (possible si pas un clip externe ?)
		clp.onEnterFrame();

		// auto delete popup on close event
		dlg.addEventListener("close", function (evt:Object){
			evt.target.deletePopUp();
		});
		
		return dlg;
	}
	
}
﻿/**
 * Input box dialog
 * 
 * @author  Pierre Rossel
 * @version 1.0
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 

import mx.containers.Window;
import mx.managers.PopUpManager;
//import mx.controls.Alert;
import mx.utils.Delegate;


import maths1p4p.application.Application;
import maths1p4p.application.AbstractStorage;
import maths1p4p.Teacher;
//import maths1p4p.application.LoginForm;


class com.prossel.InputBox extends MovieClip {

	var txtLabel  :TextField;
	var txtInput  :mx.controls.TextInput;
	var btnOK     :mx.controls.Button;
	var btnCancel :mx.controls.Button;
	var mcBgnd    :MovieClip;

	var p_mcDlg   :Window;

	function InputBox() {
		//trace("InputBox contructor");
		//init();
		trace('this._parent._parent: ' + this._parent._parent)
		p_mcDlg = Window(this._parent._parent);
	}
	
	function onLoad(){
		trace("InputBox.onLoad()");
		init();
	}

	function init() {
		trace("InputBox.Init()");
		//Alert.show("InputBox.init()");

		trace('p_mcDlg: ' + p_mcDlg)
		trace("p_mcDlg['clientData']: " + p_mcDlg['clientData'])
		txtLabel.text = p_mcDlg['clientData'].label;
		trace('txtLabel: ' + txtLabel)
		txtInput.password = p_mcDlg['clientData'].password;
		trace("p_mcDlg['clientData'].password: " + p_mcDlg['clientData'].password)
		txtInput.text = p_mcDlg['clientData'].text;
		txtInput.addEventListener("enter", Delegate.create(this, onEnter));

		btnOK.addEventListener("click", Delegate.create(this, onEnter));
		btnCancel.addEventListener("click", Delegate.create(this, function () {
			//trace("Cancel");
			p_mcDlg.dispatchEvent({type:"cancel"});
			p_mcDlg.deletePopUp();
		}));
		
		p_mcDlg.setSize(this._width + 5, this._height + 35);
		p_mcDlg.move(Stage.width / 2 - p_mcDlg._width / 2, Stage.height / 2 - p_mcDlg._height / 2);
		p_mcDlg._visible = true;
		
		txtInput.setFocus();
		
		onEnterFrame = function () {
			Selection.setSelection(0, txtInput.text.length);
			delete onEnterFrame;
		}
	}

	
	/**
	 * Touche ENTER pressée, fermer le dialogue
	 * 
	 */
	function onEnter() {
		p_mcDlg.dispatchEvent({type:"ok", text: txtInput.text});
		p_mcDlg.deletePopUp();
	}
	
	/**
	 * Affiche le dialogue de saisie de texte.
	 * Le retour se fait de manière asynchrone via un objet écouteur
	 * 
	 * Utilisation:
	 * TODO mettre à jour
 		var ol = new Object();
 		ol.close = Delegate.create(this, function (evt:Object){
			trace("ClassManager closed");
		});
		
		maths1p4p.application.InputBox.show(_root, olLogin);

	 * @param   title    Titre du dialogue
	 * @param   parent   clip parent (_root si indéfini)
	 * @param   listener objet écouteur pour événements ok ou cancel
	 * @return  la fenêtre créée
	 */
	static function show (sLabel, sText, sTitle, mcParent, listener, bPassword):Window
	{
		var o = new Object();
		var modal = true;
		if (mcParent == undefined){mcParent = o.parent = _root}else{o.parent = mcParent};
		o.title = sTitle;
		//o.closeButton = true;
		o.contentPath = Application.getRootPath() + "data/inputBox.swf";
		o.visible = false;
		o.clientData = {label: sLabel, text: sText, password: bPassword};

		var m = PopUpManager.createPopUp(mcParent, mx.containers.Window, modal, o);
		//var m = PopUpManager.createPopUp(parent, InputBox, modal, o);
		if (m == undefined) {
			trace("Failed to create a new Window, probably because there is no Window in the Library");
		}
		//trace("popup: " + m);
		m.addEventListener("ok", listener);
		m.addEventListener("cancel", listener);

		//var popup_lo = new Object();
		//popup_lo.close = function (evt:Object){
			//evt.target.deletePopUp();
		//}
//
		//m.addEventListener("complete", popup_lo);
		//m.addEventListener("close", popup_lo);
		return m;
	}

	
	
}
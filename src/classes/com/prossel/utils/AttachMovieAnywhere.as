﻿/**
 * Allows attaching a movie symbol from a dynamically loadedmovie anywhere
 * 
 * Inspired by Madarco code found on http://blog.madarco.net/26/strange-flash-cache-behaviour-and-attachmovieanywhere-solution/
 * 
 * Usage: 
 * com.prossel.utils.AttachMovieAnywhere.init(); // only needed once
 * anyClip.attachMovieAnywhere("somefile.swf", "exporedSymbol", "NewClipName")
 * 
 * Advanced usage:
 * var oListener = new Object();
 * oListener.onLoadInit(mc) {
 *   // mc is the libray holder	 
 *   // mc.NewClipName is the attached clip
 * }
 * anyClip.attachMovieAnywhere("somefile.swf", "exporedSymbol", "NewClipName", 3, {}, oListener);
 * 
 * 
 * @author  Pierre Rossel (inspiration by Madarco)
 * @version 1.0
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

class com.prossel.utils.AttachMovieAnywhere {

	// automaticall init this class if imported
	static var p_oInstance:AttachMovieAnywhere = new AttachMovieAnywhere();

	static function init() {
		if (p_oInstance == undefined) 
			p_oInstance = new AttachMovieAnywhere();
	}

	private function AttachMovieAnywhere() {
	
		//trace("+++AttachMovieAnywhere");
	
		MovieClip.prototype.attachMovieAnywhere = function(file:String, idName:String, newName:String, depth:Number, initObject:Object, oListener:Object) {
			//trace("attachMovieAnywhere: oListener: " + oListener);
			if(depth == undefined) depth = this.getNextHighestDepth();
			var parent:MovieClip = this;
			var container:MovieClip = this.createEmptyMovieClip(newName, depth);
			var mcLoader:MovieClipLoader = new MovieClipLoader();
			var listener:Object = new Object();
			listener.onLoadInit = function (mc) {
				var mcClip = mc.attachMovie(idName, newName, mc.getNextHighestDepth());
				parent[newName] = parent[newName][newName];
			}
			
			mcLoader.addListener(listener);
			mcLoader.addListener(oListener);
			
			mcLoader.loadClip(file, container);
			return container;
		} 
	}
}
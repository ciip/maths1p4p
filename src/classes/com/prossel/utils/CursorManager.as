﻿/**
 * Provides an easy way to use custom cursors from internal library of external one
 * 
 * 		CursorManager.register(clip, cursorId);  // from internal library
 * 		CursorManager.register(clip, cursorId, "cursorlib.swf");  // from external library
 * 
 * Overriding onRollOver,onRollOut and onUnload
 * --------------------------------------------
 * If you need to override these handlers for your own need, be sure to do it 
 * after calling register() and to call their _default implementation as 
 * in this example:
 * 
 * clip.onRollOver = function () {
 *	 // do whatever you have to do here then call default implementation
 *   this.onRollOver_default();
 * }
 * 
 * clip.onRollOut = function () {
 *	 // do whatever you have to do here then call default implementation
 *   this.onRollOut_default();
 * }
 *      
 * clip.onUnload = function () {
 *	 // do whatever you have to do here then call default implementation
 *   this.onUnload_default();
 * }
 *      
 * 
 * Note: A v2 component must be at least in the library for this class to work.
 * 
 * @author  Pierre Rossel
 * @version 1.0
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import mx.managers.DepthManager;
import mx.core.UIObject;

import com.prossel.utils.AttachMovieAnywhere;

class com.prossel.utils.CursorManager {
	
	private static var p_mcCursor:MovieClip;
	private static var p_arrLibraries:Array = new Array();
	private static var p_arrListeners:Array = [];

	static function addListener(oListener:Object){
		//trace("CursorManager.addListener("+oListener+")");
		//p_arrListeners = new Array();
		//trace('p_arrListeners: ' + p_arrListeners)
		p_arrListeners.push(oListener);
		//trace('p_arrListeners: ' + p_arrListeners)
		//trace('new Array: ' + new Array())
		//trace('[]: ' + [])
	}

	static function register(mc:Object, sCursorName:String, sLibrary:String) {
		//trace("CursorManager.register on " + mc);
		
		com.prossel.utils.AttachMovieAnywhere.init();
		
		// ces fonctions doivent être appelées en réponse aux événements correspondants
		mc.onRollOver_default = function () {
			CursorManager.handleRollOver(this, sCursorName, sLibrary);
		}

		mc.onRollOut_default = function () {
			CursorManager.handleRollOut();
		}
		
		mc.onUnload_default = function () {
			CursorManager.handleUnload();
		}

		// Si ces fonctions sont redéfinies par ailleurs, veillez à appeler
		// les fonctions *_default correspondantes.
		mc.onRollOver = function () {
			this.onRollOver_default();
		}
		
		mc.onRollOut = function () {
			this.onRollOut_default();
		}
		
		mc.onUnload = function () {
			this.onUnload_default();
		}
	}
	
	static function handleRollOver(mc:MovieClip, sCursorName:String, sLibrary:String) {
		//trace("CursorManager: handleRollOver " + mc + " avec curseur: " + sCursorName);
	
		showMouse(false);
		p_mcCursor.removeMovieClip();
		
		if (sLibrary != undefined) {
			
			// Look if library is already loaded
			var mcLib = p_arrLibraries[sLibrary];
			
			// Library not loaded yet or no longer available
			if (mcLib == undefined || String(mcLib) == "") {

				mcLib = DepthManager.createClassObjectAtDepth(
					UIObject, 
					DepthManager.kCursor
				);
				if (mcLib == undefined) {
					trace("CursorManager.handleRollOver() could not successfully DepthManager.createClassObjectAtDepth()");
				}
				p_arrLibraries[sLibrary] = mcLib;

				var oListener = new Object();
				oListener.onLoadInit = function (mcLib) {
					//trace("CursorManager: my listener load init " + mcLib + "mcLib.cursor: " + mcLib.cursor);
					p_arrLibraries[sLibrary] = mcLib;
					p_mcCursor = mcLib.cursor;
					if (p_mcCursor == undefined){
						trace("CursorManager: cursor \"" + sCursorName + "\" not found in external library \"" + sLibrary + "\"");
					}
					initCursor(p_mcCursor, mc);
				}
				oListener.onLoadError = function(target_mc:MovieClip, errorCode:String, httpStatus:Number) {
					trace("CursorManager: Library load error: " + errorCode + " httpStatus: " + httpStatus);
					delete p_arrLibraries[sLibrary];
					showMouse(true);
				}
				//trace("CursorManager: attaching movie anywhere: " + sCursorName);
				mcLib.attachMovieAnywhere(sLibrary, sCursorName, "cursor", undefined, undefined, oListener);
			}
			else {
				//trace("CursorManager: attaching movie: " + sCursorName);
				p_mcCursor = mcLib.attachMovie(sCursorName, "cursor", mcLib.getNextHighestDepth());
				initCursor(p_mcCursor, mc);
			}
		}
		else {
			p_mcCursor = DepthManager.createObjectAtDepth(
				sCursorName, 
				DepthManager.kCursor, 
				{_x:_root._xmouse, _y:_root._ymouse}
			);
			if (p_mcCursor != undefined) {
				initCursor(p_mcCursor, mc);
			}
			else {
				trace("CursorManager: cursor \"" + sCursorName + "\" not found in internal library");
				showMouse(true);

			}
		}
	}

	static function handleRollOut() {
		//trace("CursorManager: handleRollOut ");
		if (p_mcCursor != undefined) {
			showMouse(true);
			p_mcCursor.removeMovieClip();
			p_mcCursor = undefined;
		}
	}
	
	static function handleUnload() {
		//trace("CursorManager: handleUnload ");
		handleRollOut();
	}
	
	static function show(){
		p_mcCursor._visible = true;
	}
	
	static function hide(){
		p_mcCursor._visible = false;
	}
	
	private static function initCursor(mcCursor:MovieClip, mcHover:MovieClip){
		//trace('CursorManager: initCursor(mcCursor: ' + mcCursor + ', mcHover: ' + mcHover + ")")

		if (mcCursor == undefined) {
			showMouse(true);
			return;
		}
		
		// must watch the clip rolled over for vanishing (if parent is unloaded, we don't get an unload event
		mcHover["watchedByCursorManager"] = true;
		p_mcCursor.createEmptyMovieClip("watcher", 1);
		p_mcCursor.watcher.watchee = mcHover;
		p_mcCursor.watcher.onEnterFrame = function() {
			if (this.watchee.watchedByCursorManager == undefined) {
				// watchee has dissapeared, remove cursor as well
				CursorManager.handleUnload();
			}
		}
		
		p_mcCursor.update = function () {
			this._x = _xmouse;
			this._y = _ymouse;
			updateAfterEvent();
		}
		p_mcCursor.update();
		
		p_mcCursor.onMouseMove = function () {
			this.update();
		}
	}

	/** 
	 * show/hide the real system cursor
	 * 
	 * @param	bShow
	 */
	public static function showMouse(bShow:Boolean) {
		if (bShow == undefined)
			bShow = true;
			
		if (bShow)
			Mouse.show();
		else
			Mouse.hide();
			
		for(var iList in p_arrListeners) {
			//trace("calling onShowMouse on " + p_arrListeners[iList]);
			p_arrListeners[iList].onShowMouse(bShow);
		}
	}
}

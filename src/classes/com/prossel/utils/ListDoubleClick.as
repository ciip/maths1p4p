﻿/**
 * Add doubleclick support to V2 List component
 * 
 * Inspired by code found on http://www.darronschall.com/weblog/archives/000135.cfm
 * 
 * Sample usage:
 * // drag v2 list component onto stage and name it "list"
 * // place above code on frame 1 followed by below code:
 * com.prossel.utils.ListDoubleClick.init();
 * list.dataProvider = [{label:"test"},{label:"test2"},{label:"test3"}];
 * list.addEventListener("doubleClick", mx.utils.Delegate.create(this, onItemSelected));
 * function onItemSelected(eventObj) {
 * // do something with the selected item
 *  	trace("double click");
 * }
 * 
 * @author  Pierre Rossel
 * @version 1.0
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
class com.prossel.utils.ListDoubleClick {
	
	private static var p_bInit:Boolean = false;
	
	static function init() {
		
		if (p_bInit)
			return;
			
		p_bInit = true;
		
		/**
		 * 
		 * Note: inspired by code published by senocular on 
		 * http://www.darronschall.com/weblog/archives/000135.cfm
		 * 
		 */
		// Double-Click Hack for List-based components
		// Nov. 15, 2004 - Darron Schall
		
		// create quick reference to ScrollSelectList's prototype
		var sslp = _global.mx.controls.listclasses.ScrollSelectList.prototype;
		
		// only run this code once.., make sure that $oldOnRowPress is not
		// defined to avoid inifinite recursion when onRowPress is called
		if (!sslp.$oldOnRowPress) {
			sslp.DOUBLE_CLICK_INTERVAL = 300; 	// in milliseconds, how close two clicks must 
													// be received to register as a double click
		 
			// save a reference to the onRowPress function since we need to overwrite it to add 
			// functionality, and we don't want to lose the original.
			sslp.$oldOnRowPress = sslp.onRowPress;
			
			// add doubleClick event to the ScrollSelectList class
			sslp.onRowPress = function(rowIndex) {
				// if the user pressed the samw row within the time limit, fire off
				if (getTimer() - this.lastPressedTime < this.DOUBLE_CLICK_INTERVAL && this.lastPressedRow == rowIndex) {
		   
					this.dispatchEvent({target:this,type:"doubleClick",row:rowIndex});
				} else {
					// not a double click - record their click time and the row selected to prepare
					// for double click
					
					// only allow double clicks under the proper conditions
					if (this.enabled && this.selectable) {
							this.lastPressedTime = getTimer();
							this.lastPressedRow = rowIndex;
						} else {
							// not really necessary, but just in case.. make sure
							// doubleClick doesn't accidentally fire
							this.lastPressedTime = 0;
							this.lastPressedRow = -1;
						}
					
					// invoke the old method that we just overwrote - using apply to get the scope correct
					this.$oldOnRowPress.apply(this, [rowIndex]);
				}
			};
		}
		delete sslp;
	}
}
﻿/**
 * class com.maths1p4p.AbstractGame
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * Copyright (c) 2008 Grégory Lardon.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import XMLShortcuts;
 
import maths1p4p.utils.SoundPlayer;
import maths1p4p.utils.FilesPreloader;

class maths1p4p.AbstractGame extends MovieClip{
	 
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	

	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	/** sound player */
	static private var p_oSoundPlayer:SoundPlayer = new SoundPlayer();
	
	/** le prechargeur de fichiers */
	static private var p_oFilesPreloader:FilesPreloader = new FilesPreloader();
	
	/** le repertoire pour les sons */
	static private var p_sSoundPath:String = "";
	
	/** le dossier racine du jeu */
	private var p_sFolder:String;
	
	/** le titre du jeu */
	private var p_sTitle:String;
	
	/** le nombre de joueurs requis */
	private var p_iNbPlayers:Number;
	
	/** le texte descriptif du jeu */
	private var p_sDescription:String;
	
	/** l'id de l'objet à gagner */
	private var p_sIdLoot:String;
	
	/** le xml de configuration du jeu */
	private var p_xmlGame:XML;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function AbstractGame(){
		
		//Activate the XML Shortcuts si pas déjà fait
		if(!_global.XMLShortcutsActivated){
			XMLShortcuts.activate();
			_global.XMLShortcutsActivated = true;
		}

		// définition du dossier contenant les fichiers du jeu en cours		
		if(_parent.sGameFolder != null){
			p_sFolder = _parent.sGameFolder;
		}else{
			p_sFolder = "";
		}

		// chargement du fichier xml de configuration
		p_xmlGame = new XML();
		p_xmlGame["parent"] = this;
		p_xmlGame.onLoad = function(){
			this["parent"].readXmlGame();
		}
		p_xmlGame.load(p_sFolder + "data/game.xml");	

	}
	
	/**
	 * démmarage du jeu, à implimenter
	 */
	public function startGame(){		
	}	

	/**
	 * le jeu est terminé, à implémenter
	 */
	public function gameFinished(){		
	}
	
	//pour obtenir la valeur de p_oSoundPlayer
	public function get soundPlayer():SoundPlayer{
		return p_oSoundPlayer;
	}
	
	public static function get soundPlayerFromLevel():SoundPlayer{
		return p_oSoundPlayer;
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
		
		
	/**
	 * Lecture du fichier xml de configuration
	 * Création de la liste des fichiers à précharger
	 */
	private function readXmlGame(){
		
		for (var i in p_xmlGame["game"].media.$son){  
			p_oSoundPlayer.addASound(p_xmlGame["game"].media.$son[i]._id,p_sSoundPath+p_xmlGame["game"].media.$son[i].__text);
        }
		// on démarre le jeu
		startGame();		
		
	}
	
	

	
}
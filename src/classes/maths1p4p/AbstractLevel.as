﻿/**
 * class com.maths1p4p.AbstractLevel
 * 
 * @author Pierre Rossel, Grégory Lardon, Fred Fauquette
 * @version 1.0
 * 
 * Copyright (c) 2008 Pierre Rossel, Grégory Lardon, Fref Fauquette
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import mx.controls.Alert;
import mx.utils.Delegate;

import maths1p4p.AbstractGame;
import maths1p4p.AbstractPlayer;

class maths1p4p.AbstractLevel extends MovieClip {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var p_mcGame:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	/** le dossier racine du jeu */
	private var p_sLevelFolder:String;
	
	/** nom du jeu en cours */
	private var p_sCurGame:String;

	/** movieClipLoader du jeu */
	private var p_mclGameLoader:MovieClipLoader;
	
	/** le joueur courant */
	private var p_oCurrPlayer:AbstractPlayer;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function AbstractLevel(){
		
		// définition du dossier contenant les fichiers du jeu en cours		
		if(_global.g_oApplication != null){
			p_sLevelFolder = _global.g_oApplication.getLevelFolder();
		}else{
			p_sLevelFolder = "";
		}
		
	}
	
	/**
	 * p_sLevelFolder
	 */
	public function getLevelFolder():String{
		return p_sLevelFolder;
	}
	
	
	/**
	 * Décharge le niveau
	 */
	public function unload():Void{
		//trace("unload parent: " + _parent + "(this: "+ this + ")");
		
		var ol = new Object();
		ol.click = Delegate.create(this, function (evt:Object) {
			if (evt.detail == Alert.YES) {
				maths1p4p.application.Main.getInstance().unloadLevel();
			}
		});

		var dlg = Alert.show(
		   "Veux-tu vraiment quitter la partie ?", 
			"Quitter",
			Alert.YES | Alert.NO,
			_root, 
			ol,
			"", 
			Alert.NO);
	}
	
	/**
	 * getPlayerList
	 * @param calbackObj object callBack
	 * @param callbackMsg message du callback
	 * 
	 * renvois de maniere asynchrone la liste des joueurs inscrits pour ce niveau
	 */
	public function getPlayerList(calbackObj:Object, callbackMsg:String):Void{
		
	}
	
	/**
	 * showPlayerSelectionScreen
	 * 
	 * affiche l'écran de sélection du joueur, en début de partie
	 */
	public function showPlayerSelectionScreen():Void{
		
	}
	
	/**
	 * launchLevel
	 * 
	 * débute la partie du joueur dont l'id est passé en paramètre
	 */
	public function launchLevel(iPlayerId:Number):Void{
		
	}
	
	
	/**
	 * loadGame
	 * 
	 * charge un jeu
	 */
	public function loadGame(sGame:String) {
		
		this.unloadGame();

		p_sCurGame = sGame;
		//p_mcGame = this.createEmptyMovieClip("p_mc" + sGame, this.getNextHighestDepth());
		// modifié car le jeu se charge toujours au dessus de l'écran de sélection des joueurs, sinon....
		p_mcGame = this.createEmptyMovieClip("p_mc" + sGame, 20);
		
		
		var listenerObject:Object = new Object();
		listenerObject.ref = this;
		listenerObject.onLoadInit = function(target_mc:MovieClip) {
			target_mc.sGameFolder = target_mc._parent.p_sLevelFolder + target_mc._parent.p_sCurGame + "/";
			target_mc.play();
		}
		listenerObject.onLoadComplete = function(target_mc:MovieClip) {
			var re = this;
			re.ref.callBackLoadGame();
		}
		
		var sFile:String = p_sLevelFolder + p_sCurGame + "/" + p_sCurGame + ".swf";
		maths1p4p.utils.ClipLoader.load(sFile, p_mcGame, undefined, listenerObject);
	}
	

	
	/**
	 * unLoadGame
	 * 
	 * decharge un jeu
	 */
	private function unloadGame() {

		if (p_mcGame != undefined) {
			p_mcGame.removeMovieClip();
			p_sCurGame = undefined;
		}

	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------


	
}
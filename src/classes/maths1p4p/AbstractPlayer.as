﻿/**
 * class com.maths1p4p.AbstractPlayer
 * 
 * @author Grégory Lardon, Pierre Rossel
 * @version 1.0
 * 
 * Copyright (c) 2008 Grégory Lardon, Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.*;

class maths1p4p.AbstractPlayer extends mx.events.EventDispatcher {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	

	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	/** le nom du joueur */
	private var p_sName:String;
	/** l'id du joueur */
	private var p_iId:Number;
	/** l'état du niveau du joueur */
	private var p_xmlState:XML;
	/** les résultats du joueur */
	private var p_xmlResults:XMLNode;
	/** le mot de passe est défini */
	private var p_bPasswordSet:Boolean;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * Ne pas appeler directement, mais faire appel aux fonctions
	 * - Application.getStorage() pour obtenir la classe Storage courante
	 * - AbstractStorage.loadPlayer() pour charger un player connaissant son id.
	 */
	public function AbstractPlayer(sName:String, iId:Number){
		//trace("++AbstractPlayer(" + sName + ", " + iId + ")");
		p_sName = sName;
		p_iId = iId;
	}
	
	/**
	 * Vérifie le mot de passe du joueur
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onLogin", this) pour recevoir le retour.
	 * event.data.result  -1: erreur 0: OK
	 * event.data.error   Détails de l'erreur.
	 * 
	 * @param   sPassword   Mot de passe en clair du joueur
	 * @return  
	 */
	public function login(sPassword:String) {
		var oStorage:AbstractStorage = Application.getInstance().getStorage();
		oStorage.loginPlayer(p_iId, sPassword);
		oStorage.addEventListener("onLoginPlayer", this);
	}
	
	/**
	 * Change le mot de passe du joueur
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onChangePassword", this) pour recevoir le retour.
	 * event.data.result  -1: erreur 0: OK
	 * event.data.error   Détails de l'erreur.
	 * 
	 * @param   sCurPassword       Mot de passe en clair du joueur
	 * @param   sNewPassword       Nouveau mot de passe
	 * @return  
	 */
	public function changePassword(sCurPassword:String, sNewPassword:String) {
		var oStorage:AbstractStorage = Application.getInstance().getStorage();
		oStorage.changePlayerPassword(p_iId, sCurPassword, sNewPassword);
		oStorage.addEventListener("onChangePlayerPassword", this);
	}
	
	/**
	 * sauvegarde du joueur en cours
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onSave", this) pour recevoir le retour.
	 */
	public function save():Void{
		var oStorage:AbstractStorage = Application.getInstance().getStorage();
		oStorage.savePlayer(this);
		oStorage.addEventListener("onSavePlayer", this);
	}

	/**
	 * renvois le nom du joueur
	 */
	public function getName():String{
		return p_sName;
	}
	
	/**
	 * renvois l'id du joueur
	 */
	public function getId():Number{
		return p_iId;
	}
	
	
	/**
	 * Définit le noeud XML qui contient l'état du joueur
	 * 
	 * @param   xmlNode 
	 */
	function setState(xmlState:XMLNode){
		p_xmlState.ignoreWhite = true;
		p_xmlState = new XML();
		p_xmlState.parseXML(xmlState.toString());
	}
	
	function getState():XMLNode {
		return p_xmlState;
	}
	
	/**
	 * Définit le noeud XML qui contient les résultats du joueur
	 * 
	 * @param   xmlResults  Noeud XML à sauver dans les infos du joueur
	 */
	function setResults(xmlResults:XMLNode){
		p_xmlResults = xmlResults;
	}
	
	/**
	 * @return  le noeux XML qui contient les résultats
	 */
	function getResults():XMLNode {
		return p_xmlResults;
	}

	/**
	 * Définit si le joueur a son mot de passe définit
	 */
	function setPasswordSet(bPasswordSet:Boolean) {
		p_bPasswordSet = bPasswordSet;
	}
	
	
	/**
	 * @return  un Boolean qui indique si l'utilisateur a défini un mot de passe
	 */
	function getPasswordSet():Boolean {
		return p_bPasswordSet;
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
		
	/**
	 * Retour de la fonction asynchrone AbstractStorage.loginPlayer()
	 * @param   event 
	 */
	private function onLoginPlayer(event:Object) {
		// fait suivre
		this.dispatchEvent({type:"onLogin", data:{result: event.data.result, error: event.data.error}});
	}
	
	/**
	 * Retour de la fonction asynchrone AbstractStorage.changePlayerPassword()
	 * @param   event 
	 */
	private function onChangePlayerPassword(event:Object) {
		// fait suivre
		this.dispatchEvent({type:"onChangePassword", data:{result: event.data.result, error: event.data.error}});
	}
	
	/**
	 * Retour de la fonction asynchrone AbstractStorage.savePlayer()
	 * @param   event 
	 */
	private function onSavePlayer(event:Object) {
		// fait suivre
		this.dispatchEvent({type:"onSave", data:{result: event.data.result, error: event.data.error}});
	}
	
}
﻿/**
 * class com.maths1p4p.Classe
 * 
 * Note: Le nom de la classe est en français pour éviter la confusion avec le mot réservé class.
 * 
 * @author Pierre Rossel
 * @version 1.0
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import mx.utils.Delegate;

import maths1p4p.application.*;

class maths1p4p.Classe extends mx.events.EventDispatcher {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	var p_iId:Number;
	var p_iSchool:Number;
	var p_iTeacher:Number;
	var p_sName:String;
	var p_sPass:String;
	var p_iLevel:Number;
	
	var p_sCreateSchool: String; // utilisé quand on sauve pour ajouter automatiquement une nouvelle école
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Classe(iId:Number){
		// trace("++ Classe(" + iId + ")");
		p_iId = iId;
	}
	

	public static function loadCurrentClass(oListener:Object) {
		var oStorage:AbstractStorage = Application.getInstance().getStorage();
		var idClass = Application.getInstance().getCurrentClass();
		oStorage.addEventListener("onLoadClass", oListener);
		oStorage.loadClass(idClass);
	}
	
	
	
	/**
	 * Sauve les données de l'objet
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onSave", this) pour recevoir le retour.
	 */
	public function save():Void{
		var oStorage:AbstractStorage = Application.getInstance().getStorage();
		oStorage.saveClass(this);
		oStorage.addEventListener("onSaveClass", this);
	}

	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
		
	private function onSaveClass(event:Object) {
		// fait suivre
		this.dispatchEvent({type:"onSave", data:{result: event.data.result, error: event.data.error}});
	}

	
}
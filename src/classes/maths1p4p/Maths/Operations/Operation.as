﻿/**
 * class com.maths1p4p.Maths.Operations.Operation
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

class maths1p4p.Maths.Operations.Operation {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	/** le premier membre */
	public var p_iA:Number;
	/** le second membre */
	public var p_iB:Number;
	/** l'opérateur */
	public var p_sOperator:String;
	/** le résultat */
	public var p_iResult:Number;
	
		
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	

	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Operation(iA:Number, iB:Number, sOp:String){
		
		if(iA != null){
			p_iA = iA;
		}
		if(iB != null){
			p_iB = iB;
		}
		if(sOp != null){
			p_sOperator = sOp;
			compute();
		}
		
	}
	
	
	/**
	 * calcul l'opération
	 */	
	public function compute():Number{	
		
		switch (p_sOperator){
			
			case "+":
				p_iResult = p_iA + p_iB;
				break;
			case "-":
				p_iResult = p_iA - p_iB;
				break;
			case "x":
				p_iResult = p_iA * p_iB;
				break;
			case "/":
				p_iResult = p_iA / p_iB;
				break;
			
		}
		
		return p_iResult;
		
	}

	
}

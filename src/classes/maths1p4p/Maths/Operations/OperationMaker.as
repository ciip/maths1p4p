﻿/**
 * class com.maths1p4p.Maths.Operations.OperationMaker
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

class maths1p4p.Maths.Operations.OperationMaker {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	
		
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	

	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function OperationMaker(){		
	}
	
	
	/**
	 * Renvois une addition
	 * 
	 * @usage   getAddition(iItemMinValue:Number, iItemMaxValue:Number)
	 * @param   iItemMinValue la valeur minimale pour un item de l'opération
	 * @param   iItemMaxValue la valeur maximale pour un item de l'opération
	 * @return  un objet operation
	 */	
	public static function getAddition(iItemMinValue:Number, iItemMaxValue:Number){		
		
		// 25+57 c'est OK (car 5+7 > 10)
		
		var iR:Number = 0;

		var iA2:Number = 0;
		while(iA2<=1){
			var iA:Number = Math.floor(Math.random() * (iItemMaxValue - iItemMinValue + 1)) + iItemMinValue;
			var iA2:Number = Number ( String(iA).charAt( String(iA).length-1) );
		}
		var ok:Boolean = false;

		while(!ok){
			var iB:Number = Math.floor(Math.random() * (iItemMaxValue - iItemMinValue + 1)) + iItemMinValue;
			var iB2:Number = Number ( String(iB).charAt( String(iB).length-1) );
			if(iB2 + iA2 > 10){
				ok = true;
			}				
		}			
		iR = iA + iB;

		
		var oAddition = new Object();
		oAddition.A = iA;
		oAddition.B = iB;
		oAddition.R = iR;	
		oAddition.operator = "+";	
		
		return oAddition;
		
	}
	
	/**
	 * Renvois une addition
	 * 
	 * @usage   getAddition(iItemMinValue:Number, iItemMaxValue:Number)
	 * @param   iItemMinValue la valeur minimale pour un item de l'opération
	 * @param   iItemMaxValue la valeur maximale pour un item de l'opération
	 * @return  un objet operation
	 */	
	public static function getAddition2(iItemMinValue:Number, iItemMaxValue:Number, iMinResult:Number, iMaxResult:Number){		
		
		var okk:Boolean = false;
		while(!okk){
			var iR:Number = 0;
			var iA2:Number = 0;
			while(iA2<=1){
				var iA:Number = Math.floor(Math.random() * (iItemMaxValue - iItemMinValue + 1)) + iItemMinValue;
				var iA2:Number = Number ( String(iA).charAt( String(iA).length-1) );
			}
			var ok:Boolean = false;

			while(!ok){
				var iB:Number = Math.floor(Math.random() * (iItemMaxValue - iItemMinValue + 1)) + iItemMinValue;
				var iB2:Number = Number ( String(iB).charAt( String(iB).length-1) );
				if(iB2 + iA2 > 10){
					ok = true;
				}				
			}			
			iR = iA + iB;
			if(iR > iMinResult && iR < iMaxResult){
				okk = true;
			}
		}

		
		var oAddition = new Object();
		oAddition.A = iA;
		oAddition.B = iB;
		oAddition.R = iR;	
		oAddition.operator = "+";	
		
		return oAddition;
		
	}	
	
	
	/**
	 * Renvois une soustraction
	 * 
	 * @usage   getSoustraction(iItemMinValue:Number, iItemMaxValue:Number)
	 * @param   iItemMinValue la valeur minimale pour un item de l'opération
	 * @param   iItemMaxValue la valeur maximale pour un item de l'opération
	 * @return  un objet operation
	 */	
	public static function getSoustraction(iItemMinValue:Number, iItemMaxValue:Number){
		
		// 42-38 c'est OK (car 2 < 8)

		var PremCond:Boolean = false;
		while(!PremCond){
			var iA:Number = Math.floor(Math.random() * (iItemMaxValue - iItemMinValue + 1)) + iItemMinValue;			
			var iA2:Number = Math.floor(Math.random()*7)+1;
			var iAstr:String = String(iA).substring(0, String(iA).length-1);

			if(iAstr.length>0){
				iAstr += String(iA2) ;		
				PremCond = true;
			}else{
				iAstr = String(iA2);
			}
		}
		
		
		iA = Number(iAstr);

		var iB2:Number = 0;
		var iB:Number = iA;
		while(iB2<=iA2 || iB >= iA){
			var iB:Number = Math.floor(Math.random() * (iA)) ;
			var iB2:Number = iA2 + Math.floor(Math.random()*(9-iA2)) +1;
			var iBStr:String = String(iB).substring(0, String(iB).length-1);
			iBStr += String(iB2) ;	
			iB = Number(iBStr);			
		}
		
		var iR = iA - iB;
		
		var oSoustraction = new Object();
		oSoustraction.A = iA;
		oSoustraction.B = iB;
		oSoustraction.R = iR;	
		oSoustraction.operator = "-";	

		return oSoustraction;
		
	}
	
	/**
	 * Renvois une soustraction
	 * 
	 * @usage   getSoustraction(iItemMinValue:Number, iItemMaxValue:Number)
	 * @param   iItemMinValue la valeur minimale pour un item de l'opération
	 * @param   iItemMaxValue la valeur maximale pour un item de l'opération
	 * @return  un objet operation
	 */	
	public static function getSoustraction2(iItemMinValue:Number, iItemMaxValue:Number, iMinResult:Number, iMaxResult:Number){
		
		// 42-38 c'est OK (car 2 < 8)
		
		var okk:Boolean = false;
		while(!okk){

			var PremCond:Boolean = false;
			while(!PremCond){
				var iA:Number = Math.floor(Math.random() * (iItemMaxValue - iItemMinValue + 1)) + iItemMinValue;			
				var iA2:Number = Math.floor(Math.random()*7)+1;
				var iAstr:String = String(iA).substring(0, String(iA).length-1);

				if(iAstr.length>0){
					iAstr += String(iA2) ;		
					PremCond = true;
				}else{
					iAstr = String(iA2);
				}
			}	
			iA = Number(iAstr);

			var iB2:Number = 0;
			var iB:Number = iA;
			while(iB2<=iA2 || iB >= iA){
				var iB:Number = Math.floor(Math.random() * (iA)) ;
				var iB2:Number = iA2 + Math.floor(Math.random()*(9-iA2)) +1;
				var iBStr:String = String(iB).substring(0, String(iB).length-1);
				iBStr += String(iB2) ;	
				iB = Number(iBStr);			
			}
			
			var iR = iA - iB;
			
			if(iR > iMinResult && iR < iMaxResult){
				okk = true;
			}
		}
		
		var oSoustraction = new Object();
		oSoustraction.A = iA;
		oSoustraction.B = iB;
		oSoustraction.R = iR;	
		oSoustraction.operator = "-";	

		return oSoustraction;
		
	}	
	
	
	/**
	 * Renvois une multiplication
	 * 
	 * @usage   getSoustraction(iItemMinValue:Number, iItemMaxValue:Number)
	 * @param   iItemMinValue la valeur minimale pour un item de l'opération
	 * @param   iItemMaxValue la valeur maximale pour un item de l'opération
	 * @return  un objet operation
	 */	
	public static function getMultiplication(iItem1MinValue:Number, iItem1MaxValue:Number, iItem2MinValue:Number, iItem2MaxValue:Number){		
		
		var iA:Number = Math.floor(Math.random() * (iItem1MaxValue - iItem1MinValue + 1)) + iItem1MinValue;
		var iB:Number = Math.floor(Math.random() * (iItem2MaxValue - iItem2MinValue + 1)) + iItem2MinValue;
		var iR:Number = iA * iB;
		
		var oSoustraction = new Object();
		oSoustraction.A = iA;
		oSoustraction.B = iB;
		oSoustraction.R = iR;	
		oSoustraction.operator = "X";	
		
		return oSoustraction;
		
	}
	
	/**
	 * Renvois une multiplication
	 * 
	 * @usage   getSoustraction(iItemMinValue:Number, iItemMaxValue:Number)
	 * @param   iItemMinValue la valeur minimale pour un item de l'opération
	 * @param   iItemMaxValue la valeur maximale pour un item de l'opération
	 * @return  un objet operation
	 */	
	public static function getMultiplication2(iItem1MinValue:Number, iItem1MaxValue:Number, iItem2MinValue:Number, iItem2MaxValue:Number, iMinResult:Number, iMaxResult:Number){		
		
		var okk:Boolean = false;
		while(!okk){		
		
			var iA:Number = Math.floor(Math.random() * (iItem1MaxValue - iItem1MinValue + 1)) + iItem1MinValue;
			var iB:Number = Math.floor(Math.random() * (iItem2MaxValue - iItem2MinValue + 1)) + iItem2MinValue;
			var iR:Number = iA * iB;
			
			if(iR > iMinResult && iR < iMaxResult){
				okk = true;
			}			
			
		}
		
		var oSoustraction = new Object();
		oSoustraction.A = iA;
		oSoustraction.B = iB;
		oSoustraction.R = iR;	
		oSoustraction.operator = "X";	
		
		return oSoustraction;
		
	}	
		
	
}

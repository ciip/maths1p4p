﻿/**
 * class com.maths1p4p.Teacher
 * 
 * @author Pierre Rossel
 * @version 1.0
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import mx.utils.Delegate;

import maths1p4p.application.*;

class maths1p4p.Teacher extends mx.events.EventDispatcher {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	var p_iId:Number;
	var p_sLogin:String;
	var p_sFirstName:String;
	var p_sLastName:String;
	var p_sEmail:String;

	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Teacher(iId:Number){
		// trace("++ Teacher(" + iId + ")");
		p_iId = iId;
	}
	
	
	
	/**
	 * Affiche le dialogue de login pour l'enseignant et valide son accès.
	 * 
	 * 
	 * Fonction asynchrone. 
	 */
	public static function login(mcParent, oListener) {

		var olLogin = new Object();
		olLogin.ok = function (evt:Object){
			//trace("Login: " + evt.name + " / " + evt.password);
			// valide le nom et le mot de passe donnés
			var oStorage:AbstractStorage = Application.getInstance().getStorage();
			var olStorage:Object = new Object();
			oStorage.addEventListener("onLoginTeacher", this);
			oStorage.loginTeacher(evt.name, evt.password);
		}
		olLogin.cancel = function (evt:Object){
			//trace("Login cancelled");
			oListener.onLogin();
		}
		olLogin.onLoginTeacher = function (evt:Object){
			//trace("Teacher login");
			var oStorage:AbstractStorage = Application.getInstance().getStorage();
			oStorage.removeEventListener("onLoginTeacher", this);
			if (evt.data.result > 0) {
				// Teacher login ok
				var oTeacher = new Teacher(evt.data.result);
				oListener.onLogin(oTeacher);
			}
			else {
				// essaye encore jusqu'à ce que l'utilisateur annule
				LoginForm.show("Identification de l'enseignant", mcParent, olLogin);
			}
		}
		
		LoginForm.show("Identification de l'enseignant", mcParent, olLogin);
		
	}
	
	
	/**
	 * Sauve les données de l'objet
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onSave", this) pour recevoir le retour.
	 */
	public function save():Void{
		var oStorage:AbstractStorage = Application.getInstance().getStorage();
		oStorage.addEventListener("onSaveTeacher", this);
		oStorage.saveTeacher(this);
	}

	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
		
	private function onSaveTeacher(event:Object) {
		// fait suivre
		this.dispatchEvent({type:"onSave", data:{result: event.data.result, error: event.data.error}});
	}

	
}
﻿/**
 * Classe de base de stockage des données de l'application. 
 * Les classes dérivées spécifiques implémentent le stockage réel des données.
 * 
 * @author Pierre Rossel
 * @version 1.0
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

//import maths1p4p.application.IStorage;
import maths1p4p.AbstractPlayer;
import maths1p4p.Teacher;
import maths1p4p.Classe;
 
//class maths1p4p.application.AbstractStorage extends mx.events.EventDispatcher implements IStorage {
class maths1p4p.application.AbstractStorage extends mx.events.EventDispatcher {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	private var p_sTeacherLogin:String;
	private var p_sTeacherHash:String;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur. Ne pas créer directement un objet de cette classe, utiliser 
	 * les classes dérivées.
	 */
	public function AbstractStorage()	{		
		super();
		//trace("+++ AbstractStorage");
	}
	
	// implémentations par défaut, redéfinies dans les classes héritées
	
	
	/**
	 * Permet de savoir si les classes (d'école) sont gérées par cette classe
	 * Si oui, la fonction loadClasses() est disponible.
	 * @return  bool
	 */
	public function canUseClasses():Boolean {
		return false;
	}

	/**
	 * Charge la liste des classes d'un niveau.
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onLoadClassesList", this) pour recevoir le retour.
	 * event.data.arrClassesList  Tableau d'objets XMLNode avec les infos des classes
	 * 
	 * @param   iLevel       1..4 No du niveau
	 * @param   bForTeacher  (opt, bool, false) Seulement les classes de l'enseingant identifié
	 */
	public function loadClassesList(iLevel:Number, bForTeacher:Boolean) {
		trace("loadClassesList() appelé sur un objet qui ne supporte pas les classes.");
	}
	
	/**
	 * Charge la liste des joueurs.
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onLoadPlayersList", this) pour recevoir le retour.
	 * event.data.arrPlayersList  Tableau d'objets XMLNode avec les infos des players
	 * 
	 * @param   iLevel  1..4 No du niveau
	 * @param   bWithResults (opt, bool, false) Renvoie les résultats des joueurs
	 * @param   arrFilter  (opt, array) Filtre supplémentaire (["class": 23, "teacher": 12, "school": 56])
	 */
	public function loadPlayersList(iLevel:Number, bWithResults:Boolean, arrFilter:Array) {
	}

	/**
	 * Charge la liste des écoles.
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onLoadSchoolsList", this) pour recevoir le retour.
	 * event.data.arrSchoolsList  Tableau d'objets XMLNode avec les infos des écoles
	 */
	public function loadSchoolsList() {
	}
	
	/**
	 * Charge les données d'un joueur.
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onLoadPlayer", this) pour recevoir le retour.
	 * event.data.oPlayer  Instance de LevelXPlayer
	 * 
	 * @param   idPlayer 
	 */
	public function loadPlayer(idPlayer:Number) {
	}
	
	/**
	 * Vérifie le mot de passe du joueur

	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onLoginPlayer", this) pour recevoir le retour.
	 * event.data.result  -1: erreur 0: OK
	 * event.data.error   Détails de l'erreur.
	 * 
	 * @param   idPlayer  
	 * @param   sPassword 
	 */
	public function loginPlayer(idPlayer:Number, sPassword:String){
	}
	
	/**
	 * Change le mot de passe d'un joueur

	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onChangePlayerPassword", this) pour recevoir le retour.
	 * event.data.result  -1: erreur 0: OK
	 * event.data.error   Détails de l'erreur.
	 * 
	 * @param   idPlayer  
	 * @param   sCurrentPassword 
	 * @param   sNewPassword 
	 */
	public function changePlayerPassword(idPlayer:Number, sCurrentPassword:String, sNewPassword:String){
	}

	/**
	 * Sauve les données d'un joueur.
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onSavePlayer", this) pour recevoir le retour.
	 * event.data.oPlayer  Instance de LevelXPlayer
	 * 
	 * @param   oPlayer 
	 */
	public function savePlayer(oPlayer:AbstractPlayer) {
	}
	
	/**
	 * Renomme un joueur. 
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onRenamePlayer", this) pour recevoir le retour.
	 * event.data.result  -1: erreur 0: OK
	 * event.data.error   Détails de l'erreur.
	 * 
	 * @param   idPlayer 
	 * @param   sName   nouveau nom
	 */
	public function renamePlayer(idPlayer:Number, sName:String) {
	}

	/**
	 * Supprime un joueur et tous ses résultats. 
	 * Nécessite un appel préalable de setTeacherLogin() si les classes sont supportées
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onDeletePlayer", this) pour recevoir le retour.
	 * event.data.result  -1: erreur 0: OK
	 * event.data.error   Détails de l'erreur.
	 * 
	 * @param   idPlayer 
	 */
	public function deletePlayer(idPlayer:Number) {
	}

	/**
	 * Réinitialise le mot de passe d'un joueur. 
	 * Nécessite un appel préalable de setTeacherLogin() si les classes sont supportées
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onUnlockPlayer", this) pour recevoir le retour.
	 * event.data.result  -1: erreur 0: OK
	 * event.data.error   Détails de l'erreur.
	 * 
	 * @param   idPlayer 
	 */
	public function unlockPlayer(idPlayer:Number) {
	}

	/**
	 * Crée un joueur dans la classe courante (si applicable) ou dans le niveau
	 * spécifié sinon.
	 * Nécessite un appel préalable de setTeacherLogin() si les classes sont supportées
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onNewPlayer", this) pour recevoir le retour.
	 * event.data.result  -1: erreur 0: OK
	 * event.data.error   Détails de l'erreur.
	 * 
	 * @param   sName   nom du joueur
	 * @param   iLevel  niveau du joueur (utilisé si les classes ne sont pas supportées)
	 */
	public function newPlayer(sName:String, iLevel:Number) {
	}
	
	
	/**
	 * Crée une nouvelle classe si les classes sont supportées.
	 * Nécessite un appel préalable de setTeacherLogin() si les classes sont supportées
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onNewClass", this) pour recevoir le retour.
	 * event.data.result  -1: erreur >0: ID nouvelle classe
	 * event.data.error   Détails de l'erreur.
	 * 
	 * @param   sName   nom du joueur
	 * @param   iLevel  niveau du joueur (utilisé si les classes ne sont pas supportées)
	 */
	public function newClass(sName:String, iLevel:Number) {
	}
	
	
	/**
	 * Supprime une classe et tous ses joueur
	 * Nécessite un appel préalable de setTeacherLogin() si les classes sont supportées
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onDeleteClass", this) pour recevoir le retour.
	 * event.data.result  -1: erreur 0: OK
	 * event.data.error   Détails de l'erreur.
	 * 
	 * @param   idPlayer 
	 */
	public function deleteClass(idClass:Number) {
	}

	/**
	 * Charge les données d'une classe.
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onLoadClass", this) pour recevoir le retour.
	 * event.data.oClass  Instance de Classe
	 * event.data.aoSchools Tableau de noeuds xml des écoles si bWithSchools est true
	 * 
	 * @param   idClass 
	 * @param   oOptions (opt, object)
	 *          bWithSchools (bool, opt, false) renvoie aussi la liste des écoles
	 */
	public function loadClass(idClass:Number, oOptions:Object) {
	}

	/**
	 * Sauve les données d'une classe.
	 * 
	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onSaveClass", this) pour recevoir le retour.
	 * event.data.oClass  Instance de Classe
	 * 
	 * @param   oClass 
	 */
	public function saveClass(oClass:Classe) {
	}
	
	/**
	 * Charge les données d'un enseignant.

	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onLoadTeacher", this) pour recevoir le retour.
	 * event.data.oTeacher  Instance de Teacher
	 * 
	 * @param   idTeacher 
	 */
	public function loadTeacher(idTeacher:Number) {

	}

	/**
	 * Sauve les données d'un enseignant.

	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onSaveTeacher", this) pour recevoir le retour.
	 * event.data.result  -1: erreur 0: OK
	 * event.data.error   Détails de l'erreur.
	 * 
	 * @param   idTeacher 
	 */
	public function saveTeacher(oTeacher:Teacher) {
		
	}
	
	
	/**
	 * Vérifie l'identification d'un enseignant.

	 * Fonction asynchrone. 
	 * Utiliser obj.addEventListener("onLoginTeacher", this) pour recevoir le retour.
	 * event.data.result  -1: erreur 0: OK
	 * event.data.error   Détails de l'erreur.
	 * 
	 * @param   idTeacher 
	 */
	public function loginTeacher(sName:String, sPassword:String) {
		
	}
	
	/**
	 * Définit les infos de login d'un Teacher. Le mot de passe est hashé pour 
	 * les transmissions avec les requêtes qui le nécessitent.
	 * 	 
	 * Fonction supportée seulement par les classes dérivées qui supportent les classes
	 * 
	 * @param   sLogin    
	 * @param   sPassword 
	 */
	public function setTeacherLogin(sLogin:String, sPassword:String) {
		var oMd5 = new md5();
		p_sTeacherLogin = sLogin;
		p_sTeacherHash = oMd5.hash(sPassword);
	}
	
	/**
	 * 
	 * @return  objet avec infos de login cryptées
	 */
	public function getTeacherLogin ():Object {
		return {login:p_sTeacherLogin, hash:p_sTeacherHash};
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	
}
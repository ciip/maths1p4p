﻿/**
 * class com.maths1p4p.application.Application
 * 
 * @author Pierre Rossel
 * @version 1.0
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import mx.utils.Delegate;
import mx.controls.Alert;

import com.prossel.utils.ObjectTools;
import com.prossel.utils.CursorManager;

import maths1p4p.application.AbstractStorage;
import maths1p4p.application.LocalStorage;
import maths1p4p.application.RemoteStorage;
import maths1p4p.application.ClassEditDlg;
import maths1p4p.Teacher;

 
class maths1p4p.application.Application {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	static var p_iLevel:Number;  // niveau actuel 1..4
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	private static var p_sAppVersion:String = "3.0.9";
	
	private var p_sLevelFolder    :String;
	private var p_oStorage        :AbstractStorage;
	
	private var p_iCurrentClassId :Number;
	private var p_iCurrentClassTeacherId :Number;
	
	private static var p_mcl      :MovieClipLoader;
	private static var p_oMclSink :Object;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Implémentation du pattern Singleton
	 * 
	 * @return  l'instance unique de la classe Application
	 */
	public static function getInstance() {
		if (_global.g_oApplication == undefined)
			_global.g_oApplication = new Application;
		return _global.g_oApplication;
	}

	
	/**
	 * Renvoie le contexte d'utilisation de l'application
	 * @return "director" or "browser"
	 */
	public static function getContext() {
		if (_root.g_bDirectorHost)
			return "director";
		else
			return "browser";
	}

	/**
	 * 
	 * @return Le numéro de version de l'application
	 */
	public static function getAppVersion():String {
		return p_sAppVersion;
	}

	/**
	 * Renvoie le chemin du dossier principal de l'application
	 * 
	 * @return  le chemin (avec slash final) 
	 */
	public static function getRootPath():String {
		// TODO Optimize by caching result value in static variable
		var iPos:Number = _root._url.lastIndexOf("/data/");
		if (iPos == -1)
			iPos = _root._url.lastIndexOf("/");
		var sUrl = _root._url.slice(0, iPos + 1);
		return sUrl;
	}
	
	public function getLevelFolder():String {
		return p_sLevelFolder;
	}

	public function setLevelFolder(sFolder:String) {
		p_sLevelFolder = sFolder;
	}


	public static function useLibCursor(mc:Object, sCursor:String){
		CursorManager.register(mc, sCursor, getRootPath() + "data/cursorlib.swf");
	}

	/**
	 * Charge un fichier externe dans clip, puis saute à un repère dans le clip chargé
	 * 
	 * @param   sUrl  Chaine ou tableau de chaines. Si c'est un tableau, les urls sont préchargés dans
	 *                l'ordre dans un clip invisible, puis le dernier est chargé dans oTarget
	 *                Les chemins doivent être donnée par rapport au rootPath
	 *                Ex: "data/level1/level1.swf"
	 * @param   oTarget le clip dans lequel charger
	 * @param   sLabel  (optionnel) nom du repère sur lequel sauter une fois chargé
	 */
	public static function loadClip(sUrl:Object, oTarget:MovieClip, sLabel:String){

		var sRoot = getRootPath();
		//trace("rootpath: " + sRoot);
		if (sUrl instanceof Array) {
			for(var iUrl in sUrl){
				sUrl[iUrl] = sRoot + sUrl[iUrl];
			}
		}
		else {
			sUrl = sRoot + sUrl;
		}
		if (Application.getContext() == "director")
			getURL("event:BoostTempo");
			
		maths1p4p.utils.ClipLoader.load(sUrl, oTarget, sLabel);
		
/*
		trace('loadClip(sUrl: ' + sUrl + ', oTarget: ' + oTarget + ', sLabel: ' + sLabel + ")")
		p_mcl = new MovieClipLoader();
		trace('p_mcl: ' + p_mcl)
		p_oMclSink = new Object();
		
		//if (sLabel != undefined) {
			//p_oMclSink["sLabel"] = sLabel;
			p_oMclSink.onLoadInit = function(mcTarget:MovieClip) {
				trace("onLoadInit(), bytes loaded: " + mcTarget.getBytesLoaded() + " / " + mcTarget.getBytesTotal());
				trace('sLabel: ' + sLabel)
				mcTarget.gotoAndPlay(sLabel);
				this.mcProgress.removeMovieClip();
			}
		//}
		p_oMclSink.onLoadError = function () {
			trace("loadError");			
		}
		p_oMclSink.onLoadStart = function () {
			trace("loadStart");			
		}
		p_oMclSink.onLoadProgress = function (target_mc:MovieClip, loadedBytes:Number, totalBytes:Number)  {
			this.mcProgress.mcBar.setProgress(loadedBytes, totalBytes);
		}

		// crée la barre de progression au top niveau
		var mcProgress = DepthManager.createClassObjectAtDepth(mx.core.UIObject, DepthManager.kTooltip);
		p_oMclSink.mcProgress = mcProgress;
		var mcBar:ProgressBar = mcProgress.createClassObject(mx.controls.ProgressBar, "mcBar", mcProgress.getNextHighestDepth());
		mcBar.mode = "manual";
		mcBar.label = "";

		
		p_mcl.addListener(p_oMclSink);
		trace('p_oMclSink: ' + p_oMclSink)
		p_mcl.loadClip(getRootPath() + sUrl, oTarget);
		trace('loadClip(' + getRootPath() + sUrl + ', ' + oTarget + ')')
		//trace('p_mcl: ' + p_mcl)
		//trace('getRootPath() + sUrl: ' + getRootPath() + sUrl)
*/
	}

	/**
	 * Utilise un stockage local pour les scores. Cette fonction est appelée par
	 * l'application hôte (Director) qui se chargera d'implémenter le stockage
	 * sur le disque de l'utilisateur.
	 */
	public function useLocalStorage(oSink:Object) {
		trace("Using local storage");
		p_oStorage = new LocalStorage(oSink);
	}
	
	
	/**
	 * Permet à l'application Director d'enregistrer un écouteur dans CursorManager.
	 * Director n'a pas accès aux fonctions statiques des classes Flash
	 * 
	 * @param   oListener 
	 */
	public function addCursorManagerListener(oListener) {
		CursorManager.addListener(oListener);
	}
	
	/**
	 * 
	 * @usage   Application.getInstance().getStorage();
	 * @return  Un objet hérité de AbstractStorage (voir AbstractStorage pour l'interface)
	 */
	public function getStorage():AbstractStorage {
		if (p_oStorage == undefined) {
			// RemoteStorage par défaut
			p_oStorage = new RemoteStorage();
		}
		
		return p_oStorage;
	}
	
	/**
	 * Définit l'identificateur de la classe courante, si les classes sont gérées
	 * 
	 * @param   iClassId 
	 * @param   iClassTeacherId
	 */
	public function setCurrentClass(iClassId:Number, iClassTeacherId:Number){
		//trace("Application.setCurrentClass(" + iClassId + ", " + iClassTeacherId + ")");
		p_iCurrentClassId = iClassId;
		p_iCurrentClassTeacherId = iClassTeacherId;
	}
	
	/**
	 * 
	 * @return  l'indentificateur de la classe courante.
	 */
	public function getCurrentClass(){
		//trace("Application.getCurrentClass() -> " + p_iCurrentClassId);
		return p_iCurrentClassId;
	}

	/**
	 * Affiche le dialogue d'édition de la classe courante après avoir identifié 
	 * l'enseignant.
	 * @param parentClip clip parent pour l'affichage du dialogue (en général this fait l'affaire)
	 * @param closeCallback fonction de rappel pour la fermeture de la fenêtre
	 * @return  nothing
	 * 
	 * Exemple: 
	 * 
	 * import maths1p4p.application.Application;
	 * this.bt_Classe.onRelease = function () { 
	 *	 Application.editCurrentClass(this, Delegate.create(this, function(evt:Object){
	 *		// Recharge les classe de l'enseignant
	 *		Alert("TODO: recharger la classe");
	 *		}));
	 * };
	 * 
	 */
	public static function editCurrentClass(parentClip:MovieClip, closeCallback:Object){
		var olLogin = new Object();
		olLogin.onLogin = function (oTeacher:Teacher){
			if (oTeacher != undefined){
				// vérifie que le teacher loggé est bien le propriétaire de la classe courante
				//trace("logged teacher id: " + oTeacher.p_iId);
				//trace("current teacher id: " + Application.getInstance().p_iCurrentClassTeacherId);
				if (Application.getInstance().p_iCurrentClassTeacherId == oTeacher.p_iId) {
					var arrOptions = {param: {idClass: Application.getInstance().getCurrentClass(), idTeacher: oTeacher.p_iId}};
					var dlg = ClassEditDlg.show(parentClip, arrOptions);
					dlg.addEventListener("close", closeCallback);
				}
				else
					Alert.show("Cet enseignant n'est pas propriétaire de cette classe.");
			}
		};
		Teacher.login(_root, olLogin);		
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur privé. Utiliser getInstance() pour créer/obtenir l'instance 
	 * unique de cette classe
	 */
	private function Application()	{
		trace("+++Application");
		System.security.allowDomain("192.168.1.6");
		System.security.allowDomain("maths.prossel.info");
		ObjectTools.init();
		XMLShortcuts.activate();
		com.prossel.utils.ListDoubleClick.init();
		Alert.noLabel = "Non";
		Alert.yesLabel = "Oui";

		// make slightly opaque
		_global.style.modalTransparency = 20;
	}

	
}
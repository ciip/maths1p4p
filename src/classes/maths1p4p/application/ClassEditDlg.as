﻿/**
 * Contenu du dialogue d'édition d'une classe.
 * La fonction statique show() provoque l'affichage du dialogue modal. 
 * 
 * @author Pierre Rossel
 * @version 1.0
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 

import mx.containers.Window;
import mx.managers.PopUpManager;
import mx.controls.Alert;
import mx.utils.Delegate;

import com.prossel.InputBox;
import com.prossel.Dialog;


import maths1p4p.application.Application;
import maths1p4p.application.AbstractStorage;
import maths1p4p.Classe;

class maths1p4p.application.ClassEditDlg extends Dialog {

	var lstPlayers         : mx.controls.List;
	var btnCreatePlayer    : mx.controls.Button;
	var btnDeletePlayer    : mx.controls.Button;
	var btnUnlockPlayer    : mx.controls.Button;
	var btnRenamePlayer    : mx.controls.Button;
	var cboSchool          : mx.controls.ComboBox;
	var txtName            : mx.controls.TextInput;
	var txtPassword        : mx.controls.TextInput;
	var txtPasswordConfirm : mx.controls.TextInput;
	var btnBack     : mx.controls.Button;

	var p_idClass:Number;

	var p_oClass:Classe;


	/**
	 * Affiche un dialogue de gestion des classes
	 * Le retour se fait de manière asynchrone via un objet écouteur
	 * 
	 * Utilisation:
	 * 
		var dlg = maths1p4p.application.ClassEditDlg.show(this);
		dlg.addEventListener("close", Delegate.create(this, function(evt:Object){
			trace("ClassEditDlg closed");
		}));

	 * @param   parent   clip parent (_root si indéfini)
	 * @return  la fenêtre créée
	 */
	static function show (parent, options):Window
	{
		if (options == undefined) options = {};
		options.title = "Modifier une classe";
		
		var dlg = Dialog.show(Application.getRootPath() + "data/classEditDlg.swf", ClassEditDlg, parent, options);

		return dlg;
	}


	function init(param:Object) {
		trace("ClassEditDlg.init()");
		trace("param: " + param);
		//Alert.show("ClassEditDlg.init()");

		p_idClass = param.idClass;
		Application.getInstance().setCurrentClass(p_idClass, param.idTeacher);

		lstPlayers.addEventListener("change", Delegate.create(this, onPlayerChange));
		lstPlayers.addEventListener("doubleClick", Delegate.create(this, renamePlayer));

		btnCreatePlayer.addEventListener("click", Delegate.create(this, createPlayer));

		btnDeletePlayer.addEventListener("click", Delegate.create(this, deletePlayer));
		btnDeletePlayer.enabled = false;
		
		btnUnlockPlayer.addEventListener("click", Delegate.create(this, unlockPlayer));
		btnUnlockPlayer.enabled = false;
		if (Application.p_iLevel < 3)
			btnUnlockPlayer._visible = false;

		btnRenamePlayer.addEventListener("click", Delegate.create(this, renamePlayer));
		btnRenamePlayer.enabled = false;
		
		btnBack.addEventListener("click", Delegate.create(this, function () {
			//trace("Ok");
			
			// save data
			this.saveClass()
			
			//this._parent.dispatchEvent({type:"close"});
		}));
		


		onEnterFrame = function () {
			lstPlayers.setFocus();
			
			// pas encore implémenté
			//cboSchool.enabled = false;
			txtName.enabled = false;
			txtPassword.enabled = false;
			txtPasswordConfirm.enabled = false;

			// call this here, after disabling controls because local storage has immediate response
			loadPlayers();
			//loadSchools();
			loadClass();

			delete onEnterFrame;
		}
	
	}



	private function loadPlayers() {
		
		// charge la liste des classes
		lstPlayers.removeAll();
		lstPlayers.addItem({label: "Chargement en cours..."});
		lstPlayers.enabled = false;
		
		var oStorage:AbstractStorage = Application.getInstance().getStorage();
		oStorage.addEventListener("onLoadPlayersList", this);
		var arrFilter = [];
		arrFilter["class"] = p_idClass;
		oStorage.loadPlayersList(Application.p_iLevel, false, arrFilter);
	}


	private function onLoadPlayersList(event:Object) {
		//trace("ClassManager.onLoadPlayersList(" + event + ")");

		if (event.data.arrPlayersList != undefined) {

			lstPlayers.enabled = true;
			lstPlayers.labelField = "name";
			lstPlayers.dataProvider = event.data.arrPlayersList;
			
			if (lstPlayers.getRowCount()) {
				lstPlayers.selectedIndex = 0;
				onPlayerChange();
			}
		} else {
			lstPlayers.removeAll();
			lstPlayers.addItem({label: "Erreur"});
		}

	}

	private function loadClass() {
		
		// charge les infos de la classe
		cboSchool.removeAll();
		cboSchool.addItem({label: "Chargement en cours..."});
		cboSchool.enabled = false;
		
		var oStorage:AbstractStorage = Application.getInstance().getStorage();
		oStorage.addEventListener("onLoadClass", this);
		oStorage.loadClass(p_idClass, {bWithSchools: true});
	}

	private function onLoadClass(event:Object) {
		//trace("onLoadClass: " + event)
		cboSchool.removeAll();

		var iSelectedSchool = undefined;
		if (event.data.aoSchools != undefined) {
			cboSchool.enabled = true;
			var nSchools = event.data.aoSchools.length;
			for (var iSchool = 0; iSchool < nSchools; iSchool++) {
				cboSchool.addItem({data: event.data.aoSchools[iSchool]._idSchool, label: event.data.aoSchools[iSchool]._name});
				if (Number(event.data.aoSchools[iSchool]._idSchool) == event.data.oClass.p_iSchool){
					//trace("selecting index " + iSchool + " in school list");
					iSelectedSchool = iSchool;
				}
			}
		} else {
			cboSchool.addItem({label: "Erreur"});
		}
		
		if (event.data.oClass != undefined) {
			p_oClass = event.data.oClass;
			
			txtName.enabled = true;
			txtPassword.enabled = true;
			txtPasswordConfirm.enabled = true;
			
			txtName.text = event.data.oClass.p_sName;
			txtPassword.text = event.data.oClass.p_sPass;
			txtPasswordConfirm.text = event.data.oClass.p_sPass;
			
		}

		if (iSelectedSchool == undefined) {
			cboSchool.selectedIndex = undefined;
			cboSchool.textField.text = "";
		}
		else {
			cboSchool.selectedIndex = iSelectedSchool;
		}
	}
	
	private function saveClass() {

		if (p_oClass == undefined) {
			this._parent.dispatchEvent({type:"close"});
			return
		}
		
		// Vérifie les mots de passe
		if (txtPassword.text != txtPasswordConfirm.text) {
			Alert.show("Le mot de passe et sa confirmation ne correspondent pas. Veuillez corriger.", "");
			return;
		}
		
		if (cboSchool.selectedItem.data == undefined && cboSchool.textField.text != "") {
			// nouvelle école
			p_oClass.p_sCreateSchool = cboSchool.textField.text;
		}
		else {
			p_oClass.p_iSchool = (cboSchool.selectedItem.data != undefined) ? cboSchool.selectedItem.data : 0;
			p_oClass.p_sCreateSchool = "";
		}
		
		p_oClass.p_sName = txtName.text;
		p_oClass.p_sPass = txtPassword.text;
		
		
		var oStorage:AbstractStorage = Application.getInstance().getStorage();
		oStorage.addEventListener("onSaveClass", this);
		oStorage.saveClass(p_oClass);
	}
	
	private function onSaveClass(){
		this._parent.dispatchEvent({type:"close"});
	}


	private function createPlayer() {
		var dlg:Window = InputBox.show("Nom de l'élève", "Nouvel élève", "", this);
		
		dlg.addEventListener("ok", Delegate.create(this, function (event:Object) {
			//trace(event);
			var oStorage:AbstractStorage = Application.getInstance().getStorage();
			oStorage.addEventListener("onNewPlayer", this);
			oStorage.newPlayer(event.text, Application.p_iLevel);
		}));
	}
	
	private function onNewPlayer (event:Object) {
		event.target.removeEventListener("onNewPlayer", this);
		loadPlayers();
	}

	private function deletePlayer() {
		var ol = new Object();
		ol.click = Delegate.create(this, function (evt:Object) {
			if (evt.detail == Alert.YES) {
				var idPlayer = lstPlayers.selectedItem._idPlayer;
				var oStorage:AbstractStorage = Application.getInstance().getStorage();
				oStorage.addEventListener("onDeletePlayer", this);
				oStorage.deletePlayer(idPlayer);
			}
		});				

		var dlg = Alert.show(
		   "Etes-vous vraiment sûr de vouloir supprimer cet élève ? L'opération n'est pas réversible.", 
			"Supprimer un élève",
			Alert.YES | Alert.NO,
			_root, 
			ol,
			"", 
			Alert.NO);
	}
	
	private function onDeletePlayer (event:Object) {
		event.target.removeEventListener("onDeletePlayer", this);
		loadPlayers();
	}

	private function unlockPlayer() {
		var ol = new Object();
		ol.click = Delegate.create(this, function (evt:Object) {
			if (evt.detail == Alert.YES) {
				var idPlayer = lstPlayers.selectedItem._idPlayer;
				var oStorage:AbstractStorage = Application.getInstance().getStorage();
				oStorage.addEventListener("onUnlockPlayer", this);
				oStorage.unlockPlayer(idPlayer);
			}
		});				

		var dlg = Alert.show(
		   "Etes-vous sûr de vouloir réinitialiser le mot de passe de cet élève ? L'opération n'est pas réversible.", 
			"Mot de passe de l'élève",
			Alert.YES | Alert.NO,
			_root, 
			ol,
			"", 
			Alert.NO);
	}
	
	private function onUnlockPlayer (event:Object) {
		event.target.removeEventListener("onUnlockPlayer", this);
	}

	private function renamePlayer() {
		var dlg:Window = InputBox.show("Nom de l'élève", lstPlayers.selectedItem._name, "Renommer", this);
		
		dlg.addEventListener("ok", Delegate.create(this, function (event:Object) {
			//trace(event);
			var oStorage:AbstractStorage = Application.getInstance().getStorage();
			oStorage.addEventListener("onRenamePlayer", this);
			oStorage.renamePlayer(lstPlayers.selectedItem._idPlayer, event.text);
		}));
	}
	
	private function onRenamePlayer (event:Object) {
		event.target.removeEventListener("onRenamePlayer", this);
		loadPlayers();
	}

	public function onPlayerChange() {
		//trace("onPlayerChange");
		//trace("lstPlayers.selectedItem.data: " + lstPlayers.selectedItem.data);
		//trace("lstPlayers.selectedItem: " + lstPlayers.selectedItem);
		
		var bEnabled:Boolean = lstPlayers.selectedItem._idPlayer != undefined;
		btnDeletePlayer.enabled = bEnabled;
		btnUnlockPlayer.enabled = bEnabled;
		btnRenamePlayer.enabled = bEnabled;
	}
	
	
}
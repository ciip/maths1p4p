﻿/**
 * Contenu du dialogue de gestion des classes. 
 * La fonction statique show() provoque l'affichage du dialogue modal. 
 * 
 * @author Pierre Rossel
 * @version 1.0
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 

import mx.containers.Window;
import mx.managers.PopUpManager;
import mx.controls.Alert;
import mx.utils.Delegate;

import com.prossel.InputBox;

import maths1p4p.application.Application;
import maths1p4p.application.AbstractStorage;
import maths1p4p.application.ClassEditDlg;
import maths1p4p.Teacher;
//import maths1p4p.application.LoginForm;


class maths1p4p.application.ClassManagerDlg extends MovieClip {

	var lstClasses  : mx.controls.DataGrid;
	var btnEdit     : mx.controls.Button;
	var btnCreate   : mx.controls.Button;
	var btnDelete   : mx.controls.Button;
	var btnBack     : mx.controls.Button;
	
	private var p_idTeacher:Number;

	function ClassManagerDlg() {
		//trace("ClassManagerDlg contructor");
		//init();
	}
	
	//function onLoad(){
		//Alert.show("ClassManagerDlg.onLoad()");
		//init();
	//}

	function init() {
		trace("ClassManagerDlg.Init()");
		var olBack = new Object();
		olBack.click = Delegate.create(this, function () {
			this._parent.dispatchEvent({type:"close"});
		});
		btnBack.addEventListener("click", olBack);
		
		btnEdit.enabled = false;
		btnDelete.enabled = false;

		btnEdit.onRelease = Delegate.create(this, function () {
			editClass(lstClasses.selectedItem._idClass);
		});

		btnDelete.onRelease = Delegate.create(this, function () {
			
			var ol = new Object();
			ol.click = Delegate.create(this, function (evt:Object) {
				trace(evt);
				trace(Alert.YES);
				if (evt.detail == Alert.YES) {
					var idClass = lstClasses.selectedItem._idClass;
					var oStorage:AbstractStorage = Application.getInstance().getStorage();
					oStorage.addEventListener("onDeleteClass", this);
					oStorage.deleteClass(idClass);
				}
			});				

			var dlg = Alert.show(
			   "Etes-vous vraiment sûr de vouloir supprimer cette classe ? L'opération n'est pas réversible.", 
			    "Supprimer une classe",
				Alert.YES | Alert.NO,
				_root, 
				ol,
				"", 
				Alert.NO);
		});

		btnCreate.onRelease = Delegate.create(this, function () {
			//var dlg = Alert.show("TODO.");
			var ol = new Object();
			ol.ok = Delegate.create(this, function (event:Object) {
				trace(event);
				var oStorage:AbstractStorage = Application.getInstance().getStorage();
				oStorage.addEventListener("onNewClass", this);
				oStorage.newClass(event.text, Application.p_iLevel);
			});
			//ol.cancel = Delegate.create(this, function (event:Object) {
				//trace(event);
			//});
			InputBox.show("Nom de la classe", "nouvelle classe", "", this, ol);
		});

		lstClasses.addEventListener("change", Delegate.create(this, onClassChange));
		lstClasses.addEventListener("doubleClick", btnEdit.onRelease);

		
		var olLogin = new Object();
		olLogin.onLogin = Delegate.create(this, function (oTeacher:Teacher){
			trace("onLogin: " + oTeacher);
			if (oTeacher == undefined)
				_parent.dispatchEvent({type:"close"});
			else {
				_parent._visible = true;
				p_idTeacher = oTeacher.p_iId;
				
				// Charge les classe de l'enseignant
				loadClasses();
			}
		});
		
		Teacher.login(_root, olLogin);

		onEnterFrame = function () {
			lstClasses.setFocus();
			delete onEnterFrame;
		}
	}

	
	/**
	 * Affiche un dialogue de gestion des classes
	 * Le retour se fait de manière asynchrone via un objet écouteur
	 * 
	 * Utilisation:
	 * 
 		var ol = new Object();
 		ol.close = Delegate.create(this, function (evt:Object){
			trace("ClassManager closed");
		});
		
		maths1p4p.application.ClassManagerDlg.show(_root, olLogin);

	 * @param   title    Titre du dialogue
	 * @param   parent   clip parent (_root si indéfini)
	 * @param   listener objet écouteur pour événements ok ou cancel
	 * @return  la fenêtre créée
	 */
	static function show (parent,listener):Window
	{
		var o = new Object();
		var modal = true;
		if (parent == undefined){parent = o.parent = _root}else{o.parent = parent};
		o.title = "Gestion des classes";
		//o.closeButton = true;
		//o.validateNow = true;
		o.contentPath = Application.getRootPath() + "data/classManagerDlg.swf";
		o.visible = false;

		var m = PopUpManager.createPopUp(parent, mx.containers.Window, modal, o);
		//var m = PopUpManager.createPopUp(parent, ClassManagerDlg, modal, o);
		if (m == undefined) {
			trace("Failed to create a new Window, probably because there is no Window in the Library");
		}
		//trace("popup: " + m);
		m.addEventListener("close", listener);

		var popup_lo = new Object();
		popup_lo.complete = function(eventObject:Object) {
			
			//trace(eventObject);
			var mcForm:ClassManagerDlg = eventObject.target;
			var mcDlg:MovieClip = mcForm._parent;
			
			// Il arrive sur certains serveurs (pas sur tous) que le clip dans target
			// ne soit pas encore complètement chargé. Dans ce cas il faut encore attendre un peu
			if (mcForm.getBytesTotal() < 0 || mcForm.getBytesLoaded() < mcForm.getBytesTotal()) {
				this.idInterval = setInterval(this, "callback", 100, eventObject);
			}
			else{
				this.doInit(eventObject);
			}
		}
		popup_lo.callback = function (eventObject) {
			var mcForm:ClassManagerDlg = eventObject.target;
			if (mcForm.getBytesTotal() > 0 && mcForm.getBytesLoaded() == mcForm.getBytesTotal()) {
				//this.doInit(eventObject);
				if (this.eventObject == undefined)
					this.eventObject = eventObject;
				else {
					// wait one more interval to let the content finish init
					this.doInit(eventObject);
					clearInterval(this.idInterval);
					delete this.idInterval;
				}
			}
		}
		popup_lo.doInit = function (eventObject:Object){	
			var mcForm:ClassManagerDlg = eventObject.target;
			var mcDlg:MovieClip = mcForm._parent;

			// change la classe du clip qu'on vient de charger
			mcForm.__proto__ = ClassManagerDlg.prototype;
			ClassManagerDlg["apply"](mcForm);
			
			mcDlg.setSize(mcForm._width + 5, mcForm._height + 35);
			mcDlg.move(Stage.width / 2 - mcDlg._width / 2, Stage.height / 2 - mcDlg._height / 2);
			//mcDlg._visible = true;
			
			mcForm.init();
			//mcForm.btnEdit.enabled = false;
			//mcForm.btnDelete.enabled = false;
			//mcForm.lstClasses.addEventListener("change", Delegate.create(mcForm, mcForm.onClassChange));
//			
			//var olLogin = new Object();
			//olLogin.onLogin = Delegate.create(this, function (oTeacher:Teacher){
				//trace("onLogin: " + oTeacher);
				//if (oTeacher == undefined)
					//mcDlg.dispatchEvent({type:"close"});
				//else {
					//mcDlg._visible = true;
//					
					//// Charge les classe de l'enseignant
					//mcForm.loadClasses();
				//}
			//});
//			
			//Teacher.login(_root, olLogin);
		}
		popup_lo.close = function (evt:Object){
			evt.target.deletePopUp();
		}

		m.addEventListener("complete", popup_lo);
		m.addEventListener("close", popup_lo);
		return m;
	}

	private function loadClasses() {
		lstClasses.removeAll();
		lstClasses.removeAllColumns();
		lstClasses.columnNames = ["name", "school"];
		lstClasses.getColumnAt(0).headerText = "Classe";
		lstClasses.getColumnAt(1).headerText = "Ecole";
		lstClasses.getColumnAt(0).sortable = false;
		lstClasses.getColumnAt(1).sortable = false;
		lstClasses.spaceColumnsEqually();
		
		// charge la liste des classes
		lstClasses.addItem({name: "Chargement en cours..."});
		
		var oStorage:AbstractStorage = Application.getInstance().getStorage();
		oStorage.addEventListener("onLoadClassesList", this);
		oStorage.loadClassesList(Application.p_iLevel, true);
	}
	

	private function onLoadClassesList(event:Object) {
		//trace("ClassManager.onLoadClassesList (" + event + ")");

		if (event.data.arrClassesList != undefined) {
			lstClasses.removeAll();
			
			lstClasses.dataProvider = event.data.arrClassesList;
			
			if (lstClasses.getRowCount()) {
				lstClasses.selectedIndex = 0;
				onClassChange();
			}
		} else {
			lstClasses.addItem({name: "Erreur"});
		}
	}
	
	private function editClass(idClass:Number) {
		var arrOptions = {param: {idClass: idClass, idTeacher: p_idTeacher}};
		var dlg = ClassEditDlg.show(this, arrOptions);
		dlg.addEventListener("close", Delegate.create(this, function(evt:Object){
			// Recharge les classe de l'enseignant
			loadClasses();
		}));
	}

	// a new class has been created
	private function onNewClass(event:Object) {
		//trace(event);
		var idClass = event.data.result;
		loadClasses();
		
		// directly edit new class
		editClass(idClass);
	}
	
	
	// a new class has been created
	private function onDeleteClass(event:Object) {
		trace(event);
		loadClasses();
	}
	
	
	public function onClassChange() {
		//trace("onClassChange");
		
		var bEnabled:Boolean = lstClasses.selectedItem._idClass != undefined;
		btnEdit.enabled = bEnabled;
		btnDelete.enabled = bEnabled;
	}
	
	
}
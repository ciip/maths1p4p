﻿/**
 * class com.maths1p4p.application.LocalStorage
 * 
 * @author Pierre Rossel
 * @version 1.0
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.*;
import maths1p4p.AbstractPlayer;
import maths1p4p.Teacher;
import maths1p4p.Classe;
import maths1p4p.level1.Level1Player;
import maths1p4p.level2.Level2Player;
import maths1p4p.level3.Level3Player;
import maths1p4p.level4.Level4Player;
 
class maths1p4p.application.LocalStorage extends AbstractStorage {
	
	
	private var p_oSink:Object;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function LocalStorage(oSink:Object)	{		
		trace("+++ LocalStorage (" + oSink + ")");
		p_oSink = oSink;
		/*
		var oObj:Object = new Object();
		oObj["aa"] = 44;
		var ret = p_oSink.testSink("value 1", 2, { a: 1, b:2 }, new Object(), [4, 1, 8], new Array(11, 22, 33), oObj);
		trace("testSink returned " + ret);
		*/
	}

	/**
	 * @see AbstractStorage
	 */
	public function canUseClasses():Boolean {
		return true;
	}

	/**
	 * @see AbstractStorage
	 */
	public function loadClassesList(iLevel:Number, bForTeacher:Boolean) {
		trace("LocalStorage.LoadClassesList(" + iLevel + ", " + bForTeacher + ")");
		//trace("this: " + this);

		var arrClassesList:Array = p_oSink.loadClassesList(iLevel, bForTeacher ? p_sTeacherLogin : "");
		//trace("arrClassesList: " + arrClassesList);
		this.dispatchEvent({type:"onLoadClassesList", data:{arrClassesList: arrClassesList}});
	}
	

	/**
	 * @see AbstractStorage
	 */
	public function loadPlayersList(iLevel:Number, bWithResults:Boolean, arrFilter:Array) {
		//trace("loadPlayersList(" + iLevel + ", " + bWithResults + ", " + arrFilter+ ")");
		if (bWithResults == undefined) bWithResults = false;
		if (arrFilter == undefined) arrFilter = new Array();
		if (arrFilter["class"] == undefined) arrFilter["class"] = Application.getInstance().getCurrentClass();

		var arrPlayersList:Array = p_oSink.loadPlayersList(iLevel, bWithResults ? 1 : 0, arrFilter);

		this.dispatchEvent({type:"onLoadPlayersList", data:{arrPlayersList: arrPlayersList}});
	}
	
	/**
	 * @see AbstractStorage
	 */
	public function loadSchoolsList() {
		trace("loadSchoolsList() NOT IMPLEMENTED");
/*		
		var lv:LoadVars = new LoadVars();
		lv.action = 'SchoolsList';
			
		p_xmlLoadSchoolsList.onLoad = Delegate.create(this, onLoadSchoolsList);
		lv.sendAndLoad(getRemoteUrl(), p_xmlLoadSchoolsList);*/
	}

	/**
	 * @see AbstractStorage
	 */
	public function loadPlayer(idPlayer:Number) {
		trace("loadPlayer(" + idPlayer + ")");

		var xmlRet:XML = p_oSink.loadPlayer(idPlayer);
		var xmlPlayer = xmlRet['response'].player;
		var sLevel:String = xmlPlayer._level;
		var oPlayer:AbstractPlayer = undefined;
		
		switch (sLevel){
			case '1': oPlayer = new Level1Player(xmlPlayer._name, xmlPlayer._idPlayer); break;
			case '2': oPlayer = new Level2Player(xmlPlayer._name, xmlPlayer._idPlayer); break;
			case '3': oPlayer = new Level3Player(xmlPlayer._name, xmlPlayer._idPlayer); break;
			case '4': oPlayer = new Level4Player(xmlPlayer._name, xmlPlayer._idPlayer); break;
			default:
				trace("ERROR: invalid level in loadPlayer(): " + sLevel);
		}
		if (oPlayer != undefined) {
			oPlayer.setPasswordSet(Boolean(parseInt(xmlPlayer._passwordSet)));
			oPlayer.setState(xmlPlayer.state);
			oPlayer.setResults(xmlPlayer.results);
			//trace('oPlayer: ' + oPlayer)
		}
		
		this.dispatchEvent({type:"onLoadPlayer", data:{oPlayer: oPlayer}});
	}

	/**
	 * @see AbstractStorage
	 */
	public function loginPlayer(idPlayer:Number, sPassword:String){
		trace("loginPlayer(" + idPlayer + ")");

		var oMd5 = new md5();
		var oRet:Object = p_oSink.loginPlayer(idPlayer, oMd5.hash(sPassword));
		this.dispatchEvent({type:"onLoginPlayer", data:{result: oRet.result, error: oRet.error}});
	}

	/**
	 * @see AbstractStorage
	 */
	public function changePlayerPassword(idPlayer:Number, sCurrentPassword:String, sNewPassword:String){
		trace("changePlayerPassword(" + idPlayer + ")");
		
		var oMd5 = new md5();
		var oRet:Object = p_oSink.changePlayerPassword(idPlayer, oMd5.hash(sCurrentPassword), oMd5.hash(sNewPassword));
		this.dispatchEvent({type:"onChangePlayerPassword", data:{result: oRet.result, error: oRet.error}});
	}
	
	/**
	 * @see AbstractStorage
	 */
	public function savePlayer(oPlayer:AbstractPlayer) {
		trace("savePlayer()");

		var oRet:Object = p_oSink.savePlayer(oPlayer.getId(), oPlayer.getName(), oPlayer.getState(), oPlayer.getResults());
		this.dispatchEvent({type:"onSavePlayer", data:{result: oRet.result, error: oRet.error}});
	}

	/**
	 * @see AbstractStorage
	 */
	public function renamePlayer(idPlayer:Number, sName:String) {
		var oRet:Object = p_oSink.renamePlayer(idPlayer, sName);
		this.dispatchEvent({type:"onRenamePlayer", data:{result: oRet.result, error: oRet.error}});
	}

	/**
	 * @see AbstractStorage
	 */
	public function deletePlayer(idPlayer:Number) {
		var oRet:Object = p_oSink.deletePlayer(idPlayer, getTeacherLogin().login, getTeacherLogin().hash);
		this.dispatchEvent({type:"onDeletePlayer", data:{result: oRet.result, error: oRet.error}});
	}

	/**
	 * @see AbstractStorage
	 */
	public function unlockPlayer(idPlayer:Number) {
		var oRet:Object = p_oSink.unlockPlayer(idPlayer, getTeacherLogin().login, getTeacherLogin().hash);
		this.dispatchEvent({type:"onUnlockPlayer", data:{result: oRet.result, error: oRet.error}});
	}

	/**
	 * @see AbstractStorage
	 */
	public function newPlayer(sName:String, iLevel:Number) {
		var oRet:Object = p_oSink.newPlayer(sName, Application.getInstance().getCurrentClass(), getTeacherLogin().login, getTeacherLogin().hash);
		this.dispatchEvent({type:"onNewPlayer", data:{result: oRet.result, error: oRet.error}});
	}
	
	/**
	 * @see AbstractStorage
	 */
	public function newClass(sName:String, iLevel:Number) {
		var oRet:Object = p_oSink.newClass(sName, iLevel, getTeacherLogin().login, getTeacherLogin().hash);
		this.dispatchEvent({type:"onNewClass", data:{result: oRet.result, error: oRet.error}});
	}


	/**
	 * @see AbstractStorage
	 */
	public function deleteClass(idClass:Number) {
		var oRet:Object = p_oSink.deleteClass(idClass, getTeacherLogin().login, getTeacherLogin().hash);
		this.dispatchEvent({type:"onDeleteClass", data:{result: oRet.result, error: oRet.error}});
	}

	/**
	 * @see AbstractStorage
	 */
	public function loadClass(idClass:Number, oOptions:Object) {
		trace("loadClass(" + idClass + ")");
		
		if (oOptions == undefined) {
			oOptions = {};
		}

		var xmlRet:XML = p_oSink.loadClass(idClass, oOptions);

		var oData = {};
		if (xmlRet['response'].classe != undefined) {
			var xmlClass = xmlRet['response'].classe;
			var oClass:Classe = new Classe(xmlClass._idClass);
			oClass.p_iSchool = Number(xmlClass._school);
			oClass.p_iTeacher = Number(xmlClass._teacher);
			oClass.p_sName = xmlClass._name;
			oClass.p_sPass = xmlClass._pass;
			oClass.p_iLevel = xmlClass._level;
	
			var aoSchools = xmlRet['response'].schools.$school;
	
			oData = {oClass: oClass, aoSchools: aoSchools};
		}
		
		this.dispatchEvent({type:"onLoadClass", data:oData});
	}

	/**
	 * @see AbstractStorage
	 */
	public function saveClass(oClass:Classe) {
		var oRet:Object = p_oSink.saveClass(oClass);
		this.dispatchEvent({type:"onSaveClass", data:{result: oRet.result, error: oRet.error}});
	}
	
	
	/**
	 * @see AbstractStorage
	 */
	public function loadTeacher(idTeacher:Number){
		trace("loadTeacher(" + idTeacher + ") NOT IMPLEMENTED");
//
		//var lv:LoadVars = new LoadVars();
		//lv.action = 'GetTeacher';
		//lv.idTeacher= idTeacher;
//
		//p_xmlLoadTeacher.onLoad = Delegate.create(this, onLoadTeacher);
		//lv.sendAndLoad(getRemoteUrl(), p_xmlLoadTeacher);
	}
	
	/**
	 * @see AbstractStorage
	 */
	public function saveTeacher(oTeacher:Teacher) {
		// TODO Implémenter cette fonction
		this.dispatchEvent({type:"onSaveTeacher", data:{result: -1, error: "fonction non implémentée"}});
	}

	/**
	 * @see AbstractStorage
	 */
	public function loginTeacher(sName:String, sPassword:String) {
		trace('loginTeacher')
		var oMd5 = new md5();
		
		p_sTeacherLogin = sName;
		p_sTeacherHash = sPassword == "" ? "" : oMd5.hash(sPassword);
		
		var oResponse:Object = p_oSink.loginTeacher(sName, p_sTeacherHash);
		
		trace('oResponse: ' + oResponse);
		trace('oResponse.result: ' + oResponse.result);
		dispatchSimpleResponse("onLoginTeacher", oResponse);
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	function dispatchSimpleResponse(sType:String, oResponse:Object) {
		this.dispatchEvent({
			type: sType, 
			data:{
				result: oResponse.result, 
				error:  oResponse.error
			}
		});
	}
	
}
﻿/**
 * Contenu du dialogue de login. 
 * La fonction statique show() provoque l'affichage du dialogue modal. 
 * 
 * @author Pierre Rossel
 * @version 1.0
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 

import mx.containers.Window;
import mx.managers.PopUpManager;
import mx.controls.Alert;
import mx.utils.Delegate;


import maths1p4p.application.Application;



//class maths1p4p.application.LoginForm extends mx.containers.Window {
class maths1p4p.application.LoginForm extends MovieClip {

	var btnOk		: mx.controls.Button;
	var btnCancel	: mx.controls.Button;
	var txtName		: mx.controls.TextInput;
	var txtPassword	: mx.controls.TextInput;

	function LoginForm() {
		//trace("LoginForm contructor");
		init();
	}
	

	function init() {
		trace("LoginForm.Init()");
		//Alert.show("LoginForm.init()");
		btnOk.addEventListener("click", Delegate.create(this, onEnter));
		
		btnCancel.addEventListener("click", Delegate.create(this, function () {
			//trace("Cancel");
			_parent.dispatchEvent({type:"cancel"});
		}));
		txtName.addEventListener("enter", Delegate.create(this, onEnter));
		txtPassword.addEventListener("enter", Delegate.create(this, onEnter));

		onEnterFrame = function () {
			txtName.setFocus();
			delete onEnterFrame;
		}
	}

	
	/**
	 * Touche ENTER pressée, ferme le dialogue
	 */
	function onEnter() {
		this._parent.dispatchEvent({type:"ok", name:txtName.text, password:txtPassword.text});
	}
	
	/**
	 * Affiche un dialogue de login (nom et mot de passe)
	 * Le retour se fait de manière asynchrone via un objet écouteur
	 * 
	 * Utilisation:
	 * 
 		var olLogin = new Object();
 		olLogin.ok = Delegate.create(this, function (evt:Object){
			trace("Login: " + evt.name + " / " + evt.password);
		});
 		olLogin.cancel = Delegate.create(this, function (evt:Object){
			trace("Login cancelled");
		});
		
		maths1p4p.application.LoginForm.show("Login", _root, olLogin);

	 * @param   title    Titre du dialogue
	 * @param   parent   clip parent (_root si indéfini)
	 * @param   listener objet écouteur pour événements ok ou cancel
	 * @return  la fenêtre créée
	 */
	static function show (title,parent,listener):Window
	{
		
		var o = new Object();
		var modal = true;
		if (parent == undefined){parent = o.parent = _root}else{o.parent = parent};
		o.title = title;
		//o.closeButton = true;
		o.validateNow = true;
		o.contentPath = Application.getRootPath() + "data/loginForm.swf";
		o.visible = false;

		var m = PopUpManager.createPopUp(parent, mx.containers.Window, modal, o);
		//var m = PopUpManager.createPopUp(parent, LoginForm, modal, o);
		if (m == undefined) {
			trace("Failed to create a new Window, probably because there is no Window in the Library");
		}
		//trace("popup: " + m);
		m.addEventListener("cancel", listener);
		m.addEventListener("ok", listener);

		var popup_lo = new Object();
		popup_lo.complete = function(eventObject:Object) {
			
			//trace(eventObject);
			var mcForm:LoginForm = eventObject.target;
			var mcDlg:MovieClip = mcForm._parent;
			
			// Il arrive sur certains serveurs (pas sur tous) que le clip dans target
			// ne soit pas encore complètement chargé. Dans ce cas il faut encore attendre un peu
			if (mcForm.getBytesTotal() < 0 || mcForm.getBytesLoaded() < mcForm.getBytesTotal()) {
				this.idInterval = setInterval(this, "callback", 100, eventObject);
			}
			else{
				this.doInit(eventObject);
			}
		}
		popup_lo.callback = function (eventObject) {
			var mcForm:LoginForm = eventObject.target;
			if (mcForm.getBytesTotal() > 0 && mcForm.getBytesLoaded() == mcForm.getBytesTotal()) {
				//this.doInit(eventObject);
				if (this.eventObject == undefined)
					this.eventObject = eventObject;
				else {
					// wait one more interval to let the content finish init
					this.doInit(eventObject);
					clearInterval(this.idInterval);
					delete this.idInterval;
				}
			}
		}
		popup_lo.doInit = function (eventObject:Object){	
			var mcForm:LoginForm = eventObject.target;
			var mcDlg:MovieClip = mcForm._parent;

			// change la classe du clip qu'on vient de charger
			mcForm.__proto__ = LoginForm.prototype;
			LoginForm["apply"](mcForm);
			
			mcDlg.setSize(mcForm._width + 5, mcForm._height + 35);
			mcDlg.move(Stage.width / 2 - mcDlg._width / 2, Stage.height / 2 - mcDlg._height / 2);
			mcDlg._visible = true;
		}
		popup_lo.ok = function (evt:Object){
			evt.target.deletePopUp();
		}
		popup_lo.cancel = function (evt:Object){
			evt.target.deletePopUp();
		}

		m.addEventListener("complete", popup_lo);
		m.addEventListener("ok", popup_lo);
		m.addEventListener("cancel", popup_lo);
		return m;
	}
	
	
}
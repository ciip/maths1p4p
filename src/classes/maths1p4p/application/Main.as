﻿/**
 * class com.maths1p4p.Main
 * 
 * @author Pierre Rossel
 * @version 0.1
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;
import maths1p4p.application.AbstractStorage;
import maths1p4p.application.ClassManagerDlg;
import mx.controls.DataGrid;
import mx.controls.CheckBox;
import mx.utils.Delegate;
import mx.controls.Alert;
import XMLShortcuts;
import com.prossel.InputBox;
 
class maths1p4p.application.Main extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	var mcLevel:MovieClip;
	
	// Occurences dans MenuHome
	public var p_btnLevels:Button;
	public var p_btnClass:Button;
	public var p_btnManual:Button;
	public var p_btnCredits:Button;
	
	// Occurences dans MenuLevels
	public var p_btnBackMenuHome:Button;
	public var p_btnLevel1:Button;
	public var p_btnLevel2:Button;
	public var p_btnLevel3:Button;
	public var p_btnLevel4:Button;
	
	// Occurences dans ClassSelection
	public var p_lstClasses:DataGrid;
	public var p_chkDefaultClass:CheckBox;
	public var p_btnCancel:Button;
	public var p_btnOK:Button;
	public var p_btnManageClasses:Button;
	
	// Occurences dans Credits
	public var p_btnQuit:Button;
	public var p_btnBack:Button;
	public var p_mcCreditsTexts:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	//private var p_mcl:MovieClipLoader;
	private static var s_oInstance;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------

	/**
	 * Renvoie l'unique instance ce cette classe (mais ne la crée pas)
	 */
	public static function getInstance():Main	{		
		return s_oInstance;
	}

	
	/**
	 * Constructeur
	 */
	public function Main()	{		
		super();				
		stop();
		
		s_oInstance = this;
		
		// Create Application instance
		Application.getInstance();
	}

	/**
	 * Initialisation du clip
	 */
	public function init() {
		//trace("init Main");
		
		// notifie Director qu'on est prêt
		trace("bDirectorHost: " + _root.g_bDirectorHost);
		if (_root.g_bDirectorHost) {
			getURL("event:Init");
		}
	}

	public function go(sWhere:String){
		unloadLevel();
		gotoAndStop(sWhere);
	}

	public function unloadLevel(){
		mcLevel.level.unloadMovie();
		mcLevel.background.removeMovieClip();
	}

	/**
	 * Initialisation du clip
	 */
	public function initMenuHome() {
		//trace("initMenuHome");
		
		p_btnLevels.onRelease = function () {
			_parent.gotoAndStop("MenuLevels");
		}
		
		p_btnClass.onRelease = function () {
			
			_parent.loadLevel("data/main_class.swf");
		}
		
		p_btnManual.onRelease = function () {
			_parent.loadLevel("data/main_manual.swf");
		}
		
		p_btnCredits._alpha = (Application.getContext() == "director") ? 0 : 100;

		p_btnCredits.onRelease = function () {
			_parent.gotoAndStop("Credits");
		}
		
	}


	/**
	 * Initalise le menu des niveaux
	 * 
	 */
	public function initMenuLevels() {
		p_btnBackMenuHome.onRelease = function () {
			_parent.gotoAndStop("MenuHome");
		}
		
		p_btnLevel1.onRelease = function () {
			_parent.selectLevel(1);
		}
		p_btnLevel2.onRelease = function () {
			//_root.sLevelFolder = "data/level2/";
			//_root.loadMovie("data/level2/level2.swf");
			_parent.selectLevel(2);
		}
		p_btnLevel3.onRelease = function () {
			_parent.selectLevel(3);
		}
		p_btnLevel4.onRelease = function () {
			_parent.selectLevel(4);
		}
		
	}
	


	/**
	 * Initalise l'écran de choix de la classe
	 * 
	 */
	public function initClassSelection() {
		//trace("initClassSelection");
		p_btnBackMenuHome.onRelease = function () {
			_parent.gotoAndStop("MenuHome");
		}

		p_btnCancel.onRelease = function () {
			_parent.gotoAndStop("MenuLevels");
		}

		p_btnOK.onRelease = Delegate.create(this, function () {
			
			//trace("pass: " + p_lstClasses.selectedItem._passHash);
			if (p_lstClasses.selectedItem._passHash != ''){
				var ol = new Object();
				ol.ok = Delegate.create(this, function (event:Object) {
					//trace(event);
					var oMd5 = new md5();
					var sPassHash = oMd5.hash(event.text);
					if (sPassHash == p_lstClasses.selectedItem._passHash){
						OpenSelectedClass();
					}
					else {
						Alert.show("Le mot de passe n'est pas correct.", "");
					}
				});
				InputBox.show("Cette classe est protégée par un mot de passe.", "", "Mot de passe de la classe", this, ol, true);
				
			}
			else {
				OpenSelectedClass();
			}
		});

		p_btnManageClasses.onRelease = Delegate.create(this, function () {
			var ol = new Object();
			ol.close = Delegate.create(this, function (evt:Object) {
				loadClassesList();
			});
			
			ClassManagerDlg.show(null, ol);
			//loadClassesList();
		});
		
		p_lstClasses.addEventListener("change", Delegate.create(this, onClassChange));
		p_lstClasses.addEventListener("doubleClick", p_btnOK.onRelease);
		//trace("initClassSelection done");
		
		loadClassesList();
	}

	public function OpenSelectedClass() {
		// mémorise la classe sélectionnée
		Application.getInstance().setCurrentClass(p_lstClasses.selectedItem._idClass, p_lstClasses.selectedItem._idTeacher);
		
		// sauve la classe de manière permanente
		var so:SharedObject = SharedObject.getLocal("userPrefs");
		if (so.data["level" + Application.p_iLevel] == undefined)
			so.data["level" + Application.p_iLevel] = {};
		so.data["level" + Application.p_iLevel].defaultClass = p_chkDefaultClass.selected ? p_lstClasses.selectedItem._idClass : 0;
		
		loadLevel("level" + Application.p_iLevel);
	}

	public function loadClassesList() {
		p_lstClasses.removeAll();
		p_lstClasses.removeAllColumns();
		p_lstClasses.columnNames = ["name", "school"];
		p_lstClasses.getColumnAt(0).headerText = "Classe";
		p_lstClasses.getColumnAt(1).headerText = "Ecole";
		p_lstClasses.getColumnAt(0).sortable = false;
		p_lstClasses.getColumnAt(1).sortable = false;
		p_lstClasses.spaceColumnsEqually();

		// charge la liste des classes
		p_lstClasses.addItem({name: "Chargement en cours..."});
		var oStorage:AbstractStorage = Application.getInstance().getStorage();
		oStorage.addEventListener("onLoadClassesList", this);
		oStorage.loadClassesList(Application.p_iLevel);
		
		// désactive le bouton OK tant qu'on a pas sélectionné une classe
		p_btnOK.enabled = false;
	}
	
	public function onLoadClassesList(event:Object) {
		//trace("Main.onLoadClassesList (" + event + ")");
		
		event.target.removeEventListener("onLoadClassesList", this);
		
		if (event.data.arrClassesList != undefined) {
			p_lstClasses.removeAll();
			
			p_lstClasses.dataProvider = event.data.arrClassesList;

			if (p_lstClasses.getRowCount()) {
				p_lstClasses.selectedIndex = 0;
				
				var so:SharedObject = SharedObject.getLocal("userPrefs");
				//trace("so.data[level" + Application.p_iLevel + "].defaultClass " + so.data["level" + Application.p_iLevel].defaultClass);
				if (so.data["level" + Application.p_iLevel].defaultClass) {
					// cherche la classe par défaut, ls sélectionne et l'ouvre
					for (var iClass = p_lstClasses.getRowCount() - 1; iClass >= 0; iClass--) {
						if (p_lstClasses.getItemAt(iClass)._idClass == so.data["level" + Application.p_iLevel].defaultClass) {
							p_lstClasses.selectedIndex = iClass;
							p_chkDefaultClass.selected = true;
							OpenSelectedClass();
							break;
						}
					} 
				}

				onClassChange();
			}
		} else {
			p_lstClasses.addItem({name: "Erreur"});
		}
	}
	

	public function onClassChange() {
		//trace("onClassChange");
		p_btnOK.enabled = true;
	}
	
	public function loadLevel(sLevel:String) {
		//trace("loadLevel(" + sLevel + ")");
		
		// Crée un fond pour intercepter les clics qui passeraient à travers le fond du level
		var mcBgnd = mcLevel.createEmptyMovieClip("background", 1);
		mcBgnd.beginFill(0, 0);
		mcBgnd.lineTo(0, Stage.height);
		mcBgnd.lineTo(Stage.width, Stage.height);
		mcBgnd.lineTo(Stage.width, 0);
		mcBgnd.lineTo(0, 0);
		mcBgnd.onRelease = function (){};
		mcBgnd.useHandCursor = false;
		
		// Crée un conteneur pour le level
		mcLevel.createEmptyMovieClip("level", 2);
		
		// Charge le level
		_global.g_oApplication.setLevelFolder("data/" + sLevel + "/");
		if (Key.isDown(Key.SHIFT)) {
			Application.loadClip("data/" + sLevel + "/" + sLevel + "Test.swf", mcLevel.level);
		} else {
			
			var asUrl = [];
			switch(sLevel) {
				case "level1":
					asUrl.push("data/" + sLevel + "/" + sLevel + ".swf");
					break;
				case "level2":
					asUrl.push("data/level2/level2_interface.swf");
					asUrl.push("data/level2/level2_screens.swf");
					asUrl.push("data/" + sLevel + "/" + sLevel + ".swf");
					break;
				case "level3":
					asUrl.push("data/level3/level3_interface.swf");
					asUrl.push("data/level3/level3_screens.swf");
					asUrl.push("data/" + sLevel + "/" + sLevel + ".swf");
					break;
				case "level4":
					asUrl.push("data/level4/level4_interface.swf");
					asUrl.push("data/level4/level4_screens.swf");
					asUrl.push("data/" + sLevel + "/" + sLevel + ".swf");
					break;
				default:
					asUrl.push(sLevel);
			}
			Application.loadClip(asUrl, mcLevel.level);
		}
	}
	
	
	public function initCredits() {
		
		p_btnQuit._visible = Application.getContext() == "director";

		p_btnQuit.onRelease = function (){
			getURL("event:Quit");
		}
		
		p_btnBack.onRelease = function () {
			_parent.gotoAndStop("MenuHome");
		}
		
		p_mcCreditsTexts.txtVersion.text = "Version " + Application.getAppVersion();

	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	private function selectLevel(iLevel:Number) {
		//trace("selectClass");
		//p_iLevel = iLevel;
		Application.p_iLevel = iLevel;
		
		// si non, affiche l'écran de sélection de classe
		this.gotoAndStop("ClassSelection");
	}

	
	
}
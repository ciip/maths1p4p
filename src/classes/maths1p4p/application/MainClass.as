﻿/**
 * La partie classe virtuelle de l'application, qui permet d'accéder à 
 * la bibliothèque, au coin parents et aux jeux.
 * 
 * @author Pierre Rossel
 * @version 1.0
 *
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import mx.utils.Delegate;
import mx.controls.Button;
import mx.video.FLVPlayback;

//import com.prossel.utils.CursorManager;

import maths1p4p.application.Application;

class maths1p4p.application.MainClass extends MovieClip {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	var btnBiblio  :Button;
	var btnOrdi    :Button;
	var btnParents :Button;
	var btnQuit    :Button;

	var btnGo      :Button;
	var btnBack    :Button;
	
	var btnPrev    :Button;
	var btnPause   :Button;
	var btnPlay    :Button;
	var btnNext    :Button;
	var btnEject   :Button;

	var vdoBiblio     :FLVPlayback;
	var vdoBack       :FLVPlayback;
	var vdoOrdi       :FLVPlayback;
	var vdoParents    :FLVPlayback;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------


	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function MainClass() {
		super();
		Application.getInstance();
	}
	
	public function onLoad() {
		trace("MainClass.onLoad");
		_parent.stop();
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	private function initClass(){
		stop();

		btnBiblio.onRelease = function() {_parent.gotoAndStop("biblio");}
		btnOrdi.onRelease = function() {_parent.gotoAndStop("ordi");}
		btnParents.onRelease = function() {
			if (Key.isDown(Key.SHIFT))
				_parent.gotoAndStop("parents_film");
			else
				_parent.gotoAndStop("parents");
		}
		btnQuit.onRelease = function() {maths1p4p.application.Main.getInstance().go("MenuHome");}
		
		Application.useLibCursor(btnBiblio, "cursor-arrow-up");
		Application.useLibCursor(btnOrdi, "cursor-arrow-up");
		Application.useLibCursor(btnParents, "cursor-arrow-up");
		Application.useLibCursor(btnQuit, "cursor-arrow-down");
		/* exemple redéfinition onRollOver et appel implémentation par défaut
		btnOrdi.onRollOver = function () {
			trace("rollover ordi");
			this.onRollOver_default();
		}
		*/
	}
	
	
	private function initBiblio(){
		vdoBiblio.contentPath = Application.getRootPath() + "data/movie/classe_biblio.flv";
		btnGo._visible = false;
		btnBack._visible = false;
		
		Application.useLibCursor(btnGo, "cursor-arrow-up");
		Application.useLibCursor(btnBack, "cursor-arrow-down");

		vdoBiblio.addEventListener("complete", Delegate.create(this, function(){
			trace("video done");
			btnGo._visible = true;
			btnBack._visible = true;
			
			btnGo.onRelease = function () {
				trace("pas encore imlémenté");
			}
			btnBack.onRelease = function () {
				_parent.gotoAndStop("biblio_back");
			}
		}));
	}

	private function initBiblioBack(){
		vdoBack.contentPath = Application.getRootPath() + "data/movie/retour_biblio.flv";
		vdoBack.addEventListener("complete", Delegate.create(this, function(){
			gotoAndStop("class");
		}));
	}

	private function initOrdi(){
		vdoOrdi.contentPath = Application.getRootPath() + "data/movie/Classe_ordi.flv";
		btnGo._visible = false;
		btnBack._visible = false;

		Application.useLibCursor(btnGo, "cursor-arrow-up");
		Application.useLibCursor(btnBack, "cursor-arrow-down");
		
		vdoOrdi.addEventListener("complete", Delegate.create(this, function(){
			trace("video done");
			btnGo._visible = true;
			btnBack._visible = true;

			btnGo.onRelease = function () {
				//trace("clic " + this);
				maths1p4p.application.Main.getInstance().go("MenuLevels");
			}
			btnBack.onRelease = function () {
				_parent.gotoAndStop("ordi_back");
			}
		}));
		
	}

	private function initOrdiBack(){
		vdoBack.contentPath = Application.getRootPath() + "data/movie/Retour_ordi.flv";
		vdoBack.addEventListener("complete", Delegate.create(this, function(){
			gotoAndStop("class");
		}));
	}

	private function initParents(){
		vdoParents.contentPath = Application.getRootPath() + "data/movie/classe_tv.flv";
		
		vdoParents.addEventListener("complete", Delegate.create(this, function(){
			trace("video done");
			vdoParents.onRelease = function () {
				_parent.gotoAndStop("parents_tape");
				delete this.onRelease;
			};
		}));
	}

	private function initParentsTape(){
		vdoParents.contentPath = Application.getRootPath() + "data/movie/casette_logo.flv";
		
		vdoParents.addEventListener("complete", Delegate.create(this, function(){
			//trace("video done");
			gotoAndStop("parents_film");
		}));
	}

	private function initParentsFilm(){
		vdoParents.contentPath = Application.getRootPath() + "data/movie/C_ENF.flv";
		
		btnPrev.onRelease  = function () {_parent.vdoParents.seek(Math.max(0, _parent.vdoParents.playheadTime - 60)); }
		btnPause.onRelease = function () {_parent.vdoParents.pause(); }
		btnPlay.onRelease  = function () {_parent.vdoParents.play(); } 
		btnNext.onRelease  = function () {_parent.vdoParents.seek(_parent.vdoParents.playheadTime + 60);} 
		btnEject.onRelease = function () {_parent.gotoAndStop("class"); }
	}
}
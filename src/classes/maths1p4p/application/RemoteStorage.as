﻿/**
 * Classe stockage des données de l'application à distance, via script PHP. 
 * 
 * @author Pierre Rossel
 * @version 1.0
 * 
 * TODO :
 * - implémenter saveTeacher()
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 

import maths1p4p.application.*;
import maths1p4p.AbstractPlayer;
import maths1p4p.Teacher;
import maths1p4p.Classe;
import maths1p4p.level1.Level1Player;
import maths1p4p.level2.Level2Player;
import maths1p4p.level3.Level3Player;
import maths1p4p.level4.Level4Player;

import mx.managers.DepthManager;
import mx.core.UIObject;
import mx.utils.Delegate;

 
class maths1p4p.application.RemoteStorage extends AbstractStorage {
	
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	//private var p_xml:XML;
	private var p_xmlLoadClassesList:XML;
	private var p_xmlLoadPlayersList:XML;
	private var p_xmlLoadSchoolsList:XML;
	private var p_xmlLoadPlayer:XML;
	private var p_xmlLoginPlayer:XML;
	private var p_xmlChangePlayerPassword:XML;
	private var p_xmlSavePlayer:XML;
	private var p_xmlNewPlayer:XML;
	private var p_xmlRenamePlayer:XML;
	private var p_xmlDeletePlayer:XML;
	private var p_xmlUnlockPlayer:XML;
	private var p_xmlNewClass:XML;
	private var p_xmlDeleteClass:XML;
	private var p_xmlLoadClass:XML;
	private var p_xmlSaveClass:XML;
	private var p_xmlLoadTeacher:XML;
	private var p_xmlSaveTeacher:XML;
	private var p_xmlLoginTeacher:XML;
	private var p_mcAsyncRequestDlg:UIObject;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function RemoteStorage()	{		
		super();
		trace("+++ RemoteStorage on " + getRemoteUrl());
		p_xmlLoadClassesList = new XML();
		p_xmlLoadPlayersList = new XML();
		p_xmlLoadSchoolsList = new XML();
		p_xmlLoadPlayer = new XML();
		p_xmlLoginPlayer = new XML();
		p_xmlChangePlayerPassword = new XML();
		p_xmlSavePlayer = new XML();
		p_xmlNewPlayer = new XML();
		p_xmlRenamePlayer = new XML();
		p_xmlDeletePlayer = new XML();
		p_xmlUnlockPlayer = new XML();
		p_xmlNewClass = new XML();
		p_xmlDeleteClass = new XML();
		p_xmlLoadClass = new XML();
		p_xmlSaveClass = new XML();
		p_xmlLoadTeacher = new XML();
		p_xmlSaveTeacher = new XML();
		p_xmlLoginTeacher = new XML();
	}

	/**
	 * @see AbstractStorage
	 */
	public function canUseClasses():Boolean {
		return true;
	}

	/**
	 * @see AbstractStorage
	 */
	public function loadClassesList(iLevel:Number, bForTeacher:Boolean) {
		var lv:LoadVars = new LoadVars();
		lv.action = 'ClassesList';
		lv.level = iLevel;
		lv.teacher = bForTeacher ? p_sTeacherLogin : "";
		
		p_xmlLoadClassesList.onLoad = Delegate.create(this, onLoadClassesList);
		lv.sendAndLoad(getRemoteUrl(), p_xmlLoadClassesList);
		InitAsyncRequest();
	}
	

	/**
	 * @see AbstractStorage
	 */
	public function loadPlayersList(iLevel:Number, bWithResults:Boolean, arrFilter:Array) {
		//trace("loadPlayersList(" + iLevel + ", " + bWithResults + ", " + arrFilter+ ")");
		if (bWithResults == undefined) bWithResults = false;
		if (arrFilter == undefined) arrFilter = [];
		
		if (arrFilter["class"] == undefined) arrFilter["class"] = Application.getInstance().getCurrentClass();
		
		var lv:LoadVars = new LoadVars();
		lv.action = 'PlayersList';
		lv.level = iLevel;
		lv.withResults = bWithResults ? 1 : 0;
		if (arrFilter["class"] != undefined)
			lv.idClass = arrFilter["class"];
		if (arrFilter["teacher"] != undefined)
			lv.idTeacher = arrFilter["teacher"];
		if (arrFilter["school"] != undefined)
			lv.idSchool = arrFilter["school"];
		
		//trace('lv: ' + lv)
			
			
		p_xmlLoadPlayersList.onLoad = Delegate.create(this, onLoadPlayersList);
		lv.sendAndLoad(getRemoteUrl(), p_xmlLoadPlayersList);
		InitAsyncRequest();
	}
	
	/**
	 * @see AbstractStorage
	 */
	public function loadSchoolsList() {
		//trace("loadSchoolsList(" + iLevel + ", " + bWithResults + ", " + arrFilter+ ")");
		
		var lv:LoadVars = new LoadVars();
		lv.action = 'SchoolsList';
			
		p_xmlLoadSchoolsList.onLoad = Delegate.create(this, onLoadSchoolsList);
		lv.sendAndLoad(getRemoteUrl(), p_xmlLoadSchoolsList);
		InitAsyncRequest();
	}

	/**
	 * @see AbstractStorage
	 */
	public function loadPlayer(idPlayer:Number) {
		//trace("loadPlayer(" + idPlayer + ")");

		var lv:LoadVars = new LoadVars();
		lv.action = 'GetPlayer';
		lv.idPlayer= idPlayer;

		p_xmlLoadPlayer.onLoad = Delegate.create(this, onLoadPlayer);
		lv.sendAndLoad(getRemoteUrl(), p_xmlLoadPlayer);
		InitAsyncRequest();
	}

	/**
	 * @see AbstractStorage
	 */
	public function loginPlayer(idPlayer:Number, sPassword:String){
		var oMd5 = new md5();
		var lv:LoadVars = new LoadVars();
		lv.action = 'LoginPlayer';
		lv.idPlayer = idPlayer;
		lv.playerHash = oMd5.hash(sPassword);
		
		p_xmlLoginPlayer.onLoad = Delegate.create(this, onLoadLoginPlayer);
		lv.sendAndLoad(getRemoteUrl(), p_xmlLoginPlayer);
		InitAsyncRequest();
	}

	/**
	 * @see AbstractStorage
	 */
	public function changePlayerPassword(idPlayer:Number, sCurrentPassword:String, sNewPassword:String){
		var oMd5 = new md5();
		var lv:LoadVars = new LoadVars();
		lv.action = 'ChangePlayerPassword';
		lv.idPlayer = idPlayer;
		lv.playerHash = oMd5.hash(sCurrentPassword);
		lv.playerNewHash = oMd5.hash(sNewPassword);
		
		p_xmlChangePlayerPassword.onLoad = Delegate.create(this, onLoadChangePlayerPassword);
		lv.sendAndLoad(getRemoteUrl(), p_xmlChangePlayerPassword);
		InitAsyncRequest();
	}
	
	/**
	 * @see AbstractStorage
	 */
	public function savePlayer(oPlayer:AbstractPlayer) {
		var lv:LoadVars = new LoadVars();
		lv.action = 'SavePlayer';
		lv.idPlayer = oPlayer.getId();
		var state = oPlayer.getState(); 
		if (state != undefined)
			lv.state = state.toString();
		var results = oPlayer.getResults();
		if (results != undefined)
			lv.results = results.toString();
		//trace('lv.state: ' + lv.state)
		
		p_xmlSavePlayer.onLoad = Delegate.create(this, onLoadSavePlayer);
		lv.sendAndLoad(getRemoteUrl(), p_xmlSavePlayer);
		InitAsyncRequest();
	}

	/**
	 * @see AbstractStorage
	 */
	public function renamePlayer(idPlayer:Number, sName:String) {
		var lv:LoadVars = new LoadVars();
		lv.action = 'RenamePlayer';
		lv.idPlayer = idPlayer;
		lv.name = sName;
		
		p_xmlRenamePlayer.onLoad = Delegate.create(this, onLoadRenamePlayer);
		lv.sendAndLoad(getRemoteUrl(), p_xmlRenamePlayer);
		InitAsyncRequest();
	}

	/**
	 * @see AbstractStorage
	 */
	public function deletePlayer(idPlayer:Number) {
		var lv:LoadVars = new LoadVars();
		lv.action = 'DeletePlayer';
		lv.idPlayer = idPlayer;
		lv.teacherLogin = getTeacherLogin().login;
		lv.teacherHash = getTeacherLogin().hash;
		
		p_xmlDeletePlayer.onLoad = Delegate.create(this, onLoadDeletePlayer);
		lv.sendAndLoad(getRemoteUrl(), p_xmlDeletePlayer);
		InitAsyncRequest();
	}

	/**
	 * @see AbstractStorage
	 */
	public function unlockPlayer(idPlayer:Number) {
		var lv:LoadVars = new LoadVars();
		lv.action = 'UnlockPlayer';
		lv.idPlayer = idPlayer;
		lv.teacherLogin = getTeacherLogin().login;
		lv.teacherHash = getTeacherLogin().hash;
		
		p_xmlUnlockPlayer.onLoad = Delegate.create(this, onLoadUnlockPlayer);
		lv.sendAndLoad(getRemoteUrl(), p_xmlUnlockPlayer);
		InitAsyncRequest();
	}

	/**
	 * @see AbstractStorage
	 */
	public function newPlayer(sName:String, iLevel:Number) {
		var lv:LoadVars = new LoadVars();
		lv.action = 'NewPlayer';
		lv.name = sName;
		lv.idClass = Application.getInstance().getCurrentClass();
		lv.teacherLogin = getTeacherLogin().login;
		lv.teacherHash = getTeacherLogin().hash;
		
		p_xmlNewPlayer.onLoad = Delegate.create(this, onLoadNewPlayer);
		lv.sendAndLoad(getRemoteUrl(), p_xmlNewPlayer);
		InitAsyncRequest();
	}
	
	/**
	 * @see AbstractStorage
	 */
	public function newClass(sName:String, iLevel:Number) {
		var lv:LoadVars = new LoadVars();
		lv.action = 'NewClass';
		lv.name = sName;
		lv.level = iLevel;
		lv.teacherLogin = getTeacherLogin().login;
		lv.teacherHash = getTeacherLogin().hash;

		p_xmlNewClass.onLoad = Delegate.create(this, onLoadNewClass);
		lv.sendAndLoad(getRemoteUrl(), p_xmlNewClass);
		InitAsyncRequest();
	}


	/**
	 * @see AbstractStorage
	 */
	public function deleteClass(idClass:Number) {
		trace('deleteClass(idClass: ' + idClass + ')')
		
		var lv:LoadVars = new LoadVars();
		lv.action = 'DeleteClass';
		lv.idClass = idClass;
		lv.teacherLogin = getTeacherLogin().login;
		lv.teacherHash = getTeacherLogin().hash;
		
		p_xmlDeleteClass.onLoad = Delegate.create(this, onLoadDeleteClass);
		lv.sendAndLoad(getRemoteUrl(), p_xmlDeleteClass);
		InitAsyncRequest();
	}

	/**
	 * @see AbstractStorage
	 */
	public function loadClass(idClass:Number, oOptions:Object) {
		//trace("loadClass(" + idClass + ")");
		
		if (oOptions == undefined) {
			oOptions = {};
		}
		
		var lv:LoadVars = new LoadVars();
		lv.action = 'GetClass';
		lv.idClass = idClass;
		if (oOptions.bWithSchools != undefined)
			lv.optionWithSchools = oOptions.bWithSchools ? 1 : 0;
			
		//trace('lv: ' + lv)

		p_xmlLoadClass.onLoad = Delegate.create(this, onLoadClass);
		lv.sendAndLoad(getRemoteUrl(), p_xmlLoadClass);
		InitAsyncRequest();
	}

	/**
	 * @see AbstractStorage
	 */
	public function saveClass(oClass:Classe) {
		var lv:LoadVars = new LoadVars();
		lv.action = 'SaveClass';
		lv.idClass = oClass.p_iId;
		lv.school = oClass.p_iSchool;
		lv.name = oClass.p_sName;
		lv.pass = oClass.p_sPass;
		if (oClass.p_sCreateSchool != ""){
			lv.createSchool = oClass.p_sCreateSchool;
		}
		
		//trace('lv: ' + lv)
		
		p_xmlSaveClass.onLoad = Delegate.create(this, onLoadSaveClass);
		lv.sendAndLoad(getRemoteUrl(), p_xmlSaveClass);
		InitAsyncRequest();
	}
	
	
	/**
	 * @see AbstractStorage
	 */
	public function loadTeacher(idTeacher:Number){
		//trace("loadTeacher(" + idTeacher + ")");

		var lv:LoadVars = new LoadVars();
		lv.action = 'GetTeacher';
		lv.idTeacher= idTeacher;

		p_xmlLoadTeacher.onLoad = Delegate.create(this, onLoadTeacher);
		lv.sendAndLoad(getRemoteUrl(), p_xmlLoadTeacher);
		InitAsyncRequest();
	}
	
	/**
	 * @see AbstractStorage
	 */
	public function saveTeacher(oTeacher:Teacher) {
		// TODO Implémenter cette fonction
		this.dispatchEvent({type:"onSaveTeacher", data:{result: -1, error: "fonction non implémentée"}});
	}

	/**
	 * @see AbstractStorage
	 */
	public function loginTeacher(sName:String, sPassword:String) {
		trace('loginTeacher')
		
		// special case for logins ending in "educanet2.ch", password is sent unhashed for remote verification
		var sSearch:String = "educanet2.ch";
		var bSendCryptPass:Boolean = true;
		if (sName.lastIndexOf(sSearch) == sName.length - sSearch.length) {
			// do not encrypt password
			bSendCryptPass = false;
		}
		
		var oMd5 = new md5();
		var sPassHash:String = oMd5.hash(sPassword);
		
		var lv:LoadVars = new LoadVars();
		lv.action = 'LoginTeacher';
		lv.name = sName;
		lv.passHash = sPassword == "" ? "" : (bSendCryptPass ? sPassHash : sPassword);

		p_sTeacherLogin = sName;
		p_sTeacherHash = sPassword == "" ? "" : sPassHash; // store hashed pass for subsequent operations

		p_xmlLoginTeacher.onLoad = Delegate.create(this, onLoadLoginTeacher);
		lv.sendAndLoad(getRemoteUrl(), p_xmlLoginTeacher);
		InitAsyncRequest();
	}

	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * @return l'url du script qui implémente une partie du stockage à distance
	 */
	public static function getRemoteUrl() {
		//trace(_root._url);
		
		// TODO: supprimer ça à la fin du développement ou le rendre accessible uniquement aux développeurs
		var bAllowLocalAccessToDevServer = false;
		
		var sUrl:String;
		if (_root._url.indexOf("file:///E|/Projects/CIIP/Math%201P%2D4P/svn/trunk") == 0) {   // config de test locale chez Pierre Rossel
			sUrl = "http://192.168.1.6/projects/CIIP/Math%201P-4P/svn/trunk/src/remote/request.php";
		} else if (bAllowLocalAccessToDevServer && _root._url.indexOf("file://") == 0) {  // cas général d'un test en local
			sUrl = "http://maths.prossel.info/demo/remote/request.php";
		} else if (_root._url.indexOf("http://svn.prossel.info/maths/trunk/src") == 0) {  // test directement sur le SVN en ligne
			sUrl = "http://maths.prossel.info/demo/remote/request.php";
		} else { // cas général http
			var iPos:Number = _root._url.lastIndexOf("/data");
			if (iPos == -1)
				iPos = _root._url.lastIndexOf("/");
			
			sUrl = _root._url.slice(0, iPos) + "/remote/request.php";
		}
		//trace("Remote url: " + sUrl);
		return sUrl;
	}
	
	private function InitAsyncRequest() {
		
		if (p_mcAsyncRequestDlg == undefined) {
			
			// Crée une fenêtre transparente pour lui coller le curseur pendant l'opération asynchrone
			// Peut-être un jour on lui ajoute un bouton annuler en cas de non réponse.
			p_mcAsyncRequestDlg = DepthManager.createClassObjectAtDepth(
				UIObject, 
				DepthManager.kTooltip
			);
			if (p_mcAsyncRequestDlg == undefined) {
				trace("RemoteStorage.InitAsyncRequest() could not successfully DepthManager.createClassObjectAtDepth()");
			}
			
			with(p_mcAsyncRequestDlg){
				beginFill(0xFF0000, 0);
				moveTo(0, 0);
				drawRect(0, 0, _root._width, _root._height);
				endFill();
			}
			
			Application.useLibCursor(p_mcAsyncRequestDlg, "cursor-hourglass");
		}
	}
	
	private function DoneAsyncRequest() {
		if (p_mcAsyncRequestDlg != undefined){
			p_mcAsyncRequestDlg.removeMovieClip();
			p_mcAsyncRequestDlg = undefined;
		}
	}

	private function onLoadClassesList(bSuccess) {
		//trace("RemoteStorage.onLoadClassesList(" + bSuccess + ")");
		//trace("this: " + this);

		var arrClassesList:Array;
		if (bSuccess) {
			arrClassesList = p_xmlLoadClassesList['response'].classes.$class;
		}
		
		DoneAsyncRequest();
		this.dispatchEvent({type:"onLoadClassesList", data:{arrClassesList: arrClassesList}});
	}


	private function onLoadPlayersList(bSuccess) {
		//trace("RemoteStorage.onLoadPlayersList(" + bSuccess + ")");
		//trace("this: " + this);

		var arrPlayersList:Array;
		if (bSuccess) {
			arrPlayersList = p_xmlLoadPlayersList['response'].players.$player;
		}
		
		DoneAsyncRequest();
		this.dispatchEvent({type:"onLoadPlayersList", data:{arrPlayersList: arrPlayersList}});
	}
	
	private function onLoadSchoolsList(bSuccess) {
		//trace("RemoteStorage.onLoadSchoolsList(" + bSuccess + ")");
		//trace("this: " + this);

		var arrSchoolsList:Array;
		if (bSuccess) {
			arrSchoolsList = p_xmlLoadSchoolsList['response'].schools.$school;
		}
		
		DoneAsyncRequest();
		this.dispatchEvent({type:"onLoadSchoolsList", data:{arrSchoolsList: arrSchoolsList}});
	}
	
	private function onLoadPlayer(bSuccess) {
		//trace("onLoadPlayer(" + bSuccess + ")");

		var xmlPlayer = p_xmlLoadPlayer['response'].player;
		var sLevel:String = xmlPlayer._level;
		var oPlayer:AbstractPlayer;
		
		switch (sLevel){
			case '1': oPlayer = new Level1Player(xmlPlayer._name, xmlPlayer._idPlayer); break;
			case '2': oPlayer = new Level2Player(xmlPlayer._name, xmlPlayer._idPlayer); break;
			case '3': oPlayer = new Level3Player(xmlPlayer._name, xmlPlayer._idPlayer); break;
			case '4': oPlayer = new Level4Player(xmlPlayer._name, xmlPlayer._idPlayer); break;
		}
		oPlayer.setPasswordSet(Boolean(parseInt(xmlPlayer._passwordSet)));
		oPlayer.setState(xmlPlayer.state);
		oPlayer.setResults(xmlPlayer.results);
		//trace('oPlayer: ' + oPlayer)
		
		DoneAsyncRequest();
		this.dispatchEvent({type:"onLoadPlayer", data:{oPlayer: oPlayer}});
	}
	
	private function onLoadTeacher(bSuccess) {
		//trace("onLoadTeacher(" + bSuccess + ")");

		var xmlTeacher = p_xmlLoadTeacher['response'].teacher;
		var oTeacher = new Teacher(xmlTeacher._idTeacher);
		oTeacher.p_sLogin = xmlTeacher._login;
		oTeacher.p_sFirstName = xmlTeacher._firstName;
		oTeacher.p_sLastName = xmlTeacher._lastName;
		oTeacher.p_sEmail = xmlTeacher._email;		

		DoneAsyncRequest();
		this.dispatchEvent({type:"onLoadTeacher", data:{oTeacher: oTeacher}});
	}
	
	private function onLoadLoginPlayer(bSuccess) {
		//trace("onLoadLoginPlayer(" + bSuccess + ")");
		DoneAsyncRequest();
		this.dispatchEvent({
			type:"onLoginPlayer", 
			data:{
				result: p_xmlLoginPlayer["response"].result._value, 
				error:  p_xmlLoginPlayer["response"].error.__text
			}
		});
	}

	private function onLoadChangePlayerPassword(bSuccess) {
		//trace("onLoadChangePlayerPassword(" + bSuccess + ")");
		DoneAsyncRequest();
		this.dispatchEvent({
			type:"onChangePlayerPassword", 
			data:{
				result: p_xmlChangePlayerPassword["response"].result._value, 
				error:  p_xmlChangePlayerPassword["response"].error.__text
			}
		});
	}

	private function onLoadSavePlayer(bSuccess) {
		//trace("onLoadSavePlayer(" + bSuccess + ")");
		DoneAsyncRequest();
		this.dispatchEvent({
			type:"onSavePlayer", 
			data:{
				result: p_xmlSavePlayer["response"].result._value, 
				error:  p_xmlSavePlayer["response"].error.__text
			}
		});
	}

	private function onLoadDeletePlayer(bSuccess) {
		//trace("onLoadDeletePlayer(" + bSuccess + ")");
		//trace("p_xmlDeletePlayer.status = " + p_xmlDeletePlayer.status);
		
		DoneAsyncRequest();
		this.dispatchEvent({
			type:"onDeletePlayer", 
			data:{
				result: p_xmlDeletePlayer["response"].result._value, 
				error:  p_xmlDeletePlayer["response"].error.__text
			}
		});
	}

	private function onLoadUnlockPlayer(bSuccess) {
		trace("onLoadUnlockPlayer(" + bSuccess + ")");
		trace("p_xmlUnlockPlayer.status = " + p_xmlUnlockPlayer.status);
		
		DoneAsyncRequest();
		this.dispatchEvent({
			type:"onUnlockPlayer", 
			data:{
				result: p_xmlUnlockPlayer["response"].result._value, 
				error:  p_xmlUnlockPlayer["response"].error.__text
			}
		});
	}

	private function onLoadRenamePlayer(bSuccess) {
		//trace("onLoadRenamePlayer(" + bSuccess + ")");
		
		DoneAsyncRequest();
		this.dispatchEvent({
			type:"onRenamePlayer", 
			data:{
				result: p_xmlRenamePlayer["response"].result._value, 
				error:  p_xmlRenamePlayer["response"].error.__text
			}
		});
	}

	private function onLoadNewPlayer(bSuccess) {
		//trace("onLoadNewPlayer(" + bSuccess + ")");

		DoneAsyncRequest();
		this.dispatchEvent({
			type:"onNewPlayer", 
			data:{
				result: p_xmlNewPlayer["response"].result._value, 
				error: p_xmlNewPlayer["response"].error.__text
			}
		});
	}

	private function onLoadNewClass(bSuccess) {
		//trace("onLoadNewClass(" + bSuccess + ")");

		DoneAsyncRequest();
		this.dispatchEvent({
			type:"onNewClass", 
			data:{
				result: p_xmlNewClass["response"].result._value, 
				error: p_xmlNewClass["response"].error.__text
			}
		});
	}

	private function onLoadDeleteClass(bSuccess) {
		trace("onLoadDeleteClass(" + bSuccess + ")");
		DoneAsyncRequest();
		this.dispatchEvent({
			type:"onDeleteClass", 
			data:{
				result: p_xmlDeleteClass["response"].result._value, 
				error:  p_xmlDeleteClass["response"].error.__text
			}
		});
	}

	private function onLoadClass(bSuccess) {
		//trace("onLoadClass(" + bSuccess + ")");
		//trace("p_xmlLoadClass['response']: " + p_xmlLoadClass['response'])

		var oData = {};
		if (p_xmlLoadClass['response'].classe != undefined) {
			var xmlClass = p_xmlLoadClass['response'].classe;
			var oClass:Classe = new Classe(xmlClass._idClass);
			oClass.p_iSchool = Number(xmlClass._school);
			oClass.p_iTeacher = Number(xmlClass._teacher);
			oClass.p_sName = xmlClass._name;
			oClass.p_sPass = xmlClass._pass;
			oClass.p_iLevel = xmlClass._level;
	
			var aoSchools = p_xmlLoadClass['response'].schools.$school;
	
			oData = {oClass: oClass, aoSchools: aoSchools};
		}
		
		DoneAsyncRequest();
		this.dispatchEvent({type:"onLoadClass", data:oData});
	}


	private function onLoadSaveClass(bSuccess) {
		//trace("onLoadSaveClass(" + bSuccess + ")");
		DoneAsyncRequest();
		this.dispatchEvent({
			type:"onSaveClass", 
			data:{
				result: p_xmlSavePlayer["response"].result._value, 
				error:  p_xmlSavePlayer["response"].error.__text
			}
		});
	}


	private function onLoadSaveTeacher(bSuccess) {
		//trace("onTeacherSaveResult(" + bSuccess + ")");

		DoneAsyncRequest();
		this.dispatchEvent({type:"onSaveTeacher", data:{result: p_xmlSaveTeacher["response"].result._value, error: p_xmlSaveTeacher["response"].error.__text}});
			
	}
	
	private function onLoadLoginTeacher(bSuccess) {
		trace("onLoadLoginPlayer(" + bSuccess + ")");
		DoneAsyncRequest();
		this.dispatchEvent({
			type:"onLoginTeacher", 
			data:{
				result: p_xmlLoginTeacher["response"].result._value, 
				error:  p_xmlLoginTeacher["response"].error.__text
			}
		});
	}
	
}

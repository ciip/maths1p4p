﻿/**
 * Composant permettant de définir une zone invisible et dont l'activation 
 * fait sauter la cible à un repère.
 * 
 * @author Pierre Rossel
 * @version 1.0
 *
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

//import mx.core.UIComponent;


dynamic class maths1p4p.components.BtnGo extends MovieClip {
//class maths1p4p.components.BtnGo extends mx.core.UIComponent {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	var p_sLabel     :String;
	var p_sTarget    :String;
	var p_bPlay      :Boolean;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_btn :Button;

	private var __label:String = "";

	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function BtnGo() {
		super();
		//trace("BtnGo()");
	}

	
	public function onLoad() {
		//trace("BtnGo.onLoad");
		p_btn.onRelease = function () {
			//trace(this)
			_parent.activate();
		}
		
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	public function activate() {
		//trace("activate");
		
		//trace('p_sTarget: ' + p_sTarget)
		// Ne fonctionne qu'avec un chemin relatif au composant (pour un chemin absolu, utiliser eval())
		var mcTarget = this[p_sTarget];
		//trace('mcTarget: ' + mcTarget)
		//trace('typeof(mcTarget): ' + typeof(mcTarget))
		//trace('targetPath(mcTarget): ' + targetPath(mcTarget))
		//trace('targetPath(value(p_sTarget)): ' + targetPath(value(p_sTarget)))
		
		if (p_bPlay)
			mcTarget.gotoAndPlay(p_sLabel);
		else
			mcTarget.gotoAndStop(p_sLabel);
	}
}
﻿/**
 * class com.maths1p4p.level1.Level1
 * 
 * @author Fred FAUQUETTE
 * @version 1.0
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import mx.utils.Delegate;
import mx.controls.Alert;
import mx.controls.List;


import maths1p4p.application.Application;
import maths1p4p.application.AbstractStorage;
import maths1p4p.Teacher;
import maths1p4p.level1.Level1Player;

import maths1p4p.AbstractGame;
import maths1p4p.utils.SoundPlayer;
class maths1p4p.level1.Level1 extends maths1p4p.AbstractLevel {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var btnNbr:Button;
	public var btnFormes:Button;
	public var btnNext:Button;
	public var btnBack:Button;
	public var btnBackToInterface:Button;
	public var btnSound:Button;
	public var btnReload:Button;
	public var btnBackToLevel:Button;
	public var btnBackToLevelinInt:Button;
	public var btnResultats:Button;
	public var btnResultatsGames:Button;
	public var btnBegin:Button;
	
	
	
	
	public var btnHelp:MovieClip;
	public var mcNoSound:MovieClip;
	public var mcBtnGamesNumber:MovieClip;
	public var mcBtnGamesForms:MovieClip;
	public var mcListeClasse:MovieClip;
	public var mcQuitter:MovieClip;
	
	public var arrayBtnIntro:Array;
	public var arrayBtnInterface:Array;
	public var arrayBtnGameNbr:Array;
	public var arrayBtnGameFormes:Array;
	public var arrayBtnPlayer:Array;
	public var refActualGame:String;
	
	
	

	var lstPlayers:List;
	
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private static var nbrGameNbr:Number = 4;
	static private var p_oSoundPlayer:SoundPlayer = new SoundPlayer();
	private var p_oPlayer:Level1Player;
	private var p_oTeacher:Teacher;
	private var p_oStorage:AbstractStorage;
	private var p_xml;
	private var mcHelp:MovieClip;
	private var mcHelpResultat:MovieClip;
	private var mcResultats:MovieClip;
	private var mcResultatsDetail:MovieClip;
	private var listePlayersStats:Array;
	private var arrPlayers:Array;
	static private var gameTab:Array = [2,2,3,2,1,3,1,1,3,3,2,2,5,1,2,2,2,2,1,3,2];
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level1() {
		super();
		//
		//création interface et sons
		//
		_global.newPlayerList = false;
		var p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		AbstractGame.soundPlayerFromLevel.setGlobalVolume(40);
		p_oSoundPlayer.addASound("btnClick",p_sSoundPath+"MiniClick.mp3");
		p_oSoundPlayer.addASound("clickNom",p_sSoundPath+"nom.mp3");
		btnHelp = this.attachMovie("btnHelp","btnHelp",29);
		btnHelp._x = 600;
		btnHelp._y = 2;
		mcNoSound = this.attachMovie("mcNoSound","mcNoSound",28);
		mcNoSound._x = 579;
		mcNoSound._y = 127;
		// les tableaux avec les listes de btn
		arrayBtnIntro=[btnNbr,btnFormes,btnResultats];
		arrayBtnGameNbr = [btnNext,btnBack,mcBtnGamesNumber,btnResultatsGames];
		arrayBtnGameFormes = [btnNext,btnBack,mcBtnGamesForms,btnResultatsGames];
		arrayBtnInterface = [btnBackToInterface,btnSound,btnReload,btnBackToLevelinInt,btnHelp,mcNoSound];
		arrayBtnPlayer = [lstPlayers];
		// affichage de la liste des players
		afficheBtn(arrayBtnPlayer,true);
		// on vire l'affichage du btn quitter
		mcQuitter._visible=false;
		//création des clips d'aide vides et où on loadera les bons fichiers
		this.mcHelp = this.createEmptyMovieClip("mcHelp",30);
		this.mcHelpResultat = this.createEmptyMovieClip("mcHelpResultat",60);
		//bouton pour click d'abord sur ton nom
		btnBegin.onPress = function(){
			_parent.soundPlayer.playASound("clickNom",0);
		};
		//
		//création du mc Résultats
		//
		this.mcResultats = this.attachMovie("mcResultats","mcResultats",51);
		this.mcResultats.btnHelp.onPress = function(){
			_parent._parent.loadHelpResultat();
		};
		this.mcResultats.bg.onPress = function(){};
		this.mcResultats.bg.useHandCursor = false;
		this.mcResultats._visible = false;
		var mcNbr = this.mcResultats.createEmptyMovieClip("mcNbr",1);
		mcNbr._x = 98;
		mcNbr._y = 24;
		for(var i = 1;i<=13;i++){
			var mc = mcNbr.attachMovie("mcResultats_case","mcnbr"+i,mcNbr.getNextHighestDepth());
			mc.nbr.text = i;
			mc._x = 22*(i-1);
		}
		for(var i = 1;i<=8;i++){
			var mc = mcNbr.attachMovie("mcResultats_case","mcform"+i,mcNbr.getNextHighestDepth());
			mc.nbr.text = i;
			mc._x = 290+22*(i-1);
		}
		var mcNames = this.mcResultats.createEmptyMovieClip("mcNames",2);
		mcNames._x = 3;
		mcNames._y = 40;
		var mcRes = this.mcResultats.createEmptyMovieClip("mcRes",3);
		mcRes._x = 98;
		mcRes._y = 40;
		for(var i = 1;i<=29;i++){
			var mc = mcNames.attachMovie("mcResultats_nom","mc"+i,mcNames.getNextHighestDepth());
			mc.nbr.nbr.text = i;
			mc.nom.text = "";
			mc._y = 14*(i-1);
			var mcSubRes = mcRes.createEmptyMovieClip("mcSubRes"+i,mcRes.getNextHighestDepth());
			mcSubRes._y = mc._y;
			var t = 1;
			for(var j = 1;j<=13;j++){
				var mcs = mcSubRes.attachMovie("mcResultats_case_point","mc"+t,mcSubRes.getNextHighestDepth());
				mcs.point._visible = false;
				mcs._x = 22*(j-1);
				t++;
			}
			for(var j = 1;j<=8;j++){
				var mcs = mcSubRes.attachMovie("mcResultats_case_point","mc"+t,mcSubRes.getNextHighestDepth());
				mcs.point._visible = false;
				mcs._x = 290+22*(j-1);
				t++;
			}
		}
		this.mcResultats.btnClose.onPress = function(){
			_parent._parent.displayResult(false);
		}
		this.mcResultats.btnDetail.onPress = function(){
			_parent._parent.soundPlayer.playASound("btnClick",0);
			_parent._parent.displayResultDetail(true);
		}
		this.mcResultats.bt_Classe.onRelease = function () {
			_global.refreshResultDetail = false;
			_parent._parent.soundPlayer.playASound("btnClick",0);
			Application.editCurrentClass(this, Delegate.create(this, function(evt:Object){
				// Recharge la classe
				_parent._parent.onLoad();
			}));
		};
		this.mcResultats.btnPrint.onPress = function(){
			_parent._parent.createPrint("general");
		}
		//
		//création du mc RésultatsDetail
		//
		this.mcResultatsDetail = this.attachMovie("mcResultatsDetail","mcResultatsDetail",52);
		this.mcResultatsDetail.btnHelp.onPress = function(){
			_parent._parent.loadHelpResultat();
		};
		_global.refreshResultDetail = false;
		this.mcResultatsDetail.bt_Classe.onRelease = function () { 
			_global.refreshResultDetail = true;
			_parent._parent.soundPlayer.playASound("btnClick",0);
			Application.editCurrentClass(this, Delegate.create(this, function(evt:Object){
				// Recharge la classe
				_parent._parent.onLoad();
				
			}));
		};
		
		this.mcResultatsDetail.bg.onPress = function(){};
		this.mcResultatsDetail.bg.useHandCursor = false;
		this.mcResultatsDetail._visible = false;
		//mcResultatsDetail_nom
		var mcNames = this.mcResultatsDetail.createEmptyMovieClip("mcNames",1);
		for(var j=0;j<6;j++){
			var mcName = mcNames.createEmptyMovieClip("mcName"+j,j+1);
			mcName._x = 92+j*90;
			mcName._y = 5;
			mcName.attachMovie("mcResultatsDetail_nom","nom",1);
			var mcGames = mcName.createEmptyMovieClip("mcGames",2);
			mcGames._y = 20;
			for(var i = 1;i<=21;i++){
				var mc = mcGames.attachMovie("mcResultatsDetail_case","mc"+i,mcGames.getNextHighestDepth());
				mc.nbr.text = "";
				if(i<=13){
					mc._y = 18*(i-1);
				}else{
					mc._y = 5+18*(i-1);
				}
			}
		}
		var mcNamesGames = this.mcResultatsDetail.createEmptyMovieClip("mcNamesGames",2);
		mcNamesGames._y = 25;
		mcNamesGames._x = 2;
		for(var i = 1;i<=21;i++){
			var mc = mcNamesGames.attachMovie("mcResultatsDetail_case","mc"+i,mcNamesGames.getNextHighestDepth());
			
			if(i<=13){
				mc.nbr.text = "Nombres "+i;
				mc._y = 18*(i-1);
			}else{
				mc.nbr.text = "Formes "+(i-13);
				mc._y = 5+18*(i-1);
			}
		}
		this.mcResultatsDetail.btnPrint.onPress = function(){
			_parent._parent.createPrint("detail");
		}
		this.mcResultatsDetail.btnClose.onPress = function(){
			_parent._parent.soundPlayer.playASound("btnClick",0);
			_parent._parent.displayResultDetail(false);
		}
		this.mcResultatsDetail.btnBack.onRelease = function(){
			_parent._parent.soundPlayer.playASound("btnClick",0);
			_parent._parent.displayResultDetailBackAndFor(-1);
		}
		this.mcResultatsDetail.btnFor.onRelease = function(){
			_parent._parent.soundPlayer.playASound("btnClick",0);
			_parent._parent.displayResultDetailBackAndFor(1);
		}
		
		stop();	
	}
	
	public function initFrame() {
		//initialisation des btns et de leur fonctionnalité
		p_oSoundPlayer.setGlobalVolume(0);
		afficheBtn(arrayBtnGameNbr,false);
		afficheBtn(arrayBtnGameFormes,false);
		afficheBtn(arrayBtnInterface,false);
		afficheBtn(arrayBtnIntro,false);
		btnResultats._visible = true;
		btnResultats.enabled = true;
		
		p_oSoundPlayer.setGlobalVolume(40);
		btnNbr.onPress = function () {
			//_parent.soundPlayer.playASound("clickNom",0);
			_parent.soundPlayer.playASound("btnClick",0);
			_parent.afficheBtn(_parent.arrayBtnIntro,false);
			_parent.afficheBtn(_parent.arrayBtnGameNbr,true);
			_parent.afficheBtn(_parent.arrayBtnPlayer,false);
			_parent.gotoAndStop(2);
		}
		btnFormes.onPress = function () {
			_parent.soundPlayer.playASound("btnClick",0);
			//_parent.soundPlayer.playASound("clickNom",0);
			_parent.afficheBtn(_parent.arrayBtnIntro,false);
			_parent.afficheBtn(_parent.arrayBtnGameFormes,true);
			_parent.afficheBtn(_parent.arrayBtnPlayer,false);
			_parent.gotoAndStop(3);
		}
		btnResultatsGames.onPress = btnResultats.onPress = function () {
			_parent.displayResult(true);
		}
		
		for(var i=1;i<=21;i++){
			if(i<=13){
				var btn = this.mcBtnGamesNumber["btnGameNbr"+i];
			}else{
				var btn = this.mcBtnGamesForms["btnGameNbr"+i];
			}
			if(i<10){
				btn.ref = "0"+i;
			}else{
				btn.ref = i.toString();
			}
			
			btn.onPress = function () {
				_parent._parent.soundPlayer.playASound("btnClick",0);
				_parent._parent.afficheBtn(_parent._parent.arrayBtnGameFormes,false);
				_parent._parent.afficheBtn(_parent._parent.arrayBtnGameNbr,false);
				_parent._parent.afficheBtn(_parent._parent.arrayBtnPlayer,false);				
				_parent._parent.btnBackToLevel.enabled = false;
				_parent._parent.btnBackToLevel._visible = false;
				_parent._parent.myLoadGame(this.ref);
			}
		}
		btnBackToLevelinInt.onPress = btnBackToLevel.onPress = function(){
			_parent.unloadLevel();
		}
		btnBack.onPress = function () {
			_parent.soundPlayer.playASound("btnClick",0);
			_parent.afficheBtn(_parent.arrayBtnIntro,true);
			_parent.afficheBtn(_parent.arrayBtnGameFormes,false);
			_parent.afficheBtn(_parent.arrayBtnGameNbr,false);
			_parent.afficheBtn(_parent.arrayBtnPlayer,true);
			_parent.gotoAndStop(1);
		}
		btnNext.onPress = function () {
			_parent.soundPlayer.playASound("btnClick",0);
			if(_parent._currentframe == 2){			
				_parent.afficheBtn(_parent.arrayBtnGameNbr,false);
				_parent.afficheBtn(_parent.arrayBtnGameFormes,true);
				_parent.gotoAndStop(3);
			}else{				
				_parent.afficheBtn(_parent.arrayBtnGameFormes,false);
				_parent.afficheBtn(_parent.arrayBtnGameNbr,true);
				_parent.gotoAndStop(2);
			}
		}
		btnBackToInterface.onPress = function () {
			_parent.soundPlayer.playASound("btnClick",0);
			_parent.unloadGame();
			_parent.afficheBtn(_parent.arrayBtnInterface,false);
			_parent.btnBackToLevel.enabled = true;
			_parent.btnBackToLevel._visible = true;
			if(_parent._currentframe == 2){			
				_parent.afficheBtn(_parent.arrayBtnGameNbr,true);
			}else{				
				_parent.afficheBtn(_parent.arrayBtnGameFormes,true);
			}
		}
		btnSound.onPress = function () {
			_parent.soundPlayer.playASound("btnClick",0);
			var sPlayer = AbstractGame.soundPlayerFromLevel;
			if(	sPlayer.getGlobalVolume == 0){
				_parent.mcNoSound._visible = false;
				sPlayer.setGlobalVolume(40);
			}else{
				_parent.mcNoSound._visible = true;
				sPlayer.setGlobalVolume(0);
			}
		}
		btnReload.onPress = function () {
			_parent.soundPlayer.playASound("btnClick",0);
			_parent.loadGame("game1_"+_parent.refActualGame);
		}
		btnHelp.onPress = function(){
			this.gotoAndStop(1);
			_parent.loadHelp();
		}
		btnHelp.onRollOver = function(){
			this.gotoAndStop(2);
		}
		btnHelp.onRollOut = function(){
			this.gotoAndStop(1);
		}
	}
	public function loadHelp():Void{
		// affichage de l'aide des jeux
		Application.loadClip("data/level1/level1_help.swf",this.mcHelp,"debut");
	}
	public function loadHelpResultat():Void{
		//affichage de l'aide résultat
		Application.loadClip("data/level1/level1_helpResultat.swf",this.mcHelpResultat,"debut");
	}
	public function unloadLevel(){
		//décharger un jeu
		unload();
	}
	public function afficheBtn(tabRef:Array,val:Boolean):Void{
		// pour afficher une liste de btn
		for(var i in tabRef){
			//tabRef[i].enabled=val;
			tabRef[i]._visible=val;
		}
	}
	public function get soundPlayer():SoundPlayer{
		return p_oSoundPlayer;
	}

	public function onLoad() {
		//initialisation de l'interface du level
		initFrame();
		_global.myLevelOne = this;
		_global.resultsXMLActualPlayer = new XML();
		_global.resultsXMLActualPlayer.ignoreWhite = true;
		lstPlayers.addEventListener("change", Delegate.create(this, onPlayerChange));
		p_oStorage = Application.getInstance().getStorage();
		p_oStorage.addEventListener("onLoadPlayersList", this);
		p_oStorage.addEventListener("onLoadPlayer", this);
		p_oStorage.loadPlayersList(1, true);
		_global.newPlayerList = true;
	}
	public function saveGamePlayed(nbrGame:Number):String{
		var id = undefined;
		//pour retrouver l'id du joueur et la ligne où il faut écrire dans le tableau où j'ai sauvé les steps
		for(var i =1;i<=27;i++){
			if(this.mcResultats.mcRes["mcSubRes"+i].idPlayer == _global.idActualPlayer) id = i;
		}
		var val = undefined;
		if(id!=undefined){
			//pour savoir si le joueur a déjà joué ce jeu
			var testValAlreadyExist = false;
			for(var i in this.mcResultatsDetail.tabValues){
				if(Number(this.mcResultatsDetail.tabValues[i].id) == Number(_global.idActualPlayer)){
					if(this.mcResultatsDetail.tabValues[i][Number(refActualGame)]!=undefined && this.mcResultatsDetail.tabValues[i][Number(refActualGame)]!=""){
						testValAlreadyExist = true;
					}
				}
			}
			//si non alors on créé le noeud et on sauvegarde
			if(!testValAlreadyExist){
				var ELEMENT_NODE:Number = 1;
				var node1:XMLNode = new XMLNode(ELEMENT_NODE, "game");
				node1.attributes.num = nbrGame;
				var str = "";
				var nbG = gameTab[nbrGame-1]
				for(var i=0;i<nbG;i++){
					str+="-";
					if(i<(nbG-1)) str+=" ";
				}
				node1.attributes.step = str;
				node1.attributes.finished = 0;
				_global.resultsXMLActualPlayer.appendChild(node1);
				for(var i in this.mcResultatsDetail.tabValues){
					if(Number(this.mcResultatsDetail.tabValues[i].id) == Number(_global.idActualPlayer)){
						this.mcResultatsDetail.tabValues[i][Number(refActualGame)] = str;
					}
				}
				p_oPlayer.save();
			}
			
			// dans tous les cas, je récupère la valeur que je viens de mettre dans le noeud pour que le jeu la récupère et puisse la parser
			for (var aNode:XMLNode = _global.resultsXMLActualPlayer.firstChild; aNode != null; aNode = aNode.nextSibling) {
				 if(aNode.nodeName!=null){
					if(Number(aNode.attributes.num) == nbrGame){
						val = aNode.attributes.step;
					}
				 }
			}
			
		}
		return val;
	}
	public function retrieveNodeAndSave(val:String){
		
		
		// fonction appelée par chaque jeu pour sauvegarder les résultats
		//j'update le tableau de détails des résultats à la bonne place avec la nouvelle valeur
		for(var i in this.mcResultatsDetail.tabValues){
			if(Number(this.mcResultatsDetail.tabValues[i].id) == Number(_global.idActualPlayer)){
				this.mcResultatsDetail.tabValues[i][Number(refActualGame)] = val;
			}
		}
		//test pour savoir si le jeu est fini ou pas
		var myVal = val.split(" ");
		var teMyVal = 1;
		for(var i in myVal){
			if(Number(myVal[i]) != 0) teMyVal=0
		}
		// rendre le point visible dans l'affichage des résultats
		for(var i=1;i<=29;i++){
			var mc = this.mcResultats.mcRes["mcSubRes"+i];
			if(mc.idPlayer == Number(_global.idActualPlayer)){				
				mc["mc"+Number(refActualGame)].point._visible = teMyVal;
			}
		}
		//update du XML
		for (var aNode:XMLNode = _global.resultsXMLActualPlayer.firstChild; aNode != null; aNode = aNode.nextSibling) {
			 if(aNode.nodeName!=null){
				if(Number(aNode.attributes.num) == Number(refActualGame)){
					aNode.attributes.step = val;
					aNode.attributes.finished = teMyVal;
				}
			 }
		}
		//sauvegarde du joueur
		p_oPlayer.save();
	}
	public function myLoadGame(sGame:String){
		this.refActualGame = sGame;
		// génération de variables globales pour les récupérer dans le jeu
		// numéro du jeu
		_global.level1NbrGame = Number(this.refActualGame);
		//résultat du jeu
		_global.level1ResultGame = saveGamePlayed(Number(sGame));
		//chargement du jeu
		loadGame("game1_"+sGame);		
	}
	public function callBackLoadGame():Void{
		// quand le jeu est chargé on vire et on affiche les bons boutons
		afficheBtn(arrayBtnInterface,true);
		var sPlayer = AbstractGame.soundPlayerFromLevel;

		mcNoSound._visible = false;
		sPlayer.setGlobalVolume(40);

	}
	public function createPrint(val:String):Void{
		var my_pj:PrintJob = new PrintJob();
		var myPrint = this.createEmptyMovieClip("print",210);
		myPrint._x = -1000;
		var bg = myPrint.createEmptyMovieClip("bg",1);
		
		bg.beginFill(0xFFFFFF);
		bg.lineTo(870, 0);
		bg.lineTo(870, 559);
		bg.lineTo(0, 559);
		bg.lineTo(0, 0);
		bg.endFill();
		var myformat:TextFormat = new TextFormat();
		myformat.font = "Verdana";
		myformat.size = 10;
		var names = myPrint.createEmptyMovieClip("names",2);
		var wName = 70;
		var yName = 36;
		var h = 17;
		for(var j=0;j<this.mcResultatsDetail.tabValues.length;j++){
			var my_txt = names.createTextField("my_txt"+j, j+1, 1, yName+(h+1)*j, wName, h);
			my_txt.multiline = false;
			my_txt.wordWrap = false;
			my_txt.background = true;
			my_txt.backgroundColor = 0xCCCCCC;
			my_txt.text = this.mcResultatsDetail.tabValues[j].nom;
			my_txt.setTextFormat(myformat);
		}
		var nbr = myPrint.createEmptyMovieClip("nbr",3);
		var w = 17;
		if(val == "detail") w = 24;
		myformat.align = "center";
		
		for(var j=1;j<=21;j++){
			var my_txt = nbr.createTextField("my_txt"+j, j, (wName+2)+(w+1)*(j-1), yName-(h+1), w, h);
			my_txt.multiline = false;
			my_txt.wordWrap = false;
			my_txt.background = true;
			my_txt.backgroundColor = 0xCCCCCC;
			if(j<=13){
				my_txt.text = j;
			}else{
				my_txt.text = j-13;	
			}
			
			my_txt.setTextFormat(myformat);
		}
		var res = myPrint.createEmptyMovieClip("res",4);
		res._x = wName+2;
		res._y = yName;
		for(var j=0;j<this.mcResultatsDetail.tabValues.length;j++){
			var resSub = res.createEmptyMovieClip("res"+j,j+1);
			resSub._y = (h+1)*j;
			for(var i=1;i<=21;i++){
				var my_txt = resSub.createTextField("my_txt"+i, i, (w+1)*(i-1), 0, w, h);
				my_txt.multiline = false;
				my_txt.wordWrap = false;
				my_txt.background = true;
				my_txt.backgroundColor = 0xCCCCCC;
				if(val == "detail"){
					myformat.size = 6;
					if(i == 13) {
						myformat.size = 5;
					}
					
					var v = this.mcResultatsDetail.tabValues[j][i];
					if(v!=undefined){
						my_txt.text = v;
					}
				}else{
					var teMyVal = true;
					var v = this.mcResultatsDetail.tabValues[j][i];
					if(v!=undefined){
						var myVal = v.split(" ");				
						for(var k in myVal){
							if(Number(myVal[k]) != 0) teMyVal=false
						}
					}else{
						teMyVal=false
					}
					if(teMyVal) my_txt.text = "•";
				}
				my_txt.setTextFormat(myformat);
			}
		}
		myformat.size = 10;
		myformat.bold = true;
		var my_txt = myPrint.createTextField("nombres", 5, (wName+2), yName-2*(h+1), (w+1)*13-1, h);
		my_txt.multiline = false;
		my_txt.wordWrap = false;
		my_txt.background = true;
		my_txt.backgroundColor = 0xCCCCCC;
		my_txt.text = "Nombres";	
		my_txt.setTextFormat(myformat);

		var my_txt = myPrint.createTextField("formes", 6, (wName+2)+(w+1)*13, yName-2*(h+1), (w+1)*8-1, h);
		my_txt.multiline = false;
		my_txt.wordWrap = false;
		my_txt.background = true;
		my_txt.backgroundColor = 0xCCCCCC;
		my_txt.text = "Formes";
		my_txt.setTextFormat(myformat);
		if (my_pj.start()){
			//my_pj.orientation = "landscape";
			my_pj.addPage(myPrint, {xMin:0,xMax:870,yMin:0,yMax:559});
			my_pj.send();
		}
		delete my_pj;
		myPrint.removeMovieClip(); 
	}
	public function displayResult(val:Boolean):Void{
		// affichage des résultats
		this.soundPlayer.playASound("btnClick",0);
		var unval = !val;
		this.afficheBtn(_parent.arrayBtnIntro,unval);
		this.afficheBtn(_parent.arrayBtnPlayer,unval);
		this.btnBackToLevel.enabled = unval;
		this.btnBackToLevel._visible = unval;
		this.mcResultats._visible = val;
		if(!val){
			if(_global.newPlayerList){
				_global.newPlayerList = false;
				this.gotoAndStop(1);
				btnBegin._visible = true;
				afficheBtn(arrayBtnIntro,true);
				afficheBtn(arrayBtnGameFormes,false);
				afficheBtn(arrayBtnGameNbr,false);
				afficheBtn(arrayBtnPlayer,true);
			}
		}
	}
	public function displayResultDetailBackAndFor(val:Number):Void{
		// modification de l'affichage du résultat détaillé avec les boutons
		this.mcResultatsDetail.firstDisplay = this.mcResultatsDetail.firstDisplay+val*6;
		trace("***"+this.mcResultatsDetail.firstDisplay);
		if(this.mcResultatsDetail.firstDisplay > this.mcResultatsDetail.tabValues.length){
			this.mcResultatsDetail.firstDisplay = 0;
		}
		var t = this.mcResultatsDetail.tabValues.length;
		if(this.mcResultatsDetail.firstDisplay <0){	
			this.mcResultatsDetail.firstDisplay = Math.floor(t/6)*6;
		}
		trace(this.mcResultatsDetail.firstDisplay);
		var deb = this.mcResultatsDetail.firstDisplay;
		var r = Math.min(6,t-this.mcResultatsDetail.firstDisplay);
		for(var i=0;i<6;i++){
			var tab = this.mcResultatsDetail.tabValues[deb];
			var mc = this.mcResultatsDetail.mcNames["mcName"+i];
			trace(mc);
			if(i<r){
				mc.nom.nom.text = tab.nom;
			}else{
				mc.nom.nom.text = "";
			}
			for(var j=1;j<=21;j++){
				if(i<r){
					if(tab[j]!=undefined){
						mc.mcGames["mc"+j].nbr.text = tab[j];
					}else{
						mc.mcGames["mc"+j].nbr.text = "";
					}					
				}else{
					mc.mcGames["mc"+j].nbr.text = "";
				}
			}
			deb++;
		}
	}
	public function displayResultDetail(val:Boolean):Void{
		// affichage des résultats détaillés
		//this.soundPlayer.playASound("btnClick",0);
		var unval = !val;
		this.mcResultats._visible = unval;
		this.mcResultatsDetail._visible = val;
		if(val){
			this.mcResultatsDetail.firstDisplay = -6;
			displayResultDetailBackAndFor(1);
		}
	}
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	public function onLoadPlayersList (event:Object) {
		//trace("Level1.onLoadPlayersList " + event);
		
		this.arrPlayers = event.data.arrPlayersList;
		// un tableau pour les détails des résultats
		this.mcResultatsDetail.tabValues = new Array();
		if (arrPlayers != undefined) {
			lstPlayers.removeAll();
			// récupère les noms des classes dans le xml et les affiche dans la liste
			var iName = 1;
			// charge le tableau avec les bonnes valeurs
			for(var iM =1;iM<=27;iM++){
				this.mcResultats.mcNames["mc"+iM].nom.text = "";
				for(var jM=0;jM<22;jM++){
					this.mcResultats.mcRes["mcSubRes"+iM]["mc"+jM].point._visible = false;
				}
			}
			for(var iPlayer in arrPlayers){
				var myTab = new Array();
				var xmlPlayer = arrPlayers[iPlayer];
				//trace("in loop for classes");
				lstPlayers.addItem({label: xmlPlayer._name, data: xmlPlayer._idPlayer});
				var resultsPlayer = new XML();				
				resultsPlayer = xmlPlayer._results;
				resultsPlayer.ignoreWhite = true;
				this.mcResultats.mcRes["mcSubRes"+iName].idPlayer = Number(xmlPlayer._idPlayer);
				for (var aNode:XMLNode = resultsPlayer.firstChild; aNode != null; aNode = aNode.nextSibling) {
					 if(aNode.nodeName!=null){
						 // insertion du step dans le tableau pour affichage détaillé
						myTab[Number(aNode.attributes.num)] = aNode.attributes.step;
						// affichage du point dans les résultats en cas de réussite
						if( Number(aNode.attributes.finished)==1){
							this.mcResultats.mcRes["mcSubRes"+iName]["mc"+aNode.attributes.num].point._visible = true;
						}else{
							this.mcResultats.mcRes["mcSubRes"+iName]["mc"+aNode.attributes.num].point._visible = false;
						}
					}
				}				
				this.mcResultats.mcNames["mc"+iName].nom.text = xmlPlayer._name;
				myTab.nom = xmlPlayer._name;
				myTab.id = xmlPlayer._idPlayer;
				this.mcResultatsDetail.tabValues.push(myTab);
				iName++;
			}
			// démarre l'appli en préselectionnant le joueur en début de liste
			if (lstPlayers.length) {
				//p_oStorage.loadTeacher(arrPlayers[0]._idTeacher);
				//Application.getInstance().setCurrentClass(arrPlayers[0]._idClass,arrPlayers[0]._idTeacher);				
				//lstPlayers.selectedIndex = 0;
				//onPlayerChange();
			}
			
			if(_global.refreshResultDetail) displayResultDetail(true);
		} else {
			lstPlayers.addItem({label: "Erreur"});
		}
	}

	private function onLoadPlayer (event:Object) {
		
		p_oPlayer = event.data.oPlayer;
		//trace(">>>>>>onLoadPlayer :"+p_oPlayer.getName());
		var resultsPlayer = p_oPlayer.getResults();
		resultsPlayer.ignoreWhite = true;
		
		//trace(resultsPlayer);
		
		//POUR CLEANER LES SCORES
		/*
		var testP = false;
		var tab = new Array();
		var bNode:XMLNode = resultsPlayer.firstChild;
		if(bNode.attributes.step == undefined) testP=true;
		if(testP){
			for (var aNode:XMLNode = resultsPlayer.firstChild; aNode != null; aNode = aNode.nextSibling) {
				if(aNode.nodeName!=null){
					tab.push([aNode.attributes.num,aNode.attributes.finished,aNode]);           
				}
			}
			for(var i in tab){
				var ELEMENT_NODE:Number = 1;
				var node1:XMLNode = new XMLNode(ELEMENT_NODE, "game");
				var myVal = tab[i][1].split(" ");
				var teMyVal = 1;
				for(var j in myVal){
					if(Number(myVal[j]) != 0) teMyVal=0
				}
				node1.attributes.finished = teMyVal;
				node1.attributes.step = tab[i][1];
				node1.attributes.num = tab[i][0];
				resultsPlayer.appendChild(node1);
				tab[i][2].removeNode();
			}
			//trace(">>>>>>\n"+resultsPlayer);
			p_oPlayer.save();
			trace("modif");
		}
		*/
		/*
		var tab = new Array();
		for (var aNode:XMLNode = resultsPlayer.firstChild; aNode != null; aNode = aNode.nextSibling) {
			if(aNode.nodeName!=null){
				tab.push([aNode.attributes.num,aNode.attributes.finished,aNode]);           
			}
		}
		for(var i in tab){
			tab[i][2].removeNode();
		}
		//trace(">>>>>>\n"+resultsPlayer);
		p_oPlayer.save();
		*/
		_global.resultsXMLActualPlayer = p_oPlayer.getResults();
		_global.idActualPlayer = Number(p_oPlayer.getId());
		
		_global.nameOfThePlayerLevel1 = p_oPlayer.getName();
		afficheBtn(arrayBtnIntro,true);
		
	}

	private function onPlayerChange(event:Object) {
		//trace("onPlayerChange");
		btnBegin._visible = false;
		p_oStorage.loadPlayer(lstPlayers.selectedItem.data);
	}
}
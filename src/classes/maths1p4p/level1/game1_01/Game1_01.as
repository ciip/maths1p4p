﻿/**
 * 
 * class com.maths1p4p.level1.game1_01.Game1_01
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 *
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.level1.game1_01.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application

class maths1p4p.level1.game1_01.Game1_01 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	

	
	
	
	public var p_txtStart:TextField;
	public var p_txtName:TextField;	
	
	
	public var p_btnSun:Button;
	public var p_btnNight:Button;
	public var p_btnStart:Button;
	public var p_btnPlane:Button;
	
	
	public var p_isPlaying:Boolean;
	
	public var p_mcResults:level1_resultat;
	
	public var p_mcSun:MovieClip;
	public var p_mcNight:MovieClip;
	public var p_mcCurtain:MovieClip;
	public var p_mcBulle:MovieClip;
	public var p_mcOmbre:MovieClip;
	public var p_mcCatOver:MovieClip;
	public var p_mcPlane:MovieClip;
	public var p_mcBtnNumbers:MovieClip;
	public var p_mcCoches:MovieClip;
	
	public var nbrPassage:Number;
	public var actualTry:Number;
	public var actualTryExo:Number;
	public var selectedGame:String;
	public var nameGames:Array = ["sun","night"];
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var nbrPassageAct:Number;
	private var nbrTry:Number;
	private var myInterval:Number;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_01()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
	}
	
	

	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){
		p_isPlaying = false;
		
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){
		actualTry = 0;
		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		p_txtStart.text = "Départ";
		
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		
		
		nbrTry = 5;
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		
		p_mcBtnNumbers = this.createEmptyMovieClip("p_mcBtnNumbers",2);
		
		for(var i = 0;i<=9;i++){
			
			var res = p_mcBtnNumbers.attachMovie("btnTransparent","b_"+i,i+1);
			res._width = 183;
			res._height = 23;
			res._x = 348;
			res._y = 156 + i*24;
			res.nbr = i+1;
			res.onPress = function(){
				if(_parent._parent.p_isPlaying){
					_parent._parent.soundPlayer.playASound("btnClick",0);
					if(_parent._parent.nbrPassage == this.nbr){
						_parent._parent.p_isPlaying = false;
						_parent._parent.makeBtnNumberWithArrow(false);
						_parent._parent.p_txtStart.textColor = 0x000000;
						if(!_parent._parent.p_mcResults.modifyGoodResult()){
							_parent._parent.p_btnSun.enabled = false;
							_parent._parent.p_btnNight.enabled = false;
						}
					}else{
						_parent._parent.p_mcResults.modifyFalseResult();
					}
				}
			}
			
		}
		makeBtnNumberWithArrow(false);
		
		p_btnStart.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.p_mcBulle._visible = false;
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcResults.checkResultAndReset();
				_parent.p_txtStart.text = "Suite";
				_parent.p_txtStart.textColor = 0xCCCCCC;
				_parent.p_isPlaying = true;
				_parent.p_mcCurtain.gotoAndStop(2);
				_parent.makePlaneMove();
				_parent.p_btnSun.enabled = false;
				_parent.p_btnNight.enabled = false;
				if(_parent.selectedGame == "sun"){
					_parent.p_mcOmbre._visible = false;
				}else{
					_parent.p_mcOmbre._visible = true;
				}
			}
		}
		p_btnPlane.onPress = function(){
			if(_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.beginPlaneMove();
			}
		}
		p_btnSun.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOmbre._visible = false;
				_parent.selectedGame = "sun";
				_parent.p_mcNight._visible= false;
				_parent.p_mcSun._visible= true;
			}
		}
		p_btnNight.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOmbre._visible = true;
				_parent.p_mcNight._visible= true;
				_parent.p_mcSun._visible= false;
				_parent.selectedGame = "night";
			}
		}
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			p_mcSun._visible= false;
			selectedGame = "night";
		}else{
			p_mcNight._visible= false;
			p_mcOmbre._visible = false;
			selectedGame = "sun";
		}
		p_mcBulle._visible = false;
		p_mcCatOver._visible = false;
		p_mcPlane._visible = false;
	}
	public function switchExercice(val:Boolean):Void{
		p_isPlaying = false;
		p_btnSun.enabled = true;
		p_btnNight.enabled = true;
		p_mcBulle._visible = true;
		if(selectedGame == "sun"){
			if(!val){
				this.p_mcCoches.mc1._visible = true;
			}
			selectedGame="night";
			p_mcNight._visible= true;
			p_mcSun._visible= false;
		}else{
			if(!val){
				this.p_mcCoches.mc2._visible = true;
			}
			selectedGame="sun";
			p_mcNight._visible= false;
			p_mcSun._visible= true;
		}
		p_txtStart.text = "Départ";
	}
	public function makeBtnNumberWithArrow (val:Boolean):Void{
		for(var i in p_mcBtnNumbers){
			var res = p_mcBtnNumbers[i];
			res.enabled = val;			
		}
	}
	public function makePlaneMove(){
		this.nbrPassage = random(10)+1;
		this.actualTryExo = 1;
		this.beginPlaneMove();
	}
	private function beginPlaneMove(){
		this.nbrPassageAct = 1;
		p_mcCurtain.gotoAndStop(2);
		this.p_mcPlane._visible = true;
		if(this.myInterval != undefined){
			clearInterval(this.myInterval);
		}
		this.myInterval = setInterval(this,"affichePlane",1500,true);
		if(this.selectedGame == "sun"){
			this.p_mcPlane.play();
		}
		soundPlayer.playASound("avion");
		trace(this.nbrPassage);
	}
	private function affichePlane(val:Boolean){
		if(this.nbrPassageAct == this.nbrPassage){
			clearInterval(this.myInterval);
			makeBtnNumberWithArrow(true);
			p_mcCurtain.gotoAndStop(1);
		}else{
			this.nbrPassageAct++;
			if(this.selectedGame == "sun"){
				this.p_mcPlane.play();
			}
			soundPlayer.playASound("avion",0);
		}
	}
}

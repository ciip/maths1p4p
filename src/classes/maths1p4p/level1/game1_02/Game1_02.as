﻿/**
 * 
 * class com.maths1p4p.level1.game1_02.Game1_02
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.level1.game1_02.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_02.Game1_02 extends maths1p4p.AbstractGame{
	
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_txtStart:TextField;
	public var p_txtName:TextField;	
	
	
	public var p_btnOreille:Button;
	public var p_btnOeil:Button;
	public var p_btnStart:Button;
	
	
	public var p_isPlaying:Boolean;
	
	
	
	public var p_mcOreille:MovieClip;
	public var p_mcOeil:MovieClip;
	public var p_mcOreilleRef:MovieClip;
	public var p_mcOeilRef:MovieClip;

	public var p_mcBulle:MovieClip;
	public var p_mcCatOver:MovieClip;

	public var p_mcBtnNumbers:MovieClip;
	public var p_mcVert:MovieClip;
	public var p_mcScore:MovieClip;
	public var p_mcCoches:MovieClip;
	
	public var p_mcResults:level1_resultat;
	
	public var nbrChoosed:Number;
	public var actualTry:Number;
	public var actualTryExo:Number;
	public var selectedGame:Boolean;
	public var nameGames:Array = [true,false];
	public var replayEnabled:Boolean;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var nbrChoosedAct:Number;
	private var myInterval:Number;
	private var nbrTry:Number;
	private var actAff:Number;
	
	
	
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_02()	{		
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
	}
	
	

	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){
		actualTry = 0;
		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		p_txtStart.text = "Départ";

		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai1";
		
		
		nbrTry = 5;
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		
		p_mcBtnNumbers = this.createEmptyMovieClip("p_mcBtnNumbers",2);		
		for(var i = 0;i<=9;i++){
			
			var res = p_mcBtnNumbers.attachMovie("btnCercle","b_"+i,i+1);
			//res._alpha = 0;
			res._y = 405;
			res._x = 130 + i*40;
			res.texte.text = i+1;
			res.nbr = i+1;
			res.onPress = function(){
				if(_parent._parent.p_isPlaying){
					_parent._parent.soundPlayer.playASound("btnClick",0);
					if(_parent._parent.nbrChoosed == this.nbr){
						_parent._parent.p_isPlaying = false;
						_parent._parent.makeBtnNumberWithArrow(false);
						_parent._parent.p_mcResults.modifyGoodResult();
					}else{
						_parent._parent.p_mcResults.modifyFalseResult();
					}
				}
			}
			
		}
		for(var i = 0;i<=8;i++){
			
			var res = p_mcVert.attachMovie("mcTexteVert","b_"+i,i+1);
			res._y = 9;
			res._x = 4 + i*25;
		}
		p_mcVert.attachMovie("mcVertOver","mcVertOver",p_mcVert.getNextHighestDepth());
		
		makeBtnNumberWithArrow(false);
		
		p_btnStart.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcResults.checkResultAndReset();
				if(_parent.actualTry==0){
					_parent.p_btnOreille.enabled = false;
					_parent.p_btnOeil.enabled = false;
					for(var i = 0;i<=8;i++){
						_parent.p_mcVert["b_"+i].texte.text = "";
					}
					_parent.p_mcScore.texte.text = "";
					if(!_parent.selectedGame){
						_parent.p_mcVert.mcVertOver._visible=false;
						_parent.p_mcBtnNumbers._visible=false;
						_parent.p_mcScore._visible = true;
						_parent.p_mcOeil._visible= true;
						_parent.p_mcOreille._visible= false;
						_parent.p_mcOeilRef._visible= false;
						_parent.p_mcOreilleRef._visible= true;
					}else{
						_parent.p_mcVert.mcVertOver._visible=true;
						_parent.p_mcBtnNumbers._visible=true;
						_parent.p_mcScore._visible = false;
						_parent.p_mcOeil._visible= false;
						_parent.p_mcOreille._visible= true;
						_parent.p_mcOeilRef._visible= true;
						_parent.p_mcOreilleRef._visible= false;
					}
				}
				_parent.p_mcBulle._visible = false;
				_parent.p_txtStart.text = "Suite";
				_parent.p_isPlaying = true;
				_parent.makeSoundCalc();
			}else{
				if(_parent.selectedGame && _parent.replayEnabled){
					_parent.replay();
				}
			}
		}
		p_mcScore.btnScore.onPress = function(){
			if(_parent._parent.p_isPlaying){
				_parent._parent.soundPlayer.playASound("btnClick",0);
					if(_parent._parent.nbrChoosed == Number(_parent.texte.text)){
						_parent._parent.p_isPlaying = false;
						for(var i = 0;i<=8;i++){
							_parent._parent.p_mcVert["b_"+i].texte.text = "";
						}
						_parent.texte.text = "";
						_parent.texte.selectable = false;
						_parent._parent.makeBtnNumberWithArrow(false);
						_parent._parent.p_mcResults.modifyGoodResult();
					}else{
						_parent._parent.p_mcResults.modifyFalseResult();
					}
			}
		}
		var keyListener:Object = new Object();
		keyListener.ref = this;
		keyListener.onKeyDown = function() {
			var re = this;
			if(!re.ref.selectedGame){
				if (Key.getCode() == Key.ENTER || Key.getCode() == Key.SPACE) {
					if(re.ref.p_isPlaying){
						re.ref.soundPlayer.playASound("btnClick",0);
							if(re.ref.nbrChoosed == Number(re.ref.p_mcScore.texte.text)){
								re.ref.p_isPlaying = false;
								for(var i = 0;i<=8;i++){
									re.ref.p_mcVert["b_"+i].texte.text = "";
								}
								re.ref.p_mcScore.texte.text = "";
								re.ref.p_mcScore.texte.selectable = false;
								re.ref.makeBtnNumberWithArrow(false);
								re.ref.p_mcResults.modifyGoodResult();
							}else{
								re.ref.p_mcResults.modifyFalseResult();
							}
					}
				} 
			}
		};		
		Key.addListener(keyListener);

		p_mcScore.texte.onChanged = function(textfield_txt:TextField) {
			if(Number(textfield_txt.text)>10 || Number(textfield_txt.text)<1){
				var st = textfield_txt.text;
				textfield_txt.text = st.substring(st.length-1,st.length);
			}
			trace("the value of "+textfield_txt._name+" was changed. New value is: "+textfield_txt.text);
		};

		p_btnOreille.onPress = function(){
			if(!_parent.p_isPlaying && _parent.actualTry==0){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOeil._visible= false;
				_parent.p_mcOreille._visible= true;
				_parent.p_mcOeilRef._visible= true;
				_parent.p_mcOreilleRef._visible= false;
				_parent.selectedGame=true;
			}
		}
		p_btnOeil.onPress = function(){
			if(!_parent.p_isPlaying && _parent.actualTry==0){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOeil._visible= true;
				_parent.p_mcOreille._visible= false;
				_parent.p_mcOeilRef._visible= false;
				_parent.p_mcOreilleRef._visible= true;
				_parent.selectedGame=false;
			}
		}
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			p_mcOeil._visible= true;
			p_mcOreille._visible= false;
			p_mcOeilRef._visible= false;
			p_mcOreilleRef._visible= true;
			selectedGame=false;
		}else{			
			p_mcOeil._visible= false;
			p_mcOreille._visible= true;
			p_mcOeilRef._visible= true;
			p_mcOreilleRef._visible= false;
			selectedGame=true;
		}

		p_mcBulle._visible = false;
		p_mcCatOver._visible = false;
		p_mcScore._visible = false;
	}
	public function switchExercice(val:Boolean):Void{
		p_btnOreille.enabled = true;
		p_btnOeil.enabled = true;
		this.p_mcBulle._visible = true;
		if(selectedGame){
			if(!val){
				this.p_mcCoches.mc1._visible = true;
			}
			selectedGame = false;
			p_mcVert.mcVertOver._visible=false;
			p_mcBtnNumbers._visible=false;
			p_mcScore._visible = true;
			p_mcOeil._visible= true;
			p_mcOreille._visible= false;
			p_mcOeilRef._visible= false;
			p_mcOreilleRef._visible= true;
		}else{
			if(!val){
				this.p_mcCoches.mc2._visible = true;
			}
			selectedGame = true;
			p_mcVert.mcVertOver._visible=true;
			p_mcBtnNumbers._visible=true;
			p_mcScore._visible = false;
			p_mcOeil._visible= false;
			p_mcOreille._visible= true;
			p_mcOeilRef._visible= true;
			p_mcOreilleRef._visible= false;
		}
		p_txtStart.text = "Départ";
	}
	public function focus(texteFocus):Void{
		clearInterval(this.myInterval);
		Selection.setFocus(texteFocus);
	}
	public function makeBtnNumberWithArrow (val:Boolean):Void{
		for(var i in p_mcBtnNumbers){
			var res = p_mcBtnNumbers[i];
			res.enabled = val;			
		}
	}
	public function makeSoundCalc(){
		replayEnabled = false;
		this.nbrChoosed = random(10)+1;
		this.nbrChoosedAct = 1;
		this.actualTryExo = 1;
		if(!this.selectedGame){
			this.actAff = 0;
			for(var i = 0;i<=8;i++){
				p_mcVert["b_"+i].texte.text = "";
			}
		}
		this.p_mcScore.texte.selectable = false;
		this.myInterval = setInterval(this,"playSoundCalc",500);
	}
	public function replay(){
		replayEnabled = false;
		this.nbrChoosedAct = 1;
		this.actualTryExo = 1;
		this.myInterval = setInterval(this,"playSoundCalc",500);
	}
	private function playSoundCalc(val:Boolean){
		if(this.nbrChoosedAct > 10){
			replayEnabled = true;
			clearInterval(this.myInterval);
			makeBtnNumberWithArrow(true);
			trace("************");
			this.p_mcScore.texte.selectable = true;
			if(!this.selectedGame){
				this.myInterval = setInterval(this,"focus", 100, p_mcScore.texte);
			}
		}else{
			if(this.selectedGame){
				if(this.nbrChoosedAct != this.nbrChoosed){
					soundPlayer.playASound(this.nbrChoosedAct+"_Number",0);
					trace(this.nbrChoosedAct);
				}
			}else{
				if(this.nbrChoosedAct != this.nbrChoosed){
					p_mcVert["b_"+this.actAff].texte.text = this.nbrChoosedAct;
					this.actAff++;
				}
			}
			this.nbrChoosedAct++;
		}
	}
}

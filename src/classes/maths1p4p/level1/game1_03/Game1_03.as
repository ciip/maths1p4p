﻿/**
 * 
 * class com.maths1p4p.level1.game1_03.Game1_03
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.level1.game1_03.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_03.Game1_03 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	
	public var p_txtStart:TextField;
	public var p_txtName:TextField;
	public var p_txtSelectedNbr:TextField;	
	
	
	
	public var p_btnEqual:Button;
	public var p_btnLess:Button;
	public var p_btnMore:Button;
	
	public var p_btnStart:Button;
	
	
	public var p_isPlaying:Boolean;	
	
	public var p_mcEqual:MovieClip;
	public var p_mcLess:MovieClip;
	public var p_mcMore:MovieClip;	
	public var p_mcBulle:MovieClip;	
	public var p_mcBulleFruit:MovieClip;
	public var p_mcCoches:MovieClip;
	
	public var p_mcResults:level1_resultat;
	
	public var p_mcBulleBleues:MovieClip;
	public var p_mcCroix1:MovieClip;
	public var p_mcCroix2:MovieClip;
	public var p_mcCroix3:MovieClip;
	public var p_mcCroix4:MovieClip;
	public var p_mcCroix5:MovieClip;
	public var p_mcCroix6:MovieClip;
	public var p_mcFruits:MovieClip;
	public var p_mcNumber:MovieClip;
	public var p_btnNumber:MovieClip;
	public var p_btnFruits:MovieClip;
	
	
	
	public var tabNbr:Array;
	
	
	public var nbrSelecte:Number;
	public var actualTry:Number;
	public var actualTryExo:Number;
	public var selectedGame:String;
	public var nameGames:Array = ["equal","less","more"];
	public var withFruit:Boolean;
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var nbrTry:Number;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_03()	{		
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";	
	}
	
	

	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
		trace("vazy1");		
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 0;
		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		p_mcCoches.mc3._visible = false;
		withFruit = false;
		p_txtStart.text = "Départ";
		
		
		
		
		this.tabNbr = new Array();
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai1";
		
		
		nbrTry = 4;
		p_mcResults = new level1_resultat(this,this.nbrTry);

		
		
		p_mcBulleBleues = this.createEmptyMovieClip("p_mcBulleBleues",2);		
		for(var i = 0;i<=5;i++){		
			var res = p_mcBulleBleues.createEmptyMovieClip("p_mcBulleBleue_"+i,i+1);
			res._x = 28+(i-Math.floor(i/3)*3)*170;
			res._y = 173+Math.floor(i/3)*130;
		}
		
		for(var i = 0;i<=5;i++){
			this["p_mcCroix"+i].selecte = false;
			this["p_mcCroix"+i].croix.gotoAndStop(1);
			this["p_mcCroix"+i].onPress = function(){
				if(_parent.p_isPlaying){
					_parent.soundPlayer.playASound("btnClick",0);
					if(this.selecte){
						this.croix.gotoAndStop(1);
						this.selecte = false;
					}else{
						this.croix.gotoAndStop(2);
						this.selecte = true;
					}
				}
			}
		}
		makeCroixVisibility(false);
		
		p_btnStart.onPress = function(){
			_parent.p_mcBulle._visible = false;
			_parent.soundPlayer.playASound("btnClick",0);
			if(_parent.p_isPlaying){
				trace("test rsultat");
				var resAttendu = 0;
				for(var i = 0;i<=5;i++){
					var te = _parent.tabNbr[i];
					if(_parent.selectedGame=="equal"){
						if(te==_parent.nbrSelecte) resAttendu++
					}else if(_parent.selectedGame=="less"){
						if(te<_parent.nbrSelecte) resAttendu++
					}else{
						if(te>_parent.nbrSelecte) resAttendu++
					}
				}
				var resAct = 0;
				var resActError = false;
				for(var i = 0;i<=5;i++){
					if(_parent["p_mcCroix"+i].selecte){
						var te = _parent.tabNbr[i];
						trace("***");
						var resFinal = false;
						if(_parent.selectedGame=="equal"){
							if(te==_parent.nbrSelecte) resFinal=true
						}else if(_parent.selectedGame=="less"){
							if(te<_parent.nbrSelecte) resFinal=true
						}else{
							if(te>_parent.nbrSelecte) resFinal=true
						}
						if(resFinal) resAct++
						else resActError=true
					}
				}
				trace(resAttendu+":"+resAct);
				if(resAct==resAttendu && !resActError){
					_parent.p_mcResults.modifyGoodResult();
					for(var i = 0;i<=5;i++){			
						_parent["p_mcCroix"+i].enabled = false;	
					}	
					_parent.p_isPlaying = false;
					if(_parent.actualTry==4){
						_parent.p_txtStart.text = "Départ";
					}else{
						_parent.p_txtStart.text = "Suite";
					}
				}else{
					_parent.p_mcResults.modifyFalseResult();
				}
			}else{
				_parent.p_mcResults.checkResultAndReset();
				_parent.makeBulleBleues();
				_parent.p_btnEqual.enabled = false;
				_parent.p_btnLess.enabled = false;
				_parent.p_btnMore.enabled = false;
			}
		}
		p_btnEqual.onPress = function(){
			if(!_parent.p_isPlaying && _parent.actualTry==0){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.selectedGame = "equal";
				_parent.p_mcLess._visible= false;
				_parent.p_mcMore._visible= false;
				_parent.p_mcEqual._visible= true;
			}
		}
		p_btnLess.onPress = function(){
			if(!_parent.p_isPlaying && _parent.actualTry==0){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.selectedGame = "less";
				_parent.p_mcLess._visible= true;
				_parent.p_mcMore._visible= false;
				_parent.p_mcEqual._visible= false;
			}
		}
		p_btnMore.onPress = function(){
			if(!_parent.p_isPlaying && _parent.actualTry==0){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.selectedGame = "more";
				_parent.p_mcLess._visible= false;
				_parent.p_mcMore._visible= true;
				_parent.p_mcEqual._visible= false;
			}
		}
		
		p_btnFruits.onPress = function(){
			_parent.soundPlayer.playASound("btnClick",0);
			_parent.withFruit = true;
			_parent.p_mcFruits._visible= true;
			_parent.p_mcNumber._visible= false;
			_parent.p_mcBulleFruit._visible= true;
		}
		p_btnNumber.onPress = function(){
			_parent.soundPlayer.playASound("btnClick",0);
			_parent.withFruit = false;
			_parent.p_mcFruits._visible= false;
			_parent.p_mcNumber._visible= true;
			_parent.p_mcBulleFruit._visible= false;
		}
		for(var i=0;i<10;i++){
			var mc = p_mcBulleFruit.attachMovie("mcFruit","mc"+i,i+1);
			mc._x = 53+(i-Math.floor(i/5)*5)*35;
			mc._y = 20+Math.floor(i/5)*35;
			mc._visible = false;
		}
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			selectedGame = "less";
			p_mcLess._visible= true;
			p_mcMore._visible= false;
			p_mcEqual._visible= false;
		}else if(nbr == 1){
			selectedGame = "more";
			p_mcLess._visible= false;
			p_mcMore._visible= true;
			p_mcEqual._visible= false;
		}else{			
			selectedGame = "equal";
			p_mcLess._visible= false;
			p_mcMore._visible= false;
			p_mcEqual._visible= true;
		}
		p_mcBulle._visible = false;
		p_mcBulleFruit._visible = false;
		p_mcFruits._visible= false;
		p_mcNumber._visible= true;
	}
	
	public function switchExercice(val:Boolean):Void{
		p_btnEqual.enabled = true;
		p_btnLess.enabled = true;
		p_btnMore.enabled = true;
		actualTry==0;
		this.p_mcBulle._visible = true;
		if(this.selectedGame=="equal"){
			p_mcLess._visible= true;
			p_mcMore._visible= false;
			p_mcEqual._visible= false;
			this.selectedGame = "less";
			if(!val){
				this.p_mcCoches.mc1._visible = true;
			}
		}else if(this.selectedGame=="less"){
			p_mcLess._visible= false;
			p_mcMore._visible= true;
			p_mcEqual._visible= false;
			this.selectedGame = "more";
			if(!val){
				this.p_mcCoches.mc2._visible = true;
			}
		}else{
			p_mcLess._visible= false;
			p_mcMore._visible= false;
			p_mcEqual._visible= true;
			this.selectedGame = "equal";
			if(!val){
				this.p_mcCoches.mc3._visible = true;
			}
		}
	}
	public function makeCroixVisibility (val:Boolean):Void{
		for(var i = 0;i<=5;i++){			
			var res = this["p_mcCroix"+i];
			res.croix.gotoAndStop(1);
			res.selecte = false;
			res.enabled = val;	
		}		
		p_isPlaying = val;
	}
	public function makeBulleBleues(){
		trace("oki");
		this.nbrSelecte = random(8)+3;
		trace(this.nbrSelecte);
		p_txtSelectedNbr.text = this.nbrSelecte.toString();
		var t = random(3)+1;
		for(var i=0;i<10;i++){
			var mc = p_mcBulleFruit["mc"+i];
			mc.gotoAndStop(t);
			if(i<this.nbrSelecte){
				mc._visible = true
			}else{
				mc._visible = false
			}
		}
		if(!withFruit){			
			p_mcBulleFruit._visible = false;
		}else{
			p_mcBulleFruit._visible = true;
			
		}
		for(var i = 0;i<=5;i++){
			var test = Math.max(1,this.nbrSelecte-random(5)+2);
			this.tabNbr[i]=test;
		}
		for(var i=0;i<=1;i++){
			if(this.selectedGame=="equal"){
				this.tabNbr[random(6)]=this.nbrSelecte;
			}else if(this.selectedGame=="less"){
				this.tabNbr[random(6)]=this.nbrSelecte-1-random(2);
			}else{
				this.tabNbr[random(6)]=this.nbrSelecte+1+random(2);
			}
		}
		
		for(var i = 0;i<=5;i++){
			var res = p_mcBulleBleues["p_mcBulleBleue_"+i];
			var test = this.tabNbr[i];
			for(var j in res){
				trace(typeof(res[j]));
				if(typeof(res[j]) == "movieclip") res[j].removeMovieClip();
			}
			for(var j=0;j<test;j++){
				var mcB = res.attachMovie("mcBulleBleue","mc_"+j,j+1);
				var tx = j-Math.floor(j/4)*4;
				var xajout = tx-4;
				if(tx<2) xajout=tx
				
				mcB._x = 80+xajout*30+(random(15)-7);
				var yajout = Math.floor(j/4);
				mcB._y = 15+yajout*30+(random(15)-7);
				trace(j+":"+xajout+":"+yajout);
			}
		}
		this.actualTryExo = 1;
		makeCroixVisibility(true);
		p_txtStart.text = "J'ai fini";
	}
}

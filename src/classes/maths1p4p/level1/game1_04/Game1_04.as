﻿/**
 * 
 * class com.maths1p4p.level1.game1_04.Game1_04
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.level1.game1_04.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_04.Game1_04 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	
	public var p_txtStart:TextField;
	public var p_txtName:TextField;	
	
	
	public var p_btnChampi:Button;
	public var p_btnBouteille:Button;
	public var p_btnStart:Button;
	
	
	public var p_isPlaying:Boolean;
	
	
	
	public var p_mcChampi:MovieClip;
	public var p_mcBouteille:MovieClip;
	public var p_mcChampiGame:MovieClip;
	public var p_mcBouteilleGame:MovieClip;
	public var p_mcCoches:MovieClip;

	public var p_mcBulle:MovieClip;
	public var selectedGame:String;
	public var nameGames:Array = ["champi","bouteille"];
	public var sensGame:String;
	public var tabNumber:Array;
	
	

	public var p_mcResults:level1_resultat;
	public var actualDrag:MovieClip;
	
	public var nbrPassage:Number;
	public var actualTry:Number;
	public var actualTryExo:Number;
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var nbrPassageAct:Number;
	private var myInterval:Number;
	private var nbrTry:Number;
	
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_04()	{		
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";	
		startGame();
	}
	
	

	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 0;
		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		actualTryExo = 0;
		p_txtStart.text = "Départ";
		
		p_btnStart._alpha = 0;
		p_btnChampi._alpha = 0;
		p_btnBouteille._alpha = 0;
		p_mcBouteille._visible= false;
		p_mcBulle._visible = false;
		p_mcBouteilleGame._visible = false;
		p_mcChampiGame._visible = false;
		selectedGame = "champi";
		
		tabNumber = new Array();
		p_mcBouteilleGame.tabSelecte = new Array();
		
		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		
		
		nbrTry = 5;
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		
		//create numbers for champignons
		var p_mcBtnNumbers = p_mcChampiGame.createEmptyMovieClip("p_mcBtnNumbers",1);
		
		for(var i = 0;i<=5;i++){
			
			var res = p_mcBtnNumbers.attachMovie("btnChampiNumber","b_"+i,i+1);
			res.fond._alpha = 0;
			res._y = 320;
			res._x = 47 + i*78;
			res.nbr = i;
			res.texte.text = i+1;
			res.xinit = res._x;
			res.yinit = res._y;
			res.onPress = function(){
				this.startDrag(false,0,0,485,340);
			}
			res.onRelease =res.onReleaseOutside = function(){
				this.stopDrag();
				_parent._parent._parent.soundPlayer.playASound("btnClick",0);
				var test = false;
				for(var i =0;i<6;i++){
					var mcHit = _parent._parent["h"+i];
					if(this.hitTest(mcHit)){
						test = true;
						this._x = mcHit._x+3;
						this._y = mcHit._y+8;
						if(mcHit.selecte !=undefined){
							var t = mcHit.selecte;
							t.selecte = undefined;
							t._x = t.xinit;
							t._y = t.yinit;
						}
						if(this.selecte!=undefined){
							var t = this.selecte;
							t.selecte=undefined;
						}
						this.selecte = mcHit;
						mcHit.selecte = this;
						
					}
				}
				if(!test){
					if(this.selecte!=undefined){
						var t = this.selecte;
						t.selecte=undefined;
					}
					this.selecte = undefined;
					this._x = this.xinit;
					this._y = this.yinit;
				}
				var testNbrPose = 0;
				for(var i =0;i<6;i++){
					if(_parent["b_"+i].selecte!=undefined) testNbrPose++
				}
				if(testNbrPose==6){
					var tabRef = _parent._parent._parent.tabNumber;
					
					var resultatTest = true;
					var se = 1;
					var si = 1;
					if(_parent._parent._parent.sensGame == "desc") {
						se = 0;
						si = -1;
					}
					for(var i =0;i<6;i++){
						var t = _parent._parent["h"+(5*se-si*i)].selecte;	
						if(tabRef[i]!=t.val){
							resultatTest=false;
							trace(i+":"+tabRef[i]+":"+t.val)
						}
					}
					var t = _parent._parent._parent;
					if(!resultatTest){
						t.p_mcResults.modifyFalseResult();
					}else{						
						if(t.p_mcResults.modifyGoodResult()){						
							t.p_isPlaying = false;
							t.p_txtStart.text = "Départ";
							t.p_btnStart.enabled = true;
							t.actualTry =0
						}else{
							t.initializeGameInterface();
						}						
					}				
				}
			}
			
		}
		//create numbers for bouteilles
		var p_mcBtnNumbers = p_mcBouteilleGame.createEmptyMovieClip("p_mcBtnNumbers",1);
		this.p_mcResults.reinitializeResult
		for(var i = 0;i<=5;i++){
			
			var res = p_mcBtnNumbers.attachMovie("btnChampiNumber","b_"+i,i+1);
			res.fond._alpha = 0;
			res._y = 326;
			res._x = 52 + i*67;
			res.nbr = i;
			res.texte.text = i+1;
			res.texte.textColor = 0xCCCCCC;
			res.xinit = res._x;
			res.yinit = res._y;
			res.useHandCursor = false;
			res.onSelecte = false;
			res.onPress = function(){
				if(this.onSelecte){
					this.startDrag(false,0,0,485,340);
					if(this.selecte!=undefined){
						_parent._parent["h"+this.selecte].selecte = undefined;
					}
				}
			}
			res.onRelease =res.onReleaseOutside = function(){
				_parent._parent._parent.soundPlayer.playASound("btnClick",0);
				if(this.onSelecte){
					this.stopDrag();
					var test = false;
					for(var i =0;i<6;i++){
						var mcHit = _parent._parent["h"+i];
						if(this.hitTest(mcHit) && mcHit.selecte == undefined){
							test = true;
							this._x = mcHit._x+3;
							this._y = mcHit._y+8;							
							this.selecte = i;
							this.mcHit = mcHit;
						}
					}
					if(!test){
						this._x = this.xinit;
						this._y = this.yinit;
						this.selecte = undefined;
						this._parent._parent._parent.p_btnStart.enabled = false;
					}else{
						this._parent._parent._parent.p_btnStart.enabled = true;
					}
				}
			}
			
		}
		
		
		p_btnStart.onPress = function(){
			
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				if(_parent.selectedGame == "champi"){
					_parent.p_mcBouteilleGame._visible = false;
					_parent.p_mcChampiGame._visible = true;
					this.enabled = false;					
				}else{
					_parent.p_mcBouteilleGame._visible = true;
					_parent.p_mcChampiGame._visible = false;
				}
				_parent.p_mcResults.reinitializeResult();
				_parent.initializeGameInterface();
				_parent.p_txtStart.text = "Suite";
				_parent.p_isPlaying=true;
				_parent.p_btnChampi.enabled = false;
				_parent.p_btnBouteille.enabled = false;
			}else{				
				if(_parent.selectedGame == "bouteille"){
					if(_parent.p_isPlaying){
						_parent.soundPlayer.playASound("btnClick",0);
						var t = _parent;
						if(t.p_mcBouteilleGame.tabSelecte.length!=0){
							if(_parent.actualDrag.selecte!=undefined){								
								if(t.sensGame == "desc"){
									var test = t.tabNumber[t.actualDrag.selecte];
								}else{
									var test = t.tabNumber[5-t.actualDrag.selecte];
								}
								
								if( test != Number(t.actualDrag.texte.text)){
									var res = t.actualDrag;
									res._x = res.xinit;
									res._y = res.yinit;
									res.selecte = undefined;
									this.enabled = false;										
									t.p_mcResults.modifyFalseResult();
								}else{
									t.p_mcBouteilleGame["mcF"+t.actualDrag.selecte]._visible = true;
									t.actualDrag.mcHit.selecte = "ok";
									t.actualDrag.onSelecte = false;
									t.actualDrag.enabled = false;
									t.moveNumberBouteille(random(t.p_mcBouteilleGame.tabSelecte.length));									
								}									
							}
						}else{
							if(t.p_mcResults.modifyGoodResult()){
								_parent.p_isPlaying = false;
								_parent.p_btnChampi.enabled = true;
								_parent.p_btnBouteille.enabled = true;
								_parent.p_txtStart.text = "Départ";
								this.enabled = true;
							}else{
								_parent.initializeGameInterface();
							}
							
						}												
					}
				}
			}
		}
		p_btnChampi.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcBouteille._visible= false;
				_parent.p_mcChampi._visible= true;
				_parent.selectedGame = "champi";
			}
		}
		p_btnBouteille.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcBouteille._visible= true;
				_parent.p_mcChampi._visible= false;
				_parent.selectedGame = "bouteille";
			}
		}
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			p_mcBouteille._visible= true;
			p_mcChampi._visible= false;
			selectedGame = "bouteille";
		}else{
			p_mcBouteille._visible= false;
			p_mcChampi._visible= true;
			selectedGame = "champi";
		}
		p_btnStart._alpha = 0;
		p_btnChampi._alpha = 0;
		p_btnBouteille._alpha = 0;
		p_mcBulle._visible = false;
		p_mcBouteilleGame._visible = false;
		p_mcChampiGame._visible = false;
	}
	public function switchExercice(val:Boolean):Void{
		this.p_mcBulle._visible = true;
		p_isPlaying = false;
		if(this.selectedGame=="champi"){
			p_mcBouteille._visible= true;
			p_mcChampi._visible= false;
			this.selectedGame = "bouteille";
			if(!val){
				this.p_mcCoches.mc1._visible = true;
			}
		}else{
			p_mcBouteille._visible= false;
			p_mcChampi._visible= true;
			this.selectedGame = "champi";
			if(!val){
				this.p_mcCoches.mc2._visible = true;
			}
		}
	}
	public function initializeGameInterface():Void{
		this.p_mcBulle._visible = false;
		this.p_mcResults.checkResultAndReset();
		actualTryExo = 1;
		if(this.selectedGame == "bouteille"){
			var mcTarget = p_mcBouteilleGame;
		}else{
			var mcTarget = p_mcChampiGame;
		}
		var testSens = Math.random();
		if(testSens>0.5){
			this.sensGame = "asc";
			mcTarget.gotoAndStop(2);
		}else{
			this.sensGame = "desc";
			mcTarget.gotoAndStop(1);
		}
		var dep = 2+random(5);
		var tabRemplissage = new Array();
		for(var i =0;i<6;i++){
			this.tabNumber[i]=dep;
			tabRemplissage[i]=dep;
			dep+=1+random(5);
		}
		for(var i =0;i<6;i++){
			var mcHit = mcTarget["h"+i];
			mcHit.selecte = undefined;		
			var res = mcTarget.p_mcBtnNumbers["b_"+i];
			res.selecte = undefined;
			var te = random(tabRemplissage.length);
			res.texte.text = tabRemplissage.splice(te,1 ).toString();
			res.val = Number(res.texte.text );			
			if(this.selectedGame == "bouteille"){
				mcTarget.tabSelecte[i] = res;
				mcTarget["mcF"+i]._visible = false;
				res.onSelecte = false;
				res.texte.textColor = 0x444444;
				res.yinit = 326;
			}
			res._x = res.xinit;
			res._y = res.yinit;
		}
		if(this.selectedGame == "bouteille"){
			moveNumberBouteille(random(6));
		}
	}
	public function moveNumberBouteille(nbr:Number):Void{
		var res = p_mcBouteilleGame.tabSelecte[nbr];
		p_mcBouteilleGame.tabSelecte.splice(nbr,1);
		res._y = 257;
		this.actualDrag = res;
		res.selecte = undefined;
		res.texte.textColor = 0x000000;
		res.onSelecte = true;
		res.enabled = true;
		res.yinit = 257;
		this.p_btnStart.enabled = false;
	}
}

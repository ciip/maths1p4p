﻿/**
 * 
 * class com.maths1p4p.level1.game1_05.Game1_05
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.level1.game1_05.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_05.Game1_05 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	
	public var p_txtStart:TextField;
	public var p_txtName:TextField;	
	

	public var p_btnStart:Button;
	public var p_btnEnd:Button;
	
	public var p_isPlaying:Boolean;
	
	public var p_mcNumbers:MovieClip;
	public var lastSelected:MovieClip;
	public var mcFleches:MovieClip;
	public var p_mcCoches:MovieClip;
	public var p_mcBulle:MovieClip;
	public var catFin:MovieClip;

	public var tabNumber:Array;
	public var selectedNumber:Number;
	
	public var selectedGame:String;
	public var nameGames:Array = ["jeu"];

	public var p_mcResults:level1_resultat;


	public var actualTry:Number;
	public var actualTryExo:Number;
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	private var myInterval:Number;
	private var nbrTry:Number;
	private var p_txtNbr:TextField;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_05()	{		
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 0;
		actualTryExo = 0;
		p_mcCoches.mc1._visible = false;
		p_txtStart.text = "Départ";
		p_btnEnd.enabled = false;
		p_mcBulle._visible = false;
		catFin = this.attachMovie("mcCatFin","catFin",15);
		catFin._x = 361;
		catFin._y = 213;
		catFin._visible = false;
		tabNumber = new Array();
		selectedGame = "jeu";

		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		
		
		nbrTry = 4;//4
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		
		var tabConnexion =["*1*6*7*","*0*6*5*2*","*1*5*4*3*","*2*4*11*","*2*5*10*11*3*","*1*2*4*10*12*9*6*","*0*1*5*9*8*7*","*0*6*8*14*","*7*6*9*13*14*","*13*6*5*12*","*12*5*4*11*","*10*4*3*","*9*10*","*14*8*9*","*7*8*13*"];
		for(var i=0;i<15;i++){
			var mc = p_mcNumbers["mc"+i];
			mc._visible=false;
			mc.ref = this;
			mc.place = i;
			mc.searchString = tabConnexion[i];
			mc.bg.myColor = new Color(mc.bg);
			mc.onSelecte = false;
			mc.onPress = function(){
				this.ref.soundPlayer.playASound("btnClick",0);
				if(this.onSelecte){
					if(this.ref.tabNumber.length == 0){
						if(this.place==0 || this.place==1 || this.place==2){
							this.ref.tabNumber.push(this);
							this.bg.myColor.setRGB(0x000000);
							this.texte.textColor = 0xFFFFFF;
							this.selecte = true;
							var fleche = this.ref.mcFleches.attachMovie("mcFleche","fleche"+this.ref.tabNumber.length,this.ref.tabNumber.length);
							fleche._x = (this._x+48)/2;
							fleche._y = (this._y+57)/2;
							var angle = Math.atan((this._y-57)/(this._x-48))*180/Math.PI;
							if(this._x < 48) angle+=180
							fleche._rotation = angle;
						}
					}else{
						var last = this.ref.tabNumber[(this.ref.tabNumber.length-1)];
						if( last == this){
							this.ref.mcFleches["fleche"+this.ref.tabNumber.length].removeMovieClip();
							this.ref.tabNumber.pop();
							this.bg.myColor.setRGB(0xFFFFFF);
							this.texte.textColor = 0x000000;
							this.selecte = false;
							
						}else if(!this.selecte && last.searchString.indexOf("*"+this.place+"*")>=0){
							var mcBefore = this.ref.tabNumber[this.ref.tabNumber.length-1];
							trace(mcBefore);
							this.ref.tabNumber.push(this);
							this.bg.myColor.setRGB(0x000000);
							this.texte.textColor = 0xFFFFFF;
							this.selecte = true;
							var fleche = this.ref.mcFleches.attachMovie("mcFleche","fleche"+this.ref.tabNumber.length,this.ref.tabNumber.length);						
							fleche._x = (this._x+mcBefore._x)/2;
							fleche._y = (this._y+mcBefore._y)/2;
							var angle = Math.atan((this._y-mcBefore._y)/(this._x-mcBefore._x))*180/Math.PI;
							if(this._x < mcBefore._x) angle+=180
							fleche._rotation = angle;
						}
					}
				}
			}
		}
		
		p_btnEnd.onPress = function(){
			if(_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				var te = _parent.tabNumber[(_parent.tabNumber.length-1)].place;
				if(te==13 || te==12 || te==10){
					var sco = 0;
					for(var i in _parent.tabNumber){
						sco+=_parent.tabNumber[i].nbr;
					}
					if(sco == _parent.selectedNumber){
						var mcBefore = _parent.tabNumber[_parent.tabNumber.length-1];
						var fleche = _parent.mcFleches.attachMovie("mcFleche","fleche"+(_parent.tabNumber.length+1),(_parent.tabNumber.length+1));						
						fleche._x = (349+mcBefore._x)/2;
						fleche._y = (-214+mcBefore._y)/2;
						var angle = Math.atan((-214-mcBefore._y)/(349-mcBefore._x))*180/Math.PI;
						fleche._rotation = angle;
						_parent.p_mcResults.modifyGoodResult();
						_parent.p_mcBulle._visible = true;
						this.enabled = false;
						_parent.p_isPlaying=false;
						_parent.p_btnStart.enabled = true;
						for(var i in _parent.p_mcNumbers){
							var mc = _parent.p_mcNumbers[i];
							mc.enabled = false;
							mc.onSelecte = false;
						}
					}else{
						trace("faux");
						_parent.p_mcResults.modifyFalseResult();
					}
					
				}				
			}
		}
		
		p_btnStart.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.p_mcBulle._visible = false;
				_parent.catFin._visible = false;
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.initializeGameInterface();
				_parent.p_isPlaying=true;
				this.enabled = false;
				_parent.p_btnEnd.enabled = true;
			}
		}
		this.mcFleches = this.createEmptyMovieClip("mcFleches",4);
		this.mcFleches._x = this.p_mcNumbers._x;
		this.mcFleches._y = this.p_mcNumbers._y;
	}
	public function changeExoInit(nbr:Number):Void{
		//rien car un seul exo
	}
	public function switchExercice(val:Boolean):Void{
		this.p_mcBulle._visible = true;
		this.catFin._visible = true;
		if(!val){
			this.p_mcCoches.mc1._visible = true;
		}
	}
	public function initializeGameInterface():Void{
		actualTryExo = 1;
		this.p_mcResults.checkResultAndReset();
		this.selectedNumber = 8+random(7);
		p_txtNbr.text = this.selectedNumber.toString();
		this.tabNumber.splice(0);
		for(var i in this.mcFleches){
			this.mcFleches[i].removeMovieClip();
		}
		for(var i in p_mcNumbers){
			var mc = p_mcNumbers[i];
			mc._visible=true;
			mc.nbr = random(3)+1;
			mc.texte.text = mc.nbr;
			mc.selecte = false;
			mc.onSelecte = true;
			mc.enabled = true;
			mc.bg.myColor.setRGB(0xFFFFFF);
			mc.texte.textColor = 0x000000;
		}
	}
}

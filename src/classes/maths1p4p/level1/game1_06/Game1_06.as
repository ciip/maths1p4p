﻿/**
 * 
 * class com.maths1p4p.level1.game1_06.game1_06
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.level1.game1_06.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_06.Game1_06 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	
	public var p_txtStart:TextField;
	public var p_txtName:TextField;	
	

	public var p_btnStart:Button;
	public var p_btnTwoCol:Button;
	public var p_btnMoreRed:Button;
	public var p_btnThreeCol:Button;
	public var p_btnErase:Button;
	public var p_btnCommand:Button;
	public var p_btnPaint:Button;

	
	public var p_mcTwoCol:Button;
	public var p_mcMoreRed:Button;
	public var p_mcThreeCol:Button;
	
	public var p_isPlaying:Boolean;
	
	public var p_mcNumbers:MovieClip;
	public var lastSelected:MovieClip;
	public var p_mcCommandPanel:MovieClip;
	public var p_mcHandCommand:MovieClip;
	public var p_mcHandPaint:MovieClip;
	public var p_mcCat:MovieClip;
	public var p_mcPot0:MovieClip;
	public var p_mcPot1:MovieClip;
	public var p_mcPot2:MovieClip;
	public var p_mcPinceau:MovieClip;
	public var p_mcSquares:MovieClip;
	public var p_mcCoches:MovieClip;
	
	public var dragPinceau:Boolean;

	public var tabColor:Array;
	public var selectedNumber:Number;
	

	public var p_mcResults:level1_resultat;


	public var actualTry:Number;
	public var actualTryExo:Number;
	public var selectedGame:String;
	public var nameGames:Array = ["twocol","morered","threecol"];

	public var isPaintDecided:Boolean;
	public var potSelecte:MovieClip;
	public var selectedColor:Number;
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	private var myInterval:Number;
	private var nbrTry:Number;
	private var maxColor:Array;
	private var trueColor:Array;
	
	private var actColor:Array;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_06()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 0;
		actualTryExo = 0;
		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		p_mcCoches.mc3._visible = false;
		
		
		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		
		
		nbrTry = 1;
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		
		
		p_txtStart.text = "J'ai fini";
		p_txtStart.textColor = 0xCCCCCC;
		p_btnStart.enabled = false;
		p_btnPaint.enabled = false;
		p_btnErase.enabled = false;
		p_mcCommandPanel._visible = false;
		p_mcHandPaint._visible= false;
		
		maxColor = new Array();
		actColor = new Array();
		dragPinceau = false;
		

		p_mcPinceau = this.attachMovie("mcPinceau","p_mcPinceau",5);
		p_mcPinceau._visible= false;
		p_mcSquares = this.createEmptyMovieClip("p_mcSquares",4);
		p_mcSquares._x = 77;
		p_mcSquares._y = 153;
		for(var i=0;i<12;i++){
			var mc = p_mcSquares.attachMovie("mcSquare","square"+i,i+1);
			
			mc._x = (i-Math.floor(i/4)*4)*66;
			mc._y = Math.floor(i/4)*66;
			trace(mc._y);
			mc.useHandCursor = false;
			mc.ref = this;
			mc.bg.myColor = new Color(mc.bg);
			mc.bg.setRGB(0xadadad);
			mc.onPress = function(){
				if(this.ref.p_isPlaying && this.ref.isPaintDecided && this.ref.dragPinceau){
					this.ref.soundPlayer.playASound("btnClick",0);
					this.ref.checkAndPutColor(this.bg)
				}
			}
		}
		
		
		
		isPaintDecided = false;
		tabColor = [0xFF0000,0x0000FF,0xFFFF00];
		
		
		p_btnStart.onPress = function(){
			if(_parent.p_isPlaying && _parent.isPaintDecided){
				_parent.posePinceau();
				var test = true;
				if(_parent.selectedGame=="morered"){
					if(Number(_parent.p_mcCommandPanel.pot0.texte.text) <= Number(_parent.p_mcCommandPanel.pot1.texte.text)) test = false;
					if(Number(_parent.p_mcCommandPanel.pot0.texte.text)+Number(_parent.p_mcCommandPanel.pot1.texte.text) !=12) test = false;
				}else if(_parent.selectedGame=="twocol"){
					if(Number(_parent.p_mcCommandPanel.pot0.texte.text)+Number(_parent.p_mcCommandPanel.pot1.texte.text) !=12) test = false;
				}else{
					if(Number(_parent.p_mcCommandPanel.pot0.texte.text)+Number(_parent.p_mcCommandPanel.pot1.texte.text)+Number(_parent.p_mcCommandPanel.pot2.texte.text) !=12) test = false;	
				}
				//si gagn
				if(test){
					_parent.soundPlayer.playASound("btnClick",0);
					_parent.p_mcResults.modifyGoodResult();
					_parent.p_isPlaying=false;
					_parent.isPaintDecided = false;
					
					this.enabled = false;
					
					_parent.p_mcHandCommand._visible = true;
					
					
					_parent.p_btnErase.enabled = false;
					_parent.p_btnTwoCol.enabled = true;
					_parent.p_btnMoreRed.enabled = true;
					_parent.p_btnThreeCol.enabled = true;
					_parent.p_btnCommand.enabled = true;
					
					_parent.p_txtStart.textColor = 0xCCCCCC;
					
					_parent.p_mcCat._visible = true;
					_parent.p_mcCat.bulle._visible = true;
					_parent.p_mcCommandPanel._visible = false;
					for(var i=0;i<3;i++){
						_parent["p_mcPot"+i].enabled = false;
					}
				}else{
					_parent.p_mcResults.modifyFalseResult();
					_parent.soundPlayer.playASound("commande",0);
					_parent.continueInititializeInterface();
					_parent.isPaintDecided = false;
				
					_parent.p_mcHandCommand._visible = false;
					_parent.p_mcHandPaint._visible = true;
					
					this.enabled = false;
				
					_parent.p_btnTwoCol.enabled = false;
					_parent.p_btnMoreRed.enabled = false;
					_parent.p_btnThreeCol.enabled = false;

					_parent.p_btnPaint.enabled = true;
					_parent.p_txtStart.textColor = 0xCCCCCC;
					for(var i=0;i<3;i++){
						_parent["p_mcPot"+i].enabled = false;
					}
					this.enabled = false;
				}
				
				//
			}
		}
		p_btnErase.onPress = function(){
			if(_parent.p_isPlaying && _parent.isPaintDecided){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.posePinceau();
				for(var i in _parent.p_mcSquares){
					var mc = _parent.p_mcSquares[i];			
					mc.bg.myColor.setRGB(0xadadad);
				}
				_parent.actColor = [0,0,0];
				_parent.p_isPlaying=false;
				_parent.isPaintDecided = false;
				
				this.enabled = false;
				
				_parent.p_mcHandCommand._visible = true;
				
				
				_parent.p_btnErase.enabled = false;
				_parent.p_btnTwoCol.enabled = true;
				_parent.p_btnMoreRed.enabled = true;
				_parent.p_btnThreeCol.enabled = true;
				_parent.p_btnCommand.enabled = true;
				
				_parent.p_txtStart.textColor = 0xCCCCCC;
				
				_parent.p_mcCat._visible = true;
				_parent.p_mcCat.bulle._visible = false;
				_parent.p_mcCommandPanel._visible = false;
				for(var i=0;i<3;i++){
					_parent["p_mcPot"+i].enabled = false;
				}
			}
		}
		p_btnCommand.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcResults.checkResultAndReset();
				_parent.initializeGameInterface();
				_parent.p_isPlaying=true;
				
				_parent.p_mcHandCommand._visible = false;
				_parent.p_mcHandPaint._visible = true;
				
				_parent.p_mcCat._visible = false;
				this.enabled = false;
				
				_parent.p_btnTwoCol.enabled = false;
				_parent.p_btnMoreRed.enabled = false;
				_parent.p_btnThreeCol.enabled = false;

				_parent.p_btnPaint.enabled = true;
			}
		}
		
		this.potSelecte = undefined;
		
		for(var i=0;i<3;i++){
			var mc = this["p_mcPot"+i];
			mc.selecte = false;
			mc.col = i;
			mc.onPress = function(){
				if(_parent.p_isPlaying && _parent.isPaintDecided){
					_parent.soundPlayer.playASound("btnClick",0);
					if(!this.selecte){
						_parent.p_mcPinceau._visible = true;
						_parent.p_mcPinceau._x = _xmouse;
						_parent.p_mcPinceau._y = _ymouse;
						_parent.p_mcPinceau.startDrag(true,30,80,328,390);
						this.mcPinceau._visible = false;
						this.selecte = true;
						_parent.potSelecte.selecte = false;
						_parent.potSelecte.mcPinceau._visible = true;	
						_parent.potSelecte = this;
						_parent.selectedColor = _parent.tabColor[this.col];
						_parent.dragPinceau = true;
					}else{
						_parent.p_mcPinceau._visible = false;
						_parent.p_mcPinceau.stopDrag();
						this.mcPinceau._visible = true;
						this.selecte = false;
						_parent.potSelecte = undefined;
						_parent.dragPinceau = false;
					}
				}
			}
			this["p_mcPot"+i].enabled = false;
		}
		p_btnPaint.onPress = function(){
			if(_parent.p_isPlaying && !_parent.isPaintDecided){				
				var somme = 0;
				
				for(var i=0;i<3;i++){
					somme+=Number(this._parent.p_mcCommandPanel["pot"+i].texte.text);
				}
				if(somme>12){
					_parent.soundPlayer.playASound("tropplaques",0);
				}else{
					for(var i=0;i<3;i++){
						this._parent.p_mcCommandPanel["pot"+i].texte.selectable = false;
					}
					_parent.soundPlayer.playASound("btnClick",0);
					_parent.isPaintDecided = true;
					this.enabled = false;
					_parent.p_mcHandPaint._visible = false;
					
					_parent.p_txtStart.textColor = 0x000000;
					_parent.p_btnStart.enabled = true;
					_parent.p_btnErase.enabled = true;
					for(var i=0;i<3;i++){
						_parent["p_mcPot"+i].enabled = true;
					}
				}
				
				
			}
		}
		p_btnTwoCol.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcMoreRed._visible = false;
				_parent.p_mcTwoCol._visible= true;
				_parent.p_mcThreeCol._visible= false;
				_parent.selectedGame = "twocol";
			}			
		}
		p_btnMoreRed.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcMoreRed._visible = true;
				_parent.p_mcTwoCol._visible= false;
				_parent.p_mcThreeCol._visible= false;
				_parent.selectedGame = "morered";
			}			
		}
		p_btnThreeCol.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcMoreRed._visible = false;
				_parent.p_mcTwoCol._visible= false;
				_parent.p_mcThreeCol._visible= true;
				_parent.selectedGame = "threecol";
			}			
		}
		
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			p_mcMoreRed._visible = true;
			p_mcTwoCol._visible= false;
			p_mcThreeCol._visible= false;
			selectedGame = "morered";
		}else if(nbr == 1){
			p_mcMoreRed._visible = false;
			p_mcTwoCol._visible= false;
			p_mcThreeCol._visible= true;
			selectedGame = "threecol";
		}else{
			p_mcMoreRed._visible = false;
			p_mcTwoCol._visible= true;
			p_mcThreeCol._visible= false;
			selectedGame = "twocol";
		}
		p_mcCat.bulle._visible = false;
		
	}
	public function switchExercice(val:Boolean):Void{
		this.p_mcCat.bulle._visible = true;
		if(this.selectedGame=="twocol"){
			p_mcMoreRed._visible= true;
			p_mcThreeCol._visible= false;
			p_mcTwoCol._visible= false;
			this.selectedGame = "morered";
			if(!val){
				this.p_mcCoches.mc1._visible = true;
			}
		}else if(this.selectedGame=="morered"){
			p_mcMoreRed._visible= false;
			p_mcThreeCol._visible= true;
			p_mcTwoCol._visible= false;
			this.selectedGame = "threecol";
			if(!val){
				this.p_mcCoches.mc2._visible = true;
			}
		}else{
			p_mcMoreRed._visible= false;
			p_mcThreeCol._visible= false;
			p_mcTwoCol._visible= true;
			this.selectedGame = "twocol";
			if(!val){
				this.p_mcCoches.mc3._visible = true;
			}
		}
	}
	public function checkAndPutColor(mc:MovieClip):Void{
		for(var i in this.tabColor){
			if(this.selectedColor==this.tabColor[i]){
				var test = Math.min(this.maxColor[i],Number(this.p_mcCommandPanel["pot"+i].texte.text));
				if(this.actColor[i]<test){
					if(mc.myColor.getRGB() != 0xadadad){
						for(var j in this.tabColor){
							if(mc.myColor.getRGB()==this.tabColor[j]){
								this.actColor[j]=this.actColor[j]-1;
							}
						}
					}
					this.actColor[i]=this.actColor[i]+1;
					mc.myColor.setRGB(this.selectedColor);
				}				
			}
		}
	}
	public function focus():Void{
		clearInterval(this.myInterval);
		var texteFocus =this.p_mcCommandPanel.pot0.texte; 
		Selection.setFocus(texteFocus);
	}
	public function posePinceau():Void{
		this.p_mcPinceau._visible = false;
		this.p_mcPinceau.stopDrag();
		this.potSelecte.mcPinceau._visible = true;
		this.potSelecte.selecte = false;
		this.potSelecte = undefined;
		this.dragPinceau = false;
	}
	public function initializeGameInterface():Void{
		actualTryExo = 1;
		for(var i=0;i<3;i++){
			this.p_mcCommandPanel["pot"+i].texte.selectable = true;
		}
		this.continueInititializeInterface();
		
	}
	public function continueInititializeInterface():Void{
		for(var i=0;i<3;i++){
			this.p_mcCommandPanel["pot"+i].texte.selectable = true;
		}
		this.selectedColor = undefined;
		if(this.selectedGame =="twocol"){
			this.p_mcCommandPanel.pot2._visible = false;
			this.p_mcPot2._visible = false;
			maxColor = [12,12,0];
		}else if(this.selectedGame =="morered"){
			this.p_mcCommandPanel.pot2._visible = false;
			this.p_mcPot2._visible = false;
			maxColor = [12,12,0];
			
		}else if(this.selectedGame =="threecol"){
			this.p_mcCommandPanel.pot2._visible = true;
			this.p_mcPot2._visible = true;
			maxColor = [12,12,12];
		}
		actColor = [0,0,0];
		this.p_mcCommandPanel._visible = true;
		
		if(this.myInterval != undefined) {
			clearInterval(this.myInterval)
		}
		this.myInterval = setInterval(this,"focus",100);
		for(var i=0;i<3;i++){
			this.p_mcCommandPanel["pot"+i].texte.text = 0;
			this["p_mcPot"+i].enabled = false;
		}
		for(var i in this.p_mcSquares){
			this.p_mcSquares[i].bg.myColor.setRGB(0xadadad);
		}
	}
}

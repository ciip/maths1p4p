﻿/**
 * 
 * class com.maths1p4p.level1.game1_07.game1_07
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.level1.game1_07.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_07.Game1_07 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_txtName:TextField;
	
	

	public var p_mcSquares:MovieClip;
	public var mcBtn:MovieClip;
	public var mcNumbers:MovieClip;
	public var p_mcCoches:MovieClip;
	public var mcPrintFinal:MovieClip;
	
	public var p_mcBulle:MovieClip;
	
	
	public var actPress:Number;
	public var tabPressed:Array;
	public var p_mcResults:level1_resultat;
	
	public var p_isPlaying:Boolean;
	
	public var selectedGame:String;
	public var nameGames:Array = ["jeu"];
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var nbrTry:Number;
	private var actualTry:Number;
	private var myInterval:Number;
	private var nbrTotalFound:Number;
	private var tabBtn:Array;
	private var isBomb:Boolean;
	static private var tabAlreadyHere:Array = [5,7,9,22,24,36,42,46];
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_07()	{	
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){		
		p_isPlaying = true;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		this.mcNumbers.setMask(this.mcBtn);
		nbrTotalFound = 0
		p_mcCoches.mc1._visible = false;
		p_mcBulle = this.attachMovie("mcBulle","p_mcBulle",20);
		p_mcBulle._visible = false;
		mcPrintFinal._visible = false;
		selectedGame = "jeu";
		
		//create Resultat
		isBomb = true;
		actualTry = 5;
		nbrTry = 5;//5
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		
		this.p_mcSquares = this.createEmptyMovieClip("p_mcSquares",1);
		for(var i = 0;i<14;i++){
			var mc = this.mcBtn["btn"+i];
			mc.nbr = i;
			mc.ref = this;
			mc.onPress = function(){
				if(this.ref.p_isPlaying){
					if(this.ref.actPress == 3){
						this.ref.soundPlayer.playASound("btnClick",0);
						this.ref.testResult(this);
					}
				}
			}
		}
		for(var i =0;i<50;i++){
			var mc = this.p_mcSquares.attachMovie("mcSquare","mcSquare"+i,i+1);
			mc._x = 108+33*(i-Math.floor(i/10)*10);
			mc._y = 65+33*Math.floor(i/10);
			
			var test = true;
			for(var j in tabAlreadyHere){
				if(tabAlreadyHere[j] == i+1) test=false;
			}
			
			if(test){
				mc.bg._alpha = 0;
				mc.bg.ref = this;
				mc.nbr = i+1;
				mc.bg.selecte = false;
				mc.bg.onPress = function(){
					if(this.ref.p_isPlaying){
						
						if(!this.selecte){							
							if(this.ref.actPress<3){
								this.ref.soundPlayer.playASound("btnClick",0);
								this._alpha = 100;
								this.sel = this.ref.actPress;
								this.ref.tabPressed[this.ref.actPress] = this._parent;
								this.ref.actPress++;
								this.selecte = true;
							}else{
								this.ref.soundPlayer.playASound("trop",0);
							}
						}else{
							this.ref.soundPlayer.playASound("btnClick",0);
							this.selecte = false;
							this._alpha = 0;
							this.ref.tabPressed.splice(this.sel,1);
							this.ref.actPress--;
							this.ref.actPress=Math.max(0,this.ref.actPress);
						}
					}
				}
			}else{
				mc._visible = false
			}
		}
		this.actPress = 0;
		this.tabPressed = new Array();
		this.tabBtn = [[25,26,35],[47,48,49],[39,40,50],[13,14,23],	[32,33,43],[6,15,16],[1,11,12],[2,3,4],[21,31,41],	[28,29,30],[19,20,10],[34,44,45],[27,37,38],[17,18,8]];
	}
	public function changeExoInit(nbr:Number):Void{
		//rien car un seul exo
	}
	public function switchExercice(val:Boolean):Void{
		p_mcBulle._visible = true;
		if(!val){
			this.p_mcCoches.mc1._visible = true;
		}		
	}
	public function testResult(btn:MovieClip):Void{
		var nbr = btn.nbr;
		var stringTest =","+this.tabBtn[nbr].toString()+",";
		var te = true;
		trace(this.tabPressed);
		for(var i in this.tabPressed){
			if(stringTest.indexOf(","+this.tabPressed[i].nbr+",")<0) te = false;
		}
		if(!te){
			if(this.p_mcResults.modifyFalseResult()){
				this.myInterval=setInterval(this,"reInitGame",3000);
			}else{
				this.myInterval=setInterval(this,"reInitAfterError",2000);
			}
			p_isPlaying = false;
		}else{
			btn._xscale = 0;
			btn._yscale = 0;
			this.nbrTotalFound++;
			if(this.nbrTotalFound == 14){
				this.p_mcResults.modifyGoodResult();
				p_mcBulle._visible = true;
				mcPrintFinal._visible = true;
				mcPrintFinal.onPress = function(){
					var ref = this;
					var my_pj:PrintJob = new PrintJob();
					if (my_pj.start()){
						my_pj.addPage(ref.mcNbrFinals, {xMin:0,xMax:870,yMin:0,yMax:559});
						my_pj.send();
					}
					delete my_pj;
				}
			}
			for(var i in this.tabPressed){
				this.tabPressed[i].texte.text = this.tabPressed[i].nbr;
				this.tabPressed[i].bg._visible = false;
			}
			this.actPress = 0;
			this.tabPressed.splice(0);
		}
	}
	private function reInitAfterError():Void{
		clearInterval(this.myInterval);
		p_isPlaying = true;
	}
	private function reInitGame():Void{
		clearInterval(this.myInterval);
		this.p_mcResults.reinitializeResult();
		p_isPlaying = true;
		p_mcBulle._visible = false;
		for(var i =0;i<50;i++){
			var mc = this.p_mcSquares["mcSquare"+i]
			
			var test = true;
			for(var j in tabAlreadyHere){
				if(tabAlreadyHere[j] == i+1) test=false;
			}			
			if(test){
				mc.bg._alpha = 0;
				mc.bg._visible = true;
				mc.texte.text = "";
				mc.bg.selecte = false;
			}else{
				mc._visible = false
			}
		}
		for(var i = 0;i<14;i++){
			var btn = this.mcBtn["btn"+i];
			btn._xscale = 100;
			btn._yscale = 100;
		}
		this.nbrTotalFound = 0;
		this.actPress = 0;
		this.tabPressed.splice(0);
	}
}

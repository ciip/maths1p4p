﻿/**
 * 
 * class com.maths1p4p.level1.game1_08.game1_08
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level1.game1_08.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_08.Game1_08 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	public var p_txtName:TextField;
	public var p_txtTarget:TextField;
	
	

	public var p_btnOnePlayer:Button;
	public var p_btnTwoPlayer:Button;
	public var p_btnRollDices:Button;
	public var p_btnTarget:Button;
	public var p_btnNotTarget:Button;
	
	


	
	public var p_mcOnePlayer:MovieClip;
	public var p_mcTwoPlayer:MovieClip;
	public var p_mcNotTarget:MovieClip;
	public var p_mcTwoPlayerPannel:MovieClip;
	public var p_mcBulle:MovieClip;
	
	
	public var de0:MovieClip;
	public var de1:MovieClip;
	public var de2:MovieClip;
	public var p_mcCoches:MovieClip;
	

	
	public var p_isPlaying:Boolean;
	public var dicesAreRolling:Boolean;
	

	
	public var dragPinceau:Boolean;

	public var tabColor:Array;
	public var selectedNumber:Number;
	

	public var p_mcResults:level1_resultat;


	public var actualTry:Number;
	public var actualTryExo:Number;
	public var selectedGame:String;
	public var nbrRoll:Number;
	public var playerAct:Number;
	public var blockGameTwo:Boolean;
	public var nbrTryGameTwo:Number;
	public var nameGames:Array = ["one"];
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	private var myInterval:Number;
	private var nbrTry:Number;
	private var horloge:Number;
	
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_08()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 0;
		actualTryExo = 0;
		p_mcCoches.mc1._visible = false;
		
		p_mcTwoPlayer._visible = false;
		p_btnRollDices._visible = false;
		p_btnTarget._visible = false;
		p_btnNotTarget._visible = false;
		p_mcNotTarget._visible = false;
		p_mcTwoPlayerPannel._visible = false;
		p_mcBulle._visible = false;
		
		
		this.selectedGame = "one";
		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		
		
		nbrTry = 4;//
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		
		
		for(var i=0;i<3;i++){
			var mc = this["de"+i]
			mc.points._visible = false;
			mc.selecte = false;
			mc.first = true;
			mc.onPress = function(){
				if(_parent.p_isPlaying && !this.first){
					_parent.soundPlayer.playASound("btnClick",0);
					if(!this.selecte){
						this.bg.gotoAndStop(2);
						this.selecte = true;
					}else{
						this.bg.gotoAndStop(3);
						this.selecte = false;
					}
					_parent.makePointsCorrectColor(this);
				}
			}
			mc.useHandCursor = false;
		}
		
		
		p_btnOnePlayer.onPress = function(){
			if(_parent.actualTry == 0 && !_parent.p_isPlaying ){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcTwoPlayer._visible = false;
				_parent.p_mcOnePlayer._visible= true;
				_parent.selectedGame = "one";
				_parent.initializeGameInterface();
				this.useHandCursor = false;
				_parent.p_btnTwoPlayer.useHandCursor = false;
				_parent.dicesAreRolling = false;
			}			
		}
		p_btnTwoPlayer.onPress = function(){
			if(_parent.actualTry == 0 && !_parent.p_isPlaying ){
				_parent.p_mcBulle._visible = false;
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcTwoPlayer._visible = true;
				_parent.p_mcOnePlayer._visible= false;
				this.useHandCursor = false;
				_parent.p_btnOnePlayer.useHandCursor = false;
				_parent.p_mcTwoPlayerPannel._visible = true;
				_parent.p_mcTwoPlayerPannel.twoPlayerWindow.texte.text = "Nom du joueur 1 :";
				_parent.p_mcTwoPlayerPannel.twoPlayerWindow.nomJoueur.text = "joueur 1";
				_parent.p_mcTwoPlayerPannel.twoPlayerWindow.first = true;
				_parent.p_mcTwoPlayerPannel.player2.mcGagne._visible = false;
				_parent.p_mcTwoPlayerPannel.player1.mcGagne._visible = false;
				_parent.dicesAreRolling = true;
			}			
		}
		p_btnRollDices.onPress = function(){
			if(!_parent.dicesAreRolling){
				_parent.p_isPlaying = true;
				if(!_parent.blockGameTwo && _parent.selectedGame == "two"){
					_parent.rollDices();
				}else if(_parent.selectedGame == "one"){
					_parent.rollDices();
				}
				
			}		
		}
		p_btnTarget.onPress = function(){
			if(_parent.p_isPlaying && !_parent.dicesAreRolling){
				_parent.soundPlayer.playASound("btnClick",0);
				if(_parent.blockGameTwo && _parent.selectedGame == "two"){
					_parent.testDices();
				}else if(_parent.selectedGame == "one"){
					_parent.testDices();
				}
			}		
		}
		p_btnNotTarget.onPress = function(){
			if(_parent.p_isPlaying && !_parent.dicesAreRolling){
				_parent.soundPlayer.playASound("btnClick",0);
				if(_parent.blockGameTwo){
					_parent.nextPlayer();
				}
			}		
		}
		var mc = p_mcTwoPlayerPannel.twoPlayerWindow;
		mc.btnOk.onPress = function(){
			_parent._parent._parent.soundPlayer.playASound("btnClick",0);
			_parent._parent._parent.selectedGame = "two";
			_parent._parent._parent.actualTry = 1;
			_parent._parent._parent.p_isPlaying = true;
			if(_parent.first){
				_parent._parent.player1.nom.text = _parent.nomJoueur.text;
				_parent.first = false;
				_parent.texte.text = "Nom du joueur 2 :";
				_parent.nomJoueur.text = "joueur 2";
				
			}else{
				_parent._visible = false;
				_parent._parent.player2.nom.text = _parent.nomJoueur.text;
				_parent._parent.player2.nom.textColor = 0xCCCCCC;
				_parent._parent._parent.dicesAreRolling = false;
				_parent._parent._parent.playerAct = 1;
				_parent._parent._parent.nbrTryGameTwo = 1;
				for(var i=1;i<=5;i++){
					_parent._parent.player2["mc"+i].gotoAndStop(1);
					_parent._parent.player1["mc"+i].gotoAndStop(1);
				}
				_parent._parent.player2.score = 0;
				_parent._parent.player1.score = 0;
				_parent._parent._parent.initializeGameInterface();
			}
		}
		mc.btnCancel.onPress = function(){
			_parent._parent._parent.soundPlayer.playASound("btnClick",0);
			_parent._parent._visible = false;
			_parent._parent._parent.p_isPlaying = false;
			_parent._parent._parent.dicesAreRolling = false;
			_parent._parent._parent.p_btnOnePlayer.useHandCursor = true;
			_parent._parent._parent.p_btnTwoPlayer.useHandCursor = true;
			_parent._parent._parent.p_mcTwoPlayer._visible = false;
			_parent._parent._parent.p_mcOnePlayer._visible= true;
		}
		this.p_isPlaying = false;
		this.p_mcTwoPlayer._visible = false;
		this.p_mcOnePlayer._visible= true;
		this.selectedGame = "one";
		this.initializeGameInterface();
		this.dicesAreRolling = false;
	}
	public function changeExoInit(nbr:Number):Void{
		//rien car un seul exo
	}
	public function switchExercice(val:Boolean):Void{
		p_mcBulle._visible = true;
		actualTry = 0;
		this.p_isPlaying = false;
		this.dicesAreRolling = true;
		if(!val){
			this.p_mcCoches.mc1._visible = true;
		}		
	}
	public function nextPlayer():Void{
		clearInterval(this.myInterval);
		drawCircle(p_mcTwoPlayerPannel["player"+this.playerAct].horloge.cercle, 8, 0x000000, 100,  0);
		blockGameTwo = false;
		if(this.playerAct == 1){
			this.playerAct = 2;
			p_mcTwoPlayerPannel.player1.nom.textColor = 0xCCCCCC;
			p_mcTwoPlayerPannel.player2.nom.textColor = 0x000000;
		}else{
			this.playerAct = 1;
			p_mcTwoPlayerPannel.player2.nom.textColor = 0xCCCCCC;
			p_mcTwoPlayerPannel.player1.nom.textColor = 0x000000;
		}
	}
	public function testDices():Void{
		var somme = 0;
		for(var i=0;i<3;i++){
			var mc = this["de"+i];
			somme+=mc.nbr;			
		}
		if(this.selectedGame == "one"){
			if(somme == this.selectedNumber){
				trace("ok");				
				if(this.p_mcResults.modifyGoodResult()){
					this.p_isPlaying = false;
					this.dicesAreRolling = false;
					this.p_btnOnePlayer.useHandCursor = true;
					this.p_btnTwoPlayer.useHandCursor = true;
				}else{
					this.initializeGameInterface();
					this.p_isPlaying = false;
				}
				
			}else{
				trace("beurk");
				this.p_mcResults.modifyFalseResult();
			}
		}else{
			trace("test 2 jouers");
			if(somme == this.selectedNumber){
				trace("gagne");
				p_mcTwoPlayerPannel["player"+this.playerAct].score++;
				p_mcTwoPlayerPannel["player"+this.playerAct]["mc"+nbrTryGameTwo].gotoAndStop(2);				
				this.nextPlayer();
				p_mcTwoPlayerPannel["player"+this.playerAct]["mc"+nbrTryGameTwo].gotoAndStop(3);
				nbrTryGameTwo++;
				if(this.nbrTryGameTwo == 6){
					playWinnerTwoPlayer();
				}else{
					this.initializeGameInterface();
				}
				
			}else{
				this.nextPlayer();
			}
		}
	}
	public function playWinnerTwoPlayer():Void{
		if(p_mcTwoPlayerPannel.player1.score > p_mcTwoPlayerPannel.player2.score){
			p_mcTwoPlayerPannel.player1.mcGagne._visible =true;
		}else{
			p_mcTwoPlayerPannel.player2.mcGagne._visible =true;
		}
		this.myInterval = setInterval(this,"reinitInterface",2000);
	}
	public function reinitInterface():Void{
		if(this.myInterval!=undefined){
			clearInterval(this.myInterval);
		}
		initInterface();
	}
	
	public function rollDices():Void{
		dicesAreRolling = true;
		blockGameTwo = true;
		for(var i=0;i<3;i++){
			var mc = this["de"+i];
			mc.first = false;
			if(!mc.selecte){ 
				mc.bg.gotoAndStop(3);
				mc.nbr = random(6)+1;
			}
			mc.points._visible = true;			
		}
		if(this.myInterval!=undefined){
			clearInterval(this.myInterval);
		}
		this.nbrRoll = 0;
		this.myInterval = setInterval(this,"rollDicesForTwoSeconds",100);
		soundPlayer.playASound("des",0);
	}
	private function rollDicesForTwoSeconds():Void{
		this.nbrRoll++;
		if(this.nbrRoll>10){
			clearInterval(this.myInterval);
			dicesAreRolling = false;
			for(var i=0;i<3;i++){
				var mc = this["de"+i];
				if(!mc.selecte){ 
					mc.points.gotoAndStop(mc.nbr);
					makePointsCorrectColor(mc);
				}						
			}
			if(this.selectedGame == "two"){
				this.myInterval = setInterval(this,"makeHorloge",50);
				this.horloge = 0;
			}
		}else{
			for(var i=0;i<3;i++){
				var mc = this["de"+i];
				if(!mc.selecte){ 
					mc.points.gotoAndStop(random(6)+1);
					makePointsCorrectColor(mc);
				}		
			}
		}		
	}
	private function makeHorloge():Void{
		this.horloge +=1;
		var perc = this.horloge/100;
		drawCircle(p_mcTwoPlayerPannel["player"+this.playerAct].horloge.cercle, 8, 0x000000, 100,  2*perc*Math.PI);
		if(this.horloge == 100){
			this.nextPlayer();
			soundPlayer.playASound("pot",0);
		}
		if(this.horloge-Math.floor(this.horloge/15)*15==1){
			soundPlayer.playASound("chrono",0);
		}
	}
	private static function drawCircle(target_mc:MovieClip, radius:Number, fillColor:Number, fillAlpha:Number, angle:Number):Void {
		with (target_mc) {
			clear();
			lineStyle(10, fillColor, fillAlpha, false, "normal", "none");
			moveTo(radius, 0);
			var div = Math.floor(angle/(Math.PI/4));
			var init = 0.25;
			var init2 = 1;
			var g_radius = radius/Math.cos(Math.PI/8);
			for (var i = 1; i<div; i++) {
				curveTo(Math.cos(init2*Math.PI/8)*g_radius, Math.sin(init2*Math.PI/8)*g_radius, Math.cos(init*Math.PI)*radius, Math.sin(init*Math.PI)*radius);
				init += 0.25;
				init2 += 2;
			}
			var last_angle = (init-0.25)*Math.PI;
			var demi_angle = (angle-last_angle)/2;
			var long = radius/Math.cos(demi_angle);
			curveTo(Math.cos(last_angle+demi_angle)*long, Math.sin(last_angle+demi_angle)*long, Math.cos(angle)*radius, Math.sin(angle)*radius);
		}
	}
	public function makePointsCorrectColor(mc:MovieClip):Void{
		for(var i in mc.points){
			if(mc.selecte) mc.points[i].gotoAndStop(1);
			else mc.points[i].gotoAndStop(2);
		}
	}
	public function initializeGameInterface():Void{
		p_mcBulle._visible = false;
		actualTryExo = 1;
		p_btnRollDices._visible = true;
		p_btnTarget._visible = true;
		blockGameTwo = false;
		if(this.selectedGame == "two"){
			p_btnNotTarget._visible = true;
			p_mcNotTarget._visible = true;
			
		}else{
			p_btnNotTarget._visible = false;
			p_mcNotTarget._visible = false;
		}
		this.selectedNumber = 5+random(14);
		this.p_txtTarget.text = this.selectedNumber.toString();
		for(var i=0;i<3;i++){
			var mc = this["de"+i]
			mc.points._visible = false;
			mc.bg.gotoAndStop(1);
			mc.points._visible = false;
			mc.selecte = false;
			mc.useHandCursor = true;
			mc.first = true;
		}
	}
}

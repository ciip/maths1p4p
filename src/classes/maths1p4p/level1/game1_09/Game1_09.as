﻿/**
 * 
 * class com.maths1p4p.level1.game1_09.game1_09
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level1.game1_09.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_09.Game1_09 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	
	public var p_txtStart:TextField;
	public var p_txtName:TextField;	
	

	public var p_btnStart:Button;
	public var p_btnTwoAddition:Button;
	public var p_btnThreeAddition:Button;
	public var p_btnSigne:Button;


	
	public var p_mcTwoAddition:Button;
	public var p_mcThreeAddition:Button;
	public var p_mcSigne:Button;
	
	public var p_isPlaying:Boolean;
	
	public var p_mcNumbers:MovieClip;
	public var lastSelected:MovieClip;

	public var p_mcCat:MovieClip;
	
	
	public var p_mcCalcul0:MovieClip;
	public var p_mcCalcul1:MovieClip;
	public var p_mcCalcul2:MovieClip;
	public var p_mcCalcul3:MovieClip;
	public var p_mcCoches:MovieClip;
	

	public var p_mcResults:level1_resultat;


	public var actualTry:Number;
	public var actualTryExo:Number;
	public var selectedGame:String;
	public var nameGames:Array = ["TwoAddition","ThreeAddition","Signe"];
	public var selectedNbr:Number;
	
	public var myIntervalTab:Number;
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	private var myInterval:Number;
	private var nbrTry:Number;
	private var testResult:Boolean;
	private var actualCalculTested:Number;
	private var myIntervalResult:Number;
	private var testComplet:Number;
	private var oldSel:TextField;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_09()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 0;
		actualTryExo = 0;
		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		p_mcCoches.mc3._visible = false;
		
		
		p_txtStart.text = "Départ";

		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		
		
		nbrTry = 2;//2
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		
		
		p_btnStart.onPress = function(){
			_parent.soundPlayer.playASound("btnClick",0);
			if(!_parent.p_isPlaying){
				_parent.p_txtStart.text = "J'ai fini";
				_parent.initializeGameInterface();
				_parent.p_isPlaying=true;
				//_parent.p_txtStart.textColor = 0xCCCCCC;
				
				_parent.p_mcCat._visible = false;
				//this.useHandCursor = false;
				
				_parent.p_btnTwoAddition.useHandCursor = false;
				_parent.p_btnThreeAddition.useHandCursor = false;
				_parent.p_btnSigne.useHandCursor = false;
			}else{
				if(_parent.selectedGame != "Signe"){
					//test zéro une fois
					var max = 3
					if(_parent.selectedGame == "TwoAddition"){
						max = 2;
					}
					var testZero = 0;
					var tZero = true;
					for(var i=0;i<4;i++){
						var mc = _parent["p_mcCalcul"+i];
						for(var j = 0;j<max;j++){
							if(Number(mc.calcul["c"+j].text) == 0){
								testZero++;
							}
						}
						if(testZero>1){
							tZero = false
							testZero--;
							_parent["p_mcCalcul"+i].isok = false;
							_parent["p_mcCalcul"+i].mcPerso.gotoAndStop(1);
							for(var j = 0;j<max;j++){
								_parent["p_mcCalcul"+i].calcul["c"+j].selectable = true;	
								_parent["p_mcCalcul"+i].calcul["c"+j].text="";							
							}
						}
					}
					if(!tZero){
						_parent.soundPlayer.playASound("zero",0);
						_parent.myInterval = setInterval(_parent,"focus",100,_parent["p_mcCalcul"+i].calcul.c0);
					}					
				}
				_parent.testResultat();
			}
		}
		p_btnTwoAddition.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.p_mcThreeAddition._visible = false;
				_parent.p_mcTwoAddition._visible= true;
				_parent.p_mcSigne._visible= false;
				_parent.selectedGame = "TwoAddition";
			}			
		}
		p_btnThreeAddition.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.p_mcThreeAddition._visible = true;
				_parent.p_mcTwoAddition._visible= false;
				_parent.p_mcSigne._visible= false;
				_parent.selectedGame = "ThreeAddition";
			}			
		}
		p_btnSigne.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.p_mcThreeAddition._visible = false;
				_parent.p_mcTwoAddition._visible= false;
				_parent.p_mcSigne._visible= true;
				_parent.selectedGame = "Signe";
			}			
		}
		for(var i=0;i<4;i++){
			var mc = this["p_mcCalcul"+i];
			mc.calcul._visible = false;
		}
		this.myIntervalTab = undefined;
		var keyListener:Object = new Object();
		keyListener.ref = this;
		keyListener.onKeyDown = function() {
			var re = this;
			//Key.getCode() == Key.ENTER || 
			if (Key.getCode() == Key.TAB) {
				if(re.ref.p_isPlaying && re.ref.myIntervalTab == undefined){
					re.ref.getNextFocus();	
				}
			} 
		};		
		Key.addListener(keyListener);
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			p_mcThreeAddition._visible = true;
			p_mcTwoAddition._visible= false;
			p_mcSigne._visible= false;
			selectedGame = "ThreeAddition";
		}else if(nbr == 1){
			p_mcThreeAddition._visible = false;
			p_mcTwoAddition._visible= false;
			p_mcSigne._visible= true;
			selectedGame = "Signe";
		}else{
			p_mcThreeAddition._visible = false;
			p_mcTwoAddition._visible= true;
			p_mcSigne._visible= false;
			selectedGame = "TwoAddition";
		}
		p_mcCat._visible = false;
	}
	public function switchExercice(val:Boolean):Void{
		p_txtStart.text = "Départ";
		this.p_mcCat._visible = true;
		if(this.selectedGame=="TwoAddition"){
			p_mcSigne._visible= false;
			p_mcThreeAddition._visible= true;
			p_mcTwoAddition._visible= false;
			this.selectedGame = "ThreeAddition";
			if(!val){
				this.p_mcCoches.mc1._visible = true;
			}
		}else if(this.selectedGame=="ThreeAddition"){
			p_mcSigne._visible= true;
			p_mcThreeAddition._visible= false;
			p_mcTwoAddition._visible= false;
			this.selectedGame = "Signe";
			if(!val){
				this.p_mcCoches.mc2._visible = true;
			}
		}else{
			p_mcSigne._visible= false;
			p_mcThreeAddition._visible= false;
			p_mcTwoAddition._visible= true;
			this.selectedGame = "TwoAddition";
			if(!val){
				this.p_mcCoches.mc3._visible = true;
			}
		}
	}
	public function getNextFocus():Void{
		var spli = Selection.getFocus();
		if(spli.indexOf("p_mcCalcul")<0){
			spli = this.oldSel._name;
		}
		var mysel = spli.split(".");
		var sel = undefined;
		var nb = Number(mysel[mysel.length-1].substring(1,2));
		var nbMc = Number(mysel[mysel.length-3].substring(10,11));
		
		
		if(this.selectedGame == "ThreeAddition"){
			if(nb==2){
				if(nbMc == 3) {
					nbMc=0
				}else{
					nbMc++;
				}
				nb = -1;
			}
		}else if(this.selectedGame == "TwoAddition"){
			if(nb==1){
				if(nbMc == 3) {
					nbMc=0
				}else{
					nbMc++;
				}
				nb = -1;
			}
		}
		sel = this["p_mcCalcul"+nbMc].calcul["c"+(nb+1)];
		this.oldSel = sel;
		
		this.myIntervalTab = setInterval(this,"focusTab",50,sel);
		trace(sel);
	}
	private function focusTab(sel):Void{
		clearInterval(this.myIntervalTab);
		this.myIntervalTab = undefined;
		Selection.setFocus(sel);
	}
	public function testDoublon(mcTest:MovieClip):Boolean{
		//test les doublons
		var testDoublon = true;
		var tabDoublon = new Array();
		var max = 3
		if(this.selectedGame == "TwoAddition"){
			max = 2;
		}
		for(var i=0;i<=this.actualCalculTested;i++){
			var mc = this["p_mcCalcul"+i];			
			tabDoublon[i] = new Array();
			for(var j = 0;j<max;j++){
					tabDoublon[i].push(mc.calcul["c"+j].text);								
			}
		}

		var t = new Array();
		for(var j = 0;j<max;j++){
				t.push(mcTest.calcul["c"+j].text);								
		}
		for(var j=0;j<tabDoublon.length-1;j++){
			if(i!=j){
				var te = 0;
				for(var k in t){
					if(t[k] == tabDoublon[j][k]) te++;
				}
				if(te == t.length) {
					testDoublon=false;
				}
			}
		}
		if(!testDoublon){
			this.soundPlayer.playASound("plusieursX",0);
			for(var j = 0;j<max;j++){
				mcTest.calcul["c"+j].text="";							
			}
			this.myInterval = setInterval(_parent,"focus",100,mcTest.calcul.c0);
			this.myIntervalResult = setInterval(this,"testCalcul",2300);
		}
		return testDoublon;
	}
	public function testResultat():Void{
		this.testResult = true;
		this.testComplet = 0;
		this.myIntervalResult = setInterval(this,"testCalcul",1000);
		this.actualCalculTested = 0;
	}
	private function testCalcul(){
		clearInterval(this.myIntervalResult);
		if(this.actualCalculTested == 4){
			if(this.testResult){
				if(this.testComplet == 4){
					if(this.p_mcResults.modifyGoodResult()){								
						this.p_isPlaying=false;
						p_txtStart.text = "Départ";
						//_parent.p_txtStart.textColor = 0xCCCCCC;
						
						this.p_mcCat._visible = true;
						//this.useHandCursor = false;
						
						this.p_btnTwoAddition.useHandCursor = true;
						this.p_btnThreeAddition.useHandCursor = true;
						this.p_btnSigne.useHandCursor = true;
					}else{
						this.initializeGameInterface();
					}
				}
			}else{
				this.p_mcResults.modifyFalseResult();
			}
		}else{
			var mc = this["p_mcCalcul"+this.actualCalculTested];
			
			var tComplet = true;
			if(this.selectedGame != "Signe"){
				var somme = 0;
				var max = 2;
				if(this.selectedGame == "TwoAddition"){
					max = 1;
				}
				for(var j = 0;j<=max;j++){
					somme+=Number(mc.calcul["c"+j].text);
					if(mc.calcul["c"+j].text == ""){
						tComplet = false;
					}
				}
				if(tComplet){
					if(testDoublon(mc)){
						this.testComplet++;
						if(!mc.isok){
							this.soundPlayer.playASound("clapet2",0);
							trace(somme+":"+this.selectedNbr);
							if(somme != this.selectedNbr){
								this.testResult = false;
								mc.mcPerso.gotoAndPlay(3);
								for(var j = 0;j<3;j++){
									mc.calcul["c"+j].text="";							
								}
							}else{
								mc.isok = true;
								mc.mcPerso.gotoAndStop(2);
								for(var j = 0;j<3;j++){
									mc.calcul["c"+j].selectable = false;							
								}
							}
							this.myIntervalResult = setInterval(this,"testCalcul",1000);
						}else{
							this.myIntervalResult = setInterval(this,"testCalcul",10);
						}
					}else{
						mc.mcPerso.gotoAndStop(1);
					}
				}else{
					this.myIntervalResult = setInterval(this,"testCalcul",100);
				}
			}else{
				for(var j=1;j<=2;j++){
					if(mc.calcul["clic"+j]._currentframe == 1){
						tComplet = false;;
					}
				}
				if(tComplet){					
					this.testComplet++;
					if(!mc.isok){						
						this.soundPlayer.playASound("clapet2",0);
						var somme = true;
						if(mc.calcul.clic1._currentframe == 4){
							if(mc.calcul.clic2._currentframe == 4){
								somme = false
							}else{
								if(mc.calcul.clic2._currentframe == 2){
									if(Number(mc.calcul.c0.text) != Number(mc.calcul.c1.text)+Number(mc.calcul.total.text)){
										somme = false
									}
								}else if(mc.calcul.clic2._currentframe == 3){
									if(Number(mc.calcul.c0.text) != Number(mc.calcul.c1.text)-Number(mc.calcul.total.text)){
										somme = false;
									}
								}
							}
						}else if(mc.calcul.clic2._currentframe == 4){
							if(mc.calcul.clic1._currentframe == 4){
								somme = false
							}else{
								if(mc.calcul.clic1._currentframe == 2){
									if(Number(mc.calcul.c0.text) + Number(mc.calcul.c1.text) != Number(mc.calcul.total.text)){
										somme = false
									}
								}else if(mc.calcul.clic1._currentframe == 3){
									if(Number(mc.calcul.c0.text) - Number(mc.calcul.c1.text) != Number(mc.calcul.total.text)){
										somme = false;
									}
								}
							}
						}else{
							somme = false;
						}
						if(somme){
							for(var j=1;j<=2;j++){
								mc.calcul["clic"+j].enabled = false;
							}
							mc.isok = true;
							mc.mcPerso.gotoAndStop(2);
							mc.calcul.plus0.selectable = false;							
						}else{
							this.testResult = false;
							mc.mcPerso.gotoAndPlay(3);
						}
						this.myIntervalResult = setInterval(this,"testCalcul",1000);
					}else{
						this.myIntervalResult = setInterval(this,"testCalcul",10);
					}
				}else{
					this.myIntervalResult = setInterval(this,"testCalcul",10);
				}
			}
			this.actualCalculTested++;
		}
	}
	public function focus(texteFocus):Void{
		clearInterval(this.myInterval);
		Selection.setFocus(texteFocus);
	}
	public function initializeGameInterface():Void{
		actualTryExo = 1;
		this.p_mcResults.checkResultAndReset();
		this.selectedNbr= random(10)+5;
		for(var i=0;i<4;i++){
			var mc = this["p_mcCalcul"+i];
			mc.isok = false;
			mc.mcPerso.gotoAndStop(1);
			if(this.selectedGame == "ThreeAddition"){
				mc.calcul.gotoAndStop(2);
				mc.bg.gotoAndStop(2);
			}else if(this.selectedGame == "Signe"){
				mc.calcul.gotoAndStop(3);
				mc.bg.gotoAndStop(3);
			}else{
				mc.calcul.gotoAndStop(1);
				mc.bg.gotoAndStop(3);
			}
			mc.calcul._visible = true;			
			if(this.selectedGame == "Signe"){
				for(var j=1;j<=2;j++){
					mc.calcul["clic"+j].enabled = true;
					mc.calcul["clic"+j].gotoAndStop(1);
					mc.calcul["clic"+j].ref = this;
					mc.calcul["clic"+j].onPress = function(){
						var re = this;
						re.ref.soundPlayer.playASound("btnClick",0);
						if(re._currentframe != re._totalframes){
							re.gotoAndStop(re._currentframe+1)
						}else{
							re.gotoAndStop(2);
						}
					}
				}
				
				var r1 = random(7)+1;
				var r2 = random(7)+1;
				mc.calcul.c0.text = r1
				mc.calcul.c1.text = r2;
				var somme = 0;
				if(r1>=r2){
					var test = Math.random();
					if(test>0.5){
						somme = r1-r2;
					}else{
						somme = r1+r2;
					}
				}else{
					somme = r1+r2;
				}
				mc.calcul.total.text = somme;				
			}else{
				mc.calcul.plus0.text ="+";
				mc.calcul.plus1.text = "+";
				mc.calcul.egal.text = "=";
				for(var j = 0;j<3;j++){
					mc.calcul["c"+j].selectable = true;	
					mc.calcul["c"+j].text = "";
				}
				mc.calcul.total.text = selectedNbr;
			}
		}
		if(this.selectedGame == "Signe"){
			this.myInterval = setInterval(this,"focus",100,this.p_mcCalcul0.calcul.plus0);
		}else{
			this.myInterval = setInterval(this,"focus",100,this.p_mcCalcul0.calcul.c0);
		}
		
	}
}

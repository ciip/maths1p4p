﻿/**
 * 
 * class com.maths1p4p.level1.game1_10.game1_10
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * ETAT : 
 * stable
 * 
 * TODO :
 * -> sons mais dn
 * -> Grer les rsultats impossibles
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level1.game1_10.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_10.Game1_10 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	
	public var p_txtStart:TextField;
	public var p_txtName:TextField;	
	

	public var p_btnStart:Button;
	public var p_btn10:Button;
	public var p_btn15:Button;
	public var p_btn20:Button;


	
	public var p_mc10:Button;
	public var p_mc15:Button;
	public var p_mc20:Button;
	
	public var p_isPlaying:Boolean;
	
	public var p_mcNumbers:MovieClip;
	public var lastSelected:MovieClip;

	public var p_mcCat:MovieClip;
	
	
	public var p_mcCalcul0:MovieClip;
	public var p_mcCalcul1:MovieClip;
	public var p_mcCalcul2:MovieClip;
	public var p_mcCalcul3:MovieClip;
	public var p_mcCoches:MovieClip;
	

	public var p_mcResults:level1_resultat;


	public var actualTry:Number;
	public var actualTryExo:Number;
	public var selectedGame:Number;
	public var nameGames:Array = [10,15,20];

	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	private var myInterval:Number;
	private var nbrTry:Number;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_10()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 0;
		actualTryExo = 0;
		p_txtStart.text = "Départ";
		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		p_mcCoches.mc3._visible = false;


		
		selectedGame = 10;

		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		
		
		nbrTry = 4;//4
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		p_btnStart.onPress = function(){
			_parent.soundPlayer.playASound("btnClick",0);
			if(!_parent.p_isPlaying){
				_parent.p_mcResults.checkResultAndReset();
				_parent.initializeGameInterface();
				_parent.p_isPlaying=true;
				_parent.p_txtStart.textColor = 0xCCCCCC;
				
				_parent.p_mcCat._visible = false;
				this.useHandCursor = false;
				
				_parent.p_btn10.useHandCursor = false;
				_parent.p_btn15.useHandCursor = false;
				_parent.p_btn20.useHandCursor = false;
			}
		}
		p_btn10.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mc15._visible = false;
				_parent.p_mc10._visible= true;
				_parent.p_mc20._visible= false;
				_parent.selectedGame = 10;
			}			
		}
		p_btn15.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mc15._visible = true;
				_parent.p_mc10._visible= false;
				_parent.p_mc20._visible= false;
				_parent.selectedGame = 15;
			}			
		}
		p_btn20.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mc15._visible = false;
				_parent.p_mc10._visible= false;
				_parent.p_mc20._visible= true;
				_parent.selectedGame = 20;
			}			
		}
		for(var i=0;i<4;i++){
			var mc = this["p_mcCalcul"+i];
			mc.actGame = i;
			for(var j=0;j<6;j++){
				var mcBtn = mc["btn"+j];
				mcBtn.useHandCursor = false;
				mcBtn.ref = this;
				mcBtn.nbr = j;
				mcBtn.selecte = false;
				mcBtn.onPress = function(){
					if(this.ref.actualTry == this._parent.actGame && this.ref.p_isPlaying){
						this.ref.soundPlayer.playASound("btnClick",0);
						if(!this.selecte){
							this._parent["c"+this.nbr].textColor = 0x000000;
							this.selecte = true;
						}else{
							this._parent["c"+this.nbr].textColor = 0xCCCCCC;
							this.selecte = false;
						}
						
					}
				}
			}
			mc.btnTotal.useHandCursor = false;
			mc.btnTotal.ref = this;
			mc.btnTotal.onPress = function(){
				if(this.ref.actualTry == this._parent.actGame && this.ref.p_isPlaying){
					this.ref.soundPlayer.playASound("btnClick",0);
					var test = 0;
					for(var j=0;j<6;j++){
						if(this._parent["btn"+j].selecte){
							test+=Number(this._parent["c"+j].text);
						}
					}
					if(test == this.ref.selectedGame){
						if(this.ref.p_mcResults.modifyGoodResult()){
							this.ref.p_isPlaying = false;
							this.ref.p_btnStart.useHandCursor = true;
							this.ref.p_txtStart.textColor = 0x000000;
							this.ref.p_btn10.useHandCursor = true;
							this.ref.p_btn15.useHandCursor = true;
							this.ref.p_btn20.useHandCursor = true;
						}
						this.ref.initBtn();
					}else{
						this.ref.p_mcResults.modifyFalseResult();
					}
				}
			}
		}
		
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			p_mc15._visible = true;
			p_mc10._visible= false;
			p_mc20._visible= false;
			selectedGame = 15;
		}else if(nbr == 1){
			p_mc15._visible = false;
			p_mc10._visible= false;
			p_mc20._visible= true;
			selectedGame = 20;
		}else{
			p_mc15._visible = false;
			p_mc10._visible= true;
			p_mc20._visible= false;
			selectedGame = 10;
		}
		p_mcCat._visible = false;
	}
	public function switchExercice(val:Boolean):Void{

		this.p_mcCat._visible = true;
		if(this.selectedGame==10){
			p_mc10._visible= false;
			p_mc15._visible= true;
			p_mc20._visible= false;
			this.selectedGame = 15;
			if(!val){
				this.p_mcCoches.mc1._visible = true;
			}
		}else if(this.selectedGame==15){
			p_mc10._visible= false;
			p_mc15._visible= false;
			p_mc20._visible= true;
			this.selectedGame = 20;
			if(!val){
				this.p_mcCoches.mc2._visible = true;
			}
		}else{
			p_mc10._visible= true;
			p_mc15._visible= false;
			p_mc20._visible= false;
			this.selectedGame = 10;
			if(!val){
				this.p_mcCoches.mc3._visible = true;
			}
		}
	}
	public function initializeGameInterface():Void{
		actualTryExo = 1;
		this.p_mcResults.checkResultAndReset();
		
		var max = 2;
		var tabOk = new Array();
		for(var i=0;i<4;i++){
			var tab = [0,1,2,3,4,5];
			tabOk.length = 0;
			var div = 2*Math.floor(this.selectedGame/3);
			var numRand1 = 1+random(this.selectedGame-div);
			var numRand2 = 1+random(this.selectedGame-div);
			var numRand3 = this.selectedGame-numRand1-numRand2;
			tabOk = [numRand1,numRand2,numRand3];
			var mc = this["p_mcCalcul"+i];
			mc.total.text = this.selectedGame;
			mc.total.textColor = 0x000000;
			for(var j=0;j<6;j++){
				var mcN = mc["c"+j];
				mcN.text = 1+random(this.selectedGame-5);
				mcN.textColor = 0xCCCCCC;
				var mcBtn = mc["btn"+j];
				mcBtn.selecte = false;
			}
			trace(tabOk);
			for(var j=0;j<tabOk.length;j++){
				var r = Number(tab.splice(random(tab.length),1).toString());
				var mcN = mc["c"+r];
				mcN.text = tabOk[j];
			}
		}
		this.initBtn();
	}
	public function initBtn():Void{
		for(var i=0;i<4;i++){
			var mc = this["p_mcCalcul"+i];
			if(i!=this.actualTry){
				for(var j=0;j<6;j++){
					var mcBtn = mc["btn"+j];
					mcBtn.useHandCursor = false;
				}
				mc.btnTotal.useHandCursor = false;
			}else{
				for(var j=0;j<6;j++){
					var mcBtn = mc["btn"+j];
					mcBtn.useHandCursor = true;
				}
				mc.btnTotal.useHandCursor = true;
			}
		}
	}
}

﻿/**
 * 
 * class com.maths1p4p.level1.game1_11.game1_11
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level1.game1_11.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_11.Game1_11 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var p_txtStart:TextField;
	public var p_txtName:TextField;	
	public var p_txtTotal:TextField;
	
	

	public var p_btnStart:Button;
	public var p_btn10:Button;

	public var p_btn20:Button;


	
	public var p_mc10:Button;
	public var p_mc20:Button;
	
	public var p_isPlaying:Boolean;
	
	public var lastSelected:MovieClip;

	public var p_mcCat:MovieClip;	
	public var p_mcSquares:MovieClip;
	public var p_mcPuzzles:MovieClip;
	public var p_mcNumbers:MovieClip;
	public var p_mcCoches:MovieClip;
	

	public var p_mcResults:level1_resultat;


	public var selectedGame:Number;
	public var nameGames:Array = [10,20];
	public var selectedNumber:Number;
	

	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	private var myInterval:Number;
	private var nbrTry:Number;
	private var actualTry:Number;
	private var isBomb:Boolean;
	private var asMadeError:Boolean;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_11()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 5;
		p_txtStart.text = "Départ";
		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		
		//create Resultat
		
		isBomb = true;
		nbrTry = 5;//5
		p_mcResults = new level1_resultat(this,this.nbrTry);


		p_mcSquares = this.createEmptyMovieClip("p_mcSquares",4);
		for(var i=0;i<12;i++){
			var mc = p_mcSquares.attachMovie("mcSquare","square"+i,i+1);
			mc._x = 46+(i-Math.floor(i/4)*4)*67;
			mc._y = 70+Math.floor(i/4)*66;
		}
		
		p_mcNumbers = this.createEmptyMovieClip("p_mcSquares",5);
		for(var i=0;i<12;i++){
			var mc = p_mcNumbers.attachMovie("mcBtnNumber","btn"+i,i+1);
			mc._x = 56+(i-Math.floor(i/6)*6)*73;
			mc._y = 346+Math.floor(i/6)*36;
			mc.xinit = mc._x;
			mc.yinit = mc._y;
			mc.ref = this;
			mc.onPress = function(){
				if(this.ref.p_isPlaying){
					
					this.startDrag(false,18,22,522,426);
					this.onDrag = true;
				}else{
					this.onDrag = false;
				}
			}
			mc.onRelease = mc.onReleaseOutside = function(){
				if(this.ref.p_isPlaying && this.onDrag){
					
					this.onDrag = false;
					this.stopDrag();
					var test = false;
					var okDrag = false;
					for(var i =0;i<12;i++){
						var mcHit = this.ref.p_mcSquares["square"+i];
						var te = this._droptarget+"/";
						if(te.indexOf("/square"+i+"/")>=0){
							okDrag = true;
							if(mcHit.somme == this.somme){
								test = true;
								this.ref.soundPlayer.playASound("btnClick",0);
								trace("yes");
								this._visible = false;
								mcHit._visible = false;
								this.ref.p_txtTotal.text = this.texte.text+" = "+mcHit.texte.text;
								var end = true;
								for(var j =0;j<12;j++){
									if(this.ref.p_mcSquares["square"+j]._visible){
										end = false;
									}
								}
								if(end){
									this.ref.p_isPlaying = false;
									this.ref.p_btnStart.enabled = true;
									this.ref.p_mcCat._visible = true;
									this.ref.exoOK();
								}
							}											
						}
					}
					if(!test){
						this._x = this.xinit;
						this._y = this.yinit;
						if(okDrag){
							this.ref.testResult(false);
						}
					}
				}
			}
		}
		makeButtons(false);
		
		p_btnStart.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.p_mcCat._visible = false;
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.initializeGameInterface();
				_parent.p_isPlaying=true;
				
				_parent.p_mcCat._visible = false;				
				_parent.p_btn10.enabled = false;
				_parent.p_btn20.enabled = false;
				
				this.enabled = false;
			}
		}
		p_btn10.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mc10._visible= true;
				_parent.p_mc20._visible= false;
				_parent.selectedGame = 10;
			}			
		}
		
		p_btn20.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mc10._visible= false;
				_parent.p_mc20._visible= true;
				_parent.selectedGame = 20;
			}			
		}		
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			p_mc10._visible= false;
			p_mc20._visible= true;
			selectedGame = 20;
		}else{
			p_mc10._visible= true;
			p_mc20._visible= false;
			selectedGame = 10;
		}
		p_mcCat._visible = false;
	}
	public function exoOK():Void{
		this.p_mcResults.modifyGoodResult();
	}
	public function switchExercice():Void{

		this.p_mcCat._visible = true;
		if(this.selectedGame==10){
			p_mc10._visible= false;
			p_mc20._visible= true;
			this.selectedGame = 20;
			if(!asMadeError){
				this.p_mcCoches.mc1._visible = true;
			}
		}else{
			p_mc10._visible= true;
			p_mc20._visible= false;
			this.selectedGame = 10;
			if(!asMadeError){
				this.p_mcCoches.mc2._visible = true;
			}
		}
	}
	public function testResult(val:Boolean):Void{
		if(!val){
			asMadeError = true;
			if(this.p_mcResults.modifyFalseResult()){
				this.myInterval=setInterval(this,"reInitGame",3000);
			}else{
				this.myInterval=setInterval(this,"reInitAfterError",1000);
			}
			p_isPlaying = false;
		}
	}
	private function reInitGame():Void{
		clearInterval(this.myInterval);
		p_isPlaying = true;
		initializeGameInterface();
	}
	private function reInitAfterError():Void{
		clearInterval(this.myInterval);
		p_isPlaying = true;
	}
	public function makeButtons(val){
		for(var i=0;i<12;i++){
			var mc = p_mcNumbers["btn"+i];
			mc.enabled = val;
		}
	}
	public function initializeGameInterface():Void{
		if(this.p_mcResults.checkResultAndReset()){
			this.p_mcResults.reinitializeResult();
		}
		asMadeError = false;
		p_mcPuzzles.gotoAndStop(random(6)+1);
		makeButtons(true);
		this.p_mcCat._visible = false;
		var tabMC = new Array();
		for(var i=0;i<12;i++){
			tabMC.push(i);
		}
		for(var i=0;i<12;i++){
			if(this.selectedGame == 10){
				var somme = 6+random(15);
				var r1 = 2+random(4);
				var r2 = 5+random(10);
			}else{
				var somme = 4+random(7);
				var r1 = 2+random(3);
				var r2 = 3+random(2);
			}
			var t = tabMC.splice(random(tabMC.length),1);
			trace(t);
			var mc = p_mcNumbers["btn"+t];
			mc.texte.text = r1+"+"+(somme-r1);
			mc.somme = somme;
			mc._visible = true;
			mc._x = mc.xinit;
			mc._y = mc.yinit;
			var mcP = p_mcSquares["square"+i];
			mcP.somme = somme;
			mcP._visible = true;			
			if(this.selectedGame == 10){			
				mcP.texte.text = somme;				
			}else{
				mcP.texte.text = r2+"+"+(somme-r2);	
			}
		}
		p_txtTotal.text = "";
	}
}

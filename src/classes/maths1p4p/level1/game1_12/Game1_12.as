﻿/**
 * 
 * class com.maths1p4p.level1.game1_12.game1_12
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level1.game1_12.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_12.Game1_12 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	
	public var p_txtStart:TextField;
	public var p_txtName:TextField;	
	

	public var p_btnStart:Button;
	public var p_btn10:Button;

	public var p_btn20:Button;


	
	public var p_mc10:Button;
	public var p_mc20:Button;
	
	public var p_isPlaying:Boolean;
	
	public var lastSelected:MovieClip;

	public var p_mcCat:MovieClip;	
	public var p_mcStar:MovieClip;
	public var p_mcCoches:MovieClip;
	

	public var p_mcResults:level1_resultat;


	public var actualTry:Number;
	public var actualTryExo:Number;
	public var selectedGame:Number;
	public var nameGames:Array = [10,20];
	public var selectedNumber:Number;
	public var myInterval:Number;

	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	
	private var nbrTry:Number;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_12()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 0;
		actualTryExo = 0;
		p_txtStart.text = "Départ";
		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		this.myInterval = undefined;

		p_mcStar._visible = false;

		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		
		
		nbrTry = 4;//4
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		p_btnStart.onPress = function(){
			_parent.soundPlayer.playASound("btnClick",0);
			if(!_parent.p_isPlaying){
				_parent.p_mcResults.checkResultAndReset();
				_parent.initializeGameInterface();
				_parent.p_isPlaying=true;
				
				_parent.p_mcCat._visible = false;
				
				_parent.p_btn10.enabled = false;

				_parent.p_btn20.enabled = false;
			}else{
				var testComplet = true;
				var i=0;
				do{
					if(_parent.p_mcStar["c"+i].text ==""){
						testComplet=false;
						var texteFocus = _parent.p_mcStar["c"+i];
					}
					i++;
				}while (testComplet && i<10)
				if(testComplet){
					var testDoublon = true;
					var tabDoublon = new Array();
					for(var i=0;i<=4;i++){
						tabDoublon[i] = [_parent.p_mcStar["c"+i].text,_parent.p_mcStar["c"+(i+5)].text];							
					}
					var i =4;
					do{
						var t = tabDoublon[i];
						for(var j=0;j<=4;j++){
							if(i!=j){
								var te = 0;
								if(t[0] == tabDoublon[j][0] || t[0] == tabDoublon[j][1]) te++;
								if(t[1] == tabDoublon[j][0] || t[1] == tabDoublon[j][1]) te++;
								if(te == t.length) {
									testDoublon=false;
									var mcDoublon = i;
								}
							}
						}
						i--;
					}while(testDoublon && i>=0)
					if(testDoublon){
						var testResult = true;
						for(var i=0;i<=4;i++){
							var somme  = Number(_parent.p_mcStar["c"+i].text)+Number(_parent.p_mcStar["c"+(i+5)].text)
							if(somme != _parent.selectedNumber){
								_parent.p_mcStar["c"+i].text="";
								_parent.p_mcStar["c"+(i+5)].text="";
								if(_parent.myInterval == undefined){
									_parent.myInterval = setInterval(_parent,"focus",100,_parent.p_mcStar["c"+i]);
								}
								testResult = false;
							}
						}
						if(testResult){
							if(!_parent.p_mcResults.modifyGoodResult()){								
								_parent.initializeGameInterface();
							}
						}else{
							_parent.p_mcResults.modifyFalseResult();
						}						
					}else{
						_parent.soundPlayer.playASound("choixnb",0);						
						_parent.myInterval = setInterval(_parent,"focus",100,_parent.p_mcStar["c"+mcDoublon]);
					}
				}else{
					_parent.soundPlayer.playASound("rempCases",0);
					_parent.myInterval = setInterval(_parent,"focus",100,texteFocus);
				}
			}
		}
		p_btn10.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.p_mc10._visible= true;
				_parent.p_mc20._visible= false;
				_parent.selectedGame = 10;
			}			
		}
		
		p_btn20.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.p_mc10._visible= false;
				_parent.p_mc20._visible= true;
				_parent.selectedGame = 20;
			}			
		}
		var keyListener:Object = new Object();
		keyListener.ref = this;
		keyListener.onKeyDown = function() {
			var re = this;
			//Key.getCode() == Key.ENTER || 
			if (Key.getCode() == Key.ENTER) {
				if(re.ref.p_isPlaying && re.ref.myInterval == undefined){
					re.ref.soundPlayer.playASound("btnClick",0);
					re.ref.getNextFocus();	
				}
			} 
			if (Key.getCode() == Key.TAB) {
				if(re.ref.p_isPlaying && re.ref.myInterval == undefined){
					re.ref.getNextTabFocus();	
				}
			} 
		};		
		Key.addListener(keyListener);
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			p_mc10._visible= false;
			p_mc20._visible= true;
			selectedGame = 20;
		}else{
			p_mc10._visible= true;
			p_mc20._visible= false;
			selectedGame = 10;
		}
		p_mcCat._visible = false;
	}
	public function switchExercice(val:Boolean):Void{
		
		p_txtStart.text = "Départ";
		p_isPlaying=false;
		//p_txtStart.textColor = 0xCCCCCC;		
		p_mcCat._visible = true;
		//this.enabled = false;		
		p_btn10.enabled = true;
		p_btn20.enabled = true;
		
		if(this.selectedGame==10){
			p_mc10._visible= false;
			p_mc20._visible= true;
			this.selectedGame = 20;
			if(!val){
				this.p_mcCoches.mc1._visible = true;
			}
		}else{
			p_mc10._visible= true;
			p_mc20._visible= false;
			this.selectedGame = 10;
			if(!val){
				this.p_mcCoches.mc2._visible = true;
			}
		}
	}
	public function initializeGameInterface():Void{
		actualTryExo = 1;
		p_txtStart.text = "J'ai fini";
		this.p_mcResults.checkResultAndReset();
		this.selectedNumber = this.selectedGame+random(10);
		p_mcStar.total.text = this.selectedNumber;
		for(var i=0;i<10;i++){
			p_mcStar["c"+i].text = "";
		}
		p_mcStar._visible = true;
		this.myInterval = setInterval(this,"focus",100,p_mcStar.c0);
		
	}
	public function focus(texteFocus):Void{
		clearInterval(this.myInterval);
		this.myInterval = undefined;
		Selection.setFocus(texteFocus);
	}
	public function getNextFocus():Void{
		var mysel = Selection.getFocus().split(".");
		var nb = Number(mysel[mysel.length-1].substring(1,2));
		if(nb<5){
			var sel = p_mcStar["c"+(nb+5)];
		}else{
			var sel = p_mcStar["c"+(nb-5)];
		}
		this.myInterval = setInterval(this,"focus",100,sel);
	}
	public function getNextTabFocus():Void{
		var mysel = Selection.getFocus().split(".");
		var nb = Number(mysel[mysel.length-1].substring(1,2))+1;
		trace(nb);
		if(nb==10)nb=0;
		var sel = p_mcStar["c"+nb];
		this.myInterval = setInterval(this,"focus",10,sel);
	}
}

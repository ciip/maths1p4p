﻿/**
 * 
 * class com.maths1p4p.level1.game1_13.game1_13
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level1.game1_13.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_13.Game1_13 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	public var p_txtName:TextField;
	public var p_txtStart:TextField;
	
	

	public var p_btnStart:Button;
	
	public var mcDisplay:MovieClip;
	
	public var p_isPlaying:Boolean;
	


	public var p_mcResults:level1_resultat;
	public var actualTry:Number;
	public var nbrTry:Number;
	public var actualTryExoGame13:Number;
	public var selectedGame:Number;
	public var nameGames:Array = [1,2,3,4,5];
	public var myInterval:Number;
	
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var selectedAnswerField:TextField;
	
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_13()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTryExoGame13 = 0;
		
		
		nbrTry = 1;
		p_mcResults = new level1_resultat(this,this.nbrTry);
		trace(">>>>>>>>>>>"+selectedGame);
		
		p_txtStart.text = "Départ";
		var keyListener:Object = new Object();
		keyListener.ref = this;
		keyListener.onKeyDown = function() {
			var re = this;
			trace(Selection.getFocus());
			if (Key.getCode() == Key.ENTER || Key.getCode() == Key.SPACE) {				
				if(re.ref.p_isPlaying && re.ref.myInterval == undefined){
					re.ref.testResult();
				}
			} 
		};		
		Key.addListener(keyListener);
		
		var mouseListener:Object = new Object();
		mouseListener.ref = this;
		mouseListener.onMouseDown = function() {			
			var re = this;
			if(re.ref.p_btnStart._xmouse>0 && re.ref.p_btnStart._xmouse<83 && re.ref.p_btnStart._ymouse>0 && re.ref.p_btnStart._ymouse<34 ){
				if(re.ref.p_isPlaying && re.ref.myInterval == undefined){
					re.ref.testResult();
				}
			}
		};
		Mouse.addListener(mouseListener);
		if(selectedGame!=1){
			p_isPlaying = true;
			p_txtStart.text = "Suite";
			initializeGameInterface();			
		}
		
		p_btnStart.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_isPlaying = true;
				_parent.initializeGameInterface();
				_parent.p_txtStart.text = "Suite";
			}else if(_parent.myInterval == undefined){
				_parent.testResult();
			}
		}
		
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr != 4){
			selectedGame = nbr+2;
		}else{
			selectedGame = 1;
		}
		//selectedGame = 6;
	}
	public function switchExercice(val:Boolean):Void{
		//cohérence avec mc_results
	}
	public function testResult():Void{
		trace("test");
		actualTry = 0;
		var noInitDisplay = false;
		this.soundPlayer.playASound("btnClick",0);
		if(this.selectedGame == 1){
			if(Number(this.mcDisplay.c0.text)+Number(this.mcDisplay.c1.text)== Number(this.mcDisplay.total.text)){
				actualTryExoGame13++;				
				if(this.actualTryExoGame13>this.mcDisplay.img._totalframes){
					this.p_mcResults.modifyGoodResult();
					this.selectedGame++;
					noInitDisplay = true;
					this.initializeGameInterface();
					
				}
				this.soundPlayer.playASound("jeu"+this.selectedGame,0);
			}else{
				this.p_mcResults.modifyFalseResult();
				this.actualTryExoGame13--;
				this.actualTryExoGame13 = Math.max(this.actualTryExoGame13,1);
				noInitDisplay = true;
				this.mcDisplay.total.text = "";
				this.myInterval = setInterval(this,"focus",10,this.mcDisplay.total);
				var t = Math.floor(this.actualTryExoGame13/3)+1;
				this.soundPlayer.playASound("faux"+t,0);
			}
			this.mcDisplay.img.gotoAndStop(Math.min(this.actualTryExoGame13,this.mcDisplay.img._totalframes));
		}else if(this.selectedGame == 2){
			if(Number(this.mcDisplay.c0.text)+Number(this.mcDisplay.c1.text)+Number(this.mcDisplay.c2.text)== Number(this.mcDisplay.total.text)){
				var t = true;
				for(var i=0;i<3;i++){
					if(Number(this.mcDisplay["c"+i].text) == 0){
						t = false;
						this.mcDisplay["c"+i].text = "";
					}
				}
				if(t){
					actualTryExoGame13++;				
					if(this.actualTryExoGame13>this.mcDisplay.img._totalframes){
						this.p_mcResults.modifyGoodResult();
						this.selectedGame++;
						noInitDisplay = true;
						this.initializeGameInterface();
					}
					this.soundPlayer.playASound("jeu"+this.selectedGame,0);
				}else{
					for(var i=0;i<3;i++){
						this.mcDisplay["c"+i].text = "";
					}
					noInitDisplay = true;
					this.soundPlayer.playASound("zz",0);
				}
			}else{
				this.p_mcResults.modifyFalseResult();
				this.actualTryExoGame13--;
				this.actualTryExoGame13 = Math.max(this.actualTryExoGame13,1);
				noInitDisplay = true;
				this.mcDisplay.c0.text = "";
				this.mcDisplay.c1.text = "";
				this.mcDisplay.c2.text = "";
				this.myInterval = setInterval(this,"focus",10,this.mcDisplay.c0);
				var t = Math.floor(this.actualTryExoGame13/3)+1;
				this.soundPlayer.playASound("faux"+t,0);
			}
			this.mcDisplay.img.gotoAndStop(Math.min(this.actualTryExoGame13,this.mcDisplay.img._totalframes));
		}else if(this.selectedGame == 3 || this.selectedGame == 4){
			if(Number(this.mcDisplay.c0.text)-Number(this.mcDisplay.c1.text)== Number(this.mcDisplay.total.text)){
				actualTryExoGame13++;				
				if(this.actualTryExoGame13>this.mcDisplay.img._totalframes){
					this.p_mcResults.modifyGoodResult();
					this.selectedGame++;
					noInitDisplay = true;
					this.initializeGameInterface();
				}
				this.soundPlayer.playASound("jeu"+this.selectedGame,0);
			}else{
				this.p_mcResults.modifyFalseResult();
				this.actualTryExoGame13--;
				this.actualTryExoGame13 = Math.max(this.actualTryExoGame13,1);
				noInitDisplay = true;
				this.mcDisplay.total.text = "";
				this.myInterval = setInterval(this,"focus",10,this.mcDisplay.total);
				var t = Math.floor(this.actualTryExoGame13/3)+1;
				this.soundPlayer.playASound("faux"+t,0);
			}
			this.mcDisplay.img.gotoAndStop(Math.min(this.actualTryExoGame13,this.mcDisplay.img._totalframes));
		}else if(this.selectedGame == 5){
			if(Number(this.mcDisplay.c0.text)+Number(this.mcDisplay.c1.text)==Number(this.mcDisplay.c2.text)+Number(this.mcDisplay.c3.text)){
				actualTryExoGame13++;			
				if(this.actualTryExoGame13>this.mcDisplay.img._totalframes){
					this.p_mcResults.modifyGoodResult();
					this.selectedGame++;
					this.initializeGameInterface();					
				}
				this.soundPlayer.playASound("jeu"+this.selectedGame,0);
			}else{
				this.p_mcResults.modifyFalseResult();
				this.actualTryExoGame13--;
				this.actualTryExoGame13 = Math.max(this.actualTryExoGame13,1);
				noInitDisplay = true;
				this.selectedAnswerField.text = "";
				this.myInterval = setInterval(this,"focus",10,this.selectedAnswerField);
				var t = Math.floor(this.actualTryExoGame13/3)+1;
				this.soundPlayer.playASound("faux"+t,0);
			}
			this.mcDisplay.img.gotoAndStop(Math.min(this.actualTryExoGame13,this.mcDisplay.img._totalframes));
		}else if(this.selectedGame == 6){
			this.myInterval = 0;
			noInitDisplay = true;
			var my_pj:PrintJob = new PrintJob();
			if (my_pj.start()){
				my_pj.addPage(this.mcDisplay.mcEnd, {xMin:0,xMax:870,yMin:0,yMax:559});
				my_pj.send();
			}
			delete my_pj;
		}
		if(!noInitDisplay){
			this.initDisplay();
		}
	}
	public function focus(texteFocus):Void{
		clearInterval(this.myInterval);
		this.myInterval = undefined;
		trace("clear");
		Selection.setFocus(texteFocus);
	}
	private function initDisplay():Void{
		this.myInterval = undefined;
		if(this.selectedGame == 1){
			this.mcDisplay.c0.text = 2+random(10);
			this.mcDisplay.c1.text = 2+random(10);
			this.mcDisplay.total.text = "";
			this.myInterval = setInterval(this,"focus",10,this.mcDisplay.total);
		}else if(this.selectedGame == 2){
			this.mcDisplay.c0.text = "";
			this.mcDisplay.c1.text = "";
			this.mcDisplay.c2.text = "";
			this.mcDisplay.total.text = 5+random(15);
			this.myInterval = setInterval(this,"focus",10,this.mcDisplay.c0);
		}else if(this.selectedGame == 3 || this.selectedGame == 4  ){
			var r1= 2+random(10);
			this.mcDisplay.c0.text = r1+random(10);
			this.mcDisplay.c1.text = r1;
			this.mcDisplay.total.text = "";
			this.myInterval = setInterval(this,"focus",10,this.mcDisplay.total);
		}else if(this.selectedGame == 5){
			var t = (this.actualTryExoGame13-1) - Math.floor((this.actualTryExoGame13-1)/4)*4;
			var somme = random(10)+16;
			var r1 = random(15)+1;
			var r2 = random(10)+1;
			this.mcDisplay.c0.text = r1;
			this.mcDisplay.c2.text = r2;
			this.mcDisplay.c1.text = somme - r1;
			this.mcDisplay.c3.text = somme - r2;
			for(var i=0;i<4;i++){
				if(i!=t){
					this.mcDisplay["c"+i].type = "dynamic";
					this.mcDisplay["c"+i].selectable = false;
				}else{
					this.mcDisplay["c"+i].text = "";
					this.mcDisplay["c"+i].selectable = true;
					this.mcDisplay["c"+i].type = "input";
				}
			}
			this.selectedAnswerField = this.mcDisplay["c"+t];
			this.myInterval = setInterval(this,"focus",10,this.selectedAnswerField);
		}else if(this.selectedGame == 6){
			p_txtStart.text = "Imprimer";
			this.mcDisplay.mcEnd.nom.text = _global.nameOfThePlayerLevel1;
		}
	}
	public function initializeGameInterface():Void{
		actualTryExoGame13 = 1;
		mcDisplay.gotoAndStop(this.selectedGame+1);
		this.myInterval = undefined;
		this.initDisplay();
	}
}

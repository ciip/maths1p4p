﻿/**
 * 
 * class com.maths1p4p.level1.game1_14.game1_14
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level1.game1_14.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_14.Game1_14 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	public var p_txtName:TextField;
	public var p_txtStart:TextField;
	public var p_txtEnd:TextField;
	
	

	public var p_btnStart:Button;
	public var p_btnEnd:Button;
	
	
	public var mcDisplay:MovieClip;
	public var mcDrop:MovieClip;
	public var p_mcCheck:MovieClip;
	public var p_mcCoches:MovieClip;
	public var p_mcCat:MovieClip;
	
	
	public var p_isPlaying:Boolean;
	


	public var actualTry:Number;
	public var actualTryExo:Number;
	public var p_mcResults:level1_resultat;
	
	
	public var selectedGame:String;
	public var nameGames:Array = ["jeu"];
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	private var myInterval:Number;
	private var nbrTry:Number;
	private var selectedNumber:Number
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_14()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){
		actualTry = 0;
		actualTryExo = 0;
		p_mcCat._visible = false;
		p_mcCoches.mc1._visible = false;
		p_txtStart.text = "Départ";
		p_txtEnd.text = "J'ai fini";
		p_txtEnd.textColor = 0xCCCCCC;
		p_btnEnd.useHandCursor = false;
		p_mcCheck._visible = false;
		
		
		selectedGame = "jeu";
		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		
		
		nbrTry = 4;//4
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		mcDrop = this.createEmptyMovieClip("mcDrop",2);
		for(var i =0; i<18;i++){
			var mc = this.mcDrop.attachMovie("mcTete","mc"+i,i+1);
			mc._x = 53+(i-Math.floor(i/6)*6)*53;
			mc._y = 118+Math.floor(i/6)*59;
			mc.selecte = undefined;
			mc.gotoAndStop(13);
		}
		
		
		mcDisplay = this.createEmptyMovieClip("mcDisplay",3);
		for(var i =0; i<12;i++){
			var mc = this.mcDisplay.attachMovie("mcTete","mc"+i,i+1);
			mc._x = 53+(i-Math.floor(i/6)*6)*53;
			mc._y = 340+Math.floor(i/6)*59;
			mc.xinit = mc._x;
			mc.yinit = mc._y;
			mc.gotoAndStop(i+1);
			mc.ref = this;
			mc.mcOnDrag = undefined;
			mc.onPress = function(){
				if(this.ref.p_isPlaying){
					this.startDrag(false);
					this.swapDepths(13);
				}
			}
			mc.onRelease = mc.onReleaseOutside = function(){
				if(this.ref.p_isPlaying){
					var re = this;
					this.stopDrag();
					var test = false;
					for(var i =0; i<18;i++){
						var mcHit = this.ref.mcDrop["mc"+i];
						if(this.hitTest(mcHit) && !test){
							this._x = mcHit._x;
							this._y = mcHit._y;
							test = true;
							this.ref.soundPlayer.playASound("btnClick",0);
							if(mcHit.selecte != undefined && mcHit.selecte!=this){
								mcHit.selecte._x = mcHit.selecte.xinit;
								mcHit.selecte._y = mcHit.selecte.yinit;
								mcHit.selecte.mcOnDrag = undefined;
							}							
							//if(re.mcOnDrag != undefined){
								re.mcOnDrag.selecte = undefined;
							//}
							mcHit.selecte = this;
							re.mcOnDrag = mcHit;
						}
					}
					if(!test){
						this._x = this.xinit;
						this._y = this.yinit;
					}
				}
			}
		}
		
		
		initCursor(false);
		
		p_btnStart.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.p_mcCat._visible = false;
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_isPlaying = true;
				_parent.initializeGameInterface();
				_parent.p_txtStart.textColor = 0xCCCCCC;
				_parent.p_txtEnd.textColor = 0x000000;
				_parent.p_btnEnd.useHandCursor = true;
				this.useHandCursor = false;
			}
		}
		p_btnEnd.onPress = function(){
			if(_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				if(_parent.testResult()){
					_parent.p_isPlaying = false;
					_parent.p_txtStart.textColor = 0x000000;
					_parent.p_txtEnd.textColor = 0xCCCCCC;
					_parent.p_btnStart.useHandCursor = true;
					this.useHandCursor = false;
					_parent.initCursor(false);
				}
			}
		}
		for(var i in this.p_mcCheck){
			var te = this.p_mcCheck[i]._name;
			if(te.indexOf("btn")>=0){
				var mc = this.p_mcCheck[i]
				mc.selecte = false;
				var tabRelated = te.split("btn");
				mc.related = "mc"+tabRelated[1];
				if(te.indexOf("T")>=0){
					mc.r = "T";
					if(te.indexOf("0")>=0){						
						mc.nbr = 0;
					}else if(te.indexOf("1")>=0){
						mc.nbr = 1;
					}else{
						mc.nbr = 2;
					}
				}else if(te.indexOf("M")>=0){
					mc.r = "M";
					if(te.indexOf("0")>=0){						
						mc.nbr = 0;
					}else{
						mc.nbr = 1;
					}
				}else{
					mc.r = "C";
					if(te.indexOf("0")>=0){						
						mc.nbr = 0;
					}else{
						mc.nbr = 1;
					}
				}
				mc.onPress = function(){
					_parent._parent.soundPlayer.playASound("btnClick",0);
					if(this.selecte){
						this._parent[this.related]._visible = false;
						this.selecte = false;
					}else{
						this._parent[this.related]._visible = true;
						this.selecte = true;
						for(var i=0;i<=2;i++){
							if(i!=this.nbr){
								this._parent["mc"+this.r+i]._visible = false;
								this._parent["p_btn"+this.r+i].selecte = false;
							}
						}
					}
				}
			}
		}
	}
	public function changeExoInit(nbr:Number):Void{
		//rien car un seul exo
	}
	public function switchExercice(val:Boolean):Void{

		this.p_mcCat._visible = true;
		if(!val){
			this.p_mcCoches.mc1._visible = true;
		}
		
	}
	public function testResult():Boolean{
		var tabTest = [[0, 0, 0], [1, 0, 0], [1, 0, 1], [0, 0, 1], [0, 1, 0], [1, 1, 0], [1, 1, 1], [0, 1, 1], [0, 2, 0], [1, 2, 0], [1, 2, 1], [0, 2, 1]];
		
		var tabResult = new Array();
		for(var i in this.p_mcCheck){
			var te = this.p_mcCheck[i]._name;
			if(te.indexOf("btn")>=0){
				var mc = this.p_mcCheck[i]
				if(te.indexOf("T")>=0){
					if(mc.selecte){
						tabResult[1] = mc.nbr;
					}					
				}else if(te.indexOf("M")>=0){
					if(mc.selecte){
						tabResult[2] = mc.nbr;
					}
				}else{
					if(mc.selecte){
						tabResult[0] = mc.nbr;
					}
				}
			}
		}
		var te = true;
		for(var i=0;i<3;i++){
			if(tabTest[this.selectedNumber-1][i] != tabResult[i]) te = false
		}
		if(!te){
			this.p_mcResults.modifyFalseResult();
		}else{
			this.p_mcResults.modifyGoodResult();
			this.mcDisplay.mc11._visible = true;
		}
		return te;
	}
	private function initCursor(val:Boolean):Void{
		for(var i =0; i<12;i++){
			var mc = this.mcDisplay["mc"+i].useHandCursor = val;
		}
		p_mcCheck._visible = val;
		for(var i in this.p_mcCheck){
			if(this.p_mcCheck[i]._name.indexOf("btn")<0){
				this.p_mcCheck[i]._visible=false;
			}
		}
	}
	public function initializeGameInterface():Void{
		this.p_mcResults.checkResultAndReset();
		actualTryExo = 1;
		
		this.selectedNumber = random(12)+1;
		var tab = new Array();
		for(var i =1; i<=12;i++){
			if(i!=this.selectedNumber){
				tab.push(i);
			}
		}
		for(var i =0; i<11;i++){
			var mc = this.mcDisplay["mc"+i];
			mc._x = mc.xinit;
			mc._y = mc.yinit;
			mc.gotoAndStop(tab.splice(random(tab.length),1));
		}
		for(var i in this.p_mcCheck){
			var te = this.p_mcCheck[i]._name;
			if(te.indexOf("btn")>=0){
				var mc = this.p_mcCheck[i];
				mc.selecte = false;
			}
		}
		this.initCursor(true);
		this.mcDisplay.mc11.gotoAndStop(this.selectedNumber);
		this.mcDisplay.mc11._visible = false;
		
	}
}

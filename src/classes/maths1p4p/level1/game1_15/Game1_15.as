﻿/**
 * 
 * class com.maths1p4p.level1.game1_15.game1_15
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 *
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level1.game1_15.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_15.Game1_15 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	public var p_txtName:TextField;
	public var p_txtStart:TextField;
	
	

	public var p_btnStart:Button;
	public var p_btnOne:Button;

	public var p_btnTwo:Button;


	
	public var p_mcOne:Button;
	public var p_mcTwo:Button;
	
	public var mcInterface:MovieClip;
	public var mcSquare:MovieClip;
	public var mcIndiceDisplay:MovieClip;
	public var p_mcCoches:MovieClip;
	public var p_mcCat:MovieClip;
	
	public var p_isPlaying:Boolean;
	



	public var actualTry:Number;
	public var actualTryExo:Number;
	public var p_mcResults:level1_resultat;
	public var selectedGame:String;
	public var nameGames:Array = ["One","Two"];
	
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	private var myInterval:Number;
	private var nbrTry:Number;
	private var selectedNumber:Number;
	private var tabFormes:Array;
	private var tabResult:Array;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_15()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 0;
		actualTryExo = 0;
		p_mcCat._visible = false;
		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		
		
		p_txtStart.text = "Départ";
		
		
		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai3";
		
		
		nbrTry = 4;//4
		p_mcResults = new level1_resultat(this,this.nbrTry);
		

		mcInterface._visible = false;
		mcIndiceDisplay._visible = false;
		
		
		p_btnStart.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.p_mcCat._visible = false;
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_isPlaying = true;
				_parent.initializeGameInterface();
				_parent.p_txtStart.textColor = 0xCCCCCC;
				_parent.p_btnOne.useHandCursor = false;
				_parent.p_btnTwo.useHandCursor = false;
				this.useHandCursor = false;
			}
		}
		
		p_btnOne.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOne._visible= true;
				_parent.p_mcTwo._visible= false;
				_parent.selectedGame = "One";
			}			
		}
		
		p_btnTwo.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOne._visible= false;
				_parent.p_mcTwo._visible= true;
				_parent.selectedGame = "Two";
			}			
		}
		for(var i =0;i<12;i++){
			var btn = mcInterface["btn"+i];
			btn.ref = this;
			btn.img = i;
			btn.onPress = function(){
				if(this.ref.p_isPlaying){
					this.ref.mcSquare.startDrag(true);
					this.ref.mcSquare._visible = true;
				}
			}
			btn.onRelease = btn.onReleaseOutside = function(){
				if(this.ref.p_isPlaying){
					this.ref.mcSquare.stopDrag();
					var test = false;
					for(var i =0;i<4;i++){
						var mcHit = this.ref.mcIndiceDisplay["boiteDrop"+i];						
						if(this.ref.mcSquare.hitTest(mcHit) && !test && mcHit.notDiscovered){
							this.ref.soundPlayer.playASound("btnClick",0);
							mcHit.selectedFrame = this.img;
							mcHit.gotoAndStop(this.img+3);
							this.ref.testResult(mcHit);
							test = true;
							this.ref.soundPlayer.playASound("btnClick",0);
						}
					}
					this.ref.mcSquare._visible = false;
				}
			}
		}
		
		
		
		mcSquare = this.attachMovie("mcSquare","mcSquare",4);
		mcSquare._visible = false;
		tabFormes = new Array();
		for(var i =0;i<12;i++){
			tabFormes[i] = new Array();
			//couleur
			if(i-Math.floor(i/6)*6>=3){
				tabFormes[i][1] = 0;
			}else{
				tabFormes[i][1] = 1;
			}
			//forme
			tabFormes[i][0] = i-Math.floor(i/3)*3;
			//motif
			if(i-Math.floor(i/12)*12>=6){
				tabFormes[i][2] = 0;
			}else{
				tabFormes[i][2] = 1;
			}
		}
		tabResult = new Array();
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			p_mcOne._visible= false;
			p_mcTwo._visible= true;
			selectedGame = "Two";
		}else{
			p_mcOne._visible= true;
			p_mcTwo._visible= false;
			selectedGame = "One";
		}
		p_mcCat._visible = false;
	}
	public function switchExercice(val:Boolean):Void{

		this.p_mcCat._visible = true;
		if(this.selectedGame == "One"){
			p_mcOne._visible= false;
			p_mcTwo._visible= true;
			this.selectedGame = "Two";
			if(!val){
				this.p_mcCoches.mc1._visible = true;
			}
		}else{
			p_mcOne._visible= true;
			p_mcTwo._visible= false;
			this.selectedGame = "One";
			if(!val){
				this.p_mcCoches.mc2._visible = true;
			}
		}
		
		
	}
	public function testResult(mc:MovieClip):Void{
		var mcInd = mcIndiceDisplay["indice"+mc.nbr];
		var indiceNotRespected = false;
		for(var i = 0;i<3;i++){
			var mcIndAff = mcInd["ind"+i];
			
			if(tabFormes[this.tabResult[mc.nbr]][i] == tabFormes[mc.selectedFrame][i]){
				mcIndAff.gotoAndStop(2);				
			}else{
				if(mcIndiceDisplay["boite"+mc.nbr].alreadyTested){
					if(mcIndAff.ok == tabFormes[mc.selectedFrame][i]){
						indiceNotRespected = true;
					}
				}
				mcIndAff.gotoAndStop(1);
			}
			mcIndAff.ok = tabFormes[mc.selectedFrame][i];
		}
		if(mc.selectedFrame != this.tabResult[mc.nbr]){
			if(mcIndiceDisplay["boite"+mc.nbr].alreadyTested){
				if(indiceNotRespected){
					this.p_mcResults.modifyFalseResult();
				}
			}else{
				mcIndiceDisplay["boite"+mc.nbr].alreadyTested = true;
			}
		}else{
			mcIndiceDisplay["boite"+mc.nbr].gotoAndStop(mc.selectedFrame+3);
			actualTryExo = 1;
			if(this.p_mcResults.modifyGoodResult()){
				this.p_isPlaying = false;
				this.p_txtStart.textColor = 0x000000;
				this.p_btnStart.useHandCursor = true;
				this.p_btnOne.useHandCursor = true;
				this.p_btnTwo.useHandCursor = true;
			}
			mc.notDiscovered = false;
		}
		
	}
	public function initializeGameInterface():Void{
		actualTryExo = 1;
		this.p_mcResults.checkResultAndReset();
		if(this.selectedGame == "One"){
			this.mcInterface.gotoAndStop(1);
		}else{
			this.mcInterface.gotoAndStop(2);
		}
		mcInterface._visible = true;
		for(var i =0;i<12;i++){
			var btn = mcInterface["btn"+i];
			btn._visible = true;
			if(this.selectedGame == "One"){
				if(i>=6){
					btn._visible = false;
				}
			}
		}
		mcIndiceDisplay._visible = true;
		var tabD = new Array();
		if(this.selectedGame == "One"){
			var max = 6;
		}else{
			var max = 12;
		}
		for(var i =0; i<max;i++){
				tabD.push(i);		
		}
		for(var i =0;i<4;i++){
			tabResult[i] = Number(tabD.splice(random(tabD.length),1).toString());
		}
		trace(tabResult);
		for(var i =0;i<4;i++){
			var mc = mcIndiceDisplay["boite"+i];
			mc.gotoAndStop(2);
			mc.alreadyTested = false;
			var mc = mcIndiceDisplay["boiteDrop"+i];
			mc.gotoAndStop(1);
			mc.notDiscovered = true;
			mc.selectedFrame = 0;
			mc.nbr = i;
			var mc = mcIndiceDisplay["indice"+i];
			if(this.selectedGame == "One"){
				mc.gotoAndStop(1);
				mc.ind2._visible = false;
			}else{
				mc.gotoAndStop(2);
				mc.ind2._visible = true;
			}
			for( var j=0;j<3;j++){
				mc["ind"+j].gotoAndStop(1);
				mc["ind"+j].ok = undefined;
			}
		}
		
	}
}

﻿/**
 * 
 * class com.maths1p4p.level1.game1_16.game1_16
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level1.game1_16.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_16.Game1_16 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	public var p_txtName:TextField;
	public var p_txtStart:TextField;
	
	

	public var p_btnStart:Button;
	public var p_btnOne:Button;

	public var p_btnTwo:Button;


	
	public var p_mcOne:Button;
	public var p_mcTwo:Button;
	
	public var mcDisplay:MovieClip;
	public var mcDrop:MovieClip;
	public var p_mcCoches:MovieClip;
	public var p_mcCat:MovieClip;
	public var p_mcBulleDiff:MovieClip;
	
	
	public var p_isPlaying:Boolean;
	public var dicesAreRolling:Boolean;
	



	public var actualTry:Number;
	public var actualTryExo:Number;
	public var p_mcResults:level1_resultat;
	
	
	public var selectedGame:String;
	public var nameGames:Array = ["One","Two"];
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	private var myInterval:Number;
	private var nbrTry:Number;
	private var selectedNumber:Number
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_16()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){
		actualTry = 0;
		actualTryExo = 0;
		p_mcCat = this.attachMovie("mcCat","p_mcCat",21);

		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		p_txtStart.text = "Départ";
		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		
		
		nbrTry = 1;		
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		
		p_btnStart.onPress = function(){
			_parent.soundPlayer.playASound("btnClick",0);
			if(!_parent.p_isPlaying){
				_parent.p_mcCat._visible = false;
				_parent.p_isPlaying = true;
				_parent.initializeGameInterface();
				_parent.p_txtStart.text = "J'ai fini";
				_parent.p_btnOne.useHandCursor = false;
				_parent.p_btnTwo.useHandCursor = false;
			}else{
				_parent.testResult();
			}
		}
		
		p_btnOne.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOne._visible= true;
				_parent.p_mcTwo._visible= false;
				_parent.selectedGame = "One";
				_parent.p_mcBulleDiff.texte.text = "1\ndifférence.";
			}			
		}
		
		p_btnTwo.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOne._visible= false;
				_parent.p_mcTwo._visible= true;
				_parent.selectedGame = "Two";
				_parent.p_mcBulleDiff.texte.text = "2\ndifférences.";
			}			
		}		
		mcDrop = this.createEmptyMovieClip("mcDrop",2);
		for(var i =0; i<12;i++){
			var mc = this.mcDrop.attachMovie("mcTete","mc"+i,i+1);
			if(i>=3 && i<=5){
				mc._x = 60+(i-1)*53;
				mc._y = 101+60;
			}else if(i>=6 && i<=8){
				mc._x = 60+(i-2)*53;
				mc._y = 101;
			}else if(i>=9 && i<=11){
				mc._x = 60+(i-3)*53;
				mc._y = 101+60;
			}else{
				mc._x = 60+i*53;
				mc._y = 101;
			}
			
			mc.selecte = undefined;
			mc.gotoAndStop(13);
		}
		
		mcDisplay = this.createEmptyMovieClip("mcDisplay",3);
		for(var i =0; i<12;i++){
			var mc = this.mcDisplay.attachMovie("mcTete","mc"+i,i+1);
			mc._x = 37+(i-Math.floor(i/6)*6)*53;
			mc._y = 239+Math.floor(i/6)*60;
			mc.xinit = mc._x;
			mc.yinit = mc._y;
			mc.nbr = i;
			mc.gotoAndStop(i+1);
			mc.ref = this;
			mc.droppedOn = undefined;
			mc.selecte = false;
			mc.onPress = function(){
				if(this.ref.p_isPlaying){
					if(!this.selecte){
						this.startDrag(true);
						this.swapDepths(13);
					}
				}
			}
			mc.onRelease = mc.onReleaseOutside = function(){
				if(this.ref.p_isPlaying){
					if(!this.selecte){
						this.stopDrag();
						var test = false;
						for(var i =1; i<12;i++){
							var mcHit = this.ref.mcDrop["mc"+i];
							if(this.hitTest(mcHit) && !test){
								this.ref.soundPlayer.playASound("btnClick",0);
								this._x = mcHit._x;
								this._y = mcHit._y;
								test = true;
								this.droppedOn.selecte = undefined;
								if(mcHit.selecte != undefined && mcHit.selecte!=this){
									mcHit.selecte._x = mcHit.selecte.xinit;
									mcHit.selecte._y = mcHit.selecte.yinit;
									mcHit.selecte.droppedOn = undefined;
								}
								this.droppedOn = mcHit;
								mcHit.selecte = this;
							}
						}
						if(!test){
							this._x = this.xinit;
							this._y = this.yinit;
						}
					}
				}
			}
		}
		initCursor(false);
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			p_mcOne._visible= false;
			p_mcTwo._visible= true;
			selectedGame = "Two";
		}else{
			p_mcOne._visible= true;
			p_mcTwo._visible= false;
			selectedGame = "One";
		}
		p_mcCat._visible = false;
	}
	public function switchExercice(val:Boolean):Void{
		
		this.p_mcCat._visible = true;
		if(this.selectedGame == "One"){
			p_mcOne._visible= false;
			p_mcTwo._visible= true;
			this.selectedGame = "Two";
			if(!val){
				this.p_mcCoches.mc1._visible = true;
			}
		}else{
			p_mcOne._visible= true;
			p_mcTwo._visible= false;
			this.selectedGame = "One";
			if(!val){
				this.p_mcCoches.mc2._visible = true;
			}
		}
		
		
	}
	public function testResult():Void{
		var tabTest = [[0, 0, 0], [1, 0, 0], [1, 0, 1], [0, 0, 1], [0, 1, 0], [1, 1, 0], [1, 1, 1], [0, 1, 1], [0, 2, 0], [1, 2, 0], [1, 2, 1], [0, 2, 1]];
		var te = true;
		var isComplet = true;
		var somme = 1;
		for(var i =1; i<12;i++){
			var mc = this.mcDrop["mc"+i];
			if(mc.selecte != undefined){
				somme++;
			}
		}
		if(somme != 12){
			isComplet = false;
		}
		if(isComplet){
			for(var i =1; i<12;i++){
				var mc = this.mcDrop["mc"+i];
				if(mc.selecte != undefined){
					
					var nbrDiff = 0;
					for(var j=0;j<3;j++){
						if(tabTest[mc.selecte.nbr][j] != tabTest[this.mcDrop["mc"+(i-1)].selecte.nbr][j]) nbrDiff++
					}
					trace(mc.selecte.nbr+":"+this.mcDrop["mc"+(i-1)].selecte.nbr+"*****"+nbrDiff+":"+tabTest[mc.selecte.nbr]+":"+tabTest[this.mcDrop["mc"+(i-1)].selecte.nbr]);
					if(this.selectedGame == "One"){
						var testDiff = 1;
					}else{
						var testDiff = 2;
					}
					if(nbrDiff != testDiff){
						te = false;
						mc.selecte._x = mc.selecte.xinit;
						mc.selecte._y = mc.selecte.yinit;
					}
				}
				
			}
			if(!te){
				this.p_mcResults.modifyFalseResult();
			}else{
				this.p_mcResults.modifyGoodResult();
				p_isPlaying = false;
				p_txtStart.text = "Départ";
				p_btnOne.useHandCursor = true;
				p_btnTwo.useHandCursor = true;
			}
		}else{
			this.soundPlayer.playASound("rempCases",0);				
		}
	}
	private function initCursor(val:Boolean):Void{
		for(var i =0; i<12;i++){
			var mc = this.mcDisplay["mc"+i].useHandCursor = val;
		}
	}
	public function initializeGameInterface():Void{
		if(this.selectedGame == "One"){
			this.p_mcBulleDiff.texte.text = "1\ndifférence.";
		}else{
			this.p_mcBulleDiff.texte.text = "2\ndifférences.";
		}
		actualTryExo = 1;
		this.p_mcResults.checkResultAndReset();
		initCursor(true);
		this.selectedNumber = random(12)+1;
		var t = this.selectedNumber-1;
		var tab = new Array();
		for(var i =1; i<=12;i++){
			if(i!=this.selectedNumber){
				tab.push(i);
			}
		}
		for(var i =0; i<12;i++){
			if(i!=t){
				var mc = this.mcDisplay["mc"+i];
				mc._x = mc.xinit;
				mc._y = mc.yinit;
				mc.selecte = false;
				mc.droppedOn = undefined;
				var nbr = Number(tab.splice(random(tab.length),1).toString())
				mc.gotoAndStop(nbr);
				mc.nbr = nbr-1
			}
		}
		for(var i =0; i<12;i++){
			var mc = this.mcDrop["mc"+i];
			mc.selecte=undefined;
		}
		var mcFirst = this.mcDisplay["mc"+t];
		mcFirst.nbr = t;
		mcFirst.gotoAndStop(this.selectedNumber);
		mcFirst.selecte = true;
		mcFirst.useHandCursor = false;
		mcFirst._x = this.mcDrop.mc0._x;
		mcFirst._y = this.mcDrop.mc0._y;
		this.mcDrop.mc0.selecte = mcFirst;
	}
}

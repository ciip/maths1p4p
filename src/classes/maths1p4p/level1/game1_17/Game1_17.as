﻿/**
 * 
 * class com.maths1p4p.level1.game1_17.game1_17
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * ETAT : 
 * stable
 * 
 * TODO :
 * -> sons mais dn
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level1.game1_17.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_17.Game1_17 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	public var p_txtName:TextField;
	public var p_txtStart:TextField;
	
	

	public var p_btnStart:Button;
	public var p_btnOne:Button;

	public var p_btnTwo:Button;


	
	public var p_mcOne:Button;
	public var p_mcTwo:Button;
	
	public var mcDisplay:MovieClip;
	public var mcSquares:MovieClip;
	public var p_mcCoches:MovieClip;
	public var p_mcCat:MovieClip;
	
	public var p_isPlaying:Boolean;
	



	public var actualTry:Number;
	public var actualTryExo:Number;
	public var p_mcResults:level1_resultat;
	public var selectedGame:String;
	public var nameGames:Array = ["One","Two"];
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	private var myInterval:Number;
	private var nbrTry:Number;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_17()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 0;
		actualTryExo = 0;
		p_mcCat = this.attachMovie("mcCat","p_mcCat",210);

		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		p_txtStart.text = "Départ";

		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		
		
		nbrTry = 2;//2
		p_mcResults = new level1_resultat(this,this.nbrTry);
		

		p_btnStart.onPress = function(){
			_parent.soundPlayer.playASound("btnClick",0);
			if(!_parent.p_isPlaying){
				_parent.p_mcCat._visible = false;
				_parent.p_isPlaying = true;
				_parent.initializeGameInterface();
				_parent.p_txtStart.text = "J'ai fini";
				_parent.p_btnOne.useHandCursor = false;
				_parent.p_btnTwo.useHandCursor = false;
			}else{
				_parent.testResult();
			}
		}
		
		p_btnOne.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOne._visible= true;
				_parent.p_mcTwo._visible= false;
				_parent.selectedGame = "One";
			}			
		}
		
		p_btnTwo.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOne._visible= false;
				_parent.p_mcTwo._visible= true;
				_parent.selectedGame = "Two";
			}			
		}	
		mcDisplay = this.createEmptyMovieClip("mcDisplay",3);
		for(var i =0; i<6;i++){
			var mc = this.mcDisplay.attachMovie("mcTete","mc"+i,i+1);
			mc._x = 340+(i-Math.floor(i/2)*2)*127;
			mc._y = 89+Math.floor(i/2)*115;
			mc.xinit = mc._x;
			mc.yinit = mc._y;
			mc.gotoAndStop(i+1);
			mc.nbr = i;
			mc.ref = this;
			mc.place = i;
			mc.onPress = function(){
				if(this.ref.p_isPlaying){
					this.startDrag(true);
					this.swapDepths(7);
				}
			}
			mc.onRelease = mc.onReleaseOutside = function(){
				if(this.ref.p_isPlaying){
					this.stopDrag();
					var test = false;
					for(var i =0; i<6;i++){
						var mcHit = this._parent["mc"+i];
						if(this.hitTest(mcHit) && mcHit != this && !test){
							this.ref.soundPlayer.playASound("btnClick",0);
							var place = this.place;
							this._x = mcHit.xinit;
							this._y = mcHit.yinit;
							mcHit._x = this.xinit;
							mcHit._y = this.yinit;
							mcHit.xinit = this.xinit;
							mcHit.yinit = this.yinit;
							this.place = mcHit.place;
							mcHit.place = place;
							this.xinit = this._x;
							this.yinit = this._y;
							test = true;
							this.ref.soundPlayer.playASound("btnClick",0);
						}
					}
					if(!test){
						this._x = this.xinit;
						this._y = this.yinit;
					}
				}
			}
		}
		mcSquares = this.createEmptyMovieClip("mcSquares",2);
		var t = 1;
		for(var i = 0;i<6;i++){
			for(var j=0;j<6;j++){
				var mc = mcSquares.attachMovie("mcSquare","squarre_"+i+"_"+j,t);
				mc.i = i;
				mc.j = j;
				mc._x = 61+i*35;
				mc._y = 150+j*36;
				mc.ref = this;
				mc.selecte = false;
				mc.onPress = function(){
					if(this.ref.p_isPlaying && this.ref.selectedGame == "Two"){
						if(this.selecte){
							this.selecte = false;
							this.gotoAndStop(1);
						}else{
							this.selecte = true;
							this.gotoAndStop(2);
						}
					}
				}
				t++;
			}
		}
		initCursor(false);
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			p_mcOne._visible= false;
			p_mcTwo._visible= true;
			selectedGame = "Two";
		}else{
			p_mcOne._visible= true;
			p_mcTwo._visible= false;
			selectedGame = "One";
		}
		p_mcCat._visible = false;
	}
	public function switchExercice(val:Boolean):Void{

		this.p_mcCat._visible = true;
		if(this.selectedGame == "One"){
			p_mcOne._visible= false;
			p_mcTwo._visible= true;
			this.selectedGame = "Two";
			if(!val){
				this.p_mcCoches.mc1._visible = true;
			}
		}else{
			p_mcOne._visible= true;
			p_mcTwo._visible= false;
			this.selectedGame = "One";
			if(!val){
				this.p_mcCoches.mc2._visible = true;
			}
		}
		
		
	}
	public function testResult():Void{
		var te = true;
		var tabResHat = new Array();
		for(var i =0; i<6;i++){
			var mc = this.mcDisplay["mc"+i];
			tabResHat[mc.place] = mc.nbr;
		}
		trace(tabResHat);
		var tabResSquare = new Array();
		var t = 0;
		for(var i = 0;i<6;i++){
			for(var j=0;j<6;j++){
				var mc = mcSquares["squarre_"+i+"_"+j];
				if(mc.selecte){
					t++;
					tabResSquare[i] = j;
				}
			}
		}
		trace(tabResSquare+":"+t);
		if(t!=6){
			te = false;
		}else{
			for(var i =0; i<6;i++){
				if(tabResHat[i] != tabResSquare[i]){
					te = false;
					trace(i);
				}
			}
		}
		if(!te){
			this.p_mcResults.modifyFalseResult();
		}else{
			this.p_mcResults.modifyGoodResult();
			p_isPlaying = false;
			p_txtStart.text = "Départ";
			p_btnOne.useHandCursor = true;
			p_btnTwo.useHandCursor = true;
			initCursor(false);
		}
	}
	private function initCursor(val:Boolean):Void{
		for(var i =0; i<6;i++){
			this.mcDisplay["mc"+i].useHandCursor = val;
		}
		for(var i = 0;i<6;i++){
			for(var j=0;j<6;j++){
				var mc = mcSquares["squarre_"+i+"_"+j];
				mc.useHandCursor = val;
				mc.selecte = false;
				mc.gotoAndStop(1);
			}
		}
	}
	public function initializeGameInterface():Void{
		actualTryExo = 1;
		this.p_mcResults.checkResultAndReset();
		initCursor(true);
		var tabRes = [0,1,2,3,4,5];
		if(this.selectedGame == "One"){
			for(var i = 0;i<6;i++){
				for(var j=0;j<6;j++){
					var mc = mcSquares["squarre_"+i+"_"+j];
					mc.useHandCursor = false;
				}
			}
		}
		for(var i = 0;i<6;i++){
			var mc = mcSquares["squarre_"+i+"_"+tabRes.splice(random(tabRes.length),1)];
			mc.selecte = true;
			mc.gotoAndStop(2);
		}
	}
}

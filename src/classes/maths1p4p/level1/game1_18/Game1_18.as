﻿/**
 * 
 * class com.maths1p4p.level1.game1_18.game1_18
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level1.game1_18.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_18.Game1_18 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	public var p_txtName:TextField;
	public var p_txtStart:TextField;
	
	

	public var p_btnStart:Button;
	public var p_btnOne:Button;
	public var p_btnTwo:Button;	
	public var btnSun:Button;
	
	public var p_mcOne:MovieClip;
	public var p_mcTwo:MovieClip;	
	
	
	public var mcDisplay:MovieClip;
	public var mcAnswer:MovieClip;
	public var mcMaisons:MovieClip;
	public var mcIndiceDisplay:MovieClip;
	public var p_mcCoches:MovieClip;
	public var p_mcCat:MovieClip;
	
	
	public var p_isPlaying:Boolean;
	



	public var actualTry:Number;
	public var actualTryExo:Number;
	public var p_mcResults:level1_resultat;
	public var selectedGame:String;
	public var nameGames:Array = ["One","Two"];
	
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var tabSelecIndice:Array;
	private var myInterval:Number;
	private var nbrTry:Number;
	private var actualIndice:Number;
	private var maxIndice:Number;
	private var selectedNumber:Number;
	private var tabMaisons:Array;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_18()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 0;
		actualTryExo = 0;
		p_mcCat._visible = false;
		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		p_txtStart.text = "Départ";
		this.tabSelecIndice = new Array();
		
		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		
		
		nbrTry = 2;//2
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		
		mcAnswer._visible = false;
		mcAnswer.btn.enabled = false;
		mcAnswer.mcOmbrage._visible = false;
		
		
		p_btnStart.onPress = function(){
			_parent.soundPlayer.playASound("btnClick",0);
			if(!_parent.p_isPlaying){
				_parent.p_mcCat._visible = false;
				_parent.p_isPlaying = true;
				_parent.initializeFirstGameInterface();
				_parent.p_txtStart.text = "J'ai fini";
				_parent.p_btnOne.useHandCursor = false;
				_parent.p_btnTwo.useHandCursor = false;
			}else{
				_parent.testResult();
			}
		}
		
		p_btnOne.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOne._visible= true;
				_parent.p_mcTwo._visible= false;
				_parent.selectedGame = "One";
			}			
		}
		
		p_btnTwo.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOne._visible= false;
				_parent.p_mcTwo._visible= true;
				_parent.selectedGame = "Two";
			}			
		}
		
		btnSun.onPress = function(){
			var re = this;
			var omb = re._parent.mcOmbrage;
			if(omb._visible){
				omb._visible = false;
			}else{
				omb._visible = true;
			}
		}
		
		var tabD = new Array();
		for(var i =0; i<=8;i++){
				tabD.push(i);		
		}
		mcDisplay = this.createEmptyMovieClip("mcDisplay",3);
		for(var i =0; i<10;i++){
			var mc = this.mcDisplay.attachMovie("mcBtnIndice","mc"+i,i+1);
			mc._x = 31+(i-Math.floor(i/5)*5)*62;
			mc._y = 335+Math.floor(i/5)*58;
			mc.texte.text = "";
			mc.ref = this;
			if(i!=4 && i!=9){
				mc.bg.gotoAndStop(tabD.splice(random(tabD.length),1));
				
			}else{
				if(i ==4){
					mc.bg.gotoAndStop(9);
					mc.ty = 1;
				}
				if(i ==9){
					mc.bg.gotoAndStop(10);
					mc.ty = 0;
				}
				mc.tx = 4;
			}
			
			mc.onPress = function(){
				if(this.ref.p_isPlaying){
					if(this.inUse){
						this.ref.soundPlayer.playASound("btnClick",0);
						this.inUse = false;
						this.ref.afficheIndice(this);						
					}
				}
			}
		}
		this.mcDisplay.mc4._visible = false;
		this.mcDisplay.mc9._visible = false;
		
		
		mcMaisons = this.createEmptyMovieClip("mcMaisons",4);
		tabMaisons = new Array();
		for(var i =0; i<32;i++){
			tabMaisons[i] = new Array();
			if((i-Math.floor(i/8)*8)>=4){
				tabMaisons[i][0] = 0;
			}else{
				tabMaisons[i][0] = 1;
			}
			if((i-Math.floor(i/4)*4)>=2){
				tabMaisons[i][1] = 0;
			}else{
				tabMaisons[i][1] = 1;
			}
			if((i-Math.floor(i/2)*2)>=1){
				tabMaisons[i][2] = 0;
			}else{
				tabMaisons[i][2] = 1;
			}
			if((i-Math.floor(i/16)*16)>=8){
				tabMaisons[i][3] = 0;
			}else{
				tabMaisons[i][3] = 1;
			}
			if(i>=16){
				tabMaisons[i][4] = 0;
			}else{
				tabMaisons[i][4] = 1;
			}
			var mc = this.mcMaisons.attachMovie("mcMaison","mc"+i,i+1);
			mc.mcOmbrage._visible = false;
			mc.btn.ref = this;
			mc.btn.onPress = function(){
				var re = this;
				if(re.ref.p_isPlaying){
					var omb = re._parent.mcOmbrage;
					if(omb._visible){
						omb._visible = false;
					}else{
						omb._visible = true;
					}
				}
			}
			if(i<16){
				mc._x = 58+(i-Math.floor(i/4)*4)*53;
				mc._y = 67+Math.floor(i/4)*60;
			}else{
				mc._x = 270+(i-Math.floor(i/4)*4)*53;
				mc._y = -173+Math.floor(i/4)*60;
				mc._visible = false
			}
			mc.bg.gotoAndStop(1+i);
			mc.bg.nbr = i
			mc.bg.ref = this;
			mc.bg.onPress = function(){
				if(this.ref.p_isPlaying){
					this.ref.soundPlayer.playASound("btnClick",0);
					this.ref.mcAnswer._visible = true;
					this.ref.mcAnswer.nbr = this.nbr;
					this.ref.mcAnswer.bg.gotoAndStop(this.nbr+1);
				}
			}
		}
		
		initCursor(false);
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			p_mcOne._visible= false;
			p_mcTwo._visible= true;
			selectedGame = "Two";
		}else{
			p_mcOne._visible= true;
			p_mcTwo._visible= false;
			selectedGame = "One";
		}
		p_mcCat._visible = false;
	}
	public function switchExercice(val:Boolean):Void{

		this.p_mcCat._visible = true;
		if(this.selectedGame == "One"){
			p_mcOne._visible= false;
			p_mcTwo._visible= true;
			this.selectedGame = "Two";
			if(!val){
				this.p_mcCoches.mc1._visible = true;
			}
		}else{
			p_mcOne._visible= true;
			p_mcTwo._visible= false;
			this.selectedGame = "One";
			if(!val){
				this.p_mcCoches.mc2._visible = true;
			}
		}
		
		
	}
	public function afficheIndice(mc:MovieClip):Void{
		trace(mc.tx+":"+mc.ty);
		this.tabSelecIndice[mc.tx] = this.tabSelecIndice[mc.tx]+1;
		if(this.actualIndice<this.maxIndice){
			this.mcIndiceDisplay["ind"+this.actualIndice]._visible = true;
			if(tabMaisons[this.selectedNumber][mc.tx] == mc.ty){
				mc.texte.text = "oui";
			}else{
				mc.texte.text = "non";
			}
			this.actualIndice++;
			if(this.actualIndice==this.maxIndice){
				for(var i =0; i<=4;i++){
					if(this.tabSelecIndice[i] >=2){
						this.p_mcResults.modifyFalseResult();
						initializeGameInterface();	
					}
				}
			}
		}else{			
			for(var i =0; i<10;i++){
				var mc = this.mcDisplay["mc"+i]
				mc.enabled = false;
			}
		}
		
		
	}
	public function testResult():Void{
		var te = true;
		if(this.mcAnswer.nbr != this.selectedNumber){
			te=false;
		}
		if(!te){
			this.p_mcResults.modifyFalseResult();
		}else{
			this.p_mcResults.modifyGoodResult();
			p_isPlaying = false;
			p_txtStart.text = "Départ";
			p_btnOne.useHandCursor = true;
			p_btnTwo.useHandCursor = true;
			initCursor(false);
		}
	}
	private function initCursor(val:Boolean):Void{
		for(var i =0; i<10;i++){
			var mc = this.mcDisplay["mc"+i]
			mc.useHandCursor = val;
			mc.inUse = val;
		}
		for(var i =0; i<32;i++){
			var mc = this.mcMaisons["mc"+i].bg;
			mc.useHandCursor = val;
			var mc = this.mcMaisons["mc"+i].btn;
			mc.useHandCursor = val;
		}
		for(var i=0;i<5;i++){
			mcIndiceDisplay["ind"+i]._visible = false;
		}
	}
	public function initializeFirstGameInterface():Void{
		actualTryExo = 1;
		this.p_mcResults.checkResultAndReset();
		initializeGameInterface();
	}
	public function initializeGameInterface():Void{		
		initCursor(true);
		mcAnswer._visible = false;
		mcAnswer.nbr = undefined;
		actualIndice = 0;
		maxIndice = 5;
		var tabD = new Array();
		for(var i =1; i<=8;i++){
				tabD.push(i);		
		}
		for(var i =0; i<=4;i++){
			this.tabSelecIndice[i] = 0;		
		}
		
		for(var i =0; i<10;i++){
			var mc = this.mcDisplay["mc"+i]
			mc.enabled = true;
			mc.texte.text = "";
			mc._visible = true;
			if(i!=4 && i!=9){
				var t = Number(tabD.splice(random(tabD.length),1).toString());
				trace(t);
				mc.bg.gotoAndStop(t);
				t--;
				mc.tx = Math.floor(t/2);
				mc.ty = t-Math.floor(t/2)*2;
			}
		}
		for(var i =0; i<32;i++){
			var mc = this.mcMaisons["mc"+i];
			mc._visible = true;
			mc.mcOmbrage._visible = false;
		}
		if(this.selectedGame == "One"){
			this.selectedNumber = random(16);
			maxIndice = 4;
			this.mcDisplay.mc4.useHandCursor = false;
			this.mcDisplay.mc9.useHandCursor = false;
			this.mcDisplay.mc4.inUse = false;
			this.mcDisplay.mc9.inUse = false;
			this.mcDisplay.mc4._visible = false;
			this.mcDisplay.mc9._visible = false;
			this.mcIndiceDisplay.gotoAndStop(1);
			for(var i =16; i<32;i++){
				var mc = this.mcMaisons["mc"+i];
				mc._visible = false;
			}
		}else{
			this.selectedNumber = random(32);
			this.mcIndiceDisplay.gotoAndStop(2);
		}
		trace(tabMaisons[this.selectedNumber]);
	}
}

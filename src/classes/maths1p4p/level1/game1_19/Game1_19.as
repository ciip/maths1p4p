﻿/**
 * 
 * class com.maths1p4p.level1.game1_19.game1_19
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level1.game1_19.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_19.Game1_19 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	public var p_txtName:TextField;
	public var p_txtStart:TextField;
	
	

	public var p_btnStart:Button;


	
	public var mcDisplay:MovieClip;
	public var mcConstructor:MovieClip;
	public var selectedJewel:MovieClip;
	public var jewelT:MovieClip;
	public var mcCollier:MovieClip;
	public var p_mcCoches:MovieClip;
	public var p_mcCat:MovieClip;

	
	
	public var p_isPlaying:Boolean;
	



	public var actualTry:Number;
	public var actualTryExo:Number;
	public var p_mcResults:level1_resultat;
	public var tabColor:Array;
	
	public var selectedGame:String;
	public var nameGames:Array = ["jeu"];
	
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	private var myInterval:Number;
	private var nbrTry:Number;
	private var selectedNumber:Number;
	private var firstSelectedPattern:Number;
	private var tabJewels:Array;
	private var mcPattern:MovieClip
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_19()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 0;
		actualTryExo = 0;
		p_mcCat._visible = false;
		p_mcCoches.mc1._visible = false;
		p_txtStart.text = "Départ";
		mcPattern._visible = false;
		for(var i = 0;i<3;i++){
			var mc = this.mcPattern["j"+i].jewelBG.bg;
			mc.myColor = new Color(mc);
		}
		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		selectedGame = "jeu";
		
		nbrTry = 2;
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		
		
		
		
		p_btnStart.onPress = function(){
			_parent.soundPlayer.playASound("btnClick",0);
			if(!_parent.p_isPlaying){
				_parent.p_mcCat._visible = false;
				_parent.p_isPlaying = true;
				_parent.initializeGameInterface();
				_parent.p_txtStart.text = "J'ai fini";
				_parent.p_btnOne.useHandCursor = false;
				_parent.p_btnTwo.useHandCursor = false;
			}else{
				_parent.testResult();
			}
		}
		tabColor = [0x9c00f1,0x044ff8,0x00ce01,0xfe0000,0xff6f09];
		mcDisplay = this.createEmptyMovieClip("mcDisplay",3);
		this.selectedJewel = this.mcDisplay.attachMovie("mcJewelShadow","mc",1);
		this.selectedJewel._visible = false;
		jewelT = mcConstructor.jewel.jewelBG.bg;
		jewelT.myColor = new Color(jewelT);
		for(var i=0;i<5;i++){
			var btn = mcConstructor["btn"+i];
			btn.nbr = i;
			btn.target = jewelT;
			btn.onPress = function(){
				_parent._parent.soundPlayer.playASound("btnClick",0);
				this.target.gotoAndStop(this.nbr+1);
				_parent.selectedForm = this.nbr;
			}
			var btnC = mcConstructor["btnC"+i];
			btnC.nbr = i;
			btnC.ref = this;
			btnC.target = jewelT;
			btnC.onPress = function(){
				_parent._parent.soundPlayer.playASound("btnClick",0);
				this.target.myColor.setRGB(this.ref.tabColor[this.nbr]);
				_parent.selectedColor = this.nbr;
			}
		}
		for(var i=0;i<9;i++){
			var mc = this.mcCollier["j"+i];
			mc._alpha = 0;
			mc.selectedForm = undefined;
			mc.selectedColor = undefined;
			mc.onDrag = false;
			mc.selecte = false;
			mc.jewelBG.bg.myColor = new Color(mc.jewelBG.bg);
			mc.onPress = function(){
				if(!this.selecte){
					_parent._parent.soundPlayer.playASound("btnClick",0);
					this._alpha = 0;
					this.onDrag = false;
					this.useHandCursor = false;
				}
			}
		}
		this.jewelT.ref = this;
		this.jewelT.onPress = function(){
			if(this.ref.mcConstructor.selectedColor != undefined && this.ref.mcConstructor.selectedForm != undefined){
				this.ref.createNewJewel();
				this.ref.soundPlayer.playASound("btnClick",0);
				var mc = this.ref.selectedJewel;
				
				mc.startDrag(true);
			}
		}
		this.jewelT.onRelease = this.jewelT.onReleaseOutside = function(){
			if(this.ref.mcConstructor.selectedColor != undefined && this.ref.mcConstructor.selectedForm != undefined){
				var mc = this.ref.selectedJewel;
				mc.stopDrag();
				var i=0;
				var test = true;
				do{
					var mcHit = this.ref.mcCollier["j"+i];
					if(mc.hitTest(mcHit) && !mcHit.selecte){
						trace(mcHit);
						mcHit.selectedForm = mc.selectedForm;
						mcHit.selectedColor = mc.selectedColor;
						mcHit.jewelBG.bg.myColor.setRGB(this.ref.tabColor[mc.selectedColor]);
						mcHit.jewelBG.bg.gotoAndStop(mc.selectedForm+1);
						mcHit._alpha = 100;
						mcHit.onDrag = true;
						mcHit.useHandCursor = true;
						test = false;
						this.ref.soundPlayer.playASound("btnClick",0);
					}
					i++;
				}while(test && i<9)
				mc._visible = false;
			}			
		}
		
		
		tabJewels = new Array();
		
		initCursor(false);
	}
	public function changeExoInit(nbr:Number):Void{
		//rien car un seul exo
	}
	public function switchExercice(val:Boolean):Void{

		this.p_mcCat._visible = true;

		if(!val){
			this.p_mcCoches.mc1._visible = true;
		}
		
		
	}
	public function testResult():Void{
		var nbrJewels = 0;
		for(var i=0;i<9;i++){
			var mc = this.mcCollier["j"+i];
			if(mc.onDrag) nbrJewels++
		}
		if(nbrJewels ==9){
			var te = true;
			var tabResult = new Array();
			for(var i=0;i<9;i++){
				var mc = this.mcCollier["j"+i];
				tabResult[i] = [mc.selectedColor,mc.selectedForm];
			}
			var s = this.selectedNumber;
			var t = this.firstSelectedPattern;
			for(var i=s;i>0;i--){
				t--;
				if(t<0){
					t=2;
				}
			}
			for( var i= 0;i<9;i++){
				var mc = this.mcCollier["j"+i];
				if(!mc.selecte){
					if(tabResult[i][0] != this.tabJewels[t][0] || tabResult[i][1] != this.tabJewels[t][1]){
						te = false;						
						mc._alpha = 0;
						mc.selectedForm = undefined;
						mc.selectedColor = undefined;
						mc.onDrag = false;
						mc.selecte = false;
						mc.useHandCursor = false;
					}
				}
				t++;
				if (t==3) {
					t=0;
				}
			}
			
			if(!te){
				this.p_mcResults.modifyFalseResult();
			}else{
				this.p_mcResults.modifyGoodResult();
				p_isPlaying = false;
				p_txtStart.text = "Départ";
			}
		}else{
			this.soundPlayer.playASound("fincollier",0);
		}
	}
	private function initCursor(val:Boolean):Void{
		mcConstructor._visible = val;
	}
	public function createNewJewel():Void{
		this.selectedJewel._visible = true;
		this.selectedJewel.jewelBG.bg.myColor = new Color(this.selectedJewel.jewelBG.bg);
		this.selectedJewel.jewelBG.bg.myColor.setRGB(this.tabColor[mcConstructor.selectedColor]);
		this.selectedJewel.jewelBG.bg.gotoAndStop(mcConstructor.selectedForm+1);
		this.selectedJewel.selectedColor = mcConstructor.selectedColor;
		this.selectedJewel.selectedForm = mcConstructor.selectedForm;
		this.selectedJewel._x = this.jewelT._x;
		this.selectedJewel._y = this.jewelT._y;
	}
	public function initializeGameInterface():Void{
		actualTryExo = 1;
		
		this.p_mcResults.checkResultAndReset();
		this.mcConstructor.selectedColor = undefined;
		this.mcConstructor.selectedForm = 0;
		this.jewelT.gotoAndStop(1);
		this.jewelT.myColor.setRGB(0x999999);
		initCursor(true);
		for(var i=0;i<9;i++){
			var mc = this.mcCollier["j"+i];
			mc._alpha = 0;
			mc.selectedForm = undefined;
			mc.selectedColor = undefined;
			mc.onDrag = false;
			mc.selecte = false;
			mc.useHandCursor = false;
		}
		var tabC = [0,1,2,3,4];
		var tabF = [0,1,2,3,4];
		for(var i = 0;i<3;i++){
			this.tabJewels[i] = [Number(tabC.splice(random(tabC.length),1).toString()),Number(tabF.splice(random(tabF.length),1).toString())];
		}		
		mcPattern._visible = true;
		for(var i = 0;i<3;i++){
			var mc = this.mcPattern["j"+i].jewelBG.bg;
			mc.myColor.setRGB(this.tabColor[this.tabJewels[i][0]]);
			mc.gotoAndStop(this.tabJewels[i][1]+1);
		}
		this.firstSelectedPattern = random(3);
		this.selectedNumber = random(9);
		var mc = this.mcCollier["j"+this.selectedNumber];
		mc._alpha = 100;
		mc.selectedForm = this.tabJewels[this.firstSelectedPattern][1];
		mc.selectedColor = this.tabJewels[this.firstSelectedPattern][0];
		mc.onDrag = true;
		mc.selecte = true;
		mc.jewelBG.bg.myColor.setRGB(this.tabColor[mc.selectedColor]);
		mc.jewelBG.bg.gotoAndStop(mc.selectedForm+1);
	}
}

﻿/**
 * 
 * class com.maths1p4p.level1.game1_20.game1_20
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level1.game1_20.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_20.Game1_20 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	public var p_txtName:TextField;
	public var p_txtStart:TextField;
	
	

	public var p_btnStart:Button;
	public var p_btnOne:Button;

	public var p_btnTwo:Button;
	public var p_btnThree:Button;

	
	public var p_mcOne:Button;
	public var p_mcTwo:Button;
	public var p_mcThree:Button;
	
	public var mcDisplay:MovieClip;
	public var mcDisplayCadreAndRotate:MovieClip;
	public var mcConstructor:MovieClip;
	public var selectedSquare:MovieClip;
	public var p_mcCoches:MovieClip;
	public var p_mcCat:MovieClip;
	public var mcIntro:MovieClip;
	
	
	public var p_isPlaying:Boolean;
	



	public var actualTry:Number;
	public var actualTryExo:Number;
	public var p_mcResults:level1_resultat;
	public var selectedGame:Number;
	public var nameGames:Array = [1,2,3];
	
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	private var myInterval:Number;
	private var nbrTry:Number;
	private var selectedNumber:Number;
	private var oldSelectedGame:Number;
	private var tabPuzzle:Array;
	private var tabExceptions:Array;
	private var selectedTabOfPuzzle:Array;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_20()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 0;
		actualTryExo = 0;
		p_mcCat = this.attachMovie("mcCat","p_mcCat",34);
		p_mcCat._visible = false;
		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		p_mcCoches.mc3._visible = false;
		p_txtStart.text = "Départ";

		this.oldSelectedGame = 1;
		this.selectedNumber = -1;
		
		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		
		
		nbrTry = 1;
		p_mcResults = new level1_resultat(this,this.nbrTry);		
		
		
		mcDisplayCadreAndRotate._visible = false;
		
		
		p_btnStart.onPress = function(){
			_parent.soundPlayer.playASound("btnClick",0);
			if(!_parent.p_isPlaying){
				_parent.p_mcCat._visible = false;
				_parent.p_isPlaying = true;
				_parent.initializeGameInterface();
				_parent.p_txtStart.text = "J'ai fini";
				_parent.p_btnOne.useHandCursor = false;
				_parent.p_btnTwo.useHandCursor = false;
				_parent.p_btnThree.useHandCursor = false;
			}else{
				_parent.testResult();
			}
		}
		
		p_btnOne.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOne._visible= true;
				_parent.p_mcTwo._visible= false;
				_parent.p_mcThree._visible= false;
				_parent.selectedGame = 1;
			}			
		}
		
		p_btnTwo.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOne._visible= false;
				_parent.p_mcTwo._visible= true;
				_parent.p_mcThree._visible= false;
				_parent.selectedGame = 2;
			}			
		}
		p_btnThree.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOne._visible= false;
				_parent.p_mcTwo._visible= false;
				_parent.p_mcThree._visible= true;
				_parent.selectedGame = 3;
			}			
		}
		mcDisplayCadreAndRotate.btnCa.onPress = function(){
			if(_parent._parent.p_isPlaying){
				_parent._parent.soundPlayer.playASound("btnClick",0);
				if(!_parent.displayCadre){
					_parent.displayCadre = true;
					_parent.mcCa._visible = true;
				}else{
					_parent.displayCadre = false;
					_parent.mcCa._visible = false;
				}
				_parent._parent.afficheCadre(_parent.displayCadre);
			}
		}
		mcDisplayCadreAndRotate.btnRotateLeft.onPress = function(){
			if(_parent._parent.p_isPlaying){
				_parent._parent.soundPlayer.playASound("btnClick",0);
				_parent._parent.selectedSquare._rotation -=90;
			}
		}
		mcDisplayCadreAndRotate.btnRotateRight.onPress = function(){
			if(_parent._parent.p_isPlaying){
				_parent._parent.soundPlayer.playASound("btnClick",0);
				_parent._parent.selectedSquare._rotation +=90;
			}
		}
		
		mcConstructor = this.createEmptyMovieClip("mcConstructor",3);
		for(var i = 1;i<=3;i++){
			var mcBase = this.mcConstructor.createEmptyMovieClip("mcBase"+i,i);
			mcBase._x = 64;
			mcBase._y = 345;
			mcBase._visible = false;
			if(i==1){
				var nbrC = 2;
				var nbrL = 1;
				var h = 64;	
				mcBase._x = 77;
				mcBase._y = 367;
			}else if(i==2){
				var nbrC = 4;
				var nbrL = 2;
				var h = 44;
			}else{
				var nbrC = 7;
				var nbrL = 2;
				var h = 44;
			}
			
			for(var j = 0;j<nbrC*nbrL;j++){
				var mc = mcBase.attachMovie("mcSelSquare"+i,"mc"+j,j+1);
				mc._x = (j-Math.floor(j/nbrC)*nbrC)*h;
				mc._y = Math.floor(j/nbrC)*h;
				mc.square.gotoAndStop(j+1);
				mc._rotation = 0;
				mc.ref = this;
				mc.onPress = function(){
					if(this.ref.p_isPlaying){
						this.ref.soundPlayer.playASound("btnClick",0);
						this.border.gotoAndStop(2);
						if(this.ref.selectedSquare != this){
							this.ref.selectedSquare.border.gotoAndStop(1);
							this.ref.selectedSquare = this;
						}
					}
				}
			}
		}
		mcDisplay = this.createEmptyMovieClip("mcDisplay",4);
		for(var i = 1;i<=3;i++){
			var mcBase = this.mcDisplay.createEmptyMovieClip("mcBase"+i,i);
			mcBase._visible = false;
			var mcSource= mcBase.createEmptyMovieClip("mcSource",1);
			if(i==1){
				var nbrC = 4;
				var nbrL = 4;
				var h = 56;
				mcSource._x = 30;
				mcSource._y = 70;
			}else if(i==2){
				var nbrC = 4;
				var nbrL = 5;
				var h = 36;
				mcSource._x = 122;
				mcSource._y = 91;
			}else{
				var nbrC = 6;
				var nbrL = 6;
				var h = 36;
				mcSource._x = 20;
				mcSource._y = 65;
			}
			
			var fond= mcSource.createEmptyMovieClip("fond",1);
			
			for(var j = 0;j<nbrC*nbrL;j++){
				var mc = mcSource.attachMovie("mcSquare"+i,"mc"+j,j+2);
				mc._x = h/2+(j-Math.floor(j/nbrC)*nbrC)*h;
				mc._y = h/2+Math.floor(j/nbrC)*h;
				mc.square.gotoAndStop(j+1);
				mc.j = j;
			}
			this.traceFond(mcSource,h*nbrC+2,h*nbrL+2);
			var mcDrop= mcBase.createEmptyMovieClip("mcDrop",2);
			var fond= mcDrop.createEmptyMovieClip("fond",1);
			if(i==2){
				mcDrop._x = mcSource._x+nbrC*h;
			}else{
				mcDrop._x = mcSource._x+nbrC*h+15;
			}			
			mcDrop._y = mcSource._y;
			for(var j = 0;j<nbrC*nbrL;j++){
				var mc = mcDrop.attachMovie("mcSquare"+i,"mc"+j,j+2);
				mc._x = h/2+(j-Math.floor(j/nbrC)*nbrC)*h;
				mc._y = h/2+Math.floor(j/nbrC)*h;
				mc.gotoAndStop(mc._totalframes);
				mc.ref = this;
				mc.j = j;
				mc.myselectedFrame = undefined;
				mc.selectedRotation = undefined;
				mc.onPress = function(){
					if(this.ref.p_isPlaying){
						var r = this.ref.selectedSquare._rotation;						
						this.ref.soundPlayer.playASound("btnClick",0);						
						if(r<0){
							r+=360;
						}
						var rTest = Math.floor(r/90);
						if(this.myselectedFrame!=this.ref.selectedSquare.square._currentframe || this.selectedRotation!=rTest){
							this.selectedRotation = rTest;
							this.myselectedFrame = this.ref.selectedSquare.square._currentframe;
							this.gotoAndStop(this.ref.selectedSquare.square._currentframe);
							this._rotation=r;
						}else{
							this.myselectedFrame = undefined;
							this.selectedRotation = undefined;
							this.gotoAndStop(this._totalframes);
						}
					}
				}
			}
			this.traceFond(mcDrop,h*nbrC+2,h*nbrL+2);
		}
		tabPuzzle = new Array();
		tabPuzzle[1] = new Array();
		tabPuzzle[1][0] = [[2, 0], [1, 3], [1, 3], [2, 1], [1, 2], [2, 1], [2, 2], [1, 0], [1, 2], [2, 0], [2, 3], [1, 0], [2, 3], [1, 1], [1, 1], [2, 2]];
		tabPuzzle[1][1] = [[1, 2], [2, 0], [2, 1], [1, 0],	[2, 0], [2, 2], [2, 3], [2, 1],	[2, 3], [2, 1], [2, 0], [2, 2], [1, 2], [2, 3], [2, 2], [1, 0]	];
		tabPuzzle[1][2] =  [[2, 0], [2, 2], [1, 0], [2, 1],	[1, 2], [2, 0], [2, 2], [1, 0],	[2, 3], [1, 2], [2, 0], [2, 2],	[2, 1], [2, 3], [2, 2], [2, 0]	];
		tabPuzzle[2] = new Array();
		tabPuzzle[2][0] = [[1, 0], [6, 0], [2, 0], [2, 0], [8, 1], [2, 0], [6, 2], [1, 0],	[8, 0], [2, 0], [3, 2], [3, 0], 	[1, 0], [8, 0], [6, 1], [6, 3],	[1, 0], [1, 0], [6, 3], [6, 0]	];
		tabPuzzle[2][1] =[[5, 2], [5, 0], [5, 2], [5, 0],	[8, 0], [2, 0], [2, 0], [2, 0],	[1, 0], [8, 0], [4, 2], [4, 0], 	[1, 0], [1, 0], [8, 0], [2, 0],	[1, 0], [1, 0], [1, 0], [7, 3]	];
		tabPuzzle[2][2] =[[6, 2], [1, 0], [1, 0], [5, 2],	[6, 1], [3, 2], [3, 0], [8, 1],	[6, 2], [1, 0], [1, 0], [2, 0], 	[6, 1], [3, 2], [3, 0], [8, 0],	[6, 2], [1, 0], [1, 0], [5, 2]	];	
		tabPuzzle[2][3] = [[7, 2], [3, 2], [3, 0], [6, 0],	[7, 2], [1, 0], [1, 0], [6, 0],	[7, 2], [1, 0], [6, 0], [2, 0], 	[7, 2], [1, 0], [6, 0], [2, 0],	[7, 2], [1, 0], [1, 0], [7, 0]	];
		tabPuzzle[2][4] = [[1, 0], [7, 0], [1, 0], [1, 0],	[1, 0], [7, 0], [5, 3], [7, 0],	[6, 3], [2, 0], [2, 0], [2, 0], 	[5, 2], [2, 0], [2, 0], [2, 0],	[1, 0], [3, 2], [3, 0], [3, 2]	];
		tabPuzzle[3] = new Array();
		tabPuzzle[3][0] = [[2, 0], [2, 0], [4, 3], [4, 3],[2, 0], [2, 0],	[2, 0], [9, 3], [1, 0], [1, 0],[9, 0], [2, 0],	[2, 0], [13, 0], [1, 0], [1, 0], [13, 0], [2, 0],	[2, 0], [9, 2], [6, 1], [6, 0],[9, 1], [2, 0],	[2, 0], [11, 1], [1, 0], [1,0],[12, 3], [2, 0],	[6, 0], [1, 0], [1, 0], [1, 0],[1, 0], [6, 1]	];
		tabPuzzle[3][1] = [[2, 0], [2, 0], [2, 0], [2, 0], [2, 0], [2, 0], [6, 0], [6, 1], [2, 0], [2, 0], [2, 0], [2, 0], [1, 0], [1, 0], [7, 1], [7, 1], [7, 1], [12, 3], [1, 0], [1, 0], [13, 0], [1, 0], [13, 0], [1, 0], [7, 2], [7, 0], [1, 0], [1, 0], [1, 0], [1, 0], [2, 0], [2, 0], [2, 0], [2, 0], [2, 0], [2, 0]];
		tabPuzzle[3][2] = [[10, 2], [7, 1], [7, 1], [7, 1], [7, 1], [10, 3], [7, 0], [13, 0], [14, 0], [14, 0], [13, 0], [7, 2], [7, 0], [14, 0], [9, 3], [9, 0], [14, 0], [7, 2], [7, 0], [14, 0], [9, 2], [9, 1], [14, 0], [7, 2], [7, 0], [13, 0], [14, 0], [14, 0], [13, 0], [7, 2], [10, 1], [7, 3], [7, 3], [7, 3], [7, 3], [10, 0]];
		tabPuzzle[3][3] = [[2, 0], [2, 0], [2, 0], [2, 0], [3, 0], [2, 0], [2, 0], [6, 0], [6, 1], [2, 0], [3, 2], [2, 0], [6, 0], [10, 0], [10, 1], [6, 1], [3, 0], [3, 0], [1, 0], [10, 3], [10, 2], [1, 0], [1, 0], [1, 0], [1, 0], [11, 3], [12, 1], [1, 0], [1, 0], [1, 0], [2, 0], [2, 0], [2, 0], [2, 0], [2, 0], [2, 0]];
		tabExceptions = new Array();
		tabExceptions[1] = new Array();
		tabExceptions[2] = [1,2];
		tabExceptions[3] = [1,2,13,14];
		initCursor(false);
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			p_mcOne._visible= false;
			p_mcTwo._visible= true;
			p_mcThree._visible= false;
			selectedGame = 2;
		}else if(nbr == 1){
			p_mcOne._visible= false;
			p_mcTwo._visible= false;
			p_mcThree._visible= true;
			selectedGame = 3;
		}else{
			p_mcOne._visible= true;
			p_mcTwo._visible= false;
			p_mcThree._visible= false;
			selectedGame = 1;
		}
		p_mcCat._visible = false;
	}
	public function switchExercice(val:Boolean):Void{

		this.p_mcCat._visible = true;
		if(this.selectedGame == 1){
			p_mcOne._visible= false;
			p_mcTwo._visible= true;
			p_mcThree._visible= false;
			this.selectedGame = 2;
			if(!val){
				this.p_mcCoches.mc1._visible = true;
			}
		}else if(this.selectedGame == 2){
			p_mcOne._visible= false;
			p_mcTwo._visible= false;
			p_mcThree._visible= true;
			this.selectedGame = 3;
			if(!val){
				this.p_mcCoches.mc2._visible = true;
			}
		}else{
			p_mcOne._visible= true;
			p_mcTwo._visible= false;
			p_mcThree._visible= false;
			this.selectedGame = 1;
			if(!val){
				this.p_mcCoches.mc3._visible = true;
			}
		}
		
		
	}
	private function initMcDrop(mc:MovieClip):Void{
		mc.selectedRotation = undefined;
		mc.myselectedFrame = undefined;
		mc.gotoAndStop(mc._totalframes);
	}
	public function testResult():Void{
		var te = true;
		this.selectedTabOfPuzzle = tabPuzzle[this.selectedGame][this.selectedNumber];			
		var mcBase = this.mcDisplay["mcBase"+this.selectedGame];
		var mcDrop = mcBase.mcDrop;
		var tComplet = 0;
		var nbTot = -1;
		for(var j in mcDrop){
			if(j!="fond"){
				var mc = mcDrop[j];
				nbTot++;
				if(mc.myselectedFrame !=undefined){
					tComplet++;
				}
			}
		}
		trace(tComplet+":"+nbTot);
		if(tComplet != nbTot){
			this.soundPlayer.playASound("rempCases",0);
		}else{
			for(var j in mcDrop){
				if(j!="fond"){
					var mc = mcDrop[j];
					var ignoreRot = false;
					var c = mc.j;
					if(this.selectedGame==2){
						c = 3-c+2*Math.floor(c/4)*4;
					}
					for(var i in tabExceptions[this.selectedGame]){
						if(selectedTabOfPuzzle[c][0] == tabExceptions[this.selectedGame][i]) {
							ignoreRot = true;
						}
					}
					
					if(!ignoreRot){
						var t = mcDrop[j].selectedRotation;
						if(this.selectedGame==2){
							tab = [0,0,0,0];
							if(selectedTabOfPuzzle[c][0] <=5){
								var tab = [2,3,0,1];
							}else if(selectedTabOfPuzzle[c][0] == 6){
								var tab = [1,0,3,2];
							}else if(selectedTabOfPuzzle[c][0] == 7){
								var tab = [2,1,0,3];
							}else if(selectedTabOfPuzzle[c][0] == 8){
								var tab = [3,2,1,0];
							}
							t = tab[t];
						}
						
						if(t != selectedTabOfPuzzle[c][1]) {
							te = false;						
							initMcDrop(mcDrop[j]);
							
						}
					}
					t = mcDrop[j].myselectedFrame;
					if(t != selectedTabOfPuzzle[c][0]){
							te = false;
							initMcDrop(mcDrop[j]);
					}
				}
			}
			if(!te){
				this.p_mcResults.modifyFalseResult();
			}else{
				this.p_mcResults.modifyGoodResult();
				p_isPlaying = false;
				p_txtStart.text = "Départ";
				p_btnOne.useHandCursor = true;
				p_btnTwo.useHandCursor = true;
				p_btnThree.useHandCursor = true;
				initCursor(false);
			}
		}
	}
	public function afficheCadre(val:Boolean):Void{
		for(var i = 1;i<=3;i++){			
			var mcBase = this.mcDisplay["mcBase"+i];
			var mcSource = mcBase.mcSource;
			var mcDrop = mcBase.mcDrop;
			for(var j in mcSource){
				mcSource[j].border._visible = val;
				mcDrop[j].border._visible = val;
			}
		}
	}
	private function traceFond(mc:MovieClip,w:Number,h:Number):Void{
		mc.fond.lineStyle(2,0x222222,100);
		mc.fond.moveTo(-1,-1);
		mc.fond.lineTo(w,-1);
		mc.fond.lineTo(w,1);
		mc.fond.lineStyle(2,0xFFFFFF,100);
		mc.fond.lineTo(w,h);
		mc.fond.lineTo(-1,h);
		mc.fond.lineTo(-1,h-2);
		mc.fond.lineStyle(2,0x222222,100);
		mc.fond.lineTo(-1,-1);
	}
	private function initCursor(val:Boolean):Void{
		mcDisplayCadreAndRotate.btnCa.useHandCursor = val;
		mcDisplayCadreAndRotate.btnRotateLeft.useHandCursor = val;
		mcDisplayCadreAndRotate.btnRotateRight.useHandCursor = val;
		for(var i = 1;i<=3;i++){
			var mcBase = this.mcConstructor["mcBase"+i];			
			for(var j in mcBase){
				var mc = mcBase[j];
				mc.useHandCursor = val;
			}
			var mcBase = this.mcDisplay["mcBase"+i];
			var mcDrop = mcBase.mcDrop;
			for(var j in mcDrop){
				mcDrop[j].useHandCursor = val;
			}
		}
	}
	public function initializeGameInterface():Void{
		mcIntro._visible = false;
		actualTryExo = 1;
		this.p_mcResults.checkResultAndReset();
		initCursor(true);
		afficheCadre(true);
		mcDisplayCadreAndRotate._visible = true;
		mcDisplayCadreAndRotate.mcCa._visible = true;
		mcDisplayCadreAndRotate.displayCadre = true;
		for(var i = 1;i<=3;i++){			
			var mcBase = this.mcConstructor["mcBase"+i];
			for(var j in mcBase){
				var mc = mcBase[j];
				mc._rotation = 0;
				mc.border.gotoAndStop(1);
			}
			if(i!=this.selectedGame){
				mcBase._visible = false;
			}else{
				mcBase._visible = true;
				this.selectedSquare = mcBase.mc0;
				this.selectedSquare.border.gotoAndStop(2);
			}			
		}
		if(this.oldSelectedGame != this.selectedGame){
			this.selectedNumber = 0;
			this.oldSelectedGame = this.selectedGame;
		}else{
			this.selectedNumber++;
			if(this.selectedNumber>=tabPuzzle[this.selectedGame].length) this.selectedNumber = 0;
		}
		this.selectedTabOfPuzzle = tabPuzzle[this.selectedGame][this.selectedNumber];
		for(var i = 1;i<=3;i++){			
			var mcBase = this.mcDisplay["mcBase"+i];
			var mcDrop = mcBase.mcDrop;
			var mcSource = mcBase.mcSource;
			for(var j in mcDrop){
				initMcDrop(mcDrop[j]);
				var mc = mcSource[j];
				mc.gotoAndStop(selectedTabOfPuzzle[mc.j][0]);
				mc._rotation = selectedTabOfPuzzle[mc.j][1]*90;
			}
			if(i!=this.selectedGame){
				mcBase._visible = false;
			}else{
				mcBase._visible = true;
			}
		}
	}
}

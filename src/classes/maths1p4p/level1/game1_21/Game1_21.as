﻿/**
 * 
 * class com.maths1p4p.level1.game1_21.game1_21
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level1.game1_21.*
import maths1p4p.level1.*
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level1.game1_21.Game1_21 extends maths1p4p.AbstractGame{
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------	
	public var faux1:String;
	public var faux2:String;
	public var faux3:String;
	public var vrai:String;
	
	public var p_txtName:TextField;
	public var p_txtStart:TextField;
	
	

	public var p_btnStart:Button;
	public var p_btnOne:Button;

	public var p_btnTwo:Button;


	
	public var p_mcOne:Button;
	public var p_mcTwo:Button;
	
	public var mcPlateau:MovieClip;
	public var mcAnswer:MovieClip;
	public var mcDistrib:MovieClip;
	public var mcBallsGameTwo:MovieClip;
	public var p_mcCoches:MovieClip;
	public var p_mcCat:MovieClip;
	
	
	public var p_isPlaying:Boolean;
	



	public var actualTry:Number;
	public var actualTryExo:Number;
	public var p_mcResults:level1_resultat;
	public var selectedGame:String;
	public var nameGames:Array = ["One","Two"];

	
	
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	private var myInterval:Number;
	private var nbrTry:Number;
	private var selectedNumber:Number;
	private var tabBalls:Array;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game1_21()	{
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level1/";
		startGame();
	}	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * dmmarage du jeu, implmente
	 */
	private function startGame(){
		p_isPlaying = false;
		initInterface();
	}
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		actualTry = 0;
		actualTryExo = 0;
		p_mcCat = this.attachMovie("mcCat","p_mcCat",74);
		p_mcCat._visible = false;
		p_mcCoches.mc1._visible = false;
		p_mcCoches.mc2._visible = false;
		p_txtStart.text = "Départ";

		
		
		//create Resultat
		this.faux1 = "faux1";
		this.faux2 = "faux2";
		this.faux3 = "faux3";
		this.vrai = "vrai";
		
		
		nbrTry = 4;
		p_mcResults = new level1_resultat(this,this.nbrTry);
		
		
		
		
		p_btnStart.onPress = function(){
			
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcCat._visible = false;
				_parent.p_isPlaying = true;
				_parent.initializeGameInterface();
				_parent.p_txtStart.text = "J'ai fini";
				_parent.p_btnOne.useHandCursor = false;
				_parent.p_btnTwo.useHandCursor = false;
			}else{
				_parent.testResult();
			}
		}
		
		p_btnOne.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOne._visible= true;
				_parent.p_mcTwo._visible= false;
				_parent.selectedGame = "One";
			}			
		}
		
		p_btnTwo.onPress = function(){
			if(!_parent.p_isPlaying){
				_parent.soundPlayer.playASound("btnClick",0);
				_parent.p_mcOne._visible= false;
				_parent.p_mcTwo._visible= true;
				_parent.selectedGame = "Two";
			}			
		}
		mcAnswer = this.createEmptyMovieClip("mcAnswer",1);	
		tabBalls = new Array();
		for(var i = 0;i<5;i++){
			tabBalls[i] = new Array();
			var mc = this.mcAnswer.attachMovie("mcBall","mc"+i,i+1);
		}
		mcDistrib = this.attachMovie("mcDistrib","mcDistrib",6);
		mcDistrib._x = 17;
		mcDistrib._y = 251;
		
		mcBallsGameTwo = this.attachMovie("mcBallsGameTwo","mcBallsGameTwo",5);
		mcBallsGameTwo._visible = false;
		for(var i = 1;i<=5;i++){
			var mc = mcBallsGameTwo.attachMovie("mcBall","mc"+i,i);
			mc.ref = this;
			mc.liberated = false;
			mc._visible = false;
			mc.onDrop = undefined;
			mc.mcPlateau = this.mcPlateau;
			mc.img = i;
			mc.gotoAndStop(i);
			mc.onPress = function(){
				if(this.liberated){
					this.startDrag(true);
					this.swapDepths(5);
					if(this.onDrop !=undefined){
						this.onDrop._alpha = 0;
						this.onDrop.selecte = false;
					}
				}
			}
			mc.onRelease = function(){
				if(this.liberated){
					this.stopDrag();
					for(var i=0;i<7;i++){
						var mc = this.mcPlateau["b"+i];
						var test = false;
						for(var j in mc){
							var mcHit = mc[j];
							if(this.hitTest(mcHit) && !test && !mcHit.selecte){
								if(this.onDrop == undefined ){
									_parent.okNextBall = true;
									_parent.actualBall++;
								}
								if(this.onDrop !=undefined && this.onDrop != mcHit){
									this.onDrop._alpha = 0;
									this.onDrop.selecte = false;
								}
								this.onDrop = mcHit;
								mcHit._alpha = 100;
								mcHit.selecte = true;
								test = true;
								mcHit.gotoAndStop(this.img);
								this.xinit = mcHit._parent._x + mcHit._x;
								this.yinit = mcHit._parent._y + mcHit._y;
								this.ref.soundPlayer.playASound("btnClick",0);
							}
						}
					}
					this._x = this.xinit;
					this._y = this.yinit;
					if(!test){
						if(this.onDrop !=undefined){
							this.onDrop._alpha = 100;
							this.onDrop.selecte = true;
						}
					}
				}
			}
		}
		mcBallsGameTwo.btn.onPress = function(){
			if(_parent.okNextBall && _parent.actualBall<=5){
				_parent._parent.playNextBall();
				_parent.okNextBall = false;
			}
		}		
		
		mcAnswer._visible = false
		mcPlateau._visible = false
		this.selectedNumber = 0;
		initCursor(false);
	}
	public function changeExoInit(nbr:Number):Void{
		if(nbr == 0){
			p_mcOne._visible= false;
			p_mcTwo._visible= true;
			selectedGame = "Two";
		}else{
			p_mcOne._visible= true;
			p_mcTwo._visible= false;
			selectedGame = "One";
		}
		p_mcCat._visible = false;
	}
	public function switchExercice(val:Boolean):Void{

		this.p_mcCat._visible = true;
		if(this.selectedGame == "One"){
			p_mcOne._visible= false;
			p_mcTwo._visible= true;
			this.selectedGame = "Two";
			if(!val){
				this.p_mcCoches.mc1._visible = true;
			}
		}else{
			p_mcOne._visible= true;
			p_mcTwo._visible= false;
			this.selectedGame = "One";
			if(!val){
				this.p_mcCoches.mc2._visible = true;
			}
		}
		
		
	}
	public function playNextBall():Void{
		var mc = this.mcBallsGameTwo["mc"+this.mcBallsGameTwo.actualBall];
		mc._x=91;
		mc._y=386;
		mc._visible = true;
		this.soundPlayer.playASound("bille",0);
		this.myInterval = setInterval(this,"playBall",20);
	}
	
	private function playBall():Void{
		var mc = this.mcBallsGameTwo["mc"+this.mcBallsGameTwo.actualBall];
		var vx = (150 - mc._x)/12;
		mc._x += vx
		if(Math.abs(vx)<0.5){
			clearInterval(this.myInterval);
			trace("clear");
			mc._x = 150;
			mc.xinit = mc._x;
			mc.yinit = mc._y;
			mc.liberated = true;
		}
	}
	
	private function initCursor(val:Boolean):Void{
		for(var i=0;i<7;i++){
			var mc = this.mcPlateau["b"+i];
			for(var j in mc){
				if(this.selectedGame == "One"){
					mc[j].useHandCursor = val;	
				}							
			}
		}
	}
	
	public function testResult():Void{
		var te = true;
		var isComplet = true;
		var nbrSelecte = 0;
		for(var i=0;i<7;i++){
			var mc = this.mcPlateau["b"+i];
			for(var j in mc){
				if(mc[j].selecte) nbrSelecte++;
			}
		}
		if(nbrSelecte<5){
			isComplet = false;
		}else if(nbrSelecte>5){
			te = false;
		}{
			for(var i = 0;i<5;i++){
				var mc = this.mcPlateau["b"+tabBalls[i][1]]["b"+tabBalls[i][0]];
				if(!mc.selecte) {
					te = false;
					trace("pas selecte");
				}
				if(this.selectedGame == "Two"){
					if(mc._currentframe != i+1) {
						te=false;
						trace("pas la bonne couleur");
					}
				}
			}
		}
		if(isComplet){
			this.soundPlayer.playASound("btnClick",0);
			if(!te){
				this.p_mcResults.modifyFalseResult();
			}else{
				this.p_mcResults.modifyGoodResult();
				initCursor(false);
				p_isPlaying = false;
				p_txtStart.text = "Départ";
				p_btnOne.useHandCursor = true;
				p_btnTwo.useHandCursor = true;
			}
		}else{
			this.soundPlayer.playASound("rempperles",0);			
		}
	}
	public function initializeGameInterface():Void{
		actualTryExo = 1;
		this.p_mcResults.checkResultAndReset();
		initCursor(true);
		var tabD = new Array();
		for(var i =0; i<7;i++){
				tabD.push(i);		
		}
		this.mcAnswer._visible = true;
		for(var i = 0;i<5;i++){
			var t = Number(tabD.splice(random(tabD.length),1).toString());
			tabBalls[i] = [random(7),t];
			trace(tabBalls[i]);
			var mc = this.mcAnswer["mc"+i];
			mc._x = 16+tabBalls[i][0]*29;
			mc._y = 63+tabBalls[i][1]*29;
			if(this.selectedGame == "Two"){
				mc.gotoAndStop(i+1);
			}else{
				mc.gotoAndStop(1);
			}
		}
		this.selectedNumber++;
		if(this.selectedNumber==4){
			this.selectedNumber = 1;
		}
		mcBallsGameTwo._visible = false;
		mcPlateau._visible = true;		
		mcPlateau.gotoAndStop(this.selectedNumber);
		for(var i=0;i<7;i++){
			var mc = this.mcPlateau["b"+i];
			for(var j in mc){
				mc[j]._alpha = 0;
				mc[j].selecte = false;
				mc[j].selectedColor = undefined;
				mc[j].gotoAndStop(1);
				mc[j].ref = this;
				if(this.selectedGame == "One"){
					mc[j].onPress = function(){
						if(this.ref.p_isPlaying){
							this.ref.soundPlayer.playASound("btnClick",0);
							if(this.selecte){
								this.selecte = false;
								this._alpha = 0;
							}else{
								this.selecte = true;
								this._alpha = 100;
							}
						}
					}
				}else{
					delete mc[j].onPress;
				}
			}
		}
		if(this.selectedGame == "Two"){
			mcBallsGameTwo._visible = true;
			mcBallsGameTwo.actualBall = 1;
			mcBallsGameTwo.okNextBall = true;
			for(var i = 1;i<=5;i++){
				var mc = mcBallsGameTwo["mc"+i];
				mc.liberated = false;
				mc._visible = false;
				mc.onDrop = undefined;
			}
		}
		
	}
}

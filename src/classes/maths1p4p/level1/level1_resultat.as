﻿/**
 * PERMET DE GERER L'AFFICHAGE DU RESULTAT
 * 
 * @author Fred FAUQUETTE (Fredfoc)
 * @version 0.1
 * 
 * ETAT : Fini
 * 
 * Copyright (c) 2008 Fred FAUQUETTE (Fredfoc).
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

class maths1p4p.level1.level1_resultat{
	private var _t:MovieClip;
	private var _nbr:Number;
	private var _mcResults:MovieClip;
	private var _isBomb:Boolean;
	private var myInterval:Number;
	private var asMadeError:Boolean;
	private var nbrError:Number;
	static private var _heightMeche:Number = 112;
	static private var gameTab:Array = [2,2,3,2,1,3,1,1,3,3,2,2,5,1,2,2,2,2,1,3,2];
	
	function level1_resultat(t:MovieClip,nbr:Number){
		this._t = t;
		this._t.p_txtName.text = _global.nameOfThePlayerLevel1;
		var tabResCheck = _global.level1ResultGame.split(" ");
		var defGame = gameTab[Number(_global.level1NbrGame)-1]-1;
		for(var i = 0;i<gameTab[Number(_global.level1NbrGame)-1];i++){
			if(Number(tabResCheck[i])==0){
				this._t.p_mcCoches["mc"+(i+1)]._visible = true;				
				defGame = i;
				trace("oki"+i);
			}
		}
		trace("attention : "+defGame);
		this._t.changeExoInit(defGame);
		if(_global.level1NbrGame !=13){
			if(this._t.isBomb != undefined){
				this._isBomb = true;
			}else{
				this._isBomb = false;
			}
			if(this._isBomb){
				this._mcResults = t.attachMovie("mcBomb","mcBomb",500);
				this._mcResults._x = 576;
				this._mcResults._y = 350;
				this._mcResults.etincelle._visible = false;
				this._mcResults.explosion._visible = false;
			}else{
				this._mcResults = t.createEmptyMovieClip("p_mcResults",500);
				this._nbr = nbr;
				for(var i = 0;i<nbr;i++){
					var te = this._mcResults.createEmptyMovieClip("res_"+i,i+1);
					var res1 = te.attachMovie("mcHeadResult","r1",1);
					var res2 = te.attachMovie("mcHeadResult","r2",2);
					res2._x = 31;
					te._y = 233+31 * i;
					te._x = 567;			
				}
			}
			this.asMadeError = false;
		}	
		this.nbrError = 0;
	}
	public function modifyFalseResult():Boolean{
		this.asMadeError = true;
		this.nbrError++;
		saveScore();
		if(this._isBomb){
			this._t.actualTry--;
			if(this.myInterval!=undefined){
				clearInterval(this.myInterval);
			}
			this.myInterval = setInterval(this,"makeMecheDiminished",100);
			this._mcResults.etincelle._visible = true;
			this._t.soundPlayer.playASound("meche",0);
			if(this._t.actualTry == 0){
				return true;
			}else{
				return false;
			}
		}else{
			this._mcResults["res_"+this._t.actualTry].r1.gotoAndStop((2+this._t.actualTryExo));
			this._t.soundPlayer.playASound(this._t["faux"+this._t.actualTryExo],0);
			this._t.actualTryExo++;
			this._t.actualTryExo = Math.min(3,this._t.actualTryExo);			
			return true;
		}
	}
	private function makeMecheDiminished():Void{
		var vy =(_heightMeche*this._t.actualTry/this._t.nbrTry - this._mcResults.barre._height)/5;
		this._mcResults.barre._height += vy;
		
		this._mcResults.etincelle._y = -this._mcResults.barre._height
		if(Math.abs(vy)<0.5){
			this._mcResults.barre._height = _heightMeche*this._t.actualTry/this._t.nbrTry;
			this._mcResults.etincelle._visible = false;
			clearInterval(this.myInterval);
			if(this._t.actualTry == 0){
				this._mcResults.explosion._visible = true;
				this._mcResults.explosion.play();
				this._t.soundPlayer.playASound("boum",0);
			}
		}
	}
	private function saveScore():Void{
		var str = "";
		var tabResCheck = _global.level1ResultGame.split(" ");
		var selActGame = undefined;
		for(var i in this._t.nameGames){
			if(this._t.nameGames[i] == this._t.selectedGame) selActGame=i
		}
		if(selActGame != undefined){			
			for( var i=0;i<tabResCheck.length;i++){
				if(i!=selActGame){
					str+=tabResCheck[i]
				}else{
					str+=String(this.nbrError);
				}
				if(i<tabResCheck.length-1) str+=" ";
			}
			_global.level1ResultGame = str;
		}else{
			str = _global.level1ResultGame;
		}
		trace("**************"+selActGame+":"+str);
		_global.myLevelOne.retrieveNodeAndSave(str);
	}
	public function modifyGoodResult():Boolean{
		if(!this._isBomb){
			this._t.soundPlayer.playASound(this._t.vrai);
			this._mcResults["res_"+this._t.actualTry].r2.gotoAndStop(2);
			this._t.actualTry++;
			if(this._t.actualTry==this._t.nbrTry){
				saveScore();
				if(!this.asMadeError){
					this._t.switchExercice(this.asMadeError);
				}
				return true
			}else{
				return false
			}
		}else{
			saveScore();
			if(!this.asMadeError){
				this._t.switchExercice(this.asMadeError);
			}
			return true
		}
		
	}
	public function checkResultAndReset():Boolean{
		if(!this._isBomb){
			if(this._t.actualTry==this._t.nbrTry){
				this.reinitializeResult();
				return true;
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
	public function reinitializeResult():Void{
		this._t.actualTry =0;
		this.asMadeError = false;
		this.nbrError = 0;
		if(this._isBomb){
			this._mcResults.barre._height = _heightMeche;
			this._t.actualTry = this._t.nbrTry;
		}else{
			for(var i in this._mcResults){
				this._mcResults[i].r1.gotoAndStop(1);	
				this._mcResults[i].r2.gotoAndStop(1);
			}
		}
	}
}
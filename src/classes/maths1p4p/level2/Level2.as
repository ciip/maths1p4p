﻿/**
 * class com.maths1p4p.level2.Level2
 * 
 * @author Rémy Gay, Yoann Augen
 * @version 1.0
 *
 * Copyright (c) 2008 Rémy Gay, Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import com.prossel.utils.CursorManager;
import maths1p4p.application.Application;
import maths1p4p.application.AbstractStorage;
import maths1p4p.level2.*;
import maths1p4p.utils.SoundPlayer;
import mx.controls.List;
import maths1p4p.AbstractPlayer;
import maths1p4p.AbstractGame;

class maths1p4p.level2.Level2 extends maths1p4p.AbstractLevel {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var jeuxCours:Number;
	public var nav:AbstractGame;
	public var txt_debug;
	public var previousScreen:String;
	//parc ou ile
	public var currentPrimaryScene:String;
	
	public var flecheCurseur:MovieClip;
	
	public var interface_mc:MovieClip;
	public var aide:MovieClip;
	public var barreInterface_mc:MovieClip;
	
	private var monApplication;
	private var mesDonnees;
	private var joueur1:Level2Player;
	private var joueur2:Level2Player;
	//liste des joueurs juste avec le nom et l'id
	private var arrPlayers:Array;
	//la liste complete des joueurs avec leurs stats/results
	private var listePlayersStats:Array;
	
	static private var p_oSoundPlayer:SoundPlayer = new SoundPlayer(); 
	
	//-------------------------------------------------------------------------
	// Private properties
	//------------------------------------------------------------------------- 
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level2() { 
		super();  
		//nav = new AbstractGame();
		//AbstractGame.p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		var p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		AbstractGame.soundPlayerFromLevel.setGlobalVolume(40);
		p_oSoundPlayer.addASound("nom",p_sSoundPath+"nom.mp3");
		p_oSoundPlayer.addASound("clicClac3",p_sSoundPath+"clicClac3.mp3");
		p_oSoundPlayer.addASound("clicClac2",p_sSoundPath+"clicClac2.mp3");
		p_oSoundPlayer.addASound("plouf",p_sSoundPath+"plouf.mp3");
		p_oSoundPlayer.addASound("psh6",p_sSoundPath+"psh6.mp3");
		p_oSoundPlayer.addASound("minitic",p_sSoundPath+"minitic.mp3");
		p_oSoundPlayer.addASound("clicClac4",p_sSoundPath+"clicClac4.mp3");
		p_oSoundPlayer.addASound("moteur2",p_sSoundPath+"moteur2.mp3");
		p_oSoundPlayer.addASound("scratch",p_sSoundPath+"scratch.mp3");
		p_oSoundPlayer.addASound("fefeClac",p_sSoundPath+"fefeClac.mp3");
		p_oSoundPlayer.addASound("clangTic",p_sSoundPath+"clangTic.mp3");
		p_oSoundPlayer.addASound("grince2",p_sSoundPath+"grince2.mp3");
		p_oSoundPlayer.addASound("pas3",p_sSoundPath+"pas3.mp3");
		this.jeuxCours = 0;
		stop(); 
	}
	
	public function onEnterFrame() {
		//charges données joueurs
		this.chargeDonnee();
 		this.onEnterFrame = undefined;
	}
	
	public function chargeDonnee() {
		this.monApplication = maths1p4p.application.Application.getInstance();
		this.mesDonnees = this.monApplication.getStorage();
		this.mesDonnees.addEventListener("onLoadPlayersList", this); 
		this.mesDonnees.addEventListener("onLoadPlayer", this); 	
		this.mesDonnees.loadPlayersList(2, 1); 
	}
	
	public function init() {
				/*		p_aPlayers = event.data.arrPlayersList;

				if (p_aPlayers != undefined) {
					//trace ("Debug Yoann "+p_aPlayers);
					for (var iPlayer:Number = 0 ; iPlayer < p_aPlayers.length ; iPlayer++){
						//displayPlayerResults(iPlayer, p_aPlayers[iPlayer].results.$game);	
					}
					
				} else {
					//trace("erreur ! ");
				}*/
		//trace ("##################### init() ##################");
		//initialisation et mémorisation de l'emplacement de départ
		this.previousScreen = "mcScreen_PlayerChoice";
		this.currentPrimaryScene = "mcScreen_001";
		
		//pour le test on arrive sur la frame du port en 1er
		this.loadScene("mcScreen_PlayerChoice"); 
		this.barreInterface_mc = this.attachMovie("mcInterface", "mcInterface", 2, {_x:0, _y:400});
		//trace ("##################### FIN init() ##################");
	}
	
	public function showCurseurFleche(rotationFleche:Number, typeCurseur:String)
	{
		switch(typeCurseur)
		{
			case "interro":
				//A CHANGER PAR LE BON CLIP !!!
				this.flecheCurseur = this.attachMovie("mcCurseurInterro", "flecheCurseur",99999);
				break;
				
			default:
				this.flecheCurseur = this.attachMovie("mcFlecheEntree", "flecheCurseur",99999);
				break;
		}
		this.flecheCurseur._x = _xmouse;
		this.flecheCurseur._y = _ymouse;
		if(rotationFleche != undefined) 
		{
				this.flecheCurseur._rotation = rotationFleche;
		}
		this.flecheCurseur.startDrag();
		//Mouse.hide();
		CursorManager.showMouse(false);

	}
	
	public function hideCurseurFleche()
	{
		this.flecheCurseur.stopDrag();
		this.flecheCurseur.removeMovieClip();
		//Mouse.show();
		CursorManager.showMouse(true);
	}
	
	public function afficherNomJeu(numeroGame:Number)
	{
		this.barreInterface_mc.showGameName(numeroGame);
	}
	
	public function cacherNomJeu()
	{
		this.barreInterface_mc.hideGameName();
	}
	
	public function loadGame(sGame:String, isUnbackable:Boolean)
	{
		super.loadGame(sGame);
		
		//on retire le clip de fleche
		this.hideCurseurFleche()
		
		//on mémorise l'ancienne interface pour pouvoir y revenir
		if(this.interface_mc != undefined && !isUnbackable)
		{
			this.previousScreen = this.interface_mc._name;
		}
		
		//trace("this )> "+this+" this.interface_mc => "+this.interface_mc)
		//puis on remove le clip de l'interface precedente		
		this.interface_mc.removeMovieClip();		
		
	}
	
	public function unloadGame()
	{
		//trace("unloadGame")
		super.unloadGame();		
	}
	
	/** 
	 * appelé lors du clique sur le bouton quitter
	 */
	public function unload()
	{
		_global.styles.ScrollSelectList.backgroundColor = 0xFFFFFF;
		//trace ("upload");
		super.unload();	
	}
	
	
	


	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	private function loadScene(nomScreen:String, isUnbackable:Boolean):Void
	{
		this.unloadGame();
		this.hideCurseurFleche()
		//et enlève aussi le mini-parchemin qu'on peut afficher avec le bouton du parchemin sur l'ile
		this["miniParchemin_mc"].removeMovieClip();
		
		if(this.interface_mc != undefined && !isUnbackable) {
			this.previousScreen = this.interface_mc._name;
		}
		this.interface_mc = this.attachMovie(nomScreen, nomScreen, 1);		
		//trace("this.interface_mc => "+this.interface_mc+' this["nomScreen"] => '+this["nomScreen"])
		
		//si on vien d'arriver sur l'ile ou le parc on change de scene principale (celle ou on revient quand
		//on clique sur le bouton phare)
		if(nomScreen == "mcScreen_001")
		{
			this.currentPrimaryScene = "mcScreen_001";
		}
		else if(nomScreen == "mcScreen_002")
		{
			this.currentPrimaryScene = "mcScreen_002";
		}
	}
	
	private function onLoadPlayersList (event:Object) {
		//trace("################ onLoadPlayersList ################");
		 
		this.arrPlayers = event.data.arrPlayersList; 
		this.init();
		//trace("################ fin onLoadPlayersList ################");
	
	}
	
	private function onLoadPlayer (event:Object) {
		//trace("################ onLoadPlayer ################");
		
		//si on se trouve sur la frame du choix du joueur 1 c'est le joueur 1 qui prendra l'info
		//sinon c'est le joueur 2
		//trace("-> "+this.interface_mc._name);
		
		if (event.data.oPlayer != undefined){
			switch(this.interface_mc._name)
			{
				case "mcScreen_PlayerChoice":
					//trace("#######  mcScreen_PlayerChoice  ########");
					this.joueur1 = event.data.oPlayer; 
					//mx.controls.Alert.show("this.joueur1 : "+this.joueur1);
					//mx.controls.Alert.show("this.joueur1.getName (AbstractPlayer) : "+this.joueur1.getName());
					//mx.controls.Alert.show("this.joueur1.getNamePlayer (Level2Player) : "+this.joueur1.getNamePlayer());
					//txtState.text = this.joueur1.getState()['test']._value;
					this.barreInterface_mc.setPlayerName(this.joueur1.getName());  
					this.updateResults(this.joueur1);
					//trace ("Liste joueur "+this.joueur1.getNamePlayer());
					//trace("########  fin mcScreen_PlayerChoice  ########");
					break;
						
	  
				
				default:
					//trace("default")
					this.joueur2 = event.data.oPlayer;
					break;
			}
		}

		//trace("################ fin onLoadPlayer ################");
		
	}
	
	public function displayResult() { 
		for (var i = 0 ; i < arrPlayers.length ; i++) {
		//	//trace ("test "+i+" : "+arrPlayers[i]);
			this.interface_mc["ligne"+(i + 1)+"_mc"].nomJoueur_txt.text = arrPlayers[i].attributes.name;
			for (var aNode:XMLNode = arrPlayers[i].results.firstChild; aNode != null; aNode = aNode.nextSibling) {
				if(aNode.nodeName != null && aNode.attributes.num > 0) { 
					//trace ("jeux "+aNode.attributes.num+" : "+aNode.attributes.finished);
					if (aNode.attributes.finished == "1") {
						this.interface_mc["ligne"+(i + 1)+"_mc"]["jeu"+aNode.attributes.num+"_mc"]._alpha = 100;
					}
				}
			}  
		}
	}
	public function displayPrint() { 
		this.interface_mc.attachMovie("mcResultsPrint", "myPrint", this.interface_mc.getNextHighestDepth(), {_x: -1000, _y: 0}); 
		var x_init = 40;
		var y_init = 75;
		for (var i = 0 ; i < arrPlayers.length ; i++) {
			this.interface_mc.myPrint.attachMovie("lignePrint", "ligne"+i, this.interface_mc.myPrint.getNextHighestDepth(), {_x: x_init, _y: y_init});
			trace (this.interface_mc.myPrint["ligne"+i]);
			y_init += 17.4;
		
			this.interface_mc.myPrint["ligne"+i].txtId.text = (i + 1);
			this.interface_mc.myPrint["ligne"+i].txtNom.text = arrPlayers[i].attributes.name;
			for (var j = 1 ; j <= 20 ; ++j) {
				this.interface_mc.myPrint["ligne"+i]["pt"+j]._visible = false;
			}		
			for (var aNode:XMLNode = arrPlayers[i].results.firstChild; aNode != null; aNode = aNode.nextSibling) {
				if(aNode.nodeName != null && aNode.attributes.num > 0) {  
					if (aNode.attributes.finished == "1") {
						this.interface_mc.myPrint["ligne"+i]["pt"+aNode.attributes.num]._visible = true;
					}
				}
			}  
		}
		var pjPage:PrintJob = new PrintJob();
		if(pjPage.start()){
			pjPage.addPage(
				this.interface_mc.myPrint,
				{xMin:0,xMax:870,yMin:0,yMax:559}, 
				{printAsBitmap:true}
			);
			pjPage.send();
			delete pjPage;
		}
		this.interface_mc.myPrint.removeMovieClip();
	}
	public function HelpPrint() { 
		var pjPage:PrintJob = new PrintJob();
		 
		if(pjPage.start()){	
			if (this.aide != undefined) {
				pjPage.addPage(
					this.aide,
					{xMin:44,xMax:596,yMin:25,yMax:425}, 
					{printAsBitmap:true}
				);
			} else {	
				pjPage.addPage(
					this.interface_mc,
					{xMin:44,xMax:596,yMin:25,yMax:425}, 
					{printAsBitmap:true}
				);
			}
			pjPage.send();
			delete pjPage;
		}
	}
	public function changeResult(id, joueur) { 
		for (var i = 0 ; i < arrPlayers.length ; i++) {
			//	on est sur le bon joueur 
			if (joueur.getId() == arrPlayers[i].attributes.idPlayer) { 
				for (var aNode:XMLNode = arrPlayers[i].results.firstChild; aNode != null; aNode = aNode.nextSibling) {
					// On est sur le bon jeu
					if (aNode.nodeName != null && aNode.attributes.num == id) { 
						aNode.attributes.finished = "1";
					}
				}
			}
		}
		joueur.save();
	}
	
	
	
	
	/************ Player fonction ***********/
	
	/** Renvoi l'attribut "finished" d'un jeu d'un joeuur */
	public function getResultGame(id, joueur) {
		//mx.controls.Alert.show("----- fonction getResultGame("+id+") -----");
		var result = joueur.getResults(); 
		//trace ("getResultGame "+result);
		for (var aNode:XMLNode = result.firstChild; aNode != null; aNode = aNode.nextSibling) {
			if(aNode.nodeName != null && aNode.attributes.num == id) { 
				// on a trouvé ce jeu dans la liste
				//trace ("jeux "+id+" finished = "+aNode.attributes.finished);
				return aNode.attributes.finished;
			}
		} 	
	}
	
	/** met une victoire dans un jeux donné et sauvegarde */
	public function setWinGame(id, joueur) {
		//mx.controls.Alert.show("----- fonction setWinGame("+id+") -----");
		var result = joueur.getResults();
		//trace ("getResultGame("+id+") "+result );
		for (var aNode:XMLNode = result.firstChild; aNode != null; aNode = aNode.nextSibling) {
			if(aNode.nodeName != null && aNode.attributes.num == id) { 
				// on a trouvé ce jeu dans la liste
				aNode.attributes.finished = "1";
			 	//trace ("Mise a jour du jeu jeux "+id+" finished = "+aNode.attributes.finished);
			}
		}
		this.changeResult(id, joueur);
		joueur.save();
	}
	
	/** Met à jour les infos d'un joueur en fonction du "state" 
	 * function temporaire 
	 */
	
	public function updateResults(joueur) { 
		//mx.controls.Alert.show("----- fonction updateResults() -----");
		
		var state = joueur.getState(); 
		 
		var result = new XML();	 
		result = joueur.getResults();
		//mx.controls.Alert.show("update results : "+result+"");
		
		result.ignoreWhite = false;
		var flag = false;
		// On supprime toute les noeuds présents apres les 20 jeux (debug)
		for (var aNode:XMLNode = result.firstChild; aNode != null; ) {
			 if(aNode.nodeName != null){ 
			 	// si on a deja parcouru les 20 jeux, le reste est du superflux, on le supprime
			 	if (flag == true) { 
			 		var temps = aNode.nextSibling;
			 		aNode.removeNode();
			 		aNode = temps;
			 		continue;
			 	}
			 	// On fixe le marqueur des 20 jeux attend
			 	if (aNode.attributes.num == 20) {
			 		flag = true; 
			 	}
			}
			aNode = aNode.nextSibling;
		}
		
		//trace ("RESULT DEBUT "+result); 
		// On parcours les 20 jeux et on met à jour la liste
		for (var i = 1 ; i <= 20 ; ++i) { 
			// on cherche si le jeu exite
			flag = false;
			for (var aNode:XMLNode = result.firstChild; aNode != null; aNode = aNode.nextSibling) {
				if(aNode.nodeName != null && aNode.attributes.num == i) { 
					// on a trouvé ce jeu dans la liste
					flag = true;
				}
			}
			// Si l'element n'a pas été trouvé, on l'ajoute
			if (flag == false) {
				//trace ("Noeud jeu "+i+" inexistant, a creer avec la valeur :"+state.childNodes[0]['jeu'+i].attributes.value);
				// On ajoute un noeud pour cette partie
				var game = state.createElement('game');
				game.attributes.finished = (state.childNodes[0]['jeu'+i].attributes.value == undefined) ? 0 : 1;
				game.attributes.num = i;
				result.appendChild(game);
			} 
		}
		//trace ("----- fin getResult() -----");
		//trace ("RESULT FIN "+result); 
		joueur.save(); 
	}
	
	public function get soundPlayer():SoundPlayer{
		
		return p_oSoundPlayer;
	}
}

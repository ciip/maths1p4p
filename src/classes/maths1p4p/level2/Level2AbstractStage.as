﻿/**
 * class com.maths1p4p.level3.Level3AbstractStage
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */


class maths1p4p.level2.Level2AbstractStage extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_xmlStage:Object;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level2AbstractStage()	{		
		stop();
	
	}
	
	/**
	 * init
	 * Initialisation de la scène
	 * @param xmlNode noeud xml contenant toutes les infos de la scène
	 */
	public function init(StageNode:Object):Void{

		p_xmlStage = StageNode;
		
		// execution du noeud init
		if(p_xmlStage.init != null){
			for(var i=0; i<p_xmlStage.init.childNodes.length; i++){
				executeAction(p_xmlStage.init.childNodes[i]);
			}
		}
		
		// affichage
		_parent._visible = true;
		
	}
	
	
	/**
	 * gotoScene
	 * @param idScene id de la scène (frame) à atteindre
	 */
	public function gotoFrame(idFrame):Void{
		gotoAndStop(idFrame);
		defineActions();
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	/**
	 * defineActions
	 * Définis dynamiquement, à chaque fois qu'on change de frame, 
	 * les actions sur tous les boutons décris dans le xml du stage courant.
	 */
	private function defineActions():Void{
		
		// traitement des évènements souris sur les hotspots
		
		for (var i=0; i<p_xmlStage["$hotspot"].length; i++){
			
			var hotspot:Object = p_xmlStage["hotspot_"+i];
			
			this[hotspot._id]["data"] = hotspot;
			
			this[hotspot._id].onRelease = function(){
				_parent.onHotspotRelease(this["data"]);
			}
			this[hotspot._id].onRollOver = function(){
				_parent.onHotspotRollOver(this["data"]);
			}
			this[hotspot._id].onRollOut = this[hotspot._id].onReleaseOutside = function(){
				_parent.onHotspotRollOut(this["data"]);
			}	
			
			// test si le hotspot doit être caché
			if(hotspot.visibleIf != null){	
				var bVisible = true;				
				for(var j=0; j<hotspot.$visibleIf.length; j++){
					switch(hotspot["visibleIf_"+j]._type){
						
						case "hasLoot":						
							if(!_parent._parent.hasLoot(hotspot["visibleIf_"+j]._value)){
								bVisible = false;
							}
							break;
							
						case "hasNotLoot":						
							if(_parent._parent.hasLoot(hotspot["visibleIf_"+j]._value)){
								bVisible = false;
							}
							break;	
							
						case "hasObject":						
							if(!_parent._parent.hasObject(hotspot["visibleIf_"+j]._value)){
								bVisible = false;
							}
							break;
							
						case "hasNotObject":						
							if(_parent._parent.hasObject(hotspot["visibleIf_"+j]._value)){
								bVisible = false;
							}
							break;	
							
						case "currSubLevelIs":
							if(_parent._parent.getCurrSubLevel() != hotspot["visibleIf_"+j]._value){
								bVisible = false;
							}						
							break;
							
					}
				}
				this[hotspot._id]._visible = bVisible;
			}
			
		}
		
		
		// traitement des évènements souris sur les jeux
		
		for (var i=0; i<p_xmlStage["$game"].length; i++){
			
			var game:Object = p_xmlStage["game_"+i];
			
			this[game._id]["data"] = game;
			
			this[game._id].bt_Game.onRelease = function(){
				_parent._parent.onGameRelease(_parent["data"]);
			}
			this[game._id].bt_Game.onRollOver = function(){
				_parent._parent.onGameRollOver(_parent["data"]);
			}
			this[game._id].bt_Game.onRollOut = this[game._id].bt_Game.onReleaseOutside = function(){
				_parent._parent.onGameRollOut(_parent["data"]);
			}		

			// execute les conditions du jeu
			executeGameCondition(game["if"], this[game._id]);

		}
		
	}
	
	
	/**
	 * onHotspotRelease
	 * release sur un hotspot
	 * @param hotspot le hotspot
	 */
	private function onHotspotRelease(hotspot:Object):Void{

		// parse les actions
		for (var i=0; i<hotspot.childNodes.length; i++){			
			executeAction(hotspot.childNodes[i]);			
		}
		
	}
	
	
	/**
	 * onHotspotRollOver
	 * rollover sur un hotspot
	 * @param hotspot le hotspot
	 */
	private function onHotspotRollOver(hotspot:Object):Void{
		
		// changement de curseur
		
	}
	
	
	/**
	 * onHotspotRollOut
	 * rollout sur un hotspot
	 * @param hotspot le hotspot
	 */
	private function onHotspotRollOut(hotspot:Object):Void{
		
		// rétablissement du curseur
		
	}
	
	
	
	/**
	 * onGametRelease
	 * release sur un jeu
	 * @param game le game
	 */
	private function onGameRelease(game:Object):Void{
		// parse les actions
		_parent._parent.showGame(game);		
	}
	
	
	/**
	 * onGameRollOver
	 * rollover sur un jeu
	 * @param game le game
	 */
	private function onGameRollOver(game:Object):Void{
		
		// changement de curseur
		_parent._parent.rollOverGame(game);
		
	}
	
	
	/**
	 * onHotspotRollOut
	 * rollout sur un jeu
	 * @param game le game
	 */
	private function onGameRollOut(game:Object):Void{
		
		// rétablissement du curseur
		
		_parent._parent.rollOutGame(game);
		
	}
	
	/**
	 * executeGameCondition
	 * execute les conditions d'affichage des jeux
	 * @param node le noeud xml a executer
	 * @param game le movieclip du jeu
	 */
	private function executeGameCondition(node:Object, game:MovieClip):Void{

		switch(node.nodeName){
			
			// ==> if
			case "if":
			
				switch (node._condition){
					
					case "potionActive":
						if(_parent._parent.hasObject("potion")){
							executeGameCondition(node["then"].firstChild, game);
						}else{
							executeGameCondition(node["else"].firstChild, game);
						}
						break;
						
					case "alreadyDone":

						if(_parent._parent.isGameDone(Number(game["data"]._idGamePlace))){
							executeGameCondition(node["then"].firstChild, game);
						}else{
							executeGameCondition(node["else"].firstChild, game);
						}
						break;
						
					case "currSubLevelIs":
						if(_parent._parent.getCurrSubLevel() == Number(node._value)){
							executeGameCondition(node["then"].firstChild, game);
						}else{
							executeGameCondition(node["else"].firstChild, game);
						}
						break;
					
				}
			
				break;
				
			// ==> action
			case "action":
			
				switch (node._type){
					
					case "setVisible":
						game._visible = new Boolean(Number(node._value));
						break;
					
				}
			
				break;
				

			
		}
		

		
	}
	
	
	/**
	 * executeAction
	 * rollout sur un hotspot
	 * @param hotspot le hotspot
	 */
	private function executeAction(action:Object):Void{

		switch (action.nodeName){
			
			case "action":

				switch(action._type){
					
					case "gotoScene":
						_parent._parent.gotoScene(action._value);
						break;
						
						
					case "gotoFrame":
						gotoFrame(action._value);
						break;
						
						
					case "getObject":
						_parent._parent.getObject(action._value);
						break;
						
					case "hideObject":
						_parent._parent.hideObject(action._value);
						break;
						
						
					case "ifHasObject":
						if(_parent._parent.hasObject(action._value)){
							for(var i=0; i<action["then"].$action.length; i++){
								executeAction(action["then"]["action_"+i]);
							}					
						}else{
							for(var i=0; i<action["else"].$action.length; i++){
								executeAction(action["else"]["action_"+i]);
							}
						}
						break;		
						
					case "hideLoot":
						// on a passé un sous niveau, on enleve les 4 objets de l'interface
						_parent._parent.hideLoot(action._value);
						break;
						
					case "setSubLevelPassed":
						_parent._parent.setSubLevelPassed(action._value);
						break;
						
						
					case "setPlayerData":						
						_parent._parent.p_oPlayerData[action._value] = true	;			
						break;							
					
					
				}
				
				break;
				
			case "if" :

				switch(action._condition){
					
					case "currSubLevelIs":
						if(_parent._parent.getCurrSubLevel() == Number(action._value) ){
							for(var i=0; i<action["then"].childNodes.length; i++){
								executeAction(action["then"].childNodes[i]);
							}					
						}else{
							for(var i=0; i<action["else"].childNodes.length; i++){
								executeAction(action["else"].childNodes[i]);
							}
						}
						
						break;
						
						
					case "subLevelPassed":
						if(_parent._parent.isSubLevelPassed(action._value)){
							for(var i=0; i<action["then"].childNodes.length; i++){
								executeAction(action["then"].childNodes[i]);
							}					
						}else{
							for(var i=0; i<action["else"].childNodes.length; i++){
								executeAction(action["else"].childNodes[i]);
							}
						}
						
						break;
						
					case "subLevelIs":
						if(_parent._parent.getCurrSubLevel() == action._value){
							for(var i=0; i<action["then"].childNodes.length; i++){
								executeAction(action["then"].childNodes[i]);
							}					
						}else{
							for(var i=0; i<action["else"].childNodes.length; i++){
								executeAction(action["else"].childNodes[i]);
							}
						}
						
						break;						
						
					case "hasNotObject":
					
						if (!_parent._parent.hasObject(action._value)){
							for(var i=0; i<action["then"].childNodes.length; i++){
								executeAction(action["then"].childNodes[i]);
							}					
						}else{
							for(var i=0; i<action["else"].childNodes.length; i++){
								executeAction(action["else"].childNodes[i]);
							}
						}	
						
						break;
						
						
					case "hasObject":
					
						if (_parent._parent.hasObject(action._value)){
							for(var i=0; i<action["then"].childNodes.length; i++){
								executeAction(action["then"].childNodes[i]);
							}					
						}else{
							for(var i=0; i<action["else"].childNodes.length; i++){
								executeAction(action["else"].childNodes[i]);
							}
						}	
						
						break;	
						
					case "hasLoot":			
						if (_parent._parent.hasLoot(action._value)){
							for(var i=0; i<action["then"].childNodes.length; i++){
								executeAction(action["then"].childNodes[i]);
							}					
						}else{
							for(var i=0; i<action["else"].childNodes.length; i++){
								executeAction(action["else"].childNodes[i]);
							}
						}								
						break;
						
					case "hasNotLoot":						
						if (!_parent._parent.hasLoot(action._value)){
							for(var i=0; i<action["then"].childNodes.length; i++){
								executeAction(action["then"].childNodes[i]);
							}					
						}else{
							for(var i=0; i<action["else"].childNodes.length; i++){
								executeAction(action["else"].childNodes[i]);
							}
						}	
						break;	
													
						
					case "lastSceneIs":					
					
						if (_parent._parent.isLastScene(action._value)){
							for(var i=0; i<action["then"].childNodes.length; i++){
								executeAction(action["then"].childNodes[i]);
							}					
						}else{
							for(var i=0; i<action["else"].childNodes.length; i++){
								executeAction(action["else"].childNodes[i]);
							}
						}						
						break;
						
					case "isPlayerData":					
						if (_parent._parent.p_oPlayerData[action._value]){
							for(var i=0; i<action["then"].childNodes.length; i++){
								executeAction(action["then"].childNodes[i]);
							}					
						}else{
							for(var i=0; i<action["else"].childNodes.length; i++){
								executeAction(action["else"].childNodes[i]);
							}
						}						
						break;						
						
					
				}
				
				break;
			
		}
		
	}	
	
	
	
	
}

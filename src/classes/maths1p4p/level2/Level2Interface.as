﻿/**
 * class com.maths1p4p.level3.Level3Interface
 * 
 * @author Rémy Gay, Yoann Augen
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Rémy Gay, Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */


class maths1p4p.level2.Level2Interface extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var mcInterface_mc:MovieClip
	public var mcParcheminSecret:MovieClip;
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_oLoots:Object;
	private var p_oObjects:Object;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level2Interface()	{		
		// initialisation des boutons
		this.initAllButtons();
		
	}
	
	
	/**
	* initNoBarre
	* n'affiche aucune barre d'interface (c'est pour les écrans pleins)
	*/
	public function initNoBarre()
	{
		this.hideBarreVierge();
		this.hideBtResultat();
		this.hideBtI();
		this.hideCopyright();
		
		this.hidePlayerName();
		this.hideBarreClassique();
		this.hidePalme(); 
		this.hideBtAide();
		this.hideSwap();
		this.hideRetour();
		this.hidePhare();
		this.hideParchemin();
		this.hideQuitter();
		
		this.hideGameName();
		
		//on cache les boutons de la barre d'interface de l'ile
		this.hideBarreIle()
		this.hideParcheminSecret();
		this.hideSwap2();
		this.hidePhare2();
	}
	
	/**
	 * initBarreBegin
	 * affiche la barre de navigation du début (avec le bouton resultat et I)
	 */
	public function initBarreBegin()
	{
		trace('barre de begin')
		
		this.showBarreVierge();
		this.showBtResultat();
		this.showBtI();
		this.showCopyright();
		
		this.hideBarreClassique();
		this.hidePalme(); 
		this.hideBtAide();
		this.hideSwap();
		this.hideRetour();
		this.hidePhare();
		this.hideParchemin();
		this.showQuitter();
		
		this.hideGameName();
		this.hidePlayerName();
		
		//on cache les boutons de la barre d'interface de l'ile
		this.hideBarreIle()
		this.hideParcheminSecret();
		this.hideSwap2();
		this.hidePhare2();
	}
	
	/**
	 * initBarreBeginGameChoice
	 * affiche la barre de navigation du début (sans les bouton resultat et I)
	 */
	public function initBarreBeginGameChoice()
	{ 
		this.showBarreVierge();
		this.hideBtResultat();
		this.hideBtI();
		this.hideCopyright();
		
		this.hideBarreClassique();
		this.hidePalme();
		this.hideBtAide();
		this.hideSwap();
		this.hideRetour();
		this.hidePhare();
		this.hideParchemin();
		this.showQuitter();
		
		this.hideGameName();
		
		//on cache les boutons de la barre d'interface de l'ile
		this.hideBarreIle()
		this.hideParcheminSecret();
		this.hideSwap2();
		this.hidePhare2();
	}
	
	
	
	///////////////////// RESERVE A LA BARRE CLASSIQUE ////////////////////////
	
	/**
	 * initBarreNavigation
	 * affiche la barre de navigation classique (sans les 4 boutons du milieu et sans parchemin/palme
	 */
	public function initBarreNavigation()
	{ 
		this.showPlayerName();
		
		this.hideBarreVierge();
		this.hideBtResultat();
		this.hideBtI();
		this.hideCopyright();
		
		this.showBarreClassique();
		this.hidePalme();
		this.hideBtAide();
		this.hideSwap();
		this.hideRetour();
		this.hidePhare();
		this.hideParchemin();
		this.showQuitter();
		
		this.hideGameName();
		
		//on cache les boutons de la barre d'interface de l'ile
		this.hideBarreIle()
		this.hideParcheminSecret();
		this.hideSwap2();
		this.hidePhare2();
	}
	
	/**
	 * initBarreNavigation
	 * affiche la barre de navigation classique (sans les 4 boutons du milieu et sans parchemin/palme
	 * parchemin est rendu inutile. On se sert à present des info du getResult()
	 */
	public function initBarreJeux(num_jeu, parchemin)
	{
		trace('barre de navig')
		
		this.hideBarreVierge();
		this.hideBtResultat();
		this.hideBtI();
		this.hideCopyright();
		
		this.showBarreClassique(); 
		if (this._parent.getResultGame(num_jeu, this._parent.joueur1) == "1") {
			this.showPalme();
		} else {
			this.hidePalme();
		} 
		this.showPlayerName();
		this.showGameName(num_jeu);
		this.showBtAide();
		this.showSwap();
		this.showRetour();
		this.showPhare();
		this.hideParchemin();
		this.showQuitter();
		
		//on cache les boutons de la barre d'interface de l'ile
		this.hideBarreIle()
		this.hideParcheminSecret();
		this.hideSwap2();
		this.hidePhare2();
	}
	
	///////////////////// RESERVE A LA BARRE ILE ////////////////////////
	
	/**
	 * initBarreNavigationIle
	 * affiche la barre de navigation de l'ile (avec les 4 boutons du milieu remplis ou non de puzzle)
	 */
	public function initBarreNavigationIle()
	{
		trace('barre de navig')
		
		this.hideBarreVierge();
		this.hideBtResultat();
		this.hideBtI();
		this.hideCopyright();
		
		this.showBarreIle()
		this.hideParcheminSecret();
		this.afficherBoutonPuzzle();
		this.showSwap2();
		this.showPhare2();
		//afficher les elements kil me manque encore
		//A FAIRE !!!!
		//afficher les différents puzzle en fonction de ce que le joueur a déjà réussi a recolter
		//A FAIRE !!!!
		
		
		this.hideBarreClassique();
		this.hidePalme();
		this.hideBtVierge();
		this.hideSwap();
		this.hideRetour();
		this.hidePhare();
		this.hideParchemin();
		this.showQuitter();
		
		this.hideGameName();
	}
	
	/**
	 * initBarreJeuxIle
	 * affiche la barre de navigation ile 
	 */
	public function initBarreJeuxIle()
	{
		trace('barre de navig')
		
		this.hideBarreVierge();
		this.hideBtResultat();
		this.hideBtI();
		this.hideCopyright();
		
		this.showBarreIle()
		this.showParcheminSecret();
		this.afficherBoutonPuzzle();
		this.showSwap2();
		this.showPhare2();
		//afficher les elements kil me manque encore
		//A FAIRE !!!!
		//afficher les différents puzzle en fonction de ce que le joueur a déjà réussi a recolter
		//A FAIRE !!!!
		
		
		this.hideBarreClassique();
		this.hidePalme();
		this.hideBtVierge();
		this.hideSwap();
		this.hideRetour();
		this.hidePhare();
		this.hideParchemin();
		this.showQuitter();
		
		this.hideGameName();
	}	
	
	/**
	 * setPlayerName
	 * @param sPlayerName le nom du joueur
	 * entre le nom du joueur
	 */
	public function setPlayerName(sPlayerName:String):Void{
		
		this["txtPlayer_txt"].text = sPlayerName;
		this["txtPlayer_txt"]._visible = false;
	}
	
	/**
	 * showPlayerName
	 * affiche le nom du joueur
	 */
	public function showPlayerName():Void{
		
		this["txtPlayer_txt"]._visible = true;
		
	}
	
	/**
	 * hideBtAide
	 * cache le bouton d'aide
	 */
	public function hideBtAide():Void{
		
		this["btAide_mc"]._visible = false;
	}

	/**
	 * showBtAide
	 * affiche le bouton d'aide
	 */
	public function showBtAide():Void{
		
		this["btAide_mc"]._visible = true;
		
	}
	
	/**
	 * hidePlayerName
	 * cache le nom du joueur
	 */
	public function hidePlayerName():Void{
		
		this["txtPlayer_txt"]._visible = false;
	}

			
	/**
	 * showGameName
	 * @param sGameName le nom du jeu
	 * affiche le nom du jeu en cours
	 */
	public function showGameName(sGameName:String):Void{
		this["txtGame_txt"].text = "Jeu "+sGameName;
	}
	
	/**
	 * hideGameName
	 * @param sGameName le nom du jeu
	 * affiche le nom du jeu en cours
	 */
	public function hideGameName():Void{
		
		this["txtGame_txt"].text = "";
		
	}
	
	/**
	 * showBarreVierge
	 * affiche la barre vierge (toute grise)
	 */
	public function showBarreVierge():Void{		
		this["mcBarreVierge"]._visible = true;		
	}
		
	
	/**
	 * hideBarreVierge
	 * cache la barre classique
	 */
	public function hideBarreVierge():Void{		
		this["mcBarreVierge"]._visible = false;		
	}
	
	/**
	 * showBtResultat
	 * affiche le bouton des résultats
	 */
	public function showBtResultat():Void{		
		this["mcResultat"]._visible = true;		
	}
		
	
	/**
	 * hideBtResultat
	 * cache le bouton des résultats
	 */
	public function hideBtResultat():Void{		
		this["mcResultat"]._visible = false;		
	}
	
	/**
	 * showBtI
	 * affiche le bouton de l'aide
	 */
	public function showBtI():Void{		
		this["mcI"]._visible = true;		
	}
		
	
	/**
	 * hideBtI
	 * cache le bouton de l'aide
	 */
	public function hideBtI():Void{		
		this["mcI"]._visible = false;		
	}
	
	/**
	 * showCopyright
	 * affiche le copyright du graphiste
	 */
	public function showCopyright():Void{		
		this["mcCopyright"]._visible = true;		
	}
		
	/**
	 * hideCopyright
	 * cache le copyright du graphiste
	 */
	public function hideCopyright():Void{		
		this["mcCopyright"]._visible = false;		
	}

	/**
	 * showBarreClassique
	 * affiche la barre classique
	 */
	public function showBarreClassique():Void{		
		this["mcBarreClassique"]._visible = true;		
	}
		
	
	/**
	 * hideBarreClassique
	 * cache la barre classique
	 */
	public function hideBarreClassique():Void{		
		this["mcBarreClassique"]._visible = false;		
	}
	
	
	/**
	 * showPalme
	 * affiche la palme représentant la réussite du jeu
	 */
	public function showPalme():Void{		
		this["palme_mc"]._visible = true;		
	}
		
	
	/**
	 * hidePalme
	 * cache la palme
	 */
	public function hidePalme():Void{		
		this["palme_mc"]._visible = false;		
	}
	
	/**
	 * showParcheminSecret
	 * affiche le parchemin ? représentant les indices donnés par les jeux
	 */
	public function showParcheminSecret():Void{		
		this["mcParcheminSecret"]._visible = true;		
	}
		
	
	/**
	 * hideParcheminSecret
	 * cache le parchemin ?
	 */
	public function hideParcheminSecret():Void{		
		this["mcParcheminSecret"]._visible = false;		
	}
	
	

	
	/**
	 * showBtVierge
	 * affiche this["btVierge_mc"]
	 */
	public function showBtVierge():Void{		
		this["btVierge_mc"]._visible = true;		
	}
		
	
	/**
	 * hideBtVierge
	 * cache this["btVierge_mc"]
	 */
	public function hideBtVierge():Void{		
		this["btVierge_mc"]._visible = false;		
	}	
	
	/**
	 * showSwap
	 * affiche this["btDoublePerso_mc"]
	 */
	public function showSwap():Void{		
		this["btDoublePerso_mc"]._visible = true;		
	}
		
	
	/**
	 * hideSwap
	 * cache this["btDoublePerso_mc"]
	 */
	public function hideSwap():Void{		
		this["btDoublePerso_mc"]._visible = false;		
	}	
	
	/**
	 * showRetour
	 * affiche this["btRetour_mc"]
	 */
	public function showRetour():Void{		
		this["btRetour_mc"]._visible = true;		
	}
		
	
	/**
	 * hideRetour
	 * cache this["btRetour_mc"]
	 */
	public function hideRetour():Void{		
		this["btRetour_mc"]._visible = false;		
	}	
	
	/**
	 * showPhare_mc
	 * affiche this["btPhare_mc"]
	 */
	public function showPhare():Void{		
		this["btPhare_mc"]._visible = true;		
	}
		
	
	/**
	 * hidePhare_mc
	 * cache this["btPhare_mc"]
	 */
	public function hidePhare():Void{		
		this["btPhare_mc"]._visible = false;		
	}	
	
	/**
	 * showParchemin
	 * affiche this["mcParchemin_mc"]
	 */
	public function showParchemin():Void{		
		this["mcParchemin_mc"]._visible = true;		
	}
		
	
	/**
	 * hideParchemin
	 * cache this["mcParchemin_mc"]
	 */
	public function hideParchemin():Void{		
		this["mcParchemin_mc"]._visible = false;		
	}	
	
	
	//************************* BOUTONS DEDIES A l'ILE
	
	
	/**
	 * showBarreIle
	 * affiche this["mcBarreIle"]
	 */
	public function showBarreIle():Void{		
		this["mcBarreIle"]._visible = true;		
	}
		
	
	/**
	 * hideBarreIle
	 * cache this["btDoublePerso_mc"]
	 */
	public function hideBarreIle():Void{		
		this["mcBarreIle"]._visible = false;		
	}	
	
	
	/**
	 * showSwap2
	 * affiche this["btDoublePerso_mc"]
	 */
	public function showSwap2():Void{		
		this["btDoublePersoIle_mc"]._visible = true;		
	}
		
	
	/**
	 * hideSwap2
	 * cache this["btDoublePerso_mc"]
	 */
	public function hideSwap2():Void{		
		this["btDoublePersoIle_mc"]._visible = false;		
	}	
			
			
	/**
	 * showPhare2_mc
	 * affiche this["btPhareIle_mc"]
	 */
	public function showPhare2():Void{		
		this["btPhareIle_mc"]._visible = true;		
	}
		
	
	/**
	 * hidePhare2_mc
	 * cache this["btPhareIle_mc"]
	 */
	public function hidePhare2():Void{		
		this["btPhareIle_mc"]._visible = false;		
	}	
	
	
	
	/**
	 * showQuitter
	 * affiche this["btQuitter_mc"]
	 */
	public function showQuitter():Void{		
		this["btQuitter_mc"]._visible = true;		
	}
		
	
	/**
	 * hideQuitter
	 * cache this["btQuitter_mc"]
	 */
	public function hideQuitter():Void{		
		this['btQuitter_mc']._visible = false;		
	}	
	
	/*
	* afficherBoutonPuzzle
	* permet d'afficher les bons morceaux de puzzles récupérés (si on en a récupéré)
	*/
	public function afficherBoutonPuzzle():Void
	{
		var posX = this["mcParcheminSecret"]._x+ 75;
		var posY = this["mcParcheminSecret"]._y;
		//trace("this._parent.joueur1.getState().childNodes[0]['jeu17'].attributes.value => "+this._parent.joueur1.getState().childNodes[0]['jeu17'].attributes.value)
		switch(this._parent.joueur1.getState().childNodes[0]['jeu17'].attributes.value)
		{
			case "1":
				this.attachMovie("mcPuzzle17_1", "btPuzzle1", 1);
				this["btPuzzle1"]._x = posX;
				this["btPuzzle1"]._y = posY;
				break;
				
			case "2":
				this.attachMovie("mcPuzzle17_2", "btPuzzle1", 1);
				this["btPuzzle1"]._x = posX;
				this["btPuzzle1"]._y = posY;
				break;
				
			case "3":
				this.attachMovie("mcPuzzle17_3", "btPuzzle1", 1);
				this["btPuzzle1"]._x = posX;
				this["btPuzzle1"]._y = posY;
				break;
				
			case "4":
				this.attachMovie("mcPuzzle17_4", "btPuzzle1", 1);
				this["btPuzzle1"]._x = posX;
				this["btPuzzle1"]._y = posY;
				break;
				
			default:
				this.attachMovie("btVierge", "btPuzzle1", 1);
				this["btPuzzle1"]._x = posX;
				this["btPuzzle1"]._y = posY;
		}
		switch(this._parent.joueur1.getState().childNodes[0]['jeu18'].attributes.value)
		{
			case "1":
				this.attachMovie("mcPuzzle18_1", "btPuzzle2", 2);
				this["btPuzzle2"]._x = posX + 60;
				this["btPuzzle2"]._y = posY;
				break;
				
			case "2":
				this.attachMovie("mcPuzzle18_2", "btPuzzle2", 2);
				this["btPuzzle2"]._x = posX + 60;
				this["btPuzzle2"]._y = posY;
				break;
				
			case "3":
				this.attachMovie("mcPuzzle18_3", "btPuzzle2", 2);
				this["btPuzzle2"]._x = posX + 60;
				this["btPuzzle2"]._y = posY;
				break;
				
			case "4":
				this.attachMovie("mcPuzzle18_4", "btPuzzle2", 2);
				this["btPuzzle2"]._x = posX + 60;
				this["btPuzzle2"]._y = posY;
				break;
				
			default:
				this.attachMovie("btVierge", "btPuzzle2", 2);
				this["btPuzzle2"]._x = posX + 60;
				this["btPuzzle2"]._y = posY;
		}
		switch(this._parent.joueur1.getState().childNodes[0]['jeu19'].attributes.value)
		{
			case "1":
				this.attachMovie("mcPuzzle19_1", "btPuzzle3", 3);
				this["btPuzzle3"]._x = posX + 120;
				this["btPuzzle3"]._y = posY;
				break;
				
			case "2":
				this.attachMovie("mcPuzzle19_2", "btPuzzle3", 3);
				this["btPuzzle3"]._x = posX + 120;
				this["btPuzzle3"]._y = posY;
				break;
				
			case "3":
				this.attachMovie("mcPuzzle19_3", "btPuzzle3", 3);
				this["btPuzzle3"]._x = posX + 120;
				this["btPuzzle3"]._y = posY;
				break;
				
			case "4":
				this.attachMovie("mcPuzzle19_4", "btPuzzle3", 3);
				this["btPuzzle3"]._x = posX + 120;
				this["btPuzzle3"]._y = posY;
				break;
				
			default:
				this.attachMovie("btVierge", "btPuzzle3", 3);
				this["btPuzzle3"]._x = posX + 120;
				this["btPuzzle3"]._y = posY;
		}
		switch(this._parent.joueur1.getState().childNodes[0]['jeu20'].attributes.value)
		{
			case "1":
				this.attachMovie("mcPuzzle20_1", "btPuzzle4", 4);
				this["btPuzzle4"]._x = posX + 180;
				this["btPuzzle4"]._y = posY;
				break;
				
			case "2":
				this.attachMovie("mcPuzzle20_2", "btPuzzle4", 4);
				this["btPuzzle4"]._x = posX + 180;
				this["btPuzzle4"]._y = posY;
				break;
				
			case "3":
				this.attachMovie("mcPuzzle20_3", "btPuzzle4", 4);
				this["btPuzzle4"]._x = posX + 180;
				this["btPuzzle4"]._y = posY;
				break;
				
			case "4":
				this.attachMovie("mcPuzzle20_4", "btPuzzle4", 4);
				this["btPuzzle4"]._x = posX + 180;
				this["btPuzzle4"]._y = posY;
				break;
				
			default:
				this.attachMovie("btVierge", "btPuzzle4", 4);
				this["btPuzzle4"]._x = posX + 180;
				this["btPuzzle4"]._y = posY;
		}
		
		this["btPuzzle1"].onRelease = function()
		{
			//si on est sur le screen de la tete de mort les boutons puzzle deviennent actif pour qu'on puisse 
			//insérer dans la tete de mort
			trace("_parent._parent.interface_mc => "+_parent._parent.interface_mc+" _parent._parent.interface_mc.name => "+_parent._parent.interface_mc.name+" _parent._parent.interface_mc.puzzle1_mc => "+_parent._parent.interface_mc.puzzle1_mc)
			if(_parent._parent.interface_mc._name == "mcScreen_031")
			{
				//si c'est le bon morceau on l'insère
				if(this._parent._parent.joueur1.getState().childNodes[0]['jeu17'].attributes.value == 1)
				{
					_parent._parent.interface_mc.puzzle1_mc._visible = true;
				}
				else
				{
					//sinon on fait le bruit de l'erreur
					//A FAIRE !!!!!
				}
			}
		}
		
		this["btPuzzle2"].onRelease = function()
		{
			//si on est sur le screen de la tete de mort les boutons puzzle deviennent actif pour qu'on puisse 
			//insérer dans la tete de mort
			trace("_parent._parent.interface_mc => "+_parent._parent.interface_mc+" _parent._parent.interface_mc.name => "+_parent._parent.interface_mc.name+" _parent._parent.interface_mc.puzzle1_mc => "+_parent._parent.interface_mc.puzzle1_mc)
			if(_parent._parent.interface_mc._name == "mcScreen_031")
			{
				//si c'est le bon morceau on l'insère
				if(this._parent._parent.joueur1.getState().childNodes[0]['jeu18'].attributes.value == 4)
				{
					_parent._parent.interface_mc.puzzle2_mc._visible = true;
				}
				else
				{
					//sinon on fait le bruit de l'erreur
					//A FAIRE !!!!!
				}
			}
		}
		
		this["btPuzzle3"].onRelease = function()
		{
			//si on est sur le screen de la tete de mort les boutons puzzle deviennent actif pour qu'on puisse 
			//insérer dans la tete de mort
			trace("_parent._parent.interface_mc => "+_parent._parent.interface_mc+" _parent._parent.interface_mc.name => "+_parent._parent.interface_mc.name+" _parent._parent.interface_mc.puzzle1_mc => "+_parent._parent.interface_mc.puzzle1_mc)
			if(_parent._parent.interface_mc._name == "mcScreen_031")
			{
				//si c'est le bon morceau on l'insère
				if(this._parent._parent.joueur1.getState().childNodes[0]['jeu19'].attributes.value == 1)
				{
					_parent._parent.interface_mc.puzzle3_mc._visible = true;
				}
				else
				{
					//sinon on fait le bruit de l'erreur
					//A FAIRE !!!!!
				}
			}
		}
		
		this["btPuzzle4"].onRelease = function()
		{
			//si on est sur le screen de la tete de mort les boutons puzzle deviennent actif pour qu'on puisse 
			//insérer dans la tete de mort
			trace("_parent._parent.interface_mc => "+_parent._parent.interface_mc+" _parent._parent.interface_mc.name => "+_parent._parent.interface_mc.name+" _parent._parent.interface_mc.puzzle1_mc => "+_parent._parent.interface_mc.puzzle1_mc)
			if(_parent._parent.interface_mc._name == "mcScreen_031")
			{
				//si c'est le bon morceau on l'insère
				if(this._parent._parent.joueur1.getState().childNodes[0]['jeu20'].attributes.value == 4)
				{
					_parent._parent.interface_mc.puzzle4_mc._visible = true;
				}
				else
				{
					//sinon on fait le bruit de l'erreur
					//A FAIRE !!!!!
				}
			}
		}

	}
	
	public function clignotementPuzzle()
	{
		
		trace("clignotement")
		
		this["clignotement_mc"].swapDepths(500);
		this["clignotement_mc"].gotoAndPlay(2);
		
		/*
		var nbClignotement = 0;
		
		var disparition:Function = function()
		{
			clearInterval(idInterval);
			
			this.cache1.removeMovieClip();
			this.cache2.removeMovieClip();
			this.cache3.removeMovieClip();
			this.cache4.removeMovieClip();
			
			if(nbClignotement < 5)
			{
				var idInterval = setInterval(affichage, 250);
			}
		}
		
		var affichage:Function = function()
		{
			clearInterval(idInterval);
			nbClignotement++;
			
			this.attachMovie("mcRondNoir", "cache1", 10);
			this.cache1._x = 197;
			this.cache1._y = 10;
			
			this.attachMovie("mcRondNoir", "cache2", 11);
			this.cache2._x = 197 + 60;
			this.cache2._y = 10;
			
			this.attachMovie("mcRondNoir", "cache3", 12);
			this.cache3._x = 197 + 120;
			this.cache3._y = 10;
			
			this.attachMovie("mcRondNoir", "cache4", 13);
			this.cache4._x = 197 + 180;
			this.cache4._y = 10;
			
			trace("this.cache4 => "+this.cache4)
			
			var idInterval = setInterval(disparition, 250);
		}
		
		var idInterval = setInterval(affichage, 250);
		*/
		
	}
	

	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	private function initAllButtons():Void{
		trace ("############ initAllButtons() ##########");
		// bouton de changement de perso
		this["btDoublePerso_mc"].onRelease = function(){
			
			//_parent._parent._parent => level2.fla
			_parent._parent.unloadGame();
			_parent.unsetPlayerName();
			_parent._parent.joueur1 = undefined;
			_parent._parent.joueur2 = undefined;
			_parent._parent.loadScene("mcScreen_PlayerChoice");
			//trace("retour à l'écran de selection des joueurs");
		}
		
		// bouton de changement de perso
		this["btDoublePersoIle_mc"].onRelease = function(){
			
			//_parent._parent._parent => level2.fla
			_parent._parent.unloadGame();
			_parent.unsetPlayerName();
			_parent._parent.joueur1 = undefined;
			_parent._parent.joueur2 = undefined;
			//et vu qu'on vien de l'ile on oublie pas d'enlever les clips puzzle dynamiques
			_parent.btPuzzle1.removeMovieClip();
			_parent.btPuzzle2.removeMovieClip();
			_parent.btPuzzle3.removeMovieClip();
			_parent.btPuzzle4.removeMovieClip();
			_parent._parent.loadScene("mcScreen_PlayerChoice");
			//trace("retour à l'écran de selection des joueurs");
		}
		
		// bouton retour
		this["btRetour_mc"].onRelease = function(){
			//trace(_parent._parent.previousScreen)
			//trace(_parent._parent.currentPrimaryScene)
			_parent._parent.unloadGame();
			_parent._parent.loadScene(_parent._parent.previousScreen, true);
		}
		
		// bouton reload
		this["btPhare_mc"].onRelease = function(){
			_parent._parent.unloadGame();
			_parent._parent.loadScene(_parent._parent.currentPrimaryScene);
		}
		
		// bouton reload
		this["btPhareIle_mc"].onRelease = function(){
			_parent._parent.unloadGame();
			//et vu qu'on vien de l'ile on oublie pas d'enlever les clips puzzle dynamiques
			_parent.btPuzzle1.removeMovieClip();
			_parent.btPuzzle2.removeMovieClip();
			_parent.btPuzzle3.removeMovieClip();
			_parent.btPuzzle4.removeMovieClip();
			_parent._parent.loadScene(_parent._parent.currentPrimaryScene);
		}
		
		// bouton quitter 
		this["btQuitter_mc"].onRelease = function(){ 
			_parent._parent.unload();
		}
		this["btQuitter_mc"].onRelease = function(){
			// quitter le niveau. AbstractLevel affiche le dialogue de confirmation.
			_parent._parent.unload();
		}
		
		//bouton parchemin secret de l'ile 
			var mouseListener:Object = new Object(); 
			mouseListener.onMouseDown = function() { 
				/*trace ("clic "+_parent._xmouse+" >= "+ _root.mcInterface_mc["mcParcheminSecret"]._x +" && "+ 
					_root.mcInterface_mc._xmouse+" <= "+_root.mcInterface_mc["mcParcheminSecret"]._x  +" + "+ _root.mcInterface_mc["mcParcheminSecret"]._width +" && "+ 
					_root.mcInterface_mc._ymouse+" >= "+_root.mcInterface_mc["mcParcheminSecret"]._y +" && "+
					_root.mcInterface_mc._ymouse+" <= "+_root.mcInterface_mc["mcParcheminSecret"]._y +" + "+ _root.mcInterface_mc["mcParcheminSecret"]._height);
		 
				trace ("clic "+_root.mcInterface_mc._xmouse+" >= "+ _root.mcInterface_mc["mcParcheminSecret"]._x +" && "+ 
					_root.mcInterface_mc._xmouse+" <= "+_root.mcInterface_mc["mcParcheminSecret"]._x  +" + "+ _root.mcInterface_mc["mcParcheminSecret"]._width +" && "+ 
					_root.mcInterface_mc._ymouse+" >= "+_root.mcInterface_mc["mcParcheminSecret"]._y +" && "+
					_root.mcInterface_mc._ymouse+" <= "+_root.mcInterface_mc["mcParcheminSecret"]._y +" + "+ _root.mcInterface_mc["mcParcheminSecret"]._height);
		 
				trace ("clic "+_root.mcInterface_mc._xmouse+" >= "+ _root.mcInterface_mc["mcParcheminSecret"]._x +" && "+ 
					_root.mcInterface_mc._xmouse+" <= "+_root.mcInterface_mc["mcParcheminSecret"]._x  +" + "+ _root.mcInterface_mc["mcParcheminSecret"]._width +" && "+ 
					_root.mcInterface_mc._ymouse+" >= "+_root.mcInterface_mc["mcParcheminSecret"]._y +" && "+
					_root.mcInterface_mc._ymouse+" <= "+_root.mcInterface_mc["mcParcheminSecret"]._y +" + "+ _root.mcInterface_mc["mcParcheminSecret"]._height);
		 
				if (_root.mcInterface_mc["mcParcheminSecret"]._x != undefined && 
					_root.mcInterface_mc._xmouse >= _root.mcInterface_mc["mcParcheminSecret"]._x && 
					_root.mcInterface_mc._xmouse <= (_root.mcInterface_mc["mcParcheminSecret"]._x  + _root.mcInterface_mc["mcParcheminSecret"]._width) && 
					_root.mcInterface_mc._ymouse >= _root.mcInterface_mc["mcParcheminSecret"]._y && 
					_root.mcInterface_mc._ymouse <= (_root.mcInterface_mc["mcParcheminSecret"]._y  + _root.mcInterface_mc["mcParcheminSecret"]._height) ){
		 
					if(_root.mcInterface_mc.miniParchemin_mc == undefined) {
						_root.mcInterface_mc.attachMovie("mcMiniParchemin", "miniParchemin_mc", 1000);
						_root.mcInterface_mc.miniParchemin_mc._x = 475;
						_root.mcInterface_mc._parent.miniParchemin_mc._y = 0;
						_root.mcInterface_mc._parent.miniParchemin_mc._visible = true; 
					} else if(_root.mcInterface_mc.miniParchemin_mc._visible == false)	{
						_root.mcInterface_mc.miniParchemin_mc._visible = true;
					} else {
						_root.mcInterface_mc.miniParchemin_mc._visible = false;
					}
				}*/
			};
			Mouse.addListener(mouseListener); 
		/*this["mcParcheminSecret"].onRelease = function()
		{
			trace ("Debug Yoann release");
			trace("_parent._parent => "+_parent._parent)
			if(_parent._parent.miniParchemin_mc == undefined)
			{
				_parent._parent.attachMovie("mcMiniParchemin", "miniParchemin_mc", 1000);
				_parent._parent.miniParchemin_mc._x = 475;
				_parent._parent.miniParchemin_mc._y = 0;
				_parent._parent.miniParchemin_mc._visible = true;
				trace("_parent._parent.miniParchemin_mc => "+_parent._parent.miniParchemin_mc)
			}
			else if(_parent._parent.miniParchemin_mc._visible == false)
			{
				_parent._parent.miniParchemin_mc._visible = true;
			}
			else
			{
				_parent._parent.miniParchemin_mc._visible = false;
			}
		} */
		// bouton d'information
		this["mcI"].onRelease = function()
		{
			_parent._parent.loadScene("mcScreen_Aide01");
			_parent.initNoBarre();
		}
		
		//bouton pour l'affichage du menu des resultats et gestion de classe
		this["mcResultat"].onRelease = function(){
			
			_parent._parent.unloadGame();
			_parent._parent.loadScene("mcScreen_Resultats");
			_parent.initNoBarre();
		}
		
		
		// Bouton aide
		this["btAide_mc"].onRelease = function() {
			//_parent._parent.loadScene("mcScreen_Aide01");
			_parent._parent.attachMovie("aide", "aide", _parent._parent.getNextHighestDepth());
			 
			trace (_parent._parent.jeuxCours); 
			if (_parent._parent.jeuxCours > 0) {
				var page:Number = _parent._parent.jeuxCours + 5;
				trace (page); 
				_parent._parent.aide.gotoAndStop(page);
			}
			//_parent.initNoBarre();
		}
		trace ("############ fin initAllButtons() ##########");
		
	}
	
	
}

﻿/**
 * class com.maths1p4p.level2.Level2Player
 * 
 * @author Yoann Augen
 * @version 1.0
 *
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 

import XMLShortcuts;
 
class maths1p4p.level2.Level2Player extends maths1p4p.AbstractPlayer {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------	

	private var score:Number;
	private var nbreCoupJoues:Number;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level2Player(sName:String, iId:Number) {
		super(sName, iId); 
		//mx.controls.Alert.show("----- fonction Level2Player() -----");
		
		
		this.score = 0;
		this.nbreCoupJoues = 0;
		
	}
	public function getName():String {
		//mx.controls.Alert.show("----- fonction getNamePlayer() -----"); 
		return super.getName();
	}
	
	public function getScore():Number {
		
		return this.score;
	}
	
	public function addScore() {
		
		this.score++;
	}
	
	public function setScore(score:Number) {
		
		this.score = score;
	}
	
	public function getNbreCoupJoues():Number {
		
		return this.nbreCoupJoues;
	}
	
	public function addNbreCoupJoues() {
		
		this.nbreCoupJoues ++;
	}
	
	public function setNbreCoupJoues(nbreCoupJoues:Number) {
		
		this.nbreCoupJoues = nbreCoupJoues;
	}
	
	public function setState(xmlState:XMLNode){
		super.setState(xmlState);
		super.save();
	}
	
	public function getState(){ 
		//mx.controls.Alert.show("----- fonction getState() -----");
		return super.getState();
	}
	
	public function getResults(){ 
		
		//mx.controls.Alert.show("----- fonction getResults() -----");
		return super.getResults();
	}
	
	/** Renvoi l'attribut "finished" d'un jeu */
	/*public function getResultGame(id) {
		//mx.controls.Alert.show("----- fonction getResultGame("+id+") -----");
		var result = this.getResults();
		for (var aNode:XMLNode = result.firstChild; aNode != null; aNode = aNode.nextSibling) {
			if(aNode.nodeName != null && aNode.attributes.num == id) { 
				// on a trouvé ce jeu dans la liste
				//trace ("jeux "+id+" finished = "+aNode.attributes.finished);
				return aNode.attributes.finished;
			}
		} 	
	} */
	
	/** met une victoire dans un jeux donné et sauvegarde */
	/*public function setWinGame(id) {
		//mx.controls.Alert.show("----- fonction setWinGame("+id+") -----");
		var result = this.getResults();
		for (var aNode:XMLNode = result.firstChild; aNode != null; aNode = aNode.nextSibling) {
			if(aNode.nodeName != null && aNode.attributes.num == id) { 
				// on a trouvé ce jeu dans la liste
				aNode.attributes.finished = "1";
			//	//trace ("Mise a jour du jeu jeux "+id+" finished = "+aNode.attributes.finished);
			}
		}
		super.save();
	} */
	
	/** Met à jour les infos d'un joueur en fonction du "state" 
	 * function temporaire 
	 */
	
	/*public function updateResults() { 
		//mx.controls.Alert.show("----- fonction updateResults() -----");
		
		var state = super.getState(); 
		 
		var result = new XML();	 
		result = this.getResults();
		//mx.controls.Alert.show("update results : "+result+"");
		
		result.ignoreWhite = false;
		var flag = false;
		// On supprime toute les noeuds présents apres les 20 jeux (debug)
		for (var aNode:XMLNode = result.firstChild; aNode != null; ) {
			 if(aNode.nodeName != null){ 
			 	// si on a deja parcouru les 20 jeux, le reste est du superflux, on le supprime
			 	if (flag == true) { 
			 		var temps = aNode.nextSibling;
			 		aNode.removeNode();
			 		aNode = temps;
			 		continue;
			 	}
			 	// On fixe le marqueur des 20 jeux attend
			 	if (aNode.attributes.num == 20) {
			 		flag = true; 
			 	}
			}
			aNode = aNode.nextSibling;
		}
		
		//trace ("RESULT DEBUT "+result); 
		// On parcours les 20 jeux et on met à jour la liste
		for (var i = 1 ; i <= 20 ; ++i) { 
			// on cherche si le jeu exite
			flag = false;
			for (var aNode:XMLNode = result.firstChild; aNode != null; aNode = aNode.nextSibling) {
				if(aNode.nodeName != null && aNode.attributes.num == i) { 
					// on a trouvé ce jeu dans la liste
					flag = true;
				}
			}
			// Si l'element n'a pas été trouvé, on l'ajoute
			if (flag == false) {
				//trace ("Noeud jeu "+i+" inexistant, a creer avec la valeur :"+state.childNodes[0]['jeu'+i].attributes.value);
				// On ajoute un noeud pour cette partie
				var game = state.createElement('game');
				game.attributes.finished = (state.childNodes[0]['jeu'+i].attributes.value == undefined) ? 0 : 1;
				game.attributes.num = i;
				result.appendChild(game);
			} 
		}
		//trace ("----- fin getResult() -----");
		//trace ("RESULT FIN "+result); 
		super.save(); 
	}*/
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------


}

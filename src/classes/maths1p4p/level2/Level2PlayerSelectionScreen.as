﻿/**
 * class com.maths1p4p.level3.Level3PlayerSelectionScreen
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * 
 * TODO :
 * 
 * 
 * Copyright (c) 2008 Grégory Lardon.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import mx.utils.Delegate;
import mx.controls.List;
import maths1p4p.level2.Level2Player;

class maths1p4p.level2.Level2PlayerSelectionScreen extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	/** bouton servant à désactiver les clics sur les écrans au dessous */
	public var p_btBG:Button;
	/** bouton suite */
	public var p_btSuite:Button;
	/** composant liste, liste des joueurs */
	public var p_lstPlayers:List;
	


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------


	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level2PlayerSelectionScreen()	{	  

		
		_visible = false;
		//trace (this._visible);
		// bouton servant à désactiver les clics sur les écrans au dessous
		p_btBG.useHandCursor = false;
		p_btBG.onRelease = function(){}
		
		p_btSuite.onRelease = function(){
			_parent.validateSelection();
		}
		
		// TMP !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		this.onEnterFrame = function(){
			 
			if (this._parent._parent.arrPlayers != undefined) {
				p_lstPlayers.removeAll();
	
				// récupère les noms des classes dans le xml et les affiche dans la liste
				for(var iPlayer in this._parent._parent.arrPlayers){
					var xmlPlayer = this._parent._parent.arrPlayers[iPlayer]; 
					p_lstPlayers.addItem({label: xmlPlayer._name, data: xmlPlayer._idPlayer});
					//trace ("ajout de "+xmlPlayer._name+" ("+xmlPlayer._idPlayer+")");
				}
	
				if (p_lstPlayers.length) {
					this.mesDonnees.loadTeacher(this._parent._parent.arrPlayers[0]._idTeacher);
					this.monApplication.setCurrentClass(this._parent._parent.arrPlayers[0]._idClass);
					
					/*if (_parent.liste_init == undefined) { 
						// cas des listes des jeux
						p_lstPlayers.selectedIndex = 0; 
					} else { */
						// cas de la page d'accueil
						p_lstPlayers.selectedIndex = false; 
					//}
					//onPlayerChange();
				}
			} else {
				p_lstPlayers.addItem({label: "Erreur"});
			}
			
			//trace("p_lstPlayers => "+p_lstPlayers+" this.p_lstPlayers => "+this.p_lstPlayers)
			
			/*
			p_lstPlayers.addItem("joueur 1",1);
			p_lstPlayers.addItem("joueur 2",2);
			*/
			delete this.onEnterFrame;
		} 
		show();
	}
	
	/**
	 * show
	 * affiche l'écran de sélection
	 */
	public function show(){ 
		_visible = true;
		
	}
	
	/**
	 * hide
	 * cache l'écran de sélection
	 */
	public function hide(){

		_visible = false;
		
	}
	
	/**
	 * validateSelection
	 * valide la sélection
	 */
	public function validateSelection(){
		
		// test si un joeur est bien sélectionné
		if (p_lstPlayers.selectedIndices != null){
			// test s'il ne s'agit pas du joueur en cours
			if(_parent.getPlayer().getId() != p_lstPlayers.selectedItem.data){
				_parent.playerIsSelected(p_lstPlayers.selectedItem.label, p_lstPlayers.selectedItem.data);
				hide();
			}else{
				mx.controls.Alert.show("Tu ne peux pas choisir ton propre nom.");
			}
		}else{
			mx.controls.Alert.show("Clique sur un nom.");
		}
		
	}
	
	
	public function onLoad()
	{ 
		//trace("ON LOAD !!"+" - this.p_lstPlayers => "+this.p_lstPlayers)
		this.styleList(this.p_lstPlayers);
		this.p_lstPlayers.addEventListener("change", Delegate.create(this, onPlayerChange));
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------


	private function onPlayerChange(event:Object):Void {
		trace("onPlayerChange()");
		if (this.p_lstPlayers.selectedItem.data != undefined) {
			this._parent._parent.mesDonnees.loadPlayer(this.p_lstPlayers.selectedItem.data);
		}
	}
	
	private function styleList(listOccurence:List)
	{
		_global.styles.ScrollSelectList.backgroundColor = undefined;
		mx.controls.listclasses.SelectableRow.prototype.drawRowFill = function(listOccurence:MovieClip, newClr:Number):Void {
			listOccurence.clear();
			if (newClr == undefined) {
				listOccurence.beginFill(0xABCDEF, 0);
			} else {
				listOccurence.beginFill(newClr);
			}
			listOccurence.drawRect(1, 0, this.__width, this.__height);
			listOccurence.endFill();
			listOccurence._width = this.__width;
			listOccurence._height = this.__height;
		};
		//decommenter la lifne si on veut aucune bordure d'affichée
		//this.menuTree.border_mc._alpha = 0;
	}
}
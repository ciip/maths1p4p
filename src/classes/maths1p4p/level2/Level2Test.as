﻿/**
 * Pour faire des tests dans un niveau bidon et servir de démonstration pour l'utilisation 
 * des classes Application, *Storage, Player et Teacher.
 * Le niveau de test qui utilise cette classe peut s'atteindre en maintenant la touche SHIFT
 * enfoncée au moment de lancer le niveau 2 (après le choix de la classe).
 * 
 * @author Pierre Rossel
 * @version 1.0
 *
 * TODO: Supprimer l'accès une fois que le développement sera terminé.
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import mx.utils.Delegate;
import mx.controls.Alert;
import mx.controls.List;


import maths1p4p.application.Application;
import maths1p4p.application.AbstractStorage;
import maths1p4p.Teacher;
import maths1p4p.level2.Level2Player;

class maths1p4p.level2.Level2Test extends maths1p4p.AbstractLevel {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	var btnGame1:Button;
	var btnGame2:Button;
	var btnGame3:Button;
	var btnStateSave:Button;
	var btnAddPlayer:Button;
	var btnDeletePlayer:Button;
	var btnChangePlayerPassword:Button;
	var btnLoginPlayer:Button;
	var txtState:TextField;    // un texte bidon à sauver quelque part dans la structure XML
	var txtNewPlayer:TextField;
	var txtPlayerPassword:TextField;
	var txtPlayerNewPassword:TextField;
	var lstPlayers:List;
	var txtTeacherLogin:TextField;
	var txtTeacherPass:TextField;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_oPlayer:Level2Player;
	private var p_oTeacher:Teacher;
	private var p_oStorage:AbstractStorage;
	private var p_xml;


	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level2Test() {
		super();
		//trace("Level2Test()");
		stop();
	}
	
	public function onLoad() {
		trace("Level2Test.onLoad");

		var olLogin = new Object();
		olLogin.ok = Delegate.create(this, function (evt:Object){
			trace("Login: " + evt.name + " / " + evt.password);
			txtTeacherLogin.text = evt.name;
			txtTeacherPass.text = evt.password;
		});
 		olLogin.cancel = Delegate.create(this, function (evt:Object){
			trace("Login cancelled");
		});
		
		maths1p4p.application.LoginForm.show("Login enseignant", _root, olLogin);
		//maths1p4p.application.LoginForm.show();
		//Alert.show("Sélectionnez d'abord un joueur.", "Login joueur", Alert.OK, null, null, null, Alert.OK);
		

		lstPlayers.addEventListener("change", Delegate.create(this, onPlayerChange));

		txtTeacherPass.password = true;

		btnStateSave.onRelease = Delegate.create(this, function () {
			var xmlState = p_oPlayer.getState();
			if (xmlState['test'] == undefined)
				xmlState.appendChild(xmlState.rootNode.createElement('test'));
			xmlState['test'].attributes.value = txtState.text;
			p_oPlayer.save();
			p_oPlayer.addEventListener("onSave", this);

		});
		
		btnAddPlayer.onRelease = Delegate.create(this, function () {
			p_oStorage.setTeacherLogin(txtTeacherLogin.text, txtTeacherPass.text);
			p_oStorage.newPlayer(txtNewPlayer.text, 2);
		});

		btnDeletePlayer.onRelease = Delegate.create(this, function () {
			p_oStorage.setTeacherLogin(txtTeacherLogin.text, txtTeacherPass.text);
			p_oStorage.deletePlayer(lstPlayers.selectedItem.data);
		});

		btnLoginPlayer.onRelease = Delegate.create(this, function () {
			if (p_oPlayer == undefined) {
				Alert.show("Sélectionnez d'abord un joueur.", "Login joueur", Alert.OK, null, null, null, Alert.OK);
			} else {
				p_oPlayer.login(txtPlayerPassword.text);
			}
		});

		btnChangePlayerPassword.onRelease = Delegate.create(this, function () {
			if (p_oPlayer == undefined) {
				Alert.show("Sélectionnez d'abord un joueur.", "Login joueur", Alert.OK, null, null, null, Alert.OK);
			} else {
				p_oPlayer.changePassword(txtPlayerPassword.text, txtPlayerNewPassword.text);
			}
		});

		p_oStorage = Application.getInstance().getStorage();
		p_oStorage.addEventListener("onLoadPlayersList", this);
		p_oStorage.addEventListener("onNewPlayer", this);
		p_oStorage.addEventListener("onLoadPlayer", this);
		p_oStorage.addEventListener("onDeletePlayer", this);
		p_oStorage.addEventListener("onLoginPlayer", this);
		p_oStorage.addEventListener("onChangePlayerPassword", this);
		p_oStorage.addEventListener("onLoadTeacher", this);
		p_oStorage.loadPlayersList(2, true);
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	private function onLoadPlayersList (event:Object) {
		trace("Level2Test.onLoadPlayersList " + event);
		
		var arrPlayers:Array = event.data.arrPlayersList;
		
		if (arrPlayers != undefined) {
			lstPlayers.removeAll();

			// récupère les noms des classes dans le xml et les affiche dans la liste
			for(var iPlayer in arrPlayers){
				var xmlPlayer = arrPlayers[iPlayer];
				//trace("in loop for classes");
				lstPlayers.addItem({label: xmlPlayer._name, data: xmlPlayer._idPlayer});
			}

			if (lstPlayers.length) {
				p_oStorage.loadTeacher(arrPlayers[0]._idTeacher);
				Application.getInstance().setCurrentClass(arrPlayers[0]._idClass);
				
				lstPlayers.selectedIndex = 0;
				onPlayerChange();
			}
		} else {
			lstPlayers.addItem({label: "Erreur"});
		}
	}

	private function onLoadPlayer (event:Object) {
		//trace("onLoadPlayer " + event);
		p_oPlayer = event.data.oPlayer;
		txtState.text = p_oPlayer.getState()['test']._value;
	}

	private function onLoadTeacher (event:Object) {
		//trace("onLoadTeacher " + event);
		p_oTeacher = event.data.oTeacher;
		txtTeacherLogin.text = p_oTeacher.p_sLogin;
	}

	private function onSave (event:Object) {
		//trace("onSave " + event);
		if (event.data.result == 0)
			Alert.show("Sauvegarde effectuée avec succès", "Sauvegarde du joueur", Alert.OK, null, null, null, Alert.OK);
		else
			Alert.show("Erreur: " + event.data.error, "Sauvegarde du joueur", Alert.OK, null, null, null, Alert.OK);

		// reload player
		p_oStorage.loadPlayer(p_oPlayer.getId());
		
	}

	private function onPlayerChange(event:Object) {
		//trace("onPlayerChange");
		p_oStorage.loadPlayer(lstPlayers.selectedItem.data);
	}

	private function onNewPlayer(event:Object) {
		if (event.data.result < 0)
			Alert.show("Erreur: " + event.data.error, "Nouveau joueur", Alert.OK, null, null, null, Alert.OK);
		
		// load the list again
		p_oStorage.loadPlayersList(2);
	}

	public function onDeletePlayer (event:Object) {
		if (event.data.result < 0)
			Alert.show("Erreur: " + event.data.error, "Supression joueur", Alert.OK, null, null, null, Alert.OK);

		// load the list again
		p_oStorage.loadPlayersList(2);
	}

	public function onLoginPlayer (event:Object) {
		if (event.data.result == 0)
			Alert.show("Login OK, mot de passe valide.", "Login joueur", Alert.OK, null, null, null, Alert.OK);
		else
			Alert.show("Erreur: " + event.data.error, "Login joueur", Alert.OK, null, null, null, Alert.OK);
	}

	public function onChangePlayerPassword (event:Object) {
		if (event.data.result == 0)
			Alert.show("Mot de passe changé.", "Login joueur", Alert.OK, null, null, null, Alert.OK);
		else
			Alert.show("Erreur: " + event.data.error, "Login joueur", Alert.OK, null, null, null, Alert.OK);
	}

}
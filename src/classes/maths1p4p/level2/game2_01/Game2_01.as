﻿/**
 * class com.maths1p4p.Game2_01
 * 
 * @author Yoann Augen
 * @version 1.0
 *
 * TO DO :
 * => Gestion des noms des utilisateurs
 * => Gestion de la présentation du jeu (scene préléminaire)
 * 
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;
 

class maths1p4p.level2.game2_01.Game2_01 extends maths1p4p.AbstractGame {
	
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 1;

	public var p_mcAction:MovieClip;
	/** Bonton d'entrée sur le jeu */
	public var bt_entree:Button;
	
	/** Liste des joueurs **/
	public var mc_PlayerList:MovieClip;
	
	/** Les champs text contenant le chiffre pour chaque joueur */
	public var p_mcNumber_1:MovieClip;
	public var p_mcNumber_2:MovieClip;
	public var p_txtNumber_1:TextField;

	/** Les deux poubelles */
	public var p_mcGarbage_1:MovieClip;
	public var p_mcGarbage_2:MovieClip;
	
	/** Le clip du parchemin */
	public var p_mcFinish:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	/** le joueur dont c'est le tour (1 ou 2) */
	private var p_iTurn:Number;
	private var p_bEndGame:Boolean;
	
	/** Etat du bouton d'action (1 = depart, 2 = suite, 3 = suite grisé)  */	
	private var p_iStateAction:Number;
	
	/** Liste de valeur des échelles des deux joueurs */
	private var p_aScale_1:Array = new Array(0, 0, 0, 0, 0, 0, 0);
	private var p_aScale_2:Array = new Array(0, 0, 0, 0, 0, 0, 0);
	
	/** Coordonnée des champs contenant le text */
	private var p_nNumberX_1; 
	private var p_nNumberY_1; 
	private var p_nNumberX_2; 
	private var p_nNumberY_2; 
	
	/** les joueurs jouant au jeu */
	private var joueur1:maths1p4p.level2.Level2Player;
	private var joueur2:maths1p4p.level2.Level2Player;
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game2_01() {
		super();
		
		this.p_bEndGame = false;
		this.joueur1 = this._parent._parent.joueur1;
		this["p_txtName_1"].text = this.joueur1.getName();
		this.joueur2 = this._parent._parent.joueur2;
		this["p_txtName_2"].text = this.joueur2.getName();
		
		for (var i:Number = 0 ; i < 7 ; ++i) {
			this["p_mcEtage_1_"+i].p_txtNumber.text = "";
			this["p_aScale_1"][i] = 0;
			this["p_mcEtage_2_"+i].p_txtNumber.text = "";
			this["p_aScale_2"][i] = 0;
		}
			
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		// Définition du premier joueur
		p_iTurn = 1 + Math.round(Math.random()*1);
		
		trace("p_iTurn => "+p_iTurn)
		//this.gameFinished();
		
		this.p_nNumberX_1 = this.p_mcNumber_1._x; 
		this.p_nNumberY_1 = this.p_mcNumber_1._y; 
		this.p_nNumberX_2 = this.p_mcNumber_2._x; 
		this.p_nNumberY_2 = this.p_mcNumber_2._y;
		this.p_iStateAction = 1;
		
		initInterface();
	}
	
	/** 
	 * Nous dit si la valeur needle est présente dans un tableau haystack
	 */
	public function inArray(needle, haystack) {
		for (var i:Number = 0 ; i < haystack.length ; i++) {
			if (haystack[i] == needle) {
				return true;
			}
		}
		return false;
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
		
	/**
	 * initialisation de l'interface
	 */
	private function initInterface() {
		
		bt_entree.onRelease = function() {
			_parent.soundPlayer.playASound("grince3");
			_parent.gotoAndStop('state0');	
		}
		
		// boutons départ et suite
		p_mcAction.onRelease = function(){
			if (_parent.p_iStateAction == 1) {
				// On se met sur un 'suite' grisé
				this.gotoAndStop(3);
				_parent.newNumber();
			}
		}
	}	
	
	/** 
	 * Tirage et affichage du prochain numéro
	 */
	private function newNumber() {
		soundPlayer.playASound("Psh1");
		// Dès qu'un nouveau chiffre est tiré on bloque les tirages
		this.p_iStateAction = 3;
		// Tirage aléatoire du chiffre (1 - 52). 
		var iNumber:Number = 1 + Math.round(Math.random()*51);
		// Le chiffre ne doit pas se trouver sur l'echelle du joueur
		while (this.inArray(iNumber, this["p_aScale_"+p_iTurn]) == true) {
			iNumber = 1 + Math.round(Math.random()*51);
		}
		// place le chiffre dans le champs text correspondant à ce joueur
		this["p_mcNumber_"+p_iTurn].p_txtNumber.text = iNumber.toString();
		
		// On rend ce champs déplacable
		this["p_mcNumber_"+p_iTurn].onPress = function() {
			this.startDrag();
		}
		// On gere le fait de lacher le text
		this["p_mcNumber_"+p_iTurn].onRelease = function() {
			_parent.releaseControl(this);
		}
	}
	
	/**
	 * Gestion du numéro au moment ou on le lache (drag and drop)
	 * On vérifie les barreaux de l'echelle ou la poubelle
	 */ 
	private function releaseControl(mc_drop:MovieClip) {
		var iPlayer:Number = mc_drop._parent.p_iTurn;
		var iValid:Boolean = false;
		var bCorrect:Boolean = false;
		
		this.stopDrag();
			
		// Controle avec la poubelle 
		if (mc_drop.hitTest(mc_drop._parent["p_mcGarbage_"+iPlayer])) {
			//trace ("Poubelle");	
			iValid = true;
			// on vide la valeur du clip
			mc_drop.p_txtNumber.text = "";
		} 
		
		// controle sur chacun des barreaux de l'echelle
		for (var i:Number = 0 ; i < 7 ; ++i) {
			if (mc_drop.hitTest(mc_drop._parent["p_mcEtage_"+iPlayer+"_"+i]) && 
				mc_drop._parent["p_aScale_"+iPlayer][i] == 0) {
				// On vérifie que les numéros sont dans l'ordre 
				// on met le numéro sur l'échelle
				mc_drop._parent["p_mcEtage_"+iPlayer+"_"+i].p_txtNumber.text = mc_drop.p_txtNumber.text;
				mc_drop._parent["p_aScale_"+iPlayer][i] = Number(mc_drop.p_txtNumber.text);
				// on vide la valeur du clip
				bCorrect = this.checkScaleOrder();
				if (bCorrect == false) {
					soundPlayer.playASound("rape4");
				} else {					
					soundPlayer.playASound("MiniClick");
				}		
				mc_drop.p_txtNumber.text = "";
				iValid = true;
				break;
			}
		}
		// On replace le clip (vide ou pas)
		mc_drop._x = mc_drop._parent["p_nNumberX_"+iPlayer];
		mc_drop._y = mc_drop._parent["p_nNumberY_"+iPlayer];
		
		// Si le numéro a bien été utilisé sur un barreau ou la poubelle
		if (iValid == true) {
			soundPlayer.playASound("MiniClick");
		
			// Le bouton suite réaparait en noir
			p_mcAction.gotoAndStop(2);
			// On remet l'état cliquable
			this.p_iStateAction = 1;
			if (this.p_bEndGame == false) {
				// On change de tour
				if (iPlayer == 1) {
					mc_drop._parent.p_iTurn = 2;
				} else {
					mc_drop._parent.p_iTurn = 1;
				}
			}
		}
	}
	
	/** On va vider l'echelle du joueur en cours */
	private function emptyScale() {
		for (var i:Number = 0 ; i < 7 ; ++i) {
			this["p_mcEtage_"+this.p_iTurn+"_"+i].p_txtNumber.text = "";
			this["p_aScale_"+this.p_iTurn][i] = 0;
		}
	}
	
	/** On vérifie que les valeures sont dans l'ordre */
	public function checkScaleOrder() {
		var iWin:Number = 0;
		var nMax:Number = 0;
		for (var i:Number = 0 ; i < 7 ; ++i) {
			// Si la valeur est differente de 0
			if (this["p_aScale_"+this.p_iTurn][i] != 0) {
				// Si la valeur est plus petite que la derniere valeur, il y a une erreur 
				if (nMax > this["p_aScale_"+this.p_iTurn][i]) {
					this.emptyScale();
					return false;
				}
				// La valeur max devient la valeur active
				nMax = this["p_aScale_"+this.p_iTurn][i];
				// On compte le nombre de valeur remplie
				++iWin;
			}
		}
		trace ("Tour en cours :"+this.p_iTurn);
		// Si on a 7 valeurs sans erreur, on gagne
		if (iWin == 7) {
			this.p_bEndGame = true;
			gameFinished();
		}
		return true;
	}
			
	public function gameFinished() {
		
		trace("p_iTurn => "+p_iTurn)

		soundPlayer.playASound("frele2");
		gotoAndStop("win");
		this.p_mcFinish.onPress = function(){
			_parent.soundPlayer.playASound("frele2");
			
			
			trace("p_iTurn => "+_parent.p_iTurn)
			trace("_parent.p_iTurn => "+_parent.p_iTurn)
			
			//on enregistre la réussite du jeu
			if(this._parent.p_iTurn == 1) {
				this._parent._parent._parent.setWinGame(1, this._parent.joueur1);
			} else {
				this._parent._parent._parent.setWinGame(1, this._parent.joueur2);
			} 
			_parent._parent._parent.barreInterface_mc.showParchemin();
			
			
			_parent.gotoAndStop("end");
		}
	}
}

﻿/**
 * class com.maths1p4p.Game2_02
 * 
 * @author Yoann Augen
 * @version 1.0
 *
 * TO DO :
 * => Gestion des noms des utilisateurs
 * => Gestion de la présentation du jeu (scene préléminaire)
 * 
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level2.game2_02.Game2_02 extends maths1p4p.AbstractGame {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 2;
	/** Les cases accueillant les additions */
	public var p_mcCase1:MovieClip;
	public var p_mcCase2:MovieClip;
	public var p_mcCase3:MovieClip;
	public var p_mcCase4:MovieClip;
	public var p_mcCase5:MovieClip;
	public var p_mcCase6:MovieClip;
	public var p_mcCase7:MovieClip;
	public var p_mcCase8:MovieClip;
	public var p_mcCase9:MovieClip;
	
	/** Les 12 cases de chaque joueur */
	public var p_mcResult_1_0:MovieClip;
	public var p_mcResult_1_1:MovieClip;
	public var p_mcResult_1_2:MovieClip;
	public var p_mcResult_1_3:MovieClip;
	public var p_mcResult_1_4:MovieClip;
	public var p_mcResult_1_5:MovieClip;
	public var p_mcResult_1_6:MovieClip;
	public var p_mcResult_1_7:MovieClip;
	public var p_mcResult_1_8:MovieClip;
	public var p_mcResult_1_9:MovieClip;
	public var p_mcResult_1_10:MovieClip;
	public var p_mcResult_1_11:MovieClip;
	public var p_mcResult_1_12:MovieClip;
	public var p_mcResult_2_0:MovieClip;
	public var p_mcResult_2_1:MovieClip;
	public var p_mcResult_2_2:MovieClip;
	public var p_mcResult_2_3:MovieClip;
	public var p_mcResult_2_4:MovieClip;
	public var p_mcResult_2_5:MovieClip;
	public var p_mcResult_2_6:MovieClip;
	public var p_mcResult_2_7:MovieClip;
	public var p_mcResult_2_8:MovieClip;
	public var p_mcResult_2_9:MovieClip;
	public var p_mcResult_2_10:MovieClip;
	public var p_mcResult_2_11:MovieClip;
	public var p_mcResult_2_12:MovieClip;
	
	/** Les points */
	public var p_mcPoint1:MovieClip;
	public var p_mcPoint2:MovieClip;
	 
	/** Definition des valeurs possibles */
	public var p_aListeAdd:Array = new Array(  new Array("0 + 0", "0"), 
									    new Array("4 + 7", "11"), 
										new Array("1 + 4", "5"), 
										new Array("3 + 7", "10"), 
										new Array("2 + 4", "6"), 
										new Array("5 + 3", "8"), 
										new Array("1 + 1", "2"), 
										new Array("1 + 3", "4"), 
										new Array("5 + 2", "7"), 
										new Array("1 + 7", "8"), 
										new Array("2 + 2", "4"), 
										new Array("8 + 1", "9"), 
										new Array("3 + 8", "11"), 
										new Array("3 + 4", "7"), 
										new Array("3 + 3", "6"));
	public var p_aListeSub:Array = new Array(  new Array("6 - 3", "3"), 
									    new Array("8 - 2", "6"), 
										new Array("15 - 4", "11"), 
										new Array("9 - 2", "7"), 
										new Array("9 - 8", "1"), 
										new Array("16 - 8", "8"), 
										new Array("5 - 5", "0"), 
										new Array("17 - 8", "9"), 
										new Array("12 - 2", "10"), 
										new Array("12 - 4", "8"), 
										new Array("15 - 8", "7"), 
										new Array("7 - 5", "2"), 
										new Array("11 - 1", "10"), 
										new Array("16 - 7", "9"), 
										new Array("14 - 8", "6"));
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	/** le joueur dont c'est le tour (1 ou 2) */
	private var p_iTurn:Number;
	
	/** Nombre de partie joués */
	private var p_iNumberGames:Number;
	
	/** Nombre de victoire de chacun */
	private var p_iWin_1;
	private var p_iWin_2;
	
	/** Le vainqueur */
	private var p_iWinner;
	
	/** Nombre de match pour gagner */
	private var p_nForWin;
	
	/** Liste des proprietaire des 9 cases (le -1 ne servant à rien) */
	private var p_aCaseList = new Array(-1, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	
	/** Placement des lumieres de victoire */
	//private var p_aLight_1 = new Array(0, new Array(33, 76), new Array(75, 76), new Array(116, 76));
	//private var p_aLight_2 = new Array(0, new Array(521, 76), new Array(563, 76), new Array(604, 76));
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game2_02() {
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		p_nForWin = 3;
		//écriture des noms des joueurs
		this["p_txtName_1"].text = this._parent._parent.joueur1.getName();
		this["p_txtName_2"].text = this._parent._parent.joueur2.getName();
		// Définition du premier joueur
		p_iTurn = 1 + Math.round(Math.random()*1);
		if(p_iTurn == 1)
		{
			this["p_txtName_1"].textColor = 0x000000;
			this["p_txtName_2"].textColor = 0x999999;
		}
		else
		{
			this["p_txtName_2"].textColor = 0x000000;
			this["p_txtName_1"].textColor = 0x999999;
		}
		// Premiere partie
		p_iNumberGames = 1;
		p_iWin_1 = 0;
		p_iWin_2 = 0;
		// initialisation de l'interface
		initInterface();
	}
	
	/** 
	 * Nous dit si la valeur needle est présente dans un tableau haystack
	 */
	public function inArray(needle, haystack) {
		for (var i:Number = 0 ; i < haystack.length ; i++) {
			if (haystack[i] == needle) {
				return true;
			}
		}
		return false;
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	/** 
	 * Préparation du plateau de jeu
	 */
	private function initInterface() {
		
		var aListe:Array = new Array();
		var iChoose:Number;
		
		this.p_iWinner = 0;
		
		// Soustraction ou addition
		if (p_iNumberGames <= 3) {
			var aActu:Array = this.p_aListeAdd;	
		} else {
			var aActu:Array = this.p_aListeSub;
		}
		
		// On tire 9 valeurs dans les tableaux
		for (var i:Number = 1 ; i <= 9 ; i++) {
			// On efface un cercle eventuel			
			this["p_mcCase"+i].gotoAndStop(1);
					
			// On initialise le tableau des cercles
			p_aCaseList[i] = 0;
			// On tire un indice au hazard
			iChoose = Math.round(Math.random()*(aActu.length - 1));
			// On vérifie que l'addition n'a pas déjà été tiré
			while (this.inArray(aActu[iChoose][0], aListe)) {
				iChoose = (iChoose + 1)%(aActu.length - 1);
			}
			// Une fois qu'on a une valeur unique, on l'ajoute au tablau des valeurs tirée
			aListe.push(aActu[iChoose][0]);

			// On l'affecte à la case
			this["p_mcCase"+i].txtAdd.text = aActu[iChoose][0];
			this["p_mcCase"+i].txtAdd.result = aActu[iChoose][1];
		}
		// une fois les valeurs placé, on rend cliquable les cases du joueurs en cours
		this.startDragJoueur();
	}
	
	/** Fonction activant le drag and drop du joueur en cours */
	private function startDragJoueur() {
		var j:Number = 0;
		var mcTemp:MovieClip;
		
		
		// On se place sur l'interface de ce joueur
		gotoAndStop("init"+this.p_iTurn);
		
		for (var i:Number = 0 ; i <= 12 ; ++i) {
			//removeMovieClip("mcNewImg"+i);
			//attachMovie("mc"+i, "mcNewImg"+i, this.getNextHighestDepth(), {_visible:false});
			// Obligatoire pour se servir d'une fonction dans une boucle, autrement il se sert du max de i
			mcTemp = this["p_mcResult_"+this.p_iTurn+"_"+i];
			mcTemp.j = i;
			mcTemp.txtVal.text = mcTemp.j;
			mcTemp.onPress = function() {
				this.sauv_x = this._x; 
				this.sauv_y = this._y;
				this._x = _parent._xmouse; 
				this._y = _parent._ymouse;
				this.startDrag();
			}
			//this["mcNewImg"+mcTemp.j].onRelease = function() {
			this["p_mcResult_"+this.p_iTurn+"_"+i].onRelease = function() {
				_parent.releaseControl(this);
			}
		}
	}
	
	/** Gestion du drop d'un clip */
	private function releaseControl(mc_drop:MovieClip) {
		var iPlayer:Number = mc_drop._parent.p_iTurn;
		var iCountNull:Number = 0;
		var bFind:Boolean = false;
		// On le rend invisible
		//mc_drop._visible = false;
		// on parcours les 9 cases pour voir si il y a contact
		for (var i:Number = 1 ; i <= 9 ; ++i) {
			trace ("case : "+this.p_aCaseList[i]);
			if (this.p_aCaseList[i] != 0) {
				++iCountNull;
			}
		}
		for (var i:Number = 1 ; i <= 9 ; ++i) {
			// On compte le nombre de case pleine pour reperer le match nule
			
			if (mc_drop.hitTest(this["p_mcCase"+i]) && p_aCaseList[i] == 0) {
				// Si le chiffre est correct
				if (mc_drop.txtVal.text == mc_drop._parent["p_mcCase"+i].txtAdd.result) {
					bFind = true;
					soundPlayer.playASound("clicClacAigu");
					// On affiche le cercle de la bonne couleur				
					mc_drop._parent["p_mcCase"+i].gotoAndStop(iPlayer + 1);
					// On le rajoute à la liste
					this.p_aCaseList[i] = iPlayer;
					++iCountNull
					// On vérifie s'il gagne le point avec les 8 cas de victoire possible
					if ((p_aCaseList[1] == iPlayer && p_aCaseList[2] == iPlayer && p_aCaseList[3] == iPlayer) || 
						(p_aCaseList[4] == iPlayer && p_aCaseList[5] == iPlayer && p_aCaseList[6] == iPlayer) || 
						(p_aCaseList[7] == iPlayer && p_aCaseList[8] == iPlayer && p_aCaseList[9] == iPlayer) || 
						(p_aCaseList[1] == iPlayer && p_aCaseList[4] == iPlayer && p_aCaseList[7] == iPlayer) || 
						(p_aCaseList[2] == iPlayer && p_aCaseList[5] == iPlayer && p_aCaseList[8] == iPlayer) || 
						(p_aCaseList[3] == iPlayer && p_aCaseList[6] == iPlayer && p_aCaseList[9] == iPlayer) || 
						(p_aCaseList[1] == iPlayer && p_aCaseList[5] == iPlayer && p_aCaseList[9] == iPlayer) || 
						(p_aCaseList[3] == iPlayer && p_aCaseList[5] == iPlayer && p_aCaseList[7] == iPlayer)) {
						this.winPart();
					}
				} else {
					soundPlayer.playASound("scratch");			
				}
				if (this["p_iWin_"+this.p_iTurn] != this.p_nForWin) {
					// On change de tour
					if (iPlayer == 1) {
						this.p_iTurn = 2;
						//le nom du 2eme joueur devient de couleur noir et l'autre gris
						this["p_txtName_1"].textColor = 0x999999;
						this["p_txtName_2"].textColor = 0x000000;
					} else {
						this.p_iTurn = 1;
						this["p_txtName_1"].textColor = 0x000000;
						this["p_txtName_2"].textColor = 0x999999;
					}		
					// on stop la boucle
					i = 10;
					// On repart sur l'autre joueur
					startDragJoueur();
				}
			}
		}
		
		// Match nul 
		if (iCountNull == 9 && this.p_iWinner == 0) {
			soundPlayer.playASound("matchNul");
			initInterface();
		}
		if (bFind == false) {
			soundPlayer.playASound("clicClac6");
		}
		mc_drop._x = mc_drop.sauv_x; 
		mc_drop._y = mc_drop.sauv_y;
		mc_drop.stopDrag();		
	}
	
	/** Victoire d'une manche par un joueur */
	private function winPart() {		
		// On rajoute une manche
		this.p_iNumberGames++;
		
		this.p_iWinner = this.p_iTurn;
		// On rajoute une victoire au joueur
		this["p_iWin_"+this.p_iTurn]++;
		// On affiche la lumière
		soundPlayer.playASound("clicClac6");
		//_parent.attachMovie("mcPoint", "mcPoint_"+this.p_iTurn+"_"+this["p_iWin_"+this.p_iTurn], this.getNextHighestDepth(), {_x:this["p_aLight_"+this.p_iTurn][this["p_iWin_"+this.p_iTurn]][0], _y:this["p_aLight_"+this.p_iTurn][this["p_iWin_"+this.p_iTurn]][1]});
		this["p_mcPoint"+this.p_iTurn].gotoAndStop(this["p_iWin_"+this.p_iTurn] + 1);
		if (this["p_iWin_"+this.p_iTurn] == this.p_nForWin) {
			this.winGame();
		} else {// Sinon on relance un manche
			initInterface();
		}
	}
	
	/** Victoire du jeu par un des joueurs */
	private function winGame() {
		this.gotoAndStop("win"+this.p_iTurn);
		this["mcFinish"+this.p_iTurn].onPress = function(){
			_parent.soundPlayer.playASound("fefeClac");
			
			//on enregistre le resultat
			if(this._parent.p_iWinner == 1) {
				this._parent._parent._parent.setWinGame(2, this._parent._parent._parent.joueur1);
			} else {
				this._parent._parent._parent.setWinGame(2, this._parent._parent._parent.joueur2); 
			}
			
			_parent._parent._parent.barreInterface_mc.showParchemin();
			_parent.gotoAndStop("win"+_parent.p_iTurn+"_alter");
			gameFinished();
		}
	}
}

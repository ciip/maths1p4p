﻿/**
 * class maths1p4p.level2.game2_03.game2_03
 * 
 * @author Rémi Gay
 * @version 1.0
 *
 * TO DO :  
 * => gérer les intéractions avec l'extérieur (enregistrement des résultats dans le _root, modification de 
											   l'interface)
 * => gérer les switch entre les textes
 *
 * Copyright (c) 2008 Rémi Gay.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import flash.geom.Point;

import maths1p4p.level2.Level2Player;
//import maths1p4p.level2.game2_03.*
import maths1p4p.utils.OutilsTableaux;
import maths1p4p.application.Application; 
	
class maths1p4p.level2.game2_03.Game2_03 extends maths1p4p.AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	//variable stockant l'ancienne profondeur du clip draggé (car on lui a donné la plus grande profondeur possible)
	public static var lastDepth:Number = -11111;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 3;
	
	//variables en rapport avec les éléments déjà present sur la scène
	private var btDepart_mc:MovieClip;
	private var boiteGauche_mc:MovieClip;
	private var boule1_mc:MovieClip;
	private var boule2_mc:MovieClip;
	private var boule3_mc:MovieClip;
	private var boule4_mc:MovieClip;
	private var texteBoule1_mc:MovieClip;
	private var texteBoule2_mc:MovieClip;
	private var texteBoule3_mc:MovieClip;
	private var texteBoule4_mc:MovieClip;
	private var clapet1_mc:MovieClip;
	private var clapet2_mc:MovieClip;
	private var clapet3_mc:MovieClip;
	private var clapet4_mc:MovieClip;
	private var parchemin_mc:MovieClip;
	
	private var joueur:Level2Player;	
	
	private var toto_btn:Button;
	private var tutu_btn:Button;

	//variable privée de la classe
	private var tabClipBouleDepart:Array;
	private var tabResultat:Array;
	private var tabResultatClip:Array;
	private var score:Number;
	//2 variables qui servent à mémoriser la place des chiffres avant de les deplacer
	private var posXChiffre:Number;
	private var posYChiffre:Number;

	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * s'occupe de l'initialisation graphique et l'attribution des evenements
	 */
	public function Game2_03()	{
		
		super();
		
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		this.joueur = this._parent._parent.joueur1;

		trace('this.joueur.getState() => '+this.joueur.getState()+' this.joueur.getResults() => '+this.joueur.getResults())
			
		//on rend invisible les boutons de tests
		this.toto_btn._visible = false;
		this.tutu_btn._visible = false;
		trace("this.toto_btn => "+this.toto_btn+" this.toto_btn._visible => "+this.toto_btn._visible)
				
		this.score = 0;
		
		//on rend invisible l'aspect ouvert des clapet et du parchemin
		this.clapet1_mc._visible = false;
		this.clapet2_mc._visible = false;
		this.clapet3_mc._visible = false;
		this.clapet4_mc._visible = false;
		this.parchemin_mc._visible = false;
		
		//on rend invisible les boules au depart
		this.boule1_mc._visible = false;
		this.boule2_mc._visible = false;
		this.boule3_mc._visible = false;
		this.boule4_mc._visible = false;
		
		//initialisation du texte de départ : 
		this.btDepart_mc.text_txt.text = "DEPART";
		this.btDepart_mc.onRelease = function()	{
			
			var afficherBoule:Function = function()	{
				
				this._parent.boule1_mc._visible = true;
				this._parent.boule2_mc._visible = true;
				this._parent.boule3_mc._visible = true;
				this._parent.boule4_mc._visible = true;
				
				this._parent.initialisationChiffres();
				
				//changement du bouton départ qui devient le bouton de validation
				this._parent.btDepart_mc.text_txt.text = "J'AI FINI";
				this._parent.btDepart_mc.onRelease = function()	{
					this._parent.verifierResultat();
				}
				
			}
			
			var moveLeft:Function = function() {
				this._parent.boiteGauche_mc.tween ("_x", this._parent.boiteGauche_mc._x-150, 1/2, "easeInSine", 0.1) ;
			}
			
			var moveRight:Function = function()	{
				this._parent.boiteDroite_mc.tween ("_x", this._parent.boiteDroite_mc._x+150, 1/2, "easeInSine", 0.1, afficherBoule) ;
			}
			
			this._parent.boiteGauche_mc.tween ("_x", this._parent.boiteGauche_mc._x+65, 1/2, "easeInSine", null, moveLeft) ;
			this._parent.boiteDroite_mc.tween ("_x", this._parent.boiteDroite_mc._x-65, 1/2, "easeInSine", null, moveRight) ;

		}
		
				
		//bouton de test a enlever
		this.toto_btn.onRelease = function() {
			trace(this._parent.tabResultat[0]+"-"+this._parent.tabResultat[1]+"-"+this._parent.tabResultat[2]+"-"+this._parent.tabResultat[3])
		}
		
		this.tutu_btn.onRelease = function() {
			trace(this._parent.tabClipBouleDepart[0].nombre_txt.text+"-"+this._parent.tabClipBouleDepart[1].nombre_txt.text+"-"+this._parent.tabClipBouleDepart[2].nombre_txt.text+"-"+this._parent.tabClipBouleDepart[3].nombre_txt.text)
		}
		
		//on initialise le click sur le parchemin. De toute manière il ne pourra être effectué que lorsque le
		//joueur l'aura rendu visible en gagnat au jeu
		this.parchemin_mc.onRelease = function()
		{			
			//enregistrer le résultat => mémoriser que le joueur courant a bien finit ce jeu
			//A FAIRE !!!
			
			//afficher le parchemin dans la glissière de navigation (peut être fait lors de l'enregistrement du 
			//résultat
			trace (_parent._parent._parent._parent.barreInterface_mc);
			trace (_parent._parent._parent.barreInterface_mc);
			trace (_parent._parent.barreInterface_mc);
			trace (_parent.barreInterface_mc); 
			_parent._parent._parent.barreInterface_mc.showParchemin(); 
			_parent._parent._parent.setWinGame(3, this._parent._parent._parent.joueur1);
			//rendre de nouveau invisibles clapets et parchemins
			this._parent.clapet1_mc._visible = false;
			this._parent.clapet2_mc._visible = false;
			this._parent.clapet3_mc._visible = false;
			this._parent.clapet4_mc._visible = false;
			this._parent.parchemin_mc._visible = false;
			
			//emettre le son de la fermeture des clapets et bouchon
			_parent.soundPlayer.playASound("rape4");
			  
			this._parent.setWinGame(3, this._parent.joueur);
		}


	}
	
	/**
	* accesseur de la variable tabResultat
	*/
	public function getTabResultat():Array {
		return this.tabResultat;
	}
	
	/**
	* setteur de la variable tabResultat
	*/
	public function setTabResultat(tabResultat:Array) {
		this.tabResultat = tabResultat;
	}
	
	/**
	* accesseur de la variable score
	*/
	public function getScore():Number {
		return this.score;
	}
	
	/**
	* setteur de la variable tabResultat
	*/
	public function setScore(score:Number) {
		this.score = score;
	}
	
	
	
	/**
	 * Renvois le tableau
	 * @return un tableau
	 */
	/*public static function getSomething():Array	{
		return p_aUnArray;
	}*/
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
		
		
		
	/**
	 * Affichage de nouveaux chiffres sur les boules du jeu et réinitialisation du tableau de résultat
	 * @return rien
	 */
	 private function initialisationChiffres() {
		
		this.tabClipBouleDepart = new Array();
		this.tabResultat = new Array();
		this.tabResultatClip = new Array();
		
		//créations des futures chiffres
		var monTab:Array = new Array(1,2,3,4,5,6,7,8,9);
		//on mélange le tableau pour prendre les 4 dernières valeurs qui sont dans un ordre aléatoire
		monTab = OutilsTableaux.shuffle(monTab);
								
		//création, affichage et placement des 4 chiffres
		this.attachMovie("mcTexteBoule", "texteBoule1_mc", 1);
		this.tabClipBouleDepart[0] = this.texteBoule1_mc;
		this.texteBoule1_mc._x = 131;
		this.texteBoule1_mc._y = 225;
		this.texteBoule1_mc.nombre_txt.text = monTab.pop();
		this.texteBoule1_mc.onPress = function() {
			//mémorisation de l'emplacement du chiffre dans le cas où on devrait le faire revenir à sa position 
			//initiale au relachement
			this._parent.posXChiffre = this._x;
			this._parent.posYChiffre = this._y;
			//on change la profondeur du clip pour qu'il soit au dessus de tous les autres et on mémorise son
			//ancienne profondeur
			Game2_03.lastDepth = this.getDepth();
			trace("lastDepth => "+this.getDepth()+" this._parent.lastDepth => "+Game2_03.lastDepth)
			this.swapDepths(10000);
			trace("lastDepth => "+this.getDepth()+" this._parent.lastDepth => "+Game2_03.lastDepth)
			startDrag(this);
		}
		this.texteBoule1_mc.onRelease = function() {
			this._parent.releaseTexteBoule(this);
		}
		
		this.attachMovie("mcTexteBoule", "texteBoule2_mc", 2);
		this.tabClipBouleDepart[1] = this.texteBoule2_mc;
		this.texteBoule2_mc._x = 231;
		this.texteBoule2_mc._y = 225;
		this.texteBoule2_mc.nombre_txt.text = monTab.pop();
		this.texteBoule2_mc.onPress = function() {
			this._parent.posXChiffre = this._x;
			this._parent.posYChiffre = this._y;
			Game2_03.lastDepth = this.getDepth();
			trace("lastDepth => "+this.getDepth()+" this._parent.lastDepth => "+Game2_03.lastDepth)
			this.swapDepths(10000);
			startDrag(this);
		}
		this.texteBoule2_mc.onRelease = function() {
			this._parent.releaseTexteBoule(this);
		}
		
		this.attachMovie("mcTexteBoule", "texteBoule3_mc", 3);
		this.tabClipBouleDepart[2] = this.texteBoule3_mc;
		this.texteBoule3_mc._x = 330;
		this.texteBoule3_mc._y = 225;
		this.texteBoule3_mc.nombre_txt.text = monTab.pop();
		this.texteBoule3_mc.onPress = function() {
			this._parent.posXChiffre = this._x;
			this._parent.posYChiffre = this._y;
			Game2_03.lastDepth = this.getDepth();
			trace("lastDepth => "+this.getDepth()+" this._parent.lastDepth => "+Game2_03.lastDepth)
			this.swapDepths(10000);
			startDrag(this);
		}
		this.texteBoule3_mc.onRelease = function() {
			this._parent.releaseTexteBoule(this);
		}
		
		this.attachMovie("mcTexteBoule", "texteBoule4_mc", 4);
		this.tabClipBouleDepart[3] = this.texteBoule4_mc;
		this.texteBoule4_mc._x = 428;
		this.texteBoule4_mc._y = 225;
		this.texteBoule4_mc.nombre_txt.text = monTab.pop();
		this.texteBoule4_mc.onPress = function() {
			this._parent.posXChiffre = this._x;
			this._parent.posYChiffre = this._y;
			Game2_03.lastDepth = this.getDepth();
			trace("lastDepth => "+this.getDepth()+" this._parent.lastDepth => "+Game2_03.lastDepth)
			this.swapDepths(10000);
			startDrag(this);
		}
		this.texteBoule4_mc.onRelease = function() {
			this._parent.releaseTexteBoule(this);
		}
		
	 }
		
		
	/**
	 * Fonction qui gère l'effet dû au relachement d'un clip de chiffre.
	 * 1) le chiffre est relaché dans un emplacement valide : on ajuste correctement sa position
	 * 2) le chiffre est relaché à la place d'un autre chiffre : on echange les 2 chiffres
	 * 3) le chiffre est relaché n'importe où : on le replace à son emplacement initial
	 * @param clipTexte le clip du chiffre qui est relaché
	 * @return rien
	 */
	 private function releaseTexteBoule(clipTexte:MovieClip) {
							
		trace(clipTexte._parent.boiteGauche_mc.zoneD_mc+" - "+clipTexte.aim_mc)

		stopDrag();
		soundPlayer.playASound("minitic");
		
		trace(clipTexte._x+" "+this.posXChiffre)
		if(clipTexte.aim_mc.hitTest(clipTexte._parent.boiteGauche_mc.zoneD_mc))
		{
			
			//changement de position
			if(this.tabResultatClip[0] != undefined)
			{
				//si il y a deja un chiffre a cette place on les echange tous les 2
				clipTexte._x = this.tabResultatClip[0]._x
				clipTexte._y = this.tabResultatClip[0]._y
				
				this.tabResultatClip[0]._x = this.posXChiffre;
				this.tabResultatClip[0]._y = this.posYChiffre;

			}
			else
			{
				clipTexte._x = clipTexte._parent.boiteGauche_mc.zoneD_mc._x + clipTexte._parent.boiteGauche_mc._x - 5;;
				clipTexte._y = clipTexte._parent.boiteGauche_mc.zoneD_mc._y + clipTexte._parent.boiteGauche_mc._y - 17;
			}
			//changement dans les tableaux de résultats
			trace("OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte) => "+OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte))
		
			var pos = OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte);
			if(pos != undefined)
			{
				if(this.tabResultatClip[0] != undefined)
				{
					this.tabClipBouleDepart[pos] = this.tabResultatClip[0];
				}
				else
				{
					this.tabClipBouleDepart[pos] = undefined;
				}
			}
			trace("OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text) => "+OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text))
			var pos = OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text);
			if(pos != undefined)
			{
				this.tabResultat[pos] = this.tabResultatClip[0].nombre_txt.text;
				this.tabResultatClip[pos] = this.tabResultatClip[0];
			}
			//echanger leur place dans this.tabClipBouleDepart et/ou this.tabResultatClip
			this.tabResultatClip[0] = clipTexte;
			this.tabResultat[0] = clipTexte.nombre_txt.text;

		}
		else if(clipTexte.aim_mc.hitTest(clipTexte._parent.boiteGauche_mc.zoneU_mc))
		{
			
			//changement de position
			if(this.tabResultatClip[1] != undefined)
			{
				//si il y a deja un chiffre a cette place on les echange tous les 2
				clipTexte._x = this.tabResultatClip[1]._x
				clipTexte._y = this.tabResultatClip[1]._y
				
				this.tabResultatClip[1]._x = this.posXChiffre;
				this.tabResultatClip[1]._y = this.posYChiffre;

			}
			else
			{
				clipTexte._x = clipTexte._parent.boiteGauche_mc.zoneU_mc._x + clipTexte._parent.boiteGauche_mc._x - 5;;
				clipTexte._y = clipTexte._parent.boiteGauche_mc.zoneU_mc._y + clipTexte._parent.boiteGauche_mc._y - 17;
			}
			//changement dans les tableaux de résultats
			trace("OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte) => "+OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte))
		
			var pos = OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte);
			if(pos != undefined)
			{
				if(this.tabResultatClip[1] != undefined)
				{
					this.tabClipBouleDepart[pos] = this.tabResultatClip[1];
				}
				else
				{
					this.tabClipBouleDepart[pos] = undefined;
				}
			}
			trace("OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text) => "+OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text))
			var pos = OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text);
			if(pos != undefined)
			{
				this.tabResultat[pos] = this.tabResultatClip[1].nombre_txt.text;
				this.tabResultatClip[pos] = this.tabResultatClip[1];
			}
			//echanger leur place dans this.tabClipBouleDepart et/ou this.tabResultatClip
			this.tabResultatClip[1] = clipTexte;
			this.tabResultat[1] = clipTexte.nombre_txt.text;
		}
		else if(clipTexte.aim_mc.hitTest(clipTexte._parent.boiteDroite_mc.zoneD_mc))
		{
			
			//changement de position
			if(this.tabResultatClip[2] != undefined)
			{
				//si il y a deja un chiffre a cette place on les echange tous les 2
				clipTexte._x = this.tabResultatClip[2]._x
				clipTexte._y = this.tabResultatClip[2]._y
				
				this.tabResultatClip[2]._x = this.posXChiffre;
				this.tabResultatClip[2]._y = this.posYChiffre;

			}
			else
			{
				clipTexte._x = clipTexte._parent.boiteDroite_mc.zoneD_mc._x + clipTexte._parent.boiteDroite_mc._x - 5;;
				clipTexte._y = clipTexte._parent.boiteDroite_mc.zoneD_mc._y + clipTexte._parent.boiteDroite_mc._y - 17;
			}
			//changement dans les tableaux de résultats
			trace("OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte) => "+OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte))
		
			var pos = OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte);
			if(pos != undefined)
			{
				if(this.tabResultatClip[2] != undefined)
				{
					this.tabClipBouleDepart[pos] = this.tabResultatClip[2];
				}
				else
				{
					this.tabClipBouleDepart[pos] = undefined;
				}
			}
			trace("OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text) => "+OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text))
			var pos = OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text);
			if(pos != undefined)
			{
				this.tabResultat[pos] = this.tabResultatClip[2].nombre_txt.text;
				this.tabResultatClip[pos] = this.tabResultatClip[2];
			}
			//echanger leur place dans this.tabClipBouleDepart et/ou this.tabResultatClip
			this.tabResultatClip[2] = clipTexte;
			this.tabResultat[2] = clipTexte.nombre_txt.text;
		}
		else if(clipTexte.aim_mc.hitTest(clipTexte._parent.boiteDroite_mc.zoneU_mc))
		{
			
			//changement de position
			if(this.tabResultatClip[3] != undefined)
			{
				//si il y a deja un chiffre a cette place on les echange tous les 2
				clipTexte._x = this.tabResultatClip[3]._x
				clipTexte._y = this.tabResultatClip[3]._y
				
				this.tabResultatClip[3]._x = this.posXChiffre;
				this.tabResultatClip[3]._y = this.posYChiffre;

			}
			else
			{
				clipTexte._x = clipTexte._parent.boiteDroite_mc.zoneU_mc._x + clipTexte._parent.boiteDroite_mc._x - 5;;
				clipTexte._y = clipTexte._parent.boiteDroite_mc.zoneU_mc._y + clipTexte._parent.boiteDroite_mc._y - 17;
			}
			//changement dans les tableaux de résultats
			trace("OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte) => "+OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte))
		
			var pos = OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte);
			if(pos != undefined)
			{
				if(this.tabResultatClip[3] != undefined)
				{
					this.tabClipBouleDepart[pos] = this.tabResultatClip[3];
				}
				else
				{
					this.tabClipBouleDepart[pos] = undefined;
				}
			}
			trace("OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text) => "+OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text))
			var pos = OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text);
			if(pos != undefined)
			{
				this.tabResultat[pos] = this.tabResultatClip[3].nombre_txt.text;
				this.tabResultatClip[pos] = this.tabResultatClip[3];
			}
			//echanger leur place dans this.tabClipBouleDepart et/ou this.tabResultatClip
			this.tabResultatClip[3] = clipTexte;
			this.tabResultat[3] = clipTexte.nombre_txt.text;
		}
		//sinon si le chiffre recouvre une boule vide on le replace sur cette boule
		else if(clipTexte.aim_mc.hitTest(clipTexte._parent.boule1_mc))
		{
			trace("=>>>>>>>>>>>>>>>>>>1")

			//changement de position
			if(this.tabClipBouleDepart[0] != undefined)
			{
				//si il y a deja un chiffre a cette place on les echange tous les 2
				clipTexte._x = this.tabClipBouleDepart[0]._x
				clipTexte._y = this.tabClipBouleDepart[0]._y
				
				this.tabClipBouleDepart[0]._x = this.posXChiffre;
				this.tabClipBouleDepart[0]._y = this.posYChiffre;

			}
			else
			{
				clipTexte._x = clipTexte._parent.boule1_mc._x + 10;
				clipTexte._y = clipTexte._parent.boule1_mc._y + 5;
			}
			//changement dans les tableaux de résultats
			var pos = OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text);
			if(pos != undefined)
			{
				this.tabResultat[pos] = this.tabClipBouleDepart[0].nombre_txt.text;
				this.tabResultatClip[pos] = this.tabClipBouleDepart[0];
			}
			var pos = OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte);
			if(pos != undefined)
			{
				this.tabClipBouleDepart[pos] = this.tabClipBouleDepart[0];
			}
			//echanger leur place dans this.tabClipBouleDepart et/ou this.tabResultatClip
			this.tabClipBouleDepart[0] = clipTexte;
		}
		else if(clipTexte.aim_mc.hitTest(clipTexte._parent.boule2_mc))
		{
			trace("=>>>>>>>>>>>>>>>>>>2")
			
			//changement de position
			trace("this.tabClipBouleDepart[0] => "+this.tabClipBouleDepart[1])
			if(this.tabClipBouleDepart[1] != undefined)
			{
				//si il y a deja un chiffre a cette place on les echange tous les 2
				clipTexte._x = this.tabClipBouleDepart[1]._x
				clipTexte._y = this.tabClipBouleDepart[1]._y
				
				this.tabClipBouleDepart[1]._x = this.posXChiffre;
				this.tabClipBouleDepart[1]._y = this.posYChiffre;

			}
			else
			{
				clipTexte._x = clipTexte._parent.boule2_mc._x + 10;
				clipTexte._y = clipTexte._parent.boule2_mc._y + 5;
			}
			trace("OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text => "+OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text))
			//changement dans les tableaux de résultats
			var pos = OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text);
			if(pos != undefined)
			{
				this.tabResultat[pos] = this.tabClipBouleDepart[1].nombre_txt.text;
				this.tabResultatClip[pos] = this.tabClipBouleDepart[1];
			}
			trace("OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte) => "+OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte))
			var pos = OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte);
			if(pos != undefined)
			{
				this.tabClipBouleDepart[pos] = this.tabClipBouleDepart[1];
			}
			//echanger leur place dans this.tabClipBouleDepart et/ou this.tabResultatClip
			this.tabClipBouleDepart[1] = clipTexte;
		}
		else if(clipTexte.aim_mc.hitTest(clipTexte._parent.boule3_mc))
		{
			trace("=>>>>>>>>>>>>>>>>>>3")
			
			//changement de position
			if(this.tabClipBouleDepart[2] != undefined)
			{
				//si il y a deja un chiffre a cette place on les echange tous les 2
				clipTexte._x = this.tabClipBouleDepart[2]._x
				clipTexte._y = this.tabClipBouleDepart[2]._y
				
				this.tabClipBouleDepart[2]._x = this.posXChiffre;
				this.tabClipBouleDepart[2]._y = this.posYChiffre;

			}
			else
			{
				clipTexte._x = clipTexte._parent.boule3_mc._x + 10;
				clipTexte._y = clipTexte._parent.boule3_mc._y + 5;
			}
			//changement dans les tableaux de résultats
			var pos = OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text);
			if(pos != undefined)
			{
				this.tabResultat[pos] = this.tabClipBouleDepart[2].nombre_txt.text;
				this.tabResultatClip[pos] = this.tabClipBouleDepart[2];
			}
			var pos = OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte);
			if(pos != undefined)
			{
				this.tabClipBouleDepart[pos] = this.tabClipBouleDepart[2];
			}
			//echanger leur place dans this.tabClipBouleDepart et/ou this.tabResultatClip
			this.tabClipBouleDepart[2] = clipTexte;
		}		
		else if(clipTexte.aim_mc.hitTest(clipTexte._parent.boule4_mc))
		{
			trace("=>>>>>>>>>>>>>>>>>>4")
			
			//changement de position
			if(this.tabClipBouleDepart[3] != undefined)
			{
				//si il y a deja un chiffre a cette place on les echange tous les 2
				clipTexte._x = this.tabClipBouleDepart[3]._x
				clipTexte._y = this.tabClipBouleDepart[3]._y
				
				this.tabClipBouleDepart[3]._x = this.posXChiffre;
				this.tabClipBouleDepart[3]._y = this.posYChiffre;

			}
			else
			{
				clipTexte._x = clipTexte._parent.boule4_mc._x + 10;
				clipTexte._y = clipTexte._parent.boule4_mc._y + 5;
			}
			//changement dans les tableaux de résultats
			var pos = OutilsTableaux.getPosition(this.tabResultat, clipTexte.nombre_txt.text);
			if(pos != undefined)
			{
				this.tabResultat[pos] = this.tabClipBouleDepart[3].nombre_txt.text;
				this.tabResultatClip[pos] = this.tabClipBouleDepart[3];
			}
			var pos = OutilsTableaux.getPosition(this.tabClipBouleDepart, clipTexte);
			if(pos != undefined)
			{
				this.tabClipBouleDepart[pos] = this.tabClipBouleDepart[3];
			}
			//echanger leur place dans this.tabClipBouleDepart et/ou this.tabResultatClip
			this.tabClipBouleDepart[3] = clipTexte;
		}			
		//le cas où le chiffre est déposé n'importe où, il doit revenir à son ancienne place
		else {
			//on remet notre chiffre où il était avant le drag
			clipTexte._x = this.posXChiffre;
			clipTexte._y = this.posYChiffre;
			trace("retour du clip a sa place à faire");
		}
		
		
		//puis on mémorise le nouvel emplacement du chiffre
		this.posXChiffre = clipTexte._x;
		this.posYChiffre = clipTexte._y;
		//et on redonne au chiffre sa profondeur initiale 
		trace("clipTexte.getDepth() => "+clipTexte.getDepth())
		trace("Game2_03.lastDepth => "+Game2_03.lastDepth)
		clipTexte.swapDepths(Game2_03.lastDepth);
		trace("clipTexte.getDepth() => "+clipTexte.getDepth())

	 }
	 
	 
	 /**
	 * Fonction qui test si les chiffres sont bien placés
	 * @return rien
	 */
	 private function verifierResultat() {

		//d'abord on vérifie qu'on a bien placé les 4 chiffres
		if(this.tabResultat[0] != undefined && this.tabResultat[1] != undefined && this.tabResultat[2] != undefined && this.tabResultat[3] != undefined) {
		 
			//construire 1 deuxième tableau qui contiendra les resultats de façon ordonnés
			var tabOrdonné = OutilsTableaux.getCopie(this.tabResultat)
			tabOrdonné.sort(Array.NUMERIC);
			
			//1er test qui vérifie si le 1er nbre est le + grand possible et le 2eme le plus petit possible
			//2ème test qui vérifie si le 1er nombre est le plus petit possible et le 2ème le plus grand possible
			
			if( this.tabResultat != null && ( (this.tabResultat[0] == tabOrdonné[3] && this.tabResultat[1] == tabOrdonné[2] && this.tabResultat[2] == tabOrdonné[0] && this.tabResultat[3] == tabOrdonné[1])
			 || (this.tabResultat[0] == tabOrdonné[0] && this.tabResultat[1] == tabOrdonné[1] && this.tabResultat[2] == tabOrdonné[3] && this.tabResultat[3] == tabOrdonné[2]) ) )
			{
				this.reponseJuste();
			}
			else
			{
				soundPlayer.playASound("tac1");
				//trace("faire entendre le son qui dit que c'est pas bon");
			}
		}
		else {
			soundPlayer.playASound("tac1");
			//trace("faire entendre le son qui dit que c'est pas bon");
		}
		
		 
		//trace(tabOrdonné[0]+"-"+tabOrdonné[1]+"-"+tabOrdonné[2]+"-"+tabOrdonné[3])
	 }
	 
	 /**
	 * Fonction qui execute les bonnes instructions lorsque le joueur a bien placé ses chiffres
	 * @return rien
	 */
	 private function reponseJuste() {
	 
	 	this.score++;
		//afficher l'ouverture d'un clapet ou carrement le parchemin si le joueur a gagné
		switch(this.score) {
			
			case 1:
				
				//le 1er clapet s'ouvre
				this.clapet1_mc._visible = true;
				//produire le son du clapet 
				soundPlayer.playASound("shark2"); 
				
				//on recré de nouveaux chiffres dans les boules en supprimant les anciens
				this.initialisationChiffres();
				break;
				
			case 2:
				
				//le 1er clapet s'ouvre
				this.clapet2_mc._visible = true;
				//produire le son du clapet 
				
				soundPlayer.playASound("shark2"); 
				
				//on recré de nouveaux chiffres dans les boules en supprimant les anciens
				this.initialisationChiffres();
				break;
				
			case 3:
				
				//le 1er clapet s'ouvre
				this.clapet3_mc._visible = true;
				//produire le son du clapet 
				
				soundPlayer.playASound("shark2"); 
				
				//on recré de nouveaux chiffres dans les boules en supprimant les anciens
				this.initialisationChiffres();
				break;
				
			case 4:
				
				//le 1er clapet s'ouvre
				this.clapet4_mc._visible = true;
				//produire le son du clapet 
				
				soundPlayer.playASound("shark2"); 
				
				this.parchemin_mc._visible = true;
				//produire le son du parchemin qui sort 
				
				soundPlayer.playASound("rape1");
				
				//cette fois-ci on vérouille les chiffres, on ne peut plus y toucher
				this.texteBoule1_mc.enabled = false;
				this.texteBoule2_mc.enabled = false;
				this.texteBoule3_mc.enabled = false;
				this.texteBoule4_mc.enabled = false;
				//on verouille le bouton aussi
				this.btDepart_mc.enabled = false;
				break;
		}
		
		//interdire l'appuie sur le bouton dans le cas où le joueur a gagné 4 fois
	 }
	 
	
}

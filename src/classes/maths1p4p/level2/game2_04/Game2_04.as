﻿/**
 * class com.maths1p4p.Game2_04
 * 
 * @author Yoann Augen
 * @version 1.0
 *
 * TO DO :  
 * => Gestion de la présentation du jeu (scene préléminaire)
 * 
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.application.Application;

class maths1p4p.level2.game2_04.Game2_04 extends maths1p4p.AbstractGame {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 4;
	/** Bouton de départ */
	public var p_mcAction:MovieClip;
	
	/** Parchemin final */
	public var p_mcParchemin:MovieClip;	
	
	/** Animation Loading */
	public var p_mcLoading:MovieClip;	
	
	/** Bouton de fin */
	public var p_mcFinish:MovieClip;	
	
	/** Les emplacements de casquette initiaux */
	public var mc_loc_1:MovieClip;
	public var mc_loc_2:MovieClip;
	public var mc_loc_3:MovieClip;
	public var mc_loc_4:MovieClip;
	public var mc_loc_5:MovieClip;
	public var mc_loc_6:MovieClip;
	public var mc_loc_7:MovieClip;
	public var mc_loc_8:MovieClip;
	public var mc_loc_9:MovieClip;
	public var mc_loc_10:MovieClip;
	public var mc_loc_11:MovieClip;
	public var mc_loc_12:MovieClip;
	public var mc_loc_13:MovieClip;
	public var mc_loc_14:MovieClip;
	public var mc_loc_15:MovieClip;
	public var mc_loc_16:MovieClip;
	public var mc_loc_17:MovieClip;
	public var mc_loc_18:MovieClip;
	
	/** Les casquettes en elles même */
	public var p_mcCasquette_1:MovieClip;
	public var p_mcCasquette_2:MovieClip;
	public var p_mcCasquette_3:MovieClip;
	public var p_mcCasquette_4:MovieClip;
	public var p_mcCasquette_5:MovieClip;
	public var p_mcCasquette_6:MovieClip;
	public var p_mcCasquette_7:MovieClip;
	public var p_mcCasquette_8:MovieClip;
	public var p_mcCasquette_9:MovieClip;
	public var p_mcCasquette_10:MovieClip;
	public var p_mcCasquette_11:MovieClip;
	public var p_mcCasquette_12:MovieClip;
	public var p_mcCasquette_13:MovieClip;
	public var p_mcCasquette_14:MovieClip;
	public var p_mcCasquette_15:MovieClip;
	public var p_mcCasquette_16:MovieClip;
	public var p_mcCasquette_17:MovieClip;
	public var p_mcCasquette_18:MovieClip;
	
	/** Les emplacements libre de casquettes */
	public var p_mcPlacement_1:MovieClip;
	public var p_mcPlacement_2:MovieClip;
	public var p_mcPlacement_3:MovieClip;
	public var p_mcPlacement_4:MovieClip;
	public var p_mcPlacement_5:MovieClip;
	public var p_mcPlacement_6:MovieClip;
	public var p_mcPlacement_7:MovieClip;
	public var p_mcPlacement_8:MovieClip;
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	/** Liste des casquettes */
	private var p_aListElem:Array;
	
	/** Etat du bouton d'action (1 = depart, 2 = j'ai fini)  */	
	private var p_iStateAction:Number;
	
	/** tableau de victoire  */	
	private var p_aWin:Array;
	
	/** Nombre de victoire  */	
	private var p_nWin:Number;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game2_04() {
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		this.p_nWin = 0;

		this.initInterface();
	}
	
	/**
	* Fonction qui renvoi le tableau mélangé
	* @param Array tab0_array le tableau d'origine
	* @return Array
	*/
	public function randomTab(tab0_array:Array) {
	    var tab1_array:Array = new Array();
	    var tabTmp_array:Array = new Array();
	    var cpt:Number = 0;
	    for(var i:Number = 0 ; i < tab0_array.length ; i++) {
	        tabTmp_array.push(false);
	    }
	    while(cpt < tab0_array.length) {
	        var j:Number = randRange(0, tab0_array.length-1);
	        if(tabTmp_array[j] == false) {
	            tabTmp_array[j] = true;
	            tab1_array.push(tab0_array[j]);
	            cpt++;
	        }
	    }
	    return tab1_array;
	}
	
	/**
	* Fonction qui renvoi un nombre aleatoire entre min et max
	* @param Number min
	* @param Number max
	* @return Number
	*/
	public function randRange(min:Number, max:Number) {
	     var randomNum:Number = Math.floor(Math.random() * (max - min + 1)) + min;
	     return randomNum;
	} 
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
		
	/**
	 * initialisation de l'interface
	 */
	private function initInterface() {	
		var nRand:Number;
		var nCompteur:Number;
		var mcTemp:MovieClip;
		
		this.p_iStateAction = 1;
		this.p_mcAction.gotoAndStop(1);
		this.p_aListElem = new Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
		
		this.p_aWin = new Array(-1, 0, 0, 0, 0, 0, 0, 0, 0);
		// mélange de ce tableau 
		this.p_aListElem = randomTab(this.p_aListElem);
		
		
		// on affiche les casquettes rangées au hazard
		if (this.p_nWin == 0) {
			for (var i:Number = 0 ; i < this.p_aListElem.length ; ++i) {
				attachMovie("casquette_"+this.p_aListElem[i], "p_mcCasquette_"+p_aListElem[i], this.getNextHighestDepth(), {_x:this["mc_loc_"+(i + 1)]._x, _y:this["mc_loc_"+(i + 1)]._y});
			}
			this.initCasquette();
		} else { // Si ce n'est pas la premiere partie, on les replace juste
			for (var i:Number = 0 ; i < this.p_aListElem.length ; ++i) {
				this["p_mcCasquette_"+p_aListElem[i]]._x = this["mc_loc_"+(i + 1)]._x;
				this["p_mcCasquette_"+p_aListElem[i]]._y = this["mc_loc_"+(i + 1)]._y;
				this["p_mcCasquette_"+p_aListElem[i]].enabled = false;
			}
		}
		 
		// boutons départ et suite
		this.p_mcAction.onRelease = function(){
			if (_parent.p_iStateAction == 1) {
				
				_parent.soundPlayer.playASound("clang1");
				_parent.p_iStateAction = 2;
				// On se met sur un 'j'ai fini'
				this.gotoAndStop(2);
				// On place au hazard une casquette sur le premier emplacement
				nRand = _parent.randRange(1, 18);
				
				_parent["p_mcCasquette_"+nRand]._x = _parent.p_mcPlacement_1._x;
				_parent["p_mcCasquette_"+nRand]._y = _parent.p_mcPlacement_1._y;
				
	
				// On rend toutes les autes casquettes draguables
				for (var i:Number = 0 ; i < _parent.p_aListElem.length ; ++i) {
					mcTemp = _parent["p_mcCasquette_"+(i + 1)]
					mcTemp.j = i + 1;
					if ((mcTemp.j) != nRand) {
						mcTemp.enabled = true;
						_parent.dragDrop(mcTemp);
					}
				}
			} else if (_parent.p_iStateAction == 2) {
				nCompteur = 0;
				// Parcours des casquettes
				for (var i:Number = 1 ; i <= 18 ; ++i) {
					// pour chaque emplacement
					for (var j:Number = 1 ; j <= 8 ; ++j) {
						// Si la casquette est sur un emplacement, on sauvegarde
						if (_parent["p_mcCasquette_"+i]._x == _parent["p_mcPlacement_"+j]._x &&
							_parent["p_mcCasquette_"+i]._y == _parent["p_mcPlacement_"+j]._y) {
							_parent.p_aWin[j] = i;
							++nCompteur;
						}							
					}
				}
				if (nCompteur == 8) {
					// Il a bien place les 8 casquettes, on vérifie
					_parent.checkWin();
					
				} else {
					// Le jeu n'est pas fini
					_parent.soundPlayer.playASound("JeuPasTerminer");
			
					trace ("Jeu non termine : "+nCompteur);
					for (var j:Number = 1 ; j <= 8 ; ++j) {
						_parent.aWin[j] = 0;
					}
				}
			}	
		}
	}	
	
	private function dragDrop(mc_drag:MovieClip) {
		var nFlag:Number;
		
		mc_drag.onPress = function(){
			if (_parent.p_nWin < 3 && _parent.p_iStateAction == 2) {
				this.old_depth = this.getDepth();
				this.swapDepths(_parent.getNextHighestDepth());
				// Les coordonnées d'avant déplacement
				this.x_origine = this._x;
				this.y_origine = this._y;
				this.startDrag();
				nFlag = 1;
			}
		}
		mc_drag.onRelease = function(){
			_parent.soundPlayer.playASound("minitic");
			this.stopDrag();
			this.swapDepths(this.old_depth);
			// Les coordonnées d'avant déplacement
			_parent.actionCasquette(this);
			nFlag = 0;
		}
	}
	
	/** Gere le mouvement du drag and drop des casquettes */
	private function actionCasquette(mcCasquette:MovieClip) {
		var mcTemp:MovieClip;
		var bFlag:Boolean;
						
		// parcours des emplacements ronds possibles
		for (var i:Number = 2 ; i <= 8 ; ++i) {
			mcTemp = mcCasquette;
			mcTemp.i = i;
			if (mcTemp.hitTest(this["p_mcPlacement_"+mcTemp.i])) {
				// On vérifie qu'il n'y a pas une autre casquette sur cette case
				for (var j:Number = 1 ; j <= 18 ; ++j) {
					if (this["p_mcCasquette_"+j]._x == this["p_mcPlacement_"+mcTemp.i]._x && 
						this["p_mcCasquette_"+j]._y == this["p_mcPlacement_"+mcTemp.i]._y) {
						// Si oui, on alterne
						this["p_mcCasquette_"+j]._x = mcCasquette.x_origine;
						this["p_mcCasquette_"+j]._y = mcCasquette.y_origine;
					}
				}
				mcTemp._x = this["p_mcPlacement_"+mcTemp.i]._x;
				mcTemp._y = this["p_mcPlacement_"+mcTemp.i]._y;	

				return true;
			}
		}
		// Parcours des cases d'origines 
		for (var i:Number = 1 ; i <= 18 ; ++i) {
			mcTemp = mcCasquette;
			mcTemp.i = i;
			if (mcTemp.hitTest(this["mc_loc_"+mcTemp.i])) {
				// On vérifie qu'il n'y a pas une autre casquette sur cette case
				for (var j:Number = 1 ; j <= 18 ; ++j) {
					if (this["p_mcCasquette_"+j]._x == this["mc_loc_"+mcTemp.i]._x && 
						this["p_mcCasquette_"+j]._y == this["mc_loc_"+mcTemp.i]._y) {
						// Si oui, on alterne
						this["p_mcCasquette_"+j]._x = mcCasquette.x_origine;
						this["p_mcCasquette_"+j]._y = mcCasquette.y_origine;
					}
				}		
				mcTemp._x = this["mc_loc_"+mcTemp.i]._x;
				mcTemp._y = this["mc_loc_"+mcTemp.i]._y;

				return true;
			}
		}
		// S'il n'a été placé nul part, on le remet la ou il était
		mcTemp._x = mcTemp.x_origine;
		mcTemp._y = mcTemp.y_origine;			
	}
	
	
	/* On vérifie que l'ordre de victoire est correcte */
	private function checkWin() {
		var nTaille:Number = this["p_mcCasquette_"+this.p_aWin[8]].taille;
		var nCouleur:Number = this["p_mcCasquette_"+this.p_aWin[8]].couleur;
		var nSigne:Number = this["p_mcCasquette_"+this.p_aWin[8]].signe;
		var nTest:Number;
		
		// On Parcours les casquettes
		for (var i:Number = 1 ; i <= 8 ; ++i) {
			nTest = 0;
			if (this["p_mcCasquette_"+this.p_aWin[i]].taille == nTaille) {
			//	trace (this["p_mcCasquette_"+this.p_aWin[i]].taille +" == "+ nTaille);
				nTest++;	
			}
			if (this["p_mcCasquette_"+this.p_aWin[i]].couleur == nCouleur) {
			//	trace (this["p_mcCasquette_"+this.p_aWin[i]].couleur +" == "+ nCouleur);
				nTest++;	
			}
			if (this["p_mcCasquette_"+this.p_aWin[i]].signe == nSigne) {
			//	trace (this["p_mcCasquette_"+this.p_aWin[i]].signe +" == "+ nSigne);
				nTest++;	
			}
			if (nTest < 2) {
				// Incorrect
				this.p_mcLoading.swapDepths(this.getNextHighestDepth());
				this.p_mcLoading.gotoAndPlay(2);					
				return false
			}
			nTaille = this["p_mcCasquette_"+this.p_aWin[i]].taille;
			nCouleur = this["p_mcCasquette_"+this.p_aWin[i]].couleur;
			nSigne = this["p_mcCasquette_"+this.p_aWin[i]].signe;
		}
		// Les differentes conditions de victoires
		++this.p_nWin;
		if (this.p_nWin == 1) {
			this.gotoAndStop("win1");
			soundPlayer.playASound("shark2");
			this.initInterface();	
		} else if (this.p_nWin == 2) {
			this.gotoAndStop("win2");
			soundPlayer.playASound("shark2");
			this.initInterface();
		} else if (this.p_nWin == 3) {
			// Le jeu est terminé
			this.p_iStateAction = 3;
			
			// Pour chaque case ronde on va rendre la casquette invisible
			for (var i:Number = 1 ; i <= 8 ; ++i) {
				// On regarde la casquette sur la cas
				for (var j:Number = 1 ; j <= 18 ; ++j) {
					if (this["p_mcCasquette_"+j]._x == this["p_mcPlacement_"+i]._x && 
						this["p_mcCasquette_"+j]._y == this["p_mcPlacement_"+i]._y) {
						// On la rend invisible
						this["p_mcCasquette_"+j]._visible = false;
					}
				}
			}
			this.gotoAndStop("win3");
			this.p_mcFinish.onPress = function(){
				_parent._parent._parent.barreInterface_mc.showParchemin();
				_parent.soundPlayer.playASound("scratch");
				
				for (var i:Number = 1 ; i <= 18 ; ++i) {
					// On regarde la casquette sur la cas
					for (var j:Number = 1 ; j <= 18 ; ++j) {
						if (_parent["p_mcCasquette_"+j]._x == _parent["mc_loc_"+i]._x && 
							_parent["p_mcCasquette_"+j]._y == _parent["mc_loc_"+i]._y) {
							// On la rend invisible
							_parent["p_mcCasquette_"+j]._visible = false;
						}
					}
				}
	
				//on enregistre le resultat  
				this._parent._parent._parent.setWinGame(4, this._parent._parent._parent.joueur1);
				_parent.gotoAndStop("end");
				gameFinished();
			}
		}
		return true;
	}
	
	/** On met à la main les caracteristiques des casquettes */
	private function initCasquette() {
		// taille 
		this.p_mcCasquette_1.taille  = "grosse";
		this.p_mcCasquette_2.taille  = "grosse";
		this.p_mcCasquette_3.taille  = "grosse";
		this.p_mcCasquette_4.taille  = "petite";
		this.p_mcCasquette_5.taille  = "petite";
		this.p_mcCasquette_6.taille  = "petite";
		this.p_mcCasquette_7.taille  = "grosse";
		this.p_mcCasquette_8.taille  = "grosse";
		this.p_mcCasquette_9.taille  = "grosse";
		this.p_mcCasquette_10.taille = "petite";
		this.p_mcCasquette_11.taille = "petite";
		this.p_mcCasquette_12.taille = "petite";
		this.p_mcCasquette_13.taille = "grosse";
		this.p_mcCasquette_14.taille = "grosse";
		this.p_mcCasquette_15.taille = "grosse";
		this.p_mcCasquette_16.taille = "petite";
		this.p_mcCasquette_17.taille = "petite";
		this.p_mcCasquette_18.taille = "petite";
		// couleur
		this.p_mcCasquette_1.couleur  = "bleue";
		this.p_mcCasquette_2.couleur  = "bleue";
		this.p_mcCasquette_3.couleur  = "bleue";
		this.p_mcCasquette_4.couleur  = "bleue";
		this.p_mcCasquette_5.couleur  = "bleue";
		this.p_mcCasquette_6.couleur  = "bleue";
		this.p_mcCasquette_7.couleur  = "verte";
		this.p_mcCasquette_8.couleur  = "verte";
		this.p_mcCasquette_9.couleur  = "verte";
		this.p_mcCasquette_10.couleur = "verte";
		this.p_mcCasquette_11.couleur = "verte";
		this.p_mcCasquette_12.couleur = "verte";
		this.p_mcCasquette_13.couleur = "rouge";
		this.p_mcCasquette_14.couleur = "rouge";
		this.p_mcCasquette_15.couleur = "rouge";
		this.p_mcCasquette_16.couleur = "rouge";
		this.p_mcCasquette_17.couleur = "rouge";
		this.p_mcCasquette_18.couleur = "rouge";
		// Signe
		this.p_mcCasquette_1.signe  = "ancre";
		this.p_mcCasquette_2.signe  = "triangle";
		this.p_mcCasquette_3.signe  = "pince";
		this.p_mcCasquette_4.signe  = "ancre";
		this.p_mcCasquette_5.signe  = "triangle";
		this.p_mcCasquette_6.signe  = "pince";
		this.p_mcCasquette_7.signe  = "ancre";
		this.p_mcCasquette_8.signe  = "triangle";
		this.p_mcCasquette_9.signe  = "pince";
		this.p_mcCasquette_10.signe = "ancre";
		this.p_mcCasquette_11.signe = "triangle";
		this.p_mcCasquette_12.signe = "pince";
		this.p_mcCasquette_13.signe = "ancre";
		this.p_mcCasquette_14.signe = "triangle";
		this.p_mcCasquette_15.signe = "pince";
		this.p_mcCasquette_16.signe = "ancre";
		this.p_mcCasquette_17.signe = "triangle";
		this.p_mcCasquette_18.signe = "pince";
	}
}

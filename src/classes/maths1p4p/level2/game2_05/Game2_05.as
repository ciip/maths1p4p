﻿/**
 * class com.maths1p4p.Game2_05
 * 
 * @author Yoann Augen
 * @version 1.0
 *
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.application.Application;

class maths1p4p.level2.game2_05.Game2_05 extends maths1p4p.AbstractGame {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 5;
	/** Les clips contenant les additions */
	var p_mcOpe_1:MovieClip;
	var p_mcOpe_2:MovieClip;
	var p_mcOpe_3:MovieClip;
	var p_mcOpe_4:MovieClip;
	var p_mcOpe_5:MovieClip;
	var p_mcOpe_6:MovieClip;
	var p_mcOpe_7:MovieClip;
	var p_mcOpe_8:MovieClip;
	var p_mcOpe_9:MovieClip;
	var p_mcOpe_10:MovieClip;
	var p_mcOpe_11:MovieClip;
	var p_mcOpe_12:MovieClip;
	var p_mcOpe_13:MovieClip;
	var p_mcOpe_14:MovieClip;
	var p_mcOpe_15:MovieClip;
	var p_mcOpe_16:MovieClip;
	var p_mcOpe_17:MovieClip;
	var p_mcOpe_18:MovieClip;
	var p_mcOpe_19:MovieClip;
	var p_mcOpe_20:MovieClip;
	var p_mcOpe_21:MovieClip;
	var p_mcOpe_22:MovieClip;
	var p_mcOpe_23:MovieClip;
	var p_mcOpe_24:MovieClip;
	var p_mcOpe_25:MovieClip;
	var p_mcOpe_26:MovieClip;
	
	var p_mcDalle_1:MovieClip;
	var p_mcDalle_2:MovieClip;
	var p_mcDalle_3:MovieClip;
	var p_mcDalle_4:MovieClip;
	var p_mcDalle_5:MovieClip;
	var p_mcDalle_6:MovieClip;
	var p_mcDalle_7:MovieClip;
	var p_mcDalle_8:MovieClip;
	var p_mcDalle_9:MovieClip;
	var p_mcDalle_10:MovieClip;
	var p_mcDalle_11:MovieClip;
	var p_mcDalle_12:MovieClip;
	var p_mcDalle_13:MovieClip;
	var p_mcDalle_14:MovieClip;
	var p_mcDalle_15:MovieClip;
	var p_mcDalle_16:MovieClip;
	var p_mcDalle_17:MovieClip;
	var p_mcDalle_18:MovieClip;
	var p_mcDalle_19:MovieClip;
	var p_mcDalle_20:MovieClip;
	var p_mcDalle_21:MovieClip;
	var p_mcDalle_22:MovieClip;
	var p_mcDalle_23:MovieClip;
	var p_mcDalle_24:MovieClip;
	var p_mcDalle_25:MovieClip;
	var p_mcDalle_26:MovieClip;
	
	/** Les clips contenant les chiffres du bas */
	var p_mcChiffre_5:MovieClip;
	var p_mcChiffre_6:MovieClip;
	var p_mcChiffre_7:MovieClip;
	var p_mcChiffre_8:MovieClip;
	var p_mcChiffre_9:MovieClip;
	var p_mcChiffre_10:MovieClip;
	var p_mcChiffre_11:MovieClip;
	var p_mcChiffre_12:MovieClip;
	var p_mcChiffre_13:MovieClip;
	var p_mcChiffre_14:MovieClip;
	var p_mcChiffre_15:MovieClip;
	var p_mcChiffre_16:MovieClip;
	
	
	
	/** Definition des valeurs possibles */
	public var p_aListeAddCorrect:Array = new Array(  
										new Array("6 + 7", "13"), 
									    new Array("11 + 3", "14"), 
										new Array("6 + 4", "10"), 
										new Array("0 + 6", "6"), 
										new Array("4 + 5", "9"), 
										new Array("1 + 6", "7"), 
										new Array("10 + 1", "11"), 
										new Array("3 + 4", "7"), 
										new Array("7 + 1", "8"), 
										new Array("3 + 2", "5"), 
										new Array("15 + 1", "16"), 
										new Array("12 + 3", "15"), 
										new Array("10 + 2", "12"), 
										new Array("0 + 7", "7"), 
										new Array("9 + 1", "10"),
										new Array("4 + 1", "5"),
										new Array("12 + 2", "14"),
										new Array("1 + 12", "13"),
										new Array("3 + 6", "9"),
										new Array("9 + 6", "15"),
										new Array("2 + 10", "12"),
										new Array("5 + 3", "8"),
										new Array("5 + 1", "6"),
										new Array("0 + 9", "9"));
										
	public var p_aListeAddDivers:Array = new Array( 
										new Array("2 + 2", "4"), 
									    new Array("1 + 1", "2"), 
										new Array("8 + 10", "18"), 
										new Array("0 + 1", "1"), 
										new Array("9 + 8", "17"), 
										new Array("0 + 17", "17"), 
										new Array("5 + 15", "20"), 
										new Array("11 + 7", "18"), 
										new Array("3 + 1", "4"), 
										new Array("0 + 4", "4"), 
										new Array("1 + 3", "4"), 
										new Array("1 + 2", "3"), 
										new Array("0 + 3", "3"), 
										new Array("11 + 6", "17"), 
										new Array("3 + 1", "4"));
										
	public var p_aListeSubCorrect:Array = new Array(  
									    new Array("8 - 2", "6"), 
										new Array("15 - 4", "11"), 
										new Array("9 - 2", "7"), 
										new Array("16 - 8", "8"), 
										new Array("17 - 8", "9"), 
										new Array("12 - 2", "10"), 
										new Array("12 - 4", "8"), 
										new Array("16 - 8", "8"), 
										new Array("9 - 2", "7"), 
										new Array("15 - 4", "11"), 
										new Array("15 - 8", "7"), 
										new Array("11 - 1", "10"), 
										new Array("16 - 7", "9"), 
										new Array("11 - 1", "10"), 
										new Array("12 - 2", "10"), 
										new Array("16 - 7", "9"), 
										new Array("14 - 8", "6"));
	
	public var p_aListeSubDivers:Array = new Array(  new Array("6 - 3", "3"), 
									    new Array("28 - 9", "19"), 
										new Array("21 - 1", "20"), 
										new Array("10 - 6", "4"), 
										new Array("15 - 12", "3"), 
										new Array("21 - 4", "17"), 
										new Array("8 - 4", "4"), 
										new Array("10 - 2", "8"), 
										new Array("9 - 8", "1"), 
										new Array("5 - 5", "0"), 
										new Array("9 - 8", "1"), 
										new Array("7 - 5", "2"), 
										new Array("5 - 5", "0"), 
										new Array("18 - 1", "17"), 
										new Array("12 - 9", "3"), 
										new Array("15 - 11", "4"), 
										new Array("7 - 5", "2"), 
										new Array("8 - 5", "3"));
	/** clip d'erreur commise */
	var p_mcErreur:MovieClip;
	
	/** Premiere porte */
	var p_mcPorte:MovieClip;
	var p_mcPorteDebut:MovieClip;
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	/** Valeur max 14 ou 16 */
	var p_nMaxValue:Number;
	
	/** Partie 1 ou 2 */
	var p_iNumberGame:Number;	
	
	/** Partie tableau servant au remplissage des case d'oppération */
	var p_aTabOpp:Array = new Array();
	
	/** nombre d'erreur commise */
	var p_nErreur:Number;
	
	/** Si la manche est fini */
	var p_bEnd:Boolean;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game2_05() {
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		var go:Number = 0;
		
		this.p_iNumberGame = 1;
		this.p_nErreur = 0;
		this.p_mcPorteDebut.onPress = function () {
			_parent.soundPlayer.playASound("grince2");
			_parent.gotoAndStop(2);
		}		
	}
	
	/**
	* Fonction qui renvoi le tableau mélangé
	* @param Array tab0_array le tableau d'origine
	* @return Array
	*/
	public function randomTab(tab0_array:Array) {
	    var tab1_array:Array = new Array();
	    var tabTmp_array:Array = new Array();
	    var cpt:Number = 0;
	    for(var i:Number = 0 ; i < tab0_array.length ; i++) {
	        tabTmp_array.push(false);
	    }
	    while(cpt < tab0_array.length) {
	        var j:Number = randRange(0, tab0_array.length-1);
	        if(tabTmp_array[j] == false) {
	            tabTmp_array[j] = true;
	            tab1_array.push(tab0_array[j]);
	            cpt++;
	        }
	    }
	    return tab1_array;
	}
	/**
	* Fonction qui renvoi un nombre aleatoire entre min et max
	* @param Number min
	* @param Number max
	* @return Number
	*/
	public function randRange(min:Number, max:Number) {
	     var randomNum:Number = Math.floor(Math.random() * (max - min + 1)) + min;
	     return randomNum;
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/** Initialisation du plateau de jeu */
	private function initInterface() {
		this.p_bEnd = false;
		this.p_nErreur = 0;
		this.p_mcErreur.gotoAndStop(1);
		
		// On efface les dalles et les nombres 
		for (var x:Number = 1 ; x <= 26 ; ++x) {
			this.p_aTabOpp[x] = 0;
			this["p_mcDalle_"+x]._visible = false;
			this["p_mcOpe_"+x].txtValue.text = '';
			this["p_mcOpe_"+x].soluce = null;
			this["p_mcOpe_"+x].txtValue.textColor = 0x000000;
		}
		  
		
		// affichage des Soustraction ou addition
		this.afficheOppCase();
		
		// Si on est à la seconde manche on désactive certain premier chiffre
		if (this.p_nMaxValue == 14 && this.p_iNumberGame == 2) {
			this.p_mcChiffre_15.enabled = false;
			this.p_mcChiffre_16.enabled = false;
		} else if (this.p_nMaxValue == 15 && this.p_iNumberGame == 2) {
			this.p_mcChiffre_16.enabled = false;
		}
		
					
		// Affichage des nombres du bas
		for (var i:Number = 5 ; i <= this.p_nMaxValue ; ++i) {
			_parent["p_mcChiffre_"+i].enabled = true;
			this["p_mcChiffre_"+i].txtValeur.text = i;
			this["p_mcChiffre_"+i].txtValeur.textColor = 0x000000;
			// On les rend dragable
			this["p_mcChiffre_"+i].onPress = function () {
				if (_parent.p_bEnd == false) {
					this.init_x = this._x;
					this.init_y = this._y;
					this._x = _parent._xmouse;
					this._y = _parent._ymouse;
					this.startDrag();
				}
			}
			this["p_mcChiffre_"+i].onRelease = function () {
				_parent.releaseNumber(this);
			}
		}
	}
	
	/** Gestion du lachage des nombres */
	private function releaseNumber(mcNumber:MovieClip) {
		var bContinue:Boolean = true;
		var bFind:Boolean = false;
		// Parcours des cases 
		for (var x:Number = 1 ; x <= 26 ; ++x) {
			if (mcNumber.hitTest(this["p_mcOpe_"+x]) && bContinue == true) {
				bFind = true;
				// Si la dalle n'est pas déjà la et qu'un voisin à deja sa plaque (ou premiere ligne)
				if (isPossible(x)) {
					soundPlayer.playASound("shark2");
					// Ce doit être la bonne valeur
					if (this["p_mcOpe_"+x].soluce == Number(mcNumber.txtValeur.text)) {
						bContinue = false;
						// On rend visible la dalle
						this["p_mcDalle_"+x]._visible = true;
						// On marque que cette dalle est dévoilé
						//this.p_aTabOpp[x][y] = 2;
						// Si on est au bout, il a gagné 
						if (x >= 21) {
							this.victoireManche();	
						}
					} else {
						// Si le nombre est mauvais on rajoute une erreur
						bContinue = false;
						++this.p_nErreur;
						if (this.p_nErreur < 4) {
							this.p_mcErreur.gotoAndStop(p_nErreur + 1);
						} else {
							// Reset
							soundPlayer.playASound("boum");
							this.initInterface();
						}
					}
				} 
			} 
		} 
		if (bFind == false) {
			soundPlayer.playASound("MiniClick");
		}	
		mcNumber._x = mcNumber.init_x;
		mcNumber._y = mcNumber.init_y;
		mcNumber.stopDrag();		
	}
	
	/** Affichage des apérations sur les cases */
	private function afficheOppCase() { 
		var nRand:Number;
		// Direction (0 = rien, 1 = gauche, 2 = droite)
		//var nDir:Number;
		
		if (this.p_iNumberGame == 1) {
			var aActu:Array  = this.p_aListeAddCorrect;	
			var aActu2:Array = this.p_aListeAddDivers;	
		} else {
			var aActu:Array  = this.p_aListeSubCorrect;
			var aActu2:Array = this.p_aListeSubDivers;	
		}
		
		
		 
				
		var tab_true:Array = new Array(	new Array(3,7,8,13,19,18,17,16,15,21),
										new Array(2,6,5,10,16,17,18,19,20,26),
										new Array(3,7,6,5,10,16,17,18,19,25),
										new Array(2,6,7,8,13,19,18,17,16,22),
										new Array(1,5,6,7,12,18,17,16,15,21),
										new Array(1,5,6,7,8,13,19,18,17,23),
										new Array(4,8,7,6,5,10,16,17,18,24),
										new Array(4,8,7,6,11,17,18,19,20,26),
										new Array(3,7,6,5,10,16,17,18,19,20,26),
										new Array(2,6,7,8,13,19,18,17,16,15,21),
										new Array(1,5,6,7,8,13,19,18,17,16,22),
										new Array(4,8,7,6,5,10,16,17,18,19,25),
										new Array(4,8,13,19,18,17,11,10,9,15,21),
										new Array(1,5,10,16,17,18,12,13,14,20,26),
										new Array(2,6,5,10,16,17,18,12,13,14,20,26),
										new Array(3,7,8,13,19,18,17,11,10,9,15,21),
										new Array(4,8,7,6,5,10,16,17,18,19,20,26),
										new Array(1,5,6,7,8,13,19,18,17,16,15,21) 
										);
		var tab_false:Array = new Array(new Array(4,6,10,11,12,22,23,24,25,26),
										new Array(1,11,12,13,15,22,23,24,25),
										new Array(1,2,11,12,13,22,23,24),
										new Array(3,4,10,11,12,21,23,24,25,26),
										new Array(2,3,10,11,13,19,22,23,24),
										new Array(2,3,4,11,12,21,22,24,25,26),
										new Array(1,2,3,11,12,15,22,23),
										new Array(2,3,5,12,13,21,22,23,24,25),
										new Array(1,2,11,12,13,21,22,23,24,25),
										new Array(3,4,10,11,12,22,23,24,25,26),
										new Array(2,3,4,10,11,12,23,24,25,26),
										new Array(1,2,3,11,12,13,21,22,23,24,26),
										new Array(2,5,6,12,14,16,23,24,25,26),
										new Array(4,7,11,19,21,22,23,24),
										new Array(1,7,8,11,19,21,22,23,24),
										new Array(4,5,6,12,16,22,23,24,25,26),
										new Array(1,2,3,11,12,13,21,22,23,24,25),
										new Array(2,3,4,10,11,12,22,23,24,25,26) 
										);
		var tab_sup:Array = new Array(  new Array(1,2,5,9,14,20),
										new Array(3,4,7,8,9,14,21),
										new Array(4,8,9,14,15,20,21,26),
										new Array(1,5,9,14,15,20),
										new Array(4,8,9,14,20,25,26),
										new Array(9,10,14,15,16,20),
										new Array(9,13,14,19,20,21,25,26),
										new Array(1,9,10,14,15,16),
										new Array(4,8,9,14,15),
										new Array(1,5,9,14,20),
										new Array(9,14,15,20,21),
										new Array(9,14,15,20),
										new Array(1,3,7,20,22),
										new Array(2,3,6,8,9,15,25),
										new Array(3,4,9,15,25),
										new Array(1,2,14,20),
										new Array(9,14,15),
										new Array(9,14,20) 
										);
		var parcours:Number = Math.round(Math.random()*(tab_true.length - 1));
		
		
		// on definie une valeur max
		this.p_nMaxValue = 0;
		// On mélange le tableau des résultats corrects
		var aTab:Array = randomTab(aActu);
		// On tire un premier indice correcte au hazard
		var iChoose:Number = Math.round(Math.random()*(aTab.length - 1));
		
		// On remplis les cases correctes
		for (var i = 0 ; i < tab_true[parcours].length ; ++i) {
			// Affichage de l'op et sauvegarde du resultat		
			this["p_mcOpe_"+tab_true[parcours][i]].txtValue.text = aTab[iChoose][0];
			this["p_mcOpe_"+tab_true[parcours][i]].soluce = aTab[iChoose][1];
			// Recherche de la valeur la plus grande
			if (Number(aTab[iChoose][1]) > this.p_nMaxValue) {
				this.p_nMaxValue = aTab[iChoose][1];			
			}
			// On incremente l'indice pour la prochaine solution
			iChoose = (iChoose + 1)%(aActu.length - 1);
		}
		// On remplis les cases correctes supplementaires
		for (var i = 0 ; i < tab_sup[parcours].length ; ++i) {
			// Affichage de l'op et sauvegarde du resultat		
			this["p_mcOpe_"+tab_sup[parcours][i]].txtValue.text = aTab[iChoose][0];
			this["p_mcOpe_"+tab_sup[parcours][i]].soluce = aTab[iChoose][1];
			// Recherche de la valeur la plus grande
			if (Number(aTab[iChoose][1]) > this.p_nMaxValue) {
				this.p_nMaxValue = aTab[iChoose][1];			
			}
			// On incremente l'indice pour la prochaine solution
			iChoose = (iChoose + 1)%(aActu.length - 1);
		}
		
		// On mélange le tableau des résultats incorrects
		var aTab:Array = randomTab(aActu2);
		// On tire un premier indice correcte au hazard
		var iChoose:Number = Math.round(Math.random()*(aTab.length - 1));
		// On remplis les cases incorrectes 
		for (var i = 0 ; i < tab_false[parcours].length ; ++i) {
			// Affichage de l'op et sauvegarde du resultat		
			this["p_mcOpe_"+tab_false[parcours][i]].txtValue.text = aTab[iChoose][0];
			this["p_mcOpe_"+tab_false[parcours][i]].soluce = aTab[iChoose][1];
			// Recherche de la valeur la plus grande
			if (Number(aTab[iChoose][1]) > this.p_nMaxValue) {
				this.p_nMaxValue = aTab[iChoose][1];			
			}
			// On incremente l'indice pour la prochaine solution
			iChoose = (iChoose + 1)%(aActu2.length - 1);
		}
		
		// Algo de construction du parcours
		
		
		// On commence par verrouiller les 4 cases du bas
		/*this.p_aTabOpp[0][0] = 1;
		this.p_aTabOpp[0][1] = 1;
		this.p_aTabOpp[5][0] = 1;
		this.p_aTabOpp[5][1] = 1;
		
		// On tire un premier indice au hazard
		iChoose = Math.round(Math.random()*(aTabCorrect.length - 1));
		
		// Pour chaque ligne, on va partir à gauche ou à droite
		// avec 50% de chance de continuer à chaque fois. 
		// Quand on ne peut plus avancer on avant d'une ligne
		nX = randRange(1, 4)
		nY = 0;
		this["p_mcOpe_"+nX+"_"+nY].txtValue.text = aTabCorrect[iChoose][0];
		this["p_mcOpe_"+nX+"_"+nY].soluce = aTabCorrect[iChoose][1];
		this.p_nMaxValue = aTabCorrect[iChoose][1];
		iChoose = (iChoose + 1)%(aActu.length - 1);
		this.p_aTabOpp[nX][nY] = 1;
		nDir = 0;
		++nY;
		
			
		while (nY <= 4) {
			// Si on est monté, on affiche le premier
			if (nDir == 0) {
				// On stoque l'opération et la solution				
				this["p_mcOpe_"+nX+"_"+nY].txtValue.text = aTabCorrect[iChoose][0];
				this["p_mcOpe_"+nX+"_"+nY].soluce = aTabCorrect[iChoose][1];
				if (Number(aTabCorrect[iChoose][1]) > this.p_nMaxValue) {
					this.p_nMaxValue = aTabCorrect[iChoose][1];			
				}
				// On incremente l'indice
				iChoose = (iChoose + 1)%(aActu.length - 1);
				this.p_aTabOpp[nX][nY] = 1;
				
				// On fini
				if (nY == 4) {
					++nY;
					continue;	
				}
			}
			nRand = randRange(0, 10);
			trace (nRand);
			if ((nRand >= 0 && nRand <= 4) && this.p_aTabOpp[nX - 1][nY - 1] == 1) {
				nRand = 6;
			} else if ((nRand >= 6 && nRand <= 10) && this.p_aTabOpp[nX + 1][nY] != 1) {
				nRand = 0;
			}
			
			// On part à gauche (50% de chance)
			if ((nRand >= 0 && nRand <= 4) && nDir != 2) {

				if ((nX - 1) >= 0 && this.p_aTabOpp[nX - 1][nY] != 1) {
					// Si la case est libre
					--nX;
					// On stoque l'opération et la solution				
					this["p_mcOpe_"+nX+"_"+nY].txtValue.text = aTabCorrect[iChoose][0];
					this["p_mcOpe_"+nX+"_"+nY].soluce = aTabCorrect[iChoose][1];
					if (Number(aTabCorrect[iChoose][1]) > this.p_nMaxValue) {
						this.p_nMaxValue = aTabCorrect[iChoose][1];			
					}
					// On incremente l'indice
					iChoose = (iChoose + 1)%(aActu.length - 1);
					this.p_aTabOpp[nX][nY] = 1;
					nDir = 1;
					continue;
				} 
			}
			// On part à droite (50% de chance)
			if ((nRand >= 6 && nRand <= 10) && nDir != 1) {
				// Si la case est libre
				if ((nX + 1) <= 5 && this.p_aTabOpp[nX + 1][nY] != 1) {
					++nX;
					// On affiche et on continue
					// On stoque l'opération et la solutien				
					this["p_mcOpe_"+nX+"_"+nY].txtValue.text = aTabCorrect[iChoose][0];
					this["p_mcOpe_"+nX+"_"+nY].soluce = aTabCorrect[iChoose][1];
					if (Number(aTabCorrect[iChoose][1]) > this.p_nMaxValue) {
						this.p_nMaxValue = aTabCorrect[iChoose][1];			
					}
					// On incremente l'indice
					iChoose = (iChoose + 1)%(aActu.length - 1);
					this.p_aTabOpp[nX][nY] = 1;
					nDir = 2;
					continue;
				} 
			}
			// Rien ne se passe, on monte et on affiche
			++nY;
			nDir = 0;			
		}
		if (this.p_nMaxValue  < 14) {
				this.p_nMaxValue = 14;
		}
		
		aTabCorrect = randomTab(aActu2);
		iChoose = Math.round(Math.random()*(aTabCorrect.length - 1));
		
		// Enfin on met des valeurs quelconque dans les autres endroit
		for (var y:Number = 0 ; y <= 4 ; ++y) {
			for (var x:Number = 0 ; x <= 5 ; ++x) {
				if (this.p_aTabOpp[x][y] == 0) { 
					this["p_mcOpe_"+x+"_"+y].txtValue.text = aTabCorrect[iChoose][0];
					this["p_mcOpe_"+x+"_"+y].soluce = aTabCorrect[iChoose][1];
					iChoose = (iChoose + 1)%(aActu2.length - 1);				
				}
			}
		}		*/
	}
	/** 
	 * Decide si on peu draguer un pio sur une case ou non
	 * @return : boolean 
	 */
	private function isPossible(x) {
		// Il faut qu'au moins une case adjacente à x ai son p_mcDalle_ visible
		// Si la dalle est deja la on renvoi false
		if (this["p_mcDalle_"+x]._visible == true) {
			return false;	
		}
		// Ceci est toujours correcte pour les 4 cases du bas
		if (x >= 1 && x <= 4) {
			return true;	
		}
		if (x == 5 && (this.p_mcDalle_1._visible == true || this.p_mcDalle_6._visible == true || this.p_mcDalle_10._visible == true)) {
			return true;
		}
		if (x == 6 && (this.p_mcDalle_2._visible == true || this.p_mcDalle_5._visible == true || this.p_mcDalle_7._visible == true || this.p_mcDalle_11._visible == true)) {
			return true;
		}
		if (x == 7 && (this.p_mcDalle_3._visible == true || this.p_mcDalle_6._visible == true || this.p_mcDalle_8._visible == true || this.p_mcDalle_12._visible == true)) {
			return true;
		}
		if (x == 8 && (this.p_mcDalle_4._visible == true || this.p_mcDalle_7._visible == true || this.p_mcDalle_13._visible == true)) {
			return true;
		}
		if (x == 9 && (this.p_mcDalle_15._visible == true || this.p_mcDalle_10._visible == true)) {
			return true;
		}
		if (x == 10 && (this.p_mcDalle_5._visible == true || this.p_mcDalle_9._visible == true || this.p_mcDalle_11._visible == true || this.p_mcDalle_16._visible == true)) {
			return true;
		}
		if (x == 11 && (this.p_mcDalle_6._visible == true || this.p_mcDalle_10._visible == true || this.p_mcDalle_12._visible == true || this.p_mcDalle_17._visible == true)) {
			return true;
		}
		if (x == 12 && (this.p_mcDalle_7._visible == true || this.p_mcDalle_11._visible == true || this.p_mcDalle_13._visible == true || this.p_mcDalle_18._visible == true)) {
			return true;
		}
		if (x == 13 && (this.p_mcDalle_8._visible == true || this.p_mcDalle_12._visible == true || this.p_mcDalle_14._visible == true || this.p_mcDalle_19._visible == true)) {
			return true;
		}
		if (x == 14 && (this.p_mcDalle_13._visible == true || this.p_mcDalle_20._visible == true)) {
			return true;
		}
		if (x == 15 && (this.p_mcDalle_9._visible == true || this.p_mcDalle_16._visible == true || this.p_mcDalle_21._visible == true)) {
			return true;
		}
		if (x == 16 && (this.p_mcDalle_10._visible == true || this.p_mcDalle_15._visible == true || this.p_mcDalle_17._visible == true || this.p_mcDalle_22._visible == true)) {
			return true;
		}
		if (x == 17 && (this.p_mcDalle_11._visible == true || this.p_mcDalle_16._visible == true || this.p_mcDalle_18._visible == true || this.p_mcDalle_23._visible == true)) {
			return true;
		}
		if (x == 18 && (this.p_mcDalle_12._visible == true || this.p_mcDalle_17._visible == true || this.p_mcDalle_19._visible == true || this.p_mcDalle_24._visible == true)) {
			return true;
		}
		if (x == 19 && (this.p_mcDalle_13._visible == true || this.p_mcDalle_18._visible == true || this.p_mcDalle_20._visible == true || this.p_mcDalle_25._visible == true)) {
			return true;
		}
		if (x == 20 && (this.p_mcDalle_14._visible == true || this.p_mcDalle_19._visible == true || this.p_mcDalle_26._visible == true)) {
			return true;
		}
		if (x == 21 && (this.p_mcDalle_15._visible == true || this.p_mcDalle_22._visible == true)) {
			return true;
		}
		if (x == 22 && (this.p_mcDalle_16._visible == true || this.p_mcDalle_21._visible == true || this.p_mcDalle_23._visible == true)) {
			return true;
		}
		if (x == 23 && (this.p_mcDalle_17._visible == true || this.p_mcDalle_22._visible == true || this.p_mcDalle_24._visible == true)) {
			return true;
		}
		if (x == 24 && (this.p_mcDalle_18._visible == true || this.p_mcDalle_23._visible == true || this.p_mcDalle_25._visible == true)) {
			return true;
		}	
		if (x == 25 && (this.p_mcDalle_19._visible == true || this.p_mcDalle_24._visible == true || this.p_mcDalle_25._visible == true)) {
			return true;
		}
		if (x == 26 && (this.p_mcDalle_20._visible == true || this.p_mcDalle_25._visible == true)) {
			return true;
		}
		return false;
	}
	
	/** 
	 * Lors de la victoire d'une manche, il faut griser les résultats, 
	 * puis on rend la porte cliquable
	 */
	private function victoireManche() {
		this.p_bEnd = true;
				
		// On rend les textes grisés
		for (var x:Number = 1 ; x <= 26 ; ++x) {
			this["p_mcOpe_"+x].txtValue.textColor = 0xCCCCCC;
		}
		for (var i:Number = 5 ; i <= this.p_nMaxValue ; ++i) {
			this["p_mcChiffre_"+i].txtValeur.textColor = 0xCCCCCC;
		}
		
		// On rend la porte cliquable 
		this.p_mcPorte.onPress = function () {
			_parent.soundPlayer.playASound("clicClac3");
			
			if (_parent.p_iNumberGame == 1) {
				for (var i:Number = 5 ; i <= _parent.p_nMaxValue ; ++i) {
					_parent["p_mcChiffre_"+i].txtValeur.text = "";
				}		
				_parent.p_iNumberGame = 2;
				_parent.gotoAndStop("part2");
				_parent.initInterface();
			} else if (_parent.p_iNumberGame == 2) {
				_parent.p_iNumberGame = 3;
				_parent.gotoAndStop("win");
				_parent.endGame();			
			} 
		}
	}
	
	/** Fin du jeu */
	private function endGame() {
		// On rend le parchemin cliquable
		this.p_mcPorte.onPress = function () {
			_parent.soundPlayer.playASound("scratch");
			
			
			this._parent._parent._parent.setWinGame(5, this._parent._parent._parent.joueur1); 
			_parent._parent._parent.barreInterface_mc.showParchemin();
			_parent.gotoAndStop("win2");
			gameFinished();
		}	
	}
}

﻿/**
 * class maths1p4p.level2.game2_06.game2_06
 * 
 * @author Rémi Gay
 * @version 1.0
 *
 * ETAT : pas commencé
 *
 * TO DO : 
 * => l 224 => ça doit pas etre le bon son
 * Jeu buggué : 
 * - Fenetre : "pas deux fois le meme nombre" alors qu'il n'y a pas deux fois le meme
 * - switch des nombres défectueux.
 * 
 * Copyright (c) 2008 Rémi Gay.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import mx.controls.Alert;
//import maths1p4p.level2.game2_06.*
import maths1p4p.utils.OutilsTableaux;

class maths1p4p.level2.game2_06.Game2_06 extends maths1p4p.AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 6;
	
	//variable stockant l'ancienne profondeur du clip draggé (car on lui a donné la plus grande profondeur possible)
	public static var lastDepth:Number = -11111;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/***** variables en rapport avec les éléments déjà present sur la scène *****/
	private var depart_btn:MovieClip;
	private var chiffre1_mc:MovieClip;
	private var chiffre2_mc:MovieClip;
	private var chiffre3_mc:MovieClip;
	private var chiffre4_mc:MovieClip;
	
	private var zoneChiffreDown1_mc:MovieClip;
	private var zoneChiffreDown2_mc:MovieClip;
	private var zoneChiffreDown3_mc:MovieClip;
	private var zoneChiffreDown4_mc:MovieClip;
	
	private var parchemin1_mc:MovieClip;
	private var parchemin2_mc:MovieClip;
	private var parchemin3_mc:MovieClip;
	private var parchemin4_mc:MovieClip;
	private var parchemin5_mc:MovieClip;
	
	private var upChiffre1_mc:MovieClip;
	private var upChiffre2_mc:MovieClip;
	private var upChiffre3_mc:MovieClip;
	private var upChiffre4_mc:MovieClip;
	private var upChiffre5_mc:MovieClip;
	private var upChiffre6_mc:MovieClip;
	private var upChiffre7_mc:MovieClip;
	private var upChiffre8_mc:MovieClip;
	private var upChiffre9_mc:MovieClip;
	private var upChiffre10_mc:MovieClip;
	private var upChiffre11_mc:MovieClip;
	private var upChiffre12_mc:MovieClip;
	private var upChiffre13_mc:MovieClip;
	private var upChiffre14_mc:MovieClip;
	
	private var ptChiffreUp1_mc:MovieClip;
	private var ptChiffreUp2_mc:MovieClip;
	private var ptChiffreUp3_mc:MovieClip;
	private var ptChiffreUp4_mc:MovieClip;
	private var ptChiffreUp5_mc:MovieClip;
	private var ptChiffreUp6_mc:MovieClip;
	private var ptChiffreUp7_mc:MovieClip;
	private var ptChiffreUp8_mc:MovieClip;
	private var ptChiffreUp9_mc:MovieClip;
	private var ptChiffreUp10_mc:MovieClip;
	private var ptChiffreUp11_mc:MovieClip;
	private var ptChiffreUp12_mc:MovieClip;
	private var ptChiffreUp13_mc:MovieClip;
	private var ptChiffreUp14_mc:MovieClip;
	
	
	
	
	/***** variables privées de la classe *****/
	//tableau composé des 7 nombres de 2 chiffres (pour les comparaisons)
	private var tabResultatNombres:Array;
	//tableau composé de 14 chiffres d'où on enlèvera 4 chiffres qui seront à replacer par le joueur
	private var tabResultatChiffres:Array;

	private var tabResultatClip:Array;
	private var tabChiffresEnAttente:Array;
	//variables mémorisant l'emplacement exacte du chiffre avant un drag
	private var posXChiffre:Number;
	private var posYChiffre:Number;
	//nombre de fois où le joueur a trouvé une réponse juste
	private var score:Number;
	//objet chargeant les sons
	private var mySound:Sound;
	
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * s'occupe de l'initialisation graphique et l'attribution des evenements
	 */
	public function Game2_06()	{
		
		super();
		p_sSoundPath = "../../sound/level2/";
		//on met le score à 0
		this.score = 0;
		//initialisation du son 
		this.mySound = new Sound(this);
		this.mySound.onLoad = function(){
			this.start(0); //Une fois le son loadé on start le son
		}
		
		//on cache les éléments du parchemins
		this.parchemin1_mc._visible = false;
		this.parchemin2_mc._visible = false;
		this.parchemin3_mc._visible = false;
		this.parchemin4_mc._visible = false;
		this.parchemin5_mc._visible = false;
		
		//initialisation du clic sur le parchemin final
		this.parchemin4_mc.onRelease = function() {

			//emettre le son du chopage de parchemin
			this._parent.mySound.loadSound("../../../data/sound/level2/shark2.mp3"); //On loade le sound
			
			this._parent.parchemin5_mc._visible = true;
			this._parent.parchemin4_mc._visible = false;
			this._parent.gameFinished();
		}
		
		this.depart_btn.button_txt.text = "DEPART";
		this.depart_btn.onRelease = function() {
			
			//faire entendre le son du click
			//this.mySound.loadSound("../../../data/sound/level2/minitic.mp3"); //On loade le sound
			_parent.soundPlayer.playASound("minitic");
			this._parent.nouvelleDonne();
			this._parent.activationEvenementChiffres();
			this._parent.changeBouton();
		}

	}
	
	public function nouvelleDonne() {
		
		//on initialise ou réinitialise les tableaux de resultat
		this.tabResultatNombres = new Array();
		this.tabResultatChiffres = new Array();
		this.tabResultatClip = new Array();
		this.tabChiffresEnAttente = new Array();
		//on intialise l'emplacement des chiffres sur le tableau tabResultatChiffres
		this.chiffre1_mc.placeTabResultatChiffres = null;
		this.chiffre1_mc.placeTabChiffresEnAttente = 0;
		this.tabChiffresEnAttente[0] = this.chiffre1_mc;
		this.chiffre2_mc.placeTabResultatChiffres = null;
		this.chiffre2_mc.placeTabChiffresEnAttente = 1;
		this.tabChiffresEnAttente[1] = this.chiffre2_mc;
		this.chiffre3_mc.placeTabResultatChiffres = null;
		this.chiffre3_mc.placeTabChiffresEnAttente = 2;
		this.tabChiffresEnAttente[2] = this.chiffre3_mc;
		this.chiffre4_mc.placeTabResultatChiffres = null;
		this.chiffre4_mc.placeTabChiffresEnAttente = 3;
		this.tabChiffresEnAttente[3] = this.chiffre4_mc;
		
		
		//on replace les champs texte déplaçables à leur place initiale
		this.chiffre1_mc._x = 285;
		this.chiffre1_mc._y = 302;
		this.chiffre2_mc._x = 372;
		this.chiffre2_mc._y = 302;
		this.chiffre3_mc._x = 446;
		this.chiffre3_mc._y = 302;
		this.chiffre4_mc._x = 521;
		this.chiffre4_mc._y = 302;
		
		//on réaffiche tous les points noirs
		for (var i = 1;i<15;i++) {
		
			this["ptChiffreUp"+i+"_mc"]._visible = true;
		}
		
		//créer la suite de nombre dans l'ordre
		var nombreMini:Number = 10;
		var nombreMaxi:Number = 39;
		var j = 0;
		for(var i=0;i<7;i++) {
			
			var randomNum:Number = Math.floor(Math.random() * (nombreMaxi - nombreMini + 1)) + nombreMini;
			this.tabResultatNombres[i] = randomNum;
			
			nombreMini = randomNum+1;
			nombreMaxi += 10;
			
			//on remplit le tableau de chiffre (nombres décomposés) en même temps
			if(randomNum < 10) {
				// On a un seul chiffre 0 -> 9, le premier est 0.
				this.tabResultatChiffres[j] = 0;
				j++;
				var chiffre = this.tabResultatNombres[i];
				this.tabResultatChiffres[j] = chiffre;
				j++;
			} else {
				// On découpe le chiffre en deux
				var tabNombreCourant = this.tabResultatNombres[i].toString().split("");
				this.tabResultatChiffres[j] = tabNombreCourant[0];
				j++;
				this.tabResultatChiffres[j] = tabNombreCourant[1];
				j++;
			}
		}
		
		//ensuite on retire 4 chiffre au hasard parmis les 14 (attention, il doit toujours rester un chiffre d'un 
		//nombre)
			//ici on ne selectionne qu'un seul des chiffres des 7 nombres au hasard (soit celui de gauche, soit 
			//celui de droite. On fera ensuite la selection des 4 chiffres a retirer parmis ces chiffres là
		var monTab:Array = new Array();
		var test = 1;
		for (var i = 0 ; i < 14 ; i += 2) {
			//nombreMini = i;
			//nombreMaxi = i+1;
			nombreMini = i;
			nombreMaxi = i+1;
			//var randomNum:Number = Math.floor(Math.random() * (nombreMaxi - nombreMini + 1)) + nombreMini;
			if (test == 1) {
				var randomNum:Number = nombreMini;
				test = 2;	
			} else {
				var randomNum:Number = nombreMaxi;
				test = 1;
			}
			
			monTab.push(randomNum);
		}
   		_parent.soundPlayer.playASound("pas3");
		trace("1 "+monTab);
				 
		//selection des 4 chiffres au hasard
		monTab = OutilsTableaux.shuffle(monTab);
		trace("2 "+monTab);
		for (var i = 1 ; i < 5 ; i++) {			
			var index = monTab.pop();
			trace("vide "+index);
			this["chiffre"+i+"_mc"].chiffre_txt.text = this.tabResultatChiffres[index];
			this.tabResultatChiffres[index] = null;
			//on rend le point noir du dessus invisible pour bien marquer les chiffres manquants
			this["ptChiffreUp"+(index+1)+"_mc"]._visible = false;
		}
		trace("3 "+monTab);
		
		//puis on affiche les autres à leur place en haut
		for (var i = 1 ; i < 15 ; i++) {			
			if(this.tabResultatChiffres[i-1] != null) {				
				this["upChiffre"+i+"_mc"].chiffre_txt.text = this.tabResultatChiffres[i-1];
			} else {
				this["upChiffre"+i+"_mc"].chiffre_txt.text = "";
			}
		} 
		trace(this.tabResultatNombres)
		trace(this.tabResultatChiffres)
		
	}
	
	
	public function activationEvenementChiffres() {
		
		//affichage et placement des 4 chiffres
		for (var i = 1;i<5;i++) {

			this["chiffre"+i+"_mc"].onPress = function() {
				//mémorisation de l'emplacement du chiffre dans le cas où on devrait le faire revenir à sa position 
				//initiale au relachement
				this._parent.posXChiffre = this._x;
				this._parent.posYChiffre = this._y;
				//mémorisation de la profondeur
				Game2_06.lastDepth = this.getDepth();
				this.swapDepths(10000);
				startDrag(this);
			}
			this["chiffre"+i+"_mc"].onRelease = function() {
				this._parent.releaseTexteBoule(this);
			}
		
		}
		


	}

	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
		
		
		
	/**
	 * A faire
	 * @return rien
	 */
	 private function tabNombreToChiffre(tabNombre:Array) {
	 	
		
	 }
	 
	 /**
	 *  
	 * @return rien
	 */
	 private function tabChiffreToNombre(tabChiffre:Array) {
	 
	 	var tabNombre:Array = new Array();
		var j = 0;
		for (var i = 0;i<14;i+=2) {
			
			tabNombre[j] = parseInt(""+tabChiffre[i]+tabChiffre[i+1]);
			j++;
		}
		
		return(tabNombre);
	 
	 }
	 
	 /**
	 * Fonction qui gère l'effet dû au relachement d'un clip de chiffre.
	 * 1) le chiffre est relaché dans un emplacement valide : on ajuste correctement sa position
	 * 2) le chiffre est relaché à la place d'un autre chiffre : on echange les 2 chiffres
	 * 3) le chiffre est relaché n'importe où : on le replace à son emplacement initial
	 * @param clipTexte le clip du chiffre qui est relaché
	 * @return rien
	 */
	 private function releaseTexteBoule(clipTexte:MovieClip) {
							
		stopDrag();
		
		if(clipTexte.aim_mc.hitTest(this.upChiffre1_mc) && this.ptChiffreUp1_mc._visible == false)
		{
			this.appliqueComportementRelease(clipTexte, true, 0, 2, 8);		
		}
		else if(clipTexte.aim_mc.hitTest(this.upChiffre2_mc)  && this.ptChiffreUp2_mc._visible == false)
		{
			this.appliqueComportementRelease(clipTexte, true, 1, -3, 8);			
		}
		else if(clipTexte.aim_mc.hitTest(this.upChiffre3_mc)  && this.ptChiffreUp3_mc._visible == false)
		{
			this.appliqueComportementRelease(clipTexte, true, 2, 2, 8);
		}
		else if(clipTexte.aim_mc.hitTest(this.upChiffre4_mc)  && this.ptChiffreUp4_mc._visible == false)
		{
			this.appliqueComportementRelease(clipTexte, true, 3, -3, 8);
		}
		else if(clipTexte.aim_mc.hitTest(this.upChiffre5_mc)  && this.ptChiffreUp5_mc._visible == false)
		{
			this.appliqueComportementRelease(clipTexte, true, 4, 2, 8);
		}
		else if(clipTexte.aim_mc.hitTest(this.upChiffre6_mc)  && this.ptChiffreUp6_mc._visible == false)
		{
			this.appliqueComportementRelease(clipTexte, true, 5, -3, 8);
		}
		else if(clipTexte.aim_mc.hitTest(this.upChiffre7_mc)  && this.ptChiffreUp7_mc._visible == false)
		{
			this.appliqueComportementRelease(clipTexte, true, 6, 2, 8);
		}
		else if(clipTexte.aim_mc.hitTest(this.upChiffre8_mc)  && this.ptChiffreUp8_mc._visible == false)
		{
			this.appliqueComportementRelease(clipTexte, true, 7, -3, 8);
		}
		else if(clipTexte.aim_mc.hitTest(this.upChiffre9_mc)  && this.ptChiffreUp9_mc._visible == false)
		{
			this.appliqueComportementRelease(clipTexte, true, 8, 2, 8);
		}
		else if(clipTexte.aim_mc.hitTest(this.upChiffre10_mc)  && this.ptChiffreUp10_mc._visible == false)
		{
			this.appliqueComportementRelease(clipTexte, true, 9, -3, 8);
		}
		else if(clipTexte.aim_mc.hitTest(this.upChiffre11_mc)  && this.ptChiffreUp11_mc._visible == false)
		{
			this.appliqueComportementRelease(clipTexte, true, 10, 2, 8);
		}
		else if(clipTexte.aim_mc.hitTest(this.upChiffre12_mc)  && this.ptChiffreUp12_mc._visible == false)
		{
			this.appliqueComportementRelease(clipTexte, true, 11, -3, 8);
		}
		else if(clipTexte.aim_mc.hitTest(this.upChiffre13_mc)  && this.ptChiffreUp13_mc._visible == false)
		{
			this.appliqueComportementRelease(clipTexte, true, 12, 2, 8);
		}
		else if(clipTexte.aim_mc.hitTest(this.upChiffre14_mc)  && this.ptChiffreUp14_mc._visible == false)
		{
			this.appliqueComportementRelease(clipTexte, true, 13, -3, 8);
		}
		//sinon si le chiffre recouvre un champs de texte du bas vide on le replace sur ce champ
		else if(clipTexte.aim_mc.hitTest(this.zoneChiffreDown1_mc))
		{
			trace("new appliq")
			this.appliqueComportementRelease(clipTexte, false, 0, 10, 12);
			
			/*
			clipTexte._x = this.zoneChiffreDown1_mc._x + 10;
			clipTexte._y = this.zoneChiffreDown1_mc._y + 12;
			
			//on enlève le chiffre du tableau des resultats
			this.tabResultatChiffres[clipTexte.placeTabResultatChiffres] = null;
			clipTexte.placeTabResultatChiffres = null;
			*/
		}
		else if(clipTexte.aim_mc.hitTest(this.zoneChiffreDown2_mc))
		{
			this.appliqueComportementRelease(clipTexte, false, 1, 10, 12);
		}
		else if(clipTexte.aim_mc.hitTest(this.zoneChiffreDown3_mc))
		{
			this.appliqueComportementRelease(clipTexte, false, 2, 10, 12);
		}
		else if(clipTexte.aim_mc.hitTest(this.zoneChiffreDown4_mc))
		{
			this.appliqueComportementRelease(clipTexte, false, 3, 10, 12);
		}
		
						//sinon, si le chiffre recouvre un autre chiffre on echange la place des 2 chiffres
		//a REFAIRE !!!!!!!!!!!!!!!! => ça c'est les switch
		
		/*
		else if(clipTexte.aim_mc.hitTest(clipTexte._parent.texteBoule2_mc))
		{
			trace("echange a faire 2!!");
		}
		else if(clipTexte.aim_mc.hitTest(clipTexte._parent.texteBoule3_mc))
		{
			trace("echange a faire 3!!");
		}
		else if(clipTexte.aim_mc.hitTest(clipTexte._parent.texteBoule4_mc))
		{
			trace("echange a faire 4!!");
		}*/
		
		//le cas où le chiffre est déposé n'importe où, il doit revenir à son ancienne place
		else {
			//on remet notre chiffre où il était avant le drag
			clipTexte._x = this.posXChiffre;
			clipTexte._y = this.posYChiffre;
			trace("retour du clip a sa place à faire");
		}
		
		//le son du placement du chiffre
		soundPlayer.playASound("minitic");
		
		//puis on mémorise le nouvel emplacement du chiffre
		this.posXChiffre = clipTexte._x;
		this.posYChiffre = clipTexte._y;
		
		//et on le remet à la bonne profondeur
		clipTexte.swapDepths(Game2_06.lastDepth);

	 }
	 
	 private function appliqueComportementRelease(clipTexte:MovieClip, isUp:Boolean, numClip:Number, majX:Number, majY:Number)
	 {
	
		if(isUp)
		{
			trace("---------------------------------------")
		 	trace("this.tabResultatClip[numClip] => "+this.tabResultatClip[numClip])
			//changement de position
			if(this.tabResultatClip[numClip] != undefined)
			{
				trace("clipTexte._x => "+clipTexte._x+" this.tabResultatClip[numClip]._x => "+this.tabResultatClip[numClip]._x)
				//si il y a deja un chiffre a cette place on les echange tous les 2
				clipTexte._x = this.tabResultatClip[numClip]._x
				clipTexte._y = this.tabResultatClip[numClip]._y
				
				trace("this.tabResultatClip[numClip]._x => "+this.tabResultatClip[numClip]._x+" this.posXChiffre => "+this.posXChiffre)
				this.tabResultatClip[numClip]._x = this.posXChiffre;
				this.tabResultatClip[numClip]._y = this.posYChiffre;
	
			}
			else
			{
				trace(' this["upChiffre"+numClip+"_mc"]._x => '+this["upChiffre"+numClip+"_mc"]._x)
				trace('clipTexte._x => '+clipTexte._x+' this["upChiffre"+(numClip+1)+"_mc"]._x => '+this["upChiffre"+(numClip+1)+"_mc"]._x)
				
				clipTexte._x = this["upChiffre"+(numClip+1)+"_mc"]._x + majX;
				clipTexte._y = this["upChiffre"+(numClip+1)+"_mc"]._y + majY;
			}
			trace("---------------------------------------")
			//changement dans les tableaux de résultats
			var pos = OutilsTableaux.getPosition(this.tabChiffresEnAttente, clipTexte);
			if(pos != undefined)
			{
				if(this.tabResultatClip[numClip] != undefined)
				{
					trace("this.tabResultatClip[numClip] => "+this.tabResultatClip[numClip]+" clipTexte.placeTabChiffresEnAttente => "+clipTexte.placeTabChiffresEnAttente)
					this.tabChiffresEnAttente[pos] = this.tabResultatClip[numClip];
				}
				else
				{
					this.tabChiffresEnAttente[pos] = undefined;
				}
			}
			var pos = OutilsTableaux.getPosition(this.tabResultatClip, clipTexte);
			if(pos != undefined)
			{
				trace("pos  => "+pos)
				trace("this.tabResultatClip[numClip].chiffre_txt.text => "+this.tabResultatClip[numClip].chiffre_txt.text)
				trace("this.tabResultatChiffres[pos] => "+this.tabResultatChiffres[pos])
				this.tabResultatChiffres[pos] = this.tabResultatClip[numClip].chiffre_txt.text;
				this.tabResultatClip[pos] = this.tabResultatClip[numClip];
			}
			trace("---------------------------------------")
			//echanger leur place dans this.tabClipBouleDepart et/ou this.tabResultatClip
			
			this.tabResultatClip[numClip] = clipTexte;
			this.tabResultatChiffres[numClip] = clipTexte.chiffre_txt.text;
		 }
		 else
		 {
			 trace("down")
			 
			 //changement de position
			if(this.tabChiffresEnAttente[numClip] != undefined)
			{
				//si il y a deja un chiffre a cette place on les echange tous les 2
				clipTexte._x = this.tabChiffresEnAttente[numClip]._x
				clipTexte._y = this.tabChiffresEnAttente[numClip]._y
				
				this.tabChiffresEnAttente[numClip]._x = this.posXChiffre;
				this.tabChiffresEnAttente[numClip]._y = this.posYChiffre;
	
			}
			else
			{
				clipTexte._x = this["zoneChiffreDown"+(numClip+1)+"_mc"]._x + majX;
				clipTexte._y = this["zoneChiffreDown"+(numClip+1)+"_mc"]._y + majY;
			}
			//changement dans les tableaux de résultats
			var pos = OutilsTableaux.getPosition(this.tabResultatClip, clipTexte);
			if(pos != undefined)
			{
				if(this.tabChiffresEnAttente[numClip] != undefined)
				{
					this.tabResultatClip[pos] = this.tabChiffresEnAttente[numClip];
				}
				else
				{
					this.tabResultatClip[pos] = undefined;
				}
			}
			//si le chiffre etait un chiffre en attente
			var pos = OutilsTableaux.getPosition(this.tabChiffresEnAttente, clipTexte);
			if(pos != undefined)
			{
				this.tabChiffresEnAttente[pos] = this.tabChiffresEnAttente[numClip];
			}
			//echanger leur place dans this.tabClipBouleDepart et/ou this.tabResultatClip
			this.tabChiffresEnAttente[numClip] = clipTexte;
			
		 }
		 
		 trace("************************************")
		 trace("tabResultatChiffres => "+this.tabResultatChiffres)
		 trace("tabChiffresEnAttente => "+this.tabChiffresEnAttente)
		 trace("************************************")
		 
	 }
	 
	 /**
	 * Fonction qui change le bouton et lui attribut un nouvel ecouteur
	 * @return rien
	 */
	 private function changeBouton() {
		
		this.depart_btn.button_txt.text = "J'ai fini";
		this.depart_btn.onRelease = function() {
			
			//faire entendre le son du click
			
			_parent.soundPlayer.playASound("minitic");
			//lance la vérification du résultat du joueur
			this._parent.verificationResultat();
		}
	 }
	 
	 
	 /**
	 * Fonction qui vérifie les résultats du joueur, si ils sont mauvais le joueur continue a chercher sinon
	 * on génère une nouvelle donne
	 * @return rien
	 */
	 private function verificationResultat() {
		
		//on verifie en 1er que tous les chiffres ont été placés
		if(this.tabChiffresEnAttente[0] != null || this.tabChiffresEnAttente[1] != null || this.tabChiffresEnAttente[2] != null
			|| this.tabChiffresEnAttente[3] != null) {
			
			trace("repInc => "+this.tabResultatChiffres)
			var reponseIncomplete:Boolean = true;
		}
		else
		{		
		
			var tabNombre:Array = this.tabChiffreToNombre(this.tabResultatChiffres);
			var reponseJuste:Boolean = true;
			var nombreEnDouble:Boolean = false;
			
			for (var i = 0;i<6;i++) {			
				
				//on verifie ensuite que le nombre en i+1 n'est pas répété. Si c'est le cas 2 nombres sont en 
				//double et c'est interdit !
				for(var j = 0;j<tabNombre.length;j++) {
					
					if(j != (i+1) && tabNombre[j] == tabNombre[i+1] )
					{
						nombreEnDouble = true;
						break;
					}
				}
				
				//vérifie que tous les nombres sont dans un ordre croissant, si c'est pas le cas, la réponse est fausse
				trace("tabNombre[i] => "+tabNombre[i]+" tabNombre[i+1] => "+tabNombre[i+1])
				if(tabNombre[i] > tabNombre[i+1])
				{
					reponseJuste = false;
					break;
				}
			}
		}
		
		if(!reponseIncomplete)
		{
			if(!nombreEnDouble)
			{
				if (reponseJuste) {
					
					this.score++;
					switch(this.score) {
						
						case 1:
							this.parchemin1_mc._visible = true;
							this.nouvelleDonne();
							break;
						
						case 2:
						
							this.parchemin2_mc._visible = true;
							this.nouvelleDonne();
							break;
						
						case 3:
						
							this.parchemin3_mc._visible = true;
							this.nouvelleDonne();
							break;
						
						case 4:
						
							this.parchemin4_mc._visible = true;
							//desactiver le bouton "J'ai fini" et les champs de texte deplacable
							this.depart_btn.enabled = false;
							//retirer tous les chiffres
							this.chiffre1_mc._visible = false;
							this.chiffre2_mc._visible = false;
							this.chiffre3_mc._visible = false;
							this.chiffre4_mc._visible = false;
							this.upChiffre1_mc._visible = false;
							this.upChiffre2_mc._visible = false;
							this.upChiffre3_mc._visible = false;
							this.upChiffre4_mc._visible = false;
							this.upChiffre5_mc._visible = false;
							this.upChiffre6_mc._visible = false;
							this.upChiffre7_mc._visible = false;
							this.upChiffre8_mc._visible = false;
							this.upChiffre9_mc._visible = false;
							this.upChiffre10_mc._visible = false;
							this.upChiffre11_mc._visible = false;
							this.upChiffre12_mc._visible = false;
							this.upChiffre13_mc._visible = false;
							this.upChiffre14_mc._visible = false;
							//son quand on retire tous les chiffres
							
							soundPlayer.playASound("shark2");
		
							break;
					}
					
					
					soundPlayer.playASound("minitic");
				}	
				else
				{
					//faire entendre le son !!
					soundPlayer.playASound("shClac2");
		
				}
			}
			else
			{
				//sinon afficher un popup qui dit pas le droit à des nombres en double !
				Alert.show("Tu ne peux pas avoir deux fois le même nombre !!", "Attention !");
				soundPlayer.playASound("shClac2");
			}
			
		}
		else
		{
			//sinon on fait entendre la voix de la petite fille disant que ce n'est pas terminé
			soundPlayer.playASound("JeuPasTerminer");
		}
		
	 }
	 
	 
	 /**
	 * Fonction qui cloture la partie (enregistrement des scores etc..
	 * @return rien
	 */
	 private function gameFinished() {
		
		//on enregistre la réussite du jeu 
		this._parent._parent.setWinGame(6, this._parent._parent.joueur1); 
			
		_parent._parent.barreInterface_mc.showParchemin();
		super.gameFinished();
		
		//afficher le parchemin dans la glissière de navigation (peut être fait lors de l'enregistrement du 
		//résultat
		//A FAIRE !!!
	 }
	 
	 

	 
	
}

﻿/**
 * class maths1p4p.level2.game2_07.game2_07
 * 
 * @author Rémi Gay
 * @version 1.0
 *
 * Copyright (c) 2008 Rémi Gay.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level2.Level2Player;
//import maths1p4p.level2.game2_07.*
import maths1p4p.utils.OutilsTableaux;
import maths1p4p.application.Application;

class maths1p4p.level2.game2_07.Game2_07 extends maths1p4p.AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 7;
	

	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/***** variables en rapport avec les éléments déjà present sur la scène *****/
	private var depart_btn:MovieClip;
	
	private var nomJoueur1_txt:TextField;
	private var nomJoueur2_txt:TextField;
	
	private var joueur1:maths1p4p.level2.Level2Player;
	private var joueur2:maths1p4p.level2.Level2Player;
	
	private var arbreJ1_1_mc:MovieClip;
	private var arbreJ1_2_mc:MovieClip;
	private var arbreJ1_3_mc:MovieClip;
	private var arbreJ1_4_mc:MovieClip;
	private var arbreJ2_1_mc:MovieClip;
	private var arbreJ2_2_mc:MovieClip;
	private var arbreJ2_3_mc:MovieClip;
	private var arbreJ2_4_mc:MovieClip;
	
	private var rondin0_mc:MovieClip;
	private var rondin1_mc:MovieClip;
	private var rondin2_mc:MovieClip;
	private var rondin3_mc:MovieClip;
	private var rondin4_mc:MovieClip;
	private var rondin5_mc:MovieClip;
	private var rondin6_mc:MovieClip;
	private var rondin7_mc:MovieClip;
	private var rondin8_mc:MovieClip;
	private var rondin9_mc:MovieClip;
	private var rondin10_mc:MovieClip;
	private var rondin11_mc:MovieClip;
	private var rondin12_mc:MovieClip;
	private var rondin13_mc:MovieClip;
	private var rondin14_mc:MovieClip;
	private var rondin15_mc:MovieClip;
	private var rondin16_mc:MovieClip;
	private var rondin17_mc:MovieClip;
	private var rondin18_mc:MovieClip;
	private var rondin19_mc:MovieClip;
	private var rondin20_mc:MovieClip;
	
	private var parcheminJ1_mc:MovieClip;
	private var parcheminJ2_mc:MovieClip;
	
	
	/***** variables privées de la classe *****/
	//a qui c'est le tour
	private var joueurCourant:Level2Player;
	//tableau contenant les rondins parents de chaque rondins (du rondin 1 en position 0 au rondin 21 en position 20
	private var tabRondins:Array;
	//tableau contenant les rondins fils de chaque rondins (du rondin 1 en position 0 au rondin 21 en position 20
	private var tabRondinsChildren:Array;
	
	private var counterSetInterval:Number;

	
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * s'occupe de l'initialisation graphique et l'attribution des evenements
	 */
	public function Game2_07()	{
		
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		 
		this.joueur1 = this._parent._parent.joueur1;
		trace ('joueur 1 : '+this.joueur1);
		trace ('joueur 1 : '+this._parent.joueur1);
		trace ('joueur 1 : '+this._parent._parent.joueur1);
		this.nomJoueur1_txt.text = this._parent._parent.joueur1.getName();
		this.joueur1.setScore(0);
		
		this.joueur2 = this._parent._parent.joueur2;
		trace ('joueur 2 : '+this.joueur2);
		trace ('joueur 2 : '+this._parent.joueur2);
		trace ('joueur 2 : '+this._parent._parent.joueur2);
		this.nomJoueur2_txt.text = this._parent._parent.joueur2.getName();
		this.joueur2.setScore(0);
		
		//le premier joueur a jouer est le joueur 1 (donc on grise le nom du joueur 2
		this.joueurCourant = this.joueur2;
		this.nomJoueur2_txt.textColor = 0x999999;
		
		//on cache les éléments du parchemins
		this.arbreJ1_1_mc._visible = false;
		this.arbreJ1_2_mc._visible = false;
		this.arbreJ1_3_mc._visible = false;
		this.arbreJ1_4_mc._visible = false;
		this.parcheminJ1_mc._visible = false;
		
		this.arbreJ2_1_mc._visible = false;
		this.arbreJ2_2_mc._visible = false;
		this.arbreJ2_3_mc._visible = false;
		this.arbreJ2_4_mc._visible = false;
		this.parcheminJ2_mc._visible = false;
		
		
		
		this.initialiseParchemin();
		
		//crée la double liste des 21 rondins
		this.initialiseRondin();
		
		this.nouvelleDonne();
		
		this.initialiseBoutonDepart();
	}
	
	public function nouvelleDonne() {

		//on remet à 0 le nombre de coup joués des 2 joueurs
		this.joueur1.setNbreCoupJoues(0);
		this.joueur2.setNbreCoupJoues(0);
		//et on change de joueur
		this.changeJoueurCourant();
		//d'abord on affiche le bon nombre de rondins
		this.initialiseVisibiliteRondin()
		//puis on leur attribut un effet lors de leur click
		this.initialiseClicksRondin();	
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
		
		
	/**
	*
	*/
	private function changeJoueurCourant() {
		
		if(this.joueurCourant == this.joueur1) {
			this.joueurCourant = this.joueur2;
			this.nomJoueur2_txt.textColor = 0x000000;
			this.nomJoueur1_txt.textColor = 0x999999;
		} else {
			this.joueurCourant = this.joueur1;
			this.nomJoueur1_txt.textColor = 0x000000;
			this.nomJoueur2_txt.textColor = 0x999999;
		}
	}
		
	/**
	 * Fonction qui initialise le click sur le (ou les) parchemin
	 * @return rien
	 */
	 private function initialiseParchemin() {
		
		//initialisation du clic sur le parchemin final
		this.parcheminJ1_mc.onRelease = function() {

			//emettre le son du chopage de parchemin
			_parent.soundPlayer.playASound("shark2");
			
			this._parent.arbreJ1_4_mc._visible = true;
			this._parent.gameFinished();
		}
		//initialisation du clic sur le parchemin final
		this.parcheminJ2_mc.onRelease = function() {

			//emettre le son du chopage de parchemin
			_parent.soundPlayer.playASound("shark2");
			this._parent.arbreJ2_4_mc._visible = true;
			this._parent.gameFinished();
		}
	 }
	 
	 /**
	 * Fonction qui initialise le click sur le (ou les) parchemin
	 * @return rien
	 */
	 private function initialiseRondin() { 
		
		this.rondin0_mc.num = 0;
		this.rondin1_mc.num = 1;
		this.rondin2_mc.num = 2;
		this.rondin3_mc.num = 3;
		this.rondin4_mc.num = 4;
		this.rondin5_mc.num = 5;
		this.rondin6_mc.num = 6;
		this.rondin7_mc.num = 7;
		this.rondin8_mc.num = 8;
		this.rondin9_mc.num = 9;
		this.rondin10_mc.num = 10;
		this.rondin11_mc.num = 11;
		this.rondin12_mc.num = 12;
		this.rondin13_mc.num = 13;
		this.rondin14_mc.num = 14;
		this.rondin15_mc.num = 15;
		this.rondin16_mc.num = 16;
		this.rondin17_mc.num = 17;
		this.rondin18_mc.num = 18;
		this.rondin19_mc.num = 19;
		this.rondin20_mc.num = 20;

		
		this.tabRondins = new Array();
		this.tabRondins = [[], [0], [0], [1], [1,2], [2], [3], [3,4], [4,5], [5], [6], [6,7], [7,8], [8,9], [9], [10], [10,11], [11,12], [12,13], [13,14], [14]];
		
		this.tabRondinsChildren = new Array();
		this.tabRondinsChildren = [[1,2], [3,4], [4,5], [6,7], [7,8], [8,9], [10,11], [11,12], [12,13], [13,14], [15,16], [16,17], [17,18], [18,19], [19,20], [], [], [], [], [], []];

		
	 }
	 
	 
	 /**
	 * Fonction qui initialise le nombre de rondins et les affiche à l'ecran
	 * @return rien
	 */
	 private function initialiseVisibiliteRondin() { 
		 //choisir un nombre entre 15 et 21 pour le nombre de rondins initial
		var nbreRondinsDebut:Number = Math.floor(Math.random() * (20 - 14 + 1)) + 14;
		trace ("Nombre rondin : "+nbreRondinsDebut);
		if (nbreRondinsDebut < 14) {
			nbreRondinsDebut = 15;	
		}
		//on affiche les rondins correspondant
		for (var i = 20 ; i > (20 - nbreRondinsDebut) ; i--) {
			
			this["rondin"+i+"_mc"]._visible = true;
			//par defaut on ne peut pas clicker sur les rondins visible
			this["rondin"+i+"_mc"].enabled = false;
		}
		for (var i = 20 - nbreRondinsDebut ; i > -1 ; i--) {
			
			this["rondin"+i+"_mc"]._visible = false;
		}
	 }
	 
	 
	 
	 /**
	 * Fonction qui initialise le comportement d'un click sur un rondin
	 * @return rien
	 */
	 private function initialiseClicksRondin() {
		
		//on definit en 1er les rondins innaccessible (ceux qui ne sont pas surmontés par un autre rondin)
		for (var i = 0 ; i < 20 ; i++) {
			
			//trace(this.tabRondins[i][0]+" "+this["rondin"+this.tabRondins[i][0]+"_mc"]._visible)
			
			if(this["rondin"+this.tabRondins[i][0]+"_mc"]._visible == true || this["rondin"+this.tabRondins[i][1]+"_mc"]._visible == true)
			{
				this["rondin"+i+"_mc"].enabled = false;
				//trace("prout")
			}
			else
			{
				this["rondin"+i+"_mc"].enabled = true;
			}
			
			//trace(i+"=>> "+this["rondin"+i+"_mc"].enabled)
		}
		
		
		//pour chaque rondin on definit le onRelease
		for(var i=0;i<this.tabRondins.length;i++) {
			
			//a chaque click sur un rondin on desenablelifie les bons rondins en + de faire les changement dans 
			//tabRondins et de compter le nbre de rondins clickés par le joueur ce tour-ci bien sûr
			trace(i+" => "+this["rondin"+i+"_mc"].enabled)
			this["rondin"+i+"_mc"].onRelease = function() {
				
				//son du click sur le rondin
				_parent.soundPlayer.playASound("zip");
				//d'abord vérifier que le joueur n'a pas déjà clické sur 3 rondins
				if(this._parent.joueurCourant.getNbreCoupJoues() >= 3) {
					//on passe à l'autre joueur en produisant le son strident, en faisant clignoter le bouton suite
					//et en remettant le compteur du joueur qui vient de jouer a 0
					this._parent.joueurCourant.setNbreCoupJoues(0);
					//emettre le son strident 
					_parent.soundPlayer.playASound("clang1"); 
					this._parent.changeJoueurCourant();
					
				} else {
					//sinon on rend invisible le rondin, on incremente le nbre de coups joués par le joueur, 
					//on rend accessible les rondins qu'il faut
					this._visible = false;
					this._parent.joueurCourant.addNbreCoupJoues();

					/*trace("this.num )> "+this.num)
					trace(this._parent.tabRondinsChildren[this.num][0])
					trace("parent de l'enfant gauche => "+this._parent.tabRondins[this._parent.tabRondinsChildren[this.num][0]][0]+" - "+this._parent.tabRondins[this._parent.tabRondinsChildren[this.num][0]][1])
										
					trace(this._parent["rondin"+this._parent.tabRondinsChildren[this.num][0]+"_mc"])
					trace(this._parent["rondin"+this._parent.tabRondinsChildren[this.num][1]+"_mc"])
					
					trace(  this._parent["rondin"+this._parent.tabRondins[this._parent.tabRondinsChildren[this.num][0]][0]+"_mc"] )
					trace(  this._parent["rondin"+this._parent.tabRondins[this._parent.tabRondinsChildren[this.num][0]][1]+"_mc"] )
										
					trace(  this._parent["rondin"+this._parent.tabRondins[this._parent.tabRondinsChildren[this.num][1]][1]+"_mc"] )
					trace(  this._parent["rondin"+this._parent.tabRondins[this._parent.tabRondinsChildren[this.num][1]][0]+"_mc"] )
					*/
					//rendre accessible les rondins en dessous de celui kon a enlevé
					//si le parent gauche et droit du rondin fils n'existent pas ou sont invisible alors on rend le rondin accessible
					if(this._parent["rondin"+this._parent.tabRondins[this._parent.tabRondinsChildren[this.num][0]][0]+"_mc"]._visible != true && this._parent["rondin"+this._parent.tabRondins[this._parent.tabRondinsChildren[this.num][0]][1]+"_mc"]._visible != true) {
						
						this._parent["rondin"+this._parent.tabRondinsChildren[this.num][0]+"_mc"].enabled = true;
					}
					if(this._parent["rondin"+this._parent.tabRondins[this._parent.tabRondinsChildren[this.num][1]][0]+"_mc"]._visible != true && this._parent["rondin"+this._parent.tabRondins[this._parent.tabRondinsChildren[this.num][1]][1]+"_mc"]._visible != true) {
						
						this._parent["rondin"+this._parent.tabRondinsChildren[this.num][1]+"_mc"].enabled = true;
					}
					
					//on vérifie que ce n'était pas le dernier rondin
					if(this.num > 14 && this.num < 21) {
						var mancheFinie = true;
						trace ('Clic sur rondin du bas, verif fin');
						for (var i = 15 ; i < 21 ; i++) {
							trace ('Rondin '+i+' :'+this._parent["rondin"+i+"_mc"]._visible);
						
							if (this._parent["rondin"+i+"_mc"]._visible == true) {
								mancheFinie = false;
								break;
							}
						}
					
						//si la manche est terminé on marque les score
						if(mancheFinie)
						{
							trace ("termine : "+this._parent.joueurCourant.getScore());
							this._parent.joueurCourant.addScore();

							switch(this._parent.joueurCourant.getScore())
							{
								case 1:
trace ("case 1");
									if(this._parent.joueurCourant == this._parent.joueur1)
									{
										this._parent["arbreJ1_1_mc"]._visible = true;
									}
									else
									{
										this._parent["arbreJ2_1_mc"]._visible = true;
									}
									this._parent.nouvelleDonne();
									break;
									
								case 2:
trace ("case 2");							
									if(this._parent.joueurCourant == this._parent.joueur1)
									{
										this._parent["arbreJ1_2_mc"]._visible = true;
									}
									else
									{
										this._parent["arbreJ2_2_mc"]._visible = true;
									}
									this._parent.nouvelleDonne();
									break;
									
								case 3:
trace ("case 3");						
									if(this._parent.joueurCourant == this._parent.joueur1)
									{
										this._parent["arbreJ1_3_mc"]._visible = true;
										this._parent["parcheminJ1_mc"]._visible = true;
									}
									else
									{
										this._parent["arbreJ2_3_mc"]._visible = true;
										this._parent["parcheminJ2_mc"]._visible = true;
									}
									this._parent.depart_btn.enabled = false;
									if(this._parent.joueur1 == this._parent.joueurCourant)
									{
										this._parent.parcheminJ1_mc._visible;
									}
									else
									{
										this._parent.parcheminJ2_mc._visible;
									}
									//partie terminée
									break;
								default : 
									trace ("ERREUR SURVENUE");
							}
						}
					}

					
				}
				
			}
			
			
		}
	 }
	 
	 
	 /**
	 * Fonction qui initialise le bouton "Suite"
	 * @return rien
	 */
	 private function initialiseBoutonDepart() {
		
		this.depart_btn.onPress = function()
		{
			this.gotoAndPlay(2);
		}
		this.depart_btn.onRelease = function() {
			this.gotoAndPlay(1);
			// On doit avoir joué au moins un coup 
			if (this._parent.joueurCourant.getNbreCoupJoues() > 0){
				//on remet à 0 le nbre de coup joué du joueur courant
				this._parent.joueurCourant.setNbreCoupJoues(0);
				//on change simplement de joueur
				this._parent.changeJoueurCourant();
			}
		}
	 }
		
		

	 /**
	 * Fonction qui vérifie si le joueur n'a pas gagné, si c'est le cas on lui ajoute un point puis on vérifie
	 * si il a gagné la partie ou non, sinon on change simplement de joueur en remettant le nbre de coup joué à 0
	 * @return rien
	 */
	 private function verificationResultat() {
		
		trace("a faire ..")
	 }
	 
	 
	 /**
	 * Fonction qui cloture la partie (enregistrement des scores etc..
	 * @return rien
	 */
	 private function gameFinished() {
		
		//enregistrer le résultat => mémoriser que le joueur courant a bien finit ce jeu
		if(this.joueur1 == this.joueurCourant) {
			this._parent._parent.setWinGame(7, this.joueur1); 
		} else {
			this._parent._parent.setWinGame(7, this.joueur2); 
		}
		_parent._parent.barreInterface_mc.showParchemin();
		//afficher le parchemin dans la glissière de navigation (peut être fait lors de l'enregistrement du 
		//résultat
		//A FAIRE !!!
	 }
	 
	 

	 
	
}

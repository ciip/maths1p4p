﻿/**
 * class maths1p4p.level2.game2_08.game2_08
 * 
 * @author Rémi Gay
 * @version 1.0
 *
 * ETAT : pas commencé
 *
 * TO DO : 
 * focusManager.setFocus(monBoutonOK);
 *
 * Copyright (c) 2008 Rémi Gay.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.utils.Joueur;
//import maths1p4p.level2.game2_07.*
import maths1p4p.utils.OutilsTableaux;
import maths1p4p.application.Application;

class maths1p4p.level2.game2_08.Game2_08 extends maths1p4p.AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 8;
	
	public static var occurenceGame2_08:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var myInterval:Number;
	
	/***** variables en rapport avec les éléments déjà present sur la scène *****/
	private var depart_btn:MovieClip;
	
	private var joueur1:maths1p4p.level2.Level2Player;
	private var nomJoueur1_txt:TextField;
	private var joueur2:maths1p4p.level2.Level2Player;
	private var nomJoueur2_txt:TextField;
	
	//icone indiquant le nombre de points de chaque joueur
	private var pointJ1_1:MovieClip;
	private var pointJ1_2:MovieClip;
	private var pointJ1_3:MovieClip;
	private var pointJ2_1:MovieClip;
	private var pointJ2_2:MovieClip;
	private var pointJ2_3:MovieClip;
	
	//les clips associés aux réponses (lumières bleue, jaune, orange, rouge, blanche)
	private var reponseBleue_mc:MovieClip;
	private var reponseJaune_mc:MovieClip;
	private var reponseOrange_mc:MovieClip;
	private var reponseRouge_mc:MovieClip;
	private var reponseJuste_mc:MovieClip;
	
	//texte de saisie où on entre la réponse envisagée
	private var reponseJoueur_txt:TextField;
	//dernière réponse donnée pour chacun des joueurs
	private var repJ1_txt:TextField;
	private var repJ2_txt:TextField;
	//le nombr réponse censé s'afficher par dessus la lumière blanche
	private var nombreReponse_txt:TextField;
	
	//--------
		
	private var parcheminJ1_mc:MovieClip;
	private var parcheminJ2_mc:MovieClip;
	
	private var stop_b:Boolean;
	
	/***** variables privées de la classe *****/
	//variable mémorisant quelle lumière est actuellement allumée
	private var lumiereCourante:MovieClip;
	//a qui c'est le tour
	private var joueurCourant:maths1p4p.level2.Level2Player;
 
 	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
			
	public function focus():Void{
		clearInterval(this.myInterval);
		this.reponseJoueur_txt._visible = true;
		this.reponseJoueur_txt.enabled = true;
		var texteFocus = this.reponseJoueur_txt; 
		Selection.setFocus(texteFocus);
	} 
	
	/**
	 * Constructeur
	 * s'occupe de l'initialisation graphique et l'attribution des evenements
	 */
	public function Game2_08()	{
		
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		Game2_08.occurenceGame2_08 = this;
		 
		this.joueur1 = this._parent._parent.joueur1;
		this.nomJoueur1_txt.text = this.joueur1.getName();
		this.joueur2 = this._parent._parent.joueur2;
		this.nomJoueur2_txt.text = this.joueur2.getName();
		//le premier joueur a jouer est le joueur 1 (donc on grise le nom du joueur 2
		this.joueurCourant = this.joueur2;
		this.nomJoueur2_txt.textColor = 0x999999;
		
		this.reponseJoueur_txt.maxChars = 2;
		this.reponseJoueur_txt.restrict = "0-9";
		
		//on cache les éléments du parchemins
		this.parcheminJ1_mc._visible = false;
		this.parcheminJ2_mc._visible = false;
		
		//on cache les points des joueurs (on les met à 0 koi)
		this.pointJ1_1._visible = false;
		this.pointJ1_2._visible = false;
		this.pointJ1_3._visible = false;
		this.pointJ2_1._visible = false;
		this.pointJ2_2._visible = false;
		this.pointJ2_3._visible = false;
		
		this.initialiseParchemin();
		
		this.nouvelleDonne();
		
		this.initialiseBoutonDepart();
		
		//on place le focus sur le champs texte reponse automatiquement
		//trace("Focus = "+Selection.getFocus()+", "+this.reponseJoueur_txt+", "+this.reponseJoueur_txt._visible);
		/*onEnterFrame = function () {
	    	trace(Selection.setFocus("this.reponseJoueur_txt")); 
			delete onEnterFrame;
	    }*/
	    if(this.myInterval != undefined) {
			clearInterval(this.myInterval)
		}
		this.myInterval = setInterval(this,"focus", 100);
		
		this.reponseJoueur_txt.onKillFocus = function(newFocus) {
		    if(this._parent.myInterval != undefined) {
				clearInterval(this._parent.myInterval)
			}
			this._parent.myInterval = setInterval(this._parent,"focus", 100);		
		};
		
		//On active la touche entrée (qui validera le resultat du joueur) => on le fait ici pour pas attribuer
		//un nouvel ecouteur à chaque nouvelle donne
		this.initToucheEntree();

	}
	

	public function nouvelleDonne() {
		this.stop_b = false;
		this.reponseJoueur_txt._visible = true;
	 	//on réinitialise les reponses des joueurs et la réponse centrale
		this.repJ1_txt.text = "";
		this.repJ2_txt.text = "";
		this.reponseJoueur_txt.text = "";
		
		this.reponseBleue_mc._visible = false;
		this.reponseJaune_mc._visible = false;
		this.reponseOrange_mc._visible = false;
		this.reponseRouge_mc._visible = false;
		this.reponseJuste_mc._visible = false;
		//donc aucune des lumière n'est allumée
		this.lumiereCourante = null;
		
		//génerer un nouveau nombre à trouver compris entre 1 et 50
		var monTab:Array = new Array();
		for(var i = 0 ; i < 50 ; i++) {
			monTab[i] = i+1;
		}
		monTab = OutilsTableaux.shuffle(monTab);
		//obligé de créer une variable temporaire sinon flash considère monTab.pop() comme un Objet et non 
		//un Number -_- !!!
		var flashALaCon = monTab.pop();
		this.nombreReponse_txt.text = flashALaCon;
		trace ("Solution : "+this.nombreReponse_txt.text);
		//on rend invisible le nombre resultat
		this.nombreReponse_txt._visible = false;
		
		//on change de joueur
		this.changeJoueurCourant();
		
		//on rend le bouton de depart inactif
		//this.depart_btn.enabled = false;

	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
		
		
	/**
	*
	*/
	private function changeJoueurCourant() { 
		if(this.joueurCourant == this.joueur1)
		{
			this.joueurCourant = this.joueur2;
			this.nomJoueur2_txt.textColor = 0x000000;
			this.nomJoueur1_txt.textColor = 0x999999;
		}
		else
		{
			this.joueurCourant = this.joueur1;
			this.nomJoueur1_txt.textColor = 0x000000;
			this.nomJoueur2_txt.textColor = 0x999999;
		}
	}
		
	/**
	 * Fonction qui initialise le click sur le (ou les) parchemin
	 * @return rien
	 */
	 private function initialiseParchemin() {
		
		//initialisation du clic sur le parchemin final
		this.parcheminJ1_mc.onRelease = function() {

			//emettre le son du chopage de parchemin
			_parent.soundPlayer.playASound("shark2");
			
			//au click sur le parcho on le rend invisible ainsi que les points de score
			this._visible = false;
			this.pointJ1_1._visible = false;
			this.pointJ1_2._visible = false;
			this.pointJ1_3._visible = false;
			this.pointJ2_1._visible = false;
			this.pointJ2_2._visible = false;
			this.pointJ2_3._visible = false;
			this._parent.gameFinished();
		}
		//initialisation du clic sur le parchemin final
		this.parcheminJ2_mc.onRelease = function() {

			//emettre le son du chopage de parchemin
			_parent.soundPlayer.playASound("shark2");
			
			this._visible = false;
			this.pointJ1_1._visible = false;
			this.pointJ1_2._visible = false;
			this.pointJ1_3._visible = false;
			this.pointJ2_1._visible = false;
			this.pointJ2_2._visible = false;
			this.pointJ2_3._visible = false;
			this._parent.gameFinished();
		}
	 }
	 
	 	 
	 /**
	 * Fonction qui initialise le bouton "Suite"
	 * @return rien
	 */
	 private function initialiseBoutonDepart() {
		
		this.depart_btn.onRelease = function() {
			trace ("Bouton appuiyé "+_parent.stop_b); 
			if (_parent.stop_b == true) {
				_parent.stop_b = false;
				soundPlayer.playASound("rape1");
				this._parent.nouvelleDonne();
			} else {
				//verification du résultat
				Game2_08.occurenceGame2_08.verifResultat();
			}
		}
	 }
		
	 /**
	 * Fonction qui compare le chiffre entré par le joueur au chiffre à trouver
	 * @return rien
	 */
	 private function verifResultat() {
		soundPlayer.playASound("minitic");
		trace("calcul difference")
		var difference = Math.abs(this.reponseJoueur_txt.text - this.nombreReponse_txt.text);
		if(difference > 10) {
			
			//si une autre lumière était affichée avant on l'efface
			this.lumiereCourante._visible = false
			//pour la remplacer par la lumière bleue
			this.reponseBleue_mc._visible = true;
			this.lumiereCourante = this.reponseBleue_mc;
		}
		else if(difference > 4)
		{
			//si une autre lumière était affichée avant on l'efface
			this.lumiereCourante._visible = false
			//pour la remplacer par la lumière jaune
			this.reponseJaune_mc._visible = true;
			this.lumiereCourante = this.reponseJaune_mc;
		}
		else if(difference > 1)
		{
			//si une autre lumière était affichée avant on l'efface
			this.lumiereCourante._visible = false
			//pour la remplacer par la lumière orange
			this.reponseOrange_mc._visible = true;
			this.lumiereCourante = this.reponseOrange_mc;
		}
		else if(difference > 0)
		{
			//si une autre lumière était affichée avant on l'efface
			this.lumiereCourante._visible = false
			//pour la remplacer par la lumière rouge
			this.reponseRouge_mc._visible = true;
			this.lumiereCourante = this.reponseRouge_mc;
		}
		else if(difference == 0)
		{
			soundPlayer.playASound("fefeClac");
			//reponse juste
			this.lumiereCourante._visible = false
			//pour la remplacer par la lumière rouge
			this.reponseJuste_mc._visible = true;
			this.lumiereCourante = this.reponseJuste_mc;
			//on affiche le chiffre par dessus la lumière blanche
			this.nombreReponse_txt._visible = true;
			
			this.gagneMancheCourante();
		}
		else
		{
			var aucuneDonnee:Boolean = true;
		}
		
		if(!aucuneDonnee)
		{
			//on remplit le champs de texte dynamique du joueur par la reponse
			if(this.joueurCourant == this.joueur1)
			{
				this["repJ1_txt"].text = this.reponseJoueur_txt.text;
			}
			else
			{
				this["repJ2_txt"].text = this.reponseJoueur_txt.text;
			}
			
			//on vide le txt de saisie
			this.reponseJoueur_txt.text = "";
			
			//on change de joueur
			this.changeJoueurCourant();
		}
	 }
		
  	 /**
	 * Fonction qui fait gagner une manche a un joueur et passe à la suivante OU termine la partie
	 * @return rien
	 */
	 private function gagneMancheCourante() {
		this.stop_b = true;
		this.reponseJoueur_txt._visible = false;
		trace ("DEBUG : "+this.reponseJoueur_txt._visible);
		//si pas gagné la partie :
		this.joueurCourant.addScore();
		//on allume une loupiote de score
		if(this.joueurCourant == this.joueur1)
		{
			this["pointJ1_"+this.joueurCourant.getScore()]._visible = true;
		}
		else
		{
			this["pointJ2_"+this.joueurCourant.getScore()]._visible = true;
		}
		
		if(this.joueurCourant.getScore() != 3)
		{
			//this.depart_btn.enabled = true;
		}
		else
		{
			//sinon on affiche le parchemin du gagnant
			if(this.joueurCourant == this.joueur1)
			{
				this["parcheminJ1_mc"]._visible = true;
			}
			else
			{
				this["parcheminJ2_mc"]._visible = true;
			}
		}
	 }
 
 
	 /**
	 * Fonction qui cloture la partie (enregistrement des scores etc..
	 * @return rien
	 */
	 private function gameFinished() {
		
		//enregistrer le résultat => mémoriser que le joueur courant a bien finit ce jeu
		this.changeJoueurCourant();
		
		if(this.joueurCourant == this.joueur1) {  
			this._parent._parent.setWinGame(8, this.joueur1);
		} else { 
			this._parent._parent.setWinGame(8, this.joueur2);
		}
		_parent._parent.barreInterface_mc.showParchemin();
		//afficher le parchemin dans la glissière de navigation (peut être fait lors de l'enregistrement du 
		//résultat
		//A FAIRE !!!
	 }
	 
	 
	 private function initToucheEntree() {
		 
		 var monEcouteur:Object = new Object();
		 monEcouteur.onKeyDown = function() {
			 trace ("****Appui sur touche du clavier détecté****");
			 if(Key.isDown(Key.ENTER) && Game2_08.occurenceGame2_08.stop_b == false){
				 
				 //si la manche n'est pas terminé on teste le resultat 
				 if(Game2_08.occurenceGame2_08.stop_b == false) {
					 Game2_08.occurenceGame2_08.verifResultat();
				 }
			 }
			 // voici d'autres informations à exploiter.
			 var codeTouche:Number = Key.getCode();
			 var toucheAscii:Number = Key.getAscii();
			 var nom:String = String.fromCharCode(Key.getAscii());
			 trace ("nom : "+nom +" - code : "+codeTouche +" - code Ascii : "+toucheAscii)
		 };
	 
		 
		 Key.addListener(monEcouteur);
		 
	 }
	 
	
}

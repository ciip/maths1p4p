﻿/**
 * class com.maths1p4p.Game2_09
 * 
 * @author Yoann Augen
 * @version 1.0
 *
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level2.game2_09.Game2_09 extends maths1p4p.AbstractGame {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 9;
	// Les 10 chiffres 
	public var p_mcNum1:MovieClip;
	public var p_mcNum2:MovieClip;
	public var p_mcNum3:MovieClip;
	public var p_mcNum4:MovieClip;
	public var p_mcNum5:MovieClip;
	public var p_mcNum6:MovieClip;
	public var p_mcNum7:MovieClip;
	public var p_mcNum8:MovieClip;
	public var p_mcNum9:MovieClip;
	public var p_mcNum10:MovieClip;
	
	
	// Les boites sur lequel ils sont posés
	public var p_mcBoiteBas1:MovieClip;
	public var p_mcBoiteBas2:MovieClip;
	public var p_mcBoiteBas3:MovieClip;
	public var p_mcBoiteBas4:MovieClip;
	public var p_mcBoiteBas5:MovieClip;
	public var p_mcBoiteBas6:MovieClip;
	
	// Les boites contenant les chiffres dans la pyramide
	public var p_mcBoite1:MovieClip;
	public var p_mcBoite2:MovieClip;
	public var p_mcBoite3:MovieClip;
	public var p_mcBoite4:MovieClip;
	public var p_mcBoite5:MovieClip;
	public var p_mcBoite6:MovieClip;
	public var p_mcBoite7:MovieClip;
	public var p_mcBoite8:MovieClip;
	public var p_mcBoite9:MovieClip;
	public var p_mcBoite10:MovieClip;
	
	// Les boites contenant les chiffres dragué pyramide
	public var p_mcBoiteRes1:MovieClip;
	public var p_mcBoiteRes2:MovieClip;
	public var p_mcBoiteRes3:MovieClip;
	public var p_mcBoiteRes4:MovieClip;
	public var p_mcBoiteRes5:MovieClip;
	public var p_mcBoiteRes6:MovieClip;
	public var p_mcBoiteRes7:MovieClip;
	public var p_mcBoiteRes8:MovieClip;
	public var p_mcBoiteRes9:MovieClip;
	public var p_mcBoiteRes10:MovieClip;
	
	// Le compteur de point 
	public var p_mcPoint:MovieClip;
	
	// Le bouton de fin
	public var p_mcBoutonFin:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_aListValues:Array = new Array();
	
	// Liste des valeurs possibles pour le bas de la pyramide
	private var p_aValeurInit:Array;
	
	// Pour l'ordre des chiffres du bas
	private var p_aOrderBas:Array;
	
	// Ordre des chiffres posé sur la pyramide par le joueur 
	private var p_aListResult:Array;
	
	// nombre de victoire 
	private var p_nWin:Number = 0;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game2_09() {  
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		initInterface();	
 	}
	
	/**
	* Fonction qui renvoi un nombre aleatoire entre min et max
	* @param Number min
	* @param Number max
	* @return Number
	*/
	public function randRange(min:Number, max:Number) {
	     var randomNum:Number = Math.floor(Math.random() * (max - min + 1)) + min;
	     return randomNum;
	}
	
	/**
	* Fonction qui renvoi le tableau mélangé
	* @param Array tab0_array le tableau d'origine
	* @return Array
	*/
	public function randomTab(tab0_array:Array) {
	    var tab1_array:Array = new Array();
	    var tabTmp_array:Array = new Array();
	    var cpt:Number = 0;
	    for(var i:Number = 0 ; i < tab0_array.length ; i++) {
	        tabTmp_array.push(false);
	    }
	    while(cpt < tab0_array.length) {
	        var j:Number = randRange(0, tab0_array.length-1);
	        if(tabTmp_array[j] == false) {
	            tabTmp_array[j] = true;
	            tab1_array.push(tab0_array[j]);
	            cpt++;
	        }
	    }
	    return tab1_array;
	}
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	private function initInterface() {
		var nCaseBas:Number = 0;
		soundPlayer.playASound("fefeClac");
		this.initPyramide();
		
		this.p_aOrderBas = new Array(1, 2, 3, 4, 5, 6);
		this.p_aOrderBas = randomTab(this.p_aOrderBas);
		p_aListResult = new Array();
		
				
		// On affiche les boite du bas
		for (var i:Number = 1 ; i <= 6 ; ++i) {
			this["p_mcBoiteBas"+i]._visible = true;
		}
		// On va rendre visible seulement 4 nombres
		this.p_aValeurInit = randomTab(this.p_aValeurInit);
		for (var i:Number = 1 ; i <= 10 ; ++i) {
			// On initialise le tableau de résultat à 0 
			this.p_aListResult[i] = -1; 
			// La boite sans le point est toujours invisible au dbut
			this["p_mcBoiteRes"+i]._visible = false;
			
			// La boite avec point est visible seulement pour les 4 premier nombre tiré
			if (i == this.p_aValeurInit[0] || i == this.p_aValeurInit[1] || 
				i == this.p_aValeurInit[2] || i == this.p_aValeurInit[3]) {
				this["p_mcBoite"+i]._visible = true;
				// On lui attache le numéro 
				attachMovie("numero", "p_mcNum"+i, this.getNextHighestDepth(), {_x:this["p_mcBoite"+i]._x, _y:this["p_mcBoite"+i]._y});
				// On donne le numéro en function de la variable stocké dans la boite du fond
				this["p_mcNum"+i].txtValeur.text = this["p_mcBoite"+i].val;
				this.p_aListResult[i] = this["p_mcBoite"+i].val; 
				
				// On enregistre la boite sur laquelle il est 
				this["p_mcNum"+i].emplacement = this["p_mcBoite"+i];
				this["p_mcNum"+i].id = i;
			
			} else {
				this["p_mcBoite"+i]._visible = false;
				// On attache le numero sur une des cases du bas 
				attachMovie("numero", "p_mcNum"+i, this.getNextHighestDepth(), {_x:this["p_mcBoiteBas"+this.p_aOrderBas[nCaseBas]]._x, _y:this["p_mcBoiteBas"+this.p_aOrderBas[nCaseBas]]._y});
				// On donne le numéro en function de la variable stocké dans la boite du fond
				this["p_mcNum"+i].txtValeur.text = this["p_mcBoite"+i].val;
				this["p_mcNum"+i].id = i;
				// On enregistre la boite sur laquelle il est 
				this["p_mcNum"+i].emplacement = "p_mcBoiteBas"+this.p_aOrderBas[nCaseBas];
				this["p_mcNum"+i].num = i;
				// on rend le numéro dragable
				this["p_mcNum"+i].onPress = function () {
					this.old_depth = this.getDepth();
					this.swapDepths(_parent.getNextHighestDepth());
				
					this.init_x = this._x;
					this.init_y = this._y;
					this._x = _parent._xmouse;
					this._y = _parent._ymouse;
					this.startDrag();
				}
				this["p_mcNum"+i].onRelease =  function () {
					_parent.releaseNumber(this);
				}
				nCaseBas++;
			}
		}
	}
	
	/** Doit initialiser l'interface dans laquelle le joueur doit rentrer à la main les chiffres */
	private function initInterfaceNext() {
		
		soundPlayer.playASound("fefeClac");
		if (this.p_nWin == 3) {
			this.p_mcPoint.gotoAndStop(5);
		}
		// Création des chiffres de la pyramide
		this.initPyramide();
		
		p_aListResult = new Array();
		
		// On va rendre visible seulement 4 nombres sur la pyramlide
		this.p_aValeurInit = randomTab(this.p_aValeurInit);
		for (var i:Number = 1 ; i <= 10 ; ++i) {
			// On initialise le tableau de résultat à 0 
			this.p_aListResult[i] = -1; 
			// La boite sans le point est toujours invisible au dbut
			this["p_mcBoiteRes"+i]._visible = false;
			
			// La boite avec point est visible seulement pour les 4 premier nombre tiré
			if (i == this.p_aValeurInit[0] || i == this.p_aValeurInit[1] || 
				i == this.p_aValeurInit[2] || i == this.p_aValeurInit[3]) {
				this["p_mcBoite"+i]._visible = true;
				// On lui attache le numéro 
				attachMovie("numero", "p_mcNum"+i, this.getNextHighestDepth(), {_x:this["p_mcBoite"+i]._x, _y:this["p_mcBoite"+i]._y});
				// On donne le numéro en function de la variable stocké dans la boite du fond
				this["p_mcNum"+i].txtValeur.text = this["p_mcBoite"+i].val;
				this.p_aListResult[i] = this["p_mcBoite"+i].val; 
				
				// On enregistre la boite sur laquelle il est 
				this["p_mcNum"+i].emplacement = this["p_mcBoite"+i];
				this["p_mcNum"+i].id = i;			
			} else {
				// Les autres case apparaisent sans le point
				this["p_mcBoiteRes"+i]._visible = true;
				// On attache le champs de saisi à la case
				attachMovie("numero_saisi", "p_mcNum"+i, this.getNextHighestDepth(), {_x:this["p_mcBoiteRes"+i]._x, _y:this["p_mcBoiteRes"+i]._y});
				this["p_mcNum"+i].val = i;
				this["p_mcNum"+i].txtValeur.text = "?";
				//le champ n'accepte que Chiffre
				this["p_mcNum"+i].txtValeur.restrict = "0-9"; 
				//le champ n'accepte que 3 chiffres
				this["p_mcNum"+i].txtValeur.maxChars = 3; 
				// Suppression du point d'interrogation lors d'un clic
				this["p_mcNum"+i].txtValeur.onSetFocus = function() {
					if (this.text == "?") {
						this.text = "";
					}
				};
				this["p_mcNum"+i].txtValeur.onKillFocus = function() {
					trace (_parent.val + " = " + this.text);
					_parent.p_aListResult[this.val] = this.text; 
					trace (_parent.p_aListResult);
				}
				trace ("ADD : "+i+" "+this["p_mcNum"+i].txtValeur.text);
			}
		}
		// On va rendre le bouton de fin interactif
		this.p_mcBoutonFin.onPress = function () {
			_parent.verifEndNext();
		}
	}
	
	/** GEstion du laché de nombre */
	private function releaseNumber(mcNumber:MovieClip) {
		var bFlag:Boolean = false;
		for (var i:Number = 1 ; i <= 10 ; ++i) {
			if (mcNumber.hitTest(this["p_mcBoite"+i]) && (mcNumber.emplacement != "p_mcBoiteRes"+i)) {
				if (!((this["p_mcBoite"+i] == this["p_mcBoite"+p_aValeurInit[0]] || 
					 this["p_mcBoite"+i] == this["p_mcBoite"+p_aValeurInit[1]] || 
					 this["p_mcBoite"+i] == this["p_mcBoite"+p_aValeurInit[2]] || 
					 this["p_mcBoite"+i] == this["p_mcBoite"+p_aValeurInit[3]]))) {
					// La case sur laquelle on le pose apparait
					this["p_mcBoiteRes"+i]._visible = true;
					
					// On rend invisible la boite dans laquelle il était
					this[String(mcNumber.emplacement)]._visible = false;
					// On met le résultats à vide pour l'ancien indice
					this.p_aListResult[mcNumber.indice] = -1;
					
					// On parcours les autres chiffres pour en trouver une au même endroit
					for (var j:Number = 1 ; j <= 10 ; ++j) {
						if ((mcNumber.id != this["p_mcNum"+j].id) && (this["p_mcNum"+j]._x == this["p_mcBoiteRes"+i]._x) && (this["p_mcNum"+j]._y == this["p_mcBoiteRes"+i]._y)) {
							// La case ou il va est occupé, on fait l'echange
							
							// L'ancienne case du chiffre contient la nouvelle valeur
							this.p_aListResult[mcNumber.indice] = this["p_mcNum"+j].txtValeur.text;
							// On marque son nouvel indice 
							this["p_mcNum"+j].indice = mcNumber.indice;
							// On lui met les anciennes position de l'opbjet draggué
							this["p_mcNum"+j]._x = mcNumber.init_x; 
							this["p_mcNum"+j]._y = mcNumber.init_y;
							// On fixe le nouvelle emplacement
							this["p_mcNum"+j].emplacement = mcNumber.emplacement;
					
								
							// On rend visible sa case
							this[String(mcNumber.emplacement)]._visible = true;
							break;	
						} 
					}
					
					// On met le résultats à jour dans la liste 
					this.p_aListResult[i] = mcNumber.txtValeur.text;
					
					// On fixe le nouvelle emplacement
					mcNumber.emplacement = "p_mcBoiteRes"+i;
					mcNumber.indice = i;
					
					// Permet de placer le text sur la boite
					mcNumber.init_x = this["p_mcBoiteRes"+i]._x;
					mcNumber.init_y = this["p_mcBoiteRes"+i]._y;
					
					bFlag = true;
					break;
				}
			}
		}
		//trace (this.p_aListResult);
		mcNumber._x = mcNumber.init_x;
		mcNumber._y = mcNumber.init_y;
		mcNumber.swapDepths(mcNumber.old_depth);
		if (bFlag == true) {
			soundPlayer.playASound("glou2");
		
			// Apres chaque pose de chiffre, on vérifie si c'est fini
			this.verifEnd();		
		}
		mcNumber.stopDrag();	
	}
	
	/** On vérifie si les conditions de victoire sont remplies */
	private function verifEnd() {
		var nCompteur:Number = 0;
		trace (" (("+Number(this.p_aListResult[1])+" + "+Number(this.p_aListResult[2])+") == "+Number(this.p_aListResult[5])+") && "+
			 "(("+Number(this.p_aListResult[2])+" + "+Number(this.p_aListResult[3])+") == "+Number(this.p_aListResult[6])+") && "+
			 "(("+Number(this.p_aListResult[3])+" + "+Number(this.p_aListResult[4])+") == "+Number(this.p_aListResult[7])+") && "+
			 "(("+Number(this.p_aListResult[5])+" + "+Number(this.p_aListResult[6])+") == "+Number(this.p_aListResult[8])+") && "+
			 "(("+Number(this.p_aListResult[6])+" + "+Number(this.p_aListResult[7])+") == "+Number(this.p_aListResult[9])+") && "+
			 "(("+Number(this.p_aListResult[8])+" + "+Number(this.p_aListResult[9])+") == "+Number(this.p_aListResult[10]));
			 
			if ( ((Number(this.p_aListResult[1]) + Number(this.p_aListResult[2])) == Number(this.p_aListResult[5])) && 
			 ((Number(this.p_aListResult[2]) + Number(this.p_aListResult[3])) == Number(this.p_aListResult[6])) && 
			 ((Number(this.p_aListResult[3]) + Number(this.p_aListResult[4])) == Number(this.p_aListResult[7])) && 
			 ((Number(this.p_aListResult[5]) + Number(this.p_aListResult[6])) == Number(this.p_aListResult[8])) && 
			 ((Number(this.p_aListResult[6]) + Number(this.p_aListResult[7])) == Number(this.p_aListResult[9])) && 
			 ((Number(this.p_aListResult[8]) + Number(this.p_aListResult[9])) == Number(this.p_aListResult[10]))) {
			trace ("Victoire "+this.p_nWin);
			 
			// On avance les points
			++this.p_nWin;
			
			// Tant qu'on est à moins de 3 on avance le compteur
			if (p_nWin <= 3) {
				this.p_mcPoint.gotoAndStop(this.p_nWin + 1);
				for (var i:Number = 1 ; i <= 10 ; ++i) {
					this["p_mcNum"+i].removeMovieClip();
				}
			}
			// A 3 points, on part sur le jeux deux
			if (p_nWin == 3) {
				soundPlayer.playASound("clang1");
				this.gotoAndPlay("continue");
				trace ('ok');
			}
			// Il ne rejoue que s'il a 0, 1 ou 2 points
			if (p_nWin < 3) {
				this.initInterface();
			}
		} else {
			// Si tout n'est pas correctement rangé on regarde si la pyramide est pleine ou pas
			for (var i:Number = 1 ; i <= 10 ; ++i) {
				 
				 if (this.p_aListResult[i] != -1) {
				 	++nCompteur;
				 }
			}
			if (nCompteur == 10) {
				nCompteur = 1;
				trace ("Perdu");
				for (var i:Number = 1 ; i <= 10 ; ++i) {
					if (this.p_aListResult[i] != this.p_aListValues[i]) {
						// Si le chiffre est pas bon on le remet en bas
						
						// On rend visible une boite du bas
						this["p_mcBoiteBas"+this.p_aOrderBas[nCompteur]]._visible = true;
						this[String(this["p_mcNum"+i].emplacement)]._visible = false;
						// On lui attache le numéro 
						this["p_mcNum"+i]._x = this["p_mcBoiteBas"+this.p_aOrderBas[nCompteur]]._x;
						this["p_mcNum"+i]._y = this["p_mcBoiteBas"+this.p_aOrderBas[nCompteur]]._y;
						// On vide sa valeur dans les resultats
						this.p_aListResult[this["p_mcNum"+i].indice] = -1;
						// On marque son nouvel indice 
						this["p_mcNum"+i].indice = -1;
						// On lui met l'emplacement du cube du bas
						this["p_mcNum"+i].emplacement = "p_mcBoiteBas"+this.p_aOrderBas[nCompteur];					
						++nCompteur;
					}
				}
			} else {
				trace ("Non fini");
			}
		}				 
	}
	
	/** Vérication des points de victoire dans la seconde partie du jeu */
	private function verifEndNext() {
		var nCompteur:Number = 0
		// On verifie que le jeu est bien terminé
		for (var i:Number = 1 ; i <= 10 ; ++i) {
			if (this["p_mcNum"+i].txtValeur.text >= 0 && this["p_mcNum"+i].txtValeur.text != "?" && this["p_mcNum"+i].txtValeur.text != "") {
				++nCompteur;
			}
		}
		// Les cases sont toute remplis
		if (nCompteur == 10) {
			trace (" ( ("+Number(this.p_mcNum1.txtValeur.text)+" + "+Number(this.p_mcNum2.txtValeur.text)+") == "+Number(this.p_mcNum5.txtValeur.text)+") && "+
					 "(("+Number(this.p_mcNum2.txtValeur.text)+" + "+Number(this.p_mcNum3.txtValeur.text)+") == "+Number(this.p_mcNum6.txtValeur.text)+") && "+
					 "(("+Number(this.p_mcNum3.txtValeur.text)+" + "+Number(this.p_mcNum4.txtValeur.text)+") == "+Number(this.p_mcNum7.txtValeur.text)+") && "+
					 "(("+Number(this.p_mcNum5.txtValeur.text)+" + "+Number(this.p_mcNum6.txtValeur.text)+") == "+Number(this.p_mcNum8.txtValeur.text)+") && "+
					 "(("+Number(this.p_mcNum6.txtValeur.text)+" + "+Number(this.p_mcNum7.txtValeur.text)+") == "+Number(this.p_mcNum9.txtValeur.text)+") && "+
					 "(("+Number(this.p_mcNum8.txtValeur.text)+" + "+Number(this.p_mcNum9.txtValeur.text)+") == "+Number(this.p_mcNum10.txtValeur.text));
			if ( ((Number(this.p_mcNum1.txtValeur.text) + Number(this.p_mcNum2.txtValeur.text)) == Number(this.p_mcNum5.txtValeur.text)) && 
				 ((Number(this.p_mcNum2.txtValeur.text) + Number(this.p_mcNum3.txtValeur.text)) == Number(this.p_mcNum6.txtValeur.text)) && 
				 ((Number(this.p_mcNum3.txtValeur.text) + Number(this.p_mcNum4.txtValeur.text)) == Number(this.p_mcNum7.txtValeur.text)) && 
				 ((Number(this.p_mcNum5.txtValeur.text) + Number(this.p_mcNum6.txtValeur.text)) == Number(this.p_mcNum8.txtValeur.text)) && 
				 ((Number(this.p_mcNum6.txtValeur.text) + Number(this.p_mcNum7.txtValeur.text)) == Number(this.p_mcNum9.txtValeur.text)) && 
				 ((Number(this.p_mcNum8.txtValeur.text) + Number(this.p_mcNum9.txtValeur.text)) == Number(this.p_mcNum10.txtValeur.text))) {
				// Tous les chiffres sont correct
				++this.p_nWin;
				// Tant qu'on est à moins de 6 on avance le compteur
				if (this.p_nWin >= 3 && this.p_nWin <= 6) {
					if (this.p_nWin == 4) {
						this.p_mcPoint.gotoAndStop(6);
					}
					if (this.p_nWin == 5) {
						this.p_mcPoint.gotoAndStop(7);
					}
					// Si la partie n'est pas fini, on continue
					if (this.p_nWin < 6) { 
						// On repart sur une partie
						for (var i:Number = 1 ; i <= 10 ; ++i) {
							this["p_mcNum"+i].removeMovieClip();
						}
						this.initInterfaceNext();
					} else {
						trace ("termine");
						this.p_mcPoint.gotoAndStop(8);
						// On désactive le bouton de validation
						this.p_mcBoutonFin.enabled = false;
						// C'est terminé, le parchemin est apparu
						this.p_mcPoint.onPress = function () {
							_parent.endGame();
						}
					}
				}
			} else {
				// On supprime les valeurs des champs qui sont faux.
				for (var i:Number = 1 ; i <= 10 ; ++i) {
					if (this["p_mcNum"+i].txtValeur.text != this.p_aListValues[i]) {
						this["p_mcNum"+i].txtValeur.text = "";
						--nCompteur;
					}
				}
			}
			/*	trace (this.p_nWin + " = " + p_aListValues +", "+ p_aListResult);
				// On supprime les valeurs des champs qui sont faux.
				for (var i:Number = 1 ; i <= 10 ; ++i) {
					if (this["p_mcNum"+i].txtValeur.text != this.p_aListValues[i]) {
						this["p_mcNum"+i].txtValeur.text = "";
						--nCompteur;
					}
				}
			
			if (nCompteur == 10) {
				// Tous les chiffres sont correct
				++this.p_nWin;
				// Tant qu'on est à moins de 6 on avance le compteur
				if (this.p_nWin >= 3 && this.p_nWin <= 6) {
					if (this.p_nWin == 4) {
						this.p_mcPoint.gotoAndStop(6);
					}
					if (this.p_nWin == 5) {
						this.p_mcPoint.gotoAndStop(7);
					}
					// Si la partie n'est pas fini, on continue
					if (this.p_nWin < 6) { 
						// On repart sur une partie
						for (var i:Number = 1 ; i <= 10 ; ++i) {
							this["p_mcNum"+i].removeMovieClip();
						}
						this.initInterfaceNext();
					} else {
						trace ("termine");
						this.p_mcPoint.gotoAndStop(8);
						// On désactive le bouton de validation
						this.p_mcBoutonFin.enabled = false;
						// C'est terminé, le parchemin est apparu
						this.p_mcPoint.onPress = function () {
							_parent.endGame();
						}
					}
				}
			}*/
		} else {
			soundPlayer.playASound("JeuPasTerminer");
		}
	}
	
	
	/** Création des nombres de la pyramide */
	private function initPyramide() {
		
		this.p_aValeurInit = new Array(1, 2, 3, 4, 5, 6, 7, 8, 9);
		this.p_aValeurInit = randomTab(this.p_aValeurInit);
		// On commence par le tirage des 4 nombres du bas
		this.p_aListValues[1] = this.p_aValeurInit[0];
		this.p_mcBoite1.val = p_aListValues[1];
		this.p_aListValues[2] = this.p_aValeurInit[1];
		this.p_mcBoite2.val = p_aListValues[2];
		this.p_aListValues[3] = this.p_aValeurInit[2];
		this.p_mcBoite3.val = p_aListValues[3];
		this.p_aListValues[4] = this.p_aValeurInit[3];
		this.p_mcBoite4.val = p_aListValues[4];
		
		// Ensuite les trois valeurs du second étage
		this.p_aListValues[5] = p_aListValues[1] + p_aListValues[2];
		this.p_mcBoite5.val = p_aListValues[5];
		this.p_aListValues[6] = p_aListValues[2] + p_aListValues[3];
		this.p_mcBoite6.val = p_aListValues[6];
		this.p_aListValues[7] = p_aListValues[3] + p_aListValues[4];
		this.p_mcBoite7.val = p_aListValues[7];
		
		// Ensuite les deux valeurs du troisieme étage
		this.p_aListValues[8] = p_aListValues[5] + p_aListValues[6];
		this.p_mcBoite8.val = p_aListValues[8];
		this.p_aListValues[9] = p_aListValues[6] + p_aListValues[7];
		this.p_mcBoite9.val = p_aListValues[9];
		
		// Enfin le sommet
		this.p_aListValues[10] = p_aListValues[8] + p_aListValues[9];
		this.p_mcBoite10.val = p_aListValues[10];
		
	}
	
	/** Fin du jeu */
	private function endGame() {
		// on rend les cases visible et les numeros invisibles
		for (var i:Number = 1 ; i <= 10 ; ++i) {
			this["p_mcBoite"+i]._visible = true;
			this["p_mcNum"+i]._visible = false;
		}
		this.p_mcBoutonFin.removeMovieClip();
		soundPlayer.playASound("shark2");
		
		  
		_parent._parent.setWinGame(9, _parent._parent.joueur1); 
	  
		_parent._parent.barreInterface_mc.showParchemin();
		this.gotoAndStop("fin");
		gameFinished()
	}
}

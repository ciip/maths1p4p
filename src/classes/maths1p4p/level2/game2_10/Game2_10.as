﻿/**
 * class com.maths1p4p.Game2_10
 * 
 * @author Yoann Augen
 * @version 1.0
 *
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level2.game2_10.Game2_10 extends maths1p4p.AbstractGame {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 10;
	
	/** Bouton rouge clignotant */
	public var p_mcBoutonGo:MovieClip;
	
	/** Le repere du resultat */
	public var p_mcResultNumber:MovieClip;
	
	/** Le nombre rentré par le joueur */
	public var p_mcNumberEnter:MovieClip;
	
	/** Les 5 valeurs de drapeau raté */
	public var p_mcFlag1:MovieClip;
	public var p_mcFlag2:MovieClip;
	public var p_mcFlag3:MovieClip;
	public var p_mcFlag4:MovieClip;
	public var p_mcFlag5:MovieClip;

	/** Les points d'erreur */
	public var p_mcPoint:MovieClip;
	
	/** Le point de victoire */
	public var p_mcResultatWin:MovieClip;
	
	/** Coffre fort */
	public var p_mcCoffre:MovieClip;
	
	/** ScrollBat gauche et droite et bouton*/
	public var p_mcScrollLeft:MovieClip;
	public var p_mcScrollRight:MovieClip;
	
	public var p_mcButtonLeftUp:MovieClip;
	public var p_mcButtonLeftDown:MovieClip;
	public var p_mcButtonRightUp:MovieClip;
	public var p_mcButtonRightDown:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** Le nombre à trouver */ 
	private var p_nResultNumber:Number;
	
	/** Les deux repere 0 et 80 */
	private var p_mcRepere1:MovieClip;
	private var p_mcRepere2:MovieClip;
	
	/** Distance entre deux points */
	private var p_nDistance:Number;
	
	/** Etat du jeu : arret ou marche */
	private var p_bState:Boolean = false;
	
	/** Nombre de tentative */
	private var p_nTry:Number;
	
	/** Nombre de victoire */
	private var p_nCoffre:Number;
	

	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game2_10() {
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		this.p_nCoffre = 0;
		this.p_mcNumberEnter._visible = false;
 	}
	
	/**
	* Fonction qui renvoi un nombre aleatoire entre min et max
	* @param Number min
	* @param Number max
	* @return Number
	* 
	*/
	public function randRange(min:Number, max:Number) {
	     var randomNum:Number = Math.floor(Math.random() * (max - min + 1)) + min;
	     return randomNum;
	}
	
	/**
	* Fonction qui renvoi le tableau mélangé
	* @param Array tab0_array le tableau d'origine
	* @return Array
	*/
	public function randomTab(tab0_array:Array) {
	    var tab1_array:Array = new Array();
	    var tabTmp_array:Array = new Array();
	    var cpt:Number = 0;
	    for(var i:Number = 0 ; i < tab0_array.length ; i++) {
	        tabTmp_array.push(false);
	    }
	    while(cpt < tab0_array.length) {
	        var j:Number = randRange(0, tab0_array.length-1);
	        if(tabTmp_array[j] == false) {
	            tabTmp_array[j] = true;
	            tab1_array.push(tab0_array[j]);
	            cpt++;
	        }
	    }
	    return tab1_array;
	}
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	private function initInterface() {
		// Si la partie n'a pas déjà commencé
		if (this.p_bState == false) {
			// On ne peu plus cliquer pour lancer le jeu
			this.p_bState = true;
			
			// On efface un eventuel logo de victoire et de position
			this.p_mcResultatWin.removeMovieClip();
			this.p_mcResultNumber.removeMovieClip();
			
			// 0 tentative 
			this.p_nTry = 0;
			
			// Le bouton devient rouge
			this.p_mcBoutonGo.gotoAndStop(3);
			
			// Calcul la distance entre deux points
			this.p_nDistance = Number(this.p_mcRepere2._x - this.p_mcRepere1._x);
			this.p_nDistance = Number(this.p_nDistance/80);
			
			// On tire un chiffre à trouver
			this.p_nResultNumber = randRange(1, 79);
			trace ("Resultat : "+this.p_nResultNumber);
			
			// On place le point sur le paysage
			attachMovie("resultat", "p_mcResultNumber", this.getNextHighestDepth(), {_x: this.p_mcRepere1._x + (this.p_nDistance * this.p_nResultNumber), _y:this.p_mcRepere1._y});
			
			// On rend visible le champs à remplir 
			this.p_mcNumberEnter._visible = true;
			
			this.p_mcNumberEnter._focusrect = true;
			
			
			//le champ n'accepte que Chiffre
			this.p_mcNumberEnter.txtValue.restrict = "0-9"; 
				
			//le champ n'accepte que 2 chiffres
			this.p_mcNumberEnter.txtValue.maxChars = 2; 			
		}
		
			
		// On valide le nombre rentré : deux façons differente			
		
		// Le bouton entrée 
		Key.addListener(this.p_mcNumberEnter);
		this.p_mcNumberEnter.onKeyDown = function() {			
			if (Key.getCode() == Key.ENTER) {
				_parent.soundPlayer.playASound("minitic");		
				_parent.controleNombre();
			}						
		}
		// Le bouton rouge 
		if (this.p_bState == true) {
			soundPlayer.playASound("minitic");
			this.controleNombre();
		}
		
		// Les boutons de scrool
		p_mcButtonLeftUp.onRelease = function() {
     		_parent.p_mcScrollLeft.txtValue.scroll--;
		}
		p_mcButtonLeftDown.onRelease = function() {
			_parent.p_mcScrollLeft.txtValue.scroll++;
		}
		p_mcButtonRightUp.onRelease = function() {
     		_parent.p_mcScrollRight.txtValue.scroll--;
		}
		p_mcButtonRightDown.onRelease = function() {
			_parent.p_mcScrollRight.txtValue.scroll++;
		}
		
		// On donne le focus au champs de texte lors de chaque clique sur le bouton rouge
		Selection.setFocus(this.p_mcNumberEnter.txtValue);
			
	}
	
	/** On va controler le chiffre rentré */
	private function controleNombre() {
		var nNombre:Number = Number(this.p_mcNumberEnter.txtValue.text);
		// Le chiffre doit etre dans le bon intervalle
		if (nNombre > 0 && nNombre < 80) {
			// On l'affiche à l'écran
			if (nNombre == this.p_nResultNumber) {
				// Si il a trouvé le bon chiffre
				attachMovie("resultatWin", "p_mcResultatWin", this.getNextHighestDepth(), {_x: this.p_mcRepere1._x + (this.p_nDistance * nNombre), _y:this.p_mcRepere1._y});
				
				// On incremente le nombre de victoire 
				++this.p_nCoffre;
				
				soundPlayer.playASound("frele2");
				// On ouvre le verrou du coffre
				this.p_mcCoffre.gotoAndStop(this.p_nCoffre + 1);
				
				// On efface les numeros droite et gauche 
				this.p_mcScrollLeft.txtValue.text = "";
				this.p_mcScrollRight.txtValue.text = "";
				
				// Le bouton redevient cliquable
				this.p_bState = false;
				// On le fait clignoter
				this.p_mcBoutonGo.gotoAndPlay(1);
				// on efface les 5 drapeaux et les erreurs
				this.p_nTry = 0;
				for (var i = 1 ; i <= 5 ; ++i) {
					this["p_mcFlag"+i].removeMovieClip();
				}
				this.p_mcPoint.gotoAndStop(1);
				// On efface la valeur
				this.p_mcNumberEnter.txtValue.text = "";
				// On efface la possibilité d'écrire un nombre
				this.p_mcNumberEnter._visible = false;
				
				// Si on a fini le jeu on rend le parchemin cliquable
				if (this.p_nCoffre == 4) {
					
					this.p_mcBoutonGo.gotoAndStop(3);
					this.p_mcBoutonGo.enabled = true;
					
					this.p_mcCoffre.onPress = function () {
						_parent.soundPlayer.playASound("shark2");
						_parent.endGame();	
					}
				}	
			} else {
				// On incremente le nombre de tentative
				this.p_nTry++;
				
				// Il s'est trompé
				attachMovie("valeurRatee", "p_mcFlag"+p_nTry, this.getNextHighestDepth(), {_x: this.p_mcRepere1._x + (this.p_nDistance * nNombre), _y:this.p_mcRepere1._y});
				
				// On met à jour les points d'erreurs
				this.p_mcPoint.gotoAndStop(this.p_nTry + 1);
				
				// On met le nombre dans la scrollbar de droite ou de gauche
				if (nNombre < this.p_nResultNumber) {
					trace ("ajout gauche "+nNombre);
					this.p_mcScrollLeft.txtValue.text += nNombre+"\n";
				} else if (nNombre > this.p_nResultNumber) {
					trace ("ajout droite "+nNombre);
					this.p_mcScrollRight.txtValue.text += nNombre+"\n";
				}
				// On efface la valeur
				this.p_mcNumberEnter.txtValue.text = "";
				
				// Si on a 5 erreurs, on efface les 5 drapeaux 
				if (this.p_nTry == 5) {
					this.p_nTry = 0;
					for (var i = 1 ; i <= 5 ; ++i) {
						this["p_mcFlag"+i].removeMovieClip();
					}
					this.p_mcPoint.gotoAndPlay(7);
				}
			}
		}
	}
	
	/** Fin du jeu */
	private function endGame() {
		// On efface un eventuel logo de victoire et de position
		this.p_mcResultatWin.removeMovieClip();
		this.p_mcResultNumber.removeMovieClip();
		this._parent._parent.setWinGame(10, this._parent._parent.joueur1); 
		_parent._parent.barreInterface_mc.showParchemin();
		this.gotoAndStop("fin");
		gameFinished()
	}
}

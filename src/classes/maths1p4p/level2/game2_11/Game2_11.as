﻿/**
 * class com.maths1p4p.Game2_11
 * 
 * @author Yoann Augen
 * @version 1.0
 *
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level2.game2_11.Game2_11 extends maths1p4p.AbstractGame {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 11;
	
	/** Les 16 poissons differents */
	public var p_mcPoisson_1:MovieClip;
	public var p_mcPoisson_2:MovieClip;
	public var p_mcPoisson_3:MovieClip;
	public var p_mcPoisson_4:MovieClip;
	public var p_mcPoisson_5:MovieClip;
	public var p_mcPoisson_6:MovieClip;
	public var p_mcPoisson_7:MovieClip;
	public var p_mcPoisson_8:MovieClip;
	public var p_mcPoisson_9:MovieClip;
	public var p_mcPoisson_10:MovieClip;
	public var p_mcPoisson_11:MovieClip;
	public var p_mcPoisson_12:MovieClip;
	public var p_mcPoisson_13:MovieClip;
	public var p_mcPoisson_14:MovieClip;
	public var p_mcPoisson_15:MovieClip;
	public var p_mcPoisson_16:MovieClip;
	
	/** Les localisations des poissons */
	public var p_mcLoc1:MovieClip;
	public var p_mcLoc2:MovieClip;
	public var p_mcLoc3:MovieClip;
	public var p_mcLoc4:MovieClip;
	public var p_mcLoc5:MovieClip;
	public var p_mcLoc6:MovieClip;
	public var p_mcLoc7:MovieClip;
	public var p_mcLoc8:MovieClip;
	public var p_mcLoc9:MovieClip;
	public var p_mcLoc10:MovieClip;
	public var p_mcLoc11:MovieClip;
	public var p_mcLoc12:MovieClip;
	public var p_mcLoc13:MovieClip;
	public var p_mcLoc14:MovieClip;
	public var p_mcLoc15:MovieClip;
	public var p_mcLoc16:MovieClip;
	
	/** Les cases du milieu */
	public var p_mcCase1:MovieClip;
	public var p_mcCase2:MovieClip;
	public var p_mcCase3:MovieClip;
	public var p_mcCase4:MovieClip;
	public var p_mcCase5:MovieClip;
	public var p_mcCase6:MovieClip;
	public var p_mcCase7:MovieClip;
	public var p_mcCase8:MovieClip;
	public var p_mcCase9:MovieClip;
	public var p_mcCase10:MovieClip;
	public var p_mcCase11:MovieClip;
	public var p_mcCase12:MovieClip;
	public var p_mcCase13:MovieClip;
	public var p_mcCase14:MovieClip;
	public var p_mcCase15:MovieClip;
	public var p_mcCase16:MovieClip;
	
	/** Le contour rouge des noms */
	public var p_mcRedName1:MovieClip;
	public var p_mcRedName2:MovieClip;
	
	/** Les coffres forts */
	public var p_mcPoint1:MovieClip;
	public var p_mcPoint2:MovieClip;
	
	/** Bouton suite */
	public var p_mcBouton:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** Le joueur dont c'est le tour */
	private var p_nTour:Number;
	
	/** Liste des poissons */
	private var p_aListElem:Array;
	
	/** Liste des cases */
	private var p_aListCase:Array;
	
	/** Les points de chaque joueur */
	public var p_nPoint1:Number;
	public var p_nPoint2:Number;
	
	/** Peut on bouger les poissons */
	public var p_bMove:Boolean;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game2_11() {
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		// Liste des poissons et des cases
		this.p_aListElem = new Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
		this.p_aListCase = new Array();
		this.p_nPoint1 = 0;
		this.p_nPoint2 = 0;
		
		//on initialise les noms dans les champs textes
		this["nom1_txt"].text = this._parent._parent.joueur1.getName();
		this["nom2_txt"].text = this._parent._parent.joueur2.getName();
		
		// Joueur 1 commence
		this.p_nTour = 0;
	}
	
	/**
	* Fonction qui renvoi un nombre aleatoire entre min et max
	* @param Number min
	* @param Number max
	* @return Number
	* 
	*/
	public function randRange(min:Number, max:Number) {
	     var randomNum:Number = Math.floor(Math.random() * (max - min + 1)) + min;
	     return randomNum;
	}
	
	/**
	* Fonction qui renvoi le tableau mélangé
	* @param Array tab0_array le tableau d'origine
	* @return Array
	*/
	public function randomTab(tab0_array:Array) {
	    var tab1_array:Array = new Array();
	    var tabTmp_array:Array = new Array();
	    var cpt:Number = 0;
	    for(var i:Number = 0 ; i < tab0_array.length ; i++) {
	        tabTmp_array.push(false);
	    }
	    while(cpt < tab0_array.length) {
	        var j:Number = randRange(0, tab0_array.length-1);
	        if(tabTmp_array[j] == false) {
	            tabTmp_array[j] = true;
	            tab1_array.push(tab0_array[j]);
	            cpt++;
	        }
	    }
	    return tab1_array;
	}
	
	/** 
	 * Nous dit si la valeur needle est présente dans un tableau haystack
	 */
	public function inArray(needle, haystack) {
		for (var i:Number = 0 ; i < haystack.length ; i++) {
			if (haystack[i] == needle) {
				return true;
			}
		}
		return false;
	}
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/** Initialisation de l'interface de jeu */
	private function initInterface() {
		// On vide les cases du milieu et les poissons
		for (var i:Number = 1 ; i <= 16 ; ++i) {
			this.p_aListCase[i] = 0;
			this["p_mcPoisson_"+i].removeMovieClip();
		}
		this.p_bMove = true;
		
		// Le bouton est inactif
		this.p_mcBouton._visible = false;
		// On mélange le tableau des poissons
		this.p_aListElem = randomTab(this.p_aListElem);
		
		// On doit placer ensuite les poissons
		this.placementPoisson();
		
		// On fixe les caractéristiques des poissons
		this.initPoisson();
		
		this.changeTour();
		
	}
	
	// Change le tour du joueur
	private function changeTour() {
		if (this.p_nTour == 0 || this.p_nTour == 2) {
			// C'est au joueur 1
			this.p_nTour = 1;	
			// On met le trait rouge autour de son nom
			this.p_mcRedName1._visible = true;
			this.p_mcRedName2._visible = false;
			
			// On rend dragable les poissons du joueur 1
			for (var i:Number = 1 ; i <= 16 ; ++i) {
				if (p_aListElem[(i - 1)] <= 8 && !inArray(i, p_aListCase)) {
					this["p_mcPoisson_"+i].enabled = true;
					this["p_mcPoisson_"+i].onPress = function () {
						if (_parent.p_bMove == true) {						
							this.old_depth = this.getDepth();
							this.swapDepths(_parent.getNextHighestDepth());
							// Les coordonnées d'avant déplacement
							this.x_origine = this._x;
							this.y_origine = this._y;
							this._x = _parent._xmouse;
							this._y = _parent._ymouse;
							this.startDrag();
						}
					}
					this["p_mcPoisson_"+i].onRelease = function(){
						// Controle du drop
						_parent.dropAction(this);
					}
				} else {
					this["p_mcPoisson_"+i].enabled = false;
				}	
			}
			
		} else {
			// C'est au joueur 2
			this.p_nTour = 2;
			// On met le trait rouge autour de son nom
			this.p_mcRedName1._visible = false;
			this.p_mcRedName2._visible = true;
			// On rend dragable les poissons du joueur 1
			for (var i:Number = 1 ; i <= 16 ; ++i) {
				if (p_aListElem[(i - 1)] > 8 && !inArray(i, p_aListCase)) {
					this["p_mcPoisson_"+i].enabled = true;
					this["p_mcPoisson_"+i].onPress = function () {
						if (_parent.p_bMove == true) {						
							this.old_depth = this.getDepth();
							this.swapDepths(_parent.getNextHighestDepth());
							// Les coordonnées d'avant déplacement
							this.x_origine = this._x;
							this.y_origine = this._y;
							this._x = _parent._xmouse;
							this._y = _parent._ymouse;
							this.startDrag();
						}
					}
					this["p_mcPoisson_"+i].onRelease = function(){
						// Controle du drop
						_parent.dropAction(this);
					}
				} else {
					this["p_mcPoisson_"+i].enabled = false;
				}	
			}
		}
	}
	
	/** On lache un poisson sur une case */
	private function dropAction(mcActu:MovieClip) {
		var bFlag:Boolean = false;
		var bWin:Boolean = false;
		var bNull:Boolean = true;
		
		mcActu.stopDrag();
		mcActu.swapDepths(mcActu.old_depth);
		// On controle le laché sur une des cases
		for (var i:Number = 1 ; i <= 16 ; ++i) {
			if (mcActu.hitTest(this["p_mcCase"+i]) && this.p_aListCase[i] == 0) {
				// La case est vide, on drop
				soundPlayer.playASound("minitic");
				mcActu._x = this["p_mcCase"+i]._x;
				mcActu._y = this["p_mcCase"+i]._y;
				this.p_aListCase[i] = mcActu.id;
				bFlag = true;
				// On vérifie la victoire
				if (this.checkVictory() == true) {
					bWin = true;
					this.winPart();
				}
				// On vérifie qu'il n'y ai pas match null 
				if (bWin == false) {
					for (var j:Number = 1 ; j <= 16 ; ++j) {
						// Si on en trouve au moins un avec 0
						if (this.p_aListCase[j] == 0) {
							// Il n'y a pas match null
							bNull = false;
							break;	
						}
					} 
					// Sinon il y a match null
					if (bNull == true) {
						soundPlayer.playASound("matchNul");
						this.initInterface();
					}
				}
				break;
			}
		}
		
		if (bFlag == false) {
			mcActu._x = mcActu.x_origine;
			mcActu._y = mcActu.y_origine;
		} else if (bWin == false) {
			this.changeTour();
		}
	}
	
	/** Un joueur gagne une manche */
	function winPart() {
		this["p_nPoint"+this.p_nTour]++;
		soundPlayer.playASound("fefeClac");
		// On ouvre un verroux du coffre fort du joueur
		this["p_mcPoint"+this.p_nTour].gotoAndStop(this["p_nPoint"+this.p_nTour] + 1);
		
		this.p_bMove = false;
			
		if (this["p_nPoint"+this.p_nTour] < 3) {
			// Apparition du bouton suite
			this.p_mcBouton._visible = true;
			// S'il est à moins de 3 victoires on repart sur une partie
			this.p_mcBouton.onPress = function () {
				_parent.initInterface();
			}
		} else {
			//On rend cliquable le parchemin
			this["p_mcPoint"+this.p_nTour].onPress = function () {
				_parent.soundPlayer.playASound("shark2");
				_parent.endGame();
			}
		}
	}
	
	/** On verifie la victoire éventuelle
	 * @return Boolean 
	 */
	function checkVictory() {
		// Vérification horizontal
		if (p_aListCase[1] != 0 && p_aListCase[2] != 0 && p_aListCase[3] != 0 && p_aListCase[4] != 0) {
			if ((this["p_mcPoisson_"+p_aListCase[1]].taille == this["p_mcPoisson_"+p_aListCase[2]].taille && 
				 this["p_mcPoisson_"+p_aListCase[1]].taille == this["p_mcPoisson_"+p_aListCase[3]].taille && 
				 this["p_mcPoisson_"+p_aListCase[1]].taille == this["p_mcPoisson_"+p_aListCase[4]].taille) || 
				(this["p_mcPoisson_"+p_aListCase[1]].couleur == this["p_mcPoisson_"+p_aListCase[2]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[1]].couleur == this["p_mcPoisson_"+p_aListCase[3]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[1]].couleur == this["p_mcPoisson_"+p_aListCase[4]].couleur) || 
				(this["p_mcPoisson_"+p_aListCase[1]].rayure == this["p_mcPoisson_"+p_aListCase[2]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[1]].rayure == this["p_mcPoisson_"+p_aListCase[3]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[1]].rayure == this["p_mcPoisson_"+p_aListCase[4]].rayure) || 
				(this["p_mcPoisson_"+p_aListCase[1]].direction == this["p_mcPoisson_"+p_aListCase[2]].direction && 
				 this["p_mcPoisson_"+p_aListCase[1]].direction == this["p_mcPoisson_"+p_aListCase[3]].direction && 
				 this["p_mcPoisson_"+p_aListCase[1]].direction == this["p_mcPoisson_"+p_aListCase[4]].direction)) {
				return true;
			}		
		}
		if (p_aListCase[5] != 0 && p_aListCase[6] != 0 && p_aListCase[7] != 0 && p_aListCase[8] != 0) {
			if ((this["p_mcPoisson_"+p_aListCase[5]].taille == this["p_mcPoisson_"+p_aListCase[6]].taille && 
				 this["p_mcPoisson_"+p_aListCase[5]].taille == this["p_mcPoisson_"+p_aListCase[7]].taille && 
				 this["p_mcPoisson_"+p_aListCase[5]].taille == this["p_mcPoisson_"+p_aListCase[8]].taille) || 
				(this["p_mcPoisson_"+p_aListCase[5]].couleur == this["p_mcPoisson_"+p_aListCase[6]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[5]].couleur == this["p_mcPoisson_"+p_aListCase[7]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[5]].couleur == this["p_mcPoisson_"+p_aListCase[8]].couleur) || 
				(this["p_mcPoisson_"+p_aListCase[5]].rayure == this["p_mcPoisson_"+p_aListCase[6]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[5]].rayure == this["p_mcPoisson_"+p_aListCase[7]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[5]].rayure == this["p_mcPoisson_"+p_aListCase[8]].rayure) || 
				(this["p_mcPoisson_"+p_aListCase[5]].direction == this["p_mcPoisson_"+p_aListCase[6]].direction && 
				 this["p_mcPoisson_"+p_aListCase[5]].direction == this["p_mcPoisson_"+p_aListCase[7]].direction && 
				 this["p_mcPoisson_"+p_aListCase[5]].direction == this["p_mcPoisson_"+p_aListCase[8]].direction)) {
				 return true;
			}	
		}
		if (p_aListCase[9] != 0 && p_aListCase[10] != 0 && p_aListCase[11] != 0 && p_aListCase[12] != 0) {
			if ((this["p_mcPoisson_"+p_aListCase[9]].taille == this["p_mcPoisson_"+p_aListCase[10]].taille && 
				 this["p_mcPoisson_"+p_aListCase[9]].taille == this["p_mcPoisson_"+p_aListCase[11]].taille && 
				 this["p_mcPoisson_"+p_aListCase[9]].taille == this["p_mcPoisson_"+p_aListCase[12]].taille) || 
				(this["p_mcPoisson_"+p_aListCase[9]].couleur == this["p_mcPoisson_"+p_aListCase[10]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[9]].couleur == this["p_mcPoisson_"+p_aListCase[11]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[9]].couleur == this["p_mcPoisson_"+p_aListCase[12]].couleur) || 
				(this["p_mcPoisson_"+p_aListCase[9]].rayure == this["p_mcPoisson_"+p_aListCase[10]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[9]].rayure == this["p_mcPoisson_"+p_aListCase[11]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[9]].rayure == this["p_mcPoisson_"+p_aListCase[12]].rayure) || 
				(this["p_mcPoisson_"+p_aListCase[9]].direction == this["p_mcPoisson_"+p_aListCase[10]].direction && 
				 this["p_mcPoisson_"+p_aListCase[9]].direction == this["p_mcPoisson_"+p_aListCase[11]].direction && 
				 this["p_mcPoisson_"+p_aListCase[9]].direction == this["p_mcPoisson_"+p_aListCase[12]].direction)) {
				 return true;
			}	
		}
		if (p_aListCase[13] != 0 && p_aListCase[14] != 0 && p_aListCase[15] != 0 && p_aListCase[16] != 0) {
			if ((this["p_mcPoisson_"+p_aListCase[13]].taille == this["p_mcPoisson_"+p_aListCase[14]].taille && 
				 this["p_mcPoisson_"+p_aListCase[13]].taille == this["p_mcPoisson_"+p_aListCase[15]].taille && 
				 this["p_mcPoisson_"+p_aListCase[13]].taille == this["p_mcPoisson_"+p_aListCase[16]].taille) || 
				(this["p_mcPoisson_"+p_aListCase[13]].couleur == this["p_mcPoisson_"+p_aListCase[14]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[13]].couleur == this["p_mcPoisson_"+p_aListCase[15]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[13]].couleur == this["p_mcPoisson_"+p_aListCase[16]].couleur) || 
				(this["p_mcPoisson_"+p_aListCase[13]].rayure == this["p_mcPoisson_"+p_aListCase[14]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[13]].rayure == this["p_mcPoisson_"+p_aListCase[15]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[13]].rayure == this["p_mcPoisson_"+p_aListCase[16]].rayure) || 
				(this["p_mcPoisson_"+p_aListCase[13]].direction == this["p_mcPoisson_"+p_aListCase[14]].direction && 
				 this["p_mcPoisson_"+p_aListCase[13]].direction == this["p_mcPoisson_"+p_aListCase[15]].direction && 
				 this["p_mcPoisson_"+p_aListCase[13]].direction == this["p_mcPoisson_"+p_aListCase[16]].direction)) {
				 return true;
			}	
		}
		// Vérification verticale
		if (p_aListCase[1] != 0 && p_aListCase[5] != 0 && p_aListCase[9] != 0 && p_aListCase[13] != 0) {
			if ((this["p_mcPoisson_"+p_aListCase[1]].taille == this["p_mcPoisson_"+p_aListCase[5]].taille && 
				 this["p_mcPoisson_"+p_aListCase[1]].taille == this["p_mcPoisson_"+p_aListCase[9]].taille && 
				 this["p_mcPoisson_"+p_aListCase[1]].taille == this["p_mcPoisson_"+p_aListCase[13]].taille) || 
				(this["p_mcPoisson_"+p_aListCase[1]].couleur == this["p_mcPoisson_"+p_aListCase[5]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[1]].couleur == this["p_mcPoisson_"+p_aListCase[9]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[1]].couleur == this["p_mcPoisson_"+p_aListCase[13]].couleur) || 
				(this["p_mcPoisson_"+p_aListCase[1]].rayure == this["p_mcPoisson_"+p_aListCase[5]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[1]].rayure == this["p_mcPoisson_"+p_aListCase[9]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[1]].rayure == this["p_mcPoisson_"+p_aListCase[13]].rayure) || 
				(this["p_mcPoisson_"+p_aListCase[1]].direction == this["p_mcPoisson_"+p_aListCase[5]].direction && 
				 this["p_mcPoisson_"+p_aListCase[1]].direction == this["p_mcPoisson_"+p_aListCase[9]].direction && 
				 this["p_mcPoisson_"+p_aListCase[1]].direction == this["p_mcPoisson_"+p_aListCase[13]].direction)) {
				 return true;
			}	
		}
		if (p_aListCase[2] != 0 && p_aListCase[6] != 0 && p_aListCase[10] != 0 && p_aListCase[14] != 0) {
			if ((this["p_mcPoisson_"+p_aListCase[2]].taille == this["p_mcPoisson_"+p_aListCase[6]].taille && 
				 this["p_mcPoisson_"+p_aListCase[2]].taille == this["p_mcPoisson_"+p_aListCase[10]].taille && 
				 this["p_mcPoisson_"+p_aListCase[2]].taille == this["p_mcPoisson_"+p_aListCase[14]].taille) || 
				(this["p_mcPoisson_"+p_aListCase[2]].couleur == this["p_mcPoisson_"+p_aListCase[6]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[2]].couleur == this["p_mcPoisson_"+p_aListCase[10]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[2]].couleur == this["p_mcPoisson_"+p_aListCase[14]].couleur) || 
				(this["p_mcPoisson_"+p_aListCase[2]].rayure == this["p_mcPoisson_"+p_aListCase[6]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[2]].rayure == this["p_mcPoisson_"+p_aListCase[10]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[2]].rayure == this["p_mcPoisson_"+p_aListCase[14]].rayure) || 
				(this["p_mcPoisson_"+p_aListCase[2]].direction == this["p_mcPoisson_"+p_aListCase[6]].direction && 
				 this["p_mcPoisson_"+p_aListCase[2]].direction == this["p_mcPoisson_"+p_aListCase[10]].direction && 
				 this["p_mcPoisson_"+p_aListCase[2]].direction == this["p_mcPoisson_"+p_aListCase[14]].direction)) {
				 return true;
			}	
		}
		if (p_aListCase[3] != 0 && p_aListCase[7] != 0 && p_aListCase[11] != 0 && p_aListCase[15] != 0) {
			if ((this["p_mcPoisson_"+p_aListCase[3]].taille == this["p_mcPoisson_"+p_aListCase[7]].taille && 
				 this["p_mcPoisson_"+p_aListCase[3]].taille == this["p_mcPoisson_"+p_aListCase[11]].taille && 
				 this["p_mcPoisson_"+p_aListCase[3]].taille == this["p_mcPoisson_"+p_aListCase[15]].taille) || 
				(this["p_mcPoisson_"+p_aListCase[3]].couleur == this["p_mcPoisson_"+p_aListCase[7]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[3]].couleur == this["p_mcPoisson_"+p_aListCase[11]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[3]].couleur == this["p_mcPoisson_"+p_aListCase[15]].couleur) || 
				(this["p_mcPoisson_"+p_aListCase[3]].rayure == this["p_mcPoisson_"+p_aListCase[7]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[3]].rayure == this["p_mcPoisson_"+p_aListCase[11]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[3]].rayure == this["p_mcPoisson_"+p_aListCase[15]].rayure) || 
				(this["p_mcPoisson_"+p_aListCase[3]].direction == this["p_mcPoisson_"+p_aListCase[7]].direction && 
				 this["p_mcPoisson_"+p_aListCase[3]].direction == this["p_mcPoisson_"+p_aListCase[11]].direction && 
				 this["p_mcPoisson_"+p_aListCase[3]].direction == this["p_mcPoisson_"+p_aListCase[15]].direction)) {
				 return true;
			}	
		}
		if (p_aListCase[4] != 0 && p_aListCase[8] != 0 && p_aListCase[12] != 0 && p_aListCase[16] != 0) {
			if ((this["p_mcPoisson_"+p_aListCase[4]].taille == this["p_mcPoisson_"+p_aListCase[8]].taille && 
				 this["p_mcPoisson_"+p_aListCase[4]].taille == this["p_mcPoisson_"+p_aListCase[12]].taille && 
				 this["p_mcPoisson_"+p_aListCase[4]].taille == this["p_mcPoisson_"+p_aListCase[16]].taille) || 
				(this["p_mcPoisson_"+p_aListCase[4]].couleur == this["p_mcPoisson_"+p_aListCase[8]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[4]].couleur == this["p_mcPoisson_"+p_aListCase[12]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[4]].couleur == this["p_mcPoisson_"+p_aListCase[16]].couleur) || 
				(this["p_mcPoisson_"+p_aListCase[4]].rayure == this["p_mcPoisson_"+p_aListCase[8]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[4]].rayure == this["p_mcPoisson_"+p_aListCase[12]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[4]].rayure == this["p_mcPoisson_"+p_aListCase[16]].rayure) || 
				(this["p_mcPoisson_"+p_aListCase[4]].direction == this["p_mcPoisson_"+p_aListCase[8]].direction && 
				 this["p_mcPoisson_"+p_aListCase[4]].direction == this["p_mcPoisson_"+p_aListCase[12]].direction && 
				 this["p_mcPoisson_"+p_aListCase[4]].direction == this["p_mcPoisson_"+p_aListCase[16]].direction)) {
				 return true;
			}	
		}
		// Vérification diagonale
		if (p_aListCase[1] != 0 && p_aListCase[6] != 0 && p_aListCase[11] != 0 && p_aListCase[16] != 0) {
			if ((this["p_mcPoisson_"+p_aListCase[1]].taille == this["p_mcPoisson_"+p_aListCase[6]].taille && 
				 this["p_mcPoisson_"+p_aListCase[1]].taille == this["p_mcPoisson_"+p_aListCase[11]].taille && 
				 this["p_mcPoisson_"+p_aListCase[1]].taille == this["p_mcPoisson_"+p_aListCase[16]].taille) || 
				(this["p_mcPoisson_"+p_aListCase[1]].couleur == this["p_mcPoisson_"+p_aListCase[6]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[1]].couleur == this["p_mcPoisson_"+p_aListCase[11]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[1]].couleur == this["p_mcPoisson_"+p_aListCase[16]].couleur) || 
				(this["p_mcPoisson_"+p_aListCase[1]].rayure == this["p_mcPoisson_"+p_aListCase[6]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[1]].rayure == this["p_mcPoisson_"+p_aListCase[11]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[1]].rayure == this["p_mcPoisson_"+p_aListCase[16]].rayure) || 
				(this["p_mcPoisson_"+p_aListCase[1]].direction == this["p_mcPoisson_"+p_aListCase[6]].direction && 
				 this["p_mcPoisson_"+p_aListCase[1]].direction == this["p_mcPoisson_"+p_aListCase[11]].direction && 
				 this["p_mcPoisson_"+p_aListCase[1]].direction == this["p_mcPoisson_"+p_aListCase[16]].direction)) {
				 return true;
			}	
		}
		if (p_aListCase[4] != 0 && p_aListCase[7] != 0 && p_aListCase[10] != 0 && p_aListCase[13] != 0) {
			if ((this["p_mcPoisson_"+p_aListCase[4]].taille == this["p_mcPoisson_"+p_aListCase[7]].taille && 
				 this["p_mcPoisson_"+p_aListCase[4]].taille == this["p_mcPoisson_"+p_aListCase[10]].taille && 
				 this["p_mcPoisson_"+p_aListCase[4]].taille == this["p_mcPoisson_"+p_aListCase[13]].taille) || 
				(this["p_mcPoisson_"+p_aListCase[4]].couleur == this["p_mcPoisson_"+p_aListCase[7]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[4]].couleur == this["p_mcPoisson_"+p_aListCase[10]].couleur && 
				 this["p_mcPoisson_"+p_aListCase[4]].couleur == this["p_mcPoisson_"+p_aListCase[13]].couleur) || 
				(this["p_mcPoisson_"+p_aListCase[4]].rayure == this["p_mcPoisson_"+p_aListCase[7]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[4]].rayure == this["p_mcPoisson_"+p_aListCase[10]].rayure && 
				 this["p_mcPoisson_"+p_aListCase[4]].rayure == this["p_mcPoisson_"+p_aListCase[13]].rayure) || 
				(this["p_mcPoisson_"+p_aListCase[4]].direction == this["p_mcPoisson_"+p_aListCase[7]].direction && 
				 this["p_mcPoisson_"+p_aListCase[4]].direction == this["p_mcPoisson_"+p_aListCase[10]].direction && 
				 this["p_mcPoisson_"+p_aListCase[4]].direction == this["p_mcPoisson_"+p_aListCase[13]].direction)) {
				 return true;
			}	
		}
		return false;
	}	
	
	/** Placement des poissons à droite et à gauche */
	private function placementPoisson() {
		// On place les poissons
		for (var i:Number = 1 ; i <= 16 ; ++i) {
			this.attachMovie("poisson"+i, "p_mcPoisson_"+i, this.getNextHighestDepth(), {_x:this["p_mcLoc"+this.p_aListElem[(i - 1)]]._x, _y:this["p_mcLoc"+this.p_aListElem[(i - 1)]]._y});
			this["p_mcPoisson_"+i].id = i;
		}
	}
	
	/** On fixe les caracteristiques des poissons */
	private function initPoisson() {
		// taille 
		this.p_mcPoisson_1.taille = "petit";
		this.p_mcPoisson_2.taille = "petit";
		this.p_mcPoisson_3.taille = "petit";
		this.p_mcPoisson_4.taille = "petit";
		this.p_mcPoisson_5.taille = "petit";
		this.p_mcPoisson_6.taille = "petit";
		this.p_mcPoisson_7.taille = "petit";
		this.p_mcPoisson_8.taille = "petit";
		this.p_mcPoisson_9.taille = "gros";
		this.p_mcPoisson_10.taille = "gros";
		this.p_mcPoisson_11.taille = "gros";
		this.p_mcPoisson_12.taille = "gros";
		this.p_mcPoisson_13.taille = "gros";
		this.p_mcPoisson_14.taille = "gros";
		this.p_mcPoisson_15.taille = "gros";
		this.p_mcPoisson_16.taille = "gros";
		
		// couleur
		this.p_mcPoisson_1.couleur = "vert";
		this.p_mcPoisson_2.couleur = "vert";
		this.p_mcPoisson_3.couleur = "rouge";
		this.p_mcPoisson_4.couleur = "rouge";
		this.p_mcPoisson_5.couleur = "rouge";
		this.p_mcPoisson_6.couleur = "rouge";
		this.p_mcPoisson_7.couleur = "vert";
		this.p_mcPoisson_8.couleur = "vert";
		this.p_mcPoisson_9.couleur = "vert";
		this.p_mcPoisson_10.couleur = "vert";
		this.p_mcPoisson_11.couleur = "rouge";
		this.p_mcPoisson_12.couleur = "rouge";
		this.p_mcPoisson_13.couleur = "rouge";
		this.p_mcPoisson_14.couleur = "rouge";
		this.p_mcPoisson_15.couleur = "vert";
		this.p_mcPoisson_16.couleur = "vert";
		
		// Rayure
		this.p_mcPoisson_1.rayure = "non";
		this.p_mcPoisson_2.rayure = "non";
		this.p_mcPoisson_3.rayure = "non";
		this.p_mcPoisson_4.rayure = "non";
		this.p_mcPoisson_5.rayure = "oui";
		this.p_mcPoisson_6.rayure = "oui";
		this.p_mcPoisson_7.rayure = "oui";
		this.p_mcPoisson_8.rayure = "oui";
		this.p_mcPoisson_9.rayure = "non";
		this.p_mcPoisson_10.rayure = "non";
		this.p_mcPoisson_11.rayure = "non";
		this.p_mcPoisson_12.rayure = "non";
		this.p_mcPoisson_13.rayure = "oui";
		this.p_mcPoisson_14.rayure = "oui";
		this.p_mcPoisson_15.rayure = "oui";
		this.p_mcPoisson_16.rayure = "oui";
		
		// Direction
		this.p_mcPoisson_1.direction = "gauche";
		this.p_mcPoisson_2.direction = "droite";
		this.p_mcPoisson_3.direction = "gauche";
		this.p_mcPoisson_4.direction = "droite";
		this.p_mcPoisson_5.direction = "gauche";
		this.p_mcPoisson_6.direction = "droite";
		this.p_mcPoisson_7.direction = "gauche";
		this.p_mcPoisson_8.direction = "droite";
		this.p_mcPoisson_9.direction = "gauche";
		this.p_mcPoisson_10.direction = "droite";
		this.p_mcPoisson_11.direction = "gauche";
		this.p_mcPoisson_12.direction = "droite";
		this.p_mcPoisson_13.direction = "gauche";
		this.p_mcPoisson_14.direction = "droite";
		this.p_mcPoisson_15.direction = "gauche";
		this.p_mcPoisson_16.direction = "droite";		
	}
	
	/** Fin du jeu */
	private function endGame() {
		for (var i:Number = 1 ; i <= 16 ; ++i) {
			this["p_mcPoisson_"+i].removeMovieClip();
		}
		
		if(this.p_nTour == 1) {
			this._parent._parent.setWinGame(11, this._parent._parent.joueur1);  
		} else {
			this._parent._parent.setWinGame(11, this._parent._parent.joueur1); 
		}
		_parent._parent.barreInterface_mc.showParchemin();
		this.gotoAndStop("fin");
		gameFinished()
	}
	
}	

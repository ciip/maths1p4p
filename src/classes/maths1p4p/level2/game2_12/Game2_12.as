﻿/**
 * class com.maths1p4p.Game2_12
 * 
 * @author Yoann Augen
 * @version 1.0
 *
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level2.game2_12.Game2_12 extends maths1p4p.AbstractGame {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 12;
	/** Les bateaux */
	public var p_mcBateau:MovieClip;
	
	/** L'image de transition */
	public var p_mcTransition:MovieClip;
	
	/** Les lettres apparaissant sur les bateaux */
	public var p_mcLettre:MovieClip;
	
	/** Le chiffre à rentrer */
	public var p_mcChiffreActu:MovieClip;
	
	public var p_mcChiffreFixe1:MovieClip;
	public var p_mcChiffreFixe2:MovieClip;
	public var p_mcChiffreFixe3:MovieClip;
	public var p_mcChiffreFixe4:MovieClip;
	public var p_mcChiffreFixe5:MovieClip;
	public var p_mcChiffreFixe6:MovieClip;
	public var p_mcChiffreFixe7:MovieClip;
	public var p_mcChiffreFixe8:MovieClip;
	public var p_mcChiffreFixe9:MovieClip;
	public var p_mcChiffreFixe10:MovieClip;
	
	
	/** Bouton de validation */
	public var p_mcNext:MovieClip;
	
	/** Les point du joueur */
	public var p_mcPoint:MovieClip;
	
	/** Le parchemin final */
	public var p_mcParchemin:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** Les point du joueur */
	private var p_nPoint:Number;
	/** La ligne ou on se situe */
	private var p_nLigne:Number;
	
	/** La solution à rentrer */
	private var p_nSoluce:Number;
	
	
	/** Definition des valeurs possibles */
	private var p_aListValues:Array = new Array(  
									new Array("un", "1"), 
									new Array("deux", "2"), 
									new Array("trois", "3"), 
								    new Array("quatre", "4"), 
									new Array("cinq", "5"), 
									new Array("six", "6"), 
									new Array("sept", "7"),
									new Array("huit", "8"),
									new Array("neuf", "9"),
									new Array("dix", "10"), 
									new Array("onze", "11"), 
									new Array("douze", "12"), 
									new Array("treize", "13"), 
								    new Array("quatorze", "14"), 
									new Array("quinze", "15"), 
									new Array("seize", "16"), 
									new Array("dix-sept", "17"),
									new Array("dix-huit", "18"),
									new Array("dix-neuf", "19"),
									new Array("vingt", "20"), 
									new Array("vingt et un", "21"), 
									new Array("vingt-deux", "22"), 
									new Array("vingt-trois", "23"), 
								    new Array("vingt-quatre", "24"), 
									new Array("vingt-cinq", "25"), 
									new Array("vingt-six", "26"), 
									new Array("vingt-sept", "27"),
									new Array("vingt-huit", "28"),
									new Array("vingt-neuf", "29"),
									new Array("trente", "30"), 
									new Array("trente et un", "31"), 
									new Array("trente-deux", "32"), 
									new Array("trente-trois", "33"), 
								    new Array("trente-quatre", "34"), 
									new Array("trente-cinq", "35"), 
									new Array("trente-six", "36"), 
									new Array("trente-sept", "37"),
									new Array("trente-huit", "38"),
									new Array("trente-neuf", "39"),
									new Array("quarante", "40"), 
									new Array("quarante et un", "41"), 
									new Array("quarante-deux", "42"), 
									new Array("quarante-trois", "43"), 
								    new Array("quarante-quatre", "44"), 
									new Array("quarante-cinq", "45"), 
									new Array("quarante-six", "46"), 
									new Array("quarante-sept", "47"),
									new Array("quarante-huit", "48"),
									new Array("quarante-neuf", "49"),
									new Array("cinquante", "50"), 
									new Array("cinquante et un", "51"), 
									new Array("cinquante-deux", "52"), 
									new Array("cinquante-trois", "53"), 
								    new Array("cinquante-quatre", "54"), 
									new Array("cinquante-cinq", "55"), 
									new Array("cinquante-six", "56"), 
									new Array("cinquante-sept", "57"),
									new Array("cinquante-huit", "58"),
									new Array("cinquante-neuf", "59"),
									new Array("soixante", "60"), 
									new Array("soixante et un", "61"), 
									new Array("soixante-deux", "62"), 
									new Array("soixante-trois", "63"), 
								    new Array("soixante-quatre", "64"), 
									new Array("soixante-cinq", "65"), 
									new Array("soixante-six", "66"), 
									new Array("soixante-sept", "67"),
									new Array("soixante-huit", "68"),
									new Array("soixante-neuf", "69"),
									new Array("septante", "70"), 
									new Array("septante et un", "71"), 
									new Array("septante-deux", "72"), 
									new Array("septante-trois", "73"), 
								    new Array("septante-quatre", "74"), 
									new Array("septante-cinq", "75"), 
									new Array("septante-six", "76"), 
									new Array("septante-sept", "77"),
									new Array("septante-huit", "78"),
									new Array("septante-neuf", "79"),
									new Array("huitante", "80"), 
									new Array("huitante et un", "81"), 
									new Array("huitante-deux", "82"), 
									new Array("huitante-trois", "83"), 
								    new Array("huitante-quatre", "84"), 
									new Array("huitante-cinq", "85"), 
									new Array("huitante-six", "86"), 
									new Array("huitante-sept", "87"),
									new Array("huitante-huit", "88"),
									new Array("huitante-neuf", "89"),
									new Array("nonante", "90"), 
									new Array("nonante et un", "91"), 
									new Array("nonante-deux", "92"), 
									new Array("nonante-trois", "93"), 
								    new Array("nonante-quatre", "94"), 
									new Array("nonante-cinq", "95"), 
									new Array("nonante-six", "96"), 
									new Array("nonante-sept", "97"),
									new Array("nonante-huit", "98"),
									new Array("nonante-neuf", "99"),
									new Array("cent", "100")									
									); 
	
	/** L'indice de la valeur a tirer */
	private var p_nIndice:Number;
		
	/** L'indice de la valeur a tirer */
	private var p_bLost:Boolean;
		
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game2_12() {
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		this.p_nPoint = 0;
		this.p_nLigne = 1;
		this.p_aListValues = randomTab(this.p_aListValues);
		this.p_nIndice = 0;
		this.p_bLost = false;
	}
	
	/**
	* Fonction qui renvoi un nombre aleatoire entre min et max
	* @param Number min
	* @param Number max
	* @return Number
	* 
	*/
	public function randRange(min:Number, max:Number) {
	     var randomNum:Number = Math.floor(Math.random() * (max - min + 1)) + min;
	     return randomNum;
	}
	
	/**
	* Fonction qui renvoi le tableau mélangé
	* @param Array tab0_array le tableau d'origine
	* @return Array
	*/
	public function randomTab(tab0_array:Array) {
	    var tab1_array:Array = new Array();
	    var tabTmp_array:Array = new Array();
	    var cpt:Number = 0;
	    for(var i:Number = 0 ; i < tab0_array.length ; i++) {
	        tabTmp_array.push(false);
	    }
	    while(cpt < tab0_array.length) {
	        var j:Number = randRange(0, tab0_array.length-1);
	        if(tabTmp_array[j] == false) {
	            tabTmp_array[j] = true;
	            tab1_array.push(tab0_array[j]);
	            cpt++;
	        }
	    }
	    return tab1_array;
	}
	
	/** 
	 * Nous dit si la valeur needle est présente dans un tableau haystack
	 */
	public function inArray(needle, haystack) {
		for (var i:Number = 0 ; i < haystack.length ; i++) {
			if (haystack[i] == needle) {
				return true;
			}
		}
		return false;
	}
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/** Initialisation de l'interface de jeu */
	private function initInterface() {
		
		// On affiche un chiffre ecrit et sur un bateau
		this.displayNumber();
		
		// On donne le focus au champs de texte
		Selection.setFocus(this.p_mcChiffreActu.txtValue); // <= NE MARCHE PAS
		//le champ n'accepte que des chiffres
		this.p_mcChiffreActu.txtValue.restrict = "0-9"; 
		//le champ n'accepte que 3 chiffres
		this.p_mcChiffreActu.txtValue.maxChars = 3;
		
		// Deux facons de valider le nombre
		// Touche entrée
		Key.addListener(this.p_mcChiffreActu);
		this.p_mcChiffreActu.onKeyDown = function() {
			// il à appuyé sur entrée, on valide le chiffre
			if (Key.getCode() == Key.ENTER && this.txtValue.text != "") {
				_parent.soundPlayer.playASound("minitic");
				_parent.controleNombre(this.txtValue.text);
			}						
		}
		// Ou validation du bouton 
		this.p_mcNext.onPress = function () {
			if (_parent.p_mcChiffreActu.txtValue.text != "") {
				_parent.soundPlayer.playASound("minitic");
				_parent.controleNombre(_parent.p_mcChiffreActu.txtValue.text);
			}
		}
	}
	
	/** Affiche un nombre en lettre sur un bateau */
	private function displayNumber() {
		// On tire un bateau au hazard 
		//	this.p_mcBateau.gotoAndStop(randRange(1, 4));
		//this.p_mcTransition.gotoAndPlay(1);
		// On tire une valeur 
		p_mcLettre.txtValue.text = this.p_aListValues[this.p_nIndice][0];
		this.p_nSoluce = this.p_aListValues[this.p_nIndice][1];
		++this.p_nIndice;
		if (this.p_nIndice >= this.p_aListValues.length) {
			this.p_nIndice = 0;
		}
	}
	
	/** Verification du chiffre */
	private function controleNombre(nVal:String) {
		//trace (this.p_nSoluce +" == "+Number(nVal));
		if (this.p_nSoluce == Number(nVal)) {
			// Si le résultat est correct, on ajoute un point
			++this.p_nPoint;
			this.p_mcPoint.gotoAndStop(this.p_nPoint + 1);
			this.p_nSoluce = null;
			// On avance Seulement s'il n'a pas perdu un point avant
			if (this.p_nPoint == this.p_nLigne) {
				// On fixe avec un texte en dur la valeur précédente
				attachMovie("chiffresfixes", "p_mcChiffreFixe"+this.p_nPoint, this.getNextHighestDepth(), {_x:this.p_mcChiffreActu._x, _y:(this.p_mcChiffreActu._y)});
				this["p_mcChiffreFixe"+this.p_nPoint].txtValue.text = this.p_mcChiffreActu.txtValue.text;
				// On supprime le champs joueurs du dernier nombre
				this.p_mcChiffreActu._y += 25;
				++this.p_nLigne;
			} else {
				// on efface simplement le champs en cours
				Selection.setFocus(this.p_mcChiffreActu.txtValue);
			}
			this.p_mcChiffreActu.txtValue.text = "";
			
			p_bLost = false;	
			
			// On affiche la prochaine valeur ou la victoire
			if (p_nPoint == 9) {
				this.endGame();				
			} else {
				this.initInterface();
			}
		} else {
			// S'il s'est trompé il perd un point (qu'un seule fois)
			if (this.p_nPoint > 0 && this.p_bLost == false) {
				--this.p_nPoint;
			}
			p_bLost = true;
			Selection.setFocus(this.p_mcChiffreActu.txtValue);
			this.p_mcPoint.gotoAndStop(this.p_nPoint + 1);
		}
	}
	
	/** Fin du jeu */
	private function endGame() {
		soundPlayer.playASound("frele2");
		for (var i:Number = 1 ; i <= 10 ; ++i) {
			this["p_mcChiffreFixe"+i].removeMovieClip();
		}
		this.gotoAndStop("fin");
		this.p_mcParchemin.onPress = function () {
			this._parent._parent._parent.barreInterface_mc.showParchemin(); 
			_parent.soundPlayer.playASound("shark2");
			  
			this._parent._parent._parent.setWinGame(12, this._parent._parent._parent.joueur1);
			_parent.gotoAndStop("fin2"); 
			_parent.gameFinished()
		}
	}
}	

﻿/**
 * class maths1p4p.level2.game2_13.game2_13
 * 
 * @author Rémi Gay
 * @version 1.0
 *
 * ETAT : terminé
 *
 * Copyright (c) 2008 Rémi Gay.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import com.prossel.utils.CursorManager;
//import maths1p4p.level2.game2_13.*
import mx.controls.Alert;
import maths1p4p.utils.OutilsTableaux;
import maths1p4p.level2.game2_13.Tuyau;
import maths1p4p.application.Application;

class maths1p4p.level2.game2_13.Game2_13 extends maths1p4p.AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 13;
	

	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/***** variables en rapport avec les éléments déjà present sur la scène *****/
	private var btFini_mc:MovieClip;
	private var parchemin_mc:MovieClip;
	private var zonePorte_mc:MovieClip;
	private var annuler_mc:MovieClip;
	private var pivoter1_mc:MovieClip;
	private var pivoter2_mc:MovieClip;
	private var pivoter3_mc:MovieClip;
	private var pivoter4_mc:MovieClip;
	private var spot1_mc:MovieClip;
	private var spot2_mc:MovieClip;
	private var spot3_mc:MovieClip;
	private var contourNvoTuyau1_mc:MovieClip;
	private var contourNvoTuyau2_mc:MovieClip;
	private var contourNvoTuyau3_mc:MovieClip;
	private var contourNvoTuyau4_mc:MovieClip;
		

	//future variable contenant le clip remplacant la souris (lors du survol de la porte
	private var flecheCurseur:MovieClip;

		
	private var boutonRetour_mc:MovieClip;
	private var plus1_mc:MovieClip;
	private var moins1_mc:MovieClip;
	private var nbreTuyau1_txt:TextField;
	private var plus2_mc:MovieClip;
	private var moins2_mc:MovieClip;
	private var nbreTuyau2_txt:TextField;
	private var plus3_mc:MovieClip;
	private var moins3_mc:MovieClip;
	private var nbreTuyau3_txt:TextField;
	private var plus4_mc:MovieClip;
	private var moins4_mc:MovieClip;
	private var nbreTuyau4_txt:TextField;

	/***** variables privées de la classe *****/
	//variable precisant le nbre de fois où le joueur est rentré dans la piece de choix des tuyaux
	//car si il y va plus d'une fois pour une manche il a perdu
	private var nbreVisitePieceTuyaux:Number;
	//determine le tuyau selectionné actuellement
	private var tuyauSelectionne:Tuyau;
	//pareil mais c'est le tuyau selectionné precedemment (sert au retour arriere)
	private var tuyauSelectionnePrec:Tuyau;
	//memorisation de la place du tuyau remplacant dans la liste des nouveauTuyau (pour le remettre à la bonne place
	private var placeNouveauxTuyauxPrec:Number;
	//tableau du parcours tuyau en 2 dimensions contenant des cases vides ou avec un tuyau
	private var tabParcours:Array;
	//C'est une copie du 1er tableau juste avant le remplacement d'un tuyau, sert au retour arrière
	private var tabParcoursPrec:Array;
	//tableau de la solution du parcours => servira a savoir si le joueur a trouvé la solution ou non
	private var tabParcoursSolution:Array;
	//designe le tuyau remplacé par le tuyau selectionné (sert pour le retour arriere)
	private var tuyauRemplacePrec:Tuyau;
	//tableau représentant les 4 tuyaux selectionnés dans la piece du fond
	private var tabNouveauxTuyaux:Array;
	
	//tableau enregistrant les parcours deja fait au cours de la partie courante (pour pas que le joueur
	//refasse 2 fois le même parcours
	private var tabListeParcoursEffectues:Array;
	
	private var score:Number;

	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * s'occupe de l'initialisation graphique et l'attribution des evenements
	 */
	public function Game2_13()	{
		
		super();
		
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		this.score = 0;
		this.tabListeParcoursEffectues = [0,1,2,3,4,5,6,7,8,9];
		//au debut du jeu le bouton "J'ai fini" est desactivé
		this.btFini_mc.enabled = false;
		this.nouvelleDonne();

	}
	
	/**
	 * Fonction appelée à chaque nouvelle manche (création d'un nouveau parcours et réinitialisation des 
	 * nouveaux tuyaux
	 * @return rien
	 */
	public function nouvelleDonne() {
		
		Tuyau.viderTabTuyaux();
		
		this.nbreVisitePieceTuyaux = 0;
		this.tuyauSelectionne = null;
		this.tuyauSelectionnePrec = null;
		this.tuyauRemplacePrec = null;
		
		this.tabParcours = new Array();
		//choisir un parcours au hasard
		//=> si le joueur a deja effectué les 10 parcours sans avoir fini on reconstruit le tableau pour qu'il
		//refasse un des 10 parcours
		if(this.tabListeParcoursEffectues.length == 0)
		{
			this.tabListeParcoursEffectues= [0,1,2,3,4,5,6,7,8,9];
		}
		this.tabListeParcoursEffectues = OutilsTableaux.shuffle(this.tabListeParcoursEffectues);
		var numeroParcours = this.tabListeParcoursEffectues.pop();
		//pour linstant je force le chiffre a 0 => a enlever ensuite
		//numeroParcours = 0;
		switch(numeroParcours) {
			
			case 0:

				var tuyau01 = this.creationTuyau(1, "Tournant", 0);
				var tuyau02 = this.creationTuyau(2, "Droit", 0);
				var tuyau03 = this.creationTuyau(3, "Droit", 1);
				var tuyau04 = this.creationTuyau(4, "Droit", 0);
				var tuyau05 = this.creationTuyau(5, "Pont", 1);
				var tuyau06 = this.creationTuyau(6, "Pont", 1);
				var tuyau07 = this.creationTuyau(7, "Tournant", 1);
				var tuyau08 = this.creationTuyau(8, "Pont", 0);
				var tuyau09 = this.creationTuyau(9, "Droit", 0);
				var tuyau10 = this.creationTuyau(10, "Droit", 0);
				var tuyau11 = this.creationTuyau(11, "Droit", 0);
				var tuyau12 = this.creationTuyau(12, "Croix", 0);
				var tuyau13 = this.creationTuyau(13, "Tournant", 1);
				var tuyau14 = this.creationTuyau(14, "Droit", 0);
				var tuyau15 = this.creationTuyau(15, "Pont", 2);
				var tuyau16 = this.creationTuyau(16, "Droit", 1);
				var tuyau17 = this.creationTuyau(17, "Droit", 1);
				var tuyau18 = this.creationTuyau(18, "Pont", 0);
				var tuyau19 = this.creationTuyau(19, "Pont", 3);
				var tuyau20 = this.creationTuyau(20, "Droit", 1);
				var tuyau21 = this.creationTuyau(21, "Droit", 1);
				var tuyau22 = this.creationTuyau(22, "Pont", 0);
				var tuyau23 = this.creationTuyau(23, "Tournant", 2);
				var tuyau24 = this.creationTuyau(24, "Tournant", 3);
				var tuyau25 = this.creationTuyau(25, "Tournant", 1);
				var tuyau26 = this.creationTuyau(26, "Pont", 0);
				var tuyau27 = this.creationTuyau(27, "Pont", 2);
				var tuyau28 = this.creationTuyau(28, "Tournant", 3);
				var tuyau29 = this.creationTuyau(29, "Pont", 3);
				var tuyau30 = this.creationTuyau(30, "Tournant", 2);
				this.tabParcours = [ [null, tuyau01, tuyau02, null, null, null, null],
									 [null, tuyau03, tuyau04, tuyau05, tuyau06, tuyau07, null],
									 [null, tuyau08, tuyau09, tuyau10, tuyau11, tuyau12, tuyau13],
									 [tuyau14, tuyau15, null, tuyau16, tuyau17, tuyau18, tuyau19],
									 [null, tuyau20, null, tuyau21, tuyau22, tuyau23, null],
									 [null, tuyau24, tuyau25, tuyau26, tuyau27, null, null],
									 [null, null, tuyau28, tuyau29, tuyau30, null, null]
								   ];
				break;
				
			case 1:
			
				var tuyau01 = this.creationTuyau(1, "Tournant", 0);
				var tuyau02 = this.creationTuyau(2, "Droit", 0);
				var tuyau03 = this.creationTuyau(3, "Pont", 1);
				var tuyau04 = this.creationTuyau(4, "Droit", 0);
				var tuyau05 = this.creationTuyau(5, "Tournant", 1);				
				var tuyau06 = this.creationTuyau(6, "Droit", 0);
				var tuyau07 = this.creationTuyau(7, "Tournant", 2);
				var tuyau08 = this.creationTuyau(8, "Droit", 1);
				var tuyau09 = this.creationTuyau(9, "Pont", 0);
				var tuyau10 = this.creationTuyau(10, "Tournant", 1);				
				var tuyau11 = this.creationTuyau(11, "Droit", 0);
				var tuyau12 = this.creationTuyau(12, "Droit", 0);
				var tuyau13 = this.creationTuyau(13, "Droit", 0);
				var tuyau14 = this.creationTuyau(14, "Tournant", 3);
				var tuyau15 = this.creationTuyau(15, "Droit", 0);
				var tuyau16 = this.creationTuyau(16, "Droit", 0);
				var tuyau17 = this.creationTuyau(17, "Croix", 0);				
				var tuyau18 = this.creationTuyau(18, "Tournant", 3);
				var tuyau19 = this.creationTuyau(19, "Tournant", 1);
				var tuyau20 = this.creationTuyau(20, "Tournant", 0);
				var tuyau21 = this.creationTuyau(21, "Tournant", 2);
				var tuyau22 = this.creationTuyau(22, "Tournant", 3);
				var tuyau23 = this.creationTuyau(23, "Droit", 0);
				var tuyau24 = this.creationTuyau(24, "Droit", 0);
				var tuyau25 = this.creationTuyau(25, "Droit", 0);
				var tuyau26 = this.creationTuyau(26, "Tournant", 2);
				this.tabParcours = [ [null, null, null, null, null, null, null],
									 [null, tuyau01, tuyau02, tuyau03, tuyau04, tuyau05, null],
									 [tuyau06, tuyau07, null, tuyau08, null, tuyau09, tuyau10],
									 [tuyau11, tuyau12, tuyau13, tuyau14, tuyau15, tuyau16, tuyau17],
									 [tuyau18, tuyau19, null, null, null, tuyau20, tuyau21],
									 [null, tuyau22, tuyau23, tuyau24, tuyau25, tuyau26, null],
									 [null, null, null, null, null, null, null]
								   ];
				break;
			
			case 2:
				var tuyau01 = this.creationTuyau(1, "Tournant", 0);
				var tuyau02 = this.creationTuyau(2, "Droit", 0);
				var tuyau03 = this.creationTuyau(3, "Droit", 0);
				var tuyau04 = this.creationTuyau(4, "Tournant", 1);				
				var tuyau05 = this.creationTuyau(5, "Tournant", 0);
				var tuyau06 = this.creationTuyau(6, "Tournant", 2);
				var tuyau07 = this.creationTuyau(7, "Tournant", 0);
				var tuyau08 = this.creationTuyau(8, "Tournant", 1);
				var tuyau09 = this.creationTuyau(9, "Droit", 1);				
				var tuyau10 = this.creationTuyau(10, "Droit", 1);
				var tuyau11 = this.creationTuyau(11, "Droit", 1);
				var tuyau12 = this.creationTuyau(12, "Croix", 0);
				var tuyau13 = this.creationTuyau(13, "Droit", 1);				
				var tuyau14 = this.creationTuyau(14, "Tournant", 1);
				var tuyau15 = this.creationTuyau(15, "Droit", 1);
				var tuyau16 = this.creationTuyau(16, "Droit", 1);
				var tuyau17 = this.creationTuyau(17, "Droit", 1);
				var tuyau18 = this.creationTuyau(18, "Pont", 0);
				var tuyau19 = this.creationTuyau(19, "Droit", 0);				
				var tuyau20 = this.creationTuyau(20, "Tournant", 3);
				var tuyau21 = this.creationTuyau(21, "Pont", 3);
				var tuyau22 = this.creationTuyau(22, "Tournant", 1);
				var tuyau23 = this.creationTuyau(23, "Tournant", 2);
				var tuyau24 = this.creationTuyau(24, "Pont", 0);
				var tuyau25 = this.creationTuyau(25, "Droit", 1);				
				this.tabParcours = [ [null, null, tuyau01, tuyau02, tuyau03, tuyau04, null],
									 [null, tuyau05, tuyau06, tuyau07, tuyau08, tuyau09, null],
									 [null, tuyau10, null, tuyau11, tuyau12, tuyau13, null],
									 [tuyau14, tuyau15, null, tuyau16, tuyau17, tuyau18, tuyau19],
									 [tuyau20, tuyau21, tuyau22, tuyau23, tuyau24, tuyau25, null],
									 [null, null, null, null, null, null, null],
									 [null, null, null, null, null, null, null]
								   ];
				break;
				
			case 3:
				var tuyau01 = this.creationTuyau(1, "Tournant", 0);
				var tuyau02 = this.creationTuyau(2, "Droit", 0);
				var tuyau03 = this.creationTuyau(3, "Tournant", 1);
				var tuyau04 = this.creationTuyau(4, "Droit", 1);
				var tuyau05 = this.creationTuyau(5, "Droit", 1);
				var tuyau06 = this.creationTuyau(6, "Droit", 1);
				var tuyau07 = this.creationTuyau(7, "Tournant", 0);
				var tuyau08 = this.creationTuyau(8, "Tournant", 1);
				var tuyau09 = this.creationTuyau(9, "Tournant", 3);
				var tuyau10 = this.creationTuyau(10, "Tournant", 2);
				var tuyau11 = this.creationTuyau(11, "Croix", 0);
				var tuyau12 = this.creationTuyau(12, "Droit", 0);
				var tuyau13 = this.creationTuyau(13, "Pont", 1);
				var tuyau14 = this.creationTuyau(14, "Croix", 0);
				var tuyau15 = this.creationTuyau(15, "Pont", 2);
				var tuyau16 = this.creationTuyau(16, "Droit", 0);
				var tuyau17 = this.creationTuyau(17, "Croix", 0);
				var tuyau18 = this.creationTuyau(18, "Tournant", 2);
				var tuyau19 = this.creationTuyau(19, "Droit", 1);
				var tuyau20 = this.creationTuyau(20, "Croix", 0);
				var tuyau21 = this.creationTuyau(21, "Tournant", 2);
				var tuyau22 = this.creationTuyau(22, "Pont", 0);
				var tuyau23 = this.creationTuyau(23, "Droit", 1);
				var tuyau24 = this.creationTuyau(24, "Tournant", 0);
				var tuyau25 = this.creationTuyau(25, "Croix", 0);
				var tuyau26 = this.creationTuyau(26, "Tournant", 2);
				var tuyau27 = this.creationTuyau(27, "Droit", 1);
				var tuyau28 = this.creationTuyau(28, "Tournant", 3);
				var tuyau29 = this.creationTuyau(29, "Croix", 0);
				var tuyau30 = this.creationTuyau(30, "Droit", 0);
				var tuyau31 = this.creationTuyau(31, "Tournant", 0);
				var tuyau32 = this.creationTuyau(32, "Droit", 0);
				var tuyau33 = this.creationTuyau(33, "Tournant", 2);
				var tuyau34 = this.creationTuyau(34, "Tournant", 3);
				var tuyau35 = this.creationTuyau(35, "Droit", 0);
				var tuyau36 = this.creationTuyau(36, "Tournant", 2);				
				this.tabParcours = [ [tuyau01, tuyau02, tuyau03, null, null, null, null],
									 [tuyau04, tuyau05, tuyau06, null, null, tuyau07, tuyau08],
									 [tuyau09, tuyau10, tuyau11, tuyau12, tuyau13, tuyau14, tuyau15],
									 [tuyau16, tuyau17, tuyau18, tuyau19, tuyau20, tuyau21, tuyau22],
									 [null, tuyau23, tuyau24, tuyau25, tuyau26, null, tuyau27],
									 [null, tuyau28, tuyau29, tuyau30, tuyau31, tuyau32, tuyau33],
									 [null, null, tuyau34, tuyau35, tuyau36, null, null]
								   ];
				break;
				
			case 4:
				var tuyau01 = this.creationTuyau(1, "Tournant", 0);
				var tuyau02 = this.creationTuyau(2, "Pont", 1);
				var tuyau03 = this.creationTuyau(3, "Tournant", 1);
				var tuyau04 = this.creationTuyau(4, "Tournant", 0);
				var tuyau05 = this.creationTuyau(5, "Droit", 0);
				var tuyau06 = this.creationTuyau(6, "Tournant", 1);				
				var tuyau07 = this.creationTuyau(7, "Tournant", 3);
				var tuyau08 = this.creationTuyau(8, "Tournant", 1);
				var tuyau09 = this.creationTuyau(9, "Tournant", 3);
				var tuyau10 = this.creationTuyau(10, "Tournant", 0);
				var tuyau11 = this.creationTuyau(11, "Pont", 2);
				var tuyau12 = this.creationTuyau(12, "Droit", 1);				
				var tuyau13 = this.creationTuyau(13, "Droit", 0);
				var tuyau14 = this.creationTuyau(14, "Pont", 2);
				var tuyau15 = this.creationTuyau(15, "Pont", 0);
				var tuyau16 = this.creationTuyau(16, "Tournant", 1);
				var tuyau17 = this.creationTuyau(17, "Droit", 1);
				var tuyau18 = this.creationTuyau(18, "Tournant", 3);				
				var tuyau19 = this.creationTuyau(19, "Tournant", 0);
				var tuyau20 = this.creationTuyau(20, "Pont", 0);
				var tuyau21 = this.creationTuyau(21, "Droit", 1);
				var tuyau22 = this.creationTuyau(22, "Tournant", 3);
				var tuyau23 = this.creationTuyau(23, "Tournant", 2);				
				var tuyau24 = this.creationTuyau(24, "Tournant", 3);
				var tuyau25 = this.creationTuyau(25, "Droit", 0);
				var tuyau26 = this.creationTuyau(26, "Tournant", 2);				
				this.tabParcours = [ [null, null, null, null, null, null, null],
									 [tuyau01, tuyau02, tuyau03, null, tuyau04, tuyau05, tuyau06],
									 [tuyau07, tuyau08, tuyau09, tuyau10, tuyau11, null, tuyau12],
									 [tuyau13, tuyau14, tuyau15, tuyau16, tuyau17, null, tuyau18],
									 [tuyau19, tuyau20, tuyau21, tuyau22, tuyau23, null, null],
									 [tuyau24, tuyau25, tuyau26, null, null, null, null],
									 [null, null, null, null, null, null, null]
								   ];
				break;
				
			case 5:
				var tuyau01 = this.creationTuyau(1, "Tournant", 0);
				var tuyau02 = this.creationTuyau(2, "Droit", 0);
				
				var tuyau03 = this.creationTuyau(3, "Droit", 1);
				var tuyau04 = this.creationTuyau(4, "Croix", 0);
				var tuyau05 = this.creationTuyau(5, "Pont", 1);
				var tuyau06 = this.creationTuyau(6, "Pont", 1);
				var tuyau07 = this.creationTuyau(7, "Tournant", 1);
				
				var tuyau08 = this.creationTuyau(8, "Pont", 0);
				var tuyau09 = this.creationTuyau(9, "Droit", 0);
				var tuyau10 = this.creationTuyau(10, "Croix", 0);
				var tuyau11 = this.creationTuyau(11, "Croix", 0);
				var tuyau12 = this.creationTuyau(12, "Croix", 0);
				var tuyau13 = this.creationTuyau(13, "Tournant", 1);
				
				var tuyau14 = this.creationTuyau(14, "Droit", 0);
				var tuyau15 = this.creationTuyau(15, "Pont", 2);
				var tuyau16 = this.creationTuyau(16, "Tournant", 1);
				var tuyau17 = this.creationTuyau(17, "Droit", 1);
				var tuyau18 = this.creationTuyau(18, "Pont", 0);
				var tuyau19 = this.creationTuyau(19, "Pont", 3);
				
				var tuyau20 = this.creationTuyau(20, "Droit", 1);
				var tuyau21 = this.creationTuyau(21, "Croix", 0);
				var tuyau22 = this.creationTuyau(22, "Pont", 0);
				var tuyau23 = this.creationTuyau(23, "Tournant", 2);
				
				var tuyau24 = this.creationTuyau(24, "Tournant", 3);
				var tuyau25 = this.creationTuyau(25, "Tournant", 1);
				var tuyau26 = this.creationTuyau(26, "Pont", 0);
				var tuyau27 = this.creationTuyau(27, "Pont", 2);
				
				var tuyau28 = this.creationTuyau(28, "Tournant", 3);
				var tuyau29 = this.creationTuyau(29, "Pont", 3);
				var tuyau30 = this.creationTuyau(30, "Tournant", 2);
				
				this.tabParcours = [ [null, tuyau01, tuyau02, null, null, null, null],
									 [null, tuyau03, tuyau04, tuyau05, tuyau06, tuyau07, null],
									 [null, tuyau08, tuyau09, tuyau10, tuyau11, tuyau12, tuyau13],
									 [tuyau14, tuyau15, null, tuyau16, tuyau17, tuyau18, tuyau19],
									 [null, tuyau20, null, tuyau21, tuyau22, tuyau23, null],
									 [null, tuyau24, tuyau25, tuyau26, tuyau27, null, null],
									 [null, null, tuyau28, tuyau29, tuyau30, null, null]
								   ];
				break;
				
			case 6:
			
				var tuyau01 = this.creationTuyau(1, "Tournant", 0);
				var tuyau02 = this.creationTuyau(2, "Droit", 1);
				var tuyau03 = this.creationTuyau(3, "Tournant", 0);
				var tuyau04 = this.creationTuyau(4, "Droit", 1);
				var tuyau05 = this.creationTuyau(5, "Droit", 0);
				var tuyau06 = this.creationTuyau(6, "Tournant", 1);				
				var tuyau07 = this.creationTuyau(7, "Tournant", 0);
				var tuyau08 = this.creationTuyau(8, "Tournant", 2);
				var tuyau09 = this.creationTuyau(9, "Droit", 1);
				var tuyau10 = this.creationTuyau(10, "Droit", 1);
				var tuyau11 = this.creationTuyau(11, "Droit", 1);
				var tuyau12 = this.creationTuyau(12, "Droit", 1);				
				var tuyau13 = this.creationTuyau(13, "Croix", 0);
				var tuyau14 = this.creationTuyau(14, "Droit", 0);
				var tuyau15 = this.creationTuyau(15, "Croix", 0);
				var tuyau16 = this.creationTuyau(16, "Pont", 3);
				var tuyau17 = this.creationTuyau(17, "Croix", 0);
				var tuyau18 = this.creationTuyau(18, "Droit", 0);
				var tuyau19 = this.creationTuyau(19, "Croix", 0);				
				var tuyau20 = this.creationTuyau(20, "Tournant", 3);
				var tuyau21 = this.creationTuyau(21, "Tournant", 1);
				var tuyau22 = this.creationTuyau(22, "Droit", 1);
				var tuyau23 = this.creationTuyau(23, "Droit", 1);
				var tuyau24 = this.creationTuyau(24, "Droit", 1);				
				var tuyau25 = this.creationTuyau(25, "Tournant", 3);
				var tuyau26 = this.creationTuyau(26, "Droit", 1);
				var tuyau27 = this.creationTuyau(27, "Droit", 1);
				var tuyau28 = this.creationTuyau(28, "Droit", 0);
				var tuyau29 = this.creationTuyau(29, "Tournant", 2);				
				this.tabParcours = [ [null, null, null, null, null, null, null],
									 [null, tuyau01, tuyau02, tuyau03, tuyau04, tuyau05, tuyau06],
									 [tuyau07, tuyau08, tuyau09, tuyau10, tuyau11, null, tuyau12],
									 [tuyau13, tuyau14, tuyau15, tuyau16, tuyau17, tuyau18, tuyau19],
									 [tuyau20, tuyau21, tuyau22, null, tuyau23, null, tuyau24],
									 [null, tuyau25, tuyau26, null, tuyau27, tuyau28, tuyau29],
									 [null, null, null, null, null, null, null]
								   ];
				break;
				
			case 7:
			
				var tuyau01 = this.creationTuyau(1, "Tournant", 0);
				var tuyau02 = this.creationTuyau(2, "Tournant", 1);
				var tuyau03 = this.creationTuyau(3, "Tournant", 0);
				var tuyau04 = this.creationTuyau(4, "Tournant", 1);				
				var tuyau05 = this.creationTuyau(5, "Droit", 1);
				var tuyau06 = this.creationTuyau(6, "Pont", 0);
				var tuyau07 = this.creationTuyau(7, "Droit", 0);
				var tuyau08 = this.creationTuyau(8, "Pont", 0);
				var tuyau09 = this.creationTuyau(9, "Pont", 2);				
				var tuyau10 = this.creationTuyau(10, "Tournant", 3);
				var tuyau11 = this.creationTuyau(11, "Croix", 0);
				var tuyau12 = this.creationTuyau(12, "Tournant", 1);
				var tuyau13 = this.creationTuyau(13, "Tournant", 1);
				var tuyau14 = this.creationTuyau(14, "Pont", 0);
				var tuyau15 = this.creationTuyau(15, "Pont", 2);				
				var tuyau16 = this.creationTuyau(16, "Droit", 0);
				var tuyau17 = this.creationTuyau(17, "Croix", 0);
				var tuyau18 = this.creationTuyau(18, "Droit", 0);
				var tuyau19 = this.creationTuyau(19, "Pont", 3);
				var tuyau20 = this.creationTuyau(20, "Droit", 0);
				var tuyau21 = this.creationTuyau(21, "Tournant", 3);
				var tuyau22 = this.creationTuyau(22, "Pont", 1);				
				var tuyau23 = this.creationTuyau(23, "Pont", 0);
				var tuyau24 = this.creationTuyau(24, "Droit", 0);
				var tuyau25 = this.creationTuyau(25, "Droit", 0);
				var tuyau26 = this.creationTuyau(26, "Croix", 0);
				var tuyau27 = this.creationTuyau(27, "Tournant", 1);
				var tuyau28 = this.creationTuyau(28, "Droit", 1);				
				var tuyau29 = this.creationTuyau(29, "Tournant", 3);
				var tuyau30 = this.creationTuyau(30, "Droit", 0);
				var tuyau31 = this.creationTuyau(31, "Droit", 1);
				var tuyau32 = this.creationTuyau(32, "Tournant", 3);
				var tuyau33 = this.creationTuyau(33, "Tournant", 2);				
				var tuyau34 = this.creationTuyau(34, "Tournant", 3);
				var tuyau35 = this.creationTuyau(35, "Droit", 0);
				var tuyau36 = this.creationTuyau(36, "Tournant", 2);				
				this.tabParcours = [ [tuyau01, tuyau02, null, null, tuyau03, tuyau04, null],
									 [tuyau05, tuyau06, tuyau07, null, tuyau08, tuyau09, null],
									 [tuyau10, tuyau11, tuyau12, tuyau13, tuyau14, tuyau15, null],
									 [tuyau16, tuyau17, tuyau18, tuyau19, tuyau20, tuyau21, tuyau22],
									 [null, tuyau23, tuyau24, tuyau25, tuyau26, tuyau27, tuyau28],
									 [null, tuyau29, tuyau30, null, tuyau31, tuyau32, tuyau33],
									 [null, null, tuyau34, tuyau35, tuyau36, null, null]
								   ];
				break;
				
			case 8:
			
				var tuyau01 = this.creationTuyau(1, "Tournant", 0);
				var tuyau02 = this.creationTuyau(2, "Droit", 0);
				var tuyau03 = this.creationTuyau(3, "Droit", 0);
				var tuyau04 = this.creationTuyau(4, "Tournant", 1);				
				var tuyau05 = this.creationTuyau(5, "Tournant", 3);
				var tuyau06 = this.creationTuyau(6, "Tournant", 1);
				var tuyau07 = this.creationTuyau(7, "Tournant", 0);
				var tuyau08 = this.creationTuyau(8, "Pont", 3);
				var tuyau09 = this.creationTuyau(9, "Droit", 0);
				var tuyau10 = this.creationTuyau(10, "Tournant", 1);				
				var tuyau11 = this.creationTuyau(11, "Pont", 2);
				var tuyau12 = this.creationTuyau(12, "Droit", 1);
				var tuyau13 = this.creationTuyau(13, "Droit", 1);				
				var tuyau14 = this.creationTuyau(14, "Droit", 0);
				var tuyau15 = this.creationTuyau(15, "Droit", 0);
				var tuyau16 = this.creationTuyau(16, "Croix", 1);
				var tuyau17 = this.creationTuyau(17, "Tournant", 1);
				var tuyau18 = this.creationTuyau(18, "Tournant", 0);
				var tuyau19 = this.creationTuyau(19, "Droit", 0);
				var tuyau20 = this.creationTuyau(20, "Croix", 0);				
				var tuyau21 = this.creationTuyau(21, "Droit", 1);
				var tuyau22 = this.creationTuyau(22, "Droit", 1);
				var tuyau23 = this.creationTuyau(23, "Droit", 1);				
				var tuyau24 = this.creationTuyau(24, "Tournant", 0);
				var tuyau25 = this.creationTuyau(25, "Tournant", 2);
				var tuyau26 = this.creationTuyau(26, "Tournant", 1);
				var tuyau27 = this.creationTuyau(27, "Droit", 0);
				var tuyau28 = this.creationTuyau(28, "Droit", 0);
				var tuyau29 = this.creationTuyau(29, "Tournant", 2);				
				var tuyau30 = this.creationTuyau(30, "Tournant", 3);
				var tuyau31 = this.creationTuyau(31, "Droit", 0);
				var tuyau32 = this.creationTuyau(32, "Tournant", 2);				
				this.tabParcours = [ [null, tuyau01, tuyau02, tuyau03, tuyau04, null, null],
									 [null, tuyau05, tuyau06, tuyau07, tuyau08, tuyau09, tuyau10],
									 [null, null, tuyau11, tuyau12, null, null, tuyau13],
									 [tuyau14, tuyau15, tuyau16, tuyau17, tuyau18, tuyau19, tuyau20],
									 [null, null, tuyau21, tuyau22, null, null, tuyau23],
									 [null, tuyau24, tuyau25, tuyau26, tuyau27, tuyau28, tuyau29],
									 [null, tuyau30, tuyau31, tuyau32, null, null, null]
								   ];
				break;
				
			case 9:
			
				var tuyau01 = this.creationTuyau(1, "Tournant", 0);
				var tuyau02 = this.creationTuyau(2, "Droit", 0);
				var tuyau03 = this.creationTuyau(3, "Droit", 0);
				var tuyau04 = this.creationTuyau(4, "Droit", 0);				
				var tuyau05 = this.creationTuyau(5, "Tournant", 3);
				var tuyau06 = this.creationTuyau(6, "Tournant", 1);
				var tuyau07 = this.creationTuyau(7, "Tournant", 0);
				var tuyau08 = this.creationTuyau(8, "Tournant", 1);
				var tuyau09 = this.creationTuyau(9, "Droit", 0);
				var tuyau10 = this.creationTuyau(10, "Tournant", 1);				
				var tuyau11 = this.creationTuyau(11, "Droit", 1);
				var tuyau12 = this.creationTuyau(12, "Droit", 1);
				var tuyau13 = this.creationTuyau(13, "Droit", 1);				
				var tuyau14 = this.creationTuyau(14, "Droit", 0);
				var tuyau15 = this.creationTuyau(15, "Droit", 0);
				var tuyau16 = this.creationTuyau(16, "Croix", 0);
				var tuyau17 = this.creationTuyau(17, "Droit", 1);
				var tuyau18 = this.creationTuyau(18, "Croix", 0);				
				var tuyau19 = this.creationTuyau(19, "Droit", 1);
				var tuyau20 = this.creationTuyau(20, "Droit", 1);
				var tuyau21 = this.creationTuyau(21, "Droit", 1);				
				var tuyau22 = this.creationTuyau(22, "Tournant", 0);
				var tuyau23 = this.creationTuyau(23, "Tournant", 2);
				var tuyau24 = this.creationTuyau(24, "Pont", 0);
				var tuyau25 = this.creationTuyau(25, "Droit", 0);
				var tuyau26 = this.creationTuyau(26, "Droit", 0);
				var tuyau27 = this.creationTuyau(27, "Tournant", 2);				
				var tuyau28 = this.creationTuyau(28, "Tournant", 3);
				var tuyau29 = this.creationTuyau(29, "Droit", 0);
				var tuyau30 = this.creationTuyau(30, "Tournant", 2);				
				this.tabParcours = [ [null, tuyau01, tuyau02, tuyau03, tuyau04, null, null],
									 [null, tuyau05, tuyau06, tuyau07, tuyau08, tuyau09, tuyau10],
									 [null, null, tuyau11, tuyau12, null, null, tuyau13],
									 [tuyau14, tuyau15, tuyau16, tuyau17, null, null, tuyau18],
									 [null, null, tuyau19, tuyau20, null, null, tuyau21],
									 [null, tuyau22, tuyau23, tuyau24, tuyau25, tuyau26, tuyau27],
									 [null, tuyau28, tuyau29, tuyau30, null, null, null]
								   ];
				break;
				
		}
		this.tabParcoursPrec = OutilsTableaux.getCopie( this.tabParcours );
		
		//le tableau des nouveau tuyau est vide au debut, on a pas encore été chercher des tuyau dans la 
		//pièce du fond
		this.tabNouveauxTuyaux = new Array();
		
		
		//une fois l'initialisation de la nouvelle donne terminée, tout ce qui est affichage ou initialisation 
		//des boutons est appelé dans le scenario principale (comme quand on revient de la salle du choix
		//des tuyaux elle est appelée automatiquement
		
	}
	

	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
		
		
	 /**
	 * Fonction qui crée un clip tuyau
	 * @return le clip créé
	 */
	 private function creationTuyau(id:Number, forme:String, sens:Number) {
		 
		var monTuyau = this.attachMovie("mcTuyau"+forme, "Tuyau"+id, Tuyau.highestDepth);
		monTuyau.initialise(id, forme, sens);
		
		return monTuyau;		 
	 }
	 
	 
	 /**
	 * Fonction qui affiche les tuyaux
	 * @return rien
	 */
	 private function affichagePiecePrincipale() {
		 
		//initialisation du graphisme (parchemin forcement invisible quand on revient dans la piece principale
		this.parchemin_mc._visible = false;
		//nombre de loupiotttes affichées en fonction du score du joueur
		trace("this.score => "+this.score)
		switch(this.score) {
			
			case 0:
				this.spot1_mc._visible = false;
				this.spot2_mc._visible = false;
				this.spot3_mc._visible = false;
				break;
				
			case 1:
				this.spot1_mc._visible = true;
				this.spot2_mc._visible = false;
				this.spot3_mc._visible = false;
				break;
				
			case 2:
				this.spot1_mc._visible = true;
				this.spot2_mc._visible = true;
				this.spot3_mc._visible = false;
				break;
		}
		 
		//initialisation des evenements
		this.initialiseParchemin();		
		this.initialiseNouveauxTuyaux();
		this.initialiseAffichageParcoursTuyau();
		this.initialiseBoutonAnnuler();
		this.initialiseBoutonFin();
		this.initialisePorteFond();		
		
	 }
	 
	 
	 
		
	 /**
	 * Fonction qui initialise le click sur le parchemin
	 * @return rien
	 */
	 private function initialiseParchemin() {
		
		this.parchemin_mc.onRelease = function() {
			_parent.soundPlayer.playASound("shark2");
		
			//refermer la trappe
			this._visible = false;
		
			this._parent.gameFinished();
		}
		
	 }
	 
	 /**
	 * Fonction qui initialise les éléments ayant trait aux nouveaux tuyaux en bas à gauche
	 * @return rien
	 */
	 private function initialiseNouveauxTuyaux() {
	 
	 	//initialisation (mise en invisible) des contours des nouveaux tuyaux
		this.contourNvoTuyau1_mc._visible = false;
		this.contourNvoTuyau2_mc._visible = false;
		this.contourNvoTuyau3_mc._visible = false;
		this.contourNvoTuyau4_mc._visible = false;
		
		//initialisation des boutons permettant de faire pivoter les tuyaux du dessous
		this.pivoter1_mc.onRelease = function () {
			this._parent.tabNouveauxTuyaux[0].addSens();
		}
		this.pivoter2_mc.onRelease = function () {
			this._parent.tabNouveauxTuyaux[1].addSens();
		}
		this.pivoter3_mc.onRelease = function () {
			this._parent.tabNouveauxTuyaux[2].addSens();
		}
		this.pivoter4_mc.onRelease = function () {
			this._parent.tabNouveauxTuyaux[3].addSens();
		}
		
		//affichage des tuyaux de remplacement et initialisation de leur comportement
		if(this.tabNouveauxTuyaux.length != null || tabNouveauxTuyaux.length != 0) {
			
			this.tabNouveauxTuyaux[0]._x = 28;
			this.tabNouveauxTuyaux[0]._y = 373;
			this.tabNouveauxTuyaux[0]._visible = true;
			this.tabNouveauxTuyaux[0].onRelease = function() {
				
				if(this._parent.contourNvoTuyau1_mc._visible != true)
				{
					//si le tuyau n'avait pas deja été selectionné auparavant on le selectionne
					this._parent.tuyauSelectionnePrec = this._parent.tuyauSelectionne;
					this._parent.tuyauSelectionne = this;
					this._parent.placeNouveauxTuyauxPrec = 0;
					
					this._parent.contourNvoTuyau1_mc._visible = true;
					this._parent.contourNvoTuyau2_mc._visible = false;
					this._parent.contourNvoTuyau3_mc._visible = false;
					this._parent.contourNvoTuyau4_mc._visible = false;
				}
				else
				{
					//sinon on change l'orientation du tuyau
					this.addSens();
				}
			}
			this.tabNouveauxTuyaux[1]._x = 92;
			this.tabNouveauxTuyaux[1]._y = 373;
			this.tabNouveauxTuyaux[1]._visible = true;
			this.tabNouveauxTuyaux[1].onRelease = function() {
				
				if(this._parent.contourNvoTuyau2_mc._visible != true)
				{
					//si le tuyau n'avait pas deja été selectionné auparavant on le selectionne
					this._parent.tuyauSelectionnePrec = this._parent.tuyauSelectionne;
					this._parent.tuyauSelectionne = this;
					this._parent.placeNouveauxTuyauxPrec = 1;
					
					this._parent.contourNvoTuyau1_mc._visible = false;
					this._parent.contourNvoTuyau2_mc._visible = true;
					this._parent.contourNvoTuyau3_mc._visible = false;
					this._parent.contourNvoTuyau4_mc._visible = false;
				}
				else
				{
					//sinon on change l'orientation du tuyau
					this.addSens();
				}
			}
			this.tabNouveauxTuyaux[2]._x = 156;
			this.tabNouveauxTuyaux[2]._y = 373;
			this.tabNouveauxTuyaux[2]._visible = true;
			this.tabNouveauxTuyaux[2].onRelease = function() {
				
				if(this._parent.contourNvoTuyau3_mc._visible != true)
				{
					//si le tuyau n'avait pas deja été selectionné auparavant on le selectionne
					this._parent.tuyauSelectionnePrec = this._parent.tuyauSelectionne;
					this._parent.tuyauSelectionne = this;
					this._parent.placeNouveauxTuyauxPrec = 2;
					
					this._parent.contourNvoTuyau1_mc._visible = false;
					this._parent.contourNvoTuyau2_mc._visible = false;
					this._parent.contourNvoTuyau3_mc._visible = true;
					this._parent.contourNvoTuyau4_mc._visible = false;
				}
				else
				{
					//sinon on change l'orientation du tuyau
					this.addSens();
				}
			}
			this.tabNouveauxTuyaux[3]._x = 220;
			this.tabNouveauxTuyaux[3]._y = 373;
			this.tabNouveauxTuyaux[3]._visible = true;
			this.tabNouveauxTuyaux[3].onRelease = function() {
				
				if(this._parent.contourNvoTuyau4_mc._visible != true)
				{
					//si le tuyau n'avait pas deja été selectionné auparavant on le selectionne
					this._parent.tuyauSelectionnePrec = this._parent.tuyauSelectionne;
					this._parent.tuyauSelectionne = this;
					this._parent.placeNouveauxTuyauxPrec = 3;
					
					this._parent.contourNvoTuyau1_mc._visible = false;
					this._parent.contourNvoTuyau2_mc._visible = false;
					this._parent.contourNvoTuyau3_mc._visible = false;
					this._parent.contourNvoTuyau4_mc._visible = true;
				}
				else
				{
					//sinon on change l'orientation du tuyau
					this.addSens();
				}
			}
			
		}
	 }
	 
	 
	 /**
	 * Fonction qui initialise le parcours de tuyaux (l'affiche et attribue un evenement à chaque tuyau)
	 * @return rien
	 */
	 private function initialiseAffichageParcoursTuyau() {
		
		trace("init affichage")
		
		//affichage des tuyaux aux bons emplacements et de manière visible 
		//ainsi que l'attribution d'un evenement de click
		var posY = 23;
		for(var i = 0;i<this.tabParcours.length;i++) {
			
			var posX = 50;
			for(var j = 0;j<this.tabParcours[i].length;j++) {
				
				//positionnement
				this.tabParcours[i][j]._x = posX;
				this.tabParcours[i][j]._y = posY;
				this.tabParcours[i][j]._visible = true;
				posX += 44;
				
			
				//evenement
				this.tabParcours[i][j].onRelease = function () {
					
					//si un tuyau de remplacement est selectionné on remplace le tuyau clické par le tuyau
					//selectionné
					if(this._parent.tuyauSelectionne != null) {
						
						this._parent.tuyauRemplacePrec = this;
						this._parent.tuyauSelectionnePrec = this._parent.tuyauSelectionne;
						this._visible = false;

						var i = 0
						while( i < this._parent.tabParcours.length ) {
							
							var pos = OutilsTableaux.getPosition(this._parent.tabParcours[i], this);
							if(pos != undefined)
							{
								break;
							}
							
							i++;				
						}
						this._parent.tabParcours[i][pos] = this._parent.tuyauSelectionne;

						this._parent.tuyauSelectionne._x = this._x;
						this._parent.tuyauSelectionne._y = this._y;
						
						this._parent.tuyauSelectionne = null;
						this._parent.contourNvoTuyau1_mc._visible = false;
						this._parent.contourNvoTuyau2_mc._visible = false;
						this._parent.contourNvoTuyau3_mc._visible = false;
						this._parent.contourNvoTuyau4_mc._visible = false;

						trace("this._parent.tabNouveauxTuyaux[this._parent.placeNouveauxTuyauxPrec] => "+this._parent.tabNouveauxTuyaux[this._parent.placeNouveauxTuyauxPrec])
						//on retire le tuyau de remplacement de la liste des nouveaux tuyaux
						this._parent.tabNouveauxTuyaux[this._parent.placeNouveauxTuyauxPrec] = null;
						trace("------------------------------------------------------------------")
						trace("this._parent.tabNouveauxTuyaux => "+this._parent.tabNouveauxTuyaux)
						trace("this._parent.tuyauSelectionne => "+this._parent.tuyauSelectionne )
						trace("this._parent.tuyauSelectionnePrec => "+this._parent.tuyauSelectionnePrec )
						trace("this._parent.tuyauRemplacePrec => "+this._parent.tuyauRemplacePrec)
						
						//puis on relance la fonction d'affichage afin d'appliquer le bon evenement onRelease 
						//au nouveau tuyau
						this._parent.initialiseAffichageParcoursTuyau();
					}
				}
			}
			
			posY += 44.5;
		}
		
		
	 }
	 
	 /**
	 * Fonction qui initialise le bouton permettant de revenir sur la derniere action
	 * @return rien
	 */
	 private function initialiseBoutonAnnuler() {
		
		this.annuler_mc.onRelease = function() {
			
			if(this._parent.tuyauSelectionnePrec != null)
			{
				//il faut trouver où le dernier element remplacé se trouve dans le tableau
				var i = 0
				while( i < this._parent.tabParcours.length ) {
					
					var pos = OutilsTableaux.getPosition(this._parent.tabParcours[i], this._parent.tuyauSelectionnePrec);
					if(pos != undefined)
					{
						break;
					}
					
					i++;				
				}
				this._parent.tabParcours[i][pos] = this._parent.tuyauRemplacePrec;
				this._parent.tuyauRemplacePrec._visible = true;
				this._parent.tuyauRemplacePrec = null;
				
				//puis on remet l'autre tuyau à sa place
				switch(this._parent.placeNouveauxTuyauxPrec) {
					
					case 0:
						this._parent.tuyauSelectionnePrec._x = 28;
						this._parent.tuyauSelectionnePrec._y = 373;
						break;
						
					case 1:
						this._parent.tuyauSelectionnePrec._x = 92;
						this._parent.tuyauSelectionnePrec._y = 373;
						break;
						
					case 2:
						this._parent.tuyauSelectionnePrec._x = 156;
						this._parent.tuyauSelectionnePrec._y = 373;
						break;
						
					case 3:
						this._parent.tuyauSelectionnePrec._x = 220;
						this._parent.tuyauSelectionnePrec._y = 373;
						break;
					
				}
				
				//on replace le tuyau de remplacement dans la liste des nouveaux tuyaux
				this._parent.tabNouveauxTuyaux[this._parent.placeNouveauxTuyauxPrec] = this._parent.tuyauSelectionnePrec;
				
				this._parent.tuyauSelectionnePrec = null;
				this._parent.placeNouveauxTuyauxPrec = null;
				
				//puis on relance la fonction d'affichage afin d'appliquer le bon evenement onRelease 
				//au tuyau
				this._parent.initialiseNouveauxTuyaux();
			}
			else
			{
				trace("toto")
			}
		}
	 }
	 
	 /**
	 * Fonction qui initialise le click sur le parchemin
	 * @return rien
	 */
	 private function initialiseBoutonFin() {
		
		//si on a déja été dans la pièce du fond on active le bouton
		if(this.nbreVisitePieceTuyaux != 0) {
			
			this.btFini_mc.enabled = true;
			this.btFini_mc.texte_txt.text = "J'ai fini";
		}
		
		this.btFini_mc.onRelease = function() {
			
			this._parent.verificationResultat();
		}
		
	 }
	 
			
	 /**
	 * Fonction qui vérifie les résultats du joueur, si ils sont mauvais le joueur continue a chercher sinon
	 * on génère une nouvelle donne
	 * @return rien
	 */
	 private function verificationResultat() {
		soundPlayer.playASound("shClac2");
		//tableau qui va nous servir a savoir si il y a des ouvertures non-bouchées
		var listeOuverture:Array = new Array();
		for (var i = 0;i<this.tabParcours.length;i++) {
			
			for(var j = 0;j<this.tabParcours[i].length;j++) {
				
				//si le tuyau courant a une sortie vers le haut on regarde si il y a un tuyau au dessus ayant 
				//une sortie vers le bas, si ce n'est pas le cas on enregistre le tuyau courant dans 
				//listeOuverture (on pourrait optimiser l'algo en sortant du for dès k'il y a une mauvaise 
				//reponse)
				if(this.tabParcours[i][j].getHasOuvertureHaut() ) {
					
					var tuyauDuHaut = this.tabParcours[i-1][j];
					if(!tuyauDuHaut.getHasOuvertureBas() )
					{
						listeOuverture.push(this.tabParcours[i][j]);
					}
					
				}
				//verification pour une eventuelle sortie de droite
				if(this.tabParcours[i][j].getHasOuvertureDroit() ) {
					
					var tuyauDeDroite = this.tabParcours[i][j+1];
					if(!tuyauDeDroite.getHasOuvertureGauche() )
					{
						listeOuverture.push(this.tabParcours[i][j]);
					}
					
				}
				//verification pour une eventuelle sortie du bas
				if(this.tabParcours[i][j].getHasOuvertureBas() ) {
					
					var tuyauDuBas = this.tabParcours[i+1][j];
					if(!tuyauDuBas.getHasOuvertureHaut() )
					{
						listeOuverture.push(this.tabParcours[i][j]);
					}
					
				}
				//verification pour une eventuelle sortie de gauche
				if(this.tabParcours[i][j].getHasOuvertureGauche() ) {
					
					var tuyauDeGauche = this.tabParcours[i][j-1];
					if(!tuyauDeGauche.getHasOuvertureDroit() )
					{
						listeOuverture.push(this.tabParcours[i][j]);
					}
					
				}				
				
			}
		}
		
		//normalement la réponse est juste quand il ne reste que 2 sorties non-bouchées (par l'algo)
		//correspondant aux 2 sorties fixes gauche et droite
		if(listeOuverture.length == 2) {
			
			//le point n'est accordé que si le joueur n'a été qu'une seule fois dans la pièce du fond
			if(this.nbreVisitePieceTuyaux < 2)
			{
				this.score++;
			}
			
			if(this.score != 3) {
				
				this.nouvelleDonne();
				this.affichagePiecePrincipale();
			}
			else
			{
				//on affiche la derniere loupiote
				this.spot3_mc._visible = true;
				
				//dans ce cas le joueur a gagné et on disable tout ce qu'il faut disablifier
				this.zonePorte_mc.enabled = false;
				this.btFini_mc.enabled = false;
				this.annuler_mc.enabled = false;
				
				this.parchemin_mc._visible = true;
			}
		}
	 }
	 
	 
	 /**
	 * Fonction qui cloture la partie (enregistrement des scores etc..
	 * @return rien
	 */
	 private function gameFinished() {
		
		//enregistrer le résultat => mémoriser que le joueur courant a bien finit ce jeu 
		this._parent._parent.setWinGame(13, this._parent._parent.joueur1);
		//afficher le parchemin dans la glissière de navigation (peut être fait lors de l'enregistrement du 
		//résultat
		_parent._parent.barreInterface_mc.showParchemin();
	 }
	 
	 
	 /**
	 * Fonction qui initialise la porte du fond nous amenant à la pièce du choix des tuyaux
	 * @return rien
	 */
	 private function initialisePorteFond() {
		
		//lorsque l'on survol la porte du fond on change le curseur qui prend une forme de fleche
		this.zonePorte_mc.onRollOver = function() {
			
			this.flecheCurseur = this.attachMovie("mcFlecheEntree", "flecheCurseur",99999);
			this.flecheCurseur._x = _xmouse;
			this.flecheCurseur._y = _ymouse;
			this.flecheCurseur.startDrag();
			//Mouse.hide();
			CursorManager.showMouse(false);
		}
		
		//lorsque l'on quitte la zone de la porte on supprime le clip et on fait redevenir visible la souris
		this.zonePorte_mc.onRollOut = function() {
			
			this.flecheCurseur.removeMovieClip();
			//Mouse.show();
			CursorManager.showMouse(true);
		}
		
		this.zonePorte_mc.onRelease = function() {
			_parent.soundPlayer.playASound("frele2");
			
			//on rend invisibles les tuyaux du parcours
			for(var i = 0;i<this._parent.tabParcours.length;i++) {
			
				for(var j = 0;j<this._parent.tabParcours[i].length;j++) {
					
					this._parent.tabParcours[i][j]._visible = false;
				}
			}
			
			//ainsi que les tuyaux de remplacement si il y en a
			for(var i = 0;i<this._parent.tabNouveauxTuyaux.length;i++) {
				
				this._parent.tabNouveauxTuyaux[i]._visible = false;
			}
			
			//puis on fait redevenir visible le curseur
			this.flecheCurseur.removeMovieClip();
			//Mouse.show();
			CursorManager.showMouse(true);
			//on augmente la variable comptant le nombre de fois où l'on est entré dans la piece
			this._parent.nbreVisitePieceTuyaux++;
			//puis on passe dans la pièce d'a coté
			this._parent.gotoAndPlay(11);
		}
		
	 }
	 
	 
	 /////////////////////////////////////////// PIECE SECONDAIRE ////////////////////////////////////
	 
	 /**
	 * Fonction qui initialise le bouton de retour de la piece secondaire
	 * @return rien
	 */
	 private function initPiecesChoixTuyau() {
		
		///////////////// initialisation du nombre de tuyaux pris /////////////////
		this.nbreTuyau1_txt.maxChars = 1;
		this.nbreTuyau1_txt.text = "0";
		this.nbreTuyau2_txt.maxChars = 1;
		this.nbreTuyau2_txt.text = "0";
		this.nbreTuyau3_txt.maxChars = 1;
		this.nbreTuyau3_txt.text = "0";
		this.nbreTuyau4_txt.maxChars = 1;
		this.nbreTuyau4_txt.text = "0";
		
		
		///////////////// initialisation des boutons + et - /////////////////
		this.plus1_mc.onRelease = function () {
			_parent.soundPlayer.playASound("minitic");
			if(parseInt(this._parent.nbreTuyau1_txt.text) < 4)
			{
				this._parent.nbreTuyau1_txt.text = parseInt(this._parent.nbreTuyau1_txt.text) + 1;
			}else {
				this._parent.nbreTuyau1_txt.text = 0;
			}
		}
		this.moins1_mc.onRelease = function () {
			_parent.soundPlayer.playASound("minitic");
			
			if(parseInt(this._parent.nbreTuyau1_txt.text) > 0)
			{
				this._parent.nbreTuyau1_txt.text = parseInt(this._parent.nbreTuyau1_txt.text) - 1;
			}
		}
		this.plus2_mc.onRelease = function () {
			_parent.soundPlayer.playASound("minitic");
			
			if(parseInt(this._parent.nbreTuyau2_txt.text) < 4)
			{
				this._parent.nbreTuyau2_txt.text = parseInt(this._parent.nbreTuyau2_txt.text) + 1;
			}else {
				this._parent.nbreTuyau2_txt.text = 0;
			}
		}
		this.moins2_mc.onRelease = function () {
			_parent.soundPlayer.playASound("minitic");
			
			if(parseInt(this._parent.nbreTuyau2_txt.text) > 0)
			{
				this._parent.nbreTuyau2_txt.text = parseInt(this._parent.nbreTuyau2_txt.text) - 1;
			}
		}
		this.plus3_mc.onRelease = function () {
			_parent.soundPlayer.playASound("minitic");
			
			if(parseInt(this._parent.nbreTuyau3_txt.text) < 4)
			{
				this._parent.nbreTuyau3_txt.text = parseInt(this._parent.nbreTuyau3_txt.text) + 1;
			}else {
				this._parent.nbreTuyau3_txt.text = 0;
			}
		}
		this.moins3_mc.onRelease = function () {
			_parent.soundPlayer.playASound("minitic");
			
			if(parseInt(this._parent.nbreTuyau3_txt.text) > 0)
			{
				this._parent.nbreTuyau3_txt.text = parseInt(this._parent.nbreTuyau3_txt.text) - 1;
			}
		}
		this.plus4_mc.onRelease = function () {
			_parent.soundPlayer.playASound("minitic");
			
			if(parseInt(this._parent.nbreTuyau4_txt.text) < 4)
			{
				this._parent.nbreTuyau4_txt.text = parseInt(this._parent.nbreTuyau4_txt.text) + 1;
			}else {
				this._parent.nbreTuyau4_txt.text = 0;
			}
		}
		this.moins4_mc.onRelease = function () {
			_parent.soundPlayer.playASound("minitic");
			
			if(parseInt(this._parent.nbreTuyau4_txt.text) > 0)
			{
				this._parent.nbreTuyau4_txt.text = parseInt(this._parent.nbreTuyau4_txt.text) - 1;
			}
		}
		
		
		///////////////// initialisation du bouton retour /////////////////
		
		//lorsque l'on survol la porte du fond on change le curseur qui prend une forme de fleche
		this.boutonRetour_mc.onRollOver = function() {
			
			this.flecheCurseur = this.attachMovie("mcFlecheEntree", "flecheCurseur",99999);
			this.flecheCurseur._x = _xmouse;
			this.flecheCurseur._y = _ymouse;
			this.flecheCurseur._rotation = 180;
			this.flecheCurseur.startDrag();
			//Mouse.hide();
			CursorManager.showMouse(false);
		}
		
		//lorsque l'on quitte la zone de la porte on supprime le clip et on fait redevenir visible la souris
		this.boutonRetour_mc.onRollOut = function() {
			
			this.flecheCurseur.removeMovieClip();
			//Mouse.show();
			CursorManager.showMouse(true);
			
		}
		
		this.boutonRetour_mc.onRelease = function() {
			_parent.soundPlayer.playASound("fefe");
			var totalTuyau = (parseInt(this._parent.nbreTuyau1_txt.text) + parseInt(this._parent.nbreTuyau2_txt.text) + parseInt(this._parent.nbreTuyau3_txt.text) + parseInt(this._parent.nbreTuyau4_txt.text));
			//on verifie que le joueur a choisi au moins 1 tuyau
			if(totalTuyau > 0)
			{
			
				//on verifie que le joueur n'a pas pris plus de 4 tuyaux sinon popup qui lui dit que c po possible
				if( totalTuyau < 5 ) {
					
					var nbreTuyauxChoisis = 0;
					for(var i = 1;i<5;i++) {
						
						//puis on enregistre les nombres de tuyaux de chaque type
						for(var j = 0;j<parseInt(this._parent["nbreTuyau"+i+"_txt"].text);j++) {
							
							//si il y avait deja un tuyau en place 1 on l'efface
							if(this._parent.tabNouveauxTuyaux[nbreTuyauxChoisis] != null)
							{
								this._parent.tabNouveauxTuyaux[nbreTuyauxChoisis].removeMovieClip();
							}
							//puis on met le nouveau à la placeavec un id de 100+nbreTuyauxChoisis
							switch(i)
							{
								case 1:
									var type = "Pont";
									break;
									
								case 2:
									var type = "Tournant";
									break;
									
								case 3:
									var type = "Droit";
									break;
									
								case 4:
									var type = "Croix";
									break;
							}
								
							this["Tuyau"+nbreTuyauxChoisis] = this._parent.creationTuyau(Tuyau.getHighestFixedId(), type, 0);
							this._parent.tabNouveauxTuyaux[nbreTuyauxChoisis] = this["Tuyau"+nbreTuyauxChoisis];
							nbreTuyauxChoisis++;
							
						}
						
						
					}
					
					//d'abord on rend invisible le clip fleche
					this.flecheCurseur.removeMovieClip();
					//Mouse.show();
					CursorManager.showMouse(true);
					//puis on retourne à la piece principale
					this._parent.gotoAndPlay(1);
				}
				else
				{
					//sinon on affiche un popup lui signalant le problème
					Alert.show("Tu ne peux pas prendre plus de 4 tuyaux...", "Attention !");
				}
			}
			else
			{
				//sinon on fait entendre la petite fille qui dit qu'il faut choisir des tuyaux
				soundPlayer.playASound("choisisTuyaux");
			}
		}
	 }

	 
	
}

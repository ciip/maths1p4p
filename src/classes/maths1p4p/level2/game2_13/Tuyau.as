﻿/**
 * class maths1p4p.level2.game2_13.Tuyau
 * 
 * @author Rémi Gay
 * @version 1.0
 *
 * Copyright (c) 2008 Rémi Gay.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
//import maths1p4p.utils.OutilsTableaux;
//import maths1p4p.level2.game2_13.TuyauType; 

class maths1p4p.level2.game2_13.Tuyau extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public static var listeTuyaux:Array = new Array();
	public static var highestDepth:Number = 200;
	public static var highestFixedId = 150;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	/***** variables privées de la classe *****/
	private var id:Number;
	private var forme:String;
	private var sens:Number;
	//ces 4 variables representent les ouvertures des tuyaux permettant une jonction avec un autre tuyau
	//les 4 directions cardinale sont representée
	private var hasOuvertureHaut:Boolean;
	private var hasOuvertureDroit:Boolean;
	private var hasOuvertureBas:Boolean;
	private var hasOuvertureGauche:Boolean;
	//private var type:TuyauType;

	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * s'occupe de l'initialisation graphique et l'attribution des evenements
	 */
	public function Tuyau (id:Number, forme:String, sens:Number)	{
		
		//ATTENTION A FAIRE EN AMONT
		//la classe ne verifie pas que le nom n'existe pas dejà ! On le testera avant
		Tuyau.listeTuyaux.push(this);
		
		this.id = id;
		//this.type = null;
		this.forme = forme;
		switch(forme) {
			
			//pour un sens de 0 par defaut
			case "Tournant":
				this.hasOuvertureHaut = false;
				this.hasOuvertureDroit = true;
				this.hasOuvertureBas = true;
				this.hasOuvertureGauche = false;
				break;
				
			case "Droit":
				this.hasOuvertureHaut = false;
				this.hasOuvertureDroit = true;
				this.hasOuvertureBas = false;
				this.hasOuvertureGauche = true;
				break;
				
			case "Pont":
				this.hasOuvertureHaut = true;
				this.hasOuvertureDroit = true;
				this.hasOuvertureBas = true;
				this.hasOuvertureGauche = false;
				break;
				
			case "Croix":
				this.hasOuvertureHaut = true;
				this.hasOuvertureDroit = true;
				this.hasOuvertureBas = true;
				this.hasOuvertureGauche = true;
				break;
		}
		
		if(sens == null) {
			
			this.sens = 0;
		}
		else
		{
			this.setSens(sens);
		}
		
		//on augmente la profondeur a chaque nouvelle création dun tuyau
		Tuyau.highestDepth++;
		

	}
	
	/**
	 * Renvoi la profondeur la plus haute des clips tuyaux
	 * @return plusHauteProfondeur : Number
	 */
	public static function getHighestDepth():Number {
		
		Tuyau.highestDepth++;
		return Tuyau.highestDepth;
	}
	
	/**
	 * Renvoi l'id fixe (donné arbitrairement à 150 mini) le plus haut des clips tuyaux
	 * @return plusHautId : Number
	 */
	public static function getHighestFixedId():Number {
		
		Tuyau.highestFixedId++;
		return Tuyau.highestFixedId;
	}
	
	/**
	 * Détruit tous les tuyaux
	 * @return rien
	 */
	public static function viderTabTuyaux():Void {
		
		for (var i = 0;i<Tuyau.listeTuyaux.length;i++) {
			
			Tuyau.listeTuyaux[i].removeMovieClip();
			trace("remove");
		}
		Tuyau.listeTuyaux = new Array();
	}
	
	public function initialise(id:Number, forme:String, sens:Number):Void {
	
		this.id = id;
		this.setForme(forme);
		switch(forme) {
			
			//pour un sens de 0 par defaut
			case "Tournant":
				this.hasOuvertureHaut = false;
				this.hasOuvertureDroit = true;
				this.hasOuvertureBas = true;
				this.hasOuvertureGauche = false;
				break;
				
			case "Droit":
				this.hasOuvertureHaut = false;
				this.hasOuvertureDroit = true;
				this.hasOuvertureBas = false;
				this.hasOuvertureGauche = true;
				break;
				
			case "Pont":
				this.hasOuvertureHaut = true;
				this.hasOuvertureDroit = true;
				this.hasOuvertureBas = true;
				this.hasOuvertureGauche = false;
				break;
				
			case "Croix":
				this.hasOuvertureHaut = true;
				this.hasOuvertureDroit = true;
				this.hasOuvertureBas = true;
				this.hasOuvertureGauche = true;
				break;
		}
		this.setSens(sens);
	}
	
	public function getId():Number {
		
		return this.id;
	}
	
	/*
	public function getType():TuyauType {
		
		return this.type;
	}
	
	public function setType(type:TuyauType) {
		
		this.type = type;
	}
	*/
	
	public function getForme():String {
		
		return this.forme;
	}
	
	public function setForme(forme:String) {
		
		this.forme = forme;
	}
	
	public function getSens():Number {
		
		return this.sens;
	}
	
	public function addSens() {
		
		if(this.sens != 3) {
			
			this.sens ++;
		}
		else
		{
			this.sens = 0;
		}
		//on doit egalement changer les ouvertures de 90°
		var memOuvertureHaut = this.hasOuvertureHaut;
		this.hasOuvertureHaut = false;
		var memOuvertureDroit = this.hasOuvertureDroit;
		this.hasOuvertureDroit = false;
		var memOuvertureBas = this.hasOuvertureBas;
		this.hasOuvertureBas = false;
		var memOuvertureGauche = this.hasOuvertureGauche;
		this.hasOuvertureGauche = false;
		
		if(memOuvertureHaut == true)
		{
			this.hasOuvertureDroit = true;
		}
		if(memOuvertureDroit == true)
		{
			this.hasOuvertureBas = true;
		}
		if(memOuvertureBas == true)
		{
			this.hasOuvertureGauche = true;
		}
		if(memOuvertureGauche == true)
		{
			this.hasOuvertureHaut = true;
		}

		this._rotation = 90*this.sens;
	}

	public function setSens(sens:Number) {
		
		this.sens = sens;
		this._rotation = 90*sens;
		
		//l'orientation des ouverture changent avec le sens, donc on l'applique pour chaque rotation de 90°
		for(var i = 0;i<sens;i++)
		{
			var memOuvertureHaut = this.hasOuvertureHaut;
			this.hasOuvertureHaut = false;
			var memOuvertureDroit = this.hasOuvertureDroit;
			this.hasOuvertureDroit = false;
			var memOuvertureBas = this.hasOuvertureBas;
			this.hasOuvertureBas = false;
			var memOuvertureGauche = this.hasOuvertureGauche;
			this.hasOuvertureGauche = false;
			
			if(memOuvertureHaut == true)
			{
				this.hasOuvertureDroit = true;
			}
			if(memOuvertureDroit == true)
			{
				this.hasOuvertureBas = true;
			}
			if(memOuvertureBas == true)
			{
				this.hasOuvertureGauche = true;
			}
			if(memOuvertureGauche == true)
			{
				this.hasOuvertureHaut = true;
			}
		}
				
	}
	
	public function getHasOuvertureHaut():Boolean {
		
		return this.hasOuvertureHaut;
	}
	public function getHasOuvertureDroit():Boolean {
		
		return this.hasOuvertureDroit;
	}
	public function getHasOuvertureBas():Boolean {
		
		return this.hasOuvertureBas;
	}
	public function getHasOuvertureGauche():Boolean {
		
		return this.hasOuvertureGauche;
	}
		
	

	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	 

	 
	
}

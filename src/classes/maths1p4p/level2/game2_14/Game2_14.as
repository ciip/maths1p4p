﻿/**
 * class com.maths1p4p.Game2_14
 * 
 * @author Yoann Augen
 * @version 1.0
 *
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level2.game2_14.Game2_14 extends maths1p4p.AbstractGame {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 14;
	
	/** Les 10 valeurs */
	public var p_mcValue1:MovieClip;
	public var p_mcValue2:MovieClip;
	public var p_mcValue3:MovieClip;
	public var p_mcValue4:MovieClip;
	public var p_mcValue5:MovieClip;
	public var p_mcValue6:MovieClip;
	public var p_mcValue7:MovieClip;
	public var p_mcValue8:MovieClip;
	public var p_mcValueDroite:MovieClip;
	public var p_mcValueGauche:MovieClip;
	
	/** Les reperes de placement en haut */
	public var p_mcRepere1:MovieClip;
	public var p_mcRepere2:MovieClip;
	public var p_mcRepere3:MovieClip;
	public var p_mcRepere4:MovieClip;
	public var p_mcRepere5:MovieClip;
	public var p_mcRepere6:MovieClip;
	public var p_mcRepere7:MovieClip;
	public var p_mcRepere8:MovieClip;
	
	/** Droite et gauche */
	public var p_mcRepereDroite:MovieClip;
	public var p_mcRepereGauche:MovieClip;
	
	/** Au centre */
	public var p_mcRepereCentre1:MovieClip;
	public var p_mcRepereCentre2:MovieClip;
	public var p_mcRepereCentre3:MovieClip;
	public var p_mcRepereCentre4:MovieClip;
	public var p_mcRepereCentre5:MovieClip;
	public var p_mcRepereCentre6:MovieClip;
	public var p_mcRepereCentre7:MovieClip;
	public var p_mcRepereCentre8:MovieClip;	
	
	/** L'aiguille de point du joueur */
	public var p_mcPoint:MovieClip;
	
	/** Le bouton start */
	public var p_mcBoutonStart:MovieClip;
	
	/** Entonnoir de laché des nombres */
	public var p_mcEntonnoir:MovieClip;
	
	/** Le parchemin */
	public var p_mcParchemin:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** Nombre de point du joueur */
	private var p_nPoint:Number;
	
	/** Tableau des valeurs */
	private var p_aValue:Array = new Array();
	
	/** Valeur min et max */
	private var p_nValueMin:Number;
	private var p_nValueMax:Number;
	
	/** Vérifie si le joueur à cliquer sur le bouton pour lancer le jeu */
	private var p_bRun:Boolean;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game2_14() {
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		this.p_nPoint = 0;
		this.p_bRun = false;
	}
	
	/**
	* Fonction qui renvoi un nombre aleatoire entre min et max
	* @param Number min
	* @param Number max
	* @return Number
	* 
	*/
	public function randRange(min:Number, max:Number) {
	     var randomNum:Number = Math.floor(Math.random() * (max - min + 1)) + min;
	     return randomNum;
	}
	
	/**
	* Fonction qui renvoi le tableau mélangé
	* @param Array tab0_array le tableau d'origine
	* @return Array
	*/
	public function randomTab(tab0_array:Array) {
	    var tab1_array:Array = new Array();
	    var tabTmp_array:Array = new Array();
	    var cpt:Number = 0;
	    for(var i:Number = 0 ; i < tab0_array.length ; i++) {
	        tabTmp_array.push(false);
	    }
	    while(cpt < tab0_array.length) {
	        var j:Number = randRange(0, tab0_array.length-1);
	        if(tabTmp_array[j] == false) {
	            tabTmp_array[j] = true;
	            tab1_array.push(tab0_array[j]);
	            cpt++;
	        }
	    }
	    return tab1_array;
	}
	
	/** 
	 * Nous dit si la valeur needle est présente dans un tableau haystack
	 */
	public function inArray(needle, haystack) {
		for (var i:Number = 0 ; i < haystack.length ; i++) {
			if (haystack[i] == needle) {
				return true;
			}
		}
		return false;
	}
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/** Initialisation de l'interface de jeu, appelé à chaque clic sur le bouton */
	private function initInterface() {
		var bWin:Boolean = false;
		// On vérifie les condition de victoire, 
		if (this.p_bRun == true) {
			bWin = this.checkVictory();
		}
		// Premiere partie ou au moins un numéro placé dans l'entonnoire
		if ((this.p_bRun == false || bWin == true) && this.p_nPoint < 4) {
			this.p_bRun = true;
			// On tire et on place les 10 valeurs 
			this.displayValues();
			
			// On les rend dropable
			for (var i:Number = 1 ; i <= 8 ; ++i) {
				this["p_mcValue"+i].onPress = function () {
					this.old_depth = this.getDepth();
					this.swapDepths(_parent.getNextHighestDepth());
				
					this.init_x = this._x;
					this.init_y = this._y;
					this._x = _parent._xmouse;
					this._y = _parent._ymouse;
					this.startDrag();
				} 
				this["p_mcValue"+i].onRelease = function () {
					_parent.releaseNumber(this);
				}			
			}
		}
		// S'il a 4 points, il gagne
		if (this.p_nPoint == 4) {			
			soundPlayer.playASound("frele2");
			gotoAndStop("fin");
			this.p_mcParchemin.onPress = function() { 
				_parent.soundPlayer.playASound("shark2");
				_parent.endGame();
			}
		}
	}
	
	/** Vérification des conditions de victoire 
	* @return Boolean
	*/
	private function checkVictory() {
		var bFlag:Boolean = false;
		var bLost:Boolean = false;
		var nNombre:Number = 0;
		
		soundPlayer.playASound("minitic");
		// On regarde le nombre de valeurs placé en haut
		for (var i:Number = 1 ; i <= 8 ; ++i) {
			bFlag = false;
			for (var j:Number = 1 ; j <= 8 ; ++j) {
				if (this["p_mcRepere"+i]._x == this["p_mcValue"+j]._x && 
					this["p_mcRepere"+i]._y == this["p_mcValue"+j]._y) {
					// il y a  quelqu'un sur cette case
					bFlag = true;
					// on vérifie s'il y a une valeur qui devrait etre dans l'entonnoire
					if (Number(this["p_mcValue"+j].txtValue.text) > this.p_nValueMin &&
						Number(this["p_mcValue"+j].txtValue.text) < this.p_nValueMax) {
						bLost = true;
					}
				}	
			}
			if (bFlag == true) {
				++nNombre
			}
		}
		// Si rien n'a été placé, on ne fait rien
		if (nNombre == 8) {
			return false;
		}
		// On vérifie qu'il n'a pas placé un chiffre incorrecte au millieu
		for (var i:Number = 1 ; i <= 8 ; ++i) {
			for (var j:Number = 1 ; j <= 8 ; ++j) {
				if (this["p_mcRepereCentre"+i]._x == this["p_mcValue"+j]._x && 
					this["p_mcRepereCentre"+i]._y == this["p_mcValue"+j]._y) {
					// il y a  quelqu'un sur cette case
					// on vérifie s'il y a une valeur qui ne devrait pas etre dans l'entonnoire 
					if (Number(this["p_mcValue"+j].txtValue.text) < this.p_nValueMin ||
						Number(this["p_mcValue"+j].txtValue.text) > this.p_nValueMax) {
						bLost = true;
					}
				}	
			}
		}
		
		// Si il a bien placé une valeur mais qu'il en reste qui devrait etre placé
		// il perd un point et on ne replace pas les valeurs
		if (bLost == true) {
			if (this.p_nPoint > 0) {
				--this.p_nPoint;
			}
			this.p_mcPoint.gotoAndStop(this.p_nPoint + 1)
			return false;
		}
		
		// Sinon il gagne un point et on replace tout
		++this.p_nPoint;
		soundPlayer.playASound("clicClac6");
		
		this.p_mcPoint.gotoAndStop(this.p_nPoint + 1)
		return true
	}
	
	/** Appelé quand on lache un nombre */
	private function releaseNumber(mcNumber:MovieClip) {
		var bLocalisation:Boolean = false;
		var bFlag:Boolean = false;
		soundPlayer.playASound("minitic");
		// Si on le lache dans l'entonnoir
		if (mcNumber.hitTest(this.p_mcEntonnoir)) {
			// On regarde sur chaque localisation centrale
			for (var i:Number = 1 ; i <= 8 ; ++i) {
				bFlag = false;
				// Si quelqu'un est dessus
				for (var j:Number = 1 ; j <= 8 ; ++j) {
					// Si la localisation correspond à celle d'un chiffre
					if (this["p_mcRepereCentre"+i]._x == this["p_mcValue"+j]._x && 
						this["p_mcRepereCentre"+i]._y == this["p_mcValue"+j]._y) {
						// il y a dejà quelqu'un 
						bFlag = true;
						break;
					}					
				}
				if (bFlag == false) {
					// On place le numéro à cet endroit
					mcNumber._x = this["p_mcRepereCentre"+i]._x;
					mcNumber._y = this["p_mcRepereCentre"+i]._y;
					bLocalisation = true;
					break;
				}
			}
		}
		// Sinon on regarde si on l'a draggué en haut
		if (bLocalisation == false) {
			for (var i:Number = 1 ; i <= 8 ; ++i) {
				if (mcNumber.hitTest(this["p_mcRepere"+i])) {
					bFlag = false;
					for (var j:Number = 1 ; j <= 8 ; ++j) {
						if (this["p_mcRepere"+i]._x == this["p_mcValue"+j]._x && 
							this["p_mcRepere"+i]._y == this["p_mcValue"+j]._y) {
							// il y a dejà quelqu'un 
							bFlag = true;
							break;
						}	
					}
					if (bFlag == false) {
						// On place le numéro à cet endroit
						mcNumber._x = this["p_mcRepere"+i]._x;
						mcNumber._y = this["p_mcRepere"+i]._y;
						bLocalisation = true;
						break;
					}
				}
			}				
		}
		// S'il n'a pas été placé, on le remet en place
		if (bLocalisation == false) {
			mcNumber._x = mcNumber.init_x;
			mcNumber._y = mcNumber.init_y;
		}
		mcNumber.swapDepths(mcNumber.old_depth);
		mcNumber.stopDrag();
	}
		
	/** Affichage des valeurs */
	private function displayValues() {
		var nMarqueur:Number = 0;
		var nValue:Number;
		while (nMarqueur < 10) {
			nValue = randRange(50, 199);
		
			// Si cette valeur n'a pas déjà été tiré
			if (!inArray(nValue, this.p_aValue)) {
				this.p_aValue[nMarqueur++] = nValue;
			}
		}
		// On tri le tableau
		this.p_aValue.sort(Array.NUMERIC);
		
		// On choisi nos deux valeurs de droite et gauche
		this.p_nValueMin = this.p_aValue[randRange(0, 3)];
		this.p_nValueMax = this.p_aValue[(randRange(0, 3) + 6)];
		
		// On remelange le tableau
		this.p_aValue = randomTab(this.p_aValue);
		
		nValue = 1;
		for (var i:Number = 0 ; i <= 9 ; i++) {
			if (this.p_aValue[i] == this.p_nValueMin) {
				this.p_mcValueGauche.removeMovieClip();
				this.attachMovie("chiffre", "p_mcValueGauche", this.getNextHighestDepth(), {_x:this.p_mcRepereGauche._x, _y:this.p_mcRepereGauche._y});
				this.p_mcValueGauche.txtValue.text = this.p_aValue[i];
			} else if (this.p_aValue[i] == this.p_nValueMax) {
				this.p_mcValueDroite.removeMovieClip();
				this.attachMovie("chiffre", "p_mcValueDroite", this.getNextHighestDepth(), {_x:this.p_mcRepereDroite._x, _y:this.p_mcRepereDroite._y});
				this.p_mcValueDroite.txtValue.text = this.p_aValue[i];
			} else {
				this["p_mcValue"+nValue].removeMovieClip();
				this.attachMovie("chiffre", "p_mcValue"+nValue, this.getNextHighestDepth(), {_x:this["p_mcRepere"+nValue]._x, _y:this["p_mcRepere"+nValue]._y});
				this["p_mcValue"+nValue].txtValue.text = this.p_aValue[i];
				++nValue;
			}
		}
	}
	
	/** Fin du jeu */
	private function endGame() {
		
		// On enleve tous les numéros
		for (var i:Number = 1 ; i <= 8 ; i++) {
			this["p_mcValue"+i].removeMovieClip();
		}
		this.p_mcValueGauche.removeMovieClip();
		this.p_mcValueDroite.removeMovieClip();
		// On va a la derniere frame
		 
		this._parent._parent.setWinGame(14, this._parent._parent.joueur1);
		this.gotoAndStop("fin2");
		_parent._parent.barreInterface_mc.showParchemin();
		gameFinished()
	}
}	

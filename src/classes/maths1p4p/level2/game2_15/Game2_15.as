﻿/**
 * class com.maths1p4p.Game2_15
 * 
 * @author Yoann Augen
 * @version 1.0
 *
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level2.game2_15.Game2_15 extends maths1p4p.AbstractGame {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 15;
	/** Bouton de départ et validation */
	public var p_mcBouton:MovieClip;
	
	/** Prise du parchemin */
	public var p_mcParchemin:MovieClip;
	
	/** Verroux */
	public var p_mcVerrou:MovieClip;
	
	/** Les 5 pions */
	public var p_mcPion1:MovieClip;
	public var p_mcPion2:MovieClip;
	public var p_mcPion3:MovieClip;
	public var p_mcPion4:MovieClip;
	public var p_mcPion5:MovieClip;
	
	/** Leur position de départ */
	public var p_mcTrouInit1:MovieClip;
	public var p_mcTrouInit2:MovieClip;
	public var p_mcTrouInit3:MovieClip;
	public var p_mcTrouInit4:MovieClip;
	public var p_mcTrouInit5:MovieClip;
	
	/** Les trous */
	public var p_mcTrou_0_0:MovieClip;
	public var p_mcTrou_0_1:MovieClip;
	public var p_mcTrou_0_2:MovieClip;
	public var p_mcTrou_0_3:MovieClip;
	public var p_mcTrou_0_4:MovieClip;
	public var p_mcTrou_1_0:MovieClip;
	public var p_mcTrou_1_1:MovieClip;
	public var p_mcTrou_1_2:MovieClip;
	public var p_mcTrou_1_3:MovieClip;
	public var p_mcTrou_1_4:MovieClip;
	public var p_mcTrou_2_0:MovieClip;
	public var p_mcTrou_2_1:MovieClip;
	public var p_mcTrou_2_2:MovieClip;
	public var p_mcTrou_2_3:MovieClip;
	public var p_mcTrou_2_4:MovieClip;
	public var p_mcTrou_3_0:MovieClip;
	public var p_mcTrou_3_1:MovieClip;
	public var p_mcTrou_3_2:MovieClip;
	public var p_mcTrou_3_3:MovieClip;
	public var p_mcTrou_3_4:MovieClip;
	public var p_mcTrou_4_0:MovieClip;
	public var p_mcTrou_4_1:MovieClip;
	public var p_mcTrou_4_2:MovieClip;
	public var p_mcTrou_4_3:MovieClip;
	public var p_mcTrou_4_4:MovieClip;
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	/** Nombre de victoire (3 pour gagner) */
	private var p_nVictory;
	
	/** Etat du jeu et du bouton correspondant, 0 = départ, 1 = J'ai fini, 2 = autre */
	private var p_nGameState;
	
	/** Si le jeu est fini ou pas */
	private var p_bFinish;
	
	/** Le numéro du pion bloqué */
	private var p_nPionLock;
	
	 
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game2_15() {
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		this.p_nVictory = 0;
		this.p_nGameState = 0;
		this.p_bFinish = false;
 	}
	
	/**
	* Fonction qui renvoi un nombre aleatoire entre min et max
	* @param Number min
	* @param Number max
	* @return Number
	*/
	public function randRange(min:Number, max:Number) {
	     var randomNum:Number = Math.floor(Math.random() * (max - min + 1)) + min;
	     return randomNum;
	}
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	private function initInterface() {
		var mcPion:MovieClip;
		var nTemp:Number;
		var xTemp:Number;
		var yTemp:Number;
		
		soundPlayer.playASound("psh4");
		// On initialise les 5 pions 
		for (var i:Number = 1 ; i <= 5 ; ++i) {
			this["p_mcPion"+i]._x = this["p_mcTrouInit"+i]._x;
			this["p_mcPion"+i]._y = this["p_mcTrouInit"+i]._y;
		}
		 	
		// On place un pion au hasard sur le plateau
		this.p_nPionLock  = 5;
		xTemp 	  = randRange(0, 4);
		yTemp	  = randRange(0, 4); 
		this["p_mcPion"+this.p_nPionLock]._x = this["p_mcTrou_"+xTemp+"_"+yTemp]._x;
		this["p_mcPion"+this.p_nPionLock]._y = this["p_mcTrou_"+xTemp+"_"+yTemp]._y;
		
		// On rend dropable les 5 pions
		for (var i:Number = 1 ; i <= 5 ; ++i) {
			if (this.p_bFinish == true) {
				break;	
			}
			if (i == this.p_nPionLock) {
				continue;	
			}
			mcPion = this["p_mcPion"+i];
			mcPion.j = i;
			mcPion.onPress = function () {
				// On le met au dessus
				this.old_depth = this.getDepth();
				this.swapDepths(_parent.getNextHighestDepth());
				// Loc en pixel
				this.init_x = this._x;
				this.init_y = this._y;
				// Loc dans le tableau init hors du tableau
				this._x = _parent._xmouse;
				this._y = _parent._ymouse;
				this.startDrag();
			}
			mcPion.onRelease = function () {
				this.swapDepths(this.old_depth);
				_parent.releaseNumber(this);
			}
		}
		// On rend cliquable le bouton de validation 
		this.p_mcBouton.onPress = function () {
			_parent.soundPlayer.playASound("psh4");
			_parent.checkWin();
		}
	}
	
	/** 
	 * Gestion du lachage d'un pion 
	 * @param MovieClip mcPion
	 */
	private function releaseNumber(mcPion:MovieClip) {
		var bFlag:Boolean = false;
		var bOccupe:Boolean = false;
		// On vérifie si le lieu ou le pion est laché est un trou
		for (var x:Number = 0 ; x <= 4 ; ++x) {
			for (var y:Number = 0 ; y <= 4 ; ++y) {
				if (mcPion.hitTest(this["p_mcTrou_"+x+"_"+y]) && bFlag == false) {
					soundPlayer.playASound("minitic");
					// on verifie qu'il n'y ai personne sur cette case 
					for (var i:Number = 1 ; i <= 5 ; ++i) {
						if (this["p_mcPion"+i]._x == this["p_mcTrou_"+x+"_"+y]._x && 
							this["p_mcPion"+i]._y == this["p_mcTrou_"+x+"_"+y]._y) {
							bOccupe = true;	
						}
					}
					// S'il n'y a personne sur cette case, on l'y met
					if (bOccupe == false) {
						// On déplace le pion
						mcPion._x = this["p_mcTrou_"+x+"_"+y]._x;
						mcPion._y = this["p_mcTrou_"+x+"_"+y]._y;
						
						bFlag = true;
					}
				} 		
			}
		}
		if (bFlag == false) {
			// Il repart sur la case d'origine si pas de hittest reussi
			mcPion._x = mcPion.init_x;
			mcPion._y = mcPion.init_y;
		}
		mcPion.stopDrag();				
	}
	
	/** 
	 * Vérification des conditions de victoire 
	 * @return boolean
	 */
	private function checkWin() {
		var nTemp:Number;
		// Création du tableau 5 x 5
		var aTab:Array = new Array(new Array(), new Array(), new Array(),new Array(), new Array());
		// On l'initialise 
		for (var x:Number = 0 ; x <= 4 ; ++x) {
			for (var y:Number = 0 ; y <= 4 ; ++y) {
				// Pour chaque case on regarde si un pion est présent
				for (var i:Number = 1 ; i <= 5 ; ++i) {
					if (this["p_mcPion"+i]._x == this["p_mcTrou_"+x+"_"+y]._x && 
						this["p_mcPion"+i]._y == this["p_mcTrou_"+x+"_"+y]._y) {
						aTab[x][y] = 1;
					}
				}
				if (aTab[x][y] != 1) {
					aTab[x][y] = 0;
				}
			}
		}
		// Vérification verticale
		for (var x:Number = 0 ; x <= 4 ; ++x) {
			// Pour chaque colonne, on va compter le nombre de case pleine
			nTemp = 0
			for (var y:Number = 0 ; y <= 4 ; ++y) {
				if (aTab[x][y] == 1) {
					++nTemp;	
				}
			}
			// défaite si != 1 
			if (nTemp != 1) {
				this.lostPart();
				return false;
			}
		}
		// Vérification horizontale
		for (var y:Number = 0 ; y <= 4 ; ++y) {
			// Pour chaque colonne, on va compter le nombre de case pleine
			nTemp = 0
			for (var x:Number = 0 ; x <= 4 ; ++x) {
				if (aTab[x][y] == 1) {
					++nTemp;	
				}
			}
			// défaite si != 1 
			if (nTemp != 1) {
				this.lostPart();
				return false;
			}
		}
		// Vérification diagonales
		if ((aTab[0][1] + aTab[1][0]) > 1 ||
			(aTab[0][2] + aTab[1][1] + aTab[2][0]) > 1 ||
			(aTab[0][3] + aTab[1][2] + aTab[2][1] + aTab[3][0]) > 1 ||
			(aTab[0][4] + aTab[1][3] + aTab[2][2] + aTab[3][1] + aTab[4][0]) > 1 ||
			(aTab[1][4] + aTab[2][3] + aTab[3][2] + aTab[4][1]) > 1 ||
			(aTab[2][4] + aTab[3][3] + aTab[4][2]) > 1 ||
			(aTab[3][4] + aTab[4][3]) > 1 ||
			(aTab[3][0] + aTab[4][1]) > 1 ||
			(aTab[2][0] + aTab[3][1] + aTab[4][2]) > 1 ||
			(aTab[1][0] + aTab[2][1] + aTab[3][2] + aTab[4][3]) > 1 ||
			(aTab[0][0] + aTab[1][1] + aTab[2][2] + aTab[3][3] + aTab[4][4]) > 1 ||
			(aTab[0][1] + aTab[1][2] + aTab[2][3] + aTab[3][4]) > 1 ||
			(aTab[0][2] + aTab[1][3] + aTab[2][4]) > 1 ||
			(aTab[0][3] + aTab[1][4]) > 1) {
			this.lostPart();
			return false;
		} 
		// On a bien un pion partout, on gagne
		this.winPart();
			
		return true;
	}
	
	/** 
	 * Met à jour une victoire
	 */
	private function winPart() {
		this.p_nVictory++;
		this.p_mcVerrou.gotoAndStop(this.p_nVictory + 1);
		if (this.p_nVictory == 3) {
			this.gotoAndStop("win1");
			this.endGame();	
		} else {
			// On repart pour une partie
			this.initInterface();	
		}
	}
	/** 
	 * Met à jour une défaite
	 */
	private function lostPart() {
		if (this.p_nVictory > 0) {
			this.p_nVictory--;
			this.p_mcVerrou.gotoAndStop(this.p_nVictory + 1);
		}
	}
	/** Fin du jeu */
	private function endGame() {
		this.p_bFinish = true;
		
		// On désactive les drag
		for (var i:Number = 1 ; i <= 5 ; ++i) {
			this["p_mcPion"+i].enabled = false;
		}
		
		// On rend le parchemin cliquable
		this.p_mcParchemin.onPress = function() {
			_parent.soundPlayer.playASound("shark2");
			
			//on enregistre la réussite du jeu
			this._parent._parent._parent.setWinGame(15, this._parent._parent._parent.joueur1); 
			_parent._parent._parent.barreInterface_mc.showParchemin();
			_parent.gotoAndStop("win2");
			gameFinished();
		}	
	}
}

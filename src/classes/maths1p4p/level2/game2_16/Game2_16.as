﻿/**
 * class com.maths1p4p.Game2_16
 * 
 * @author Yoann Augen
 * @version 1.0
 *
 * TO DO :  
 * => Gestion des deux joueurs
 * 
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level2.game2_16.Game2_16 extends maths1p4p.AbstractGame {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 16;
	/** Les differentes cases contenant les chiffres */
	public var p_mcNum_0_0:MovieClip;
	
	public var p_mcNum_1_1:MovieClip;
	public var p_mcNum_2_1:MovieClip;
	public var p_mcNum_3_1:MovieClip;
	public var p_mcNum_4_1:MovieClip;
	public var p_mcNum_5_1:MovieClip;
	public var p_mcNum_6_1:MovieClip;
	public var p_mcNum_7_1:MovieClip;
	public var p_mcNum_8_1:MovieClip;
	public var p_mcNum_9_1:MovieClip;
		
	public var p_mcNum_1_2:MovieClip;
	public var p_mcNum_2_2:MovieClip;
	public var p_mcNum_3_2:MovieClip;
	public var p_mcNum_4_2:MovieClip;
	public var p_mcNum_5_2:MovieClip;
	public var p_mcNum_6_2:MovieClip;
	public var p_mcNum_7_2:MovieClip;
	public var p_mcNum_8_2:MovieClip;
	public var p_mcNum_9_2:MovieClip;
		
	public var p_mcNum_1_3:MovieClip;
	public var p_mcNum_2_3:MovieClip;
	public var p_mcNum_3_3:MovieClip;
	public var p_mcNum_4_3:MovieClip;
	public var p_mcNum_5_3:MovieClip;
	public var p_mcNum_6_3:MovieClip;
	public var p_mcNum_7_3:MovieClip;
	public var p_mcNum_8_3:MovieClip;
	public var p_mcNum_9_3:MovieClip;
		
	public var p_mcNum_1_4:MovieClip;
	public var p_mcNum_2_4:MovieClip;
	public var p_mcNum_3_4:MovieClip;
	public var p_mcNum_4_4:MovieClip;
	public var p_mcNum_5_4:MovieClip;
	public var p_mcNum_6_4:MovieClip;
	public var p_mcNum_7_4:MovieClip;
	public var p_mcNum_8_4:MovieClip;
	public var p_mcNum_9_4:MovieClip;
		
	public var p_mcNum_1_5:MovieClip;
	public var p_mcNum_2_5:MovieClip;
	public var p_mcNum_3_5:MovieClip;
	public var p_mcNum_4_5:MovieClip;
	public var p_mcNum_5_5:MovieClip;
	public var p_mcNum_6_5:MovieClip;
	public var p_mcNum_7_5:MovieClip;
	public var p_mcNum_8_5:MovieClip;
	public var p_mcNum_9_5:MovieClip;
	
	public var p_mcNum_1_6:MovieClip;
	public var p_mcNum_2_6:MovieClip;
	public var p_mcNum_3_6:MovieClip;
	public var p_mcNum_4_6:MovieClip;
	public var p_mcNum_5_6:MovieClip;
	public var p_mcNum_6_6:MovieClip;
	public var p_mcNum_7_6:MovieClip;
	public var p_mcNum_8_6:MovieClip;
	public var p_mcNum_9_6:MovieClip;
		
	public var p_mcNum_9_7:MovieClip;
		
	// Champs de saisi	
	public var p_mcTextfield:MovieClip;
	// case actuelle	
	public var p_mcCaseActu:MovieClip;
	// Tour des joueurs
	public var p_mcTour1:MovieClip;
	public var p_mcTour2:MovieClip;
	// Longueur de la chaine
	public var p_nLongueur:Number;
	// Bouton de fin de jeu
	public var p_mcBoutonFin:MovieClip;
	
	// Joueur un ou deux à jouer
	public var p_nTour;
	// Joueur 1 ou 2 victorieux
	public var p_nVictoire;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	// Tableau contenant les propriétés des cases
	// -1 = case bloqué
	// 1 = joueur 0
	// 1 = joueur 1
	// 2 = joueur 2
	// 3 = neutre
	// 4 = inspection ok faite
	// 5 = inspection pas ok faite
	var p_aTabCase:Array = new Array(new Array(), new Array(), new Array(), new Array(), new Array(), new Array(), new Array(), new Array(), new Array(), new Array());
	 
	// Score des joueurs
	var p_mcScore1:MovieClip;
	var p_mcScore2:MovieClip;
	
	/** Les joueurs **/
	private var joueur1:maths1p4p.level2.Level2Player;
	private var nomJoueur1_txt:TextField;
	private var joueur2:maths1p4p.level2.Level2Player;
	private var nomJoueur2_txt:TextField;
	
	// Controle de la victoire
	var p_bWin:Boolean;
	
	 
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game2_16() {
		super();
		this.joueur1 = this._parent._parent.joueur1;
		this.nomJoueur1_txt.text = this.joueur1.getName();
		this.joueur2 = this._parent._parent.joueur2;
		this.nomJoueur2_txt.text = this.joueur2.getName();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";

 	}
	/**
	* Fonction qui renvoi un nombre aleatoire entre min et max
	* @param Number min
	* @param Number max
	* @return Number
	*/
	public function randRange(min:Number, max:Number) {
	     var randomNum:Number = Math.floor(Math.random() * (max - min + 1)) + min;
	     return randomNum;
	}
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	private function initInterface() {
		 
		
		var mcTemp:MovieClip;
		// Tirage du premier chiffre
		var nInit:Number = this.randRange(1, 10);
		soundPlayer.playASound("rape1");
		this.p_bWin = false;
		this.changeTour();
		this.p_mcScore1.txtPoint.text = 0;
		this.p_mcScore2.txtPoint.text = 0;
		
		nInit = Number(""+nInit+"0");
		
		// Création de la premiere case 
		attachMovie("num", "p_mcNum_0_0", this.getNextHighestDepth(), {_x:157, _y:39});
		this.p_mcNum_0_0.txtValue.text = nInit;
		
		this.p_nLongueur = 0;
				
		// Création des cases suivante
		for (var y:Number = 1 ; y <= 6 ; ++y) {
			for (var x:Number = 0 ; x <= 9 ; ++x) {
				if ((x == 8 && y == 1) || (x == 9 && y == 1) || (x == 9 && y == 2)) {
					this.p_aTabCase[x][y] = -1;
				} else {
					this.p_aTabCase[x][y] = 0;
				}
				if (x == 0) {
					// On le place en fonction du clip du dessus
					attachMovie("num", "p_mcNum_"+x+"_"+y, this.getNextHighestDepth(), {_x:this["p_mcNum_"+x+"_"+(y - 1)]._x, _y:(this["p_mcNum_"+x+"_"+(y - 1)]._y + 31)});
					this["p_mcNum_"+x+"_"+y].txtValue.text = Number(this["p_mcNum_"+x+"_"+(y - 1)].txtValue.text) + 100;
				} else {
					// On le place en fonction du clip de gauche
					attachMovie("num", "p_mcNum_"+x+"_"+y, this.getNextHighestDepth(), {_x:((this["p_mcNum_"+(x - 1)+"_"+y])._x + 36), _y:(this["p_mcNum_"+(x - 1)+"_"+y]._y)});
					this["p_mcNum_"+x+"_"+y].txtValue.text = Number(this["p_mcNum_"+(x - 1)+"_"+y].txtValue.text) + 10;
				}
				mcTemp = this["p_mcNum_"+x+"_"+y];
				mcTemp.x = x;
				mcTemp.y = y;
				if (!((x == 8 && y == 1) || (x == 9 && y == 1) || (x == 9 && y == 2))) {
					// on rend le nombre invisible
					mcTemp.txtValue._visible = false;
					// On les rends sensible au clic
					mcTemp.onPress = function () {
						if (_parent.p_bWin == false) {
							_parent.p_mcCaseActu = this;
							_parent.p_mcCaseActu.x = this.x;
							_parent.p_mcCaseActu.y = this.y;
							// Placement du champs texte
							_parent.attachMovie("champ", "p_mcTextfield", _parent.getNextHighestDepth(), {_x:this._x, _y:this._y});
							//le champ n'accepte que des chiffres
							_parent.p_mcTextfield.txtValue.restrict = "0-9"; 
							//le champ n'accepte que 3 chiffres
							_parent.p_mcTextfield.txtValue.maxChars = 3; 
							
							// On lui donne le focus
							Selection.setFocus(_parent.p_mcTextfield.txtValue);
							// S'il presse un bouton
							Key.addListener(_parent.p_mcTextfield);
							_parent.p_mcTextfield.onKeyDown = function() {
								// il à appuyé sur entrée, on valide le chiffre
								if (Key.getCode() == Key.ENTER) {
									_parent.soundPlayer.playASound("Psh1");
									_parent.controleNombre(this.txtValue.text);
									// On supprime le champs
									this.removeMovieClip();
								}						
							}
							// On prévoit la perte du focus en supprimant le champs texte et en enlevant le listeneur
							_parent.p_mcTextfield.txtValue.onKillFocus = function() {
								_parent._parent.p_mcTextfield.removeMovieClip();		
							};
						}
					}
				}
			}	
		}
		// Création de la derniere case 
		attachMovie("num", "p_mcNum_9_7", this.getNextHighestDepth(), {_x:this["p_mcNum_9_6"]._x, _y:(this["p_mcNum_9_6"]._y + 31)});
		this.p_mcNum_9_7.txtValue.text = Number(this["p_mcNum_9_6"].txtValue.text) + 100;
		
		// Bouton de fin de jeu
		this.p_mcBoutonFin.onPress = function () {
			if (_parent.valideFin() == false) {
				// La partie n'est pas fini, jouer son	
			} else {
				_parent.p_bWin = true;
				_parent.soundPlayer.playASound("rape1");
				// Partie fini, on fait apparaitre le parchemin
				if (Number(_parent.p_mcScore1.txtPoint.text) > Number(_parent.p_mcScore2.txtPoint.text)) {
					_parent.p_nVictoire = 1;
					_parent.gotoAndPlay("win");
				} else if (Number(_parent.p_mcScore1.txtPoint.text) < Number(_parent.p_mcScore2.txtPoint.text)){
					_parent.p_nVictoire = 2;
					_parent.gotoAndPlay("win");
				} else {
					trace("match nul");
					_parent.soundPlayer.playASound("matchNul");
					// jouer son 
				}
			}
		}
		
	}
	
	/** Change le tour */
	private function changeTour() {
		if (this.p_nTour == 1) {
			this.p_nTour = 2;	
			this.p_mcTour2._visible = true;
			this.p_mcTour1._visible = false;
		} else if (this.p_nTour == 2) {
			this.p_nTour = 1;			
			this.p_mcTour1._visible = true;
			this.p_mcTour2._visible = false;
		} else {
			this.p_nTour = 1;	
			this.p_mcTour1._visible = true;
			this.p_mcTour2._visible = false;
		}
	}

	/** Controle que le nombre rentré est correct */
	private function controleNombre(nVal:String) {
		// On affiche la case grisé si le nombre est correct
		if (this.p_mcCaseActu.txtValue.text == nVal) {
			this.p_mcCaseActu.txtValue._visible = true;
			this.p_mcCaseActu.enabled = false;
			attachMovie("case_grise", "case_grise", this.getNextHighestDepth(), {_x:this.p_mcCaseActu._x + 1, _y:this.p_mcCaseActu._y + 1 });
			this.p_mcCaseActu.swapDepths(this.getNextHighestDepth());
			// On la marque comme neutre
			this.p_aTabCase[this.p_mcCaseActu.x][this.p_mcCaseActu.y] = 3;
			// On cherche si on a au moins 5 carrés adjacent
			verifCase(this.p_mcCaseActu.x, this.p_mcCaseActu.y);
			trace ("-- Longueur : "+this.p_nLongueur+"--");
			
			// Si on a bien 5 cases 
			if (this.p_nLongueur >= 5) {
				// Gain des points 
				this["p_mcScore"+p_nTour].txtPoint.text = Number(this["p_mcScore"+p_nTour].txtPoint.text) + this.p_nLongueur;
				
				// On met à jour la couleur des cases	
				for (var y:Number = 1 ; y <= 6 ; ++y) {
					for (var x:Number = 0 ; x <= 9 ; ++x) {
						if (this.p_aTabCase[x][y] == 4) {
							// Rouge pour joueur 1, verte pour joueur 2
							if (this.p_nTour == 1) {
								this.p_aTabCase[x][y] = 1;
								soundPlayer.playASound("minitic");
								attachMovie("case_rouge", "case_rouge", this.getNextHighestDepth(), {_x:this["p_mcNum_"+x+"_"+y]._x + 1, _y:this["p_mcNum_"+x+"_"+y]._y + 1 });
							} else {
								this.p_aTabCase[x][y] = 2;
								soundPlayer.playASound("minitic");
								attachMovie("case_verte", "case_verte", this.getNextHighestDepth(), {_x:this["p_mcNum_"+x+"_"+y]._x + 1, _y:this["p_mcNum_"+x+"_"+y]._y + 1 });
							}								
							this["p_mcNum_"+x+"_"+y].swapDepths(this.getNextHighestDepth());
						}
					}	
				}
			}
			// remise à jour du tableau de parcours
			this.initTableau();
		}
		// Si on avait bien une valeur, on change de tour
		if (nVal != "") {
			this.changeTour();
		}
	}
	
	/** 
	 * Verification recursive du nombre de cases adjacentes 
	 * @param Number x
	 * @param Number y
	 */
	private function verifCase(x:Number, y:Number) {
		// Si l'on est sur une case neutre
		if (this.p_aTabCase[x][y] == 3) {
			
			// La case est neutre, elle devient parcouru
			this.p_aTabCase[x][y] = 4;
			
			// On augmente la taille
			++this.p_nLongueur;
			
			// Parcours recursif
			// Haut 
			if (y - 1 >= 1) {
				this.verifCase(x, y - 1);
			}
			// Bas 
			if (y + 1 <= 6) {
				this.verifCase(x, y + 1);
			}
			// gauche
			if (x - 1 >= 0) {
				this.verifCase(x - 1, y);
			}
			// droite
			if (x + 1 <= 9) {
				this.verifCase(x + 1, y);
			}
		} else if (this.p_aTabCase[x][y] == 0) {
			// Nous somme sur une case vide 
			this.p_aTabCase[x][y] == 5; 
		}
	}
	
	/** 
	 * Verification recursive du nombre de cases adjacentes vide
	 * @param Number x
	 * @param Number y
	 */
	private function verifCaseVide(x:Number, y:Number) {
		// Si l'on est sur une case neutre ou vide 
		if (this.p_aTabCase[x][y] == 0 || this.p_aTabCase[x][y] == 3) {
			
			// La case est vide, elle devient parcouru
			if (this.p_aTabCase[x][y] == 0) {
				this.p_aTabCase[x][y] = 5;
			} else {
				this.p_aTabCase[x][y] = 4;	
			}
			// On augmente la taille
			++this.p_nLongueur;
			
			// Parcours recursif
			// Haut 
			if (y - 1 >= 1) {
				this.verifCaseVide(x, y - 1);
			}
			// Bas 
			if (y + 1 <= 6) {
				this.verifCaseVide(x, y + 1);
			}
			// gauche
			if (x - 1 >= 0) {
				this.verifCaseVide(x - 1, y);
			}
			// droite
			if (x + 1 <= 9) {
				this.verifCaseVide(x + 1, y);
			}
		}
	}
	
	/** Initialise le tableau de parcours en gardant les chiffres neutres */
	private function initTableau() {
		this.p_nLongueur = 0;
		for (var y:Number = 1 ; y <= 6 ; ++y) {
			for (var x:Number = 0 ; x <= 9 ; ++x) {
				if ((x == 8 && y == 1) || (x == 9 && y == 1) || (x == 9 && y == 2)) {
					this.p_aTabCase[x][y] = -1;
				} else {
					// C'était une case neutre
					if (this.p_aTabCase[x][y] == 4 || this.p_aTabCase[x][y] == 3) {
						this.p_aTabCase[x][y] = 3;
					} else if (this.p_aTabCase[x][y] == 5) {
						// C'était une case vide
						this.p_aTabCase[x][y] = 0;
					}					
				}
			}
		}
	}
	
	/** Valide la fin du jeu en cherchant des chemins de plus de cinq cases */
	private function valideFin() {
		// Pour chaque case
		for (var y:Number = 1 ; y <= 6 ; ++y) {
			for (var x:Number = 0 ; x <= 9 ; ++x) {
				if (!((x == 8 && y == 1) || (x == 9 && y == 1) || (x == 9 && y == 2))) {
					// On parcours le terrain
					this.verifCaseVide(x, y);
					trace ("-- Longueur : "+this.p_nLongueur+"--");
					// Le jeu n'est pas fini, il reste des chaines de 5 cases
					if (this.p_nLongueur > 5) {
						this.initTableau();
						return false;
					}
					this.initTableau(); 
				}
			}
		}
		// Aucune chaine de plus de 5 cases n'a été trouvé
		return true;
	}
}

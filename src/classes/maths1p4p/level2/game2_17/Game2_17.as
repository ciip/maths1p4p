﻿/**
 * class maths1p4p.level2.game2_17.game2_17
 * 
 * @author Rémi Gay
 * @version 1.0
 *
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
//import maths1p4p.level2.game2_17.*
//import mx.controls.Alert;
import maths1p4p.utils.OutilsTableaux;
import maths1p4p.application.Application;

class maths1p4p.level2.game2_17.Game2_17 extends maths1p4p.AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 17;
	
	public static var occurenceGame2_17:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/***** variables en rapport avec les éléments déjà present sur la scène *****/
	private var op1_txt:TextField;
	private var op2_txt:TextField;
	private var res_txt:TextField;
	
	private var spot1_mc:MovieClip;
	private var spot2_mc:MovieClip;
	private var spot3_mc:MovieClip;
	private var spot4_mc:MovieClip;
	private var spot5_mc:MovieClip;
	private var spot6_mc:MovieClip;
	
	private var puzzle1_mc:MovieClip;
	private var puzzle2_mc:MovieClip;
	private var puzzle3_mc:MovieClip;
	private var puzzle4_mc:MovieClip;
	private var btFini_mc:MovieClip;
	private var tiroir_mc:MovieClip;

	/***** variables privées de la classe *****/
	private var score:Number;
	private var resultat:Number;


	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * s'occupe de l'initialisation graphique et l'attribution des evenements
	 */
	public function Game2_17()	{
		
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		Game2_17.occurenceGame2_17 = this;
		
		this.score = 0;
		
		this.spot1_mc._visible = false;
		this.spot2_mc._visible = false;
		this.spot3_mc._visible = false;
		this.spot4_mc._visible = false;
		this.spot5_mc._visible = false;
		this.spot6_mc._visible = false;		
		
		this.tiroir_mc._visible = false;
		this.puzzle1_mc._visible = false;
		this.puzzle2_mc._visible = false;
		this.puzzle3_mc._visible = false;
		this.puzzle4_mc._visible = false;
		
		//on restreint les caractères que le champs de saisie résultat peut accueillir
		this.res_txt.restrict = "0-9";
		this.res_txt.maxChars = 2;
		
		//initialisation des evenements sur les pièves de puzzle
		this.initialisePuzzle();
		
		this.nouvelleDonne();
		
		this.initialiseBoutonValider();
		
		//pour n'ajouter qu'une seule fois l'ecouteur
		this.initToucheEntree();

	}
	
	/**
	 * Fonction appelée à chaque nouvelle manche (création d'un nouveau parcours et réinitialisation des 
	 * nouveaux tuyaux
	 * @return rien
	 */
	public function nouvelleDonne() {
		
		this.res_txt.text = "";
		Selection.setFocus(this.res_txt);
		//générer un résultat qui n'est pas supérieur à 25 (donc entre 0 et 25)
		this.resultat = Math.floor(Math.random() * (25 - 0 + 1)) + 0;
		//le 1er nombre n'est pas supérieur à 50;
		var premierNbre:Number = Math.floor(Math.random() * (50 - resultat + 1)) + resultat;
		this.op1_txt.text = ""+premierNbre;
		//et donc le dernier nombre = premierNbre - resultat
		var deuxiemeNbre:Number = premierNbre - resultat;
		this.op2_txt.text = ""+deuxiemeNbre;		
		
	}
	

	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
				
	 /**
	 * Fonction qui initialise le click sur le parchemin
	 * @return rien
	 */
	 private function initialisePuzzle() {
		
		this.puzzle1_mc.onRelease = function()
		{
			_parent.soundPlayer.playASound("shark2");
			//refermer le tiroir (et rendre invisibler les pièces)
			this._parent.tiroir_mc._visible = false;
			this._visible = false;
			this._parent.puzzle2_mc._visible = false;
			this._parent.puzzle3_mc._visible = false;
			this._parent.puzzle4_mc._visible = false;
			
			this._parent.gameFinished(1);
		}
		
		this.puzzle2_mc.onRelease = function()
		{
			_parent.soundPlayer.playASound("shark2");
			//refermer le tiroir (et rendre invisibler les pièces)
			this._parent.tiroir_mc._visible = false;
			this._parent.puzzle1_mc._visible = false;
			this._visible = false;
			this._parent.puzzle3_mc._visible = false;
			this._parent.puzzle4_mc._visible = false;
			
			this._parent.gameFinished(2);
		}
		
		this.puzzle3_mc.onRelease = function()
		{
			_parent.soundPlayer.playASound("shark2");
			//refermer le tiroir (et rendre invisibler les pièces)
			this._parent.tiroir_mc._visible = false;
			this._parent.puzzle1_mc._visible = false;
			this._parent.puzzle2_mc._visible = false;
			this._visible = false;
			this._parent.puzzle4_mc._visible = false;
			
			this._parent.gameFinished(3);
		}
		
		this.puzzle4_mc.onRelease = function()
		{
			_parent.soundPlayer.playASound("shark2");
			//refermer le tiroir (et rendre invisibler les pièces)
			this._parent.tiroir_mc._visible = false;
			this._parent.puzzle1_mc._visible = false;
			this._parent.puzzle2_mc._visible = false;
			this._parent.puzzle3_mc._visible = false;
			this._visible = false;			
			
			this._parent.gameFinished(4);
		}
		

		
	 }
	 
	 /**
	 * Fonction qui vérifie les résultats du joueur, si ils sont mauvais le joueur perd 1 point et continue a 
	 * chercher sinon on génère une nouvelle donne
	 * @return rien
	 */
	 private function initialiseBoutonValider() {
		
		this.btFini_mc.enabled = true;
		this.btFini_mc.onRelease = function() {

			this._parent.verifResultat();
		}
		
	 }
	 
	 private function verifResultat()
	 {
		 trace("this.resultat => "+this.resultat)
					 trace("this.res_txt.text => "+this.res_txt.text)
		 soundPlayer.playASound("ticAigu");
		 trace(typeof(this.res_txt.text)+" typeof(this.resultat) => "+typeof(this.resultat))
		 if (this.res_txt.text != "" && this.res_txt.text != undefined) {
			 if(this.res_txt.text == this.resultat) {
				//on ajoute un point, si le joueur a 6 points il a gagné et on efface les champs de saisie 
				//(_visible false) + on desactive le bouton + on ouvre le tiroir avec les pièces dedans
				this.score++;
				//on allume la loupiotte correspondante
				trace(this.score)
				trace(this["spot"+this.score+"_mc"])
				this["spot"+this.score+"_mc"]._visible = true;
				
				//si le joueur a un score de 6 :
				if(this.score == 6)	{
					this.op1_txt._visible = false;
					this.op2_txt._visible = false;
					this.res_txt._visible = false;
					this.btFini_mc.enabled = false;
					
					this.tiroir_mc._visible = true;
					this.puzzle1_mc._visible = true;
					this.puzzle2_mc._visible = true;
					this.puzzle3_mc._visible = true;
					this.puzzle4_mc._visible = true;
				} else {
					this.nouvelleDonne();
				}
				
			} else {
				if(this.score != 0)	{
					//on efface la derniere loupiotte allumée
					this["spot"+this.score+"_mc"]._visible = false;
					this.score--;
				}
			}
		 }
	 }
	 
			
	 private function initToucheEntree() {
		 
		 var monEcouteur:Object = new Object();
		 monEcouteur.onKeyDown = function() 
		 {
			 trace ("****Appui sur touche du clavier détecté****");
			 if(Key.isDown(Key.ENTER)){
				 
				 //si la manche n'est pas terminé on teste le resultat
				 if(Game2_17.occurenceGame2_17.btFini_mc.enabled == true)
				 {
					 Game2_17.occurenceGame2_17.verifResultat();
				 }
			 }

		 };
		
		 Key.addListener(monEcouteur);
		 
	 }
	
	 
	 /**
	 * Fonction qui cloture la partie (enregistrement des scores etc..
	 * @return rien
	 */
	 private function gameFinished(numMorceauPuzzleChoisi) {
		
		//enregistrer le résultat => mémoriser que le joueur courant a bien finit ce jeu
		var state = this._parent._parent.joueur1.getState();
		if(state.childNodes[0]['jeu17'] == undefined) {
			var etatJeu = state.createElement('jeu17');
			state.childNodes[0].appendChild(etatJeu);
		} else {
			var etatJeu = state.childNodes[0]['jeu17'];
		}
		trace("numMorceauPuzzleChoisi => "+numMorceauPuzzleChoisi)
		etatJeu.attributes.value = numMorceauPuzzleChoisi;
		this._parent._parent.joueur1.setState(state);
		trace("this._parent._parent.joueur1.getState() => "+this._parent._parent.joueur1.getState())
		 
		this._parent._parent.setWinGame(17, this._parent._parent.joueur1);
		//afficher le parchemin dans la glissière de navigation (peut être fait lors de l'enregistrement du 
		//résultat
		this._parent._parent.barreInterface_mc.initBarreJeuxIle();
		
		super.gameFinished();
	 }
	 
	 	 
	
}

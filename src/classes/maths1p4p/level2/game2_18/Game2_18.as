﻿/**
 * class maths1p4p.level2.game2_18.game2_18
 * 
 * @author Rémi Gay
 * @version 1.0
 *
 * ETAT : a faire
 *
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
//import maths1p4p.level2.game2_18.*
//import mx.controls.Alert;
import maths1p4p.utils.OutilsTableaux;
import maths1p4p.application.Application;

class maths1p4p.level2.game2_18.Game2_18 extends maths1p4p.AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 18;
	
	public static var occurenceGame2_18:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/***** variables en rapport avec les éléments déjà present sur la scène *****/
	private var op1_txt:TextField;
	private var op2_txt:TextField;
	private var op3_txt:TextField;
	private var op4_txt:TextField;
	
	private var operateur1_txt:TextField;
	private var operateur2_txt:TextField;
	
	private var sceau1_mc:MovieClip;
	private var sceau2_mc:MovieClip;
	private var sceau3_mc:MovieClip;
	private var sceau4_mc:MovieClip;
	private var sceau5_mc:MovieClip;
	
	private var animManivelle_mc:MovieClip;
	
	private var puzzle1_mc:MovieClip;
	private var puzzle2_mc:MovieClip;
	private var puzzle3_mc:MovieClip;
	private var puzzle4_mc:MovieClip;
	private var btFini_mc:MovieClip;

	/***** variables privées de la classe *****/
	private var score:Number;
	private var resultat:Number;
	private var intervalId:Number;


	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * s'occupe de l'initialisation graphique et l'attribution des evenements
	 */
	public function Game2_18()	{
		
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		Game2_18.occurenceGame2_18 = this;
		
		this.score = 0;
		Game2_18.occurenceGame2_18.score = 0;
		
		this.sceau1_mc._visible = false;
		this.sceau2_mc._visible = false;
		this.sceau3_mc._visible = false;
		this.sceau4_mc._visible = false;
		this.sceau5_mc._visible = false;

		this.puzzle1_mc._visible = false;
		this.puzzle2_mc._visible = false;
		this.puzzle3_mc._visible = false;
		this.puzzle4_mc._visible = false;
		
		//on restreint les caractères que le champs de saisie résultat peut accueillir
		this.op4_txt.restrict = "0-9";
		this.op4_txt.maxChars = 2;
		
		//initialisation des evenements sur les pièves de puzzle
		this.initialisePuzzle();
		
		this.nouvelleDonne();
		
		this.initialiseBoutonValider();
		
		//pour n'ajouter qu'une seule fois l'ecouteur
		this.initToucheEntree();
		
		
	}
	
	/**
	 * Fonction appelée à chaque nouvelle manche (création d'un nouveau parcours et réinitialisation des 
	 * nouveaux tuyaux
	 * @return rien
	 */
	public function nouvelleDonne() {
		
		this.op4_txt.text = "";
		Selection.setFocus(this.op4_txt);
		
		//on tire au hasard un type d'opération (soustraction : 1 ou addition : 0) pour la 1ere et la 2ème opérations
		var typeOperation1:Number = this.resultat = Math.floor(Math.random() * (1 - 0 + 1)) + 0;
		var typeOperation2:Number = this.resultat = Math.floor(Math.random() * (1 - 0 + 1)) + 0;
		
		//générer les 2 première opérandes jamais plus haut que 10 (donc max 10+10) et le 1er supérieur au 2
		//ème dans le cas d'une soustraction. 
		var premiereOperande:Number = Math.floor(Math.random() * (10 - 0 + 1)) + 0;
		if(typeOperation1 == 1)
		{
			this.operateur1_txt.text = "-";
			trace("soustraction")
			var deuxiemeOperande:Number = Math.floor(Math.random() * (premiereOperande - 0 + 1)) + 0;
			var res1 = premiereOperande - deuxiemeOperande;
			
		}
		else
		{
			this.operateur1_txt.text = "+";
			trace("addition")
			var deuxiemeOperande:Number = Math.floor(Math.random() * (10 - 0 + 1)) + 0;
			var res1 = premiereOperande + deuxiemeOperande;
			
		}
		if(typeOperation2 == 0)
		{
			this.operateur2_txt.text = "+";
			//la 3ème opérande doit alors être inférieur ou égale au résultat dans le cas d'une addition
			//et supérieur à 0 bien sûr
			trace(res1)
			var troisiemeOperande = Math.floor(Math.random() * (res1 - 0 + 1)) + 0;
			this.resultat = res1 - troisiemeOperande;
		}
		else
		{
			this.operateur2_txt.text = "-";
			//la 3ème opérande doit alors être supérieur ou égale au résultat dans le cas d'une soustraction
			//et aussi inférieur a 20
			var troisiemeOperande = Math.floor(Math.random() * (20 - res1 + 1)) + res1;
			this.resultat = troisiemeOperande - res1;
		}
		
		//affectation des chiffres aux xhamps textes
		this.op1_txt.text = ""+premiereOperande;
		this.op2_txt.text = ""+deuxiemeOperande;
		this.op3_txt.text = ""+troisiemeOperande;
		
		//determination du resultat qu'il faudra tester lors de la verif
		
		
	}
	

	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
				
	 /**
	 * Fonction qui initialise le click sur le parchemin
	 * @return rien
	 */
	 private function initialisePuzzle() {
		
		this.puzzle1_mc.onRelease = function()
		{
			_parent.soundPlayer.playASound("shark2");
			//refermer le tiroir (et rendre invisibler les pièces)
			this._parent.sceau5_mc._visible = false;
			this._visible = false;
			this._parent.puzzle2_mc._visible = false;
			this._parent.puzzle3_mc._visible = false;
			this._parent.puzzle4_mc._visible = false;
			
			this._parent.gameFinished(1);
		}
		
		this.puzzle2_mc.onRelease = function()
		{
			_parent.soundPlayer.playASound("shark2");
			//refermer le tiroir (et rendre invisibler les pièces)
			this._parent.sceau5_mc._visible = false;
			this._parent.puzzle1_mc._visible = false;
			this._visible = false;
			this._parent.puzzle3_mc._visible = false;
			this._parent.puzzle4_mc._visible = false;
			
			this._parent.gameFinished(2);
		}
		
		this.puzzle3_mc.onRelease = function()
		{
			_parent.soundPlayer.playASound("shark2");
			//refermer le tiroir (et rendre invisibler les pièces)
			this._parent.sceau5_mc._visible = false;
			this._parent.puzzle1_mc._visible = false;
			this._parent.puzzle2_mc._visible = false;
			this._visible = false;
			this._parent.puzzle4_mc._visible = false;
			
			this._parent.gameFinished(3);
		}
		
		this.puzzle4_mc.onRelease = function()
		{
			_parent.soundPlayer.playASound("shark2");
			//refermer le tiroir (et rendre invisibler les pièces)
			this._parent.sceau5_mc._visible = false;
			this._parent.puzzle1_mc._visible = false;
			this._parent.puzzle2_mc._visible = false;
			this._parent.puzzle3_mc._visible = false;
			this._visible = false;			
			
			this._parent.gameFinished(4);
		}		
	 }
	 
	 /**
	 * Fonction qui vérifie les résultats du joueur, si ils sont mauvais le joueur perd 1 point et continue a 
	 * chercher sinon on génère une nouvelle donne
	 * @return rien
	 */
	 private function initialiseBoutonValider() {
		
		this.btFini_mc.enabled = true;
		this.btFini_mc.onRelease = function() {

			this._parent.verifResultat();
		}
		
	 }
	 
	 private function verifResultat()
	 {
		trace("this.resultat => "+this.resultat)
		trace("this.res_txt.text => "+this.op4_txt.text)
		if (this.op4_txt.text != "" && this.op4_txt.text != undefined) {
			//quelque soit le resultat : animation de la manivelle
			soundPlayer.playASound("grince3");
			this.animManivelle_mc.gotoAndPlay(2);
			trace("init intervale")
			declencheResultat(this);
			//this.intervalId = setInterval(this.declencheResultat, (13/25)*1000, this);
		}
	 }
	 
	 private function declencheResultat(occurenceDuJeu):Void {

		trace("Suppression intervale => "+occurenceDuJeu.intervalId)
		//clearInterval(occurenceDuJeu.intervalId);
		trace(occurenceDuJeu.op4_txt.text+" - "+occurenceDuJeu.resultat);
		
		if(occurenceDuJeu.op4_txt.text == occurenceDuJeu.resultat) 
		{
			//on ajoute un point, si le joueur a 6 points il a gagné et on efface les champs de saisie 
			//(_visible false) + on desactive le bouton + on ouvre le tiroir avec les pièces dedans
			occurenceDuJeu.score++;
			
			//declencher l'animation de la manivelle
			//A FAIRE  !!!
			//et monter le sceau en même temps
			occurenceDuJeu["sceau"+occurenceDuJeu.score+"_mc"]._visible = true;
			
			//si le joueur a un score de 6 :
			if(occurenceDuJeu.score == 5)
			{
				occurenceDuJeu.op1_txt._visible = false;
				occurenceDuJeu.op2_txt._visible = false;
				occurenceDuJeu.op3_txt._visible = false;
				occurenceDuJeu.op4_txt._visible = false;
				occurenceDuJeu.btFini_mc.enabled = false;
				
				occurenceDuJeu.puzzle1_mc._visible = true;
				occurenceDuJeu.puzzle2_mc._visible = true;
				occurenceDuJeu.puzzle3_mc._visible = true;
				occurenceDuJeu.puzzle4_mc._visible = true;
			}
			else
			{
				occurenceDuJeu.nouvelleDonne();
			}
			
		}
		else
		{
			if(occurenceDuJeu.score != 0)
			{
				//declencher l'animation de la manivelle  
				occurenceDuJeu["sceau"+occurenceDuJeu.score+"_mc"]._visible = false;
				occurenceDuJeu.score--;
			}
		}
		

	}
	 
			
	private function initToucheEntree() {
		var monEcouteur:Object = new Object();
		monEcouteur.onKeyDown = function() {
			trace ("TOUCHE");
			if(Key.isDown(Key.ENTER)){
				//si la manche n'est pas terminé on teste le resultat
				if(Game2_18.occurenceGame2_18.btFini_mc.enabled == true) {
					 Game2_18.occurenceGame2_18.verifResultat();
				}
			}
		 };		
		 Key.addListener(monEcouteur);		 
	 }
	
	 
	 /**
	 * Fonction qui cloture la partie (enregistrement des scores etc..
	 * @return rien
	 */
	 private function gameFinished(numMorceauPuzzleChoisi) {
		
		//enregistrer le résultat => mémoriser que le joueur courant a bien finit ce jeu
		var state = this._parent._parent.joueur1.getState();
		if(state.childNodes[0]['jeu18'] == undefined)
		{
			var etatJeu = state.createElement('jeu18');
			state.childNodes[0].appendChild(etatJeu);
		}
		else
		{
			var etatJeu = state.childNodes[0]['jeu18'];
		}
		trace("numMorceauPuzzleChoisi => "+numMorceauPuzzleChoisi)
		etatJeu.attributes.value = numMorceauPuzzleChoisi;
		this._parent._parent.joueur1.setState(state);
		trace("this._parent._parent.joueur1.getState() => "+this._parent._parent.joueur1.getState())
		this._parent._parent.setWinGame(18, this._parent._parent.joueur1);
		//afficher le parchemin dans la glissière de navigation (peut être fait lors de l'enregistrement du 
		//résultat
		this._parent._parent.barreInterface_mc.initBarreJeuxIle();
		
		super.gameFinished();
	 }
	 
	 	 
	
}

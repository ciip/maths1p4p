﻿/**
 * class com.maths1p4p.Game2_19
 * 
 * @author Yoann Augen
 * @version 1.0
 *
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level2.game2_19.Game2_19 extends maths1p4p.AbstractGame {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 19;
	
	/** La solution */
	public var p_mcSoluce:MovieClip;
	/** La solution précédente */
	public var p_old_mcSoluce:Number;
	
	/** Les trois champs de saisi */
	public var p_mcInput1:MovieClip;
	public var p_mcInput2:MovieClip;
	public var p_mcInput3:MovieClip;
	
	/** Le bouton de validation */ 
	public var p_mcBouton:MovieClip;
	
	/** L'accoupole */ 
	public var p_mcPoint:MovieClip;
	
	/** Les pieces de puzzle */
	public var p_mcPiece1:MovieClip;
	public var p_mcPiece2:MovieClip;
	public var p_mcPiece3:MovieClip;
	public var p_mcPiece4:MovieClip;
	public var p_mcPuzzle:MovieClip;
	public var p_mcPuzzle2:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var myInterval:Number;
	public var myIntervalTab:Number;
	
	/** Le nombre de point */
	private var p_nPoint;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game2_19() {
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		this.p_nPoint = 0;
		// On fixe les contraintes
		this.p_mcInput1.txtValue.restrict = "0-9"; 
		this.p_mcInput1.txtValue.maxChars = 2; 
		this.p_mcInput2.txtValue.restrict = "0-9"; 
		this.p_mcInput2.txtValue.maxChars = 2; 
		this.p_mcInput3.txtValue.restrict = "0-9"; 
		this.p_mcInput3.txtValue.maxChars = 2; 
	//	this.initToucheEntree();
		this.initInterface();
	}
	public function focus():Void{
		clearInterval(this.myInterval); 
		var texteFocus = this.p_mcInput1.txtValue; 
		Selection.setFocus(texteFocus);
	} 
	/**
	* Fonction qui renvoi un nombre aleatoire entre min et max
	* @param Number min
	* @param Number max
	* @return Number
	* 
	*/
	public function randRange(min:Number, max:Number) {
	     var randomNum:Number = Math.floor(Math.random() * (max - min + 1)) + min;
	     return randomNum;
	}
	
	/**
	* Fonction qui renvoi le tableau mélangé
	* @param Array tab0_array le tableau d'origine
	* @return Array
	*/
	public function randomTab(tab0_array:Array) {
	    var tab1_array:Array = new Array();
	    var tabTmp_array:Array = new Array();
	    var cpt:Number = 0;
	    for(var i:Number = 0 ; i < tab0_array.length ; i++) {
	        tabTmp_array.push(false);
	    }
	    while(cpt < tab0_array.length) {
	        var j:Number = randRange(0, tab0_array.length-1);
	        if(tabTmp_array[j] == false) {
	            tabTmp_array[j] = true;
	            tab1_array.push(tab0_array[j]);
	            cpt++;
	        }
	    }
	    return tab1_array;
	}
	
	/** 
	 * Nous dit si la valeur needle est présente dans un tableau haystack
	 */
	public function inArray(needle, haystack) {
		for (var i:Number = 0 ; i < haystack.length ; i++) {
			if (haystack[i] == needle) {
				return true;
			}
		}
		return false;
	}
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/** Initialisation de l'interface de jeu, appelé à chaque clic sur le bouton */
	private function initInterface() {
		// On tire un résultat
		do {
			this.p_mcSoluce.txtValue.text = randRange(30, 60);
		} while (this.p_old_mcSoluce == Number(this.p_mcSoluce.txtValue.text))
		
		trace (this.p_mcSoluce.txtValue.text);
		
		// On met un evenement de validation sur le bouton
		this.p_mcBouton.onRelease = function() {
			_parent.controlNumber();			
		}
		
		// On en met également un sur l'appuis du bouton 3
		// Le champs 3
		Key.addListener(this.p_mcInput3);
		this.p_mcInput3.onKeyDown = function() {
			if (Key.getCode() == Key.ENTER) {
				_parent.controlNumber();
			}				
		}
		Key.addListener(this);
		this.onKeyDown = function() { 
			if(Key.isDown(Key.TAB)){ 
			 	this.getNextFocus();
			}		
		};	 
			
		// On donne le focus au premier champs
		if(this.myInterval != undefined) {
			clearInterval(this.myInterval)
		}
		this.myInterval = setInterval(this,"focus", 100);		
		
	} 
	public function getNextFocus():Void{
	 	var avant = Selection.getFocus();
		if (avant.indexOf("p_mcInput1") > -1) {
			var apres = this.p_mcInput2.txtValue;
			trace("1");
		} else if (avant.indexOf("p_mcInput2") > -1) {
			var apres = this.p_mcInput3.txtValue;
			trace("2");
		} else {
			var apres = this.p_mcInput1.txtValue;
			trace("3");
		}
		trace(avant+", "+apres);
		this.myIntervalTab = setInterval(this,"focusTab",50, apres);
	}
	private function focusTab(apres):Void{
		clearInterval(this.myIntervalTab);
		this.myIntervalTab = undefined;
		Selection.setFocus(apres);
	}
	private function controlNumber() {
		if (Number(this.p_mcInput1.txtValue.text) >= 0 && 
			Number(this.p_mcInput2.txtValue.text) >= 0 && 
			Number(this.p_mcInput3.txtValue.text) >= 0 && 
			!isNaN(Number(this.p_mcInput1.txtValue.text)) && 
			!isNaN(Number(this.p_mcInput2.txtValue.text)) && 
			!isNaN(Number(this.p_mcInput3.txtValue.text))) {
				trace (Number(this.p_mcInput1.txtValue.text) +" + "+
				Number(this.p_mcInput2.txtValue.text) +" + "+
				Number(this.p_mcInput3.txtValue.text));
			soundPlayer.playASound("shClac2");
					
			// Les trois champs sont remplis, on controle leur validité
			if (Number(this.p_mcInput1.txtValue.text) +
				Number(this.p_mcInput2.txtValue.text) +
				Number(this.p_mcInput3.txtValue.text) == 
				Number(this.p_mcSoluce.txtValue.text)) {
				// Il gagne un point
				++this.p_nPoint;
				this.p_mcPoint.gotoAndStop(this.p_nPoint + 1);
				trace ("gagne : "+this.p_nPoint+" Point");
				// on effece les valeurs
				this.p_mcInput1.txtValue.text = "";
				this.p_mcInput2.txtValue.text = "";
				this.p_mcInput3.txtValue.text = "";
				
				// On sauvegarde le numéro
				this.p_old_mcSoluce = Number(this.p_mcSoluce.txtValue.text);
				
				// On rejoue ou on gagne
				if (this.p_nPoint < 5) {
					this.initInterface();
				} else {
					endGame();	
				}
			} else if (this.p_nPoint > 0) {
				// Il perd un point
				--this.p_nPoint;
				this.p_mcPoint.gotoAndStop(this.p_nPoint + 1);
				Selection.setFocus(this.p_mcInput1.txtValue);
				trace ("perd : "+this.p_nPoint+" Point");				
			}
		}
	}
	
	/** Fin du jeu */
	private function endGame() {
		this.gotoAndStop("fin");
		this.p_mcPuzzle2._visible = false;
		// Choix de la piece de puzzle
		for (var i:Number = 1 ; i <= 4 ; ++i) {
			this["p_mcPiece"+i].id = i;
			this["p_mcPiece"+i].onRelease = function () {
				_parent.soundPlayer.playASound("shark2");
				trace ("Piece "+this.id+" choisie");
				_parent.p_mcPuzzle._visible = false;
				_parent.p_mcPiece1._visible = false;
				_parent.p_mcPiece2._visible = false;
				_parent.p_mcPiece3._visible = false;
				_parent.p_mcPiece4._visible = false;
				_parent.p_mcPuzzle2._visible = true;
		
				_parent.gameFinished(this.id);
			}
		}
		this.p_mcPuzzle2.onRelease = function () {
			_parent.p_mcPuzzle._visible = true;
			_parent.p_mcPiece1._visible = true;
			_parent.p_mcPiece2._visible = true;
			_parent.p_mcPiece3._visible = true;
			_parent.p_mcPiece4._visible = true;
			this._visible = false;				
		}
	}
	
	private function gameFinished(idPieceChoisie:Number)
	{
		var state = this._parent._parent.joueur1.getState();
		if(state.childNodes[0]['jeu19'] == undefined)
		{
			var etatJeu = state.createElement('jeu19');
			state.childNodes[0].appendChild(etatJeu);
		}
		else
		{
			var etatJeu = state.childNodes[0]['jeu19'];
		}
		etatJeu.attributes.value = idPieceChoisie;
		this._parent._parent.joueur1.setState(state); 
		this._parent._parent.setWinGame(19, this._parent._parent.joueur1);
		//afficher le parchemin dans la glissière de navigation (peut être fait lors de l'enregistrement du 
		//résultat
		this._parent._parent.barreInterface_mc.initBarreJeuxIle();
		
		super.gameFinished();
	}

}

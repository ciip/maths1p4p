﻿/**
 * class com.maths1p4p.Game2_20
 * 
 * @author Yoann Augen
 * @version 1.0
 *
 * Copyright (c) 2008 Yoann Augen.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level2.game2_20.Game2_20 extends maths1p4p.AbstractGame {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iNumJeu = 20;
	/** Texte d'entrée de l'utilisateur */
	public var p_mcTextInput:MovieClip;
	
	/** Premier nombre */
	public var p_mcTextBegin:MovieClip;
	
	/** Le total */
	public var p_mcTextSoluce:MovieClip;
	
	/** Bouton validation */
	public var p_mcButton:MovieClip;
	
	/** Le signe de l'opération */
	public var p_mcSigne:MovieClip;
	
	
	/** Les dalles representant les points */
 	public var p_mcPoint:MovieClip;
 	
 	/** Les pieces de puzzle */
	public var p_mcPiece1:MovieClip;
	public var p_mcPiece2:MovieClip;
	public var p_mcPiece3:MovieClip;
	public var p_mcPiece4:MovieClip;
	public var p_mcPuzzle:MovieClip;
	public var p_mcPuzzle2:MovieClip;
 	
 	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** Le nombre de point du joueur */
	private var p_nPoint:Number;
	
	/** Le type d'opération en cours (1 = add, 2 = sous)*/
	private var p_nOpp:Number;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game2_20() {
		super();
		p_sSoundPath = Application.getRootPath() + "data/sound/level2/";
		this.p_nPoint = 0;
		// On fixe les contraintes
		this.p_mcTextInput.txtValue.restrict = "0-9"; 
		this.p_mcTextInput.txtValue.maxChars = 2; 
		
		this.initInterface();
	}
	
	/**
	* Fonction qui renvoi un nombre aleatoire entre min et max
	* @param Number min
	* @param Number max
	* @return Number
	* 
	*/
	public function randRange(min:Number, max:Number) {
	     var randomNum:Number = Math.floor(Math.random() * (max - min + 1)) + min;
	     return randomNum;
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/** Initialisation de l'interface de jeu, appelé à chaque clic sur le bouton */
	private function initInterface() {
		// Le tirage de l'opération
		this.makeOperation();
		
		// On met un evenement de validation sur le bouton
		this.p_mcButton.onRelease = function() {
			_parent.controlNumber();			
		}
		
		// On en met également un sur l'appuis du bouton 3
		// Le champs 3
		Key.addListener(this.p_mcTextInput);
		this.p_mcTextInput.onKeyDown = function() {
			if (Key.getCode() == Key.ENTER) { 
				_parent.controlNumber();
			}						
		}
		// On donne le focus au centre
		trace (Selection.setFocus(this.p_mcTextInput.txtValue));
	}
	
	/** Va vérifier la valeur entrée */
	private function controlNumber() {
		// On valide seulement si quelque chose est rentré
		if (!isNaN(Number(this.p_mcTextInput.txtValue.text)) &&
			Number(this.p_mcTextInput.txtValue.text) >= 0) { 
			soundPlayer.playASound("rape1");
			// On verifie simplement que la valeur est égale au nombre sauvegardé	
			if (this.p_mcTextInput.valeur == Number(this.p_mcTextInput.txtValue.text)) {
				// Il gagne	
				++this.p_nPoint;
				if (this.p_nPoint == 6) {
					// Partie gagné
					this.endGame();	
				} else {
					this.p_mcPoint.gotoAndStop(this.p_nPoint + 1);
					this.initInterface();
				}
			} else if (this.p_nPoint > 0) {
				// Il perd
				--this.p_nPoint;
				this.p_mcPoint.gotoAndStop(this.p_nPoint + 1);
			}
		}
	}
	
	/** Va creer l'opération à effectuer */
	private function makeOperation() {
		// On tire le signe
		this.p_nOpp = randRange(1, 2);
		
		if (this.p_nOpp == 1) {
			// Addition	
			this.p_mcSigne.txtValue.text = "+";
			// On tire deux valeur entre 0 et 30
			this.p_mcTextBegin.txtValue.text = randRange(0, 30);
			// Pour le chiffre a trouver on le cache dans une variable
			this.p_mcTextInput.valeur = randRange(0, 30);
			this.p_mcTextInput.txtValue.text = "";
			trace (this.p_mcTextInput.valeur);
			// La solution est la somme des deux
			this.p_mcTextSoluce.txtValue.text = Number(this.p_mcTextBegin.txtValue.text) + this.p_mcTextInput.valeur;
			
		} else {
			// Soustraction	
			this.p_mcSigne.txtValue.text = "-"; 
			// On tire une valeur entre 20 et 60
			this.p_mcTextBegin.txtValue.text = randRange(20, 60);
			// La seconde est plus petite que la premiere
			this.p_mcTextInput.valeur = randRange(0, Number(this.p_mcTextBegin.txtValue.text));
			this.p_mcTextInput.txtValue.text = "";
			trace (this.p_mcTextInput.valeur);
			// La solution est la soustraction des deux
			this.p_mcTextSoluce.txtValue.text = Number(this.p_mcTextBegin.txtValue.text) - this.p_mcTextInput.valeur;
		}
	}
	
	/** Fin du jeu */
	private function endGame() {
		_parent.soundPlayer.playASound("rape1");
		this.gotoAndStop("fin");
		this.p_mcPuzzle2._visible = false;
		// Choix de la piece de puzzle
		for (var i:Number = 1 ; i <= 4 ; ++i) {
			this["p_mcPiece"+i].id = i;
			this["p_mcPiece"+i].onRelease = function () {
				_parent.soundPlayer.playASound("shark2");
				trace ("Piece "+this.id+" choisie");
				_parent.p_mcPuzzle._visible = false;
				_parent.p_mcPiece1._visible = false;
				_parent.p_mcPiece2._visible = false;
				_parent.p_mcPiece3._visible = false;
				_parent.p_mcPiece4._visible = false;
				_parent.p_mcPuzzle2._visible = true;
		
				_parent.gameFinished(this.id);
			}
		}
		this.p_mcPuzzle2.onRelease = function () {
			_parent.p_mcPuzzle._visible = true;
			_parent.p_mcPiece1._visible = true;
			_parent.p_mcPiece2._visible = true;
			_parent.p_mcPiece3._visible = true;
			_parent.p_mcPiece4._visible = true;
			this._visible = false;				
		}
	}
	
	private function gameFinished(idPieceChoisie:Number)
	{
		var state = this._parent._parent.joueur1.getState();
		if(state.childNodes[0]['jeu20'] == undefined)
		{
			var etatJeu = state.createElement('jeu20');
			state.childNodes[0].appendChild(etatJeu);
		}
		else
		{
			var etatJeu = state.childNodes[0]['jeu20'];
		}
		trace("idPieceChoisie => "+idPieceChoisie)
		etatJeu.attributes.value = idPieceChoisie;
		this._parent._parent.joueur1.setState(state);
		trace("this._parent._parent.joueur1.getState() => "+this._parent._parent.joueur1.getState())
		this._parent._parent.setWinGame(20, this._parent._parent.joueur1);
		//afficher le parchemin dans la glissière de navigation (peut être fait lors de l'enregistrement du 
		//résultat
		this._parent._parent.barreInterface_mc.initBarreJeuxIle();
		
		super.gameFinished();
	}
}

﻿/**
 * class com.maths1p4p.level3.Level3
 * 
 * @author Pierre Rossel, Grégory Lardon
 * @version 1.0
 *
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import XMLShortcuts;

import maths1p4p.application.Application;
import maths1p4p.application.AbstractStorage;
import maths1p4p.AbstractPlayer;
import maths1p4p.level3.*;
import maths1p4p.utils.SoundPlayer;


class maths1p4p.level3.Level3 extends maths1p4p.AbstractLevel {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** écran de sélection du joueur principal */
	public var p_mcPlayerSelection:MovieClip;
	
	/** écran de sélection du 2e joueurs */
	public var p_mcSecondPlayerSelection:MovieClip;
	
	/** écran des résultats */
	public var p_mcResults:MovieClip;
	
	/** écran d'intro du niveau */
	public var p_mcIntroScreen:MovieClip;
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------	
	/** l'interface */
	private var p_oInterface:Level3Interface;
	
	/** le clip de l'interface */
	private var p_mcInterface:MovieClip;
	
	/** le clip pour les écrans spéciaux */
	private var p_mcSpecialScreens:MovieClip;
	
	/** la structure xml de la map */
	private var p_xmlMap:XML;
	
	/** xml contenant les sons */
	private var p_xmlSounds:XML;	
	
	/** le clip contenant toutes les scènes du niveau */
	private var p_mcSceneLib:MovieClip;
	
	/** le clip pour afficher la scène en cours */
	private var p_mcScene:MovieClip;
	
	/** la scene courante */
	private var p_sCurrScene:String
	
	/** la scene precedente */
	private var p_sLastScene:String
	
	/** le joueur */
	private var p_oCurrPlayer:Level3Player;
	
	/** la liste des loots à trouver */
	private var p_aLoots:Array;
	
	/** liste des bijoux/argent */
	private var p_aJewels:Array;
	
	/** l'object du jeu en cours */
	private var p_oCurrGame:Object;


	private var p_oStorage:AbstractStorage;
	
	/** sound player */
	static private var p_oSoundPlayer:SoundPlayer = new SoundPlayer();
	
	private var p_sSoundPath:String;


	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level3() {
		
		super();
		stop();
		
		p_oStorage = Application.getInstance().getStorage();
		p_oStorage.addEventListener("onSavePlayer", this);
		
		// creation de la scène 
		p_mcScene = createEmptyMovieClip("p_mcScene",15);
		
		// écrans spéciaux, au dessus de l'interface
		p_mcSpecialScreens = createEmptyMovieClip("p_mcSpecialScreens",60);
		
		// chargement de l'interface
		p_mcInterface = createEmptyMovieClip("p_mcInterface",50);
		var obj:Object = new Object();
		obj.onLoadInit = function(target:MovieClip){
			target._parent.initInterface();
		}
		var mcl:MovieClipLoader = new MovieClipLoader();
		mcl.addListener(obj);
		mcl.loadClip(p_sLevelFolder + "level3_interface.swf", p_mcInterface);
		
		// initialisation de XMLShortcuts
		XMLShortcuts.activate();
		
		// initialisation de la liste des loots		
		initLootsList();
		
		// chargement des scènes
		loadScenes();
		
		// passe l'écran de sélection des joueurs au premier plan
		p_mcSecondPlayerSelection = this.attachMovie("mcSecondPlayerSelection","p_mcSecondPlayerSelection",300);
		p_mcPlayerSelection = this.attachMovie("mcPlayerSelection","p_mcPlayerSelection",301);
		
		// écran des résultats
		p_mcResults = this.attachMovie("mcResultats", "p_mcResultats", 302);		
				
		// écran d'intro
		p_mcIntroScreen = this.attachMovie("mcLevelIntro", "p_mcIntroScreen", 305);
		p_mcIntroScreen.onRelease = function(){
			_visible = false;
			_parent.showPlayerSelectionScreen();
		}
		
	
		// sons
		// chargement du fichier xml de configuration
		p_sSoundPath = Application.getRootPath() + "data/sound/level3/";
		p_xmlSounds = new XML();
		p_xmlSounds["parent"] = this;
		p_xmlSounds.onLoad = function(){
			this["parent"].readXmlSounds();
		}
		p_xmlSounds.load(p_sLevelFolder + "data/sounds.xml");		
	
	}
	
	
	/**
	 * playerIsLoaded
	 * 
	 * débute la partie du joueur dont l'id est passé en paramètre
	 */
	public function playerIsLoaded():Void{
		launchLevel(p_oCurrPlayer);
	}
	
	
	/**
	 * showGame
	 * @param oGame l'objet game issu de la map
	 * 
	 * charge un jeu depuis le noeud Game de la map
	 */
	public function showGame(oGame:Object) {

		var iIdGamePlace:Number = Number(oGame._idGamePlace);
		var iSubLevel:Number = Number(oGame._subLevel)-1;
		var iIdGame:Number = p_oCurrPlayer.getGameInGamePlace(iSubLevel, iIdGamePlace);
		
		levelLoadGame(iIdGame, iSubLevel);

	}
	
	/**
	 * levelLoadGame
	 * 
	 * charge un jeu
	 */
	public function levelLoadGame(iIdGame:Number, iSubLevel:Number):Void{
		
		// défini le jeu en cours
		p_oCurrGame = p_oCurrPlayer.getGame(iIdGame);
		
		// on lance le jeu
		loadGame(p_oCurrGame.file.__text);
		
		// interface
		p_oInterface.showAide();
		p_oInterface.hideMessages();
		p_oInterface.hideArgent();
		p_oInterface.showReload();
		p_oInterface.showRetour();		

		if(p_oCurrGame.palme.__text != "null"){
			p_oInterface.showPalme(p_oCurrGame.palme.__text);
		}
		
	}

	
	/**
	 * getGame
	 * @param iIdGame l'id du game
	 * 
	 * renvois l'objet game
	 */	
	public function getGame(iIdGame:Number):Object{
		return p_oCurrPlayer.getGame(iIdGame);
	}
	
	/**
	 * getGames 
	 * renvois le tableau des games
	 */	
	public function getGames():Object{
		return p_oCurrPlayer.getGames();
	}	
	
	/**
	 * getJewels 
	 * renvois le tableau des bijoux
	 */	
	public function getJewels():Array{
		return p_oCurrPlayer.getJewels();
	}		
	
	/**
	 * showSpecialScreen
	 * @param sSpecialScreenName le nom de liaison de clip
	 * 
	 * affiche un écran spécial
	 */
	public function showSpecialScreen(sSpecialScreenName:String):Void{
		p_mcSpecialScreens.attachMovie(sSpecialScreenName,"mcSpecialScreen",1);
		p_mcSpecialScreens._visible = true;
	}
	
	/**
	 * hideSpecialScreen
	 * 
	 * cache un écran spécial
	 */
	public function hideSpecialScreen():Void{
		p_mcSpecialScreens = createEmptyMovieClip("p_mcSpecialScreens",60);
	}	
	
	/**
	 * rollOverGame
	 * @param oGame l'objet game issu de la map
	 * 
	 * rollover sur l'accès au jeu
	 */
	public function rollOverGame(oGame:Object):Void{
		
		var iIdGamePlace:Number = Number(oGame._idGamePlace);
		var iSubLevel:Number = Number(oGame._subLevel)-1;
		var iIdGame:Number = p_oCurrPlayer.getGameInGamePlace(iSubLevel, iIdGamePlace);		
		var oTheGame:Object = p_oCurrPlayer.getGame(iIdGame);
		if (oTheGame.done.__text == "0"){
			var sIndication:String = "\n(à faire)";
		}else{
			var sIndication:String = "\n(terminé)";
		}
		
		p_oInterface.showGameName(oTheGame.name.__text + sIndication);
		

	}	
	
	
	/**
	 * rollOutGame
	 * @param oGame l'objet game issu de la map
	 * 
	 * rollout sur l'accès au jeu
	 */	 
	public function rollOutGame(oGame:Object):Void{
		
		p_oInterface.hideGameName();
		
	}
	
	
	/**
	 * gotoHotSpot
	 * @param idScene id du stage destination dans le stage courant
	 * 
	 * déplacement dans la map
	 */
	public function gotoScene(idScene:String):Void{
		
		var scene:Object = getSceneWithId(idScene);
		p_sLastScene = p_sCurrScene;
		p_sCurrScene = idScene;
		p_oCurrPlayer.setCurrScene(p_sCurrScene);
		loadScene(scene);
		
	}	
	
	
	/**
	 * gotoSubDirection
	 * @param idDir id de la frame à atteindre dans la scène courante
	 * 
	 * déplacement dans la scène courante
	 */
	public function gotoSubDirection(idDir:String):Void{
		p_mcScene["scene"].gotoScene(Number(idDir));
	}		
	
	
	/**
	 * hasLoot
	 * @param sLoot nom du loot à tester
	 * 
	 * renvois true si le loot est dans l'inventaire du joueur
	 */
	public function hasLoot(sLoot:String):Boolean{
		return p_oCurrPlayer.hasLoot(sLoot);
	}
		
	
	/**
	 * hasObject
	 * @param sObject nom de l'objet
	 * 
	 * renvois true si l'objet est dans l'inventaire du joueur
	 */
	public function hasObject(sObject:String):Boolean{
		return p_oCurrPlayer.hasObject(sObject);
	}
	
	/**
	 * getObject
	 * @param sObject nom de l'objet
	 * 
	 * rajoute un objet à l'inventaire du joueur
	 */
	public function getObject(sObject:String):Void{

		p_oCurrPlayer.addObject(sObject);		
		p_oInterface.addObject(sObject);
		
	}
	
	
	/**
	 * hideObject
	 * @param sObject nom d'objet à cacher
	 * 
	 * cache un objet dans l'interface
	 */
	public function hideObject(sObject:String):Void{		
		p_oInterface.hideObject(sObject);		
	}	
	
	
	
	/**
	 * showObject
	 * @param sObject nom d'objet à cacher
	 * 
	 * cache un objet dans l'interface
	 */
	public function showObject(sObject:String):Void{		
		p_oInterface.showObject(sObject);		
	}	
		
	/**
	 * deleteObject
	 * @param sObject nom d'objet à supprimer
	 * 
	 * supprime un objet de l'inventaire
	 */
	public function deleteObject(sObject:String):Void{
			
		p_oInterface.hideObject(sObject);
		p_oCurrPlayer.deleteObject(sObject);
		
	}	

	
	/**
	 * reLoadGame	  
	 * recharge le jeu
	 */
	public function reLoadGame():Void{
		freezeQuitButtons(true);
		loadGame(p_sCurGame);
	}
	
	
	/**
	 * leaveGame
	 * 
	 * quitte le jeu en cours
	 * appellé depuis l'interface
	 */
	public function leaveGame():Void{
		
		p_oInterface.hideAide();
		p_oInterface.hideReload();
		p_oInterface.hideRetour();		
		p_oInterface.showMessages();
		p_oInterface.showArgent();
		freezeQuitButtons(true);
		p_oCurrGame = null;
		unloadGame();
		p_oInterface.hideGameName();
		p_oInterface.hidePalme();
		
		// recharge la scène en cours
		gotoScene(p_sCurrScene);
		
	}	
	
	
	public function getLootToWinForCurrGame(){
		
		if(p_oCurrGame.done.__text == "0"){
			return getNextLootToWin();
		}else{
			return getRandomJewel();
		}
		
	}
	
	
	/**
	 * getNextLootToWin
	 * 
	 * renvois le nom du prochain loot à gagner
	 * appellé depuis la classe game
	 */
	public function getNextLootToWin():String{
		return p_aLoots[p_oCurrPlayer.getLastLootWon()+1];
	}
	
	/**
	 * getRandomJewel
	 * 
	 * renvois le nom du prochain loot à gagner
	 * appellé depuis la classe game
	 */
	public function getRandomJewel():String{
		
		var iJewel:Number = Math.floor(Math.random() * p_aJewels.length);
		return p_aJewels[iJewel];
		
	}
	
	
	/**
	 * getCurrLoot
	 * 
	 * l'objet gagné courant a été saisi
	 */
	public function getCurrLoot(sLootName):Void{
		
		// game done
		p_oCurrGame.done.__text = "1";
		// mémorise la palme
		if(p_oCurrGame.palme.__text != "or"){
			p_oCurrGame.palme.__text = p_mcGame.mcAnimation.getPalme();
		}
		// update de l'affichage de la palme dans le jeu
		p_oInterface.showPalme(p_oCurrGame.palme.__text);
		
		var iJewel:Number = null;
		for(var i in p_aJewels){
			if(p_aJewels[i] == sLootName){
				iJewel = i;
			}
		}		
		if(iJewel != null){
			// c'est un bijou, on incrémente le compte			
			p_oCurrPlayer.addJewel(sLootName)
			// animation de l'argent
			p_oInterface.animArgent();
		}else{
			// c'est un loot
			// incrémentation, pour le prochain loot à gagner
			p_oCurrPlayer.setLastLootWon(p_oCurrPlayer.getLastLootWon()+1);
			var sLoot:String = p_aLoots[p_oCurrPlayer.getLastLootWon()];

			p_oCurrPlayer.addLoot(sLoot);		
			p_oInterface.addLoot(sLoot);
			
			// test si changement de sous niveau
			if(p_oCurrPlayer.getLastLootWon() == 3 || p_oCurrPlayer.getLastLootWon() == 7 || p_oCurrPlayer.getLastLootWon() == 11  || p_oCurrPlayer.getLastLootWon() == 15 ){
				// incrementation de p_iCurrSubLevel
				p_oCurrPlayer.setCurrSubLevel(p_oCurrPlayer.getCurrSubLevel()+1);
				// suppression de la potion
				p_oInterface.hidePotion();
				p_oCurrPlayer.deleteObject("potion");
				// attributions des jeux dans les nouveaux espaces
				p_oCurrPlayer.defineGamePlaces(p_oCurrPlayer.getCurrSubLevel());
			}			
			
		}
		
		// sauvegarde du joueur
		p_oCurrPlayer.save();
		
	}
	
	/**
	 * getCurrSubLevel
	 * 
	 * renvois l'id du sous niveau actuel
	 */
	public function getCurrSubLevel():Number{
		return p_oCurrPlayer.getCurrSubLevel();
	}


	/**
	 * isGameDone
	 * 
	 * renvois l'état "done" du jeu à l'emplacement idGamePlace
	 * @param oGame l'object xml game
	 */
	public function isGameDone(oGame:Object):Boolean{
		var iIdGame:Number = p_oCurrPlayer.getGameInGamePlace(oGame._subLevel-1, oGame._idGamePlace);
		return p_oCurrPlayer.getGame(iIdGame).done.__text == "1";
	}
	
	
	/**
	 * showPlayerSelectionScreen
	 * 
	 * Affiche l'écran de sélection du 2e joueur
	 */
	public function showSecondPlayerSelectionScreen(){
		p_mcSecondPlayerSelection.show();
	}
	
	
	
	/**
	 * showPlayerSelectionScreen
	 * 
	 * Affiche l'écran de sélection de joueurs
	 */
	public function showPlayerSelectionScreen(){
		p_mcPlayerSelection.show();
	}	
	
	
	/**
	 * playerIsSelected
	 * 
	 * un joueur a été sélectionné dans l'écran de sélection de joueurs
	 * @param nom le nom du joueur
	 * @param id l'id du joueur
	 */
	public function playerIsSelected(nom:String, id:Number){
		
		// on transmet le joueur au jeu en cours
		var oPlayer:Object = new Object();
		oPlayer.name = nom;
		oPlayer.id = id;
		
		p_mcGame.mcAnimation.secondPlayerSelected(oPlayer);		
		
	}
	
	
	/**
	 * getPlayerData
	 * 
	 * renvois p_oPlayerData
	 */
	public function getPlayerData():Object{
		return p_oCurrPlayer.getState();
	}
	
	/**
	 * hideLoot
	 * 
	 * cache un loot de l'inventaire
	 */
	public function hideLoot(sLoot:String):Void{
		p_oInterface.hideLoot(sLoot);
	}
	
	/**
	 * setSubLevelPassed
	 * 
	 * déclare un sous niveau comme terminé
	 */
	public function setSubLevelPassed(sSubLevel:String):Void{
		p_oCurrPlayer.setSubLevelPassed(sSubLevel);
		// sauvegarde du joueur
		p_oCurrPlayer.save();
	}
	
	/**
	 * isSubLevelPassed
	 * 
	 * test si un sous niveau a été franchi
	 */
	public function isSubLevelPassed(sSubLevel:String):Boolean{
		return p_oCurrPlayer.isSubLevelPassed(sSubLevel);
	}
	
	/**
	 * getGameTiroirsData
	 * 
	 * renvois les données du jeu tiroirs
	 */
	public function getGameTiroirsData():Object{
		return p_oCurrPlayer.getGameTiroirsData();
	}
	
	
	/**
	 * setGameTiroirsData
	 * 
	 * définis les données du jeu tiroirs
	 */
	public function setGameTiroirsData(oData:Object):Void{
		p_oCurrPlayer.setGameTiroirsData(oData);
	}
	
	/**
	 * getPlayer
	 * 
	 * renvois le joueur en cours
	 */
	public function getPlayer():Object{
		return p_oCurrPlayer;
	}
	
	/**
	 * showHelp
	 * affiche l'écran d'aide du jeu en cours
	 */
	public function showHelp():Void{
		p_mcGame.mcAnimation.showHelp();
	}
	
	/**
	 * hideHelp
	 * cache l'écran d'aide du jeu
	 */
	public function hideHelp():Void{
		p_mcGame.mcAnimation.hideHelp();
	}
	
	/**
	 * isLastScene
	 * check la scene precedente
	 */
	public function isLastScene(sScene:String):Boolean{
		return p_sLastScene == sScene;
	}
	
	/**
	 * renvois la liste des messages
	 */	
	public function getMessages():Array{
		return p_oCurrPlayer.getMessages();
	}
	
	/**
	 * active la visibilité d'un message dans l'écran messages
	 */	
	public function setMessageOn(iIdMessage):Void{
		p_oCurrPlayer.setMessageOn(iIdMessage);
	}
	
	
	public function askForQuit():Void{
		// sauvegarde du joueur en cours
		savePlayer();
		unload();
	}
	
	public function changePlayer():Void{
		
		// quit le jeu en cours, si besoin
		leaveGame()
		// sauvegarde du joueur en cours
		savePlayer();
		// choix du nouveau joueur
		p_mcPlayerSelection.show();
		
	}
	
	public function getPlayerState():Object{
		return p_oCurrPlayer.getState();
	}
	
	/**
	 * affiche l'écran des résultats
	 */	
	public function showResults():Void{
		p_mcPlayerSelection.hide();
		p_mcResults.show();
	}
	
	/**
	 * freeze quit buttons interactions
	 */	
	public function freezeQuitButtons(val:Boolean):Void{
		p_oInterface.freezeQuitButtons(val);
	}
	
	/**
	 * play a sound
	 */		
	public function playSound(soudName:String):Void{
		p_oSoundPlayer.playASound(soudName,0);
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	
	private function savePlayer():Void{
		p_oCurrPlayer.refreshInterface(p_oInterface.getElementsVisible());
		p_oCurrPlayer.refreshResults();
		p_oStorage.savePlayer(p_oCurrPlayer);		
	}
	
	private function onSavePlayer(event:Object):Void{
		//trace("onLoadSavePlayer " + event.data);
	}
	
	
	/**
	 * launchLevel
	 * 
	 * débute la partie du joueur passé en paramètre
	 */
	private function launchLevel(oPlayer:Level3Player):Void{
		
		p_oCurrPlayer = oPlayer;
		initInterface();
		gotoScene(p_oCurrPlayer.getCurrScene());
		
	}
	
	
	/**
	 * initInterface
	 * 
	 * initialisation de l'interface
	 */
	private function initInterface():Void{		

		p_oInterface = p_mcInterface["mcInterface"];
		p_oInterface.hideAide();
		p_oInterface.hideRetour();
		p_oInterface.hideReload();
		p_oInterface.showArgent();
		
		p_oInterface.initElements(p_oCurrPlayer.getState()["state"]["interface"]);
		p_oInterface.setPlayerName(p_oCurrPlayer.getName());
		
	}
	
	/**
	 * loadScenes
	 * 
	 * chargement des scènes
	 */
	private function loadScenes():Void{
		
		p_mcSceneLib = createEmptyMovieClip("mcScenesLib", 10);
		var mclScene:MovieClipLoader = new MovieClipLoader();
		var obj:Object = new Object();
		obj.onLoadInit = function(target:MovieClip){
			target._parent.scenesLoaded();
		}
		mclScene.addListener(obj);
		mclScene.loadClip(p_sLevelFolder + "level3_screens.swf", p_mcSceneLib);

	}
	
	/**
	 * scenesLoaded
	 * 
	 * les scènes sont chargées
	 */
	private function scenesLoaded():Void{
		
		// on charge ensuite la map xml
		loadMap();
		
	}
	
	
	/**
	 * loadMap
	 * 
	 * chargement de la map xml
	 */
	private function loadMap():Void{
		
		p_xmlMap = new XML();
		p_xmlMap.ignoreWhite = true;
		p_xmlMap["callback"] = this;
		p_xmlMap.onLoad = function(){
			this["callback"].mapLoaded();
		}
		p_xmlMap.load(p_sLevelFolder + "data/level3.xml");
		
	}
		
	
	/**
	 * mapLoaded
	 * 
	 * appellé lorsque le xml de la map est chargé
	 */
	private function mapLoaded():Void{
		
		// affichage de la liste des joueurs
		//p_oCurrPlayer = new Level3Player("joueur1",1);
		//p_oCurrPlayer.load(0, this, "playerIsLoaded")
		
	}
	
	
	/**
	 * loadScene
	 * 
	 * charge la scène idStage
	 */
	//private function loadScene(scene:Object, iFrame:Number):Void{
	private function loadScene(scene:Object):Void{
		
		// la scène à afficher
		var sMovie:String = "mcScreen_" + scene._id;

		p_mcScene._visible = false;
		var mcScreen:MovieClip = p_mcScene.attachMovie(sMovie, "scene", 1);
		
		mcScreen.init(scene);
		
	}
	
	/**
	 * getSceneWithId
	 * @param idScene l'id de la scène à renvoyer
	 * 
	 * renvois le noeud scene dont l'id est passé en paramètre
	 */
	private function getSceneWithId(idScene:String){

		for (var i in p_xmlMap["root"]["$scene"]){
			if( p_xmlMap["root"]["scene_"+i]._id == idScene){
				return p_xmlMap["root"]["scene_"+i];
			}
		}
		return null;
		
	}
	
	
	/**
	 * initLootsList
	 * initialise la liste des loots
	 */
	private function initLootsList():Void{
		
		p_aLoots = new Array();
		
		// premiere phase du niveau
		p_aLoots[0]  = "ecu1";
		p_aLoots[1]  = "ecu2";
		p_aLoots[2]  = "ecu3";
		p_aLoots[3]  = "ecu4";
		// 2e phase du niveau
		p_aLoots[4]  = "rubis1";
		p_aLoots[5]  = "rubis2";
		p_aLoots[6]  = "rubis3";
		p_aLoots[7]  = "rubis4";
		// 3e phase du niveau
		p_aLoots[8]  = "morceauBois";		
		p_aLoots[9]  = "roueBleue";		
		p_aLoots[10] = "roue";
		p_aLoots[11] = "corde";		
		// 4e phase du niveau
		p_aLoots[12] = "insigne";
		p_aLoots[13] = "symbole";
		p_aLoots[14] = "couteau";
		p_aLoots[15] = "passe";			
		p_aLoots[16] = "poignard";			
		p_aLoots[17] = "archer";			

		// bijoux
		p_aJewels = new Array("pierreVerte","pierreBleue","pierreBlanche","piece");
		
	}	
	
	/**
	 * Lecture du fichier xml de configuration
	 * Création de la liste des fichiers à précharger
	 */
	private function readXmlSounds(){
		
		for (var i in p_xmlSounds["level"].media.$son){  
			p_oSoundPlayer.addASound(p_xmlSounds["level"].media.$son[i]._id, p_sSoundPath + p_xmlSounds["level"].media.$son[i].__text);
        }
		
	}		
	
	
}

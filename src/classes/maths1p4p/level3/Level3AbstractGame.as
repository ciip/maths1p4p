﻿/**
 * class com.maths1p4p.level3.Level3AbstractStage
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.AbstractGame;
import maths1p4p.application.Application;

class maths1p4p.level3.Level3AbstractGame extends AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** les loots */
	public var p_mcLoots:MovieClip;
	
	/** ecran d'aide */
	public var p_mcHelp:MovieClip;
	

	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** contient "normal" ou "or" selon la palme gagnée */
	private var p_sPalme:String;
	private var p_bPalmeOr:Boolean = false;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	
	/**
	 * Constructeur
	 */
	public function Level3AbstractGame()	{		
		
		super();	
		
		p_sSoundPath = Application.getRootPath() + "data/sound/level3/";
		
		p_mcHelp._visible = false;
		p_mcHelp.onPress = function(){};
		p_mcHelp.useHandCursor = false;
		
		// affichage du nom du jeu 
		if (_parent._parent.p_oCurrGame.done.__text == "0"){
			var sIndication:String = "\n(à faire)";
		}else{
			var sIndication:String = "\n(terminé)";
		}			
		_parent._parent.p_oInterface.showGameName(_parent._parent.p_oCurrGame.name.__text + sIndication);	
		
		// palme		
		if(_parent._parent.p_oCurrGame.palme.__text == undefined){
			_parent._parent.p_oCurrGame.palme.__text = "";
		}
		p_sPalme = _parent._parent.p_oCurrGame.palme.__text;

	}
	
	
	/**
	 * renvois le loot à gagner
	 */
	public function getLootToWin():String{
		return _parent._parent.getLootToWinForCurrGame();
	}	
	
	
	/**
	 * le joueur a ramassé l'objet à gagner
	 */
	public function getLoot(){
		
		// ajoute l'objet à l'inventaire
		_parent._parent.getCurrLoot(p_mcLoots.p_sCurrLootName);
		
		gotoAndStop("end");
		
		// réactive les boutons quitter
		_parent._parent.freezeQuitButtons(true);		
		
		// le jeu est terminé
		gameFinished();
		
	}
	
	/**
	 * affiche l'écran d'aide
	 */
	public function showHelp():Void{
		p_mcHelp.swapDepths(p_mcHelp._parent.getNextHighestDepth());
		p_mcHelp._visible = true;
	}
	
	/**
	 * cache l'écran d'aide
	 */
	public function hideHelp():Void{
		p_mcHelp._visible = false;
	}
	
	/**
	 * return p_sPalme
	 */
	public function getPalme():String{
		return p_sPalme;
	}
	
	/**
	 * la partie est gagnée
	 */
	public function won(){
		
		// palme
		if(p_bPalmeOr){
			p_sPalme = "or";
		}else{
			p_sPalme = "verte";
		}

		gotoAndStop("win");
		p_mcLoots.show();
		
		// désactive les boutons quitter
		_parent._parent.freezeQuitButtons(false);
		
	}	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	
	
	
}

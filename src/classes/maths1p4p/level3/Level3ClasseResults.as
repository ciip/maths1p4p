﻿/**
 * class com.maths1p4p.level3.Level3Manual
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import maths1p4p.Teacher;
import maths1p4p.Classe;
import maths1p4p.application.AbstractStorage;
import maths1p4p.level3.Level3Player;
import maths1p4p.AbstractPlayer;
import mx.utils.Delegate;
import mx.controls.Alert;

class maths1p4p.level3.Level3ClasseResults extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var p_btPrint:Button;
	public var p_btMerlin:Button;
	public var p_btRetour:Button;
	public var p_btListe:Button;


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_oStorage:AbstractStorage;
	private var p_oClass:Classe;
	
	// le clip contenant le tableau des résultats
	private var p_mcPlayersList:MovieClip;
	
	private var p_aPlayers:Array;
	
	private var p_iCurrPlayerToLoad:Number;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level3ClasseResults()	{	
		
		// init des boutons		
		Application.useLibCursor(p_btPrint, "cursor-finger");
		p_btPrint.onRelease = function(){
			_parent.printResults();
		}
		
		Application.useLibCursor(p_btMerlin, "cursor-finger");
		p_btMerlin.onRelease = function(){				
			
			// Charge les données de la classe courante tout de suite
			// Il y a 99.00 % de chances que ça arrive avant que le login soit terminé.
			var olClass = new Object();
			olClass.onLoadClass = Delegate.create(this, function (event:Object) {
				p_oClass = event.data.oClass;
				trace("Class loaded (teacher: " + p_oClass.p_iTeacher + ")");
			});
			Classe.loadCurrentClass(olClass);
			
			var olLogin = new Object();
			olLogin.onLogin = Delegate.create(this, function (oTeacher:Teacher){
				if (oTeacher != undefined){
					// vérifie que le teacher loggé est bien le propriétaire de la classe courante
					if (p_oClass != undefined) {
						if (p_oClass.p_iTeacher == oTeacher.p_iId)
							_parent._parent.gotoAndStop("merlin");
						else
							Alert.show("Cet enseignant n'est pas propriétaire de cette classe.");
					}
					else {
						// le 0.01 % de risque que ça arrive
						Alert.show("Problème réseau, veuillez réessayer.");
					}
				}
			});
			Teacher.login(_root, olLogin);

		}
		
		Application.useLibCursor(p_btListe, "cursor-finger");
		p_btListe.onRelease = function () {
			Application.editCurrentClass(this, Delegate.create(this, function(evt:Object){
				// Recharge la classe
				show();
			}));
		};		
		
		Application.useLibCursor(p_btRetour, "cursor-finger");
		p_btRetour.onRelease = function(){
			_parent._parent.hide();
			_parent._parent._parent.p_mcPlayerSelection.show();
		}
		
		p_oStorage = Application.getInstance().getStorage();
		
	}
	
	public function show(){
		p_oStorage.addEventListener("onLoadPlayersList", this);
		p_oStorage.loadPlayersList(3, true);
		_visible = true;
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------	
	
	private function onLoadPlayersList (event:Object) {

		p_aPlayers = event.data.arrPlayersList;

		if (p_aPlayers != undefined) {
			if(p_mcPlayersList!=null){
				p_mcPlayersList.removeMovieClip();
				delete p_mcPlayersList;
			}
			p_mcPlayersList = createEmptyMovieClip("mcPlayerList",this.getNextHighestDepth());
			for(var iPlayer:Number=0; iPlayer<p_aPlayers.length; iPlayer++){
				displayPlayerResults(iPlayer, p_aPlayers[iPlayer].results.$game);	
			}
			
		} else {
			trace("erreur ! ");
		}
		
	}
	
	private function printResults():Void{
		
		var mc:MovieClip = attachMovie("mcResultsPrint", "mcResultsPrint", this.getNextHighestDepth());
		mc._x = 2000;

		for(var j=0;j<29;j++){
			mc["mcLine"+j]._visible = false;;
		}
		
		for(var iPlayer:Number=0; iPlayer<p_aPlayers.length; iPlayer++){
			var mcLine:MovieClip = mc["mcLine"+iPlayer];
			mcLine["txtId"].text = String(iPlayer+1);
			mcLine["txtNom"].text = p_aPlayers[iPlayer]._name;
			mcLine._visible = true;
			var aGames:Array = p_aPlayers[iPlayer].results.$game;
			for (var i=0; i<aGames.length; i++){
				if(aGames[i]._finished == "1"){
					mcLine["txtNote"+i].text = "x";
				}
			}
		}
		
		var pjResults:PrintJob = new PrintJob();
		if(pjResults.start()){
			pjResults.addPage(mc);
			pjResults.send();
			delete pjResults;	
		}
		
		delete mc;	
		
	}
		
	
	private function displayPlayerResults(idPlayer:Number, aGames:Array):Void{
		
		// affichage du nom du joueur
		p_mcPlayersList.createTextField("txtPlayer_"+idPlayer, p_mcPlayersList.getNextHighestDepth(), 28, 39 + idPlayer*15, 150, 20);
		p_mcPlayersList["txtPlayer_"+idPlayer].text = p_aPlayers[idPlayer]._name;
		p_mcPlayersList["txtPlayer_"+idPlayer].selectable = false;
		
		// affichage des résultats
		var iInitX:Number = 124;
		var iInitY:Number = 49;
		
		for (var i=0; i<aGames.length; i++){
			if(aGames[i]._finished == "1"){
				var mcPuce:MovieClip = p_mcPlayersList.attachMovie("mcPuce","puce_"+idPlayer+"_"+i, p_mcPlayersList.getNextHighestDepth());
				mcPuce._x = iInitX + i*21;
				mcPuce._y = iInitY + idPlayer*15;
			}
		}
		
	}
	

	
}

﻿/**
 * class com.maths1p4p.level3.Level3Interface
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level3.Level3Interface extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_mcBarreHelp:MovieClip
	public var p_btAnticlic:Button;

	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_oLoots:Object;
	private var p_oObjects:Object;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level3Interface()	{		
		
		// initialisation de la liste des loots
		p_oLoots = new Object();
		p_oLoots.ecu1 = this["mcEcu1"];
		p_oLoots.ecu2 = this["mcEcu2"];
		p_oLoots.ecu3 = this["mcEcu3"];
		p_oLoots.ecu4 = this["mcEcu4"];
		
		p_oLoots.rubis1 = this["mcRubis1"];
		p_oLoots.rubis2 = this["mcRubis2"];
		p_oLoots.rubis3 = this["mcRubis3"];
		p_oLoots.rubis4 = this["mcRubis4"];
		
		p_oLoots.morceauBois = this["mcMorceauBois"];
		p_oLoots.roueBleue = this["mcRoueBleue"];
		p_oLoots.roue = this["mcRoue"];
		p_oLoots.corde = this["mcCorde"];
		
		p_oLoots.insigne = this["mcInsigne"];
		p_oLoots.symbole = this["mcSymbole"];
		p_oLoots.couteau = this["mcCouteau"];
		p_oLoots.passe = this["mcPasse"];		
		
		p_oLoots.poignard = this["mcPoignard"];		
		p_oLoots.archer = this["mcArcher"];		
		
		// initialisation de la liste des objets
		p_oObjects = new Object();
		p_oObjects.cle1 = this["mcCle1"];
		p_oObjects.parchemin = this["mcParchemin"];
		
		hideAllLoots();
		hideAllObjects();
		hidePotion();
		hidePalme();
	
		// initialisation des boutons
		initAllButtons();
		
		// init des actions sur objets
		Application.useLibCursor(this["mcParchemin"], "cursor-finger");
		this["mcParchemin"].onRelease = function(){
			_parent._parent._parent.showSpecialScreen("mcScreen_201_parchemin");
		}
		// action machine sous niveau 3
		Application.useLibCursor(this["mcMorceauBois"], "cursor-finger");
		Application.useLibCursor(this["mcRoueBleue"], "cursor-finger");
		Application.useLibCursor(this["mcRoue"], "cursor-finger");
		Application.useLibCursor(this["mcCorde"], "cursor-finger");
		this["mcMorceauBois"].onRelease = this["mcRoueBleue"].onRelease = this["mcRoue"].onRelease = this["mcCorde"].onRelease = function(){
			if(_parent._parent._parent.p_sCurrScene == "041"){
				_parent._parent._parent.p_mcScene.scene.clickOnObject(this._name);
			}
		}
		
		// couteau cuisine, poignard, symbole
		Application.useLibCursor(this["mcCouteau"], "cursor-finger");
		Application.useLibCursor(this["mcPoignard"], "cursor-finger");
		Application.useLibCursor(this["mcSymbole"], "cursor-finger");
		this["mcCouteau"].onRelease = this["mcPoignard"].onRelease = this["mcSymbole"].onRelease = this["mcArcher"].onRelease = function(){
			_parent._parent._parent.p_mcScene.scene.clickOnObject(this._name);
		}
		
		// plan du chateau
		Application.useLibCursor(this["mcInsigne"], "cursor-finger");
		this["mcInsigne"].onRelease = function(){
			_parent._parent._parent.showSpecialScreen("mcScreen_202_plan");
		}		

		p_mcBarreHelp._visible = false;		
		p_btAnticlic.useHandCursor = false;
		p_btAnticlic.onRelease = function(){};
	
	}

	
	
	/**
	 * setPlayerName
	 * @param sPlayerName le nom du joueur
	 * affiche le nom du joueur
	 */
	public function setPlayerName(sPlayerName:String):Void{
		
		this["txtPlayer"].text = sPlayerName;
		
	}
		
	/**
	 * showLoot
	 * @param sLoot le nom du loot
	 * affiche le loot à son emplacement prévu
	 */
	public function showLoot(sLoot:String):Void{
		p_oLoots[sLoot]._visible = true;
	}
		
	/**
	 * hideLoot
	 * @param sLoot le nom du loot
	 * cache le loot 
	 */
	public function hideLoot(sLoot:String):Void{
		p_oLoots[sLoot]._visible = false;
	}
	
		
	/**
	 * hideObject
	 * @param sObject le nom de l'objet
	 * cache l'objet
	 */
	public function hideObject(sObject:String):Void{
		p_oObjects[sObject]._visible = false;
	}
	
	/**
	 * showObject
	 * @param sObject le nom de l'objet
	 * affiche l'objet à son emplacement prévu
	 */
	public function showObject(sObject:String):Void{
		p_oObjects[sObject]._visible = true;
	}
	
	/**
	 * showGameName
	 * @param sGameName le nom du jeu
	 * affiche le nom du jeu en cours
	 */
	public function showGameName(sGameName:String):Void{
		this["txtGame"].text = sGameName;
	}
	
	/**
	 * hideGameName
	 * @param sGameName le nom du jeu
	 * affiche le nom du jeu en cours
	 */
	public function hideGameName():Void{
		
		this["txtGame"].text = "";
		
	}
	
	/**
	 * showPotion
	 * affiche la potion
	 */
	public function showPotion():Void{		
		this["mcPotion"]._visible = true;		
	}
		
	
	/**
	 * hidePotion
	 * cache la potion
	 */
	public function hidePotion():Void{		
		this["mcPotion"]._visible = false;		
	}
	
	/**
	 * animArgent
	 * animaiton gain argent
	 */
	public function animArgent():Void{
		this["mcAnimArgent"].gotoAndPlay(2);
	}
		
	
	/**
	 * showPalme
	 * @param type le type de palme
	 * affiche la palme
	 */
	public function showPalme(type:String):Void{		
		switch(type){
			case "verte":
				this["mcPalme"]._visible = true;	
				break;
			case "or":
				this["mcPalmeOr"]._visible = true;	
				break;				
		}
			
	}
		
	
	/**
	 * hidePalme
	 * cache la palme
	 */
	public function hidePalme():Void{		
		this["mcPalme"]._visible = false;		
		this["mcPalmeOr"]._visible = false;		
	}
	
		
	
	/**
	 * showAide
	 * affiche l'aide
	 */
	public function showAide():Void{		
		this["btAide"]._visible = true;		
	}
		
	
	/**
	 * hideAide
	 * cache l'aide
	 */
	public function hideAide():Void{		
		this["btAide"]._visible = false;		
	}
		
	
	/**
	 * showArgent
	 * affiche l'aide
	 */
	public function showArgent():Void{		
		this["btArgent"]._visible = true;		
	}
		
	
	/**
	 * hideArgent
	 * cache l'aide
	 */
	public function hideArgent():Void{		
		this["btArgent"]._visible = false;		
	}
	
	/**
	 * showRetour
	 * affiche this["btRetour"]
	 */
	public function showRetour():Void{		
		this["btRetour"]._visible = true;		
	}
		
	
	/**
	 * hideRetour
	 * cache this["btRetour"]
	 */
	public function hideRetour():Void{		
		this["btRetour"]._visible = false;		
	}	
	
	/**
	 * showReload
	 * affiche this["btReload"]
	 */
	public function showReload():Void{		
		this["btReload"]._visible = true;		
	}
		
	
	/**
	 * hideReload
	 * cache this["btReload"]
	 */
	public function hideReload():Void{		
		this["btReload"]._visible = false;		
	}	
	
	
	/**
	 * showMessages
	 * affiche this["btReload"]
	 */
	public function showMessages():Void{		
		this["bt_messages"]._visible = true;		
	}
		
	
	/**
	 * hideMessages
	 * cache this["btReload"]
	 */
	public function hideMessages():Void{		
		this["bt_messages"]._visible = false;		
	}		
	
	
	/**
	 * addObject
	 * @param sObject le nom de l'objet
	 * ajoute un objet à l'inventaire
	 */
	public function addObject(sObject:String):Void{		
		
		switch (sObject){
			
			case "potion":
				showPotion();
				break;
				
			default:
				showObject(sObject);
				break;
			
			
		}
		
	}	
	
	
	/**
	 * addLoot
	 * @param sLoot le nom du loot
	 * ajoute un loot à l'inventaire
	 */
	public function addLoot(sLoot:String):Void{		
		showLoot(sLoot);
	}		
	
	/**
	 * init des elements visibles
	 */
	public function initElements(xmlElements:XMLNode):Void{
		
		hideAllLoots();
		hideAllObjects();
		hidePotion();
		hidePalme();		

		// loots
		for(var i=0;i<xmlElements["$loot"].length;i++){
			p_oLoots[String(xmlElements["$loot"][i].__text)]._visible = true;
		}
		// objets
		for(var i=0;i<xmlElements["$object"].length;i++){
			p_oObjects[xmlElements["$object"][i].__text]._visible = true;
		}
		// potion
		if(xmlElements["potion"] != null){
			showPotion();
		}
		
	}
	
	/**
	 * renvois les elements actuellement visibles
	 */
	public function getElementsVisible():XMLNode{

		var xmlRes:XMLNode = new XMLNode(1, "interface");
		
		// loots
		for(var i in p_oLoots){
			if(p_oLoots[i]._visible){
				var vNode:XMLNode = new XMLNode(1, "loot");
				var txtNode:XMLNode = new XMLNode(3, i);
				vNode.appendChild(txtNode);
				xmlRes.appendChild(vNode);
			}
		}

		// objets
		
		for(var i in p_oObjects){
			if(p_oObjects[i]._visible){
				var vNode:XMLNode = new XMLNode(1, "object");
				var txtNode:XMLNode = new XMLNode(3, i);
				vNode.appendChild(txtNode);
				xmlRes.appendChild(vNode);
			}
		}
		
		// potion
		if(this["mcPotion"]._visible){
			var vNode:XMLNode = new XMLNode(1, "potion");
			var txtNode:XMLNode = new XMLNode(3, "potion");
			vNode.appendChild(txtNode);
			xmlRes.appendChild(vNode);
		}

		return xmlRes;
		
	}
	
	/**
	 * freeze quit buttons interactions
	 */	
	public function freezeQuitButtons(val:Boolean):Void{
		this["bt_quit"].enabled = val;
		this["bt_changeUser"].enabled = val;
	}	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	private function initAllButtons():Void{
		
		// bouton temporaire
		this["btWinGame"].useHandCursor = false;
		this["btWinGame"].onRelease = function(){
			if(Key.isDown(Key.SHIFT)){
				_parent._parent._parent.p_mcGame.mcAnimation.won();
			}
		}
		
		// bouton retour
		Application.useLibCursor(this["btRetour"], "cursor-finger");
		this["btRetour"].onRelease = function(){
			_parent._parent._parent.leaveGame();
		}
		
		// bouton reload
		Application.useLibCursor(this["btReload"], "cursor-finger");
		this["btReload"].onRelease = function(){
			_parent._parent._parent.reLoadGame();
		}
		
		// bouton aide
		Application.useLibCursor(this["btAide"], "cursor-finger");
		this["btAide"].onRelease = function(){
			_parent._parent._parent.showHelp();
			_parent.p_mcBarreHelp._visible = true;
			_parent.p_mcBarreHelp.onRelease = function(){
				this._visible = false;
				_parent._parent._parent.hideHelp();
			}
		}
		
		// bouton messages
		Application.useLibCursor(this["bt_messages"], "cursor-finger");
		this["bt_messages"].onRelease = function(){
			_parent._parent._parent.showSpecialScreen("mcScreen_203_messages");
		}
		
		// argent
		Application.useLibCursor(this["btArgent"], "cursor-finger");
		this["btArgent"].onRelease = function(){
			_parent._parent._parent.showSpecialScreen("mcScreen_204_resultats");
		}		
		
		// quitter
		Application.useLibCursor(this["bt_quit"], "cursor-finger");
		this["bt_quit"].onRelease = function(){
			_parent._parent._parent.askForQuit();
		}
		
		// change user
		Application.useLibCursor(this["bt_changeUser"], "cursor-finger");
		this["bt_changeUser"].onRelease = function(){
			_parent._parent._parent.changePlayer();
		}
		
	}
	
	private function hideAllLoots():Void{
		for(var i in p_oLoots){
			p_oLoots[i]._visible = false;
		}
	}
	
	private function hideAllObjects():Void{
		for(var i in p_oObjects){
			p_oObjects[i]._visible = false;
		}
	}
	
}

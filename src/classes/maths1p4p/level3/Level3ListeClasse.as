﻿/**
 * class com.maths1p4p.level3.Level3ListeClasse
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import maths1p4p.application.AbstractStorage;
import maths1p4p.level3.Level3Player;
import maths1p4p.AbstractPlayer;
import mx.controls.List;
import mx.utils.Delegate;

class maths1p4p.level3.Level3ListeClasse extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var p_btAddName:Button;
	public var p_btDeleteName:Button;
	public var p_btModifyName:Button;
	public var p_btEraseAll:Button;
	
	public var p_btMerlin:Button;
	public var p_btResultats:Button;
	public var p_btRetour:Button;


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_oStorage:AbstractStorage;
	
	// le clip contenant le tableau des résultats
	private var p_listPlayers:List;
	
	private var p_aPlayers:Array;
	
	private var p_oCurrPlayer:Object;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level3ListeClasse()	{	
		
		// init des boutons		
		Application.useLibCursor(p_btAddName, "cursor-finger");
		p_btAddName.onRelease = function(){
			_parent.addName();
		}
		Application.useLibCursor(p_btDeleteName, "cursor-finger");
		p_btDeleteName.onRelease = function(){
			_parent.deleteName();
		}
		Application.useLibCursor(p_btModifyName, "cursor-finger");
		p_btModifyName.onRelease = function(){
			_parent.modifyName();
		}
		Application.useLibCursor(p_btEraseAll, "cursor-finger");
		p_btEraseAll.onRelease = function(){
			_parent.eraseAll();
		}
		Application.useLibCursor(p_btMerlin, "cursor-finger");
		p_btMerlin.onRelease = function(){
			_parent._parent.gotoAndStop("merlin");
		}
		Application.useLibCursor(p_btResultats, "cursor-finger");
		p_btResultats.onRelease = function(){
			_parent._parent.gotoAndStop("resultats");
		}
		Application.useLibCursor(p_btRetour, "cursor-finger");
		p_btRetour.onRelease = function(){
			_parent._parent.hide();
		}		
		
		p_oStorage = Application.getInstance().getStorage();
		
		show();
		
	}
	
	public function show(){
		p_oStorage.addEventListener("onLoadPlayersList", this);
		p_oStorage.loadPlayersList(3, true);
		_visible = true;
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------	
	
	private function addName():Void{
		
	}
	private function deleteName():Void{
		trace("delete "+p_oCurrPlayer._idPlayer);
		p_oStorage.addEventListener("onDeletePlayer", onDeletePlayer);
		p_oStorage.deletePlayer(p_oCurrPlayer._idPlayer);
	}
	private function onDeletePlayer(event:Object){
		p_oStorage.removeEventListener("onDeletePlayer", onDeletePlayer);
		p_oStorage.addEventListener("onLoadPlayersList", this);
		p_oStorage.loadPlayersList(3, true);
	}
	private function modifyName():Void{
		
	}
	private function eraseAll():Void{
		
	}
	
	private function onLoadPlayersList (event:Object) {
		
		p_oStorage.removeEventListener("onLoadPlayersList", onLoadPlayersList);

		p_aPlayers = event.data.arrPlayersList;
		p_listPlayers.removeAll();
		p_listPlayers.addEventListener("change",  Delegate.create(this, onSelectPlayer));	

		if (p_aPlayers != undefined) {		
			
			for(var iPlayer:Number=0; iPlayer<p_aPlayers.length; iPlayer++){
				p_listPlayers.addItem(p_aPlayers[iPlayer]._name, iPlayer);
			}
			
		} else {
			p_listPlayers.addItem("erreur");
		}
		
	}
	
	private function onSelectPlayer(event:Object):Void{
		
		p_oCurrPlayer = p_aPlayers[p_listPlayers.selectedItem.data];
		
	}
	

	
}

﻿/**
 * class com.maths1p4p.level3.Level3Manual
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level3.Level3Manual extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var p_btBG:Button;
	
	/** bouton impression */
	public var p_btPrint:Button;
	/** bouton précédent */
	public var p_btLeft:Button;
	/** bouton suivant */
	public var p_btRight:Button;
	/** bouton fermer */
	public var p_btClose:Button;
	

	


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------


	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level3Manual()	{	
		
		_visible = false;
		
		// bouton servant à désactiver les clics sur les écrans au dessous
		p_btBG.useHandCursor = false;
		p_btBG.onRelease = function(){}		

		Application.useLibCursor(p_btPrint, "cursor-finger");
		p_btPrint.onRelease = function(){
			_parent.printCurrPage();
		}
		
		Application.useLibCursor(p_btLeft, "cursor-finger");
		p_btLeft.onRelease = function(){
			_parent.prevFrame();
		}
		
		Application.useLibCursor(p_btRight, "cursor-finger");
		p_btRight.onRelease = function(){
			_parent.nextFrame();
		}
		
		Application.useLibCursor(p_btClose, "cursor-finger");
		p_btClose.onRelease = function(){
			_parent.hide();
		}

	}

	/**
	 * show
	 * affiche l'écran de sélection
	 */
	public function show(){
		gotoAndStop(1);
		_visible = true;
	}
	
	/**
	 * hide
	 * cache l'écran de sélection
	 */
	public function hide(){
		_visible = false;		
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	private function printCurrPage(){
		var pjPage:PrintJob = new PrintJob();
		if(pjPage.start()){
			pjPage.addPage(
				this,
				{xMin:45, xMax:587, yMin:37, yMax:425},
				{printAsBitmap:true}
			);
			pjPage.send();
			delete pjPage;	
		}
		
	}

	
}

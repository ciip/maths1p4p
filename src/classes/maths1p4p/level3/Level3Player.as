﻿/**
 * class com.maths1p4p.level3.Level3Player
 * 
 * @author Grégory Lardon
 * @version 1.0
 *
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import XMLShortcuts;

class maths1p4p.level3.Level3Player extends maths1p4p.AbstractPlayer {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------	
	private var p_aGames:Array;

	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level3Player(sName:String, iId:Number) {
		
		super(sName, iId); 

	}
	
	/**
	 * @see AbstractPlayer
	 */
	public function setState(xmlState:XMLNode){
		super.setState(xmlState);
		p_aGames = p_xmlState["state"]["games"].$game;
	}
	
	
	/**
	 * getCurrStage
	 * Renvois la scène en cours
	 */
	public function getCurrScene():String{		
		return p_xmlState["state"]["currScene"].__text;		
	}
	
	/**
	 * setCurrScene
	 * définis la scène en cours
	 */
	public function setCurrScene(sScene:String):Void{
		p_xmlState["state"]["currScene"].__text = sScene;
	}
	
	
	/**
	 * addObject
	 * @param sObject le nom de l'objet à ajouter à l'inventaire
	 * ajoute l'objet sObject à l'inventaire du joueur
	 */
	public function addObject(sObject:String):Void{
		p_xmlState["state"]["objects"][sObject].__text = "1";
	}
	
	
	/**
	 * deleteObject
	 * @param sObject le nom de l'objet à supprimer de l'inventaire
	 * supprime l'objet sObject de l'inventaire du joueur
	 */
	public function deleteObject(sObject:String):Void{
		p_xmlState["state"]["objects"][sObject].__text = "0";
	}
	
	
	/**
	 * addLoot
	 * @param sLoot le nom du loot à ajouter à l'inventaire
	 * ajoute le loot à l'inventaire du joueur
	 */
	public function addLoot(sLoot:String):Void{
		p_xmlState["state"]["loots"][sLoot].__text = "1";
	}
	
	public function addJewel(sJewel:String):Void{
		var i:Number = Number(p_xmlState["state"]["jewels"][sJewel].__text);
		p_xmlState["state"]["jewels"][sJewel].__text = i+1;
	}	
	
	/**
	 * hasObject
	 * @param sObject le nom de l'objet à tester
	 * return True si l'objet sObject est dans l'inventaire du joueur
	 */
	public function hasObject(sObject:String):Boolean{
		return p_xmlState["state"]["objects"][sObject].__text == "1";
	}
	
	
	/**
	 * hasLoot
	 * @param lootName le nom de l'objet à tester
	 * return True si l'objet lootName est dans l'inventaire du joueur
	 */
	public function hasLoot(lootName:String):Boolean{
		return p_xmlState["state"]["loots"][lootName].__text == "1";
	}
	
	/**
	 * setSubLevelPassed
	 * 
	 * déclare un sous niveau comme terminé
	 */
	public function setSubLevelPassed(sSubLevel:String):Void{
		trace("setSubLevelPassed "+sSubLevel);
		p_xmlState["state"]["subLevel" + sSubLevel + "Passed"].__text = "1";
	}
	
	/**
	 * isSubLevelPassed
	 * 
	 * test si un sous niveau a été franchi
	 */
	public function isSubLevelPassed(sSubLevel:String):Boolean{
		return p_xmlState["state"]["subLevel" + sSubLevel + "Passed"].__text == "1";
	}
	
	/**
	 * getCurrSubLevel
	 * 
	 * renvois l'id du sous niveau actuel
	 */
	public function getCurrSubLevel():Number{
		return Number(p_xmlState["state"]["currSubLevel"].__text);
	}	
	
	/**
	 * setCurrSubLevel
	 * 
	 * renvois l'id du sous niveau actuel
	 */
	public function setCurrSubLevel(i:Number){
		trace("setCurrSubLevel " +i);
		p_xmlState["state"]["currSubLevel"].__text = String(i);
	}		
		
	/**
	 * getGameTiroirsData
	 * 
	 * renvois les données du jeu tiroirs
	 */
	public function getGameTiroirsData():Object{
		return p_xmlState["state"]["tiroirs"];
	}
	
	
	/**
	 * setGameTiroirsData
	 * 
	 * définis les données du jeu tiroirs
	 */
	public function setGameTiroirsData(oData:Object):Void{
		p_xmlState["state"]["tiroirs"] = oData;
	}		
	
	/**
	 * active la visibilité d'un message dans l'écran messages
	 */	
	public function setMessageOn(iIdMessage):Void{
		p_xmlState["state"]["messages"]["message_"+iIdMessage].__text = "1";
	}	
	
	/**
	 * Renvois l'id du jeu qui se trouve dans l'emplacement idGamePlace
	 * 
	 * @param   iSubLevel    id du sous niveau 
	 * @param   iIdGamePlace 	id de l'emplacement
	 * @return  l'id du jeu
	 */	
	public function getGameInGamePlace(iSubLevel:Number, iIdGamePlace:Number):Number{
		return Number(p_xmlState["state"]["gamePlaces_"+iSubLevel].$gamePlace[iIdGamePlace].idGame.__text);
	}


	/**
	 * defineGamePlaces
	 * attributs les jeux dans les espaces
	 */
	public function defineGamePlaces(iCurrSubLevel:Number):Void{

		iCurrSubLevel--;
		var lastCount:Number = 0;
		
		// parse les gamesplaces du sous niveau en cours (iCurrSubLevel)
		var aCurrGamePlaces:Array = p_xmlState["state"]["$gamePlaces"][iCurrSubLevel].$gamePlace;
		var aLastGamePlaces:Array = p_xmlState["state"]["$gamePlaces"][(iCurrSubLevel-1)].$gamePlace;

		for(var i=0; i<aCurrGamePlaces.length;i++){

			if (aCurrGamePlaces[i].idGame.__text == "-1"){
				
				// le gameplace est vide
				var found:Boolean = false;
				var count:Number = lastCount;
				// parse les gameplaces du sous niveau précédent
				while (!found){
					if(p_aGames[ Number(aLastGamePlaces[count].idGame.__text) ].done.__text != "1"){	
						found = true;
						lastCount = count+1;
					}else{
						count++;
					}
				}

				// deplace le jeu du sublevel précédent à l'actuel
				aCurrGamePlaces[i].idGame.__text = aLastGamePlaces[count].idGame.__text;
				aLastGamePlaces[count].idGame.__text = -1;


			}
			
		}
		
	}
	
	/**
	 * getGame 
	 */	
	public function getGame(iIdGame:Number):Object{
		return p_aGames[Number(iIdGame)];
	}
	
	/**
	 * getGames 
	 * renvois le tableau des games
	 */	
	public function getGames():Array{
		return p_aGames;
	}	
	
	
	/**
	 * getJewels 
	 * renvois le tableau des bijoux
	 */	
	public function getJewels():Array{
		return p_xmlState["state"]["jewels"].childNodes;
	}		
	
	/**
	 * getResultsXml
	 * @return la liste des résultats au format xml
	 */	
	public function refreshResults(){
		
		var xmlResults:XML = new XML();		
		var xmlRootNode:XMLNode = xmlResults.createElement("results");
		
		for (var i=0; i<p_aGames.length; i++){
			
			var xmlGameNode:XMLNode = xmlResults.createElement("game");
			if(p_aGames[i].done.__text == "1")
				xmlGameNode.attributes.finished = "1";
			else
				xmlGameNode.attributes.finished = "0";				
			xmlGameNode.attributes.num = p_aGames[i].num.__text;				
			xmlRootNode.appendChild(xmlGameNode);			
			
		}
		
		//xmlResults.appendChild(xmlRootNode);
		
		// mémorisation
		setResults(xmlRootNode);  // on donne un XMLNode

	}	
	
	public function refreshInterface(xmlElements:XMLNode):Void{

		//p_xmlState["state"]["interface"].removeNode();
		//p_xmlState["state"].appendChild(xmlElements);
		
		for(var i in p_xmlState["state"].$interface){
			p_xmlState["state"].$interface[i].removeNode();
		}			
		p_xmlState["state"].appendChild(xmlElements);			
		
	}
	
	/**
	 * renvois la liste des messages
	 */	
	public function getMessages():Array{
		return p_xmlState["state"]["messages"].$message;
	}
	
	/**
	 * renvois le nombre de loot gagnés
	 */
	public function getLastLootWon():Number{	
		return Number(p_xmlState["state"].LastLootWon.__text);		
	}
	
	/**
	 * renvois le nombre de loot gagnés
	 */
	public function setLastLootWon(iLastLootWon:Number){	
		p_xmlState["state"].LastLootWon.__text = String(iLastLootWon);		
	}	

	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------


	



}

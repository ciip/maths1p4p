﻿/**
 * class com.maths1p4p.level3.Level3PlayerSelectionScreen
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;
import maths1p4p.application.AbstractStorage;
import mx.controls.List;
import mx.utils.Delegate;

class maths1p4p.level3.Level3SecondPlayerSelectionScreen extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	/** bouton servant à désactiver les clics sur les écrans au dessous */
	public var p_btBG:Button;
	/** bouton suite */
	public var p_btSuite:Button;
	/** composant liste, liste des joueurs */
	public var p_lstPlayers:List;
	


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_oStorage:AbstractStorage;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level3SecondPlayerSelectionScreen()	{		
		
		_visible = false;
		
		// bouton servant à désactiver les clics sur les écrans au dessous
		p_btBG.useHandCursor = false;
		p_btBG.onRelease = function(){}
		
		Application.useLibCursor(p_btSuite, "cursor-finger");
		p_btSuite.onRelease = function(){
			_parent.validateSelection();
		}
		
		p_oStorage = Application.getInstance().getStorage();
		
	}
	
	/**
	 * show
	 * affiche l'écran de sélection
	 */
	public function show(){

		_visible = true;
		p_oStorage.addEventListener("onLoadPlayersList", this);
		p_oStorage.addEventListener("onLoadPlayer", this);		
		p_oStorage.loadPlayersList(3);
		
		this["bt_retour"].addEventListener("click", Delegate.create(this, onRetour));
		
	}
	
	/**
	 * hide
	 * cache l'écran de sélection
	 */
	public function hide(){

		p_oStorage.removeEventListener("onLoadPlayersList", this);
		p_oStorage.removeEventListener("onLoadPlayer", this);
		_visible = false;
		
	}
	
	/**
	 * validateSelection
	 * valide la sélection
	 */
	public function validateSelection(){
		
		// test si un joeur est bien sélectionné
		if (p_lstPlayers.selectedIndices != null){
			// test s'il ne s'agit pas du joueur en cours
			if(_parent.getPlayer().getId() != p_lstPlayers.selectedItem.data){
				_parent.playerIsSelected(p_lstPlayers.selectedItem.label, p_lstPlayers.selectedItem.data);
				hide();
			}else{
				mx.controls.Alert.show("Tu ne peux pas choisir ton propre nom.");
			}
		}else{
			mx.controls.Alert.show("Clique sur un nom.");
		}
		
	}
	
	public function onRetour(){
		hide();
		_parent.leaveGame();
	}	
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	private function onLoadPlayersList (event:Object) {

		var arrPlayers:Array = event.data.arrPlayersList;

		if (arrPlayers != undefined) {
			
			p_lstPlayers.removeAll();

			// récupère les noms des classes dans le xml et les affiche dans la liste
			for(var iPlayer=0; iPlayer<arrPlayers.length; iPlayer++){
				var xmlPlayer = arrPlayers[iPlayer];
				p_lstPlayers.addItem({label: xmlPlayer._name, data: xmlPlayer._idPlayer});
			}

			if (p_lstPlayers.length) {
				p_oStorage.loadTeacher(arrPlayers[0]._idTeacher);
				Application.getInstance().setCurrentClass(arrPlayers[0]._idClass);				
				p_lstPlayers.selectedIndex = 0;
			}
		} else {
			
			p_lstPlayers.addItem({label: "Erreur"});
			
		}
		
	}



	
}

﻿/**
 * class com.maths1p4p.level3.game3_01.Game3_01
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * TODO :
 * certains éléments ne sont pas toujours masqués au démarrage
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import maths1p4p.level3.game3_02.*
import maths1p4p.Maths.Operations.Operation;

class maths1p4p.level3.game3_01.Game3_01 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	

	
	/** les slots */
	public var p_mcSlots:MovieClip;
	
	/** la forme dans le miroir */
	private var p_mcMiror:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
			
	/** le tableau des slots */
	private var p_aSlots:Array;
			
	/** le nombre de parties à gagner */
	private var p_iNbPartsToWin:Number = 6;
	/** le nombre de parties gagnées*/
	private var p_iNbPartsWon:Number = 0;	
	/** nombre d'essais */
	private var p_iNbTries:Number;
	/** nombre d'essais maximum*/
	private var p_iNbMaxTries:Number = 3;
	
	/** spécialeemnt pour ce jeu, on sauvegarde des infos */
	private var p_oDataSaved:Object;
	
	private var p_iNext:Number;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_01()	{		
		super();	
	}	
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		// init de p_aSlots
		p_aSlots = new Array(8);
		for(var i=0;i<8;i++){
			p_aSlots[i] = new Array(8);
			for(var j=0;j<8;j++){
				p_aSlots[i][j] = 13;
			}
		}
		
		// check le mode du jeu
		p_oDataSaved = _parent._parent.getGameTiroirsData();
		
		if (p_oDataSaved.viewed == true){
			p_aSlots[p_oDataSaved.obj0.x][p_oDataSaved.obj0.y] = p_oDataSaved.obj0.idShape;
			p_aSlots[p_oDataSaved.obj1.x][p_oDataSaved.obj1.y] = p_oDataSaved.obj1.idShape;
			p_aSlots[p_oDataSaved.obj2.x][p_oDataSaved.obj2.y] = p_oDataSaved.obj2.idShape;
			p_aSlots[p_oDataSaved.obj3.x][p_oDataSaved.obj3.y] = p_oDataSaved.obj3.idShape;
			p_aSlots[p_oDataSaved.obj4.x][p_oDataSaved.obj4.y] = p_oDataSaved.obj4.idShape;
			p_aSlots[p_oDataSaved.obj5.x][p_oDataSaved.obj5.y] = p_oDataSaved.obj5.idShape;
			p_aSlots[p_oDataSaved.obj6.x][p_oDataSaved.obj6.y] = p_oDataSaved.obj6.idShape;
			p_aSlots[p_oDataSaved.obj7.x][p_oDataSaved.obj7.y] = p_oDataSaved.obj7.idShape;
			p_aSlots[p_oDataSaved.obj8.x][p_oDataSaved.obj8.y] = p_oDataSaved.obj8.idShape;
			p_aSlots[p_oDataSaved.obj9.x][p_oDataSaved.obj9.y] = p_oDataSaved.obj9.idShape;
		}else{
			
			p_oDataSaved = new Object();
			
			var lastFrames = [];
			for(var i=0; i<10; i++){
				var ok:Boolean = true;
				while(ok){
					var x:Number = Math.floor(Math.random()*8);
					var y:Number = Math.floor(Math.random()*8);
					if (p_aSlots[x][y] == 13){
						ok = false;
					}
				}
				var cond:Boolean = true;
				while(cond){
					var frame:Number = Math.floor(Math.random()*12) +1;
					cond = false;
					for (var j in lastFrames){
						if(lastFrames[j] == frame){
							cond = true;
						}
					}
				}
				lastFrames.push(frame);
				p_aSlots[x][y] = frame;
				p_oDataSaved["obj"+i] = {idShape:frame, x:x, y:y};
			}
		
		}

		// init de l'interface
		initInterface();
				
	}	


	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		

		p_mcSlots = createEmptyMovieClip("p_mcLoots",10);
		
		for(var i in p_aSlots){
			for(var j in p_aSlots[i]){
				
				var mcSlot:MovieClip = p_mcSlots.attachMovie("mcSlot","mcSlot_"+i+"_"+j,p_mcSlots.getNextHighestDepth());
				mcSlot._x = 103 + 42 * i;
				mcSlot._y = 62 + 38 * j;
				mcSlot["x"] = i;
				mcSlot["y"] = j;
				
				// selon le mode, l'intéractivité change
				if (p_oDataSaved.viewed == true){
					mcSlot.gotoAndStop(14);
					mcSlot.onRelease = function(){
						_parent._parent.checkAnswer(this["x"],this["y"],this);
					}
					
				}else{
					if(p_aSlots[i][j] == 13){
						mcSlot.gotoAndStop(14);
					}else{
						mcSlot.gotoAndStop(p_aSlots[i][j]);
					}
				}
				
				Application.useLibCursor(mcSlot, "cursor-finger");
				
			
			}
		}
		
		if (p_oDataSaved.viewed == true){
			gotoAndStop("play");
			askNewQuestion();
			p_oDataSaved.viewed = false;
		}else{
			p_oDataSaved.viewed = true;
		}
		

		// sauvegarde de la disposition		
		_parent._parent.setGameTiroirsData(p_oDataSaved);


	}	
	
	/**
	 * nouvelle question
	 */	
	private function askNewQuestion():Void{
		
		trace("askNewQuestion");
		
		gotoAndStop("play");
		
		// vérifie le cas où tous les objets suivants auraient été découvert par erreur
		var all:Boolean = true;
		for (var i=0; i<10; i++){
			trace("rest " + p_oDataSaved["obj"+i].x + " " + p_oDataSaved["obj"+i].y)
			trace("next : " + p_mcSlots["mcSlot_"+ p_oDataSaved["obj"+i].x + "_" + p_oDataSaved["obj"+i].y]);
			trace("next : " + p_mcSlots["mcSlot_"+ p_oDataSaved["obj"+i].x + "_" + p_oDataSaved["obj"+i].y]._currentframe);
			if ( p_mcSlots["mcSlot_"+ p_oDataSaved["obj"+i].x + "_" + p_oDataSaved["obj"+i].y]._currentframe == 14 ){
				
				trace(p_oDataSaved["obj"+i].x + "_" + p_oDataSaved["obj"+i].y);
				all = false;
				p_iNext = i;
			}
		}
		
		if(!all){		
			// cherche le prochain élément non découvert
			p_iNbTries = 0;
			var iFrame:Number = p_oDataSaved["obj"+p_iNext].idShape;
			p_mcMiror.gotoAndStop(iFrame);	
		}else{
			trace("tous découvert");
			loose();
		}
				
	}
	
	
	/**
	 * nouvelle question
	 */	
	private function checkAnswer(i:Number, j:Number, mc:MovieClip):Void{
				
		if(p_iNbPartsWon < p_iNbPartsToWin){
			
			mc.gotoAndStop(p_aSlots[mc["x"]][mc["y"]]);
		
			if(p_aSlots[i][j] == p_oDataSaved["obj"+p_iNext].idShape){
				// bonne réponse
				p_iNbPartsWon++;
				if(p_iNbPartsWon < p_iNbPartsToWin){
					askNewQuestion();
				}else{
					won();
				}
			}else{
				p_iNbTries++;
				if(p_iNbTries == p_iNbMaxTries){
					loose();
				}else{
					gotoAndStop("loose");
				}
				
			}
			
		}
		
				
	}
	
	
	private function loose():Void{
		
		gotoAndStop("solution");
		
		// affichage de la solution
		for (var i in p_oDataSaved){			
			p_mcSlots[ "mcSlot_" +p_oDataSaved[i].x+ "_"+ p_oDataSaved[i].y ].gotoAndStop( p_aSlots[p_oDataSaved[i].x][p_oDataSaved[i].y] );
		}
		
		// on desactive les cases
		for(var i in p_aSlots){
			for(var j in p_aSlots[i]){
				p_mcSlots["mcSlot_" +i+"_"+j].enabled = false;;
			}
		}
		
	}
	
	
	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();
				
	}
	
}

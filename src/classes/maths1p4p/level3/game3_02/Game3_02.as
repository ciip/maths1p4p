﻿/**
 * class com.maths1p4p.level3.game3_02.Game3_02
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * TODO :
 * certains éléments ne sont pas toujours masqués au démarrage
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
import maths1p4p.application.Application;
import maths1p4p.level3.game3_02.*
import maths1p4p.Maths.Operations.Operation;

class maths1p4p.level3.game3_02.Game3_02 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** le bouton départ */
	public var p_btDepart:Button;
	/** le bouton suite */
	public var p_btSuite:Button;
	
	/** les champs texte */
	public var p_txtNbTries:TextField;
	
	public var p_txtDigit1:TextField;
	public var p_txtDigit2:TextField;
	public var p_txtDigit3:TextField;
	public var p_txtDigit4:TextField;
	public var p_txtDigit5:TextField;
	public var p_txtDigit6:TextField;
	
	public var p_txtBouton:TextField;
	public var p_txtNumberToFind:TextField;
	public var p_txtOperations:TextField;
	
	/** les 3 boutons de la machine */
	public var p_mcBouton1, p_mcBouton2, p_mcBouton3:MovieClip;
	/** l'animation mécanique */
	public var p_mcMecanique:MovieClip;
	
	/** les 10 boutons chiffrés */
	public var p_btNumber0, p_btNumber1, p_btNumber2, p_btNumber3, p_btNumber4, p_btNumber5, p_btNumber6, p_btNumber7, p_btNumber8, p_btNumber9:Button;
	/** les opérateurs */
	public var p_btReset:Button;
	public var p_btPlus:Button;
	public var p_btMoins:Button;
	public var p_btFois:Button;
	public var p_btEgal:Button;

	/** les loots */
	public var p_mcLoots:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
			
	/** liste de toutes les opérations possibles */
	private var p_aOperations:Array;
	/** l'id de l'opération en cours */
	private var p_iCurrOperationId:Number;
	
	/** la suite d'opération en cours par le joueur */
	private var p_aUserOperations:Array;
	/** l'opération en cours par le joueur */
	private var p_oCurrOperation:Operation;
			
	/** le nombre de parties à gagner */
	private var p_iNbPartsToWin:Number = 3;
	/** le nombre de parties gagnées*/
	private var p_iNbPartsWon:Number = 0;	
	/** nombre d'essais */
	private var p_iNbTries:Number;
	/** nombre d'essais maximum*/
	private var p_iNbMaxTries:Number = 10;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_02()	{		
		super();	
	}	
	
	

	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		// init des opérations possibles
		p_aOperations = [
			[2 , 8 , 174],
			[6 , 7 , 278],
			[4 , 7 , 194],
			[2 , 3 , 195],
			[7 , 6 , 115],
			[4 , 0 , 124],
			[5 , 3 , 144],
			[4 , 5 , 225],
			[7 , 8 , 176],
			[4 , 0 , 140],
			[2 , 3 , 152],
			[7 , 6 , 146],
			[5 , 0 , 185],
			[4 , 3 , 157],
			[5 , 0 , 235],
			[7 , 3 , 297],
			[5 , 2 , 133],
			[7 , 4 , 183],
			[4 , 5 , 108],
			[4 , 9 , 281],
			[2 , 3 , 165],
			[2 , 3 , 230],
			[2 , 6 , 188],
			[8 , 9 , 217],
			[5 , 6 , 104],
			[7 , 5 , 107],
			[2 , 0 , 118],
			[2 , 6 , 248],
			[2 , 6 , 258]
		];
		
		// init de l'interface
		initInterface();
				
	}	


	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		
		
		p_btSuite._visible = false;
		
		// les 3 boutons victoire
		p_mcBouton1._visible = p_mcBouton2._visible = p_mcBouton3._visible = false;
		Application.useLibCursor(p_mcBouton1, "cursor-finger");
		Application.useLibCursor(p_mcBouton2, "cursor-finger");
		Application.useLibCursor(p_mcBouton3, "cursor-finger");
		
		// les 10 boutons chiffrés
		for (var i:Number = 0; i<10; i++){
			this["p_btNumber"+i]._visible = false;
			this["p_btNumber"+i]["val"] = i;
			this["p_btNumber"+i].onRelease = function(){
				_parent.setNumber(this["val"]);
			}
			Application.useLibCursor(this["p_btNumber"+i], "cursor-finger");
		}
		
		// les opérateurs
		Application.useLibCursor(p_btReset, "cursor-finger");
		p_btReset.onRelease = function(){
			_parent.reset();
		}
		Application.useLibCursor(p_btPlus, "cursor-finger");
		p_btPlus.onRelease = function(){
			_parent.setOperator("+");
		}
		Application.useLibCursor(p_btMoins, "cursor-finger");
		p_btMoins.onRelease = function(){
			_parent.setOperator("-");
		}

		Application.useLibCursor(p_btFois, "cursor-finger");
		p_btFois.onRelease = function(){
			_parent.setOperator("x");
		}

		Application.useLibCursor(p_btEgal, "cursor-finger");
		p_btEgal.onRelease = function(){
			_parent.setEgal();
		}
		
		
		// le bouton départ
		//Application.useLibCursor(p_btDepart, "cursor-finger");
		trace(typeof(p_btDepart));
		p_btDepart.onRelease = function(){
			_parent.askNewQuestion();
		}


	}	
	
	/**
	 * nouvelle question
	 */	
	private function askNewQuestion():Void{
		
		p_oSoundPlayer.playASound("metalOndule",0);
		
		p_iNbTries = 0;
		p_txtNbTries.text = String(p_iNbTries);
		
		p_mcMecanique.gotoAndPlay(2);
		gotoAndStop("play");
		showNumbers();
		p_btDepart._visible = false;
		
		// opération aléatoire
		p_iCurrOperationId = Math.floor(Math.random()*p_aOperations.length);
		
		// init des touches de la calculette
		for(var i=0; i<10; i++){
			this["p_btNumber"+i]._visible = true;
			if(i == p_aOperations[p_iCurrOperationId][0] || i == p_aOperations[p_iCurrOperationId][1]){
				this["p_btNumber"+i]._alpha = 100;
				this["p_btNumber"+i].enabled = true;
				this["p_btNumber"+i].useHandCursor = true;
			}else{
				this["p_btNumber"+i]._alpha = 30;
				this["p_btNumber"+i].enabled = false;
				this["p_btNumber"+i].useHandCursor = false;
			}
		}
		
		// init des champs
		p_txtOperations.text = "";
		p_txtNbTries.text = "0";
		p_txtDigit1.text = p_txtDigit2.text = p_txtDigit3.text = p_txtDigit4.text = p_txtDigit5.text = p_txtDigit6.text = "";
		p_txtNumberToFind.text = p_aOperations[p_iCurrOperationId][2];
				
		// init de l'opération en cours
		p_aUserOperations = [];
		p_oCurrOperation = new Operation();
		p_aUserOperations.push(p_oCurrOperation);
				
	}
	

	/**
	 * efface une ligne 
	 */	
	private function reset():Void{
		
		p_oSoundPlayer.playASound("clic",0);
		
		if(p_oCurrOperation.p_iB != null){
			p_oCurrOperation.p_iB = null;
		}else if(p_oCurrOperation.p_sOperator != null){
			p_oCurrOperation.p_sOperator = null;
		}else if(p_oCurrOperation.p_iA != null){
			p_oCurrOperation.p_iA = null;
			p_aUserOperations.pop();
			p_oCurrOperation = p_aUserOperations[p_aUserOperations.length-1];
			if(p_oCurrOperation == null) {
				p_oCurrOperation = new Operation();
				p_aUserOperations.push(p_oCurrOperation);
			}
		}
		displayOperations();
		
	}
	
	/**
	 * défini un nombre
	 */	
	private function setNumber(nNumber:Number):Void{
		
		p_oSoundPlayer.playASound("clic",0);
		
		// seulement si l'opérateur est défini
		if(p_oCurrOperation.p_sOperator == null){
			
			if(p_oCurrOperation.p_iA != null){
				var n:String = String(p_oCurrOperation.p_iA);
			}else{
				var n:String = "";
			}
			var newN = n + String(nNumber);
			p_oCurrOperation.p_iA = Number(newN);
			
		}else{
			
			if(p_oCurrOperation.p_iB != null){
				var n:String = String(p_oCurrOperation.p_iB);
			}else{
				var n:String = "";
			}
			var newN = n + String(nNumber);
			p_oCurrOperation.p_iB = Number(newN);
			
		}
		
		displayOperations();
		
	}	
	
	/**
	 * défini l'operateur
	 */	
	private function setOperator(sOperator:String):Void{
		
		p_oSoundPlayer.playASound("clic",0);
		
		// seulement si on a saisi le premier nombre
		if(p_oCurrOperation.p_iA != null){
			p_oCurrOperation.p_sOperator = sOperator;
			displayOperations();
		}
		
	}
	
	/**
	 * resultat de l'opération
	 */	
	private function setEgal():Void{
		
		p_oSoundPlayer.playASound("clic",0);
		
		// seulement si on a saisi le 2e nombre
		if(p_oCurrOperation.p_iB != null){
			
			p_iNbTries++;
			p_txtNbTries.text = String(p_iNbTries);
			
			var nResult:Number = p_oCurrOperation.compute();		
		
			p_oCurrOperation = new Operation(nResult);
			p_aUserOperations.push(p_oCurrOperation);
			
			displayOperations();
			
			// vérifie la réponse
			if(p_aOperations[p_iCurrOperationId][2] == p_oCurrOperation.p_iA){
				// réponse trouvé
				p_oSoundPlayer.playASound("metalOndule",0);
				p_iNbPartsWon++;
				if(!p_bPalmeOr && p_iNbTries <= 6){
					p_bPalmeOr = true;
				}
				if (p_iNbPartsWon == p_iNbPartsToWin){
					// 3 parties gagnées
					this["p_mcBouton"+p_iNbPartsWon]._visible = true;
					won();
				}else{
					gotoAndStop(1);
					p_mcHelp._visible = false;
					hideNumbers();
					p_btDepart._visible = false;
					p_btSuite._visible = true;
					p_btSuite._alpha = 100;
					p_btSuite.onRelease = function(){
						_parent.askNewQuestion();
					}
					if (p_iNbPartsWon > 0){
						// affichage du bouton
						this["p_mcBouton"+p_iNbPartsWon]._visible = true;
					}
				}
				
			}else{
				
				if (p_iNbTries == p_iNbMaxTries){
					loose();
				}
				
			}
		
		}
		
	}

	/**
	* Affichage des opération dans le champs texte
	*/
	private function displayOperations():Void{
		
		var sOperations:String = "";
		for (var i=0; i<p_aUserOperations.length; i++){
			
			if(p_aUserOperations[i].p_iA != null){
				sOperations += p_aUserOperations[i].p_iA + " ";
			}
			if(p_aUserOperations[i].p_sOperator != null){
				sOperations += p_aUserOperations[i].p_sOperator + " ";
			}
			if(p_aUserOperations[i].p_iB != null){
				sOperations += p_aUserOperations[i].p_iB;
			}
			sOperations += "\n";
			
		}
		
		p_txtOperations.text = sOperations;
		
		displayDigits();
		
	}
	
	/**
	* affichage des digits
	*/
	private function displayDigits(){
		
		p_txtDigit1.text = p_txtDigit2.text = p_txtDigit3.text = p_txtDigit4.text = p_txtDigit5.text = p_txtDigit6.text = "";
		
		if(p_oCurrOperation.p_iB != null){
			var sNum:String = String(p_oCurrOperation.p_iB);
		}else{
			var sNum:String = String(p_oCurrOperation.p_iA);
		}
		
		if(sNum != "undefined"){
			var iCount:Number = 0;
			for (var i=sNum.length-1; i>=0; i--){
				iCount++;
				this["p_txtDigit"+iCount].text = sNum.charAt(i);
			}
		}
		
	}

	/**
	 * caches les chiffres 
	 */	
	private function hideNumbers():Void{
		for (var i:Number = 0; i<10; i++){
			this["p_btNumber"+i]._visible = false;
		}
	}
	/**
	 * affiche les chiffres 
	 */	
	private function showNumbers():Void{
		for (var i:Number = 0; i<10; i++){
			this["p_btNumber"+i]._visible = true;
			this["p_btNumber"+i]._alpha = 100;
		}
	}

	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();				
		p_mcMecanique.gotoAndPlay(2);

	}
	
	
	/**
	 * la partie est perdue
	 * @param msg le message a afficher
	 */
	private function loose(sMsg:String){
		
		gotoAndStop("loose");
		
	}	

	
	
	
}

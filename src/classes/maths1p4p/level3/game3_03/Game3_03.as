﻿/**
 * class com.maths1p4p.level3.game3-03.Game3-03
 * 
 * @author Grégory Lardon
 * @version 0.2
 * 
 * ETAT : 
 * Le jeu semble fonctionner correctement
 * 
 * TODO :
 * -> manque l'intégration des sons
 * -> gestion des loots
 * -> gestion du jeu déjà gagné, loot différent...
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import maths1p4p.AbstractGame;
import maths1p4p.level3.game3_03.*
import flash.geom.Point;

class maths1p4p.level3.game3_03.Game3_03 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_txtInput:TextField;
	public var p_txtNbTries:TextField;	
	public var p_mcBG:MovieClip;
	
	public var p_mcNext:MovieClip;

	public var p_mcBarres:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** le nombre de parties à gagner */
	private var p_iNbPartsToWin:Number = 4;
	/** le nombre de parties gagnées*/
	private var p_iNbPartsWon:Number = 0;
	/** le nombre d'essais à ne pas dépasser */
	private var p_iNbMaxTries:Number = 30;
	/** le nombre d'essais actuels du joueur*/
	private var p_iCurrNbTries:Number = 0;

	
	/** le point 0,0 de dessin des barres */
	private var p_ptBarInitLoc:Point;
	/** hauteur d'une barre */
	private var p_iBarW:Number = 4;
	/** largeur d'une barre */
	private var p_iBarH:Number = 44;
	/** espace entre 2 barres */
	private var p_iBarsOffset:Number = 5;
	
	/** prochain nombre à trouver */
	private var p_iNumberToFind:Number;
	
	/** liste des écarts */
	private var p_aDifferences:Array = new Array(0, 1, 5, 20, 100);
	/** liste des couleurs pour les écarts */
	private var p_aColors:Array = new Array(0xFF0000, 0xEDAE13, 0x00FF00, 0x9C37D1, 0x000000);
	
	/** état du focus sur le champs de saisie */
	private var p_bTxtInputHasFocus:Boolean;

	/** les loots */
	public var p_mcLoots:MovieClip;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_03()	{		
		super();				
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){		
		
		// initialisation des propriétés
		p_ptBarInitLoc = new Point(63, 187);
		p_bTxtInputHasFocus = false;
		
		// initialisation de l'interface
		initInterface();
		
		Key.addListener(this);
		
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		
		
		p_txtInput.restrict = "0123456789";
		p_txtInput._visible = false;
		p_txtInput.text = "";
		p_txtInput.onKillFocus = function(newFocus:Object) {
			if (this.text == "") this.text = "?";
			 _parent.p_bTxtInputHasFocus = false;
		}
		p_txtInput.onSetFocus = function(oldFocus:Object) {
			if (this.text == "?") this.text = "";
			 _parent.p_bTxtInputHasFocus = true;
		}
		
		p_txtNbTries.text = "";
		
		// boutons départ et suite
		Application.useLibCursor(p_mcNext, "cursor-finger");
		p_mcNext.onRelease = function(){
			_parent.startNewPart();
		}
		
		
	}	
	
	/**
	 * démarrage d'une partie
	 */
	private function startNewPart(){
		
		// définition du nombre à trouver
		p_iNumberToFind = Math.round(Math.random()*99);
		
		// (re)création du movieclip contenant les barres
		p_mcBarres = createEmptyMovieClip("p_mcBarres", 5);
		p_mcBarres._x = p_ptBarInitLoc.x;
		p_mcBarres._y = p_ptBarInitLoc.y;	
		
		// on cache le bouton suite
		p_mcNext._visible = false;
		// on affiche le champs de saisie
		p_txtInput._visible = true;
		p_txtInput.text = "?";
		
	}
			
	
	/**
	 * appellé lorsqu'une touche tu clavier est appuyée
	 */
	private function onKeyDown(){
		
		// si le joueur presse ENTER, on test la valeur de p_txtInput
		if(Key.getCode() == Key.ENTER){
			if(p_bTxtInputHasFocus){
				checkAnswer(p_txtInput.text);
			}
		}
		
	}

	/**
	 * Vérifie la réponse du joueur
	 * @param ans le contenu du champs p_txtInput
	 */
	private function checkAnswer(ans:String){

		var v_iAnswer:Number = Number(ans);
		
		// si le nombre n'est pas correcte on ne prend pas en compte
		if(v_iAnswer != null && !isNaN(v_iAnswer)){

			var v_iDiff:Number = Math.abs(p_iNumberToFind - v_iAnswer);
			
			// test les écarts : (0, 1, 5, 20, 100);
			if (v_iDiff == 0){
				// bonne réponse :
				var v_iDiffId:Number = 0;
			}else if (v_iDiff == 1){
				// difference de 1
				var v_iDiffId:Number = 1;
			}else if (v_iDiff <= p_aDifferences[2]){
				// difference de 2 à 5
				var v_iDiffId:Number = 2;
			}else if (v_iDiff <= p_aDifferences[3]){
				// difference de 6 à 20
				var v_iDiffId:Number = 3;
			}else {
				// difference >= 20
				var v_iDiffId:Number = 4;
			}
			
			// dessine la barre de couleur
			var v_nBarColor:Number = p_aColors[v_iDiffId];
			drawBar(v_iAnswer, v_nBarColor);
			
			if(v_iDiffId == 0){
				
				// partie gagnée !!!
				
				p_oSoundPlayer.playASound("ClicBrdrrdr",0);
				
				p_iNbPartsWon++;
				
				if(p_iNbPartsWon == p_iNbPartsToWin){	
					
					p_oSoundPlayer.playASound("metalAigu",0);
					
					// le joueur a gagner suffisemment de parties
					if(p_iCurrNbTries <= p_iNbMaxTries){
						// il a gagné en suffisemment d'essais
						p_mcBarres.removeMovieClip();			
						if(p_iCurrNbTries <= 28){
							p_bPalmeOr = true;
						}
						won();
					}else{
						// il a gagné mais en trop d'essais
						trace("gagné mais en trop d'essais...");
					}
					
				}else{
					
					// il doit gagner encore d'autres parties
					nextFrame();
					// on affiche le bouton suite
					p_mcNext.gotoAndStop(2);
					p_mcNext._visible = true;
					// on cache le champs de saisie
					p_txtInput._visible = false;
					
				}
				
			}
			
			// incrémente le nombre d'essais
			p_iCurrNbTries++;
			p_txtNbTries.text = String(p_iCurrNbTries);
			
		}
		
		// efface le champs
		p_txtInput.text = "";
		
	}
	
	/**
	 * dessine une barre
	 * @param v_iAnswer nombre saisie par le joueur
	 * @param v_nBarColor couleur de la barre à dessiner
	 */
	private function drawBar(v_iAnswer:Number, v_nBarColor:Number){
	
		// dessin de la barre
		var v_iX:Number = v_iAnswer*p_iBarsOffset -1;
				
		p_mcBarres.beginFill(v_nBarColor);
		p_mcBarres.moveTo(v_iX,0);
		p_mcBarres.lineTo((v_iX+p_iBarW), 0);
		p_mcBarres.lineTo((v_iX+p_iBarW), p_iBarH);
		p_mcBarres.lineTo(v_iX, p_iBarH);
		p_mcBarres.lineTo(v_iX, 0);
		p_mcBarres.endFill();			
		
	}
	

	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();

	}
	
	
	/**
	 * la partie est perdue
	 * @param msg le message a afficher
	 */
	private function loose(sMsg:String){
		
		gotoAndStop("loose");
		
	}	

	
}

﻿/**
 * class com.maths1p4p.level3.game3-04.Game3-04
 * 
 * @author Grégory Lardon
 * @version 0.2
 * 
 * ETAT : 
 * Le jeu semble fonctionner correctement
 * 
 * TODO :
 * -> manque l'intégration des sons
 * -> gestion des loots
 * -> gestion du jeu déjà gagné, loot différent...
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import maths1p4p.level3.game3_04.*
import maths1p4p.utils.OutilsTableaux;
import flash.geom.Point;

class maths1p4p.level3.game3_04.Game3_04 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	/** bouton depart */
	public var p_btDepartSuite:Button;
	
	/** textfields */
	public var p_txtInfos:TextField;	
	public var p_txtDepartSuite:TextField;
	
	/** le clip contenant les 6 silhouettes */
	public var p_mcElements:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** le nombre de parties à gagner */
	private var p_iNbPartsToWin:Number = 4;
	/** le nombre de parties gagnées*/
	private var p_iNbPartsWon:Number = 0;
	/** le nombre d'essais à ne pas dépasser */
	private var p_iNbMaxTries:Number = 4;
	/** le nombre d'essais actuels du joueur*/
	private var p_iCurrNbTries:Number = 0;
	
	private var p_iTotalNbTries:Number = 0;
	
	private var p_aCurrSolution:Array;
	private var p_o6Symbols:Object;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_04()	{		
		super();				
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){		
		
		// initialisation de l'interface
		initInterface();
		
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		
		
		p_txtInfos = this["txtInfos"];
		p_txtDepartSuite = this["txtDepartSuite"];
		
		p_txtDepartSuite.text = "Départ";
		
		Application.useLibCursor(p_btDepartSuite, "cursor-finger");
		p_btDepartSuite.onRelease = function(){
			_parent.startNewPart();
			_parent.p_txtDepartSuite.text = "OK";
			this.onRelease = function(){
				_parent.checkAnswer();
			}
		}
		
		this["mcPuce0"]._visible = false;
		this["mcPuce1"]._visible = false;
		this["mcPuce2"]._visible = false;
		this["mcPuce3"]._visible = false;

		
		// init des boutons
		this["btElement0"]["data"] = {element:"casque" , value:"raye"};
		this["btElement1"]["data"] = {element:"casque" , value:"rond"};
		this["btElement2"]["data"] = {element:"insigne" , value:"tour"};
		this["btElement3"]["data"] = {element:"insigne" , value:"lice"};
		this["btElement4"]["data"] = {element:"insigne" , value:"couronne"};
		this["btElement5"]["data"] = {element:"armure" , value:"gris"};
		this["btElement6"]["data"] = {element:"armure" , value:"or"};
		this["btElement7"]["data"] = {element:"queue" , value:"rouge"};
		this["btElement8"]["data"] = {element:"queue" , value:"bleu"};
		this["btElement9"]["data"] = {element:"queue" , value:"vert"};
		
		for(var i=0;i<10;i++){
			Application.useLibCursor(this["btElement"+i], "cursor-finger");
			this["btElement"+i].onRelease = function(){
				if(this._currentframe == 1){
					gotoAndStop(2);
				}else{
					gotoAndStop(1);
				}
			}
			this["btElement"+i].enabled = false;
			this["btElement"+i].getActive = function():Boolean{
				return this._currentframe==2;
			}			
		}
		
		p_mcElements._x = 432;
		
	}	
	
	/**
	 * démarrage d'une partie
	 */
	private function startNewPart(){
		
		p_txtInfos.text = "";
		
		p_iCurrNbTries = 0;
		
		// obtient 1 ou 2 éléments au hazard
		p_aCurrSolution = get1or2RandomElements();
		
		// determine les 6 combinaisons complémentaires
		p_o6Symbols = get6SymbolsForSolution(p_aCurrSolution);
		
		// creation des 6 elements
		for(var i=0;i<6;i++){
			// armure
			p_mcElements["t"+i].mcArmure.gotoAndStop(p_o6Symbols.armure[i]);
			// insigne
			p_mcElements["t"+i].mcArmure.mcInsigne.gotoAndStop(p_o6Symbols.insigne[i]);
			// casque
			p_mcElements["t"+i].mcArmure.mcCasque.gotoAndStop(p_o6Symbols.casque[i]);
			// queue
			p_mcElements["t"+i].mcQueue.gotoAndStop(p_o6Symbols.queue[i]);
		}
		
		this[p_mcElements._name].xSlideTo(10, 1, "easeInCubic");
		
		// active les boutons
		for(var i=0;i<10;i++){
			this["btElement"+i].enabled = true;
			this["btElement"+i].gotoAndStop(1);
		}
		
	}
	
	/**
	* get1or2RandomElements
	* renvois 1 ou 2 element de symbole, au hazard
	*/
	private function get1or2RandomElements():Array{
		
		var aElements:Array = [
			{element:"armure" , value:"or"},
			{element:"armure" , value:"gris"},
			{element:"insigne", value:"tour"},
			{element:"insigne", value:"couronne"},
			{element:"insigne", value:"lice"},
			{element:"casque" , value:"rond"},
			{element:"casque" , value:"raye"},
			{element:"queue"  , value:"vert"},
			{element:"queue"  , value:"rouge"},
			{element:"queue"  , value:"bleu"}		
		];
		
		var aRes:Array = [];
		
		var i1or2:Number = Math.floor(Math.random()*2)+1;
		var iLastElement:String = null;
		for(var i=0; i<i1or2; i++){
			var ok:Boolean = true;
			while(ok){
				var IElementId:Number = Math.floor(Math.random()*aElements.length);
				if(aElements[IElementId].element != iLastElement){
					aRes.push(aElements[IElementId]);
					var iLastElement = aElements[IElementId].element;
					ok = false;
				}
			}
		}
		
		return aRes;
		
	}
	
	/**
	* get6SymbolsForSolution
	* renvois les 6 symbols complémentaire à la solution à trouver
	*/	
	private function get6SymbolsForSolution(p_aCurrSolution):Object{
		
		// armures
		var aElements:Array = [
			{element:"armure" , value:"or"},
			{element:"armure" , value:"gris"}
		];
		var a8Armures:Array = OutilsTableaux.shuffle(getSymbols(aElements));
		
		// insignes
		var aElements:Array = [
			{element:"insigne", value:"tour"},
			{element:"insigne", value:"couronne"},
			{element:"insigne", value:"lice"}
		];
		var a8Insignes:Array = OutilsTableaux.shuffle(getSymbols(aElements));
		
		// casques
		var aElements:Array = [
			{element:"casque" , value:"raye"},
			{element:"casque" , value:"rond"}
		];
		var a8Casques:Array = OutilsTableaux.shuffle(getSymbols(aElements));
		
		// queues
		var aElements:Array = [
			{element:"queue"  , value:"vert"},
			{element:"queue"  , value:"rouge"},
			{element:"queue"  , value:"bleu"}
		];
		var a8Queues:Array = OutilsTableaux.shuffle(getSymbols(aElements));
		
		var oRes:Object = new Object();
		oRes.armure = a8Armures;
		oRes.insigne = a8Insignes;
		oRes.casque = a8Casques;
		oRes.queue = a8Queues;
		
		return oRes;
		
	}
	
	/**
	* getSymbols
	* renvois 6 values qui ne font pas partie de la solution
	*/	
	private function getSymbols(aElements:Array):Array{
		
		var aRes:Array = new Array();
		
		for(var i=0;i<aElements.length;i++){
			if(!isObjectInArray( p_aCurrSolution, aElements[i] )){
				aRes.push(aElements[i].value);	
			}
		}
		
		while (aRes.length < 6){
			var i:Number = Math.floor(Math.random()*aElements.length);
			if(!isObjectInArray( p_aCurrSolution, aElements[i] )){
				aRes.push(aElements[i].value);	
			}
		}		

		return aRes;
		
	}
	
	/**
	* isObjectInArray
	* test la presence d'un element dans un tableau
	* @param aObjs le tableau d'objects
	* @param oObj l'element
	* @return true si l'element est trouvé
	*/	
	private function isObjectInArray(aObjs:Array, oObj:Object):Boolean{
		for(var i in aObjs){
			if (aObjs[i].element == oObj.element && aObjs[i].value == oObj.value){
				return true;
			}
		}
		return false;
	}	
	
	/** renvois un objet Symbol vide */
	private function getNewSymbol():Object{
		
		var oSymbol:Object = new Object();
		oSymbol.armure = "";
		oSymbol.insigne = "";
		oSymbol.casque = "";
		oSymbol.queue = "";
		
		return oSymbol;
		
	}
			

	/**
	 * Vérifie la réponse du joueur
	 */
	private function checkAnswer(){
		
		var bWon:Boolean = false;
		var aActiveElements:Array = new Array();
		for(var i=0;i<10;i++){
			if(this["btElement"+i].getActive()){
				aActiveElements.push(this["btElement"+i].data);
			}
		}
		if(aActiveElements.length>0 && aActiveElements.length<3){
			var ok:Boolean = true;
			for(var i in p_aCurrSolution){
				if(!isObjectInArray( aActiveElements, p_aCurrSolution[i] )){
					ok = false;
				}
			}
			bWon = ok;
		}
		
		
		if(bWon){
			// gagné
			p_iNbPartsWon++
			p_txtInfos.text = "Bravo !";
			if(p_iNbPartsWon == p_iNbPartsToWin){
				if(p_iTotalNbTries < 2){
					p_bPalmeOr = true;
				}
				won();
			}else{
				startGame();
				gotoAndStop("step"+(p_iNbPartsWon+1));
			}
		}else{
			this["mcPuce"+p_iCurrNbTries]._visible = true;
			p_txtInfos.text = "Non, essaie encore !";
			p_iCurrNbTries++;	
			p_iTotalNbTries++;
			if(p_iCurrNbTries < p_iNbMaxTries){
				var x:Number = p_mcElements._x -140;
				this[p_mcElements._name].xSlideTo(x, 0.5, "easeInCubic");
			}else if(p_iCurrNbTries == p_iNbMaxTries){
				// trop d'essais, perdu
				loose();
			}
			
		}
		
		
	}
	
	/**
	 * getMcWithData
	 * renvois le bouton portant oData
	 * @param   oData 
	 * @return  le clip
	 */	
	private function getMcWithData(oData:Object):MovieClip{
		
		for(var i=0;i<10;i++){
			if(this["btElement"+i].data.element == oData.element && this["btElement"+i].data.value == oData.value){
				return this["btElement"+i];
			}			
		}
		
		return null;
		
	}
	
	

	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();

	}
	
	
	/**
	 * la partie est perdue
	 * @param msg le message a afficher
	 */
	private function loose(sMsg:String){
		
		gotoAndStop("loose");
		
		p_txtInfos.text = "La solution était...";
		
		// affichage des 6 elements
		for(var i=0;i<6;i++){
			// armure
			p_mcElements["t"+i].mcArmure.gotoAndStop(p_o6Symbols.armure[i]);
			// insigne
			p_mcElements["t"+i].mcArmure.mcInsigne.gotoAndStop(p_o6Symbols.insigne[i]);
			// casque
			p_mcElements["t"+i].mcArmure.mcCasque.gotoAndStop(p_o6Symbols.casque[i]);
			// queue
			p_mcElements["t"+i].mcQueue.gotoAndStop(p_o6Symbols.queue[i]);
		}		
		
		// affichage de la solution
		var mcReponse1:MovieClip = getMcWithData(p_aCurrSolution[0]);
		mcReponse1._x = 450;
		mcReponse1._y = 265;
		mcReponse1.gotoAndStop(2); 
		mcReponse1.enabled = false;
		var mcReponse2:MovieClip = getMcWithData(p_aCurrSolution[1]);
		mcReponse2._x = 560;
		mcReponse2._y = 265;
		mcReponse2.gotoAndStop(2); 		
		mcReponse2.enabled = false;
		
	}	

	
}

﻿/**
 * class com.maths1p4p.level3.game3_05.Game3_05
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * 
 * TODO :
 * certains éléments ne sont pas toujours masqués au démarrage
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import com.prossel.utils.CursorManager;

class maths1p4p.level3.game3_05.Game3_05 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** tirettes */
	public var p_mcTirette1, p_mcTirette2:MovieClip;
	
	/** bouton depart / suite */
	public var p_btDepartSuite:Button;
	public var p_txtDepartSuite:TextField;
	
	/** textField Count */
	public var p_txtCount:TextField;

	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
			
	/** le tableau des tiles */
	private var p_aTiles:Array;
	
	/** tableau des puces */
	private var p_aPuces:Array;
			
	/** le nombre de parties à gagner */
	private var p_iNbPartsToWin:Number = 2;
	/** le nombre de parties gagnées*/
	private var p_iNbPartsWon:Number = 0;	

	/** nombre de coups jouées */
	private var p_iPlayCount:Number = 0;
	

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_01()	{		
		super();	
	}	
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		// init de l'interface
		initInterface();
				
	}	


	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		
		
		p_txtDepartSuite = this["txtDepartSuite"];
		p_txtCount = this["txtCount"];
		
		_parent.p_txtDepartSuite.text = "Départ";
		
		p_mcTirette1._visible = false;
		p_mcTirette2._visible = false;

		startPart();	

	}	
	
	/**
	 * start
	 * départ d'une partie
	 */
	private function startPart():Void{
		
		p_aTiles = new Array(7);
		for(var i=0; i<7;i++){
			p_aTiles[i] = new Array(7);
		}
		
		p_aPuces = new Array();		

		Application.useLibCursor(p_btDepartSuite, "cursor-finger");
		p_btDepartSuite.onRelease = function(){
			_parent.p_txtDepartSuite.text = "J'ai fini";
			_parent.randomizePuces();
			this.onRelease = function(){
				_parent.checkEnd();
			}			
		}
		
		// les tiles
		for(var i=0; i<7;i++){
			for(var j=0;j<7;j++){
				this["mcTile_"+i+"_"+j]["i"] = i;
				this["mcTile_"+i+"_"+j]["j"] = j;
			}
		}		
		
		// les puces
		var iCount:Number = 0;
		for(var i=2; i<5;i++){
			for(var j=2;j<5;j++){
				
				var mc:MovieClip = attachMovie("mcPuce", "mcPuce_"+i+"_"+j, iCount+10);
				mc._x = this["mcTile_"+i+"_"+j]._x;
				mc._y = this["mcTile_"+i+"_"+j]._y;
				mc["initX"] = mc._x;
				mc["initY"] = mc._y;
				mc["i"] = i;
				mc["j"] = j;
				mc.onPress = function(){
					this._x = this._parent._xmouse - this._width/2;
					this._y = this._parent._ymouse - this._height/2;
					startDrag(this);
				}
				Application.useLibCursor(mc, "cursor-finger");
				mc.onRelease = mc.onReleaseOutside = function(){
					CursorManager.hide();
					stopDrag();
					this._parent.dropPuce(this);
					CursorManager.show();
				}
				mc.enabled = false;
				p_aTiles[i][j] = mc;
				p_aPuces.push(mc);
				
				iCount++;

			}
		}

		
	}
	
	/**
	 * randomizePuces
	 * mouvement aléatoire des puces en début de partie
	 */	
	private function randomizePuces():Void{
		
		p_oSoundPlayer.playASound("clicClac",0);

		var aMoves = [-2, 0, 2];

		var iMoveCount:Number = 0;
		var i:Number = 0;
		while (iMoveCount < 12){
			
			var ok:Boolean = true;
			var iTries:Number = 0;
			while(ok){
				var iMoveX:Number = aMoves[Math.floor(Math.random()*aMoves.length)];
				var iMoveY:Number = aMoves[Math.floor(Math.random()*aMoves.length)];
				if(iMoveX != 0 || iMoveY != 0){
					var mcDrop:MovieClip = this["mcTile_"+ (p_aPuces[i].i+iMoveX) + "_" + (p_aPuces[i].j+iMoveY)];
					
					if(checkPuceMove(p_aPuces[i], mcDrop)){
						makeMove(p_aPuces[i], mcDrop);
						ok = false;
						iMoveCount++;
					}		
					iTries++;
					if(iTries > 8) ok = false;
				}
			}			
			i++;
			if (i >= p_aPuces.length) i=0;
			
		}
		
		for(var i in p_aPuces){
			p_aPuces[i].enabled = true;
		}
		
	}
	
	/**
	 * dropPuce
	 * drop d'une puce
	 */	
	private function dropPuce(mcPuce:MovieClip):Void{
		
		p_oSoundPlayer.playASound("miniTic",0);
		
		var mcDrop:MovieClip = eval(mcPuce._droptarget);
		if(mcDrop._name.indexOf("mcTile_") != -1){
			// drop sur un emplacement
			// vérifie que la puce a bien sauté une autre
			if(checkPuceMove(mcPuce, mcDrop)){
				
				makeMove(mcPuce, mcDrop);
				p_iPlayCount++;
				p_txtCount.text = String(p_iPlayCount);				

			}else{
				mcPuce._x = mcPuce.initX;
				mcPuce._y = mcPuce.initY;
				
			}
			
		}else{
			mcPuce._x = mcPuce.initX;
			mcPuce._y = mcPuce.initY;
		}
		
	}
	
	/**
	 * makeMove
	 * fait un mouvement de puce
	 */	
	private function makeMove(mcPuce:MovieClip, mcDrop:MovieClip):Void{
		
		p_aTiles[mcPuce.i][mcPuce.j] = null;
		mcPuce.i = mcDrop.i;
		mcPuce.j = mcDrop.j;
		p_aTiles[mcDrop.i][mcDrop.j] = mcPuce;
		mcPuce._x = mcDrop._x;
		mcPuce._y = mcDrop._y;
		mcPuce.initX = mcPuce._x
		mcPuce.initY = mcPuce._y
		
	}
	
	/**
	 * checkPuceMove
	 * check si un deplacement est correcte
	 */	
	private function checkPuceMove(mcPuce:MovieClip, mcDrop:MovieClip):Boolean{

		// test que le deplacement est de 0 ou 2 tiles
		var iOffset:Number = mcPuce.i - mcDrop.i;
		var jOffset:Number = mcPuce.j - mcDrop.j;
		if(Math.abs(iOffset) == 2 || Math.abs(iOffset) == 0 ){
			if(Math.abs(jOffset) == 2 || Math.abs(jOffset) == 0 ){
				// test qu'il y a bien une puce entre les 2
				if(p_aTiles[mcPuce.i - iOffset/2][mcPuce.j - jOffset/2] != null){
					// test qu'il n'y  a pas déjà une puce à cette place
					if(p_aTiles[mcDrop.i][mcDrop.j]==null){
						return true;
					}
				}			
			}			
		}		
		
		return false;
		
	}
	
	
	/**
	 * checkEnd
	 * test si la partie est gagnée
	 */	
	private function checkEnd(){
		
		p_oSoundPlayer.playASound("bong2",0);
		
		// check les puces
		var bAllIsOk:Boolean = true;
		for(var i=2; i<5;i++){
			for(var j=2;j<5;j++){
				if(p_aTiles[i][j] == null){
					bAllIsOk = false;
				}
			}
		}
		
		if(bAllIsOk){
			// partie gagnée
			p_iNbPartsWon++;
			if (p_iNbPartsToWin == p_iNbPartsWon){
				
				if(p_iPlayCount <= 30){
					if(p_iPlayCount <= 24){
						p_bPalmeOr = true;
					}
					// partie gagnée
					p_mcTirette2._visible = true;
					won();
				}else{
					loose();
				}
			}else{
				// 2e partie, réinitialisation
				startPart();				
				p_txtDepartSuite.text = "Suite";
				p_mcTirette1._visible = true;
			}
		}
		
	}
	
	
	/**
	 * loose
	 * la partie est perdue
	 */
	private function loose():Void{
		
		// cache les puces
		for(var i in p_aPuces){
			p_aPuces[i]._visible = false;
		}				
		
		gotoAndStop("loose");	
		
	}
	
	
	/**
	 * won
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();
		
		p_oSoundPlayer.playASound("cle6",0);
		
		// cache les puces
		for(var i in p_aPuces){
			p_aPuces[i]._visible = false;
		}		
		
	}
	
}

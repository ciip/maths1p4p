﻿/**
 * class com.maths1p4p.level3.game3-04.Game3-04
 * 
 * @author Grégory Lardon
 * @version 0.2
 * 
 * ETAT : 
 * Le jeu semble fonctionner correctement
 * 
 * TODO :
 * -> manque l'intégration des sons
 * -> gestion des loots
 * -> gestion du jeu déjà gagné, loot différent...
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import com.prossel.utils.CursorManager;
import maths1p4p.AbstractGame;
import maths1p4p.level3.game3_06.*
import flash.geom.Point;
import flash.filters.GlowFilter;

class maths1p4p.level3.game3_06.Game3_06 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** les boutons de commande */
	public var p_btFlipH:Button;
	public var p_btFlipV:Button;
	public var p_btLeft:Button;
	public var p_btRight:Button;
	
	/** le bouton depart suite */
	public var p_btDepartSuite:Button;
	
	/** le textfield depart/suite */
	public var p_txtDepartSuite:TextField;
	
	/** les couronnes */
	public var p_mcCouronne1, p_mcCouronne2:MovieClip
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** la grille en cours */
	private var p_aCurrGrid;
	
	/** l'element ionné */
	private var p_mcCurrElem:MovieClip;
	
	/** le nombre de parties à gagner */
	private var p_iNbPartsToWin:Number = 2;
	/** le nombre de parties gagnées*/
	private var p_iNbPartsWon:Number = 0;
	
	/** le numéro de la partie en cours */
	private var p_iCurrPart:Number;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game06()	{		
		super();				
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){		
		
		// initialisation de l'interface
		initInterface();
		
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		
		
		p_mcCouronne1._visible = false;
		p_mcCouronne2._visible = false;
		
		p_txtDepartSuite = this["txtDepartSuite"];
		
		Application.useLibCursor(p_btDepartSuite, "cursor-finger");
		p_btDepartSuite.onRelease = function(){
			_parent.p_txtDepartSuite.text = "J'ai fini";
			_parent.startNewPart(1);
			this.onRelease = function(){
				_parent.checkAnswer();
			}
		}
		
		// init des boutons
		Application.useLibCursor(p_btFlipH, "cursor-finger");
		p_btFlipH.onRelease = function(){
			_parent.flipH(_parent.p_mcCurrElem);
		}
		Application.useLibCursor(p_btFlipV, "cursor-finger");
		p_btFlipV.onRelease = function(){
			_parent.flipV(_parent.p_mcCurrElem);
		}
		Application.useLibCursor(p_btLeft, "cursor-finger");
		p_btLeft.onRelease = function(){
			_parent.rotate(_parent.p_mcCurrElem, -1);
		}
		Application.useLibCursor(p_btRight, "cursor-finger");
		p_btRight.onRelease = function(){
			_parent.rotate(_parent.p_mcCurrElem, +1);
		}
						
	}	
	
	/**
	 * démarrage d'une partie
	 */
	private function startNewPart(idPart:Number){
		
		p_oSoundPlayer.playASound("swiftCliClac",0);
		
		p_iCurrPart = idPart;
		gotoAndStop("part"+idPart);
		this["mcWin"]._visible = false;
		
		// init des grilles 
		p_aCurrGrid = [];
		p_aCurrGrid[0] = [-1, -1, -1, -1, -1];
		p_aCurrGrid[1] = [-1, -1, -1, -1, -1];
		p_aCurrGrid[2] = [-1, -1, -1, -1, -1];
		p_aCurrGrid[3] = [-1, -1, -1, -1, -1];
		
		switch(idPart){
			
			case 1: 
				// init des pieces/clips puzzle 1
				this["mcElem1_3"]["value1"] = new Point(0,0);
				this["mcElem1_3"]["value2"] = new Point(0,1);

				this["mcElem1_10"]["value1"] = new Point(1,0);
				this["mcElem1_10"]["value2"] = new Point(2,0);

				this["mcElem1_1"]["value1"] = new Point(3,0);
				this["mcElem1_1"]["value2"] = new Point(3,1);

				this["mcElem1_9"]["value1"] = new Point(1,1);
				this["mcElem1_9"]["value2"] = new Point(2,1);

				this["mcElem1_5"]["value1"] = new Point(0,2);
				this["mcElem1_5"]["value2"] = new Point(1,2);
				
				this["mcElem1_6"]["value1"] = new Point(2,2);
				this["mcElem1_6"]["value2"] = new Point(3,2);
				
				this["mcElem1_4"]["value1"] = new Point(0,3);
				this["mcElem1_4"]["value2"] = new Point(0,4);
				
				this["mcElem1_2"]["value1"] = new Point(1,3);
				this["mcElem1_2"]["value2"] = new Point(1,4);
				
				this["mcElem1_8"]["value1"] = new Point(2,3);
				this["mcElem1_8"]["value2"] = new Point(3,3);
				
				this["mcElem1_7"]["value1"] = new Point(2,4);
				this["mcElem1_7"]["value2"] = new Point(3,4);
				
				break;
			
			case 2: 
				// init des pieces/clips puzzle 2
				this["mcElem2_3"]["value1"] = new Point(2,2);
				this["mcElem2_3"]["value2"] = new Point(2,3);

				this["mcElem2_10"]["value1"] = new Point(0,0);
				this["mcElem2_10"]["value2"] = new Point(0,1);

				this["mcElem2_1"]["value1"] = new Point(0,3);
				this["mcElem2_1"]["value2"] = new Point(0,4);

				this["mcElem2_9"]["value1"] = new Point(1,3);
				this["mcElem2_9"]["value2"] = new Point(1,4);

				this["mcElem2_5"]["value1"] = new Point(3,2);
				this["mcElem2_5"]["value2"] = new Point(3,3);
				
				this["mcElem2_6"]["value1"] = new Point(2,1);
				this["mcElem2_6"]["value2"] = new Point(3,1);
				
				this["mcElem2_4"]["value1"] = new Point(2,4);
				this["mcElem2_4"]["value2"] = new Point(3,4);
				
				this["mcElem2_2"]["value1"] = new Point(1,0);
				this["mcElem2_2"]["value2"] = new Point(1,1);
				
				this["mcElem2_8"]["value1"] = new Point(0,2);
				this["mcElem2_8"]["value2"] = new Point(1,2);
				
				this["mcElem2_7"]["value1"] = new Point(2,0);
				this["mcElem2_7"]["value2"] = new Point(3,0);
				
				break;
				
			case 3: 
				// init des pieces/clips puzzle 2
				this["mcElem3_3"]["value1"] = new Point(1,2);
				this["mcElem33_"]["value2"] = new Point(1,3);

				this["mcElem3_10"]["value1"] = new Point(0,2);
				this["mcElem3_10"]["value2"] = new Point(0,3);

				this["mcElem3_1"]["value1"] = new Point(0,4);
				this["mcElem3_1"]["value2"] = new Point(1,4);

				this["mcElem3_9"]["value1"] = new Point(2,3);
				this["mcElem3_9"]["value2"] = new Point(2,4);

				this["mcElem3_5"]["value1"] = new Point(0,0);
				this["mcElem3_5"]["value2"] = new Point(0,1);
				
				this["mcElem3_6"]["value1"] = new Point(2,2);
				this["mcElem3_6"]["value2"] = new Point(3,2);
				
				this["mcElem3_4"]["value1"] = new Point(3,0);
				this["mcElem3_4"]["value2"] = new Point(3,1);
				
				this["mcElem3_2"]["value1"] = new Point(1,0);
				this["mcElem3_2"]["value2"] = new Point(2,0);
				
				this["mcElem3_8"]["value1"] = new Point(1,1);
				this["mcElem3_8"]["value2"] = new Point(2,1);
				
				this["mcElem3_7"]["value1"] = new Point(3,3);
				this["mcElem3_7"]["value2"] = new Point(3,4);
				
				break;				
			
		}
		
		// init des elements
		for(var i=1; i<11;i++){
			Application.useLibCursor(this["mcElem"+p_iCurrPart+"_"+i], "cursor-finger");
			this["mcElem"+p_iCurrPart+"_"+i].onPress = function(){
				_parent.selectElement(this);
			}
			this["mcElem"+p_iCurrPart+"_"+i].onRelease = this["mcElem"+p_iCurrPart+"_"+i].onReleaseOutside = function(){
				CursorManager.hide();
				_parent.dropElement(this);
				CursorManager.show();
			}
			this["mcElem"+p_iCurrPart+"_"+i].initX = this["mcElem"+p_iCurrPart+"_"+i]._x;
			this["mcElem"+p_iCurrPart+"_"+i].initY = this["mcElem"+p_iCurrPart+"_"+i]._y;
			this["mcElem"+p_iCurrPart+"_"+i].flipH = false;
			this["mcElem"+p_iCurrPart+"_"+i].flipV = false;
			this["mcElem"+p_iCurrPart+"_"+i].rot = 0; 
			
			
			// flip H
			var bFlipH:Boolean = false;
			if(Math.floor(Math.random()*2)==1){
				flipH(this["mcElem"+p_iCurrPart+"_"+i], false);
				bFlipH = true;
			}
			
			// flip V
			var bFlipV:Boolean = false;
			if(Math.floor(Math.random()*2)==1){
				flipV(this["mcElem"+p_iCurrPart+"_"+i], false);
				bFlipV = true;
			}
			
			// rotation
			var iRotation = Math.floor(Math.random()*4)==1;
			if(iRotation != 0)	rotate(this["mcElem"+p_iCurrPart+"_"+i], iRotation, false);
			
			

		}
		
		// init des tiles
		for(var i=0;i<4;i++){
			for(var j=0;j<5;j++){
				this["mcTile_"+i+"_"+j]["i"] = i;
				this["mcTile_"+i+"_"+j]["j"] = j;
			}
		}
		
	}
	
	/**
	 * fait un flip horizontal sur le clip
	 */
	private function flipH(mcElem:MovieClip, soundOn:Boolean){
		
		if (soundOn) p_oSoundPlayer.playASound("pas2",0);
		
		mcElem._xscale *= -1;
		if(mcElem._xscale == 100){
			mcElem["flipH"] = false;
		}else{
			mcElem["flipH"] = true;
		}
		
	}
	
	/**
	 * fait un flip vertical sur le clip
	 */
	private function flipV(mcElem:MovieClip, soundOn:Boolean){
		
		if (soundOn) p_oSoundPlayer.playASound("pas2",0);
		
		mcElem._yscale *= -1;
		if(mcElem._yscale == 100){
			mcElem["flipV"] = false;
		}else{
			mcElem["flipV"] = true;
		}
		
	}	
	
	/**
	 * rotation sur le clip
	 */
	private function rotate(mcElem:MovieClip, iRotation:Number, soundOn:Boolean){
		
		if (soundOn) p_oSoundPlayer.playASound("pas2",0);

		var iNbRotations:Number = Math.abs(iRotation);
		if(iRotation>0){
			var iSens:Number = 1;
		}else{
			var iSens:Number = -1;
		}
		
		
		for(var i=0; i<iNbRotations; i++){
			mcElem._rotation += 90*iSens;
		}
		
		mcElem["rot"] += iRotation;		
		if(mcElem["rot"] == 4) mcElem["rot"] = 0;
		if(mcElem["rot"] == -1) mcElem["rot"] = 3;
		
	}		
		
	/**
	 * clic sur un élément
	 */
	private function selectElement(mcElem:MovieClip):Void{
		
		if(p_mcCurrElem == mcElem){
			if(mcElem._width > mcElem._height){
				mcElem._x = _xmouse + 20;
				mcElem._y = _ymouse;
			}else{
				mcElem._x = _xmouse;
				mcElem._y = _ymouse + 20;
			}
			mcElem.swapDepths(this.getNextHighestDepth());
			mcElem.startDrag(false);
		}else{
			p_mcCurrElem.filters = [];
			p_mcCurrElem = mcElem;
			if(p_mcCurrElem["droped"] == true){
					// l'element est déjà en place
				if(mcElem._width > mcElem._height){
					mcElem._x = _xmouse + 20;
					mcElem._y = _ymouse;
				}else{
					mcElem._x = _xmouse;
					mcElem._y = _ymouse + 20;
				}				
				p_mcCurrElem.swapDepths(this.getNextHighestDepth());
				p_mcCurrElem.startDrag(false);				
				// vide dans p_aCurrGrid
				p_aCurrGrid[p_mcCurrElem["tile1"].x][p_mcCurrElem["tile1"].y] = -1;
				p_aCurrGrid[p_mcCurrElem["tile2"].x][p_mcCurrElem["tile2"].y] = -1;
				
			}else{
				// sinon				
				var glowFilter:GlowFilter = new GlowFilter(0x00FF00,100,7,7,1000);
				p_mcCurrElem.filters = [glowFilter];
			}
			
		}	
		
	}
	
	/**
	 * drop d'un élément
	 */
	private function dropElement(mcElem:MovieClip):Void{
		
		p_oSoundPlayer.playASound("clic",0);
		
		stopDrag();
		var mcTile:MovieClip = eval(mcElem._droptarget);
		mcElem["droped"] = false;

		if(mcTile != null && mcTile._name.indexOf("mcTile_") != -1){
			// placé sur une tuile		
			if(p_aCurrGrid[mcTile["i"]][mcTile["j"]] == -1){
				// la place est libre
				if(mcElem._width > mcElem._height){
					// element horizontal, test la tile suivante sur i
					if(p_aCurrGrid[mcTile["i"]+1][mcTile["j"]] == -1){
						// ok
						mcElem._x = mcTile._x + mcElem._width/2;
						mcElem._y = mcTile._y + mcElem._height/2;
						mcElem.filters = [];
						mcElem["droped"] = true;
						p_mcCurrElem = null;
						mcElem["tile1"] = new Point(mcTile["i"], mcTile["j"]);
						mcElem["tile2"] = new Point(mcTile["i"]+1, mcTile["j"]);
						// vide dans p_aCurrGrid
						p_aCurrGrid[mcTile["i"]][mcTile["j"]] = mcElem;
						p_aCurrGrid[mcTile["i"]+1][mcTile["j"]] = mcElem;						
						
					}else{
						// perdu
						mcElem._x = mcElem.initX;
						mcElem._y = mcElem.initY;
					}
				}else{
					// element vertical, test la tile suivante sur j
					if(p_aCurrGrid[mcTile["i"]][mcTile["j"]+1] == -1){
						mcElem._x = mcTile._x + mcElem._width/2;
						mcElem._y = mcTile._y + mcElem._height/2;
						mcElem.filters = [];
						mcElem["droped"] = true;
						p_mcCurrElem = null;
						mcElem["tile1"] = new Point(mcTile["i"], mcTile["j"]);
						mcElem["tile2"] = new Point(mcTile["i"], mcTile["j"]+1);
						// vide dans p_aCurrGrid
						p_aCurrGrid[mcTile["i"]][mcTile["j"]] = mcElem;
						p_aCurrGrid[mcTile["i"]][mcTile["j"]+1] = mcElem;							
					}else{
						// perdu
						mcElem._x = mcElem.initX;
						mcElem._y = mcElem.initY;
					}
				}
			}else{
				
				mcElem._x = mcElem.initX;
				mcElem._y = mcElem.initY;
				
			}
			
		}else{
			// retour à sa place
			mcElem._x = mcElem.initX;
			mcElem._y = mcElem.initY;
		}
		

		
	}
		

	/**
	 * Vérifie la réponse du joueur
	 */
	private function checkAnswer(){

		var bWon:Boolean = true;
		for(var i=0;i<p_aCurrGrid.length; i++){
			for(var j=0;j<p_aCurrGrid[i].length; j++){
				
				if(p_aCurrGrid[i][j] != -1){
					// la case est occupée
					var mcElem:MovieClip = p_aCurrGrid[i][j];
					if( ! ( (mcElem["tile1"].x == i && mcElem["tile1"].y == j) || (mcElem["tile2"].x == i && mcElem["tile2"].y == j) ) ){
						bWon = false;
					}
					
				}else{
					// la case est vide, perdu
					bWon = false;
				}
				
			}
		}
		
		if(bWon){

			// check maintenant les rotations et les  flips
			for(var i=1; i<11;i++){
				
				if( this["mcElem"+p_iCurrPart+"_"+i].rot != 0 ){
					
					if( this["mcElem"+p_iCurrPart+"_"+i].rot == 2 ){
						// si les 2 flips sont actif, cela revient à rot=0
						if( ! (this["mcElem"+p_iCurrPart+"_"+i].flipH != false && this["mcElem"+p_iCurrPart+"_"+i].flipV != false) ){
							bWon = false;
						}
					}else{
						bWon = false;
					}
					
				}else{
					if( this["mcElem"+p_iCurrPart+"_"+i].flipH != false ){
						bWon = false;
					}
					if( this["mcElem"+p_iCurrPart+"_"+i].flipV != false ){
						bWon = false;
					}
				}

			}
	
		}
		
		if(!bWon){
			// on va tester si le puzzle n'est pas bon en mode flipH
			bWon = true;
			
			// test si toutes les pièces sont flipHed !
			for(var i=1; i<11;i++){				
				var mcElem:MovieClip = this["mcElem"+p_iCurrPart+"_"+i];				
				if( !(mcElem.rot == 0  && mcElem.flipH && !mcElem.flipV)  && !(mcElem.rot == 2  && !mcElem.flipH && mcElem.flipV)) {
					bWon = false;
				}
			}
			
			// test maintenant les position flipéés
			if(bWon){
				for(var i=0;i<p_aCurrGrid.length; i++){
					for(var j=0;j<p_aCurrGrid[i].length; j++){
						
						if(p_aCurrGrid[i][j] != -1){
							// la case est occupée
							var i2:Number = 3-i;
							var mcElem:MovieClip = p_aCurrGrid[i2][j];
							
							if( ! ( (mcElem["tile1"].x == i2 && mcElem["tile1"].y == j) || (mcElem["tile2"].x == i2 && mcElem["tile2"].y == j) ) ){
								bWon = false;
							}
							
						}else{
							// la case est vide, perdu
							bWon = false;
						}
						
					}
				}			
			}
			
			
		}
		
		if(bWon){
			for(var i=1; i<11;i++){
				this["mcElem"+p_iCurrPart+"_"+i]._visible = false;
			}				
			p_iNbPartsWon++;
			if(p_iNbPartsWon == p_iNbPartsToWin){
				p_mcCouronne2._visible = true;
				won();
			}else{
				p_mcCouronne1._visible = true;
				var idPart:Number = Math.floor(Math.random()*2)+2;
				startNewPart(idPart);
				
			}
		}else{
			mx.controls.Alert.show("Ton puzzle n'est pas juste !");
		}
		
	}	
	

	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();
		
		p_oSoundPlayer.playASound("metalOndule",0);
				
		this["mcWin"]._visible = true;
		p_btDepartSuite.enabled = false;

	}
	
	
	/**
	 * la partie est perdue
	 * @param msg le message a afficher
	 */
	private function loose(sMsg:String){
		
		gotoAndStop("loose");
		
	}	

	
}

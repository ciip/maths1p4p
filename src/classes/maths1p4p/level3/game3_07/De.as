﻿/**
 * class com.maths1p4p.level3.game3_07.De
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * 
 * TODO :
 * certains éléments ne sont pas toujours masqués au démarrage
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level3.game3_07.De extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	/** la valeur du dé */
	public var p_iValue:Number;
	
	/** l'effet seleted */
	public var mcSelected:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	/** nombre de frame écoulées lors de l'anim du dé */
	private var p_iAnimFrameCount:Number;
	
	/** init x et y */
	private var p_iInitX, p_iInitY:Number;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function De()	{	
		p_iInitX = _x;
		p_iInitY = _y;
		mcSelected._visible = false;
		enabled = false;
		Application.useLibCursor(this, "cursor-finger");
	}	
	
	/** 
	* roll
	* random de la valeur du de, et animation
	*/
	public function roll():Void{
		
		p_iValue = Math.floor(Math.random()*6)+1;
		
		p_iAnimFrameCount = 0;
		this.onEnterFrame = function(){
			if(p_iAnimFrameCount%3==0){
				var cond:Boolean = true;
				while(cond){
					var iNewFrame:Number = Math.floor(Math.random()*6)+1;
					if(iNewFrame != _currentframe) cond = false;
				}
				gotoAndStop(iNewFrame);
				
			}
			p_iAnimFrameCount++;
			if(p_iAnimFrameCount >= 20){
				delete this.onEnterFrame;
				gotoAndStop(p_iValue);
			}
		}
		
		enabled = true;
		
	}
	
	/** 
	* getValue
	* renvois p_iValue
	*/
	public function getValue():Number{
		return p_iValue;
	}
	
	public function onPress(){
		startDrag(true);
	}
	
	public function onRelease(){
		
		stopDrag();
		
		// test des collisions
		var bDrop:Boolean = false;
		if(_parent.p_mcDigit1.hitTest(this) && _parent.p_mcDigit1.txt.text == ""){
			_parent.p_mcDigit1.txt.text = p_iValue;
			bDrop = true;
		}else if(_parent.p_mcDigit2.hitTest(this) && _parent.p_mcDigit2.txt.text == ""){
			_parent.p_mcDigit2.txt.text = p_iValue;
			bDrop = true;
		}else if(_parent.p_mcDigit3.hitTest(this) && _parent.p_mcDigit3.txt.text == ""){
			_parent.p_mcDigit3.txt.text = p_iValue;
			bDrop = true;
		}
		
		if (bDrop){
			mcSelected._visible = true;
			enabled = false;
		}
		_x = p_iInitX;
		_y = p_iInitY;
				
	}
	
	public function onReleaseOutside(){
		onRelease();
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	

	
	
	
}

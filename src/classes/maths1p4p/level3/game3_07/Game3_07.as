﻿/**
 * class com.maths1p4p.level3.game3_07.Game3_07
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * 
 * TODO :
 * certains éléments ne sont pas toujours masqués au démarrage
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import maths1p4p.level3.game3_07.*

class maths1p4p.level3.game3_07.Game3_07 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** les loots */
	public var p_mcLoots:MovieClip;
	
	/** les machines */
	public var p_mcMachine1:MovieClip;
	public var p_mcMachine2:MovieClip;
	
	/** le bouton joueurs */
	private var p_btJoueur:Button
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** joueurs */
	private var p_oPlayer1, p_oPlayer2;
	
	/** tour actuel, = 0 ou 1 */
	private var p_iCurrTurn:Number;
	
	/** tableau contenant les 2 machines */
	private var p_aMachines:Array;
			
	/** le tableau des slots */
	private var p_aSlots:Array;
			
	/** le nombre de parties à gagner */
	private var p_iNbPartsToWin:Number = 6;
	/** le nombre de parties gagnées*/
	private var p_iNbPartsWon:Number = 0;	
	/** nombre d'essais */
	private var p_iNbTries:Number;
	/** nombre d'essais maximum*/
	private var p_iNbMaxTries:Number = 3;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_07()	{		
		super();	
	}	
		
	/**
	 * secondPlayerSelected
	 * appellé depuis level3
	 */
	public function secondPlayerSelected(oPlayer2:Object):Void{
		
		p_oPlayer1 = new Object();
		p_oPlayer1.name = _parent._parent.getPlayer().getName();
		p_oPlayer1.id = _parent._parent.getPlayer().getId();
		p_oPlayer2 = oPlayer2;
		
		initInterface();
		
	}
	
	/**
	 * un joueur a gagné
	 */
	public function setVictory(mcMachineWin:MovieClip){
		
		if(mcMachineWin.p_oPlayer.name == p_oPlayer1.name){
			// c'est le premier joueur qui gagne
			won();
		}else{
			// c'est le 2e joueur qui gagne
			
		}
		
		

	}

	/**
	 * définis le status activé du bouton joueurs
	 */
	public function setBtJoueurEnabled(bEnabled:Boolean){
		p_btJoueur.enabled = bEnabled;
		if(bEnabled){
			p_btJoueur._alpha = 100;
		}else{
			p_btJoueur._alpha = 60;
		}
	}
	
	/**
	 * partie suivante
	 */
	public function nextPart():Void{
		
		p_mcMachine1.clearAll();
		p_mcMachine2.clearAll();		
		
		// premier tour au hazard
		p_iCurrTurn = Math.floor( Math.random()*2 );		
		nextTurn();	
		
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
	
		p_aMachines = [p_mcMachine1,p_mcMachine2];
		
		// affichage de l'écran de sélection du joueur 2
		if(_parent._parent != null){
			_parent._parent.showSecondPlayerSelectionScreen();
		}else{
			// mode autonome
			p_oPlayer1 = new Object();
			p_oPlayer1.name = "joueur 1";
			p_oPlayer1.id = 1;
			
			p_oPlayer2 = new Object();
			p_oPlayer2.name = "joueur 2";
			p_oPlayer2.id = 2;
			
			// init de l'interface
			initInterface();
			
		}
		

				
	}	


	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		// init des machines
		p_mcMachine1.setPlayer(p_oPlayer1);
		p_mcMachine2.setPlayer(p_oPlayer2);
		
		Application.useLibCursor(p_btJoueur, "cursor-finger");
		p_btJoueur.onRelease = function(){
			_parent.nextTurn();
		}
		setBtJoueurEnabled(false);

		// nouvelle partie
		nextPart();

	}	
	
	
	private function nextTurn():Void{
		
		p_oSoundPlayer.playASound("clicClac6",0);
		
		p_aMachines[p_iCurrTurn].sleep();
		
		if(p_iCurrTurn==0){
			p_iCurrTurn = 1;
		}else{
			p_iCurrTurn = 0;
		}
		
		p_aMachines[p_iCurrTurn].start();
		
		setBtJoueurEnabled(false);
		
	}
	
	
	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();
		
	}
	
}

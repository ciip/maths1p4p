﻿/**
 * class com.maths1p4p.level3.game3_07.Machine
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * 
 * TODO :
 * certains éléments ne sont pas toujours masqués au démarrage
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level3.game3_07.Machine extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	/** les dés */
	public var p_mcDe1, p_mcDe2, p_mcDe3:MovieClip;
	/** les chiffres */
	public var p_mcDigit1, p_mcDigit2, p_mcDigit3:MovieClip;
	
	/** couronnes */
	public var p_mcCouronne1, p_mcCouronne2:MovieClip;
	
	/** le nombre en cours */
	public var p_mcResult:MovieClip;	
	
	/** le bouton déclencheur */
	public var p_btLaunch:Button;
	/** la manette de validation du nombre */
	public var p_btValidate:Button;
	/** le bouton supprimer */
	public var p_btCancel:Button;
	
	/** le mask jaune sur le nom du joueur */
	public var p_mcTxtPlayerMask:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_iCurrNumber:Number;
	
	/** les 7 clips marches */
	private var aDropPlaces:Array;
	
	/** l'objet joueur lié à la machine */
	private var p_oPlayer:Object;
	
	/** nom de victoires */
	private var p_iNumberWon:Number;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Machine()	{	
		
		p_iNumberWon = 0;
		
		p_mcCouronne1._visible = false;
		p_mcCouronne2._visible = false;
		
		aDropPlaces = [this["p_mcMarche1"],this["p_mcMarche2"],this["p_mcMarche3"],this["p_mcMarche4"],this["p_mcMarche5"],this["p_mcMarche6"],this["p_mcMarche7"]];
		for(var i=0;i<aDropPlaces.length;i++){
			aDropPlaces[i]["id"] = i;
		}
		
		p_mcTxtPlayerMask._alpha = 50;
		
	}	
	
	/**
	 * setPlayer
	 */
	public function setPlayer(oPlayer:Object):Void{
		p_oPlayer = oPlayer;
		this["p_txtPlayer"].text = oPlayer.name;
	}
	
	/**
	 * start
	 * Le joueur doit jouer
	 */
	public function start():Void{

		p_mcTxtPlayerMask._alpha = 0;
		gotoAndStop("play");	
		Application.useLibCursor(p_btLaunch, "cursor-finger");
		p_btLaunch.onRelease = function(){
			_parent.desRoll();
		}
		
	}
	
	/**
	 * sleep
	 * La machine est inactive
	 */
	public function sleep():Void{
		
		gotoAndStop(1);
		Application.useLibCursor(p_btLaunch, "cursor-finger");
		p_btLaunch.onRelease = function(){
			_parent.desRoll();
		}
		
		p_mcTxtPlayerMask._alpha = 50;
		
	}
	
	public function addResult(mcDropPlace:MovieClip, txt:String):Boolean{
		
		var iResult:Number = Number(txt);		
		var bPossible:Boolean = true;
		
		for(var i=mcDropPlace["id"];i>=0;i--){
			if (Number(aDropPlaces[i].txt.text) > iResult){
				// mauvaise position
				bPossible = false;
			}
		}

		for(var i=mcDropPlace["id"];i<aDropPlaces.length;i++){
			if (Number(aDropPlaces[i].txt.text) < iResult){
				// mauvaise position
				bPossible = false;
			}
		}
		
		if(bPossible){
			mcDropPlace.txt.text = iResult;		
			gotoAndStop(1);
			if(!IsFinished()){		
				_parent.setBtJoueurEnabled(true);
			}			
		}
		
		return bPossible;
		
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	
	private function clearAll():Void{
		
		for (var i in aDropPlaces){
			aDropPlaces[i].txt.text = "";
		}
		
	}
	
	private function IsFinished():Boolean{
		
		var finished:Boolean = true;
		for(var i=0;i<aDropPlaces.length;i++){
			if(aDropPlaces[i].txt.text == ""){
				finished = false;
			}			
		}

		if(finished){
			p_iNumberWon++;
			_parent.soundPlayer.playASound("carrelage",0);
			this["p_mcCouronne"+p_iNumberWon]._visible = true;
			if(p_iNumberWon == 2){
				_parent.setVictory(this);
			}else{
				_parent.nextPart();
			}
		}
		
		return finished;
		
	}
	
	private function desRoll():Void{
		
		_parent.soundPlayer.playASound("des",0);
		
		gotoAndStop("select");
		this.onEnterFrame = function(){
			p_mcDe1.roll();
			p_mcDe2.roll();
			p_mcDe3.roll();
			delete this.onEnterFrame;
		}	

		Application.useLibCursor(p_btValidate, "cursor-finger");
		p_btValidate.onRelease = function(){
			_parent.validateNumber();
		}
		Application.useLibCursor(p_btCancel, "cursor-finger");
		p_btCancel.onRelease = function(){
			_parent.deleteNumber();
		}
		
	}
	
	
	private function validateNumber():Void{
		
		_parent.soundPlayer.playASound("fefeClac",0);
		
		if (p_mcDigit1.txt.text != "" && p_mcDigit2.txt.text != "" && p_mcDigit3.txt.text != ""){
					
			p_iCurrNumber = Number (p_mcDigit1.txt.text + p_mcDigit2.txt.text +p_mcDigit3.txt.text);
			
			p_mcDigit1.txt.text = "";
			p_mcDigit2.txt.text = "";
			p_mcDigit3.txt.text = "";
			
			gotoAndStop("drag");
			
			p_mcResult.enabled = true;
			p_mcResult.txt.text = p_iCurrNumber;
			
		}
		
	}
	
	
	private function deleteNumber():Void{
		_parent.soundPlayer.playASound("clicClac6",0);
		p_mcResult.txt.text = "";
		p_mcResult.enabled = false;
		p_mcDigit1.txt.text = "";
		p_mcDigit2.txt.text = "";
		p_mcDigit3.txt.text = "";
		sleep();
		_parent.setBtJoueurEnabled(true);
	}
	
	
	
	
}

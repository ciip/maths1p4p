﻿/**
 * class com.maths1p4p.level3.game3_07.Result
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * 
 * TODO :
 * certains éléments ne sont pas toujours masqués au démarrage
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import com.prossel.utils.CursorManager;

class maths1p4p.level3.game3_07.Result extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** init x et y */
	private var p_iInitX, p_iInitY:Number;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Result()	{	
		p_iInitX = _x;
		p_iInitY = _y;
		enabled = false;
	}	
	
	
	
	public function onPress(){
		_x = _parent._xmouse - _width/2;
		_y = _parent._ymouse - _height/2;
		startDrag(true);
	}
	
	public function onRelease(){
		
		CursorManager.hide();
		
		stopDrag();
		
		// test des collisions
		var bDrop:Boolean = false;
		for(var i=1; i<8; i++){
			if (_parent["p_mcMarche"+i].hitTest(_root._xmouse, _root._ymouse) && !bDrop){
				if(_parent["p_mcMarche"+i].txt.text == ""){
					
					var dropPlace:MovieClip = _parent["p_mcMarche"+i];
					if(_parent.addResult(dropPlace, this["txt"].text)){
						bDrop = true;
					}
				}
			}
		}
		
		if (bDrop){
			this["txt"].text = "";
			enabled = false;
		}else{
			// test si le drop n'est pas sur la poubelle
			if (_parent["p_mcCancel"].hitTest(_root._xmouse, _root._ymouse) ){
				_parent.deleteNumber();
			}
		}
		
		_x = p_iInitX;
		_y = p_iInitY;
		
		CursorManager.show();
				
	}
	
	public function onReleaseOutside(){
		onRelease();
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	

	
	
	
}

﻿/**
 * class com.maths1p4p.level3.game3_08.Game3_08
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import maths1p4p.level3.game3_08.*
import flash.geom.Point;

class maths1p4p.level3.game3_08.Game3_08 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** les boutons */
	public var p_btUp, p_btDown, p_btRight, p_btLeft:Button;
	
	/** textfields */
	public var p_txtCount:TextField;
	
	// la cle
	public var p_mcKey:MovieClip;
	
	// la serrure
	public var p_mcSerrure:MovieClip
	
	// l'info bulle
	public var p_mcBulle:MovieClip;
	
	// le plan
	public var p_mcPlan:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var p_aMap:Array;
	
	private var p_ptCurrPos:Point;
	private var p_ptCurrDir:Point;
	
	private var p_aDirs:Array;
	private var p_iCurrDirId:Number;
	
	private var p_ptDestPos:Point;
	private var p_ptDestDir:Point;
	
	private var p_aDestPoss:Array;
	private var p_aDestDirs:Array;
	
	private var p_ptBackPos:Point;
	private var p_ptBackDir:Point;

	private var p_bGotKey:Boolean = false;
	
	private var p_iStepCount:Number;

	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_08()	{		
		super();	
	}	
		



	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		// creation de la map
		p_aMap = [];
		p_aMap[0] = [0,0,0,0,0,0,0,1,1,0,0,0];
		p_aMap[1] = [0,0,0,0,0,0,0,0,1,1,1,1];
		p_aMap[2] = [0,1,0,1,1,1,1,0,0,1,0,1];
		p_aMap[3] = [0,1,1,1,0,0,1,1,1,1,0,0];
		p_aMap[4] = [0,0,0,1,0,0,0,1,0,0,0,0];
		p_aMap[5] = [1,1,1,1,0,1,1,1,1,0,1,0];
		p_aMap[6] = [1,0,0,0,0,1,0,0,1,1,1,0];
		p_aMap[7] = [0,0,0,0,1,1,0,0,0,0,0,0];
		
		// les 4 arrivées possibles
		p_aDestPoss = new Array(
			new Point(7,4),
			new Point(5,10),
			new Point(2,11),
			new Point(0,7)
		);
		p_aDestDirs = new Array(
			new Point(0,-1),
			new Point(-1,0),
			new Point(1,0),
			new Point(0,-1)
		);
		
		// position initiale
		p_ptCurrPos = new Point(6,0);	
		
		p_aDirs = [];
		p_aDirs[0] = new Point(1,0);
		p_aDirs[1] = new Point(0,1);
		p_aDirs[2] = new Point(-1,0);
		p_aDirs[3] = new Point(0,-1);
		
		p_iCurrDirId = 0;
		p_ptCurrDir = p_aDirs[p_iCurrDirId];	
		
		var iIdDest:Number = Math.floor(Math.random()*4);
		p_ptDestPos = p_aDestPoss[iIdDest];
		p_ptDestDir = p_aDestDirs[iIdDest];
		
		p_mcPlan.gotoAndStop(iIdDest+1);
		
		p_ptBackPos = new Point(6,0);
		p_ptBackDir = new Point(1,0);

		initInterface();
		
	}	


	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		p_txtCount = this["txtCount"];

		Application.useLibCursor(p_btUp, "cursor-finger");
		p_btUp.onRelease = function(){
			var ptMove:Point = _parent.move("up", _parent.p_ptCurrPos);
			_parent.tryToMove(ptMove);
		}
		
		Application.useLibCursor(p_btDown, "cursor-finger");
		p_btDown.onRelease = function(){
			var ptMove:Point = _parent.move("down", _parent.p_ptCurrPos);
			_parent.tryToMove(ptMove);
		}
		
		Application.useLibCursor(p_btLeft, "cursor-finger");
		p_btLeft.onRelease = function(){
			_parent.turn("left");
			_parent.render();
		}
		
		Application.useLibCursor(p_btRight, "cursor-finger");
		p_btRight.onRelease = function(){
			_parent.turn("right");
			_parent.render();
		}
		
		Application.useLibCursor(p_mcKey, "cursor-finger");
		p_mcKey.onRelease = function(){
			_parent.p_bGotKey = true;
			_parent.p_mcKey._visible = false;
			_parent.p_mcBulle._visible = true;
			_parent.p_iStepCount = 21;
			_parent.p_txtCount.text = String(_parent.p_iStepCount);
		}
		
		Application.useLibCursor(p_mcSerrure, "cursor-finger");
		p_mcSerrure.onRelease = function(){
			if(_parent.p_bGotKey){
				if(_parent.p_iStepCount <= 21){
					_parent.won();
				}else{
					_parent.loose();
				}
			}
		}
		
		p_mcKey._visible = false;
		p_mcBulle._visible = false;
		
		startPart();
		
	}	
	
	/**
	 * rotation
	 */
	private function turn(sDir:String){
		
		p_mcBulle._visible = false;
		
		switch(sDir){
			case "left":
				p_iCurrDirId++;
				if(p_iCurrDirId>3)p_iCurrDirId=0;
				break;				
			case "right":
				p_iCurrDirId--;
				if(p_iCurrDirId<0)p_iCurrDirId=3;
				break;
		}
		
		p_ptCurrDir = p_aDirs[p_iCurrDirId];
		
	}
	
	/**
	 * essaye un déplacement
	 */
	private function tryToMove(ptMove:Point):Void{
		
		if (p_aMap[ptMove.x][ptMove.y] == 1){
			p_ptCurrPos.x = ptMove.x;
			p_ptCurrPos.y = ptMove.y;
			if(p_bGotKey){
				if(p_iStepCount <= 0){
					loose();
				}else{
					p_iStepCount--;
					p_txtCount.text = String(p_iStepCount);
				}
			}
			render();
		}
		
	}
	
	/**
	 * déplacement
	 * @param sDir direction
	 * @param pt la position en cours
	 */
	private function move(sDir:String, pt:Point):Point{
		
		p_mcBulle._visible = false;
		
		var res:Point = new Point();
		
		switch(sDir){
			case "up":
				res.x = pt.x + p_ptCurrDir.x;
				res.y = pt.y + p_ptCurrDir.y;				
				break;				 
			case "down":
				res.x = pt.x - p_ptCurrDir.x;
				res.y = pt.y - p_ptCurrDir.y;
				break;
		}

		return res;
		
	}
	
	/**
	 * début de la partie
	 */
	private function startPart():Void{
		
		render();
		
	}
	
	/**
	 * affichage de la position en cours
	 */
	private function render():Void{
		
		p_oSoundPlayer.playASound("pas2",0);
		
		var ptView:Point = new Point(p_ptCurrPos.x, p_ptCurrPos.y);
		
		p_mcKey._visible = false;
		p_mcKey.enabled = false;
		p_mcSerrure._visible = false;
		p_mcSerrure.enabled = false;
		
		for(var iPlan:Number=0; iPlan<4; iPlan++){
			
			if(Math.abs(p_ptCurrDir.x) == 1){
				// la gauche et la droite sur l'axe y
				var ptLeft:Point =  new Point(ptView.x, ptView.y + p_ptCurrDir.x);
				var ptRight:Point = new Point(ptView.x, ptView.y - p_ptCurrDir.x);
				var ptUp:Point =    new Point(ptView.x + p_ptCurrDir.x, ptView.y);
			}else{
				// la gauche et la droite sur l'axe x
				var ptLeft:Point =  new Point(ptView.x - p_ptCurrDir.y, ptView.y);
				var ptRight:Point = new Point(ptView.x + p_ptCurrDir.y, ptView.y);
				var ptUp:Point =    new Point(ptView.x, ptView.y + p_ptCurrDir.y);
			}
			
			// mur de gauche
			this["mcPlan"+iPlan]["mcLeft"]._visible  = (p_aMap[ptLeft.x] [ptLeft.y]  == 1);
			// mur de droite
			this["mcPlan"+iPlan]["mcRight"]._visible = (p_aMap[ptRight.x][ptRight.y] == 1);
			// mur en face
			this["mcPlan"+iPlan]["mcUp"]._visible    = (p_aMap[ptUp.x]   [ptUp.y]    != 1);
						
			ptView = move("up", ptView);
			
			// test si il faut afficher la clé en avant plan			
			if (iPlan < 1 && ptView.x == p_ptDestPos.x && ptView.y == p_ptDestPos.y){
				if(!p_bGotKey){
					p_mcKey._visible = true;						
				}
			}
			// test si il faut afficher la serrure en avant plan			
			if (ptView.x == p_ptBackPos.x && ptView.y == p_ptBackPos.y){
				p_mcSerrure._visible = true;	
				p_mcSerrure.gotoAndStop(2);				
			}
			
		}
		
		// test les points clés
		if (p_ptCurrPos.x == p_ptDestPos.x && p_ptCurrPos.y == p_ptDestPos.y){
			if (p_ptCurrDir.x == p_ptDestDir.x && p_ptCurrDir.y == p_ptDestDir.y){
				if(!p_bGotKey){
					p_mcKey._visible = true;
					p_mcKey.enabled = true;
				}
			}
		}
		
		if (p_ptCurrPos.x == p_ptBackPos.x && p_ptCurrPos.y == p_ptBackPos.y){
			if (p_ptCurrDir.x == p_ptBackDir.x && p_ptCurrDir.y == p_ptBackDir.y){
				p_mcSerrure._visible = true;
				p_mcSerrure.gotoAndStop(1);
				p_mcSerrure.enabled = true;
			}
		}
		
	}
	
	
	
	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();
		
		p_oSoundPlayer.playASound("cle1",0);
			
	}
		
	/**
	 * la partie est perdu
	 */
	private function loose(){
		gotoAndStop("loose");		
	}	
	
}

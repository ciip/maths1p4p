﻿/**
 * class com.maths1p4p.level3.game3_09.CubeControler
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

class maths1p4p.level3.game3_09.Cube extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	/** la position actuelle du cube */
	private var p_sCurrPosition:String;
	
	/** le chemin à parcourir */
	private var p_aPath:Array;
	
	/** La position en couras dans le chemin */
	private var p_iCurrPosition:Number;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Cube()	{	
	
		p_sCurrPosition = "center";
		p_aPath = [];
			
	}
	
	/**
	 * Déplace le cube
	 */
	public function moveTo(sDirection:String):Void{
		
		p_aPath = [];
				
		// on revient au centre si besoin
		switch (p_sCurrPosition){
			
			case "left":
				p_aPath = ["before_left", "center"];
				break;
			case "right":
				p_aPath = ["before_right", "center"];
				break;
			case "up":
				p_aPath = ["before_up", "center"];
				break;
			case "down":
				p_aPath = ["before_down", "center"];
				break;
			
		}
		
		// on se déplace si besoin
		switch (sDirection){
			
			case "left":
				p_aPath.push("before_left");
				p_aPath.push("left");
				break;
			case "right":
				p_aPath.push("before_right");
				p_aPath.push("right");
				break;
			case "up":
				p_aPath.push("before_up");
				p_aPath.push("up");
				break;
			case "down":
				p_aPath.push("before_down");
				p_aPath.push("down");
				break;
			
		}
		
		p_sCurrPosition = sDirection;
		
		// on execute les deplacements
		doMove();
		
		
	}
	

	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	/** on execute les déplacements */
	private function doMove(){
		
		if(p_aPath.length > 0){
			
			var sNextFrame = p_aPath.shift();
			gotoAndPlay(sNextFrame);
			
		}
		
	}
	
	
	
}

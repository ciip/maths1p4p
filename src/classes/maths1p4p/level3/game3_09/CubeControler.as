﻿/**
 * class com.maths1p4p.level3.game3_09.CubeControler
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level3.game3_09.CubeControler extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	/** les boutons de commandes*/
	public var p_btLeft, p_btRight, p_btDown, p_btUp, p_btCenter1, p_btCenter2:Button;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function CubeControler()	{	
		
		Application.useLibCursor(p_btLeft, "cursor-finger");
		p_btLeft.onRelease = function(){
			_parent._parent.p_mcCube.moveTo("left");
		}
		
		Application.useLibCursor(p_btRight, "cursor-finger");
		p_btRight.onRelease = function(){
			_parent._parent.p_mcCube.moveTo("right");
		}
		
		Application.useLibCursor(p_btUp, "cursor-finger");
		p_btUp.onRelease = function(){
			_parent._parent.p_mcCube.moveTo("up");
		}
		
		Application.useLibCursor(p_btDown, "cursor-finger");
		p_btDown.onRelease = function(){
			_parent._parent.p_mcCube.moveTo("down");
		}
		
		Application.useLibCursor(p_btCenter1, "cursor-finger");
		Application.useLibCursor(p_btCenter2, "cursor-finger");
		p_btCenter1.onRelease = p_btCenter2.onRelease = function(){
			_parent._parent.p_mcCube.moveTo("center");
		}		
			
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	
	
	
}

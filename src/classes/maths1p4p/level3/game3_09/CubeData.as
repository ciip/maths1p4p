﻿/**
 * class com.maths1p4p.level3.game3_09.CubeData
 * 
 * @author Grégory Lardon
 * @version 0.2
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.utils.OutilsTableaux;

class maths1p4p.level3.game3_09.CubeData extends Array{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	

	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	/** le array qui représente les 6 faces du cube */
	private var p_aCube:Array;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function CubeData()	{	
	
		// liste des couleurs
		p_aCube = ["bleu","vert","marron","jaune","rouge","violet"];

			
	}
	
	/**
	 * renvois le cube
	 */
	public function getCube():Array{
		return p_aCube;
	}
	
	
	/**
	 * renvois le cube
	 */
	public function getTurnedCube(x:Number, y:Number):Array{
		
		var aTurnedCube:Array = OutilsTableaux.getCopie(p_aCube, true);
		
		// rotation en x
		for(var i:Number = 0; i<Math.abs(x); i++){
			aTurnedCube = turnX(aTurnedCube, x>0);
		}
		// rotation en y
		for(var i:Number = 0; i<Math.abs(y); i++){
			aTurnedCube = turnY(aTurnedCube, y>0);
		}
		
		return aTurnedCube;
		
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/** Fait une rotation du cube en X
	* @param aCube le cube
	* @param pIsPositive true si x>0, sinon x<0 
	*/
	private function turnX(aCube:Array, bIsPositive:Boolean):Array{
		
		var aRes:Array = [];
		if(bIsPositive){
			aRes[0] = aCube[3];
			aRes[1] = aCube[0];
			aRes[2] = aCube[1];
			aRes[3] = aCube[2];
			aRes[4] = aCube[4];
			aRes[5] = aCube[5];
		}else{
			aRes[0] = aCube[1];
			aRes[1] = aCube[2];
			aRes[2] = aCube[3];
			aRes[3] = aCube[0];
			aRes[4] = aCube[4];
			aRes[5] = aCube[5];
		}
		
		return aRes;
		
	}
	
	/** Fait une rotation du cube en X
	* @param aCube le cube
	* @param pIsPositive true si x>0, sinon x<0 
	*/
	private function turnY(aCube:Array, bIsPositive:Boolean):Array{
		
		var aRes:Array = [];
		if(bIsPositive){
			aRes[0] = aCube[0];
			aRes[2] = aCube[2];
			aRes[5] = aCube[1];
			aRes[1] = aCube[4];
			aRes[4] = aCube[3];
			aRes[3] = aCube[5];
		}else{
			aRes[0] = aCube[0];
			aRes[2] = aCube[2];
			aRes[5] = aCube[3];
			aRes[1] = aCube[5];
			aRes[4] = aCube[1];
			aRes[3] = aCube[4];
		}
		
		return aRes;
		
	}
	
}

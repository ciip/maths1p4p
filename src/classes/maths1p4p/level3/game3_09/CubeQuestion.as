﻿/**
 * class com.maths1p4p.level3.game3_09.CubeQuestion
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

class maths1p4p.level3.game3_09.CubeQuestion extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	/** les 3 faces affichées */
	public var p_mcFace1, p_mcFace2, p_mcFace5:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	/** la couleur cachée */
	private var p_sColorToFind:String;
	/** la face a trouver */
	private var p_mcColorToFind:MovieClip;
	/** la couleur choisie par le joueur */
	private var p_sSelectedColor:String;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function CubeQuestion()	{	
	
		_visible = false;
			
	}
	
	/**
	 * initialisation
	 * @param aTurnedCube la configuration du cube
	 */
	public function init(aTurnedCube:Array):Void{

		p_mcFace1.gotoAndStop(aTurnedCube[0]);
		p_mcFace2.gotoAndStop(aTurnedCube[1]);
		p_mcFace5.gotoAndStop(aTurnedCube[4]);
		
		var iIdColorToFind:Number = Math.floor(Math.random()*3);
		switch(iIdColorToFind){
			case 0:
				p_sColorToFind = aTurnedCube[0];
				p_mcColorToFind = p_mcFace1;
				break;
			case 1:
				p_sColorToFind = aTurnedCube[1];
				p_mcColorToFind = p_mcFace2;
				break;
			case 2:
				p_sColorToFind = aTurnedCube[4];
				p_mcColorToFind = p_mcFace5;
				break;
		}
		
		p_mcColorToFind.gotoAndStop("question");
		
		_visible = true;

	}
	
	public function selectColor(sColor:String):Void{
		p_mcColorToFind.gotoAndStop(sColor);
		p_sSelectedColor = sColor;
	}
	
	/** vérifie si la réponse est bonne
	* @param sColor la couleur choisi par le joueur
	* @return true si la réponse est bonne, sinon false
	*/
	public function checkResponse():Boolean{
		return (p_sSelectedColor == p_sColorToFind);
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	
	
	
}

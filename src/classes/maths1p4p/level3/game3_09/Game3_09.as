﻿/**
 * class com.maths1p4p.level3.game3_09.Game3_09
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import maths1p4p.level3.game3_09.*
import mx.utils.Delegate;

class maths1p4p.level3.game3_09.Game3_09 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	/** le bouton démarrer, ok */
	public var p_btOk:Button;
	/** le texte sur le bouton ok, démarrer */
	public var p_txtBtOk:TextField;
	
	/** les boutons de réponse */
	public var p_btColor1, p_btColor2, p_btColor3, p_btColor4, p_btColor5, p_btColor6:Button;
	
	/** le clip cache */
	public var p_mcCache:MovieClip;
	
	/** le cube animé */
	public var p_mcCube:MovieClip;
	/** le controler du cube */
	public var p_mcCubeControler:MovieClip;
	/** le cube question */
	public var p_mcQuestion:MovieClip;
	
	/** les petits cubes */
	public var p_mcPetitCube1, p_mcPetitCube2, p_mcPetitCube3, p_mcPetitCube4:MovieClip;
	
	/** le champs affichage des réponses */
	public var p_txtResult:TextField;	

	/** les loots */
	public var p_mcLoots:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	/** le cubeData */
	private var p_oCubeData:CubeData;
		
	/** le nombre de parties à gagner */
	private var p_iNbPartsToWin:Number = 4;
	/** le nombre de parties gagnées*/
	private var p_iNbPartsWon:Number = 0;
	
	/** nombre d'essais */
	private var p_iNbTries:Number;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_09()	{	
		
		super();		
		p_oCubeData = new CubeData();
		p_mcPetitCube1._visible = false;
		p_mcPetitCube2._visible = false;
		p_mcPetitCube3._visible = false;
		p_mcPetitCube4._visible = false;
		p_btColor1._visible = false;
		p_btColor2._visible = false;
		p_btColor3._visible = false;
		p_btColor4._visible = false;
		p_btColor5._visible = false;
		p_btColor6._visible = false;
		
	}
		
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		initInterface();
					
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		p_txtResult.text = "";

		p_txtBtOk.text = "Départ";
		Application.useLibCursor(p_btOk, "cursor-finger");
		p_btOk.onRelease = function(){
			_parent.start();
		}
		
		Application.useLibCursor(p_btColor1, "cursor-finger");
		Application.useLibCursor(p_btColor2, "cursor-finger");
		Application.useLibCursor(p_btColor3, "cursor-finger");
		Application.useLibCursor(p_btColor4, "cursor-finger");
		Application.useLibCursor(p_btColor5, "cursor-finger");
		Application.useLibCursor(p_btColor6, "cursor-finger");
		
		p_btColor1["color"] = "violet";
		p_btColor2["color"] = "bleu";
		p_btColor3["color"] = "jaune";
		p_btColor4["color"] = "vert";
		p_btColor5["color"] = "marron";
		p_btColor6["color"] = "rouge";
		p_btColor1.onRelease = p_btColor2.onRelease = p_btColor3.onRelease = p_btColor4.onRelease = p_btColor5.onRelease = p_btColor6.onRelease = function(){
			_parent.selectColor(this["color"]);
		}

	}	
	
	/**
	 * choix d'une couleur
	 */
	private function selectColor(col:String):Void{
		p_mcQuestion.selectColor(col);
		p_oSoundPlayer.playASound("clic",0);
	}
	
	
	/**
	 * démarrage de la partie
	 */
	private function start(){
		
		p_txtBtOk.text = "Question";
		p_mcCache.gotoAndPlay(2);
		p_btOk.onRelease = function(){			
			_parent.AskNewQuestion();
		}
		
		p_oSoundPlayer.playASound("carrelage4",0);
		
	}
	
	
	
	/**
	 * nouvelle question
	 */
	private function AskNewQuestion(){
		
		p_oSoundPlayer.playASound("clicClacToc",0);
		
		p_btColor1._visible = true;
		p_btColor2._visible = true;
		p_btColor3._visible = true;
		p_btColor4._visible = true;
		p_btColor5._visible = true;
		p_btColor6._visible = true;
		
		p_txtBtOk.text = "J'ai fini";
		p_mcCubeControler._visible = false;
		p_btOk.onRelease = function(){			
			_parent.checkResponse();
		}
		
		// random des rotations en x et y, entre -10 et 10
		var iRotationX:Number = Math.round(Math.random()*20)-9;
		var iRotationY:Number = Math.round(Math.random()*20)-9;
		
		// rotation aléatoire du cube
		var aTurnedCube:Array = p_oCubeData.getTurnedCube(iRotationX,iRotationY);
		// affichage de la question
		p_mcQuestion.init(aTurnedCube);
		
		p_iNbTries = 0;
		
	}
	
	/** 
	* vérifie la réponse du joueur 
	* @param sColor la couleur choisie par le joueur
	*/
	private function checkResponse(){
		
		p_oSoundPlayer.playASound("petitShlac",0);
		
		p_iNbTries++;

		if(p_mcQuestion.checkResponse()){
			
			if(p_iNbTries==1){
				
				p_txtResult.text = "Bravo!\nBonne réponse.";
				p_iNbPartsWon++;
				this["p_mcPetitCube"+p_iNbPartsWon]._visible = true;
				
				if(p_iNbPartsWon == p_iNbPartsToWin){
					won();
				}else{
					AskNewQuestion();
				}
				
			}else{
				
				p_txtResult.text = "Bravo, mais tu dois trouver la réponse du premier coup.";
				AskNewQuestion();
				
			}

		}else{
			p_txtResult.text = "Ce n'est pas la bonne couleur.";
		}
		
	}
	

	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();

	}
	
	
	/**
	 * la partie est perdue
	 * @param msg le message a afficher
	 */
	private function loose(sMsg:String){
		
		gotoAndStop("loose");
		
	}	
	
	
}

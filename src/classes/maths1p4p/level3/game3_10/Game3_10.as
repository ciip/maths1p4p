﻿/**
 * class com.maths1p4p.level3.game3_10.Game3_10
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * 
 * TODO :
 * certains éléments ne sont pas toujours masqués au démarrage
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application; 
import maths1p4p.level3.game3_10.*
import maths1p4p.Maths.Operations.OperationMaker;
import maths1p4p.utils.OutilsTableaux;

class maths1p4p.level3.game3_10.Game3_10 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** les loots */
	public var p_mcLoots:MovieClip;
	
	/** le bouton ok */
	public var p_btOk:Button;
	
	/** masks des noms des joueurs */
	public var p_mcTxtPlayerMask1, p_mcTxtPlayerMask2:MovieClip;
	
	/** les cachces */
	public var p_mcCache1, p_mcCache2:MovieClip;
	
	/** bouton pour empecher les clics sur les tiles */
	public var p_btAntiClic:Button;
	
	/** le clip operation */
	public var p_mcOperation:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** joueurs */
	private var p_oPlayer1, p_oPlayer2;
	
	/** tour actuel, = 0 ou 1 */
	private var p_iCurrTurn:Number;
			
	/** le tableau des slots */
	private var p_aSlots:Array;
	
	/** la tile courante */
	private var p_mcCurrTile:MovieClip;
	
	/** l'operation en cours */
	private var p_oCurrOperation:Object;
	/** l'element de l'opération à trouver */
	private var p_iCurrItemToFind:String;
			
	/** le nombre de parties à gagner */
	private var p_iNbPartsToWin:Number = 2;
	/** le nombre de parties gagnées*/
	private var p_iNbPartsWon:Number = 0;	
	private var p_iNbPlayer2PartsWon:Number = 0;	
	/** nombre d'essais */
	private var p_iNbTries:Number;
	/** nombre d'essais maximum*/
	private var p_iNbMaxTries:Number = 3;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_10()	{		
		super();	
	}	
	
	
	/**
	 * secondPlayerSelected
	 * appellé depuis level3
	 */
	public function secondPlayerSelected(oPlayer2:Object):Void{
		
		p_oPlayer1 = new Object();
		p_oPlayer1.name = _parent._parent.getPlayer().getName();
		p_oPlayer1.id = _parent._parent.getPlayer().getId();
		p_oPlayer2 = oPlayer2;
		
		initInterface();
		
	}
	
	/**
	 * un joueur a gagné
	 */
	public function setVictory(mcMachineWin:MovieClip){
		
		if(mcMachineWin.p_oPlayer.name == p_oPlayer1.name){
			// c'est le premier joueur qui gagne
			won();
		}else{
			// c'est le 2e joueur qui gagne
			
		}
				

	}

	
	/**
	 * partie suivante
	 */
	public function nextPart():Void{
		
		// init des slots
		p_aSlots = new Array(8);
		for(var i=0;i<8;i++){
			p_aSlots[i] = new Array(8);
			for (var j=0; j<8; j++){
				p_aSlots[i][j] = this["mcTile_"+i+"_"+j];
				p_aSlots[i][j]["x"] = i;
				p_aSlots[i][j]["y"] = j;
				Application.useLibCursor(p_aSlots[i][j], "cursor-finger");
				p_aSlots[i][j].onRelease = function(){
					_parent.tileClicked(this);
				}
				p_aSlots[i][j].gotoAndStop(1);
				p_aSlots[i][j].enabled = true;
			}
		}			
		
		// premier tour au hazard
		p_iCurrTurn = Math.floor( Math.random()*2 );			
		nextTurn();				
		
	}
	
	/**
	 * appuis sur ENTER
	 */
	public function onKeyDown(){
		
		if(Key.getCode() == Key.ENTER){
			checkCurrResult();
		}
		
	}
	
	
	/**
	 * checkCurrResult
	 * check la réponse du joueur
	 */
	public function checkCurrResult(){
		
		p_oSoundPlayer.playASound("clic",0);
		
		if(isNaN(p_mcOperation.getResponse())){
			return;
		}
		
		p_btOk.enabled = false;
		Key.removeListener(this);
		p_mcOperation.hide();
		
		if(p_mcOperation.getResponse() == p_oCurrOperation[p_iCurrItemToFind]){
			// bonne réponse
			p_mcCurrTile.enabled = false;
			if (checkEnd(p_iCurrTurn)){
				// partie gagnée
				if(p_iCurrTurn == 0){
					// player 1
					p_iNbPartsWon++;
					this["p_mcPlayer1Couronne"+p_iNbPartsWon]._visible = true;
					if(p_iNbPartsWon == p_iNbPartsToWin){
						won();
					}else{
						nextPart();
					}
				}else{
					p_iNbPlayer2PartsWon++;
					this["p_mcPlayer2Couronne"+p_iNbPartsWon]._visible = true;
					if(p_iNbPlayer2PartsWon == p_iNbPartsToWin){
						loose();
					}
				}
				
				nextPart();
			}else{
				nextTurn();
			}
			
			p_oSoundPlayer.playASound("tagadaClac",0);

		}else{
			// mauvaise réponse
			p_oSoundPlayer.playASound("brBrBrSmalc",0);
			p_mcCurrTile.gotoAndStop(1);
			p_mcCurrTile.p_mcAnimation.gotoAndPlay(2);
			nextTurn();
		}			
		
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		// affichage de l'écran de sélection du joueur 2
		if(_parent._parent != null){
			_parent._parent.showSecondPlayerSelectionScreen();
		}else{
			// mode autonome
			p_oPlayer1 = new Object();
			p_oPlayer1.name = "joueur 1";
			p_oPlayer1.id = 1;
			
			p_oPlayer2 = new Object();
			p_oPlayer2.name = "joueur 2";
			p_oPlayer2.id = 2;
			
			// init de l'interface
			initInterface();
			
		}
		
	}	


	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		this["p_txtPlayer1"].text = p_oPlayer1.name;
		this["p_txtPlayer2"].text = p_oPlayer2.name;
		
		this["p_mcPlayer1Couronne1"]._visible = false;
		this["p_mcPlayer1Couronne2"]._visible = false;
		this["p_mcPlayer2Couronne1"]._visible = false;
		this["p_mcPlayer2Couronne2"]._visible = false;
		
		p_mcCache1._alpha = 100;
		p_mcCache2._alpha = 100;
		
		Application.useLibCursor(p_btOk, "cursor-finger");
		p_btOk.onRelease = function(){
			_parent.checkCurrResult();
		}
				
		p_btOk.enabled = false;
		
		p_btAntiClic.useHandCursor = false;
		p_btAntiClic.onRelease = function(){};
		p_btAntiClic._visible = false;
		
		

		// nouvelle partie
		nextPart();
		
	}	
	
	/**
	 * tileClicked
	 * @param la tile cliquée
	 */
	private function tileClicked(mcTile:MovieClip):Void{
		
		p_oSoundPlayer.playASound("clic",0);
		
		p_mcCurrTile = mcTile;
		
		if(p_iCurrTurn == 0){
			// player 1
			p_mcCurrTile.gotoAndStop(2);
		}else{
			// player 2
			p_mcCurrTile.gotoAndStop(3);
		}
		
		newQuestion();
		p_btAntiClic._visible = true;
		p_btOk.enabled = true;
		Key.addListener(this);
		
	}
	
	/**
	 * newQuestion
	 */
	private function newQuestion():Void{
		
		// addition ou soustraction ?
		var iTypeOperation:Number = Math.floor(Math.random()*2);		
		if(iTypeOperation == 0){
			// cree une addition
			p_oCurrOperation = OperationMaker.getAddition(5, 90);
		}else{
			// cree une soustraction
			p_oCurrOperation = OperationMaker.getSoustraction(5, 90);
		}
		
		//p_oCurrOperation = OperationMaker.getSoustraction(5, 90);
		// choix de l'éléments de l'opération à trouver
		var iItemToFind:Number = Math.floor(Math.random()*3);
		p_iCurrItemToFind = ["A","B","R"][iItemToFind];
		
		p_mcOperation.init(p_oCurrOperation, p_iCurrItemToFind);
		
	}
	
	/**
	 * nextTurn
	 * changement de tour
	 */
	private function nextTurn():Void{
		
		// changement de tour
		this["p_mcCache"+(p_iCurrTurn+1)]._alpha = 60;		
				
		if(p_iCurrTurn==0){
			p_iCurrTurn = 1;
		}else{
			p_iCurrTurn = 0;
		}		
		this["p_mcCache"+(p_iCurrTurn+1)]._alpha = 0;
		
		p_btAntiClic._visible = false;
		
	}
	
	/**
	 * checkEnd
	 * test si la partie est gagnée par le joueur en cours
	 */
	private function checkEnd(iTurn:Number):Boolean{
		
		switch(iTurn){
			
			case 0:
				// de gauche à droite
				for(var j=0; j<8; j++){
					var aTilesWon:Array = [];
					var iCurrFrame = 2;
					var aPath:Array = buildPath(0,j,aTilesWon,iCurrFrame);
					// vérifie si le path est gagant
					for(var k in aPath){
						if(aPath[k].charAt(0) == 7){
							// gagné
							return true;
						}
					}
					
				}
			
				break;
				
			case 1:
				// de haut en bas
				for(var i:Number=0; i<8; i++){
					var aTilesWon:Array = [];
					var iCurrFrame = 3;
					var aPath:Array = buildPath(i,0,aTilesWon,iCurrFrame); 
					// vérifie si le path est gagant
					for(var k in aPath){
						if(aPath[k].charAt(2) == 7){
							// gagné
							loose();
						}
					}
					
				}
				break;
			
		}
		
		return false;
		
	}
	

	
	/**
	 * buildPath
	 * @param i : i
	 * @param j : j
	 * @param aTilesWon array contenant le path, incrémenté
	 * @param iCurrFrame Int correspondant à la frame 2 ou 3, selon le joueur en cours de test
	 */
	private function buildPath(i:Number,j:Number,aTilesWon:Array,iCurrFrame:Number):Array{
		
		// on test si la tile demandé, [i,j] est gagnée ou pas
		if(p_aSlots[i][j]._currentframe == iCurrFrame){
			// on l'ajoute au path
			aTilesWon.push(i+"_"+j);

			// on test les 6 tiles avoisinantes, à condition qu'elles ne soient pas déjà dans le path
			
			// 0, -1
			if (!isInArray(aTilesWon, i+"_"+String(j-1)) ){
				aTilesWon = buildPath(i, j-1, aTilesWon, iCurrFrame);
			}		
			
			// -1,0
			if (!isInArray(aTilesWon, String(i-1)+"_"+String(j))){
				aTilesWon = buildPath(i-1, j, aTilesWon, iCurrFrame);
			}	
			
			// -1,+1
			if (!isInArray(aTilesWon, String(i-1)+"_"+String(j+1))){
				aTilesWon = buildPath(i-1, j+1, aTilesWon, iCurrFrame);
			}
			
			// +1,-1
			if (!isInArray(aTilesWon, String(i+1)+"_"+String(j-1))){
				aTilesWon = buildPath(i+1, j-1, aTilesWon, iCurrFrame);
			}
			
			// +1,0
			if (!isInArray(aTilesWon, String(i+1)+"_"+String(j))){
				aTilesWon = buildPath(i+1, j, aTilesWon, iCurrFrame);
			}
			
			// 0,+1
			if (!isInArray(aTilesWon, String(i)+"_"+String(j+1))){
				aTilesWon = buildPath(i, j+1, aTilesWon, iCurrFrame);
			}

		}
		
		// on retourne le path
		return aTilesWon;
		
	}
	
	/**
	 * check une valeur dans un array
	 */
	private function isInArray(arr:Array, val):Boolean{
		for(var i=0; i<arr.length; i++){
			if (arr[i] == val){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();
		
		p_oSoundPlayer.playASound("ClangTic",0);
		
	}
	
	/**
	 * la partie est gagnée, par l'adversaire
	 */
	private function loose(){

		gotoAndStop("win");
		
	}
}

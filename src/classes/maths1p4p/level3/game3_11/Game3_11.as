﻿/**
 * class com.maths1p4p.level3.game3-11.Game3_11
 * 
 * @author Grégory Lardon
 * @version 0.2
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * TODO :
 * integrer la gestion des sons
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import maths1p4p.level3.game3_11.*
import flash.geom.Point;


class maths1p4p.level3.game3_11.Game3_11 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** le texte d'affichage des commandes */
	public var p_txtCommands:TextField;
	
	/** les différents boutons de l'interface de commande */
	public var p_btLeft, p_btRight, p_btBack, p_btAhead:Button;
	public var p_btC, p_btCE:Button;
	public var p_btExecute:Button;

	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** la grille du jeu */
	private var p_aMap:Array;
	/** position courante dans la map */
	private var p_aCurrLoc:Array;
	/** arrivée */
	private var p_aEndLoc:Array;
	/** les directions */
	private var p_aDirections:Array;
	/** id de la direction en cours */
	private var p_iCurrDirection:Number;
	/** rotation courante dans la map */
	private var p_iCurrRotation:Number;
	
	/** écarts entre les points */
	private var p_iPtXOffset, p_iPtYOffset:Number;
	/** position du point de référence 0,0 */
	private var p_iInitPointLoc:Point;
	
	/** le movieclip qui va contenir le parcours de la fourmi */
	private var p_mcRoad:MovieClip;
		
	/** le nombre de parties à gagner */
	private var p_iNbPartsToWin:Number = 1;
	/** le nombre de parties gagnées*/
	private var p_iNbPartsWon:Number = 0;
	/** le nombre d'essais à ne pas dépasser */
	private var p_iNbMaxTries:Number = 6;
	/** le nombre d'essais actuels du joueur*/
	private var p_iCurrNbTries:Number = 0;
	
	/** la commande en cours de saisie */
	private var p_aCommands:Array;
	
	/** la fourmi en cours d'animation */
	private var p_mcCurrSprite:MovieClip;
	/** le nombre de sprites crée */
	private var p_iSpritesCount:Number;
	
	/** true si les actions utilisateurs sont interprétées */
	private var p_bActionOk:Boolean;
	
	/** nombre de steps sur le trajet de la fourmie */
	private var p_iGoSteps:Number;
	/** step en cours */
	private var p_iCurrStep:Number;
	/** commande en cours */
	private var p_oCommand:Object;
	
	/** sauvegardes des commandes en texte */
	private var p_sLastCommands:String;
	
	/** les loots */
	public var p_mcLoots:MovieClip;


	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_11()	{		
		super();				
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){		
		
		// initialisation des propriétés
		
		p_sLastCommands = "";
		
		// map
		p_aMap = new Array();
		p_aMap[0]  = [0,0,0,0,1,1,1,0,1,0,1,0,1];
		p_aMap[1]  = [0,0,0,0,1,0,1,0,1,0,1,0,1];
		p_aMap[2]  = [1,1,1,1,1,1,1,0,1,1,1,1,1];
		p_aMap[3]  = [0,0,0,0,1,0,1,0,1,0,0,0,1];
		p_aMap[4]  = [0,0,1,1,1,1,1,0,1,0,1,0,1];
		p_aMap[5]  = [0,0,0,0,1,0,1,0,1,0,1,0,0];
		p_aMap[6]  = [0,0,1,1,1,0,1,0,1,1,1,1,1];
		p_aMap[7]  = [0,0,1,0,0,0,0,0,0,0,1,0,1];
		p_aMap[8]  = [0,0,1,1,1,1,1,0,1,0,1,1,1];
		p_aMap[9]  = [0,0,0,0,1,0,1,0,1,0,0,0,1];
		p_aMap[10] = [0,0,0,0,1,1,1,1,1,1,1,1,1];
		
		// position initiale
		p_aCurrLoc = [2,0];

		// direction initiale
		p_aDirections = ["DROITE","BAS","GAUCHE","HAUT"];
		p_iCurrDirection = 0;
		
		p_iCurrRotation = 0;
		
		// positions des points
		p_iInitPointLoc = new Point(43,207);
		p_iPtXOffset = 17.5;
		p_iPtYOffset = 17.5;
		
		// init du parcours
		p_mcRoad = createEmptyMovieClip("mcRoad", 5);
		p_iSpritesCount = 1;
		p_mcCurrSprite = createSpriteAt(p_iSpritesCount, p_aCurrLoc);
				
		// init de la commande
		p_aCommands = new Array();	
		
		/** arrivée */
		p_aEndLoc = [4,12];
		
		// initialisation de l'interface
		initInterface();
				
		// lancement de la partie
		p_bActionOk = true;
		
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		
		
		p_txtCommands.text = "";		
		
		// boutons de direction		
		Application.useLibCursor(p_btLeft, "cursor-finger");
		Application.useLibCursor(p_btRight, "cursor-finger");
		p_btLeft["value"] = "GAU";
		p_btRight["value"] = "DR";
		p_btLeft.onRelease = p_btRight.onRelease = function(){
			_parent.addCommand("direction", this["value"]);
		}
		// boutons de sens
		Application.useLibCursor(p_btAhead, "cursor-finger");
		Application.useLibCursor(p_btBack, "cursor-finger");
		p_btAhead["value"] = "AV";
		p_btBack["value"] = "RE";
		p_btAhead.onRelease = p_btBack.onRelease = function(){
			_parent.addCommand("sens", this["value"]);
		}
		
		// boutons chiffrés
		for(var i:Number=1; i<7; i++){
			Application.useLibCursor(this["p_btNum"+i], "cursor-finger");
			this["p_btNum"+i]["value"] = i;
			this["p_btNum"+i].onRelease = function(){
				_parent.addCommand("go", this["value"]);
			}
		}
		
		// boutons C et CE
		Application.useLibCursor(p_btC, "cursor-finger");
		p_btC.onRelease = function(){
			_parent.doC();
		}
		Application.useLibCursor(p_btCE, "cursor-finger");
		p_btCE.onRelease = function(){
			_parent.doCE();
		}
		
		// bouton execute
		Application.useLibCursor(p_btExecute, "cursor-finger");
		p_btExecute.onRelease = function(){
			_parent.executeCommands();
		}
		
		// bouton essais cachés
		for(var i:Number=0; i<6; i++){
			this["p_mcTrie"+i]._visible = false;
		}

		
	}	
	
	
	/**
	 * démarrage d'une partie
	 */
	private function startNewPart(){
		

		
	}
	
	/**
	* Ajoute une commande
	* @param type "direction" ou "go"
	* @param val valeur : string ou number
	*/
	private function addCommand(type:String, val){
		
		p_oSoundPlayer.playASound("clic",0);
		
		if(p_bActionOk){
	
			// on va comparer à la dernière commande
			var oLastCommand:Object = p_aCommands[p_aCommands.length-1];		
			var oNewCommand:Object = new Object();
			
			if(oLastCommand != null){
			
				switch (type){
					
					case "direction":
						if(oLastCommand.type != "sens"){
							oNewCommand.type = type;
							oNewCommand.value = val;
							p_aCommands.push(oNewCommand);
						}
						break;
						
					case "sens":
						if(oLastCommand.type == "sens"){
							// remplace le sens différent
							p_aCommands.pop();
						}
						oNewCommand.type = type;
						oNewCommand.value = val;
						p_aCommands.push(oNewCommand);
						break;
						
					case "go":
						// seulement après une direction
						if(oLastCommand.type == "sens"){
							oNewCommand.type = type;
							oNewCommand.value = val;
							p_aCommands.push(oNewCommand);
						}
						break;				
									
				}
				
			}else{
				
				// il s'agit de la première commande, 
				// donc pas de comparaison avec la précédente
				if(type != "go"){
					oNewCommand.type = type;
					oNewCommand.value = val;
					p_aCommands.push(oNewCommand);			
				}
				
			}
			
			displayCommand(p_aCommands);
			
		}
		
	}
	
	/**
	* action du bouton C
	*/
	private function doC(){	
		
		p_oSoundPlayer.playASound("clic",0);
		
		if(p_bActionOk){
			
			p_aCommands.pop();
			displayCommand(p_aCommands);
			
		}
		
	}
	
	/**
	* action du bouton CE
	*/
	private function doCE(){	
		
		p_oSoundPlayer.playASound("clic",0);
		
		if(p_bActionOk){
			p_aCommands = [];
			displayCommand(p_aCommands);
		}
		
	}
	
	/**
	* execute la liste des commandes
	*/
	private function executeCommands(){		
		
		p_oSoundPlayer.playASound("miniClick",0);
		
		if(p_bActionOk){
			
			// vérifie que la dernière commande n'est pas un sens
			if(p_aCommands[p_aCommands.length-1].type != "sens"){				
				p_sLastCommands = p_txtCommands.text;						
				this["p_mcTrie"+p_iCurrNbTries]._visible = true;
				p_iCurrNbTries ++;			
				executeOneCommand();
			}
		}
		
	}			
	
	
	/**
	* execute une commande
	*/
	private function executeOneCommand(){	

		if(p_aCommands.length > 0){
		
			// prend la première commande de la liste
			p_oCommand = p_aCommands.shift();
			if(p_oCommand.type == "direction"){
				
				// COMMANDE DIRECTION
				
				if(p_oCommand.value == "DR"){
					
					// modification de la direction en cours
					p_iCurrDirection ++;
					if(p_iCurrDirection == p_aDirections.length){
						p_iCurrDirection = 0;
					}
					// rotation du clip
					if(p_iCurrRotation<270){
						p_iCurrRotation += 90;
					}else{
						p_iCurrRotation = 0;
					}

				}else{
					
					// modification de la direction en cours
					p_iCurrDirection --;
					if(p_iCurrDirection <0){
						p_iCurrDirection = p_aDirections.length - 1;
					}
					// rotation du clip
					if(p_iCurrRotation == -180){
						p_iCurrRotation = 180;
						p_mcCurrSprite._rotation = 180;
					}else if(p_iCurrRotation == 180){
						p_iCurrRotation = -180;
						p_mcCurrSprite._rotation = -180;
					}					
					p_iCurrRotation -= 90;

				}

				p_mcRoad[p_mcCurrSprite._name].rotateTo(p_iCurrRotation,0.5,"easeInOutCubic", 0, tweenEnd);


			}else if(p_oCommand.type == "sens"){
				
				// COMMANDE AVANCER
				// prend la commande suivante pour la distance
				var oCommand2:Object = p_aCommands.shift();
				
				p_iGoSteps = oCommand2.value*2;
				p_iCurrStep = 0;
				doSlide();
								
			}else{
				
				trace("ne devrait jamais arriver");
				
			}
			
		}else{
			
			if(p_iCurrNbTries == p_iNbMaxTries){
				loose("Tu n'as plus d'essai. Recommence...");
			}
			
		}
		
	}
	
	private function doSlide():Void{
		
		// test si on est arrivé
		if (p_aEndLoc[0] == p_aCurrLoc[0] && p_aEndLoc[1] == p_aCurrLoc[1]){
			
			if(p_iCurrNbTries <= 3){
				p_bPalmeOr = true;
			}
			won();
		
		}else{		
			
			if(p_iCurrStep< p_iGoSteps){			
				
				if(p_oCommand.value == "AV"){
					var vSens = 1
				}else{
					var vSens = -1;
				}

				
				if(p_iCurrStep % 2 == 0){
					
					// 1er passage
					
					var nextLoc:Array = new Array();
					nextLoc[0] = p_aCurrLoc[0];
					nextLoc[1] = p_aCurrLoc[1];
					switch (p_aDirections[p_iCurrDirection]){
						case "DROITE":
							nextLoc[1] += vSens;
							break;
						case "GAUCHE":
							nextLoc[1] -= vSens;
							break;
						case "HAUT":
							nextLoc[0] += vSens;
							break;
						case "BAS":
							nextLoc[0] -= vSens;
							break;
					}
				
					if(p_aMap[nextLoc[0]][nextLoc[1]] == 1){
						// le chemin est libre
						p_iCurrStep++;
						doSlide();
					}else{				
						loose("Recommence...");								
						var mcExplosion:MovieClip = p_mcRoad.attachMovie("mcExplosion", "mcExplosion", p_mcRoad.getNextHighestDepth());
						mcExplosion._x = p_mcCurrSprite._x;
						mcExplosion._y = p_mcCurrSprite._y;
						mcExplosion._rotation = p_mcCurrSprite._rotation;
						if(vSens == -1)mcExplosion._rotation += 180;
					}
				
				}else{			
				
					// 2e passage on fait le slide
					// création d'un nouveau sprite					
					p_iSpritesCount++;
					p_mcCurrSprite = createSpriteAt(p_iSpritesCount, p_aCurrLoc);
					p_mcCurrSprite._rotation = p_iCurrRotation;					
			
					switch (p_aDirections[p_iCurrDirection]){
						case "DROITE":
							p_aCurrLoc[1] += vSens*2;
							break;
						case "GAUCHE":
							p_aCurrLoc[1] -= vSens*2;
							break;
						case "HAUT":
							p_aCurrLoc[0] += vSens*2;
							break;
						case "BAS":
							p_aCurrLoc[0] -= vSens*2;
							break;
					}
					
					var nextPt:Point = getPixCoords(p_aCurrLoc);
					
					p_iCurrStep++;
									
					p_mcRoad[p_mcCurrSprite._name].xSlideTo(nextPt.x, 0.5, "easeInOutCubic");
					p_mcRoad[p_mcCurrSprite._name].ySlideTo(nextPt.y, 0.5, "easeInOutCubic", 0, reDoSlide);
					
				}			
								
			}else{
				executeOneCommand();
			}
			
		}
		
	}
	
	private function reDoSlide():Void{
		
		// ici, this est p_mcCurrSprite
		_parent._parent.doSlide();

	}
	
	/**
	* appellé à la fin d'une commande
	*/
	private function tweenEnd(){
		
		// ici, this est p_mcCurrSprite
		_parent._parent.executeOneCommand();
		
	}

	
	/**
	* Création d'un sprite dans le jeu, aux coordonnées...
	*/
	private function createSpriteAt(count:Number, loc:Array):MovieClip{

		var vMcSprite:MovieClip = p_mcRoad.attachMovie("mcSprite", "sprite_"+p_iSpritesCount, p_iSpritesCount);
		var pt:Point = getPixCoords(loc);
		vMcSprite._x = pt.x;
		vMcSprite._y = pt.y;
		return vMcSprite;
		
	}
	
	
	/** renvois les coordonnées en pixels dans la map */
	private function getPixCoords(loc:Array){
		var res:Point = new Point();
		res.x = p_iInitPointLoc.x + loc[1] * p_iPtXOffset;
		res.y = p_iInitPointLoc.y - loc[0] * p_iPtYOffset;
		return res;
	}


	/**
	* Affichage de la commande dans la zone de texte
	*/
	private function displayCommand(aCommand:Array){
		
		var txtCommand:String = "";
		for (var i:Number=0; i<aCommand.length; i++){
			txtCommand += String(aCommand[i].value);
			if(aCommand[i].type != "sens"){
				txtCommand += ", ";
			}
		}

		p_txtCommands.text = p_sLastCommands + txtCommand + "\n";
		
	}
	
	
	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();
		
	}
	
	
	/**
	 * la partie est perdue
	 * @param msg le message a afficher
	 */
	private function loose(sMsg:String){
		
		p_oSoundPlayer.playASound("bicle2",0);
		
		p_txtCommands.text = sMsg;
		gotoAndStop("loose");
		
	}	
	
	
}

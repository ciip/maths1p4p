﻿/**
 * class com.maths1p4p.level3.game3_12.Game3_12
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import maths1p4p.Maths.Operations.OperationMaker;

class maths1p4p.level3.game3_12.Game3_12 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** les loots */
	public var p_mcLoots:MovieClip;
	
	/** image des flips */
	public var p_mcFlips:MovieClip;
	
	/** boutons des flips */
	public var p_btLeft, p_btRight;
	
	/** bouton suite/départ */
	public var p_btDepartSuite
	
	/** champs texte opération */
	public var p_txtOperation:TextField;
	
	/** champs départ/suite */
	private var p_txtDepartSuite:TextField;
	
	/** champs texte des erreurs */
	private var p_txtErrors:TextField;

	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** liste des opérations */
	private var p_aOperations:Array;
	private var p_iCurrOperation:Number;
	
	/** flip ouvert "left","right"*/
	private var p_sCurrFlip:String
		
	
	/** colonne de gauche */
	private var p_iLeftXAlign:Number = 85;
	
	/** colonne de droite */
	private var p_iRightXAlign:Number = 417;
	
	/** largeur et hauteur des textfields */
	private var p_iTxtWidth:Number = 135;
	private var p_iTxtHeight:Number = 20;
	
	/** y de l'eau */
	private var p_iWaterY:Number = 318;
	
	/** true si un des boutons est enfoncé */
	private var p_bFlipDown:Boolean;
	
	/** nombre de réponses passées */
	private var p_iAnswersCount:Number;
	
	/** nombre d'erreurs */
	private var p_iErrorsCount:Number = 0;
	
	private var p_iLeftCount, p_iRightCount:Number;	
	private var p_aLeftColumn, p_aRightColumn:Array;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_12()	{		
		super();	
	}	
	
		
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		p_txtOperation = this["txtOperation"];
		p_txtDepartSuite = this["txtDepartSuite"];
		p_txtErrors = this["txtErrors"];
		
		p_iAnswersCount = 0;	
		p_iLeftCount = 0;
		p_iRightCount = 0;		
		
		p_aLeftColumn = [];
		p_aRightColumn = [];
		
		// random des opérations
		p_iCurrOperation = -1;
		p_aOperations = new Array();
		for(var i=0; i<15; i++){
			
			// addition ? soustraction ? multiplication ?
			var iOperationType:Number = Math.floor(Math.random()*3);
			switch (iOperationType){				
				case 0:
					var oOperation:Object = OperationMaker.getAddition(1,400);
					break;
				case 1:
					var oOperation:Object = OperationMaker.getSoustraction(1,400);
					break;
				case 2:
					var oOperation:Object = OperationMaker.getMultiplication(10,150,1,6);
					break;
			}
			
			p_aOperations.push(oOperation);
			
		}
		
		initInterface();
		
	}	


	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		// bouton depart
		Application.useLibCursor(p_btDepartSuite, "cursor-finger");
		p_btDepartSuite.onRelease = function(){			
			_parent.p_txtDepartSuite.text = "Suite";
			_parent.nextTurn();
			this.enabled = false;	
		}
		
		// boutons flips
		p_btLeft.enabled = false;
		Application.useLibCursor(p_btLeft, "cursor-finger");
		p_btLeft.onPress = function(){
			_parent.flip("left");
			_parent.p_bFlipDown = true;
		}
		p_btRight.enabled = false;
		Application.useLibCursor(p_btRight, "cursor-finger");
		p_btRight.onPress = function(){
			_parent.flip("right");
			_parent.p_bFlipDown = true;
		}	
		
		p_btLeft.onRelease = p_btRight.onRelease = p_btLeft.onReleaseOutside = p_btRight.onReleaseOutside = function(){
			_parent.p_bFlipDown = false;
		}
		
		// txtfields ...
		p_txtOperation.text = "";
		p_txtErrors.text = "";
		var txtForm:TextFormat = new TextFormat();
		txtForm.size = 14;
		txtForm.bold = true;
		p_txtOperation.setTextFormat(txtForm);		
		p_txtErrors.setTextFormat(txtForm);		
		
	}	
	
	/**
	 * Nouvelle question
	 */
	private function nextTurn():Void{
		
		p_oSoundPlayer.playASound("clic",0);
		
		p_btLeft.enabled = true;
		p_btRight.enabled = true;
		
		p_sCurrFlip = "";
		p_mcFlips.gotoAndStop(1);
		
		p_iCurrOperation++;
		var oOperation:Object = p_aOperations[p_iCurrOperation];
		
		p_txtOperation._visible = true;
		p_txtOperation._y = 46;
		p_txtOperation.text = oOperation.A + " " + oOperation.operator + " " + oOperation.B;

		this.onEnterFrame = this.loop;

	}
	
	/**
	 * loop
	 */
	private function loop():Void{
		
		// l'operation descend
		p_txtOperation._y += 2;
		// accélération si un bouton est cliqué 
		if(p_bFlipDown) p_txtOperation._y += 2;
		
		var oOperation:Object = p_aOperations[p_iCurrOperation];
		
		if(p_txtOperation._y >= p_iWaterY){
			
			// le joeur n'a pas fait de choix, tombe à l'eau	
			addOperationToErrors(oOperation);			
			delete this.onEnterFrame;
			
		}else if(p_txtOperation._y >= p_iWaterY-40){
			if(p_sCurrFlip != ""){
				// rangement de l'operation à gauche ou droite
				if (oOperation.column != ""){
					addOperationToColumn(oOperation.column, oOperation);
					delete this.onEnterFrame;
				}
			}
			
		}
		

		
	}
	
	/**
	 * flip
	 * flip à gauche ou droite
	 */
	private function flip(sDir:String):Void{
		
		p_oSoundPlayer.playASound("cle2",0);
		
		p_aOperations[p_iCurrOperation].column = sDir;
		p_sCurrFlip = sDir;
		
		if(sDir == "left"){
			p_mcFlips.gotoAndStop(2);
		}else{
			p_mcFlips.gotoAndStop(3);
		}
		
	}
	
	/**
	 * addOperationToColumn
	 * ajoute une operation dans une colonne
	 * @param sColumn left ou right
	 * @param oOperation l'object operation
	 */
	private function addOperationToColumn(sColumn:String, oOperation:Object):Void{
		
		if (sColumn == "left"){
			p_iLeftCount ++;
			var yCount:Number = p_iLeftCount;
			var iXAlign = p_iLeftXAlign;
			p_aLeftColumn.push(oOperation);
		}else{
			p_iRightCount ++;
			var yCount:Number = p_iRightCount;
			var iXAlign = p_iRightXAlign;
			p_aRightColumn.push(oOperation);
		}
		
		p_iAnswersCount++;
		
		createTextField("txtOperation_"+p_iAnswersCount, getNextHighestDepth(), iXAlign, 308-18*yCount,p_iTxtWidth, p_iTxtHeight);
		var txtField:TextField = this["txtOperation_"+p_iAnswersCount];
		txtField.text += oOperation.A + " " + oOperation.operator + " " + oOperation.B;
		txtField.selectable = false;
		
		oOperation.txtField = txtField;
	
		var txtForm:TextFormat = new TextFormat();
		txtForm.align = "center";
		txtForm.font = "arial";
		txtForm.size = 13;
		txtForm.bold = true;
		txtField.setTextFormat(txtForm);
		
		p_txtOperation._visible = false;
		if(p_iCurrOperation < p_aOperations.length-1){		
			p_btDepartSuite.enabled = true;
		}else{
			// partie terminée
			checkEnd();
		}
	}
	
	/**
	 * addOperationToErrors
	 * ajoute une operation aux erreurs
	 * @param sColumn left ou right
	 * @param oOperation l'object operation
	 */
	private function addOperationToErrors(oOperation:Object):Void{
		
		p_iErrorsCount ++;
		
		p_txtErrors.text += oOperation.A + " " + oOperation.operator + " " + oOperation.B + " / ";
		p_txtOperation._visible = false;
		if(p_iCurrOperation < p_aOperations.length-1){		
			p_btDepartSuite.enabled = true;
		}else{
			// partie terminée
			checkEnd();
		}
		
	}
	
	/**
	 * checkEnd
	 * test si la partie est gagnée par le joueur en cours
	 */
	private function checkEnd(iTurn:Number){		

		for(var i=0; i<p_aLeftColumn.length; i++){
			p_aLeftColumn[i].txtField.background = true;
			if(p_aLeftColumn[i].R < 400){				
				p_aLeftColumn[i].txtField.backgroundColor = 0x00FF00;
			}else{
				p_aLeftColumn[i].txtField.backgroundColor = 0xFF0000;
				p_iErrorsCount++;
			}
		}

		for(var i=0; i<p_aRightColumn.length; i++){
			p_aRightColumn[i].txtField.background = true;
			if(p_aRightColumn[i].R > 400){
				p_aRightColumn[i].txtField.backgroundColor = 0x00FF00;
			}else{
				p_aRightColumn[i].txtField.backgroundColor = 0xFF0000;
				p_iErrorsCount++;
			}
		}	
		
		if (p_iErrorsCount > 3){
			// perdu
			loose();
		}else{
			// gagné
			if(p_iErrorsCount<=1){
				p_bPalmeOr = true;
			}
			won();
		}
		
	}


	
	
	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();
		
		p_oSoundPlayer.playASound("carrelage3",0);
				
	}
	
	/**
	 * la partie est gagnée, par l'adversaire
	 */
	private function loose(){

		gotoAndStop("win");
		
	}
}

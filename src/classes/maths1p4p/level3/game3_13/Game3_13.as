﻿/**
 * class com.maths1p4p.level3.game3_13.Game3_13
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import com.prossel.utils.CursorManager;
import maths1p4p.level3.game3_13.*

class maths1p4p.level3.game3_13.Game3_13 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** les loots */
	public var p_mcLoots:MovieClip;
	
	/** le bouton ok */
	public var p_btDepart:Button;
	
	/** boutons manettes */
	public var p_btManette1, p_btManette2:Button;
	
	/** les drapeaux */
	public var p_mcFlags:MovieClip;
	
	/** l'affichage de la solution */
	public var p_mcSolution:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** liste des puces */
	private var p_aPuces:Array;

	/** tableau des tiles */
	private var p_aTiles:Array;
	
	/** liste des cercles */
	private var p_aCircles:Array;
	
	/** liste des carrés */
	private var p_aSquares:Array;
	
	/** liste des couleurs */
	private var p_aColors:Array;
	
	/** le tableau reponse */
	private var p_aAnswer:Array;
	
	/** rotation du tableau en cours */
	private var p_iCurrRotation:Number;
			
	/** nombre d'essais */
	private var p_iNbTries:Number;
	/** nombre d'essais maximum*/
	private var p_iNbMaxTries:Number = 5;
	/** le nombre de parties agner */
	private var p_iNbPartsToWin:Number = 2;
	/** le nombre de parties gagn*/
	private var p_iNbPartsWon:Number = 0;	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_13()	{		
		super();	
	}	
	
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		p_iNbTries = 0;
		
		/** liste des puces */
		p_aPuces = [this["mcPuce0"], this["mcPuce1"], this["mcPuce2"], this["mcPuce3"], this["mcPuce4"]];
		
		/** tableau des tiles */
		p_aTiles = new Array();
		for(var i=0; i<5; i++){
			p_aTiles.push(new Array());
			for(var j=0; j<5; j++){
				p_aTiles[i][j] = this["mcTile_" + i + "_" + j];
				p_aTiles[i][j]["i"] = i;
				p_aTiles[i][j]["j"] = j;
			}
		}
				
		/** liste des cercles */
		p_aCircles = [this["mcCircle0"], this["mcCircle1"], this["mcCircle2"], this["mcCircle3"], this["mcCircle4"]];			
		
		/** liste des carrés */
		p_aSquares = [this["mcSquare0"], this["mcSquare1"], this["mcSquare2"], this["mcSquare3"], this["mcSquare4"]];			
		
		/** liste des couleurs */
		p_aColors = ["b", "j", "r", "m", "v"];
		
		
		/** le tableau reponse */
		p_aAnswer = new Array(5);
		for(var i=0; i<p_aAnswer.length; i++){
			p_aAnswer[i] = new Array(5);
		}
		
		
		//var count:Number = 0;
		
		var count:Number = 0;
		for (var i in p_aColors){
			var ok:Boolean = true;
			while(ok){
				var x:Number = Math.floor(Math.random()*5);
				var y:Number = Math.floor(Math.random()*5);
				if(p_aAnswer[x][y] == null){
					ok = false;
				}
			}
			p_aAnswer[x][y] = p_aColors[i];
			
			// creation de la solution
			p_mcSolution["mcCircle"+count].gotoAndStop(p_aColors[i]);
			p_mcSolution["mcCircle"+count]._x = p_mcSolution["mcCircle"+count]._width/2 + 65 + y*35 ;
			p_mcSolution["mcCircle"+count]._y = p_mcSolution["mcCircle"+count]._height/2 + 52 + x*35 ;		
			
			count++;
		}
		
		/** rotation */
		p_iCurrRotation = 0;
		
		initInterface();
		
	}	


	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		// cache la solution
		p_mcSolution._visible = false;
		
		// manettes
		Application.useLibCursor(p_btManette1, "cursor-finger");
		p_btManette1.onRelease = function(){
			_parent.rotate(1);
		}
		Application.useLibCursor(p_btManette2, "cursor-finger");
		p_btManette2.onRelease = function(){
			_parent.rotate(-1);
		}
		
		// cercles
		
		for(var i=0; i<p_aCircles.length; i++){
			if(p_aCircles[i]["initX"] != null){
				p_aCircles[i]._x = p_aCircles[i]["initX"];
				p_aCircles[i]._y = p_aCircles[i]["initY"]; 
			}
			p_aCircles[i].gotoAndStop(i+1);
			p_aCircles[i]["color"] = p_aColors[i];
			p_aCircles[i]["initX"] = p_aCircles[i]._x;
			p_aCircles[i]["initY"] = p_aCircles[i]._y;
			p_aCircles[i]["i"] = -1;
			p_aCircles[i]["j"] = -1;
			p_aCircles[i].onPress = function(){
				_x = _parent._xmouse;
				_y = _parent._ymouse;
				startDrag(this);
			}
			Application.useLibCursor(p_aCircles[i], "cursor-finger");
			p_aCircles[i].onRelease = p_aCircles[i].onReleaseOutside = function(){
				CursorManager.hide();
				stopDrag();
				_parent.dropCircle(this);
				CursorManager.show();
			}
		}
		
		
		// puces
		for(var i in p_aPuces){
			p_aPuces[i]._visible = false;
		}		
		
		// squares
		for(var i in p_aSquares){
			p_aSquares[i]._visible = false;
		}		
		
		// bouton depart / j'ai fini
		p_btDepart.onRelease = function(){
			_parent.updateDisplay();
			_parent["p_txtBouton"].text = "j'ai fini";
			this.onRelease = function(){
				_parent.checkEnd();
			}
		}
		
		// aide spéciale
		Application.useLibCursor(p_mcHelp, "cursor-finger");
		p_mcHelp.onMouseDown = function(){
			if(this._visible){
				if(this._currentframe == this._totalframes){
					this.gotoAndStop(1);
				}else{
					this.gotoAndStop(this._currentframe+1);
				}
			}
		}		
		
	}	
	
	/**
	 * dropCircle
	 * drop d'un cercle
	 */
	private function dropCircle(mcCircle:MovieClip):Void{
		
		p_oSoundPlayer.playASound("clic",0);
		
		var mcDrop:MovieClip = eval(mcCircle._droptarget);
		if (mcDrop._name.indexOf("mcTile_") != -1){
			
			// drop sur une tile
			mcCircle._x = mcDrop._x;
			mcCircle._y = mcDrop._y;	
			switch(p_iCurrRotation){				
				case 0:			
					mcCircle["i"] = p_aTiles[ mcDrop["i"] ][ mcDrop["j"] ]["i"];
					mcCircle["j"] = p_aTiles[ mcDrop["i"] ][ mcDrop["j"] ]["j"];
					break;
				case 1:			
					mcCircle["i"] = 4 - p_aTiles[ mcDrop["i"] ][ mcDrop["j"] ]["j"];
					mcCircle["j"] = p_aTiles[ mcDrop["i"] ][ mcDrop["j"] ]["i"];
					break;
				case 2:			
					mcCircle["i"] = 4 - p_aTiles[ mcDrop["i"] ][ mcDrop["j"] ] ["i"];
					mcCircle["j"] = 4 - p_aTiles[ mcDrop["i"] ][ mcDrop["j"] ] ["j"];
					break;
				case 3:			
					mcCircle["i"] = p_aTiles[ mcDrop["i"] ][ mcDrop["j"] ]["j"];
					mcCircle["j"] = 4 - p_aTiles[ mcDrop["i"] ][ mcDrop["j"] ]["i"];
					break;
			}
			
		}else{
			mcCircle._x = mcCircle["initX"];
			mcCircle._y = mcCircle["initY"];
			mcCircle["i"] = -1;
			mcCircle["j"] = -1;
		}
		
	}
	
	/**
	 * rotate
	 * rotation du tableau
	 */
	private function rotate(iSens:Number):Void{
		
		// incrementation
		p_iCurrRotation += iSens;
		if(p_iCurrRotation==4) p_iCurrRotation = 0;
		if(p_iCurrRotation==-1) p_iCurrRotation = 3;
		
		updateDisplay();
		
	}
	
	/**
	 * updateDisplay
	 * met à jour l'affichage du tableau
	 */
	private function updateDisplay():Void{
		
		p_oSoundPlayer.playASound("swiftCliClac",0);
		
		p_mcFlags.gotoAndStop(p_iCurrRotation+1);
		
		switch(p_iCurrRotation){
			
			case 0:
				// grille
				for(var i in p_aCircles){
					var iId:Number = p_aCircles[i]["i"];
					var jId:Number = p_aCircles[i]["j"];
					p_aCircles[i]._x = p_aTiles[iId][jId]._x;
					p_aCircles[i]._y = p_aTiles[iId][jId]._y;
				}
				// soluce
				for(var j=0; j<5; j++){
					p_aSquares[j]._visible = false;
					for(var i=0; i<5; i++){						
						if(p_aAnswer[i][j] != null){
							p_aSquares[j].gotoAndStop(p_aAnswer[i][j]);
							p_aSquares[j]._visible = true;
						}
					}					
				}
				break;
			
			case 1:
				// grille
				for(var i in p_aCircles){
					var iId:Number = p_aCircles[i]["j"];
					var jId:Number = 4 - p_aCircles[i]["i"];
					p_aCircles[i]._x = p_aTiles[iId][jId]._x;
					p_aCircles[i]._y = p_aTiles[iId][jId]._y;
				}
				// soluce
				for(var i=0; i<5; i++){					
					p_aSquares[4-i]._visible = false;
					for(var j=0; j<5; j++){						
						if(p_aAnswer[i][j] != null){
							p_aSquares[4-i].gotoAndStop(p_aAnswer[i][j]);
							p_aSquares[4-i]._visible = true;
						}
					}					
				}
				break;
				
			case 2:	
				// grille
				for(var i in p_aCircles){
					var iId:Number = 4 - p_aCircles[i]["i"];
					var jId:Number = 4 - p_aCircles[i]["j"];
					p_aCircles[i]._x = p_aTiles[iId][jId]._x;
					p_aCircles[i]._y = p_aTiles[iId][jId]._y;
				}
				// soluce
				for(var j=0; j<5; j++){					
					p_aSquares[4-j]._visible = false;
					for(var i=4; i>-1; i--){						
						if(p_aAnswer[i][j] != null){
							p_aSquares[4-j].gotoAndStop(p_aAnswer[i][j]);
							p_aSquares[4-j]._visible = true;
						}
					}					
				}	
				break;
				
			case 3:
				// grille
				for(var i in p_aCircles){
					var iId:Number = 4 - p_aCircles[i]["j"];
					var jId:Number = p_aCircles[i]["i"];
					p_aCircles[i]._x = p_aTiles[iId][jId]._x;
					p_aCircles[i]._y = p_aTiles[iId][jId]._y;
				}
				// soluce
				for(var i=0; i<5; i++){					
					p_aSquares[i]._visible = false;
					for(var j=4; j>-1; j--){						
						if(p_aAnswer[i][j] != null){
							p_aSquares[i].gotoAndStop(p_aAnswer[i][j]);
							p_aSquares[i]._visible = true;
						}
					}					
				}	
				break;				
			
		}
		
	}
	
	
	/**
	 * checkEnd
	 * test si la partie est gagnée par le joueur en cours
	 */
	private function checkEnd(iTurn:Number){
		
		var ok:Boolean = true;
		for(var i in p_aCircles){
			var x:Number = p_aCircles[i]["i"];
			var y:Number = p_aCircles[i]["j"];
			if ( p_aAnswer[x][y] != p_aCircles[i]["color"]){
				ok = false;
			}
		}
		
		if(ok){
			p_iNbPartsWon++;
			if(p_iNbPartsWon == p_iNbPartsToWin){
				won();
			}else{
				startGame();
				gotoAndStop("part2");
				p_btDepart.onRelease();
			}
		}else{
			p_oSoundPlayer.playASound("bicle2",0);
			p_iNbTries++;
			p_aPuces[p_iNbTries-1]._visible = true;
			if (p_iNbTries == p_iNbMaxTries){
				loose();
			}

		}
		
	}


	
	
	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();
		
		p_oSoundPlayer.playASound("clicClacToc",0);
		
	}
	
	/**
	 * la partie est gagnée, par l'adversaire
	 */
	private function loose(){

		gotoAndStop("loose");
		
		// affichage de la solution
		p_mcSolution._visible = true;
		
	}
}

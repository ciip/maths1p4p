﻿/**
 * Copyright nir.....
 * 
 * class com.maths1p4p.level3.game3_14.Game3_14
 * 
 * @author Grry Lardon
 * @version 0.1
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import maths1p4p.level3.game3_14.*

class maths1p4p.level3.game3_14.Game3_14 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** le clip cache */
	public var p_mcCache:MovieClip;
	
	/** le champs départ */
	public var p_txtDepart:TextField;
	/** le bouton départ */
	public var p_btDepart:Button;

	/** le nombre de tuiles */
	public static var NBTILE:Number=16;
	/** le depth de départ */
	public static var DEPTH:Number=100;

	/** les loots */
	public var p_mcLoots:MovieClip;
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var p_aSlot:Array;
	private var p_aStartSlot:Array;
	private var p_aTile:Array;
	
	/** le nombre de parties agner */
	private var p_iNbPartsToWin:Number = 1;
	/** le nombre de parties gagn*/
	private var p_iNbPartsWon:Number = 0;
	
	/** nombre d'essais */
	private var p_iNbTries:Number;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_14()	{		
		super();			
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * darage du jeu, implnt
	 */
	private function startGame(){	
		
		initInterface();
				
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		
		
		Application.useLibCursor(p_btDepart, "cursor-finger");
		p_btDepart.onRelease = function(){
			_parent.startPart();
		}	
		
	}	
	
	
	/**
	 * début de la partie
	 */
	private function startPart():Void{		
		
		p_oSoundPlayer.playASound("miniClick",0);
		
		p_txtDepart.text = "J'ai fini";
		p_btDepart.onRelease = function(){
			_parent.checkEnd();
		}	

		p_aStartSlot=new Array(NBTILE);
		p_aTile=new Array(NBTILE);
		
		
		createStartSlot();
		createMiddleSlot();
		createTile();
		
	}	
	
	
	/**
	 * creation du plateau du jeu
	 */
	private function createMiddleSlot():Void{
		
		var X:Number=243;
		var Y:Number=104;
		
		p_aSlot=new Array(NBTILE/4);
		
		for(var i:Number=0;i<NBTILE/4;i++){
			p_aSlot[i]=new Array(4);
			
			for(var e:Number=0;e<NBTILE/4;e++){	
				p_aSlot[i][e]=this.attachMovie("mcEmtpySlot","mcFinalSlot"+i+""+e,DEPTH++,{_x:X,_y:Y})
				Y+=44;
			}
		Y=104;
		X+=44;
		}
			
	}
	
	/**
	 * initialisation des tuiles
	 */
	private function createStartSlot():Void {
		
		var X:Number=85;
		var Y:Number=148;
		var numberTilePlace=0;
		
		for(var i:Number=0;i<NBTILE;i++){
			
			p_aStartSlot[i]=this.attachMovie("mcEmtpySlot","mcStartSlot"+i,DEPTH++,{_x:X,_y:Y})
			Y+=46;
			
			numberTilePlace++;
			if(numberTilePlace==4 && i>0){
				X+=50;
				if(i==7){X=475;}
				Y=148;
				numberTilePlace=0;
			}
			
		}
	}
	
	/**
	 * creation des tuiles
	 */
	private function createTile():Void{
		
		var X:Number=85;
		var Y:Number=148;
		var numberTilePlace=0;
		
		for(var i:Number=0;i<NBTILE;i++){
			
			p_aTile[i]=this.attachMovie("mcTile"+i,"mcTile"+i,DEPTH++,{_x:X,_y:Y})
			p_aTile[i].setParent(this);
			p_aStartSlot[i].putTile(p_aTile[i]);
			p_aTile[i].setSlot(p_aStartSlot[i]);
			Y+=45;
			
			numberTilePlace++;
			if(numberTilePlace==4 && i>0){
				X+=50;
				if(i==7){X=475;}
				Y=148;
				numberTilePlace=0;
			}
			
		}
		
		p_aTile[0].setColor(0,0,0,0);
		p_aTile[1].setColor(0,0,1,0);
		p_aTile[2].setColor(0,0,0,1);
		p_aTile[3].setColor(0,1,0,0);
		p_aTile[4].setColor(1,0,0,0);
		p_aTile[5].setColor(0,0,1,1);
		p_aTile[6].setColor(0,1,0,1);
		p_aTile[7].setColor(1,1,0,0);
		p_aTile[8].setColor(0,1,1,0);
		p_aTile[9].setColor(1,0,1,0);
		p_aTile[10].setColor(1,0,0,1);
		p_aTile[11].setColor(0,1,1,1);
		p_aTile[12].setColor(1,0,1,1);
		p_aTile[13].setColor(1,1,1,0);
		p_aTile[14].setColor(1,1,0,1);
		p_aTile[15].setColor(1,1,1,1);
		
	}
	
	/**
	 * drop d'une tuile
	 */
	public function onAction(objet:MovieClip):Void{
	
		for(var i:Number=0;i<NBTILE/4;i++){
			for(var e:Number=0;e<NBTILE/4;e++){
				
				if(objet.hitTest(p_aSlot[i][e])&& p_aSlot[i][e].hitTest(this._xmouse,this._ymouse, true)){
					
					if(p_aSlot[i][e].emptySlot()){
					
						if(checkTileAround(Tile(objet),i,e)){
							
							// placé
							p_oSoundPlayer.playASound("clic",0);
							
							(objet.getSlot()).removeTile();
							 objet.setSlot(p_aSlot[i][e]);
							
							Slot(p_aSlot[i][e]).putTile(Tile(objet));

						}else{
							// mal placée
							p_oSoundPlayer.playASound("bicle2",0);
							objet.backToOriginSlot();
						}
						
					}else{
						objet.backToOriginSlot();
					}
						
					return;
					
				}

			}
		}
		
		for(var i:Number=0;i<NBTILE;i++){
			if(objet.hitTest(p_aStartSlot[i])){
				if(p_aStartSlot[i].emptySlot()){
					objet.getSlot().removeTile();
					objet.setSlot(p_aStartSlot[i]);
					Slot(p_aStartSlot[i]).putTile(Tile(objet));
				}
				else{
					 objet.backToOriginSlot();
				}
				return;
			}
		}		
	
		objet.backToOriginSlot();
						
	}
	
	/**
	 * renvois le nombre de tuiles placées en jeu
	 */
	public function getNumberOfTilePlaced():Number{
		
		var numberWin:Number=0;
		
		for(var i=0;i<NBTILE/4;i++){
			for(var j=0;j<NBTILE/4;j++){
				if(p_aSlot[i][j].getTile()!=undefined){
					numberWin++;
				}
			}
		}						
						
		return numberWin;
		
	}
	
	
	/**
	 * check des tuiles environnantes
	 */
	private function checkTileAround(objet:Tile,pX:Number,pY:Number):Boolean{
		
		var checkResult:Boolean=true;
		
		if(p_aSlot[pX-1][pY].getTile()!=undefined && p_aSlot[pX-1][pY].getTile()!=objet)
			checkResult&=objet.compareLeft(p_aSlot[pX-1][pY].getTile());
			
		if(p_aSlot[pX+1][pY].getTile()!=undefined && p_aSlot[pX+1][pY].getTile()!=objet)
			checkResult&=objet.compareRight(p_aSlot[pX+1][pY].getTile());	
		
		if(p_aSlot[pX][pY-1].getTile()!=undefined && p_aSlot[pX][pY-1].getTile()!=objet)
			checkResult&=objet.compareUp(p_aSlot[pX][pY-1].getTile());
			
		if(p_aSlot[pX][pY+1].getTile()!=undefined && p_aSlot[pX][pY+1].getTile()!=objet)
			checkResult&=objet.compareDown(p_aSlot[pX][pY+1].getTile());
		
		return checkResult;
		
	}
	
	/**
	 * vérifie si la partie est gagnée
	 */
	private function checkEnd(){
		trace("nvtiles : "+getNumberOfTilePlaced());
		if(getNumberOfTilePlaced() >= 15){
			if(getNumberOfTilePlaced() == 16){
				trace("plame or");
				p_bPalmeOr = true;
			}
			won();
		}
		
	}
	
	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();		
		p_oSoundPlayer.playASound("petitShlac",0);
				
	}
	
	
	/**
	 * la partie est perdue
	 * @param msg le message a afficher
	 */
	private function loose(sMsg:String){
		
		gotoAndStop("loose");
		
	}	
	
	
}

﻿/**
 * Copyright 
 * 
 * class com.maths1p4p.level3.game3_14.Slot
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 
import maths1p4p.level3.game3_14.Tile;
class maths1p4p.level3.game3_14.Slot extends MovieClip {
	
	
	private var p_mcTile:Tile;
	
	public function Slot(){
		
		p_mcTile=undefined;
		
	}
	
	public function emptySlot():Boolean{
		
		if(p_mcTile==undefined){
			return true;
		}
		return false;
		
	}
	
	public function getTile():Tile{
		
		return p_mcTile;
		
	}
	
	public function putTile(tuile:Tile):Boolean{
		
		if(emptySlot()){
			p_mcTile=tuile;
			return true;
		}
		return false;
		
	}
	
	public function removeTile():Boolean{
		
		if(!emptySlot()){
			p_mcTile=undefined;
			return true;
		}
		return false;
		
	}
	
}

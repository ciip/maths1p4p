﻿/**
 * Copyright 
 * 
 * class com.maths1p4p.level3.game3_14.Tile
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import mx.utils.Delegate;
import maths1p4p.level3.game3_14.Slot;

class maths1p4p.level3.game3_14.Tile extends MovieClip {
	
	private var p_aGreenOrWhite:Array;
	
	public static var WHITE:Number=0;
	public static var GREEN:Number=1;
	public static var TOPLEFT:Number=0;
	public static var TOPRIGHT:Number=1;
	public static var BOTTOMLEFT:Number=2;
	public static var BOTTOMRIGHT:Number=3;
	
	private var p_oParent:Object;
	private var p_mcCurrentSlot:Slot;
	
	public function Tile(){
		init();
		p_mcCurrentSlot=undefined;
		Application.useLibCursor(this, "cursor-finger");

	}
	
	
	private function init():Void{
		p_aGreenOrWhite=new Array(4);
		
		for(var i:Number=0;i<4;i++){
			p_aGreenOrWhite[i]=WHITE;
		}
		
	}
	
	public function setSlot(slt:Slot):Void{
			p_mcCurrentSlot=slt;
			_x=p_mcCurrentSlot._x;
			_y=p_mcCurrentSlot._y;
	}
	
	public function getSlot():Slot{
		return p_mcCurrentSlot;
	}
	
	public function setParent(obj:Object):Void{
		p_oParent=obj;
	}
	
	public function backToOriginSlot():Void{
		MovieClip(this).tween(["_x","_y"],[p_mcCurrentSlot._x,p_mcCurrentSlot._y],0.15,"linear");
	}
	
	public function setColor(ctopLeft:Number,ctopRight:Number,cbottomLeft:Number,cbottomRight:Number):Void{
	
		p_aGreenOrWhite[BOTTOMLEFT]=cbottomLeft;
		p_aGreenOrWhite[TOPLEFT]=ctopLeft;
		p_aGreenOrWhite[BOTTOMRIGHT]=cbottomRight;
		p_aGreenOrWhite[TOPRIGHT]=ctopRight;
	}
	
	function onPress(){
		this.swapDepths(maths1p4p.level3.game3_14.Game3_14.DEPTH++);
		startDrag(this);
	}
	
	function onRelease() {
		stopDrag();
		Delegate.create(this,p_oParent.onAction(this));
	}

	public function getColorBottomLeft():Number{
			return p_aGreenOrWhite[BOTTOMLEFT];
	}
	
	public function getColorTopLeft():Number{
			return p_aGreenOrWhite[TOPLEFT];
	}
	
	public function getColorBottomRight():Number{
			return p_aGreenOrWhite[BOTTOMRIGHT];
	}
	
	public function getColorTopRight():Number{
			return p_aGreenOrWhite[TOPRIGHT];
	}
	
	public function compareLeft( tuile:Tile):Boolean{
		return (getColorTopLeft()==tuile.getColorTopRight()) 
		&& (getColorBottomLeft()==tuile.getColorBottomRight());
	}
	
	public function compareRight( tuile:Tile):Boolean{
		return (getColorTopRight()==tuile.getColorTopLeft()) 
		&& (getColorBottomRight()==tuile.getColorBottomLeft());
	}
	
	public function compareUp(tuile:Tile):Boolean{
		return (getColorTopRight()==tuile.getColorBottomRight()) 
		&& (getColorTopLeft()==tuile.getColorBottomLeft());
	}
	
	public function compareDown(tuile:Tile):Boolean{
		return (getColorBottomRight()==tuile.getColorTopRight()) 
		&& (getColorBottomLeft()==tuile.getColorTopLeft());
	}
	
}

﻿/**
 * class com.maths1p4p.level3.game3-15.DisplayMap
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * TODO :
 * integrer la gestion des sons
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

class maths1p4p.level3.game3_15.AbstractMap {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** largeur et hauteur de la map */
	public var p_iW, p_iH:Number;
	

		
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	/** la map */
	private var p_aMap:Array;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function AbstractMap(w, h){	
		
		p_aMap = new Array();
		for(var i=0; i<w; i++){
			p_aMap[i] = new Array();
			for(var j=0; j<h; j++){
				p_aMap[i][j] = "";				
			}
		}
		
	}
	
	/**
	 * définit une tile
	 */
	public function setTile(x, y, val):Void{
		
		p_aMap[x][y] = val;
		
	}
	
	
	/**
	 * renvoi la valeur d'une tile
	 */
	public function getTile(x, y){
		
		return p_aMap[x][y];
		
	}
	
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	
	
}

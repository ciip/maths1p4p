﻿/**
 * class com.maths1p4p.level3.game3-15.DisplayMap
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * TODO :
 * integrer la gestion des sons
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

class maths1p4p.level3.game3_15.DisplayMap extends MovieClip {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** largeur et hauteur de la map */
	public var p_iW, p_iH:Number;
	
	/** largeur et hauteur des tiles */
	public var p_iTileW, p_iTileH:Number;
	
	/** espace entre les tiles */
	public var p_iTileBoderW, p_iTileBoderH:Number;	
	
		
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	/** la map */
	private var p_aMap:Array;
	
	/** le mc contenant les tiles */
	private var p_mcTiles:MovieClip;
	

	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function DisplayMap(){		
		
	}
	
	/**
	 * initialisation
	 */
	public function init(w, h, tileW, tileH, tileBorderW, tileBorderH){		

		p_iW = w;
		p_iH = h;
		p_iTileW = tileW;
		p_iTileH = tileH;
		p_iTileBoderW = tileBorderW;
		p_iTileBoderH = tileBorderH;
		
		// creation de la map
		p_aMap = new Array(p_iW);
		for (var i in p_aMap){
			p_aMap[i] = new Array(p_iH);
			for(var j in p_aMap[i]){
				p_aMap[i][j] = "default";
			}
		}
		
		// creation du mc contenant les tiles
		p_mcTiles = createEmptyMovieClip("p_mcTiles", 5);
		
		// dessin de la map de base
		drawMap();
	
	}
	
	/**
	 * définit une tile
	 */
	public function setTile(x, y, val):Void{
		
		p_aMap[x][y] = val;
		
	}
	
	
	/**
	 * renvoi la valeur d'une tile
	 */
	public function getTile(x, y){
		
		return p_aMap[x][y];
		
	}
	
	/** une tuile est cliquée */
	public function TileClicked(mcTile:MovieClip){
		
		_parent.TileClicked(mcTile);
		
	}
	
	public function getTilesAround(mcTile):Array{
		
		var aResult = [];
		if(mcTile.isAccessible("haut")){
			aResult.push(getTileMc(mcTile.p_iX, mcTile.p_iY-1));
		}
		if(mcTile.isAccessible("bas")){
			aResult.push(getTileMc(mcTile.p_iX, mcTile.p_iY+1));
		}
		if(mcTile.isAccessible("droite")){
			aResult.push(getTileMc(mcTile.p_iX+1, mcTile.p_iY));
		}
		if(mcTile.isAccessible("gauche")){
			aResult.push(getTileMc(mcTile.p_iX-1, mcTile.p_iY));
		}
		return aResult;
		
	}
	
	/** renvois le clip aux coordonnées x,y */
	public function getTileMc(x, y){
		return p_mcTiles["mcTile_"+x+"_"+y];
	}	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	
	
	/** dessin de la map de base */
	private function drawMap(){

		var iTileCount:Number = 0;
		for (var i=0; i<p_iW; i++){
			
			for (var j=0; j<p_iH; j++){

				var mcTile:MovieClip = p_mcTiles.attachMovie("mcTile", "mcTile_"+i+"_"+j, iTileCount);
				mcTile._x = i * p_iTileW + i * p_iTileBoderW;
				mcTile._y = j * p_iTileH + j * p_iTileBoderH;
				mcTile.init(i,j);	
				iTileCount++;
				
			}
			
		}
		
	}
	
	
	
}

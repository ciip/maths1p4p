﻿/**
 * class com.maths1p4p.level3.game3-15.Game3_15
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import maths1p4p.level3.game3_15.*
import maths1p4p.Maths.Operations.OperationMaker;
import flash.geom.Point;


class maths1p4p.level3.game3_15.Game3_15 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** le bouton cache */
	public var p_btCache:Button;
	/** le bouton ok depart */
	public var p_btOk:Button;
	
	/** les loots */
	public var p_mcLoots:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** la map */
	private var p_oMap:AbstractMap;
	
	/** le movieclip contenant la map */
	private var p_mcDisplayMap:MovieClip;
	
	/** arrivée */
	private var p_ptEndLoc:Array;

	/** la tile selectionnée */
	private var p_mcCurrTile:MovieClip;
	
	/** le movieclip opération */
	private var p_mcOperation:MovieClip;
	
	/** l'operation en cours */
	private var p_oCurrOperation:Object;
	/** l'element de l'opération à trouver */
	private var p_iCurrItemToFind:String;

		
	/** le nombre de parties à gagner */
	private var p_iNbPartsToWin:Number = 1;
	/** le nombre de parties gagnées*/
	private var p_iNbPartsWon:Number = 0;

	/** le champs du bouton depart/ok */
	private var p_txtOkButton:TextField;
	
	/** nombre d'essais */
	private var p_iNbTries:Number;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_15()	{		
		super();			
	}
	
	/**
	 * une tuile est cliquée
	 */
	public function TileClicked(mcTile:MovieClip){
		
		p_mcCurrTile = mcTile;
		mcTile.setState("selected");	
		p_btCache._visible = true;
		// nouvelle question
		askNewQuestion();
		
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		initInterface();
	
		// initialisation des propriétés
		
		p_oMap = new AbstractMap(7, 7);
		
		p_mcDisplayMap._x = 180;
		p_mcDisplayMap._y = 44;
		p_mcDisplayMap.init(7, 7, 36, 36, 3, 3);
		
		// définition des access
		p_mcDisplayMap.getTileMc(1,2).setAccess({droite:false});
		p_mcDisplayMap.getTileMc(1,3).setAccess({droite:false});
		p_mcDisplayMap.getTileMc(1,4).setAccess({droite:false});
		
		p_mcDisplayMap.getTileMc(2,1).setAccess({bas:false});
		p_mcDisplayMap.getTileMc(2,2).setAccess({gauche:false, haut:false});
		p_mcDisplayMap.getTileMc(2,3).setAccess({gauche:false});
		p_mcDisplayMap.getTileMc(2,4).setAccess({gauche:false, bas:false});
		p_mcDisplayMap.getTileMc(2,5).setAccess({haut:false});
		
		p_mcDisplayMap.getTileMc(3,1).setAccess({bas:false});
		p_mcDisplayMap.getTileMc(3,2).setAccess({haut:false});		
		p_mcDisplayMap.getTileMc(3,4).setAccess({bas:false});
		p_mcDisplayMap.getTileMc(3,5).setAccess({haut:false});
		
		p_mcDisplayMap.getTileMc(4,1).setAccess({bas:false});
		p_mcDisplayMap.getTileMc(4,2).setAccess({haut:false});		
		p_mcDisplayMap.getTileMc(4,4).setAccess({bas:false});
		p_mcDisplayMap.getTileMc(4,5).setAccess({haut:false});

		// position d'arrivée
		p_ptEndLoc = [ new Point(2,2), new Point(2,4), new Point(3,3) ];
				
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		

		p_btCache._visible = false;
		p_btCache.useHandCursor = false;
		
		p_btOk.enabled = true;
		Application.useLibCursor(p_btOk, "cursor-finger");
		p_btOk.onRelease = function(){
			_parent.start();
		}
		
		p_txtOkButton =  this["p_txtOkButton"];
		p_txtOkButton.text = "Départ";
		
	}	
	
	/** début de la partie */
	private function start():Void{
		
		p_oSoundPlayer.playASound("miniClick",0);
		
		// début de la partie
		TileClicked(p_mcDisplayMap.getTileMc(0,3));
		p_btOk.onRelease = function(){
			var iReponse:Number = Number(_parent.p_mcOperation.getResponse());
			_parent.checkResponse(iReponse);
		}
		p_btOk.enabled = true;
		p_txtOkButton.text = "Ok";
		
		p_iNbTries = 0;
	
	}
	
	
	
	/**
	 * nouvelle question
	 */
	private function askNewQuestion(){		
		
		// addition ou soustraction
		var iTypeOperation:Number = Math.floor(Math.random()*2);
		if(iTypeOperation == 0){
			// cree une addition
			p_oCurrOperation = OperationMaker.getAddition(10, 90);
			trace("addiition");
		}else{
			// cree une soustraction
			trace("soustraction");
			p_oCurrOperation = OperationMaker.getSoustraction(30, 60);
		}
		
		// choix de l'éléments de l'opération à trouver
		var iItemToFind:Number = Math.floor(Math.random()*3);
		p_iCurrItemToFind = ["A","B","R"][iItemToFind];
		
		p_mcOperation.init(p_oCurrOperation, p_iCurrItemToFind);

		p_txtOkButton.text = "Ok";
		p_btOk.enabled = true;
		
	}	
	
	/**
	 * vérifie la réponse
	 */
	private function checkResponse(iReponse){

		p_mcCurrTile.enabled = false;
		
			
		// vide les champs
		p_mcOperation.hide();
		
		p_btCache._visible = false;
		p_txtOkButton.text = "";
		p_btOk.enabled = false;

		if(iReponse == p_oCurrOperation[p_iCurrItemToFind]){
			
			// Bonne réponse		
			
			p_oSoundPlayer.playASound("swiftCliClac",0);
			
			p_mcCurrTile.gotoAndStop("win");
			// active les tiles autour
			var aTiles:Array = p_mcDisplayMap.getTilesAround(p_mcCurrTile);

			for(var i in aTiles){
				if(aTiles[i].getState() == "default"){
					aTiles[i].enabled = true;
				}
			}
			
			// vérifie si la partie est finie
			if(isFinished(p_mcCurrTile.p_iX, p_mcCurrTile.p_iY)){
				won();
			}
			p_iNbTries++;
		}else{
			
			// mauvaise réponse
			
			p_oSoundPlayer.playASound("plouf",0);
			
			if (p_iNbTries == 0){
				p_mcCurrTile.gotoAndStop("default");
				p_mcCurrTile.enabled = true;
				initInterface();
			}else{
				p_mcCurrTile.gotoAndStop("loose");
			}			
			
		}
		
	}
	
	/** vérifie si la partie est finie */
	private function isFinished(x,y):Boolean{
		for(var i in p_ptEndLoc){
			if(p_ptEndLoc[i].x == x && p_ptEndLoc[i].y == y){
				return true;
			}
		}
		return false;
	}

	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();		
		p_oSoundPlayer.playASound("metalOndule",0);
				
	}
	
	
	/**
	 * la partie est perdue
	 * @param msg le message a afficher
	 */
	private function loose(sMsg:String){
		
		gotoAndStop("loose");
		
	}	
	
}

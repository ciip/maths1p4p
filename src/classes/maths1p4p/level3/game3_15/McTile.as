﻿/**
 * class com.maths1p4p.level3.game3-15.McTile
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level3.game3_15.McTile extends MovieClip {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_iX, p_iY:Number;
	
		
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_oAccess:Object;
	private var p_sState:String;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function McTile(){		
		p_sState = "default";
		p_oAccess = new Object();
		p_oAccess.haut = true;
		p_oAccess.bas = true;
		p_oAccess.droite = true;
		p_oAccess.gauche = true;
		enabled = false;
		Application.useLibCursor(this, "cursor-finger");
	}
	
	/** initialisations */
	public function init(x,y){
		p_iX = x;
		p_iY = y;
	}
	
	/** change l'état de la tile */
	public function setState(sState:String):Void{
		p_sState = sState;
		gotoAndStop(sState);
		if(sState == "won" || sState == "loose"){
			enabled = false;
		}
	}
	
	/** renvois l'état de la tile */
	public function getState():String{
		return p_sState;		
	}
	
	/** definit les access */
	public function setAccess(oAccess:Object){		
		for (var i in oAccess){
			p_oAccess[i] = oAccess[i];
		}		
	}
	
	/** renvois l'accessibilité */
	public function isAccessible(sDirection:String):Boolean{
		return p_oAccess[sDirection];
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	private function onRelease(){
		_parent._parent.TileClicked(this);
	}
	
	
	
}

﻿/**
 * class com.maths1p4p.level3.game3_16.Game3_16
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * 
 * TODO :
 * certains éléments ne sont pas toujours masqués au démarrage
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import com.prossel.utils.CursorManager;
import maths1p4p.level3.game3_16.*
import maths1p4p.Maths.Operations.OperationMaker;
import flash.geom.Point;

class maths1p4p.level3.game3_16.Game3_16 extends maths1p4p.level3.Level3AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** les loots */
	public var p_mcLoots:MovieClip;
	
	/** le bouton ok */
	public var p_btDepart:Button;
	
	/** masks des noms des joueurs */
	public var p_mcTxtPlayerMask1, p_mcTxtPlayerMask2:MovieClip;
	
	/** les clips des joueurs */
	public var p_mcPlayer1, p_mcPlayer2:MovieClip;
	
	/** bouton pour empecher les clics sur les tiles */
	public var p_btAntiClic:Button;
	
	/** le clip operation */
	public var p_mcOperation:MovieClip;
	
	/** le champs texte du bouton */
	public var p_txtBouton:TextField;
	
	/** les info bulles */
	public var p_mcInfo1, p_mcInfo2:MovieClip;
	
	/** les couronnes */
	public var p_mcPlayer1Couronne1, p_mcPlayer1Couronne2, p_mcPlayer2Couronne1, p_mcPlayer2Couronne2:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** joueurs */
	private var p_oPlayer1, p_oPlayer2;
	
	/** tour actuel, = 0 ou 1 */
	private var p_iCurrTurn:Number;
			
	/** le tableau des slots */
	private var p_aSlots:Array;
	
	/** la tile courante */
	private var p_mcCurrTile:MovieClip;
	
	/** l'operation en cours */
	private var p_oCurrOperation:Object;
	/** l'element de l'opération à trouver */
	private var p_iCurrItemToFind:String;
	
	/** phase du jeu en cours : 1 ou 2 */
	private var p_iCurrPhase:Number;
			
	/** le nombre de parties à gagner */
	private var p_iNbPartsToWin:Number = 2;
	/** le nombre de parties gagnées*/
	private var p_iNbPartsWon1:Number = 0;
	private var p_iNbPartsWon2:Number = 0;	

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game3_16()	{		
		super();	
		// cache les infos
		p_mcInfo1._visible = p_mcInfo2._visible = false;	
		// cache les couronnes
		p_mcPlayer1Couronne1._visible = p_mcPlayer1Couronne2._visible = p_mcPlayer2Couronne1._visible = p_mcPlayer2Couronne2._visible = false;
	}	
	
		
	/**
	 * secondPlayerSelected
	 * appellé depuis level3
	 */
	public function secondPlayerSelected(oPlayer2:Object):Void{
		
		p_oPlayer1 = new Object();
		p_oPlayer1.name = _parent._parent.getPlayer().getName();
		p_oPlayer1.id = _parent._parent.getPlayer().getId();
		p_oPlayer2 = oPlayer2;
		
		initInterface();
		
	}
	
	/**
	 * un joueur a gagné
	 */
	public function setVictory(oPlayer:Object){
		
		if(oPlayer == p_oPlayer1){
			// c'est le premier joueur qui gagne
			won();
		}else{
			// c'est le 2e joueur qui gagne
			
		}
		
		

	}

	
	/**
	 * partie suivante
	 */
	public function nextPart():Void{
		
		p_oSoundPlayer.playASound("clic",0);
		
		Application.useLibCursor(p_btDepart, "cursor-finger");
		p_btDepart.onRelease = function(){
			_parent.showInfo();
		}
		
		// init des tiles
		for(var i=0; i<6; i++){
			for(var j=0; j<6; j++){
				this["mcTile_" +i+"_"+j].gotoAndStop(1);
				this["mcTile_" +i+"_"+j].enabled = true;;
			}
		}
		
		p_iCurrPhase = 1;
		// premier tour au hazard		
		p_iCurrTurn = Math.floor( Math.random()*2 );	
		// position des joueurs
		p_mcPlayer1["loc"] = new Point(Math.floor(Math.random()*6), Math.floor(Math.random()*6));
		var ok:Boolean = true;
		while(ok){
			var newPos:Point = new Point(Math.floor(Math.random()*6), Math.floor(Math.random()*6));;
			if(newPos.x != p_mcPlayer1.loc.x && newPos.y != p_mcPlayer1.loc.y){
				ok = false;
			}
		}
		p_mcPlayer2["loc"] = newPos;
		
		p_mcPlayer1._x = p_aSlots[p_mcPlayer1.loc.x][p_mcPlayer1.loc.y].initX;
		p_mcPlayer1._y = p_aSlots[p_mcPlayer1.loc.x][p_mcPlayer1.loc.y].initY;
		p_mcPlayer2._x = p_aSlots[p_mcPlayer2.loc.x][p_mcPlayer2.loc.y].initX;
		p_mcPlayer2._y = p_aSlots[p_mcPlayer2.loc.x][p_mcPlayer2.loc.y].initY;
		
		nextTurn();	
		
	}
	
	
	/**
	 * checkCurrResult
	 * check la réponse du joueur
	 */
	public function checkCurrResult(){
		
		Key.removeListener(this);
		
		p_btDepart.enabled = false;
		p_mcOperation.hide();
		
		if(p_mcOperation.getResponse() == p_oCurrOperation[p_iCurrItemToFind]){
			// bonne réponse
			p_mcCurrTile.enabled = false;
		}else{
			// mauvaise réponse
			p_mcCurrTile.gotoAndStop(1);
		}		
		nextTurn();
		
	}
	
	/** affiche les info bullles */
	public function showInfo():Void{

		this["p_mcInfo"+p_iCurrPhase]._visible = true;
		this["p_mcInfo"+p_iCurrPhase].onMouseDown = function(){
			_visible = false;
			delete this.onMouseDown;
		}
	}
	
	/** onKeyDown */
	public function onKeyDown():Void{
		if(Key.getCode() == Key.ENTER){
			checkCurrResult();
		}
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		// init des slots
		p_aSlots = new Array(6);
		for(var i=0;i<6;i++){
			p_aSlots[i] = new Array(8);
			for (var j=0; j<6; j++){
				p_aSlots[i][j] = this["mcTile_"+i+"_"+j];
				p_aSlots[i][j]["x"] = i;
				p_aSlots[i][j]["y"] = j;
				p_aSlots[i][j]["initX"] = p_aSlots[i][j]._x;
				p_aSlots[i][j]["initY"] = p_aSlots[i][j]._y;
				Application.useLibCursor(p_aSlots[i][j], "cursor-finger");
				p_aSlots[i][j].onRelease = function(){
					_parent.tileClicked(this);
				}
			}
		}
		
		// affichage de l'écran de sélection du joueur 2
		if(_parent._parent != null){
			_parent._parent.showSecondPlayerSelectionScreen();
		}else{
			// mode autonome
			p_oPlayer1 = new Object();
			p_oPlayer1.name = "joueur 1";
			p_oPlayer1.id = 1;
			
			p_oPlayer2 = new Object();
			p_oPlayer2.name = "joueur 2";
			p_oPlayer2.id = 2;
			
			// init de l'interface
			initInterface();
			
		}
		
	}	


	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		this["p_txtPlayer1"].text = p_oPlayer1.name;
		this["p_txtPlayer2"].text = p_oPlayer2.name;
		
		p_mcTxtPlayerMask1._alpha = 0;
		p_mcTxtPlayerMask2._alpha = 0;
		
		p_btDepart.onRelease = function(){
			_parent.nextPart();
			_parent.p_txtBouton.text = "suite";
		}
		p_btDepart.enabled = true;
		
		p_btAntiClic.useHandCursor = false;
		p_btAntiClic.onRelease = function(){};
		p_btAntiClic._visible = false;
		
		Application.useLibCursor(this["p_mcPlayer1"], "cursor-finger");
		Application.useLibCursor(this["p_mcPlayer2"], "cursor-finger");
		this["p_mcPlayer1"].onPress = this["p_mcPlayer2"].onPress = function(){
			_x = _parent._xmouse - _width/2;
			_y = _parent._ymouse - _height/2;
			startDrag(this);
		}
		this["p_mcPlayer1"].onRelease = this["p_mcPlayer2"].onRelease = this["p_mcPlayer1"].onReleaseOutside = this["p_mcPlayer2"].onReleaseOutside = function(){
			CursorManager.hide();
			stopDrag();
			_parent.dropPlayer(this);
			CursorManager.show();
		}
		
	}	
	
	/**
	 * dropPlayer
	 * gère le drop du joueur sur une case
	 */	
	private function dropPlayer(mcPlayer:MovieClip):Void{
		
		p_oSoundPlayer.playASound("clic",0);
		
		var mcDrop = eval(mcPlayer._droptarget);
		
		var ok:Boolean = false;
		
		if(mcDrop._name.indexOf("mcTile_") != -1){

			var xOffset:Number = Math.abs(mcDrop.x - mcPlayer.loc.x);
			var yOffset:Number = Math.abs(mcDrop.y - mcPlayer.loc.y);
			
			if( (xOffset != 0 || yOffset != 0) && (xOffset < 2 && yOffset < 2)){
				
				if(mcDrop._currentframe != 2){

					mcPlayer._x = mcDrop._x;
					mcPlayer._y = mcDrop._y;
					mcPlayer.loc.x = mcDrop.x;
					mcPlayer.loc.y = mcDrop.y;
					
					// phase 2 du tour
					p_btAntiClic._visible = false;
					this["p_mcPlayer"+(p_iCurrTurn+1)].enabled = false;
					p_iCurrPhase = 2;
					
					ok = true;
					
				}				
		
			}
			
		}
		
		if(!ok){
			mcPlayer._x = this["mcTile_"+mcPlayer.loc.x + "_" + mcPlayer.loc.y]._x;
			mcPlayer._y = this["mcTile_"+mcPlayer.loc.x + "_" + mcPlayer.loc.y]._y;							
		}
		
	}
	
	/**
	 * tileClicked
	 * @param la tile cliquée
	 */
	private function tileClicked(mcTile:MovieClip):Void{
		
		if(p_iCurrPhase == 2){
		
			p_mcCurrTile = mcTile;
			
			newQuestion();
			p_btAntiClic._visible = true;
			p_btDepart.enabled = false;
			
			mcTile.gotoAndStop(2);
			
		}else{
			showInfo();
		}
		
	}
	
	/**
	 * newQuestion
	 */
	private function newQuestion():Void{
		
		// multiplication
		p_oCurrOperation = OperationMaker.getMultiplication(0,10,0,10);
		
		// choix de l'éléments de l'opération à trouver
		p_iCurrItemToFind = "R";
		
		p_mcOperation.init(p_oCurrOperation, p_iCurrItemToFind);
		
		Key.addListener(this);
		
	}
	
	/**
	 * nextTurn
	 * changement de tour
	 */
	private function nextTurn():Void{
		
		p_oSoundPlayer.playASound("swiftCliClac",0);
		
		this["p_mcPlayer1"]._alpha = 50;
		this["p_mcPlayer2"]._alpha = 50;

		// changement de tour
		this["p_mcTxtPlayerMask"+(p_iCurrTurn+1)]._alpha = 60;		
		this["p_mcPlayer"+(p_iCurrTurn+1)].enabled = false;
		if(p_iCurrTurn==0){
			p_iCurrTurn = 1;
		}else{
			p_iCurrTurn = 0;
		}		
		this["p_mcPlayer"+(p_iCurrTurn+1)].enabled = true;
		this["p_mcPlayer"+(p_iCurrTurn+1)]._alpha = 100;
		this["p_mcTxtPlayerMask"+(p_iCurrTurn+1)]._alpha = 0;
		
		p_btAntiClic._visible = false;
		
		p_btDepart.onRelease = function(){
			_parent.showInfo();
		}
		
		p_iCurrPhase = 1;
		
		// check si le joueur en cours est bloqué, et donc aurait perdu
		if (!canMove(this["p_mcPlayer"+(p_iCurrTurn+1)])){
			if(p_iCurrTurn == 0){
				// player 2 gagne
				p_iNbPartsWon2++;
				this["p_mcPlayer2Couronne"+p_iNbPartsWon2]._visible = true;
				if(p_iNbPartsWon2 >= p_iNbPartsToWin){
					setVictory(p_oPlayer2);
				}else{
					nextPart();
				}				
			}else{
				// player 1 gagne
				p_iNbPartsWon1++;
				this["p_mcPlayer1Couronne"+p_iNbPartsWon1]._visible = true;
				if(p_iNbPartsWon1 >= p_iNbPartsToWin){
					setVictory(p_oPlayer1);
				}else{
					nextPart();
				}
			}
		}
		
	}
	
	/**
	 * check si le joueur peut encore bouger
	 * @param mcPlayer le clip du joueur
	 * @return un booleen
	 */
	private function canMove(mcPlayer:MovieClip):Boolean{
		
		var x:Number = mcPlayer.loc.x;
		var y:Number = mcPlayer.loc.y;
		
		var canMove:Boolean = false;
		
		if(this["mcTile_" + (x-1) + "_" + (y-1)]._currentframe == 1) canMove = true;
		if(this["mcTile_" + (x) + "_" + (y-1)]._currentframe == 1) canMove = true;
		if(this["mcTile_" + (x+1) + "_" + (y-1)]._currentframe == 1) canMove = true;
		if(this["mcTile_" + (x-1) + "_" + (y)]._currentframe == 1) canMove = true;
		if(this["mcTile_" + (x+1) + "_" + (y)]._currentframe == 1) canMove = true;
		if(this["mcTile_" + (x-1) + "_" + (y+1)]._currentframe == 1) canMove = true;
		if(this["mcTile_" + (x) + "_" + (y+1)]._currentframe == 1) canMove = true;
		if(this["mcTile_" + (x+1) + "_" + (y+1)]._currentframe == 1) canMove = true;		
		
		return canMove;
	}
	
	
	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();
		
		p_oSoundPlayer.playASound("clingClang...clic",0);
		
		this["p_mcPlayer1"].enabled = false;
		this["p_mcPlayer2"].enabled = false;
		p_btAntiClic._visible = true;
		p_btAntiClic.onRelease = function(){};
		
	}
	
	/**
	 * la partie est gagnée, par l'adversaire
	 */
	private function loose(){

		this["p_mcPlayer1"].enabled = false;
		this["p_mcPlayer2"].enabled = false;
		p_btAntiClic._visible = true;
		p_btAntiClic.onRelease = function(){};
		gotoAndStop("win");
		
	}
}

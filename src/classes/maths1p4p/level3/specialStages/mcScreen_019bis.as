﻿/**
 * class maths1p4p.level3.specialStages.mcScreen_019bis
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level3.specialStages.mcScreen_019bis extends maths1p4p.level3.Level3AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var bt_subDir0, bt_subDir1, bt_subDir2, bt_subDir3:Button;


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** le code secret */
	private var p_aCode:Array;
	/** les 4 derniers chiffres donnés par le joueur */
	private var p_aUserCode:Array;
	

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_019bis()	{		
		
		super();
		
		Application.useLibCursor(bt_subDir0, "cursor-finger");
		bt_subDir0.onRelease = function(){
			_parent.gotoAndStop(1);
			_parent.addNumber(1);
		}	
		Application.useLibCursor(bt_subDir1, "cursor-finger");
		bt_subDir1.onRelease = function(){
			_parent.gotoAndStop(2);
			_parent.addNumber(2);
		}	
		Application.useLibCursor(bt_subDir2, "cursor-finger");
		bt_subDir2.onRelease = function(){
			_parent.gotoAndStop(3);
			_parent.addNumber(3);
		}	
		Application.useLibCursor(bt_subDir3, "cursor-finger");
		bt_subDir3.onRelease = function(){
			_parent.gotoAndStop(4);
			_parent.addNumber(4);
		}	
		
		p_aUserCode = [];
	
	}
	
	/**
	 * init
	 * Initialisation de la scène
	 * @param xmlNode noeud xml contenant toutes les infos de la scène
	 */
	public function init(StageNode:Object):Void{
		
		super.init(StageNode);
		
		p_aCode = _parent._parent.getPlayerState().state.waterFallSecretCode.__text.split(",");;		
				
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	private function addNumber(iNumber:Number){
		
		p_aUserCode.push(iNumber);
		if (p_aUserCode.length == 5){
			p_aUserCode.shift();
		}

		if(p_aCode.join("") == p_aUserCode.join("")){
			// ok, on ouvre la cascade !!!
			gotoAndStop("ouverture");
			_parent._parent.playSound("pontanim");
		}
		
		_parent._parent.playSound("manivelle");
		
	}
	
	/**
	 * animation1Completed
	 * appellé depuis le scénario
	 * l'animation est terminée
	 */
	private function animation1Completed(){
		gotoFrame("escalier");
	}
	
	/**
	 * animation2Completed
	 * appellé depuis le scénario
	 * l'animation est terminée
	 */
	private function animation2Completed(){
		_parent._parent.gotoScene("019");
	}
	
}

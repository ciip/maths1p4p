﻿/**
 * class maths1p4p.level3.specialStages.mcScreen_041
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
class maths1p4p.level3.specialStages.mcScreen_041 extends maths1p4p.level3.Level3AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var bt_subDir0:Button;

	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var p_oMachineState:Object;
	
	private var p_aObjects:Array;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_041()	{		
		
		super();
		
		p_oMachineState = _parent._parent.getPlayerState().state.machineSub3;
		
		// test si par hasard le sublevel est passé, mais les objets n'ont pas été mis sur la machine (via Merlin par exemple...)
		if (_parent._parent.getPlayerState().state.subLevel3Passed.__text == "1"){
			p_oMachineState.roue.__text="1";
			p_oMachineState.roueBleue.__text="1";
			p_oMachineState.morceauBois.__text="1";
			p_oMachineState.corde.__text="1";			
		}		
		
		trace(p_oMachineState.roue.__text);
		trace(p_oMachineState.roueBleue.__text);
		trace(p_oMachineState.morceauBois.__text);
		trace(p_oMachineState.corde.__text);

		this["mcRoue"]._visible = p_oMachineState.roue.__text=="1";
		this["mcRoue"]["id"] = "roue";
		this["mcRoueBleue"]._visible = p_oMachineState.roueBleue.__text=="1";
		this["mcRoueBleue"]["id"] = "roueBleue";
		this["mcMorceauBois"]._visible = p_oMachineState.morceauBois.__text=="1";
		this["mcMorceauBois"]["id"] = "morceauBois";
		this["mcCorde"]._visible = p_oMachineState.corde.__text=="1";
		this["mcCorde"]["id"] = "corde";
		
		p_aObjects = [this["mcMorceauBois"], this["mcRoueBleue"], this["mcRoue"], this["mcCorde"]];
		
	}
	
	/**
	 * clic sur l'un des 4 objets de l'interface
	 */
	public function clickOnObject(sMcName:String):Void{
				
		// check si les objets précédent sont en place
		var iObjectId:Number = Number(p_oMachineState[this[sMcName]["id"]]._id);
		var bOk:Boolean = true;
		
		for(var i=0; i<iObjectId; i++){
			if(p_aObjects[i]._visible == false){
				bOk = false;
			}
		}
	
		if(bOk){
			this[sMcName]._visible = true;
			p_oMachineState[this[sMcName]["id"]].__text = "1";
			_parent._parent.hideLoot(this[sMcName]["id"]);
			
			if(this["mcRoue"]._visible == true && this["mcRoueBleue"]._visible == true && this["mcMorceauBois"]._visible == true && this["mcCorde"]._visible == true ){			
				_parent._parent.setSubLevelPassed(3);
				gotoAndPlay("animation");
				_parent._parent.playSound("machine");
			}
		}
	
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	/**
	 * animationCompleted
	 * appellé depuis le scénario
	 * l'animation est terminée
	 */
	private function animationCompleted(){
		_parent._parent.gotoScene("040");
	}
	

	
}

﻿/**
 * class maths1p4p.level3.specialStages.mcScreen_070
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;


class maths1p4p.level3.specialStages.mcScreen_070 extends maths1p4p.level3.Level3AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_iCurrFrame:Number;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_070()	{		
		
		super();
		
	}
	
	public function init(StageNode:Object):Void{
		
		super.init(StageNode);
		p_iCurrFrame = 1;

		Application.useLibCursor(this["bt_subDir0"], "cursor-finger");
		this["bt_subDir0"].onRelease = function(){
			if(_parent._parent._parent.getPlayerState().state.animation1Viewed=="1"){
				_parent.gotoFrame("open");
			}else{
				_parent.gotoFrame(2);
			}			
		}
		
	}
	
	public function gotoFrame(iIdFrame:Number){
		
		super.gotoFrame(iIdFrame);

		p_iCurrFrame = iIdFrame;
		
		Application.useLibCursor(this["btRew"], "cursor-finger");
		this["btRew"].onRelease = function(){
			_parent.gotoFrame(1);
			_parent._parent._parent.playSound("diapo");
		}
		Application.useLibCursor(this["btPlay"], "cursor-finger");
		this["btPlay"].onRelease = function(){
			if(_parent.p_iCurrFrame < 9){
				_parent.gotoFrame(_parent.p_iCurrFrame+1);
				_parent._parent._parent.playSound("diapo");
			}
		}
		Application.useLibCursor(this["btStop"], "cursor-finger");
		this["btStop"].onRelease = function(){
			_parent._parent._parent.getPlayerState().state.animation1Viewed="1";
			_parent.gotoFrame("open");
			_parent._parent._parent.playSound("diapo");
		}
		
	}
	

	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------



	
}

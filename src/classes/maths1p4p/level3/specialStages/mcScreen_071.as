﻿/**
 * class maths1p4p.level3.specialStages.mcScreen_071
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;


class maths1p4p.level3.specialStages.mcScreen_071 extends maths1p4p.level3.Level3AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------


	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_071()	{		
		
		super();
		
	}
	
	public function init(StageNode:Object):Void{
		
		super.init(StageNode);
		
		this["mcPorte1"]._visible = false;
		this["mcPorte2"]._visible = false;
		
		this["bt_Dir0"]._visible = false;
		this["bt_Dir1"]._visible = false;

		Application.useLibCursor(this["mcCrane"], "cursor-finger");
		this["mcCrane"].onRelease = function(){
			this.gotoAndPlay(2);	
			_parent._parent._parent.playSound("porte6");
		}

		Application.useLibCursor(this["mcCornes"], "cursor-finger");
		this["mcCornes"].onRelease = function(){
			this.gotoAndPlay(2);	
			_parent.mcPorte1._visible = true;
			_parent["bt_Dir1"]._visible = true;
			_parent._parent._parent.playSound("porte5");
		}

		
	}

	
	public function animationCompleted():Void{
		this["mcPorte2"]._visible = true;
		this["bt_Dir0"]._visible = true;
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------



	
}

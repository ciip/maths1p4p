﻿/**
 * class maths1p4p.level3.specialStages.mcScreen_072
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level3.specialStages.mcScreen_072 extends maths1p4p.level3.Level3AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------


	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_072()	{		
		
		super();
		
	}
	
	public function init(StageNode:Object):Void{
		
		super.init(StageNode);
		
	}

	
	public function animationCompleted():Void{
		Application.useLibCursor(this["mcAnimFin"].mcBoutonSuite, "cursor-finger");
		this["mcAnimFin"].mcBoutonSuite.onRelease = function(){
			_parent._parent._parent._parent.gotoScene("073");
		}
		
	}
	
	/**
	 * clic sur l'un des objets de l'interface
	 */
	public function clickOnObject(sMcName:String):Void{
	
		if(sMcName == "mcSymbole"){
			gotoFrame(2);
			_parent._parent.hideLoot("symbole");
			Application.useLibCursor(this["mcAnimFin"].mcBoutonSuite, "cursor-finger");
			this["mcAnimFin"].mcBoutonSuite.onRelease = function(){
				this._parent.play();
			}
		}
	
	}	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------



	
}

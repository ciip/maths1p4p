﻿/**
 * class maths1p4p.level3.Level3AbstractStage
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level3.specialStages.mcScreen_201 extends maths1p4p.level3.Level3AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** le bouton retour */
	public var p_bBack:Button;
	
	/** le textfields code */
	public var p_txtCode:TextField;
	
	/** anticlic */
	public var p_btBG:Button;

	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** le code secret */
	private var p_iCode:Number;
	

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_201()	{		
		
		super();
		p_txtCode = this["txtCode"];
		
		p_txtCode.text = _parent._parent.getPlayerState().state.waterFallSecretCode.__text.split(",").join(" ");
		
		Application.useLibCursor(p_bBack, "cursor-finger");
		p_bBack.onRelease = function(){
			_parent._parent._parent.hideSpecialScreen();
		}		
	
		p_btBG.onPress = function(){};
		p_btBG.useHandCursor = false;
	
	}
	
	/**
	 * init
	 * Initialisation de la scène
	 * @param xmlNode noeud xml contenant toutes les infos de la scène
	 */
	public function init(StageNode:Object):Void{
		
		super.init(StageNode);
				
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	
	
	
}

﻿/**
 * class maths1p4p.level3.specialStages.mcScreen_203
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level3.specialStages.mcScreen_203 extends maths1p4p.level3.Level3AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	
	/** anticlic */
	public var p_btBG:Button;
	
	/** le bouton retour */
	public var p_btBack:Button;

	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_203()	{		
		
		super();		
	
		p_btBG.onPress = function(){};
		p_btBG.useHandCursor = false;
		
		Application.useLibCursor(p_btBack, "cursor-finger");
		p_btBack.onRelease = function(){
			_parent._parent._parent.hideSpecialScreen();
		}
		
		for(var i=0;i<9;i++){
			this["bt_message"+i]["id"] = i;
			Application.useLibCursor(this["bt_message"+i], "cursor-finger");
			this["bt_message"+i].onRelease = function(){
				_parent.showMessage(this["id"]);
			}
		}
		

				
	}
	
	public function showMessage(iIdMessage:Number):Void{
		
		var aMessages:Array = _parent._parent.getMessages();
		if(aMessages[iIdMessage].__text=="1"){
			// special pour bt_message1
			if(iIdMessage == 1){
				if(aMessages[iIdMessage+1].__text=="1"){
					if(aMessages[iIdMessage+2].__text=="1"){
						gotoAndStop(iIdMessage+2+2);
					}else{
						gotoAndStop(iIdMessage+2+1);
					}
				}else{
					gotoAndStop(iIdMessage+2);
				}
			}else{
				gotoAndStop(iIdMessage+2);
			}
		}else{
			gotoAndStop("nomessage")
		}
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	
	
	
}

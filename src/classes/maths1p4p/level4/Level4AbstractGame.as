﻿/**
 * class com.maths1p4p.level3.Level3AbstractStage
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.AbstractGame;
import maths1p4p.application.Application;

class maths1p4p.level4.Level4AbstractGame extends AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** les loots */
	public var p_mcLoots:MovieClip;
	
	/** ecran d'aide */
	public var p_mcHelp:MovieClip;
	
	/** planetes (parties gagnées */
	public var p_mcPlanets:MovieClip;
	

	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** le nombre de parties à gagner */
	private var p_iNbPartsToWin:Number = 4;
	/** le nombre de parties gagnées*/
	private var p_iNbPartsWon:Number = 0;
	/** le nombre d'essais à ne pas dépasser */
	private var p_iNbMaxTries:Number = 30;
	/** le nombre d'essais actuels du joueur*/
	private var p_iCurrNbTries:Number = 0;	
	/** id du joueur qui gagne le jeu 1 ou 2 */
	private var p_iPlayerWon:Number;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	
	/**
	 * Constructeur
	 */
	public function Level4AbstractGame()	{		
		
		super();	
		
		p_sSoundPath = Application.getRootPath() + "data/sound/level4/";
		
		Application.useLibCursor(p_mcHelp, "cursor-finger");
		p_mcHelp._visible = false;
		p_mcHelp.onRelease = function(){
			_parent.hideHelp();
		}		
		
		// affichage du nom du jeu 
		if (_parent._parent.p_oCurrGame.done.__text == "0"){
			var sIndication:String = "\n(à faire)";
			var finished:Boolean = false;
		}else{
			var sIndication:String = "\n(terminé)";
			var finished:Boolean = true;
		}			
		_parent._parent.p_oInterface.showGameName(_parent._parent.p_oCurrGame.name.__text + sIndication, finished);	
		
		// swap de l'aide
		p_mcHelp.swapDepths(1000);
		
		// cache les planetes
		for (var i in p_mcPlanets){
			p_mcPlanets[i]._visible = false;
		}

	}
	
	
	/**
	 * renvois le loot à ganger
	 */
	public function getLootToWin():String{
		return _parent._parent.getLootToWinForCurrGame();
	}	
	
	
	/**
	 * le joueur a ramassé l'objet à gagner
	 */
	public function getLoot(){
		
		p_oSoundPlayer.playASound("getloot",0);
		
		if(p_iPlayerWon == 1){
			
			// signale au niveau que le jeu est terminé
			_parent._parent.currGameIsDone();
			
			// ajoute l'objet à l'inventaire
			_parent._parent.getCurrLoot(p_mcLoots.p_sCurrLootName);
			
		}
		
		gotoAndStop("end");
		
		// le jeu est terminé
		gameFinished();
		
	}
	
	/**
	 * affiche l'écran d'aide
	 */
	public function showHelp():Void{
		p_mcHelp._visible = true;
	}
	
	/**
	 * cache l'écran d'aide
	 */
	public function hideHelp():Void{
		p_mcHelp._visible = false;
		_parent._parent.hideHelp();
	}
	
	/**
	 * la sous partie est gagnée
	 */
	public function partWon(){		
		p_iNbPartsWon++;
		p_mcPlanets["mcPlanet"+(p_iNbPartsWon-1)]._visible = true;
		if(p_iNbPartsWon == p_iNbPartsToWin){			
			won();
		}else{
			p_oSoundPlayer.playASound("partwon",0);
		}
	}	
	
	/**
	 * la partie est gagnée
	 */
	public function won(idPlayer:Number){	
		p_oSoundPlayer.playASound("won",0);
		if(idPlayer==null) idPlayer=1;
		p_iPlayerWon = idPlayer;
		gotoAndStop("win");
		p_mcLoots.show();
	}

	/**
	 * la partie est perdue
	 * @param msg le message a afficher
	 */
	private function loose(sMsg:String){
		
		gotoAndStop("loose");
		
	}	
	
	
	/**
	 * démarrage d'une partie
	 * à implémenter
	 */
	private function nextPart(){
		
	}

	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	
	
	
}

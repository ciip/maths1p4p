﻿/**
 * class com.maths1p4p.level3.Level3Interface
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;
import mx.accessibility.ListAccImpl;
import mx.controls.List;
import mx.utils.Delegate;

class maths1p4p.level4.Level4Interface extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var p_mcGameInterface:MovieClip;
	public var p_mcMainInterface:MovieClip;
	public var p_mcMessages:MovieClip;

	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_oLoots:Object;
	private var p_oObjects:Object;

	private var p_iCurrMessageId:Number;	
	
	private var p_aPinkMenuContent:Array = ["salon", "cheminée", "télescope", "champignons", "gare", "exploitation", "couloir beiige", "maison bleue", "fusée"];
	private var p_aPinkMenuData:Array = ["001", "010", "017", "019", "031", "036", "052", "057", "064"];
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level4Interface()	{		
		
		// initialisation de la liste des loots
		p_oLoots = new Object();
		for(var i=0;i<4;i++){
			for(var j=0;j<4;j++){
				p_oLoots["loot_"+i+"_"+j] = p_mcMainInterface["mcLoot_"+i+"_"+j];
				p_oLoots["loot_"+i+"_"+j]["i"] = i;
				p_oLoots["loot_"+i+"_"+j]["j"] = j;
				p_oLoots["loot_"+i+"_"+j]._visible = false;
				Application.useLibCursor(p_oLoots["loot_"+i+"_"+j], "cursor-finger");
				p_oLoots["loot_"+i+"_"+j].onRelease = function(){
					_parent._parent._parent._parent.p_mcScene.scene.mcMachine.addLoot(this["i"],this["j"]);			
				}
			}
		}
		
		// initialisation de la liste des objets
		p_oObjects = new Object();
		p_oObjects.potion = p_mcMainInterface["mcPotion"];
				
		hideAllLoots();
		hideAllObjects();
	
		// initialisation des boutons
		initAllButtons();
		
		// init des actions sur objets
		
		p_mcMainInterface["mcPotion"]._visible = false;
		Application.useLibCursor(p_mcMainInterface["mcPotion"], "cursor-finger");
		p_mcMainInterface["mcPotion"].onRelease = function(){
			_parent._parent._parent._parent.p_mcScene.scene.clickOnPotion();
		}
				
		currentInterface = "main";
		
		this.onEnterFrame = function(){
			p_mcMainInterface["mcPinkMenu"]._visible = false;
			p_mcMainInterface["mcPinkMenu"].addEventListener("change", Delegate.create(this, onPinkMenuClick));			
			delete this.onEnterFrame;
		}
	
	}

	public function screenIsOpen(){
		Application.useLibCursor(p_mcMessages.btStop, "cursor-finger");
		p_mcMessages.btStop.onRelease = function(){
			_parent._parent.closeMessage();
		}
	}
	public function openMessage(idMessage:Number){
		p_iCurrMessageId = idMessage;
		p_mcMessages.gotoAndPlay(2);
	}
	public function closeMessage(idMessage:Number){
		p_mcMessages.gotoAndPlay(18);
	}	
	
	/**
	 * setPlayerName
	 * @param sPlayerName le nom du joueur
	 * affiche le nom du joueur
	 */
	public function setPlayerName(sPlayerName:String):Void{		
		p_mcMainInterface["p_txtPlayerName"].text = sPlayerName;		
		p_mcGameInterface["p_txtPlayerName"].text = sPlayerName;		
	}
		
	/**
	 * showLoot
	 * @param sLoot le nom du loot
	 * affiche le loot à son emplacement prévu
	 */
	public function showLoot(sLoot:String):Void{
		trace(p_oLoots[sLoot]);
		p_oLoots[sLoot]._visible = true;
	}
		
	/**
	 * hideLoot
	 * @param sLoot le nom du loot
	 * cache le loot 
	 */
	public function hideLoot(sLoot:String):Void{
		p_oLoots[sLoot]._visible = false;
	}
	
		
	/**
	 * hideObject
	 * @param sObject le nom de l'objet
	 * cache l'objet
	 */
	public function hideObject(sObject:String):Void{
		p_oObjects[sObject]._visible = false;
	}
	
	/**
	 * showObject
	 * @param sObject le nom de l'objet
	 * affiche l'objet à son emplacement prévu
	 */
	public function showObject(sObject:String):Void{
		p_oObjects[sObject]._visible = true;
	}
	
	/**
	 * showGameName
	 * @param sGameName le nom du jeu
	 * affiche le nom du jeu en cours
	 */
	public function showGameName(sGameName:String, bFinished:Boolean):Void{
		p_mcMainInterface["p_txtGameName"].text = sGameName;
		p_mcGameInterface["p_txtGameName"].text = sGameName;
		if(bFinished){
			p_mcMainInterface["p_mcGameState"].gotoAndStop(2);
			p_mcGameInterface["p_mcGameState"].gotoAndStop(2);
		}
	}
	
	/**
	 * hideGameName
	 * @param sGameName le nom du jeu
	 * affiche le nom du jeu en cours
	 */
	public function hideGameName():Void{
		p_mcMainInterface["p_txtGameName"].text = "";
		p_mcMainInterface["p_mcGameState"].gotoAndStop(1);
		p_mcGameInterface["p_txtGameName"].text = "";
		p_mcGameInterface["p_mcGameState"].gotoAndStop(1);
	}
	
	
	/**
	 * addObject
	 * @param sObject le nom de l'objet
	 * ajoute un objet à l'inventaire
	 */
	public function addObject(sObject:String):Void{		
		showObject(sObject);		
	}	
	
	
	/**
	 * addLoot
	 * @param sLoot le nom du loot
	 * ajoute un loot à l'inventaire
	 */
	public function addLoot(sLoot:String):Void{		
		trace("addLoot  " + sLoot);
		showLoot(sLoot);
	}		
	
	/**
	 * init des elements visibles
	 */
	public function initElements(xmlElements:XMLNode):Void{
		
		hideAllLoots();
		hideAllObjects();

		// loots
		for(var i=0;i<xmlElements["$loot"].length;i++){
			p_oLoots[String(xmlElements["$loot"][i].__text)]._visible = true;
		}
		// objets
		for(var i=0;i<xmlElements["$object"].length;i++){
			p_oObjects[xmlElements["$object"][i].__text]._visible = true;
		}
		
	}
	
	/**
	 * renvois les elements actuellement visibles
	 */
	public function getElementsVisible():XMLNode{

		var xmlRes:XMLNode = new XMLNode(1, "interface");
		
		// loots
		for(var i in p_oLoots){
			if(p_oLoots[i]._visible){
				var vNode:XMLNode = new XMLNode(1, "loot");
				var txtNode:XMLNode = new XMLNode(3, i);
				vNode.appendChild(txtNode);
				xmlRes.appendChild(vNode);
			}
		}

		// objets		
		for(var i in p_oObjects){
			if(p_oObjects[i]._visible){
				var vNode:XMLNode = new XMLNode(1, "object");
				var txtNode:XMLNode = new XMLNode(3, i);
				vNode.appendChild(txtNode);
				xmlRes.appendChild(vNode);
			}
		}

		return xmlRes;
		
	}
	
	/**
	 * freeze quit buttons interactions
	 */	
	public function freezeQuitButtons(val:Boolean):Void{
		p_mcMainInterface["btQuit"].enabled = val;
		p_mcMainInterface["btChangeUser"].enabled = val;
		p_mcGameInterface["btQuit"].enabled = val;
		p_mcGameInterface["btChangeUser"].enabled = val;		
	}	
	
	/**
	 * switch current visible interface
	 */	
	public function set currentInterface(sCurrInterface:String):Void{
		p_mcGameInterface._visible = sCurrInterface == "game";
		p_mcMainInterface._visible = sCurrInterface == "main";
	}
	
	public function openPinkMenu():Void{
		p_mcMainInterface["mcPinkMenu"].selectedIndex = undefined;
		p_mcMainInterface["mcPinkMenu"]._visible = true;	
		this.onMouseUp = function(){
			closePinkMenu();
		}
	}
	public function closePinkMenu():Void{
		p_mcMainInterface["mcPinkMenu"]._visible = false;
		delete this.onMouseUp;
	}
	
	public function setPinkMenu(sublevel:Number){
		var id:Number;
		switch(sublevel){
			case 1:
				id = 2;
				break;
			case 2:
				id = 4;
				break;
			case 3:
				id = 6;
				break;
			case 4:
				id = 7;
				break;
			case 5:
				id = 8;
				break;				
			default:
				id = 8;
		}
		p_mcMainInterface["mcPinkMenu"].removeAll();
		for(var i=0; i<=id; i++){
			p_mcMainInterface["mcPinkMenu"].addItem({label:p_aPinkMenuContent[i], data:p_aPinkMenuData[i]});
		}
	}

	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	private function initAllButtons():Void{
		
		Application.useLibCursor(p_mcMainInterface["p_mcManette"], "cursor-finger");
		p_mcMainInterface.p_mcManette.onPress = function(){
			gotoAndStop(2);
		}
		p_mcMainInterface.p_mcManette.onReleaseOutside = function(){
			gotoAndStop(1);
		}
		p_mcMainInterface.p_mcManette.onRelease = function(){
			_parent._parent._parent._parent.goBack();
			gotoAndStop(1);
		}
		
		// bouton temporaire
		this["btWinGame"].useHandCursor = false;
		this["btWinGame"].onRelease = function(){
			if(Key.isDown(Key.SHIFT)){
				_parent._parent._parent.p_mcGame.mcAnimation.won();
			}
		}
		
		// MAIN INTERFACE
		// bouton messages
		Application.useLibCursor(p_mcMainInterface["bt_messages"], "cursor-finger");
		p_mcMainInterface["bt_messages"].onRelease = function(){
			_parent._parent._parent._parent.showSpecialScreen("mcScreen_201_messages");
		}		
		// argent
		Application.useLibCursor(p_mcMainInterface["btArgent"], "cursor-finger");
		p_mcMainInterface["btArgent"].onRelease = function(){
			_parent._parent._parent._parent.showSpecialScreen("mcScreen_202_resultats");
		}			
		// quitter
		Application.useLibCursor(p_mcMainInterface["btQuit"], "cursor-finger");
		p_mcMainInterface["btQuit"].onRelease = function(){
			_parent._parent._parent._parent.askForQuit();
		}		
		// change user
		Application.useLibCursor(p_mcMainInterface["btChangeUser"], "cursor-finger");
		p_mcMainInterface["btChangeUser"].onRelease = function(){
			_parent._parent._parent._parent.changePlayer();
		}
		// pink menu
		Application.useLibCursor(p_mcMainInterface["btPinkMenu"], "cursor-finger");
		p_mcMainInterface["btPinkMenu"].onRelease = function(){
			trace(_parent._parent.openPinkMenu);
			_parent._parent.openPinkMenu();			
		}
		
		// GAME INTERFACE
		// bouton retour
		Application.useLibCursor(p_mcGameInterface["btRetour"], "cursor-finger");
		p_mcGameInterface["btRetour"].onRelease = function(){
			_parent._parent._parent._parent.leaveGame();
		}		
		// bouton reload
		Application.useLibCursor(p_mcGameInterface["btReload"], "cursor-finger");
		p_mcGameInterface["btReload"].onRelease = function(){
			_parent._parent._parent._parent.reLoadGame();
		}		
		// bouton aide
		Application.useLibCursor(p_mcGameInterface["btAide"], "cursor-finger");
		p_mcGameInterface["btAide"].onRelease = function(){
			_parent._parent._parent._parent.showHelp();
		}					
		// quitter
		Application.useLibCursor(p_mcGameInterface["btQuit"], "cursor-finger");
		p_mcGameInterface["btQuit"].onRelease = function(){
			_parent._parent._parent._parent.askForQuit();
		}		
		// change user
		Application.useLibCursor(p_mcGameInterface["btChangeUser"], "cursor-finger");
		p_mcGameInterface["btChangeUser"].onRelease = function(){
			_parent._parent._parent._parent.changePlayer();
		}		
		
	}
	
	private function hideAllLoots():Void{
		for(var i in p_oLoots){
			p_oLoots[i]._visible = false;
		}
	}
	
	private function hideAllObjects():Void{
		for(var i in p_oObjects){
			p_oObjects[i]._visible = false;
		}
	}
	
	private function onPinkMenuClick(){
		_parent._parent.gotoScene(p_mcMainInterface["mcPinkMenu"].selectedItem.data);
		trace(p_mcMainInterface["mcPinkMenu"].selectedItem.data);
	}
	
}

﻿/**
 * class com.maths1p4p.level4.Level4Merlin
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import maths1p4p.application.AbstractStorage;
import maths1p4p.level3.Level3Player;
import maths1p4p.AbstractPlayer;
import mx.controls.ComboBox;
import mx.utils.Delegate;
import mx.controls.Alert;
import maths1p4p.utils.OutilsTableaux;

class maths1p4p.level4.Level4Merlin extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var p_btMiseAZero:Button;

	public var comboJoueur:ComboBox;
	public var comboAcces:ComboBox;
	
	public var p_mcModeRadio:MovieClip;
	
	public var bt_quit:Button;
	public var bt_changeUser:Button;	


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_oStorage:AbstractStorage;
	
	private var p_aPlayers:Array;
	
	private var p_xmlCurrState:XML;
	
	private var p_iCurrPlayerId:Number;
	
	private var p_oPlayer:Object;
	
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level4Merlin()	{	
		
		// cache les clips jeux
		clearMcGames();
		
		p_mcModeRadio.gotoAndStop(1);
		p_mcModeRadio["p_btAccesDirect"].onRelease = function(){
			_parent.gotoAndStop(1);
		}
		p_mcModeRadio["p_btGain"].onRelease = function(){
			_parent.gotoAndStop(2);
		}
		
		this.onEnterFrame = function(){
			comboAcces.addEventListener("change", Delegate.create(this, onSelectScene));	
			delete this.onEnterFrame;
		}
		
		// init des boutons		
		Application.useLibCursor(p_btMiseAZero, "cursor-finger");
		p_btMiseAZero.onRelease = function(){
			_parent.clearPlayer();
		}
		
		Application.useLibCursor(bt_quit, "cursor-finger");
		bt_quit.onRelease = function(){
			_parent.quit();
		}
		
		Application.useLibCursor(bt_changeUser, "cursor-finger");
		bt_changeUser.onRelease = function(){
			_parent.changePlayer();
		}
				
		p_oStorage = Application.getInstance().getStorage();
		p_oStorage.addEventListener("onLoadPlayersList", this);
		p_oStorage.loadPlayersList(4, true);	
		
	}

	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------	
	

	private function changePlayer():Void{
		_parent._parent.changePlayer();
		_parent.hide();
	}
		
	private function quit(){
		_parent._parent.unload();
	}	
	
	private function onSelectScene(event:Object){
		
		var sSelect:String = comboAcces.selectedItem.data;
		if(sSelect == "resultats"){
			_parent.gotoAndStop(1);
		}else if(sSelect == "classes"){
			_parent.gotoAndStop(2);
		}else{
			
			var bOK:Boolean = true;			
			switch(sSelect){				
				case "27":
					if( !(Number(p_xmlCurrState["currSubLevel"].__text) >= 2) ) bOK = false;
					break;
				case "19":
					if( !(Number(p_xmlCurrState["currSubLevel"].__text) >= 2) ) bOK = false;
					break;
				case "040":
					if( !(Number(p_xmlCurrState["currSubLevel"].__text) >= 3) ) bOK = false;
					break;
				case "048":
					if( !(Number(p_xmlCurrState["currSubLevel"].__text) >= 4) ) bOK = false;
					break;
				case "057":
					if( !(Number(p_xmlCurrState["currSubLevel"].__text) >= 5) ) bOK = false;
					break;
				
			}
			
			if(bOK){
				_parent._parent.gotoScene(sSelect);
				// on quitte l'écran merlin
				_parent.hide();
			}else{
				trace("Ce niveau est inaccessible");
				//Alert.show("Ce niveau est inaccessible");
			}
								
		}
		
	}
	
	private function onLoadPlayersList (event:Object) {
		
		p_oStorage.removeEventListener("onLoadPlayersList", this);

		p_aPlayers = event.data.arrPlayersList;

		if (p_aPlayers != undefined) {
			
			comboJoueur.removeAll();
			comboJoueur.addEventListener("change", Delegate.create(this, onPlayerChange));
			
			for(var iPlayer:Number=0; iPlayer<p_aPlayers.length; iPlayer++){
				comboJoueur.addItem(p_aPlayers[iPlayer]._name, iPlayer);	
			}
			
		} else {
			trace("erreur ! ");
		}
		
		// charge le premier player de la liste
		loadPlayer(p_aPlayers[0]._idPlayer);
		
	}
		
	private function getCurrMode():String{
		if (p_mcModeRadio._currentframe == 1){
			return "acces";
		}else{
			return "gain"
		}
	}

	private function onPlayerChange(){		
		var idJoueur:Number = comboJoueur.selectedItem.data;
		loadPlayer(p_aPlayers[idJoueur]._idPlayer);		
	}
	
	private function loadPlayer(idPlayer:Number):Void{
		p_iCurrPlayerId = idPlayer;
		p_oStorage.addEventListener("onLoadPlayer", this);
		p_oStorage.loadPlayer(idPlayer);
	}
	
	private function onLoadPlayer(dataPlayer){		
		p_oStorage.removeEventListener("onLoadPlayer", this);
		p_oPlayer = dataPlayer.data.oPlayer;
		if(p_oPlayer.getState()["state"].childNodes.length == 0 ){
			// il s'agit d'un nouveau jour, on charge les données par défaut
			var x:XML = new XML();
			x.ignoreWhite = true;
			x["instance"] = this;
			x.onLoad = function(){
				this["instance"].initNewPlayer(this);
			}
			x.load(_parent._parent.p_sLevelFolder + "data/playerData.xml")
		}else{
			initPlayer(p_oPlayer);
		}
		
	}
	
	private function initNewPlayer(xmlState:XML):Void{
		trace("choing");
		// code cheminée
		var aCode:Array = [Math.floor(Math.random()*10),Math.floor(Math.random()*10),Math.floor(Math.random()*10)];		
		xmlState["state"].chemineeSecretCode.__text = aCode.join(",");
		// codes couleurs
		var aColors:Array = ["bleu","jaune","vert"];
		xmlState["state"]["colors"].$color[0].__text = aColors[Math.floor(Math.random()*3)];
		xmlState["state"]["colors"].$color[1].__text = aColors[Math.floor(Math.random()*3)];
		xmlState["state"]["colors"].$color[2].__text = aColors[Math.floor(Math.random()*3)];
		xmlState["state"]["colors"].$color[3].__text = aColors[Math.floor(Math.random()*3)];
		p_oPlayer.setState(xmlState);
		initPlayer(p_oPlayer);

	}	
	
	private function initPlayer(player):Void{
		
		p_xmlCurrState = player.getState().state;
		//trace(p_xmlCurrState);
		clearMcGames();
		
		// affichage des jeux		
		var iMaxi:Number = Number(p_xmlCurrState["currSubLevel"].__text);
		
		for(var i=0; i<iMaxi; i++){
			// ==> gamePlaces

			for(var j=0; j<p_xmlCurrState["$gamePlaces"][i].$gamePlace.length; j++){
				// ==> games
				if(p_xmlCurrState["$gamePlaces"][i].$gamePlace[j].idGame.__text == "-1"){
					this["mcGame_"+i+"_"+j]._visible = false;
				}else{
					if(i<iMaxi-1){
						// test si le jeu est fait, dans un sous niveau terminé
						var idGame:String = p_xmlCurrState["$gamePlaces"][i].$gamePlace[j].idGame.__text;
						if(p_xmlCurrState["games"].$game[idGame].done.__text == "0"){
							this["mcGame_"+i+"_"+j]._visible = false;
						}else{
							this["mcGame_"+i+"_"+j]._visible = true;
							this["mcGame_"+i+"_"+j]["idGame"] = p_xmlCurrState["$gamePlaces"][i].$gamePlace[j].idGame.__text;	
							this["mcGame_"+i+"_"+j]["sublevel"] = i;	
							this["mcGame_"+i+"_"+j]["idGamePlace"] = j;	
							this["mcGame_"+i+"_"+j]["idScene"] = p_xmlCurrState["$gamePlaces"][i].$gamePlace[j].idScene.__text;	
						}
					}else{
						if( i>0 && p_xmlCurrState["$machine"][i-1].done.__text == "0"){
							this["mcMachine"+(i-1)]._visible = true;
						}else{
							this["mcGame_"+i+"_"+j]._visible = true;
							this["mcGame_"+i+"_"+j]["idGame"] = p_xmlCurrState["$gamePlaces"][i].$gamePlace[j].idGame.__text;					
							this["mcGame_"+i+"_"+j]["sublevel"] = i;	
							this["mcGame_"+i+"_"+j]["idGamePlace"] = j;	
							this["mcGame_"+i+"_"+j]["idScene"] = p_xmlCurrState["$gamePlaces"][i].$gamePlace[j].idScene.__text;							
						}
					}
				}				
			}
						
		}
		
		if(iMaxi==5){
			if( p_xmlCurrState["$machine"][3].done.__text == "0"){
				this["mcMachine"+3]._visible = true;
			}
		}
		
		// affichage des gains
		this["txtObjSuppl"].text = p_xmlCurrState["objsuppl"].__text;
		
		if(p_oPlayer.getId() != _parent._parent.getPlayer().getId()){
			_parent._parent.launchLevel(p_oPlayer);
		}
		_parent._parent.showInterface();
		
	}
	
	private function selectGame(mcGame:MovieClip):Void{
		
		if (getCurrMode() == "acces"){
			// on va à la scène précédent le jeu
			_parent._parent.gotoScene(mcGame["idScene"]);
			// on lance le jeu			
			_parent._parent.levelLoadGame(mcGame["idGame"], mcGame["sublevel"]);
			// on quitte l'écran merlin
			_parent.hide();
			
		}else{
			if(p_xmlCurrState["games"].$game[mcGame["idGame"]].done.__text == "0"){
				// gain du jeu
				_parent._parent.p_oCurrGame = p_xmlCurrState["games"].$game[mcGame["idGame"]];
				var sLoot = _parent._parent.getNextLootToWin();
				_parent._parent.getCurrLoot(sLoot);			
				// sauvegarde du joueur
				//_parent._parent.savePlayer();				
				initPlayer(_parent._parent.getPlayer());	
			}
		}
		
	}
	
	private function rollOverGame(idGame):Void{
		
		var sGameName:String = p_xmlCurrState["games"].$game[idGame].name.__text;
		if(p_xmlCurrState["games"].$game[idGame].done.__text == "0"){
			// à faire
			var sGameState:String = "(à faire)";
		}else{
			// terminé
			var sGameState:String = "(terminé)";
		}
		
		this["txtRollover"].text = sGameName + "\n" + sGameState;
		
	}
	
	private function rollOutGame():Void{
		this["txtRollover"].text = "";
	}
	
	private function machineClick(){
		
		_parent._parent.p_oInterface.hideAllLoots();		
		p_xmlCurrState["$machine"] [ Number(p_xmlCurrState["currSubLevel"].__text) -2 ].$loot[0].__text = "1";
		p_xmlCurrState["$machine"] [ Number(p_xmlCurrState["currSubLevel"].__text) -2 ].$loot[1].__text = "1";
		p_xmlCurrState["$machine"] [ Number(p_xmlCurrState["currSubLevel"].__text) -2 ].$loot[2].__text = "1";
		p_xmlCurrState["$machine"] [ Number(p_xmlCurrState["currSubLevel"].__text) -2 ].$loot[3].__text = "1";
		p_xmlCurrState["$machine"] [ Number(p_xmlCurrState["currSubLevel"].__text) -2 ].done.__text = "1";
		initPlayer(_parent._parent.getPlayer());
		
	}
	
	private function clearMcGames():Void{
		
		for(var i=0; i<4; i++){
			for(var j=0; j<8; j++){
				this["mcGame_"+i+"_"+j]._visible = false;
				this["mcGame_"+i+"_"+j].onRollOver = function(){
					_parent.rollOverGame(this["idGame"]);
				}
				this["mcGame_"+i+"_"+j].onRollOut = this["mcGame_"+i+"_"+j].onReleaseOutside = function(){
					_parent.rollOutGame();
				}
				this["mcGame_"+i+"_"+j].onRelease = function(){
					_parent.selectGame(this);
				}
			}
			
		}
		
		// machines
		for(var i=0;i<4;i++){
			this["mcMachine"+i]._visible = false;
			this["mcMachine"+i].txt.text = "Machine "+ String(i+1);
			this["mcMachine"+i].onRelease = function(){
				_parent.machineClick();
			}
			
		}
		
	}
	
	private function clearPlayer():Void{
		
		var x:XML = new XML();
		x.ignoreWhite = true;
		x["instance"] = this;
		x.onLoad = function(){
			this["instance"].setPlayerState(this);
		}
		x.load(_parent._parent.p_sLevelFolder + "data/playerData.xml")
		
	}
	
	private function setPlayerState(xmlState:XML):Void{
				
		// code cheminée
		var aCode:Array = [Math.floor(Math.random()*10),Math.floor(Math.random()*10),Math.floor(Math.random()*10)];		
		xmlState["state"].chemineeSecretCode.__text = aCode.join(",");
		// codes couleurs
		var aColors:Array = ["bleu","jaune","vert"];
		xmlState["state"]["colors"].$color[0].__text = aColors[Math.floor(Math.random()*3)];
		xmlState["state"]["colors"].$color[1].__text = aColors[Math.floor(Math.random()*3)];
		xmlState["state"]["colors"].$color[2].__text = aColors[Math.floor(Math.random()*3)];
		xmlState["state"]["colors"].$color[3].__text = aColors[Math.floor(Math.random()*3)];
		
		p_xmlCurrState = xmlState["state"];
		
		_parent._parent.getPlayer().setState(xmlState);
		initPlayer(_parent._parent.getPlayer());
		
		// init de l'interface
		_parent._parent.initInterface();
		
		// sauvegarde du joueur
		_parent._parent.savePlayer();
		
	}
	
}

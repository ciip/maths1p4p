﻿/**
 * class com.maths1p4p.level4.Level4PlayerSelectionScreen
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import mx.containers.Window;
import mx.utils.Delegate;
import mx.controls.Alert;
import mx.controls.List;

import XMLShortcuts;

import maths1p4p.utils.OutilsTableaux;

import maths1p4p.application.Application;
import maths1p4p.application.AbstractStorage;
import maths1p4p.level4.Level4Player;

import com.prossel.InputBox;


class maths1p4p.level4.Level4PlayerSelectionScreen extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	/** bouton servant à désactiver les clics sur les écrans au dessous */
	public var p_btBG:Button;

	/** composant liste, liste des joueurs */
	public var p_lstPlayers:List;
	
	/** boutons */
	public var p_btStart:Button;
	public var p_btInfo:Button;
	public var p_btQuit:Button;
	public var p_btResultats:Button;
	
	
	/** popup */
	public var p_mcPopup:MovieClip;
	
	/** manual */
	public var p_mcManual:MovieClip;
	

	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	private var p_oStorage:AbstractStorage;
	private var p_oPlayer:Level4Player;
	/** le 1er mot de passe saisi, avant la demande de confirmation */
	private var p_s1stPass:String
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Level4PlayerSelectionScreen()	{		
		
		_visible = false;
		
		// bouton servant à désactiver les clics sur les écrans au dessous
		p_btBG.useHandCursor = false;
		p_btBG.onRelease = function(){}
		
		Application.useLibCursor(p_btStart, "cursor-finger");
		p_btStart.onRelease = function(){
			_parent.login();
		}		
		
		Application.useLibCursor(p_btInfo, "cursor-finger");
		p_btInfo.onRelease = function(){
			_parent.p_mcManual.show();
		}
		
		Application.useLibCursor(p_btQuit, "cursor-finger");
		p_btQuit.onRelease = function(){
			_parent.quit();
		}
		
		Application.useLibCursor(p_btResultats, "cursor-finger");
		p_btResultats.onRelease = function(){
			_parent.showResults();
		}				
		
		p_oStorage = Application.getInstance().getStorage();
		
		this.onEnterFrame = function(){
			p_lstPlayers.addEventListener("change", Delegate.create(this, onPlayerChange));
			delete this.onEnterFrame;
		}
			
	}
	
	/**
	 * show
	 * affiche l'écran de sélection
	 */
	public function show(){
		_visible = true;				
		p_oStorage.addEventListener("onLoadPlayersList", this);
		p_oStorage.addEventListener("onLoadPlayer", this);
		p_oStorage.addEventListener("onLoginPlayer", this);
		p_oStorage.addEventListener("onChangePlayerPassword", this);
		p_oStorage.loadPlayersList(4);		
	}
	
	/**
	 * hide
	 * cache l'écran de sélection
	 */
	public function hide(){
		p_oStorage.removeEventListener("onLoadPlayersList", this);
		p_oStorage.removeEventListener("onLoadPlayer", this);
		p_oStorage.removeEventListener("onLoginPlayer", this);
		p_oStorage.removeEventListener("onChangePlayerPassword", this);
		_visible = false;		
	}
	
	
	public function checkPassword(evt:Object):Void{		
		p_oStorage.loginPlayer(p_lstPlayers.selectedItem.data, evt.text);		
	}
	
	public function setCurrPlayerPassword(txtPass:String):Void{
		p_oPlayer.changePassword( "", txtPass );
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	private function quit():Void{
		_parent.askForQuit();
	}
	
	private function launchCurrPlayer():Void{
		
		if(p_oPlayer.getState()["state"].childNodes.length == 0 ){
			// il s'agit d'un nouveau jour, on charge les données par défaut
			var x:XML = new XML();
			x.ignoreWhite = true;
			x["instance"] = this;
			x.onLoad = function(){
				this["instance"].initNewPlayer(this);
			}
			x.load(_parent.p_sLevelFolder + "data/playerData.xml")
		}else{
			// le player est chargé, on peut lancer le niveau
			_parent.launchLevel(p_oPlayer);
		}
		
		hide();
		
	}
	
	private function login():Void{
		
		if(p_oPlayer != null){
			if (p_oPlayer.getPasswordSet()){
				var w:Window = InputBox.show("Introduis ton mot de passe", "", "Connexion joueur", this, undefined, true);
				w.addEventListener("ok", Delegate.create(this, checkPassword));
			}else{
				var w = InputBox.show("Définis ton mot de passe", "", "Connexion joueur", this, undefined, true);
				w.addEventListener("ok", Delegate.create(this, askSecondPass));
			}
		}else{
			Alert.show("Clique d'abord sur ton nom");
		}
		
	}
	
	public function askSecondPass(evt){
		p_s1stPass = evt.text;
		var w = InputBox.show("Confirme ton mot de passe", "", "Connexion joueur", this, undefined, true);
		w.addEventListener("ok", Delegate.create(this, checkSecondPass));
	}
	
	public function checkSecondPass(evt){
		if(p_s1stPass == evt.text){
			setCurrPlayerPassword(p_s1stPass);
		}else{
			Alert.show("Les deux mots sont différents...");
		}
	}
	

	private function showResults():Void{
		_parent.showResults();
	}


	private function onLoadPlayersList (event:Object) {
		
		var arrPlayers:Array = event.data.arrPlayersList;

		if (arrPlayers != undefined) {
			p_lstPlayers.removeAll();

			// récupère les noms des classes dans le xml et les affiche dans la liste
			for(var iPlayer:Number=0; iPlayer<arrPlayers.length; iPlayer++){
				var xmlPlayer = arrPlayers[iPlayer];
				p_lstPlayers.addItem({label: xmlPlayer._name, data: xmlPlayer._idPlayer});
			}

		} else {
			p_lstPlayers.addItem({label: "Erreur"});
		}
		
	}

	private function onLoadPlayer (event:Object) {

		p_oPlayer = event.data.oPlayer;
		p_btStart.enabled = true;

	}
	
	private function initNewPlayer(xmlState:XML):Void{
		
		// code cheminée
		var aCode:Array = [Math.floor(Math.random()*10),Math.floor(Math.random()*10),Math.floor(Math.random()*10)];		
		xmlState["state"].chemineeSecretCode.__text = aCode.join(",");
		// codes couleurs
		var aColors:Array = ["bleu","jaune","vert"];
		xmlState["state"]["colors"].$color[0].__text = aColors[Math.floor(Math.random()*3)];
		xmlState["state"]["colors"].$color[1].__text = aColors[Math.floor(Math.random()*3)];
		xmlState["state"]["colors"].$color[2].__text = aColors[Math.floor(Math.random()*3)];
		xmlState["state"]["colors"].$color[3].__text = aColors[Math.floor(Math.random()*3)];
		p_oPlayer.setState(xmlState);
		_parent.launchLevel(p_oPlayer);

	}


	private function onSave (event:Object) {
		if (event.data.result == 0)
			Alert.show("Sauvegarde effectuée avec succès", "Sauvegarde du joueur", Alert.OK, null, null, null, Alert.OK);
		else
			Alert.show("Erreur: " + event.data.error, "Sauvegarde du joueur", Alert.OK, null, null, null, Alert.OK);

		// reload player
		p_oStorage.loadPlayer(p_oPlayer.getId());
		
	}

	private function onPlayerChange(event:Object) {
		p_oPlayer = null;
		p_btStart.enabled = false;
		p_oStorage.loadPlayer(p_lstPlayers.selectedItem.data);
	}


	private function onLoginPlayer (event:Object) {
		if (event.data.result == 0){
			// password ok, on lance le level
			launchCurrPlayer();
		}else{
			Alert.show("Erreur: " + event.data.error, "Connexion joueur", Alert.OK, null, null, null, Alert.OK);
		}
	}

	public function onChangePlayerPassword (event:Object) {
		// recharge le joueur
		p_oPlayer = null;
		p_oStorage.loadPlayer(p_lstPlayers.selectedItem.data);
		
		if (event.data.result == 0)
			Alert.show("Mot de passe changé.", "Connexion joueur", Alert.OK, null, null, null, Alert.OK);
		else
			Alert.show("Erreur: " + event.data.error, "Connexion joueur", Alert.OK, null, null, null, Alert.OK);
	}


	
}

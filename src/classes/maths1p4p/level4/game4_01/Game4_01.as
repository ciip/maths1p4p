﻿/**
 * class com.maths1p4p.level4.game4_01.Game4_01
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;
import maths1p4p.utils.OutilsTableaux;
import com.prossel.utils.CursorManager;
import mx.controls.Alert;
 

class maths1p4p.level4.game4_01.Game4_01 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------


	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_oDataSaved:Object;
	private var p_aSlots:Array;
	private var p_errors:Number;
	private var p_iFound:Number;
	private var p_aQuestion:Array;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_01()	{		
		super();				
	}


	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		p_iNbMaxTries = 4;	
		p_errors = 0;
		p_iFound = 0;
		
		// init de p_aSlots
		p_aSlots = new Array(8);
		for(var i=0;i<4;i++){
			p_aSlots[i] = new Array(5);
			for(var j=0;j<5;j++){
				p_aSlots[i][j] = new Array(5);
				for(var k=0;k<5;k++){
					p_aSlots[i][j][k] = 13;
				}				
			}
		}
		
		// check le mode du jeu		
		if (_global.biblio != null){
			initSlots();
		}else{
			
			p_oDataSaved = new Object();
			
			var frames:Array = [1,2,3,4,5,6,7,8,9,10,11,12];
			for(var ii=0; ii<10; ii++){
				var ok:Boolean = true;
				while(ok){
					var i:Number = Math.floor(Math.random()*4);
					var j:Number = Math.floor(Math.random()*5);
					var k:Number = Math.floor(Math.random()*5);
					if (p_aSlots[i][j][k] == 13){
						ok = false;
					}
				}
				var cond:Boolean = true;
				var idFrame = Math.floor(Math.random()*frames.length);
				var zeFrame = frames[idFrame];
				frames.splice(idFrame, 1);

				p_aSlots[i][j][k] = zeFrame;
				p_oDataSaved["obj"+ii] = {idShape:zeFrame, i:i, j:j, k:k};
			}
			p_oDataSaved.viewed = false;
			//_global.biblio = p_oDataSaved;
		
		}				
		
		initInterface();
		
	}	
	
	private function initSlots(){
		p_oDataSaved = _global.biblio;
		p_aSlots[p_oDataSaved.obj0.i][p_oDataSaved.obj0.j][p_oDataSaved.obj0.k] = p_oDataSaved.obj0.idShape;
		p_aSlots[p_oDataSaved.obj1.i][p_oDataSaved.obj1.j][p_oDataSaved.obj1.k] = p_oDataSaved.obj1.idShape;
		p_aSlots[p_oDataSaved.obj2.i][p_oDataSaved.obj2.j][p_oDataSaved.obj2.k] = p_oDataSaved.obj2.idShape;
		p_aSlots[p_oDataSaved.obj3.i][p_oDataSaved.obj3.j][p_oDataSaved.obj3.k] = p_oDataSaved.obj3.idShape;
		p_aSlots[p_oDataSaved.obj4.i][p_oDataSaved.obj4.j][p_oDataSaved.obj4.k] = p_oDataSaved.obj4.idShape;
		p_aSlots[p_oDataSaved.obj5.i][p_oDataSaved.obj5.j][p_oDataSaved.obj5.k] = p_oDataSaved.obj5.idShape;
		p_aSlots[p_oDataSaved.obj6.i][p_oDataSaved.obj6.j][p_oDataSaved.obj6.k] = p_oDataSaved.obj6.idShape;
		p_aSlots[p_oDataSaved.obj7.i][p_oDataSaved.obj7.j][p_oDataSaved.obj7.k] = p_oDataSaved.obj7.idShape;
		p_aSlots[p_oDataSaved.obj8.i][p_oDataSaved.obj8.j][p_oDataSaved.obj8.k] = p_oDataSaved.obj8.idShape;
		p_aSlots[p_oDataSaved.obj9.i][p_oDataSaved.obj9.j][p_oDataSaved.obj9.k] = p_oDataSaved.obj9.idShape;
		initQuestion();		
	}
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		if(!p_oDataSaved.viewed){
			frame1();
		}else{
			frame10();
		}
		
		p_mcHelp._visible = false;
		
	}	
	
	private function initQuestion(){
		// 5 formes a cacher 
		var aSoluces:Array = [0,1,2,3,4,5,6,7,8,9];
		p_aQuestion = [];
		for(var i=0;i<5;i++){
			var id = Math.floor(Math.random()*aSoluces.length);
			p_aQuestion.push( p_oDataSaved["obj"+aSoluces[id]].idShape);
			aSoluces.splice(id,1);
		}		
	}
	

	private function initBiblio(){
		
		var count:Number=0;
		
		for(var i=0; i<p_aSlots.length; i++){
			this["biblio_"+i]._visible = false;
			Application.useLibCursor(this["biblio_"+i], "cursor-finger");
			this["biblio_"+i].onRelease = function(){
				_visible = false;
				_parent.playSound("armoire");
			}			
			for(var j=0; j<p_aSlots[i].length; j++){
				for(var k=0; k<p_aSlots[i][j].length; k++){				

					var mcSlot:MovieClip = this["biblio_"+i]["mc_" + j + "_" + k];					
					
					mcSlot["i"] = i;
					mcSlot["j"] = j;
					mcSlot["k"] = k;
					
					// selon le mode, l'intéractivité change
					if (p_oDataSaved.viewed){
						if(OutilsTableaux.getPosition(p_aQuestion, p_aSlots[i][j][k])!=null){
							mcSlot.gotoAndStop(13);					
							mcSlot["i"] = this["rep_"+count]["i"] = i;
							mcSlot["j"] = this["rep_"+count]["j"] = j;
							mcSlot["k"] = this["rep_"+count]["k"] = k;
							this["rep_"+count].gotoAndStop(p_aSlots[i][j][k]);
							this["rep_"+count]["initX"] = this["rep_"+count]._x;
							this["rep_"+count]["initY"] = this["rep_"+count]._y;
							this["rep_"+count]["idShape"] = p_aSlots[i][j][k];
							Application.useLibCursor(this["rep_"+count], "cursor-finger");
							this["rep_"+count].onPress = function(){
								this._x = _parent._xmouse;
								this._y = _parent._ymouse;
								startDrag(this);
							}
							this["rep_"+count].onRelease = this["rep_"+count].onReleaseOutside = function(){
								CursorManager.hide();
								_parent.drop(this);
								CursorManager.show();
							}
							
							count++;
						}else{
							mcSlot.gotoAndStop(p_aSlots[i][j][k]);	
						}
					}else{
						mcSlot.gotoAndStop(p_aSlots[i][j][k]);
					}
				
				}
			}
		}	
		
		//p_oDataSaved.viewed = true;
		

		// boutons tiroirs
		for(var i=0; i<4; i++){
			Application.useLibCursor(this["bt"+i], "cursor-finger");
			this["bt"+i].id = i;
			this["bt"+i].onRelease = function(){
				_parent.playSound("armoire");
				_parent["biblio_"+this["id"]]._visible = true;
				_global["biblio"+this["id"]] = true;
				_parent.checkIfAllViewed();
			}
		}

		
	}
	
	private function checkIfAllViewed():Void{
		
		if(_global.biblio0 && _global.biblio1 && _global.biblio2 && _global.biblio3){
			_global.biblio = p_oDataSaved;
			_global.biblio0 = false;
			_global.biblio1 = false;
			_global.biblio2 = false;
			_global.biblio3 = false;
			p_oDataSaved.viewed = true;
		}
		
	}
	
	
	private function drop(mcDrop:MovieClip){
		
		CursorManager.hide();
		stopDrag();

		var mcTarget:MovieClip = eval(mcDrop._droptarget);

		if(mcTarget.i == mcDrop.i && mcTarget.j == mcDrop.j && mcTarget.k == mcDrop.k){
			p_oSoundPlayer.playASound("ok",0);
			mcTarget.gotoAndStop(mcDrop.idShape);
			mcDrop._visible = false;
			p_iFound++;
			if(p_iFound==5){
				_global.biblio = null;
				won();
			}
		}else{
			if(mcDrop._droptarget.indexOf("mc_")!=-1){
				p_oSoundPlayer.playASound("error",0);
				this["error_"+p_errors]._visible = true;
				p_errors++;
				if(p_errors == p_iNbMaxTries){
					_global.biblio = null;
					showSoluce();
				}
			}
			mcDrop._x = mcDrop.initX;
			mcDrop._y = mcDrop.initY;			
		}
		CursorManager.show();
	}
	
	private function showSoluce(){
		
		Alert.show("Voici la solution !");
		
		for(var i=0; i<p_aSlots.length; i++){
			this["biblio_"+i]._visible = true;
			this["biblio_"+i].useHandCursor = false;
			this["biblio_"+i].onRelease = function(){};
			for(var j=0; j<p_aSlots[i].length; j++){
				for(var k=0; k<p_aSlots[i][j].length; k++){		
					var mcSlot:MovieClip = this["biblio_"+i]["mc_" + j + "_" + k];		
					mcSlot.gotoAndStop(p_aSlots[i][j][k]);			
				}
			}
		}		
		gotoAndStop("soluce");
		
	}	
	
	private function showReps(){
		var count:Number=0;
		for(var i=0; i<p_aSlots.length; i++){		
			for(var j=0; j<p_aSlots[i].length; j++){
				for(var k=0; k<p_aSlots[i][j].length; k++){									
					if(OutilsTableaux.getPosition(p_aQuestion, p_aSlots[i][j][k])!=null){
						this["rep_"+count].gotoAndStop(p_aSlots[i][j][k]);
						count++;
					}				
				}
			}
		}			
		
	}
	
	private function frame1(){
		gotoAndStop(1);
		p_oSoundPlayer.playASound("pas",0);
		Application.useLibCursor(this["bt0"], "cursor-finger");
		this["bt0"].onRelease = function(){
			_parent.frame2();
		}
		p_mcHelp._visible = false;
		
		if(p_oDataSaved.viewed){
			initSlots();
			frame10();
		}		
		
	}

	private function frame2(){
		gotoAndStop(2);
		p_oSoundPlayer.playASound("pas",0);
		initBiblio();
		Application.useLibCursor(this["btBack"], "cursor-arrow-down");
		this["btBack"].onRelease = function(){
			_parent.frame1();
		}	
		p_mcHelp._visible = false;
	}
	
	private function frame3(){
		gotoAndStop(3);
		p_oSoundPlayer.playASound("pas",0);
		Application.useLibCursor(this["bt0"], "cursor-arrow-right");
		this["bt0"].onRelease = function(){
			_parent.frame4();
		}
		showReps();
		p_mcHelp._visible = false;
	}	
	
	private function frame4(){
		gotoAndStop(4);
		p_oSoundPlayer.playASound("pas",0);
		Application.useLibCursor(this["bt0"], "cursor-arrow-up");
		this["bt0"].onRelease = function(){
			_parent.frame5();
		}
		Application.useLibCursor(this["bt1"], "cursor-arrow-left");
		this["bt1"].onRelease = function(){
			_parent.frame3();
		}		
		showReps();
		p_mcHelp._visible = false;
	}
	
	private function frame5(){
		gotoAndStop(5);
		p_oSoundPlayer.playASound("pas",0);
		Application.useLibCursor(this["bt0"], "cursor-arrow-left");
		this["bt0"].onRelease = function(){
			_parent.frame6();
		}
		Application.useLibCursor(this["bt1"], "cursor-arrow-left");
		this["bt1"].onRelease = function(){
			_parent.frame4();
		}		
		showReps();
		p_mcHelp._visible = false;
	}
	
	private function frame6(){
		gotoAndStop(6);
		p_oSoundPlayer.playASound("pas",0);
		initBiblio();
		this["error_0"]._visible = false;
		this["error_1"]._visible = false;
		this["error_2"]._visible = false;
		p_mcHelp._visible = false;
	}		
	
	private function frame10(){
		gotoAndStop(10);
		p_oSoundPlayer.playASound("pas",0);
		showReps();
		Application.useLibCursor(this["mcConsignes"], "cursor-finger");
		this["mcConsignes"].onRelease = function(){
			_parent.frame3();
		}		
		p_mcHelp._visible = false;
	}
	
	private function playSound(idSound:String){
		p_oSoundPlayer.playASound(idSound,0);
	}
	
}

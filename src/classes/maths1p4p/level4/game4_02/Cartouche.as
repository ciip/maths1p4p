﻿/**
 * class com.maths1p4p.level4.game4_02.Cartouche
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

 import maths1p4p.application.Application;
 import com.prossel.utils.CursorManager;

class maths1p4p.level4.game4_02.Cartouche extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	private var initX, initY:Number;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Cartouche()	{		
		Application.useLibCursor(this, "cursor-finger");
	}
	
	public function onPress(){
		
		initX = _x;
		initY = _y;		
		startDrag(true);
		
	}
	
	public function onRelease(){		
		CursorManager.hide();
		stopDrag();
		if(_droptarget.indexOf("FinalSlot") != -1){
			if(_parent.dropOnFinalSlot(_droptarget, this["txt"].text)){
				gotoAndStop(1);
			}			
		}else if(_droptarget.indexOf("p_mcPoubelle") != -1){
			_parent.p_mcPoubelle.onRelease();
		}
		_x = initX;
		_y = initY;
		CursorManager.show();
	}
	
	public function onReleaseOutside(){
		onRelease();
	}


	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	
}

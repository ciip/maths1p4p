﻿/**
 * class com.maths1p4p.level4.game4_02.Game4_02
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level4.game4_02.Game4_02 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	/** animation loterie */
	public var p_mcLoterieAnim:MovieClip;
	
	/** le cartouche */
	public var p_mcCartouche:MovieClip;
	
	/** la poubelle */
	public var p_mcPoubelle:MovieClip;
	
	/** bouton depart/suite */
	public var p_btDepartSuite:Button;
	
	/** le plateau */
	public var p_mcPlateau:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** les 3 nombres aléatoire en cours */
	private var p_aCurrNumbers:Array;
	
	/** nombre d'animations du loto */
	private var p_iCurrAnimCount:Number;
	
	/** nombre de drops */
	private var p_iDropCount:Number;
	
	/** tableau des résultats */
	private var p_aResults:Array;

	/** nombre de numéros déposés */
	private var p_iDone:Number;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_02()	{		
		super();				
	}


	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		p_iNbMaxTries = 12;
		p_iDone = 0;		
		p_aResults = new Array(8);		
		initInterface();
		
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		// init des champs résultats
		for(var i=0;i<8;i++){
			this["txtFinalSlot"+i].id = i;
		}		
		
		Application.useLibCursor(p_mcCartouche, "cursor-finger");
		
		p_mcPoubelle.enabled = false;
		Application.useLibCursor(p_mcPoubelle, "cursor-finger");
		p_mcPoubelle.onRelease = function(){
			this.gotoAndPlay(2);
			_parent.cancel();
		}
		Application.useLibCursor(p_btDepartSuite, "cursor-finger");
		p_btDepartSuite.onRelease = function(){
			_parent.randomNumbers();
		}
		
	}	
	
	private function cancel(){
		
		p_mcPoubelle.enabled = false;
		p_oSoundPlayer.playASound("poubelle",0);
		if(p_iCurrNbTries<p_iNbMaxTries){
			p_btDepartSuite.enabled = true;
			this["txtDepartSuite"].text = "Suite";
			p_mcPlateau.gotoAndStop(1);
			p_mcCartouche.gotoAndStop(1);
			this["txtSlot0"].text = this["txtSlot1"].text = this["txtSlot2"].text = "";
		}else{
			loose();
		}
		
	}
	
	private function randomNumbers(){
		
		p_mcPoubelle.enabled = false;
		p_iCurrNbTries++;
		this["txtPartCount"].text = 12-p_iCurrNbTries;
		
		this["txtDepartSuite"].text = "";
		p_btDepartSuite.enabled = false;
		p_iCurrAnimCount = 0;
		p_iDropCount = 0;
		
		// on tire les 3 nombres au hasard
		p_aCurrNumbers = getNewRandomNumbers();
		p_mcPlateau.gotoAndStop(1);
		doAnimation();
		
	}
	
	private function getNewRandomNumbers():Array{

		var aRes = [
			Math.floor(Math.random()*3+1),
			Math.floor(Math.random()*3+1),
			Math.floor(Math.random()*3+1)
		];
		
		return aRes;		
		
	}
	
	private function doAnimation(){
		p_oSoundPlayer.playASound("tirage",0);
		p_mcLoterieAnim.gotoAndPlay(2);
	}
	
	private function LoterieAnimationFinished(){
						
		// on lance l'animation des boules tirées
		p_mcPlateau.play();
		
		p_iCurrAnimCount++;
		if(p_iCurrAnimCount<2){
			p_mcLoterieAnim.gotoAndPlay(2);			
		}else if (p_iCurrAnimCount == 3){
			p_mcPoubelle.enabled = true;
		}
		
		
	}
	
	private function dropOnSlot(txtSlot:String, ballId:Number){
		
		p_oSoundPlayer.playASound("number",0);
		
		var a:Array = txtSlot.split("/");
		var txtField:TextField = this[a[a.length-1]];		
		txtField.text = String(p_aCurrNumbers[ballId]);
		
		p_iDropCount++;
		if(p_iDropCount==3){
			showCartouche();
		}
		
	}
	
	private function dropOnFinalSlot(txtFinalSlot:String, val:String):Boolean{
		
		p_oSoundPlayer.playASound("number",0);

		var a:Array = txtFinalSlot.split("/");
		var txtField:TextField = this[a[a.length-1]];	
		
		var ok:Boolean;
		
		if(txtField.text == ""){
			
			if( checkResponse( Number(val), txtField.id) ){
				p_aResults[txtField.id] = Number(val);
				txtField.text = val;
				p_iDone++;
				if(p_iDone == 8){
					won();
				}else{
					if(p_iCurrNbTries<p_iNbMaxTries){
						p_mcPoubelle.enabled = false;
						p_btDepartSuite.enabled = true;
						this["txtDepartSuite"].text = "Suite";
					}else{
						loose();
					}
				}
				ok = true;
			}else{
				ok = false;
			}				
			
		}else{
			ok = false;
		}
		
		return ok;
		
	}
	
	private function showCartouche(){		
		
		p_mcCartouche["txt"].text = this["txtSlot0"].text + this["txtSlot1"].text + this["txtSlot2"].text;
		p_mcCartouche.gotoAndPlay(2);
		this["txtSlot0"].text = this["txtSlot1"].text = this["txtSlot2"].text = "";
		p_oSoundPlayer.playASound("glisse",0);
		
	}
	
	private function checkResponse(val:Number, id:Number):Boolean{
		
		// test les nombres précédents
		if(id>0){			
			for(var i=id-1; i>=0; i--){
				if(p_aResults[i] != null && p_aResults[i] >= val){
					return false;
				}
			}			
		}
		// test les nombres suivants
		if(id<p_aResults.length-2){			
			for(var i=id+1; i<p_aResults.length; i++){
				if(p_aResults[i] != null && p_aResults[i] <= val){
					return false;
				}
			}			
		}
		
		return true;
		
	}
	
}

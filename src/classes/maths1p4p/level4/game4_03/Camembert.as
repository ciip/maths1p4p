﻿/**
 * class com.maths1p4p.level4.game4_03.Camembert
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;
class maths1p4p.level4.game4_03.Camembert extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	public var mcQuart0:MovieClip;
	public var mcQuart1:MovieClip;
	public var mcQuart2:MovieClip;
	public var mcQuart3:MovieClip;

	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	private var p_aIds:Array;
	private var p_aColors:Array;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Camembert()	{		
		
		p_aIds = [null, null, null, null];
		
		p_aColors = [
			0x1A44E7,
			0x00FF00,
			0xCE0005,
			0xFFCE00,
			0x00FFFF,
			0xF56409		
		];
		
		for(var i=0; i<4; i++){
			this["mcQuart"+i]["id"] = i;
			Application.useLibCursor(this["mcQuart"+i], "cursor-finger");
			this["mcQuart"+i].onRelease = function(){
				_parent.selectQuart(this["id"]);
			}
		}
		mcQuart0
		
		
	}
	
	public function clear(){
		for(var i=0; i<4; i++){
			var maColor:Color = new Color(this["mcQuart"+i]);
			maColor.setRGB(0xFFCE9C);	
			this["mcQuart"+i].enabled = true;
		}
		_visible = false;
	}	
	
	public function isReady():Boolean{
		return (p_aIds[0]!=null && p_aIds[1]!=null && p_aIds[2]!=null && p_aIds[3]!=null);
	}
	
	public function getAnswer():Array{
		return p_aIds;
	}

	public function freeze(){
		for(var i=0; i<4; i++){
			this["mcQuart"+i].enabled = false;
		}
	}

	public function setColors(aColors){
		for(var i=0; i<4; i++){
			this["mcQuart"+i].enabled = false;
			var maColor:Color = new Color(this["mcQuart"+i]);
			maColor.setRGB(p_aColors[aColors[i]]);
		}
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	private function selectQuart(id:Number){
		
		var iCurrColor:Number = _parent.getCurrColorId();
		var maColor:Color = new Color(this["mcQuart"+id]);
		maColor.setRGB(p_aColors[iCurrColor]);
		p_aIds[id] = iCurrColor;
		_parent.playSound("quartier");
		
	}
	
	
}

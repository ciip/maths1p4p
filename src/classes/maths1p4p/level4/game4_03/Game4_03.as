﻿/**
 * class com.maths1p4p.level4.game4_03.Game4_03
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.utils.OutilsTableaux;
import mx.controls.Alert;
import maths1p4p.application.Application;
 
class maths1p4p.level4.game4_03.Game4_03 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	
	/** bouton depart/suite */
	public var p_btDepartSuite:Button;
	

	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** le camembert en cors */
	private var p_iCurrCamembert:Number;
	
	/** la couleur en cours */
	private var p_iCurrColorId:Number;
	
	/** liste des couleurs */
	private var p_aColors:Array;
	
	/** réponse */
	private var p_aAnswer:Array;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_02()	{		
		super();				
	}

	public function getCurrColorId():Number{		
		return p_iCurrColorId;		
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		p_iNbPartsToWin = 2;
		p_iNbPartsWon = 0;
		p_iNbMaxTries = 10;
		p_iCurrNbTries = 0;	
		
		p_aColors = [
			0x1A44E7,
			0x00FF00,
			0xCE0005,
			0xFFCE00,
			0x00FFFF,
			0xF56409		
		];
		
		initInterface();
		
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		this["mcPlanet0"]._visible = this["mcPlanet1"]._visible = false;
		
		for (var i=0; i<6; i++){
			this["btColor"+i]._alpha = 0;
			this["btColor"+i]["id"] = i;
			Application.useLibCursor(this["btColor"+i], "cursor-finger");
			this["btColor"+i].onRelease = function(){
				_parent.selectColor(this["id"]);
			}			
		}		

		Application.useLibCursor(p_btDepartSuite, "cursor-finger");
		p_btDepartSuite.onRelease = function(){
			_parent.checkAnswer();
		}
		
		newPart();
		
	}	
	
	private function checkAnswer(){
				
		var aAnswer:Array = this["mcCamembert"+p_iCurrCamembert].getAnswer();
		
		p_oSoundPlayer.playASound("valid",0);
		
		if(OutilsTableaux.getIndex(aAnswer, null) == undefined){
			
			var iNbViolet:Number = 0;
			var iNbBlanches:Number = 0;
			
			for(var i=0; i<aAnswer.length; i++){			
				if( aAnswer[i] == p_aAnswer[i]){
					iNbViolet++;
				}else if(OutilsTableaux.getIndex(p_aAnswer, aAnswer[i]) != undefined){
					iNbBlanches++;
				}		
			}
			
			this["mcResult"+p_iCurrNbTries].setResult(iNbViolet, iNbBlanches);
			if(iNbViolet==4){
				this["mcPlanet"+p_iNbPartsWon]._visible = true;
				p_iNbPartsWon++;
				if(p_iNbPartsWon == p_iNbPartsToWin){
					won();
				}else{
					this["txtDepartSuite"].text = "Suite";
					p_btDepartSuite.onRelease = function(){
						_parent.newPart();
					}
				}
			}else{
				this["mcCamembert"+p_iCurrNbTries].freeze();
				p_iCurrNbTries++;
				p_iCurrCamembert++;
				this["mcCamembert"+p_iCurrNbTries]._visible = true;			
				if(p_iCurrNbTries == p_iNbMaxTries){
					loose();
					this.onEnterFrame = function(){
						this["mcCamembertLoose"].setColors(p_aAnswer);
						delete this.onEnterFrame;
					}
				}
			}
			
		}else{
			Alert.show("Tu n'as pas terminé !");
		}
		
	}
	
	private function newPart(){
		
		this["txtDepartSuite"].text = "J'ai fini";
		p_btDepartSuite.onRelease = function(){
			_parent.checkAnswer();
		}		
		
		p_iCurrNbTries = 0;	
		p_iCurrCamembert = 0;
		
		// cache les réponses
		for(var i=0; i<10; i++){
			this["mcCamembert"+i].clear();
			this["mcResult"+i].clear();
		}
		this["mcCamembert0"]._visible = true;
		
		// random answer
		p_aAnswer = OutilsTableaux.shuffle([0,1,2,3,4,5]);
		p_aAnswer.pop();
		p_aAnswer.pop();
		trace(p_aAnswer);
	
	}
	
	private function selectColor(id:Number){
		
		p_oSoundPlayer.playASound("couleur",0);
		
		for (var i=0; i<6; i++){
			this["btColor"+i]._alpha = 0;
		}
		this["btColor"+id]._alpha = 100;
		
		p_iCurrColorId = id;
		
	}
	
	private function playSound(id:String){
		p_oSoundPlayer.playASound(id,0);
	}
	
}

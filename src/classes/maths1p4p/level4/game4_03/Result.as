﻿/**
 * class com.maths1p4p.level4.game4_03.Camembert
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

class maths1p4p.level4.game4_03.Result extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------



	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Result()	{		
		_visible = false;
	}
	
	public function clear(){
		for(var i=0; i<4; i++){
			this["mcPuceBlanche"+i]._visible = false;	
			this["mcPuceVio"+i]._visible = false;	
		}
		_visible = false;
	}
	
	public function setResult(iNbColorVio:Number, iNbColorBlanches:Number){
		var iPuceCount:Number=0;
		for(var i=0; i<iNbColorVio; i++){
			this["mcPuceVio"+iPuceCount]._visible = true;	
			iPuceCount++;
		}
		for(var i=0; i<iNbColorBlanches; i++){
			this["mcPuceBlanche"+iPuceCount]._visible = true;	
			iPuceCount++;
		}
		_visible = true;
	}

	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	

	
	
}

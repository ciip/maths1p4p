﻿/**
 * class com.maths1p4p.level3.game3_08.Game3_08
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import flash.geom.Point;

class maths1p4p.level4.game4_04.Game4_04 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** les boutons */
	public var p_btUp, p_btDown, p_btRight, p_btLeft:Button;
	
	/** les 2 portes */
	public var p_mcDoors:MovieClip;
	
	/** bouton valider */
	public var p_btValid:Button;
	
	/** maps */
	public var p_mcAnswers:MovieClip;
	
	public var p_mcLoose:MovieClip;
	

	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var p_aMaps:Array;
	
	private var p_ptCurrPos:Point;
	private var p_ptCurrDir:Point;
	
	private var p_aDirs:Array;
	private var p_iCurrDirId:Number;
	
	private var p_aStartPoss:Array;
	private var p_aStartDirs:Array;
	
	private var p_ptBackPos:Point;
	private var p_ptBackDir:Point;
	
	private var p_iCurrMap:Number;
	private var p_aCurrMap:Array;

	private var p_iCurrAnswer:Number;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_04()	{		
		super();	
	}	
		



	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		// creation des maps
		
		var aMap0 = [];
		aMap0[0]  = [1,1,1,1,1,0,0,0,0,0,0,0];
		aMap0[1]  = [1,0,0,0,1,0,0,0,0,0,0,0];
		aMap0[2]  = [1,0,0,0,1,0,0,0,0,0,0,0];
		aMap0[3]  = [1,0,0,0,1,1,1,0,0,0,0,0];
		aMap0[4]  = [1,1,1,0,0,0,0,0,0,0,0,0];
		aMap0[5]  = [0,0,1,0,0,0,0,0,0,0,0,0];
		aMap0[6]  = [0,0,1,1,1,0,0,0,0,0,0,0];
		aMap0[7]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap0[8]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap0[9]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap0[10] = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap0[11] = [0,0,0,0,0,0,0,0,0,0,0,0];
				
		var aMap1 = [];
		aMap1[0]  = [0,0,0,1,0,0,0,0,0,0,0,0];
		aMap1[1]  = [0,0,0,1,0,0,0,0,0,0,0,0];
		aMap1[2]  = [1,1,1,1,0,0,1,0,0,0,0,0];
		aMap1[3]  = [1,0,0,0,0,0,1,0,0,0,0,0];
		aMap1[4]  = [1,0,0,1,1,1,1,0,0,0,0,0];
		aMap1[5]  = [1,0,0,1,0,0,0,0,0,0,0,0];
		aMap1[6]  = [1,1,1,1,0,0,0,0,0,0,0,0];
		aMap1[7]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap1[8]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap1[9]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap1[10] = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap1[11] = [0,0,0,0,0,0,0,0,0,0,0,0];	
		
		var aMap2 = [];
		aMap2[0]  = [1,1,1,0,0,0,0,0,0,0,0,0];
		aMap2[1]  = [0,0,1,0,0,0,0,0,0,0,0,0];
		aMap2[2]  = [0,0,1,0,1,1,1,1,1,0,0,0];
		aMap2[3]  = [0,0,1,0,1,0,0,0,1,0,0,0];
		aMap2[4]  = [0,0,1,1,1,0,0,0,1,0,0,0];
		aMap2[5]  = [0,0,0,0,0,0,0,0,1,1,1,0];
		aMap2[6]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap2[7]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap2[8]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap2[9]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap2[10] = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap2[11] = [0,0,0,0,0,0,0,0,0,0,0,0];	
		
		var aMap3 = [];
		aMap3[0]  = [1,1,1,0,0,0,0,0,0,0,0,0];
		aMap3[1]  = [0,0,1,0,0,0,0,0,0,0,0,0];
		aMap3[2]  = [0,0,1,0,0,0,0,0,0,0,0,0];
		aMap3[3]  = [0,0,1,0,0,0,0,0,0,0,0,0];
		aMap3[4]  = [0,0,1,1,1,0,0,0,0,0,0,0];
		aMap3[5]  = [0,0,0,0,1,0,0,0,0,0,0,0];
		aMap3[6]  = [0,0,0,0,1,0,0,0,0,0,0,0];
		aMap3[7]  = [0,0,0,0,1,0,0,0,0,0,0,0];
		aMap3[8]  = [0,0,1,1,1,0,0,0,0,0,0,0];
		aMap3[9]  = [0,0,1,0,0,0,0,0,0,0,0,0];
		aMap3[10] = [0,0,1,0,0,0,0,0,0,0,0,0];
		aMap3[11] = [0,0,1,1,1,0,0,0,0,0,0,0];
		
		var aMap4 = [];
		aMap4[0]  = [0,0,0,1,0,0,0,0,0,0,0,0];
		aMap4[1]  = [0,0,0,1,0,0,0,0,0,0,0,0];
		aMap4[2]  = [1,1,1,1,0,0,0,0,0,0,0,0];
		aMap4[3]  = [1,0,0,0,0,0,0,0,0,0,0,0];
		aMap4[4]  = [1,0,0,0,0,0,0,0,0,0,0,0];
		aMap4[5]  = [1,1,1,1,0,0,0,0,0,0,0,0];
		aMap4[6]  = [0,0,0,1,0,0,0,0,0,0,0,0];
		aMap4[7]  = [0,0,0,1,0,0,0,0,0,0,0,0];
		aMap4[8]  = [0,0,0,1,0,0,0,0,0,0,0,0];
		aMap4[9]  = [0,1,1,1,0,0,0,0,0,0,0,0];
		aMap4[10] = [0,1,0,0,0,0,0,0,0,0,0,0];
		aMap4[11] = [0,1,0,0,0,0,0,0,0,0,0,0];
		
		var aMap5 = [];
		aMap5[0]  = [0,0,0,0,0,1,0,0,0,0,0,0];
		aMap5[1]  = [0,0,0,0,0,1,0,0,0,0,0,0];
		aMap5[2]  = [0,1,1,1,1,1,0,0,0,0,0,0];
		aMap5[3]  = [0,1,0,0,0,0,0,0,0,0,0,0];
		aMap5[4]  = [0,1,1,1,1,0,0,0,0,0,0,0];
		aMap5[5]  = [0,0,0,0,1,0,0,0,0,0,0,0];
		aMap5[6]  = [1,1,1,1,1,0,0,0,0,0,0,0];
		aMap5[7]  = [1,0,0,0,0,0,0,0,0,0,0,0];
		aMap5[8]  = [1,0,0,0,0,0,0,0,0,0,0,0];
		aMap5[9]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap5[10] = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap5[11] = [0,0,0,0,0,0,0,0,0,0,0,0];
		
		var aMap6 = [];
		aMap6[0]  = [0,0,0,0,1,1,1,0,0,0,0,0];
		aMap6[1]  = [0,0,0,0,1,0,1,0,0,0,0,0];
		aMap6[2]  = [1,1,1,1,1,0,1,0,0,0,0,0];
		aMap6[3]  = [1,0,0,0,0,0,1,0,0,0,0,0];
		aMap6[4]  = [1,0,0,1,1,1,1,0,0,0,0,0];
		aMap6[5]  = [0,0,0,1,0,0,0,0,0,0,0,0];
		aMap6[6]  = [0,0,0,1,0,0,0,0,0,0,0,0];
		aMap6[7]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap6[8]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap6[9]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap6[10] = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap6[11] = [0,0,0,0,0,0,0,0,0,0,0,0];
		
		var aMap7 = [];
		aMap7[0]  = [0,0,0,0,0,0,0,0,0,1,0,0];
		aMap7[1]  = [0,0,0,0,0,0,0,0,0,1,0,0];
		aMap7[2]  = [1,1,1,0,0,1,1,1,1,1,0,0];
		aMap7[3]  = [1,0,1,0,0,1,0,0,0,0,0,0];
		aMap7[4]  = [1,0,1,0,0,1,0,0,0,0,0,0];
		aMap7[5]  = [0,0,1,1,1,1,0,0,0,0,0,0];
		aMap7[6]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap7[7]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap7[8]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap7[9]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap7[10] = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap7[11] = [0,0,0,0,0,0,0,0,0,0,0,0];
		
		var aMap8 = [];
		aMap8[0]  = [0,0,0,1,1,1,1,0,0,0,1,0];
		aMap8[1]  = [0,0,0,1,0,0,1,0,0,0,1,0];
		aMap8[2]  = [0,0,0,1,0,0,1,1,1,1,1,0];
		aMap8[3]  = [1,1,1,1,0,0,0,0,0,0,0,0];
		aMap8[4]  = [1,0,0,0,0,0,0,0,0,0,0,0];
		aMap8[5]  = [1,0,0,0,0,0,0,0,0,0,0,0];
		aMap8[6]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap8[7]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap8[8]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap8[9]  = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap8[10] = [0,0,0,0,0,0,0,0,0,0,0,0];
		aMap8[11] = [0,0,0,0,0,0,0,0,0,0,0,0];
		
		p_aMaps = [aMap0, aMap1, aMap2, aMap3, aMap4, aMap5, aMap6, aMap7, aMap8];
		
		// les 4 arrivées possibles
		p_aStartPoss = new Array(
			new Point(6,4),
			new Point(0,3),
			new Point(0,0),
			new Point(0,0),
			new Point(0,3),
			new Point(0,5),
			new Point(4,0),
			new Point(5,0),
			new Point(6,0)
		);
		p_aStartDirs = new Array(
			3,
			0,
			1,
			1,
			0,
			0,
			2,
			2,
			2
		);
		
		// position initiale
		//p_ptCurrPos = new Point(6,0);	
		
		p_aDirs = [];
		p_aDirs[0] = new Point(1,0);
		p_aDirs[1] = new Point(0,1);
		p_aDirs[2] = new Point(-1,0);
		p_aDirs[3] = new Point(0,-1);
						
		p_ptBackPos = new Point(6,0);
		p_ptBackDir = new Point(1,0);
		
		p_iCurrNbTries = 0;
		p_iNbMaxTries = 1;
		
		p_iNbPartsWon = 0;
		p_iNbPartsToWin = 3;

		initInterface();
		
	}	


	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		this["mcPlanet0"]._visible = this["mcPlanet1"]._visible = this["mcPlanet2"]._visible = false;
		
		p_mcAnswers._visible = false;

		Application.useLibCursor(p_btUp, "cursor-finger");
		p_btUp.onRelease = function(){
			var ptMove:Point = _parent.move("up", _parent.p_ptCurrPos);
			_parent.tryToMove(ptMove);
		}
		
		Application.useLibCursor(p_btDown, "cursor-finger");
		p_btDown.onRelease = function(){
			var ptMove:Point = _parent.move("down", _parent.p_ptCurrPos);
			_parent.tryToMove(ptMove);
		}
		
		Application.useLibCursor(p_btLeft, "cursor-finger");
		p_btLeft.onRelease = function(){
			_parent.turn("left");
			_parent.render();
		}
		
		Application.useLibCursor(p_btRight, "cursor-finger");
		p_btRight.onRelease = function(){
			_parent.turn("right");
			_parent.render();
		}
		
		Application.useLibCursor(p_btValid, "cursor-finger");
		p_btValid.onRelease = function(){
			_parent.checkAnswer();
		}		
		
		
		// init des boutons réponse
		for(var i=0; i<p_aMaps.length; i++){
			this["btAnswer"+i]["id"] = i;
			Application.useLibCursor(this["btAnswer"+i], "cursor-finger");
			this["btAnswer"+i].onRelease = function(){
				_parent.setAnswer(this["id"]);
			}
		}
		
		Application.useLibCursor(p_mcDoors, "cursor-finger");
		p_mcDoors.onRelease = function(){
			gotoAndPlay(2);
			_parent.p_mcLoose._visible = false;
			this.enabled = false;
			_parent.playsound("porte");
		}
		
		p_mcLoose._visible = false;
		
		startPart();
		
	}	
	
	private function playsound(id:String){
		p_oSoundPlayer.playASound(id,0);
	}
	
		
	/**
	 * début de la partie
	 */
	private function startPart():Void{
		
		p_mcAnswers._visible = false;
		p_iCurrAnswer = null;
		
		p_btValid.enabled = false;
		
		p_mcDoors.gotoAndStop(1);
		p_mcDoors.enabled = true;
		
		p_iCurrMap = Math.floor(Math.random()*p_aMaps.length);
		trace("reponse : "+p_iCurrMap);
		
		p_aCurrMap = p_aMaps[p_iCurrMap];
		
		p_iCurrDirId = p_aStartDirs[p_iCurrMap];
		p_ptCurrDir = p_aDirs[p_iCurrDirId];		
		p_ptCurrPos = p_aStartPoss[p_iCurrMap];
		
		render();
		
		p_iCurrNbTries = 0;
		
	}
	
	/**
	 * rotation
	 */
	private function turn(sDir:String){
		
		switch(sDir){
			case "left":
				p_iCurrDirId++;
				if(p_iCurrDirId>3)p_iCurrDirId=0;
				break;				
			case "right":
				p_iCurrDirId--;
				if(p_iCurrDirId<0)p_iCurrDirId=3;
				break;
		}
		
		p_ptCurrDir = p_aDirs[p_iCurrDirId];
		
	}
	
	/**
	 * essaye un déplacement
	 */
	private function tryToMove(ptMove:Point):Void{
		
		if (p_aCurrMap[ptMove.x][ptMove.y] == 1){
			p_ptCurrPos.x = ptMove.x;
			p_ptCurrPos.y = ptMove.y;
			render();
		}
		
	}
	
	/**
	 * déplacement
	 * @param sDir direction
	 * @param pt la position en cours
	 */
	private function move(sDir:String, pt:Point):Point{
		
		p_oSoundPlayer.playASound("pas",0);
				
		var res:Point = new Point();
		
		switch(sDir){
			case "up":
				res.x = pt.x + p_ptCurrDir.x;
				res.y = pt.y + p_ptCurrDir.y;				
				break;				 
			case "down":
				res.x = pt.x - p_ptCurrDir.x;
				res.y = pt.y - p_ptCurrDir.y;
				break;
		}

		return res;
		
	}

	
	/**
	 * affichage de la position en cours
	 */
	private function render():Void{
		
		var ptView:Point = new Point(p_ptCurrPos.x, p_ptCurrPos.y);
		
		for(var iPlan:Number=0; iPlan<5; iPlan++){
			
			if(Math.abs(p_ptCurrDir.x) == 1){
				// la gauche et la droite sur l'axe y
				var ptLeft:Point =  new Point(ptView.x, ptView.y + p_ptCurrDir.x);
				var ptRight:Point = new Point(ptView.x, ptView.y - p_ptCurrDir.x);
				var ptUp:Point =    new Point(ptView.x + p_ptCurrDir.x, ptView.y);
			}else{
				// la gauche et la droite sur l'axe x
				var ptLeft:Point =  new Point(ptView.x - p_ptCurrDir.y, ptView.y);
				var ptRight:Point = new Point(ptView.x + p_ptCurrDir.y, ptView.y);
				var ptUp:Point =    new Point(ptView.x, ptView.y + p_ptCurrDir.y);
			}
			
			// mur de gauche
			this["mcPlan"+iPlan]["mcLeft"]._visible  = (p_aCurrMap[ptLeft.x] [ptLeft.y]  == 1);
			// mur de droite
			this["mcPlan"+iPlan]["mcRight"]._visible = (p_aCurrMap[ptRight.x][ptRight.y] == 1);
			// mur en face
			this["mcPlan"+iPlan]["mcUp"]._visible    = (p_aCurrMap[ptUp.x]   [ptUp.y]    != 1);
						
			ptView = move("up", ptView);
						
		}
		
	}
	
	private function setAnswer(iAnswer:Number){
		p_iCurrAnswer = iAnswer;
		p_mcAnswers.gotoAndStop(iAnswer+1);
		p_mcAnswers._visible = true;
		p_btValid.enabled = true;
		p_oSoundPlayer.playASound("tac",0);
	}
	
	private function checkAnswer(){
		
		p_oSoundPlayer.playASound("tac",0);

		p_iCurrNbTries++;

		if(p_iCurrAnswer == p_iCurrMap){
			// bon choix
			if(p_iCurrNbTries <= p_iNbMaxTries){
				this["mcPlanet"+p_iNbPartsWon]._visible = true;
				p_iNbPartsWon++;				
				if(p_iNbPartsWon == p_iNbPartsToWin){
					won();
				}else{
					startPart();
				}
			}else{
				startPart();
				p_mcLoose._visible = true;
			}
		}
		
	}
	
	

	
}

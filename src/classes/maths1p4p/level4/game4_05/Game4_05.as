﻿/**
 * class com.maths1p4p.level4.game4_05.Game4_05
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.AbstractGame;
import maths1p4p.application.Application;
import com.prossel.utils.CursorManager;
import flash.geom.Point;

class maths1p4p.level4.game4_05.Game4_05 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_mcAnswer:MovieClip;
	
	public var p_btSuite:Button;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** la réponse à trouver */
	private var p_iAnswer:Number;


	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_05()	{		
		super();		
		
		/** le nombre de parties à gagner */
		p_iNbPartsToWin = 4;
		/** le nombre de parties gagnées*/
		p_iNbPartsWon = 0;
		/** nombre d'essais */
		p_iNbMaxTries = 40;
		
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){		
		
		// initialisation de l'interface
		initInterface();
		
		// 
		nextPart();
		
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		
		
		// init de la grille
		initGrid();
		
		p_mcAnswer["txt"].type = "input";
		p_mcAnswer["txt"].selectable = false;
		p_mcAnswer["txt"].restrict  = "0123456789";
		p_mcAnswer["txt"].maxChars  = 3;
		p_mcAnswer["txt"].background = false;
		p_mcAnswer["initLoc"] = new Point(p_mcAnswer._x, p_mcAnswer._y);
		Application.useLibCursor(p_mcAnswer, "cursor-finger");
		p_mcAnswer.onPress = function(){
			if(this.txt.text!="?"){
				this._x = _parent._xmouse - this._width/2;
				this._y = _parent._ymouse - this._height/2;	
				startDrag(this);
			}
		}
		p_mcAnswer.onRelease = p_mcAnswer.onReleaseOutside = function(){	
			if(this.txt.text!="?"){				
				CursorManager.hide();
				stopDrag();
				_parent.drop(this._droptarget);
				CursorManager.show();
				this._x = this["initLoc"].x;
				this._y = this["initLoc"].y;		
			}
		}
		
		p_mcAnswer._visible = false;
		Key.addListener(this);
		
		p_btSuite._visible = false;
		Application.useLibCursor(p_btSuite, "cursor-finger");
		p_btSuite.onRelease = function(){
			_parent.nextPart();
			this._visible = false;
		}
				
	}	
	
	
	/**
	 * démarrage d'une partie
	 */
	private function nextPart(){
		
		super.nextPart();
		p_iAnswer = Math.floor(Math.random()*1000);
		p_mcAnswer._visible = true
		p_mcAnswer.txt.text = "?";
		initGrid();
		
	}	
	
	/**
	 * drop
	 */
	private function drop(Drop:String){
		
		var mcDrop:MovieClip = eval(Drop)._parent;
		if(mcDrop._name.indexOf("item_") != -1 && mcDrop["bg"]._alpha != 100){
			
			p_oSoundPlayer.playASound("drop",0);
			
			mcDrop["txt"].text = p_mcAnswer["txt"].text;
			mcDrop["txt"]._visible = true;
			
			var iColor = checkAnswer(Number(p_mcAnswer["txt"].text));
			
			var maColor:Color = new Color(mcDrop["bg"]);
			maColor.setRGB(iColor);
			mcDrop["bg"]._alpha = 100;
			
			p_mcAnswer["txt"].text = "?";
						
			p_iCurrNbTries++;			
			this["txtCount"].text = p_iCurrNbTries;
			if(p_iCurrNbTries == p_iNbMaxTries){
				p_mcAnswer._visible = false;
				loose();
			}			
			
		}
		
	}
	
	private function initGrid():Void{

		var initLoc:Point = new Point(13,6);
		var itemW:Number = 61;
		var itemH:Number = 27;
		
		var iCount:Number = 0;
		for (var i=0;i<10;i++){
			for(var j=0;j<10;j++){				
				var mcItem:MovieClip = attachMovie("mcGridItem", "item_"+i+"_"+j, iCount);
				mcItem._x = initLoc.x + i*itemW;
				mcItem._y = initLoc.y + j*itemH;
				mcItem["txt"]._visible = false;
				mcItem["bg"]._alpha = 0;
				iCount++;
			}
		}
		
		p_mcAnswer.swapDepths(iCount+10);
		p_mcAnswer["bg"]._alpha = 0;
		
	}
	
	/**
	 * appellé lorsqu'une touche du clavier est appuyée
	 */
	private function onKeyDown(){

		var iCode;
		switch(Key.getCode()){
			case 96:
				iCode = 48;
				break;
			case 97:
				iCode = 49;
				break;
			case 98:
				iCode = 50;
				break;
			case 99:
				iCode = 51;
				break;
			case 100:
				iCode = 52;
				break;
			case 101:
				iCode = 53;
				break;
			case 102:
				iCode = 54;
				break;
			case 103:
				iCode = 55;
				break;
			case 104:
				iCode = 56;
				break;
			case 105:
				iCode = 57;
				break;
			default:
				iCode = Key.getCode();
		}
		
		if(iCode == Key.BACKSPACE || iCode == Key.DELETEKEY){
			p_mcAnswer["txt"].text = p_mcAnswer["txt"].text.substr(0, p_mcAnswer["txt"].text.length-1);
		}else{
			var nb:Number = Number(String.fromCharCode(iCode));
			
			if(!isNaN(nb)) {
				if(p_mcAnswer["txt"].text == "?"){
					p_mcAnswer["txt"].text = "";
				}
				if (p_mcAnswer["txt"].text.length<3){
					p_mcAnswer["txt"].text += String(nb);
				}
			}
		}
		
	}
	
	/**
	 * démarrage d'une partie
	 */
	private function partWon(){
		
		super.partWon();
		p_mcAnswer._visible = false;
		p_btSuite._visible = true;

	}	

			

	/**
	 * Vérifie la réponse du joueur
	 */
	private function checkAnswer(answ:Number):Number{
		
		var iDiff:Number = Math.abs(p_iAnswer - answ);
		
		if(iDiff == 0){
			partWon();
			return 0xFF0000;
		}else if (iDiff == 1){
			return 0xF56409;
		}else if (iDiff <= 5){
			return 0xFFCE00;
		}else if (iDiff <= 20){
			return 0x00FF00;
		}else if (iDiff <= 50){
			return 0x7EC9F6;
		}else if (iDiff <= 100){
			return 0x2063E3;
		}else if (iDiff <= 300){			
			return 0x0105A9;
		}else if (iDiff <= 1000){
			return 0xF2E7EF;
		}

		
	}
	
	


	
}

﻿/**
 * class com.maths1p4p.level4.game4_05.Game4_05
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.AbstractGame;
import maths1p4p.application.Application;
import flash.geom.Point;

class maths1p4p.level4.game4_06.Game4_06 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var p_mcAnswer:MovieClip;
	
	public var p_btSuite:Button;
	public var p_btOk:Button;
	
	public var p_mcCursor:MovieClip;
	
	public var p_mcBarres:MovieClip;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** la réponse à trouver */
	private var p_iAnswer:Number;
	
	private var p_iNb1, p_iNb2:Number;
	
	private var p_iBarreLeft:Number = 129;
	private var p_iBarreRight:Number = 507;


	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_06()	{		
		super();		
		
		/** le nombre de parties à gagner */
		p_iNbPartsToWin = 3;
		/** le nombre de parties gagnées*/
		p_iNbPartsWon = 0;
		/** nombre d'essais */
		p_iNbMaxTries = 18;
		
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){		
		
		// initialisation de l'interface
		initInterface();
		
		// 
		nextPart();
		
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		

		p_mcAnswer["txt"].type = "input";
		p_mcAnswer["txt"].selectable = true;
		p_mcAnswer["txt"].restrict  = "0123456789";
		p_mcAnswer["txt"].maxChars  = 4;
		p_mcAnswer["txt"].background = false;
		p_mcAnswer["initLoc"] = new Point(p_mcAnswer._x, p_mcAnswer._y);
		
		p_mcAnswer._visible = false;
		Key.addListener(this);
		
		p_btSuite._visible = false;
		Application.useLibCursor(p_btSuite, "cursor-finger");
		p_btSuite.onRelease = function(){
			_parent.nextPart();
			this._visible = false;
		}
		
		Application.useLibCursor(p_btOk, "cursor-finger");
		p_btOk.onRelease = function(){
			_parent.checkAnswer(Number(_parent.p_mcAnswer["txt"].text));
		}
				
		Selection.setFocus(p_mcAnswer["txt"]);
		
	}	
	
	
	/**
	 * démarrage d'une partie
	 */
	private function nextPart(){
		
		super.nextPart();
		
		p_mcCursor.gotoAndStop(1);
		
		p_mcBarres = createEmptyMovieClip("mcBarres", 10);
		
		p_iNb1 = 30 + Math.floor(Math.random()*170);
		switch(p_iNbPartsWon){
			case 0:				
				p_iNb2 = p_iNb1 + 101;
				break;
			case 1:
				p_iNb2 = p_iNb1 + 505;
				break;
			case 2:
				p_iNb2 = p_iNb1 + 2020;
				break;
		}

		this["txtNb1"].text = p_iNb1;
		this["txtNb2"].text = p_iNb2;
		
		p_iAnswer = p_iNb1 + Math.floor(Math.random()*(p_iNb2-p_iNb1-20)) +10;
		p_mcAnswer._visible = true;

		placeMcAt(p_mcCursor, p_iAnswer);
		
		Selection.setFocus(p_mcAnswer["txt"]);
		
	}	
	
	private function placeMcAt(mc:MovieClip, numb:Number){

		var tx:Number = (numb-p_iNb1)/(p_iNb2-p_iNb1);
		//var txGood:Number = (numb-p_iAnswer)/(p_iNb2-p_iNb1);
		var txGood:Number = (p_iAnswer-p_iNb1)/(p_iNb2-p_iNb1);
		
		var pos:Number = p_iBarreLeft + (tx*(p_iBarreRight - p_iBarreLeft));
		var posGood:Number = p_iBarreLeft + ( txGood*(p_iBarreRight - p_iBarreLeft));
		
		switch(p_iNbPartsWon){
			case 1:
			case 2:
				if(pos-posGood<4 && pos-posGood>0){
					pos = posGood + 4;
				}else if(posGood-pos<4 && posGood-pos>0){
					pos = posGood - 4;
				}
				break;
		}
		
		mc._x = pos;
		
		p_mcCursor.swapDepths(getNextHighestDepth());

	}
	
	private function addOneBarreAt(at:Number){
		
		// repasse toutes les barres en vert
		for(var i in p_mcBarres){			
			var maColor = new Color(p_mcBarres[i]);
			maColor.setRGB(0x00FF00);			
		}
		
		// ajout d'une barre rouge
		var mcBarre:MovieClip = p_mcBarres.attachMovie("mcBarre", "barre_"+p_iCurrNbTries,p_mcBarres.getNextHighestDepth());
		mcBarre._y = 198;
		placeMcAt(mcBarre, at);		
		
	}

	
	/**
	 * appellé lorsqu'une touche du clavier est appuyée
	 */
	private function onKeyDown(){
		
		// si le joueur presse ENTER, on test la valeur de p_txtInput
		if(Key.getCode()==Key.ENTER){
			checkAnswer(Number(p_mcAnswer["txt"].text));			
		}

	}
	
	/**
	 * démarrage d'une partie
	 */
	private function partWon(){
		
		super.partWon();
		p_mcAnswer._visible = false;
		p_btSuite._visible = true;
		p_mcCursor.gotoAndStop(2);
		
	}	

			

	/**
	 * Vérifie la réponse du joueur
	 */
	private function checkAnswer(answ:Number){
		
		if(answ > p_iNb1 && answ < p_iNb2){
		
			p_iCurrNbTries++;
			this["p_txtNbTries"].text = p_iCurrNbTries;			
			if(answ == p_iAnswer){			
				p_oSoundPlayer.playASound("ok",0);
				partWon();			
			}else{				
				addOneBarreAt(answ);
				p_oSoundPlayer.playASound("error",0);
			}
			
		}
		
		p_mcAnswer["txt"].text = "";
		
	}
	
	


	
}

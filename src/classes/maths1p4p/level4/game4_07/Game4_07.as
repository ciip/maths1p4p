﻿/**
 * class com.maths1p4p.level4.game4_07.Game4_07
 * 
 * @author Grégory Lardon
 * @version 0.2
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * TODO :
 * integrer la gestion des sons
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level4.game4_07.Game4_07 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** le texte d'affichage des commandes */
	public var p_txtCommands:TextField;
	
	/** les différents boutons de l'interface de commande */
	public var p_btLeft, p_btRight, p_btBack, p_btAhead:Button;
	public var p_btC:Button;
	public var p_btExecute:Button;
	
	/** le movieclip la fourmi */
	public var p_mcFourmi:MovieClip;

	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** la grille du jeu */
	private var p_aMap:Array;
	/** position courante dans la map */
	private var p_aCurrLoc:Array;
	
	/** liste des points à atteindre */
	private var p_aDestinations:Array;

	/** les directions */
	private var p_aDirections:Array;
	/** id de la direction en cours */
	private var p_iCurrDirection:Number;
	/** rotation courante dans la map */
	private var p_iCurrRotation:Number;
	
	/** écarts entre les points */
	private var p_iPtXOffset, p_iPtYOffset:Number;
	/** position du point de référence 0,0 */
	private var p_iInitPointLoc:Point;
	
	
		

	/** la commande en cours de saisie */
	private var p_aCommands:Array;
	
	/** true si les actions utilisateurs sont interprétées */
	private var p_bActionOk:Boolean;
	
	/** nombre de steps sur le trajet de la fourmie */
	private var p_iGoSteps:Number;
	/** step en cours */
	private var p_iCurrStep:Number;
	/** commande en cours */
	private var p_oCommand:Object;


	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_07()	{		
		super();				
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){		
		
		// initialisation des propriétés
		
		p_iNbMaxTries = 6;
		
		// map
		p_aMap = new Array();
		p_aMap[0]   = [0,0,0,0,0,0,0,0,0,2,0,0,0,1,1];
		p_aMap[1]   = [0,0,0,0,0,0,0,0,0,0,0,0,0,1,1];
		p_aMap[2]   = [0,0,1,0,0,1,1,1,1,1,1,1,1,1,1];
		p_aMap[3]   = [2,0,1,0,0,0,0,0,0,0,0,0,1,0,0];
		p_aMap[4]   = [0,0,1,0,0,0,0,0,0,0,0,0,1,0,0];
		p_aMap[5]   = [0,0,1,0,0,0,0,0,1,0,0,0,1,2,0];
		p_aMap[6]   = [0,0,1,0,0,0,0,0,1,0,0,0,1,0,0];
		p_aMap[7]   = [0,0,1,0,2,0,0,0,1,0,0,0,1,0,0];
		p_aMap[8]   = [0,0,0,0,0,0,0,0,1,0,0,0,1,0,0];
		p_aMap[9]   = [0,0,1,1,1,1,1,1,1,0,2,0,1,0,0];
		p_aMap[10]  = [0,0,0,0,0,0,0,0,1,0,0,0,0,0,0];
		p_aMap[11]  = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		p_aMap[12]  = [0,0,0,1,1,1,1,1,1,1,1,1,1,1,1];
		p_aMap[13]  = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1];
		p_aMap[14]  = [0,0,0,0,0,0,0,0,0,0,2,0,0,0,0];
		
		p_aDestinations = [
			new Point(0,3),
			new Point(4,7),
			new Point(9,0),
			new Point(10,9),
			new Point(10,14),
			new Point(13,5)			
		];

		
		// position initiale
		p_aCurrLoc = [11,3];

		// direction initiale
		p_aDirections = ["DROITE","BAS","GAUCHE","HAUT"];
		p_iCurrDirection = 0;
		
		p_iCurrRotation = 0;
		
		// positions des points
		p_iInitPointLoc = new Point(280+8,33+5);
		p_iPtXOffset = 22;
		p_iPtYOffset = 23;
		
				
		// init de la commande
		p_aCommands = new Array();	
		
		// initialisation de l'interface
		initInterface();
				
		// lancement de la partie
		p_bActionOk = true;
		
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		
		
		p_txtCommands.text = "";		
		
		// boutons de direction		
		p_btLeft["value"] = "GA";
		p_btRight["value"] = "DR";
		Application.useLibCursor(p_btLeft, "cursor-finger");
		Application.useLibCursor(p_btRight, "cursor-finger");
		p_btLeft.onRelease = p_btRight.onRelease = function(){
			_parent.addCommand("direction", this["value"]);
		}
		// boutons de sens
		p_btAhead["value"] = "AV";
		p_btBack["value"] = "RE";
		Application.useLibCursor(p_btAhead, "cursor-finger");
		Application.useLibCursor(p_btBack, "cursor-finger");
		p_btAhead.onRelease = p_btBack.onRelease = function(){
			_parent.addCommand("sens", this["value"]);
		}
		
		// boutons chiffrés
		for(var i:Number=0; i<10; i++){
			this["p_btNum"+i]["value"] = i;
			Application.useLibCursor(this["p_btNum"+i], "cursor-finger");
			this["p_btNum"+i].onRelease = function(){
				_parent.addCommand("go", this["value"]);
			}
		}
		
		// boutons C et CE
		Application.useLibCursor(p_btC, "cursor-finger");
		p_btC.onRelease = function(){
			_parent.doC();
		}

		
		// bouton execute
		Application.useLibCursor(p_btExecute, "cursor-finger");
		p_btExecute.onRelease = function(){
			_parent.executeCommands();
		}
		
		// bouton essais cachés
		for(var i:Number=0; i<6; i++){
			this["p_mcTrie"+i]._visible = false;
		}
		
		// cache les solutions
		for(var i in p_aDestinations){
			this["mcDest"+i]._visible = false;
		}
		
		// init de la position de la fourmi
		p_mcFourmi._x = p_iInitPointLoc.x + p_aCurrLoc[1]*p_iPtXOffset;
		p_mcFourmi._y = p_iInitPointLoc.y + p_aCurrLoc[0]*p_iPtYOffset;
		
	}	
	
	
	/**
	 * démarrage d'une partie
	 */
	private function startNewPart(){
		

		
	}
	
	/**
	* Ajoute une commande
	* @param type "direction" ou "go"
	* @param val valeur : string ou number
	*/
	private function addCommand(type:String, val){
		
		p_oSoundPlayer.playASound("bouton",0);
		
		if(p_bActionOk){
	
			// on va comparer à la dernière commande
			var oLastCommand:Object = p_aCommands[p_aCommands.length-1];		
			var oNewCommand:Object = new Object();
			
			if(oLastCommand != null){
			
				switch (type){
					
					case "direction":
						if(oLastCommand.type != "sens"){
							oNewCommand.type = type;
							oNewCommand.value = val;
							p_aCommands.push(oNewCommand);
						}
						break;
						
					case "sens":
						if(oLastCommand.type == "sens"){
							// remplace le sens différent
							p_aCommands.pop();
						}
						oNewCommand.type = type;
						oNewCommand.value = val;
						p_aCommands.push(oNewCommand);
						break;
						
					case "go":
						// seulement après une direction
						if(oLastCommand.type == "sens"){
							oNewCommand.type = type;
							oNewCommand.value = val;
							p_aCommands.push(oNewCommand);
						}else if(oLastCommand.type == "go" && oLastCommand.value==1){
							oNewCommand = p_aCommands.pop();
							oNewCommand.value = Number(String(oNewCommand.value) + String(val));
							p_aCommands.push(oNewCommand);
						}
						break;				
									
				}
				
			}else{

				// il s'agit de la première commande, 
				// donc pas de comparaison avec la précédente
				if(type != "go"){
					oNewCommand.type = type;
					oNewCommand.value = val;
					p_aCommands.push(oNewCommand);			
				}
				
			}
			
			displayCommand(p_aCommands);
			
		}
		
	}
	
	/**
	* action du bouton C
	*/
	private function doC(){		
		
		p_oSoundPlayer.playASound("bouton",0);
		
		if(p_bActionOk){
			
			p_aCommands.pop();
			displayCommand(p_aCommands);
			
		}
		
	}
	
	
	/**
	* execute la liste des commandes
	*/
	private function executeCommands(){		
		
		p_oSoundPlayer.playASound("bouton",0);
		
		if(p_bActionOk){
			
			// vérifie que la dernière commande n'est pas un sens
			if(p_aCommands[p_aCommands.length-1].type != "sens"){				
				this["p_mcTrie"+p_iCurrNbTries]._visible = true;
				p_iCurrNbTries ++;			
				executeOneCommand();
			}
		}
		
	}			
	
	
	/**
	* execute une commande
	*/
	private function executeOneCommand(){	

		if(p_aCommands.length > 0){
		
			// prend la première commande de la liste
			p_oCommand = p_aCommands.shift();
			if(p_oCommand.type == "direction"){
				
				// COMMANDE DIRECTION
				
				if(p_oCommand.value == "DR"){
					
					// modification de la direction en cours
					p_iCurrDirection ++;
					if(p_iCurrDirection == p_aDirections.length){
						p_iCurrDirection = 0;
					}
					// rotation du clip
					if(p_iCurrRotation<270){
						p_iCurrRotation += 90;
					}else{
						p_iCurrRotation = 0;
					}

				}else{
					
					// modification de la direction en cours
					p_iCurrDirection --;
					if(p_iCurrDirection <0){
						p_iCurrDirection = p_aDirections.length - 1;
					}
					// rotation du clip
					if(p_iCurrRotation == -180){
						p_iCurrRotation = 180;
						p_mcFourmi._rotation = 180;
					}else if(p_iCurrRotation == 180){
						p_iCurrRotation = -180;
						p_mcFourmi._rotation = -180;
					}					
					p_iCurrRotation -= 90;

				}

				this[p_mcFourmi._name].rotateTo(p_iCurrRotation,0.5,"easeInOutCubic", 0, reDoSlide);


			}else if(p_oCommand.type == "sens"){
				
				// COMMANDE AVANCER
				// prend la commande suivante pour la distance
				var oCommand2:Object = p_aCommands.shift();
				
				p_iGoSteps = oCommand2.value*2;
				p_iCurrStep = 0;
				doSlide();
								
			}else{
				
				trace("ne devrait jamais arriver");
				
			}
			
		}else{
			
			p_txtCommands.text="";
			if(p_iCurrNbTries == p_iNbMaxTries){
				loose("Tu n'as plus d'essai. Recommence...");
			}
			
		}
		
	}
	
	private function doSlide():Void{
				
		if(p_iCurrStep< p_iGoSteps){			
			
			if(p_oCommand.value == "AV"){
				var vSens = 1
			}else{
				var vSens = -1;
			}

			
			if(p_iCurrStep % 2 == 0){
				
				// 1er passage
			
				var nextLoc:Array = new Array();
				nextLoc[0] = p_aCurrLoc[0];
				nextLoc[1] = p_aCurrLoc[1];

				switch (p_aDirections[p_iCurrDirection]){
					case "DROITE":
						nextLoc[1] += vSens;
						break;
					case "GAUCHE":
						nextLoc[1] -= vSens;
						break;
					case "HAUT":
						nextLoc[0] -= vSens;
						break;
					case "BAS":
						nextLoc[0] += vSens;
						break;
				}

				if(p_aMap[nextLoc[0]][nextLoc[1]] != 1 && p_aMap[nextLoc[0]][nextLoc[1]]!=null){
					// le chemin est libre
					p_iCurrStep++;
					doSlide();
				}else{	
					loose("Recommence...");		
					p_oSoundPlayer.playASound("lost",0);
					var mcExplosion:MovieClip = attachMovie("mcExplosion", "mcExplosion", 300);
					mcExplosion._x = p_mcFourmi._x;
					mcExplosion._y = p_mcFourmi._y;
					mcExplosion._rotation = p_mcFourmi._rotation;
					if(vSens == -1)mcExplosion._rotation += 180;
				}
			
			}else{			
				
				// 2e passage on fait le slide
				p_mcFourmi._rotation = p_iCurrRotation;		
						
				switch (p_aDirections[p_iCurrDirection]){
					case "DROITE":
						p_aCurrLoc[1] += vSens;
						break;
					case "GAUCHE":
						p_aCurrLoc[1] -= vSens;
						break;
					case "HAUT":
						p_aCurrLoc[0] -= vSens;
						break;
					case "BAS":
						p_aCurrLoc[0] += vSens;
						break;
				}

				var nextPt:Point = getPixCoords(p_aCurrLoc);

				p_iCurrStep++;
								
				this[p_mcFourmi._name].xSlideTo(nextPt.x, 0.2, "linear");
				this[p_mcFourmi._name].ySlideTo(nextPt.y, 0.2, "linear", 0, reDoSlide);
				
			}			
							
		}else{
			executeOneCommand();
		}
		
	}
	
	private function reDoSlide():Void{
		
		// ici, this est p_mcFourmi
		
		if( _parent.checkIfOnAPoint(_parent.p_aCurrLoc) ){
			// on est sur une soluce, on test si la partie est finie
			if(_parent.areAllFound()){
				_parent.won();
			}else{
				_parent.doSlide();
			}			
		}else{
			_parent.doSlide();
		}
		
	}
	
	private function checkIfOnAPoint(aLoc:Array):Boolean{
		
		for(var i in p_aDestinations){
			
			if(p_aDestinations[i].x == aLoc[1] && p_aDestinations[i].y == aLoc[0]){
				// sur un point clé
				this["mcDest"+i]._visible = true;
				p_aDestinations[i] = null;
				p_oSoundPlayer.playASound("ding",0);
				return true
			}
			
		}
		
		return false;
		
	}
	
	private function areAllFound():Boolean{
		
		for(var i in p_aDestinations){
			if(p_aDestinations[i] != null){
				return false;
			}
		}
		return true;
		
	}
	
	
	
	/** renvois les coordonnées en pixels dans la map */
	private function getPixCoords(loc:Array){
		var res:Point = new Point();
		res.x = p_iInitPointLoc.x + loc[1] * p_iPtXOffset;
		res.y = p_iInitPointLoc.y + loc[0] * p_iPtYOffset;
		return res;
	}


	/**
	* Affichage de la commande dans la zone de texte
	*/
	private function displayCommand(aCommand:Array){
		
		var txtCommand:String = "";
		for (var i:Number=0; i<aCommand.length; i++){
			txtCommand += String(aCommand[i].value);
			if(aCommand[i].type != "sens"){
				txtCommand += "\n";
			}
		}
		p_txtCommands.text = txtCommand;
		
	}
	
	
	
	
}

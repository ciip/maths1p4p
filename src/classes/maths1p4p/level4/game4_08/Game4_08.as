﻿/**
 * class com.maths1p4p.level4.game4_08.Game4_08
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level4.game4_08.Game4_08 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** les différents boutons de l'interface de commande */
	public var p_btPlus, p_btMoins, p_btFois, p_btDivise, p_btEgal, p_btCancel, p_btDepartSuite, p_btReset:Button;


	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** la liste des questions possibles */
	private var p_aQuestions:Array;
	
	/** l'opération en cours */
	private var p_oCurrOperation:Object;
	
	/** le nombre à trouver */
	private var p_iNumberToFind:Number;
	

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_08()	{		
		super();		
		startGame();
		
	}
	
	/**
	 * ajoute un nombre à l'opération en cours
	 * @param	iNumber le nombre à ajouter
	 */
	public function addNumber(iNumber:Number, mcCall:MovieClip):Void{
		
		p_oSoundPlayer.playASound("bouton",0);
		
		if(p_oCurrOperation.A == null){			
			p_oCurrOperation.A = iNumber;		
			p_oCurrOperation.mcA = mcCall;
			mcCall.setActive(false);
		}else if(p_oCurrOperation.operator != null && p_oCurrOperation.B == null){		
			p_oCurrOperation.B = iNumber;			
			p_oCurrOperation.mcB = mcCall;
			mcCall.setActive(false);
		}else{
			trace("ce n'est pas le meoment d'ajouter un nombre");
		}
		
		updateOperation();
		
	}
	
	/**
	 * définit l'opérateur
	 * @param	sOperator l'opérateur
	 */
	public function setOperator(sOperator:String):Void{
		
		p_oSoundPlayer.playASound("bouton",0);
		
		if(p_oCurrOperation.operator == null && p_oCurrOperation.A != null){	
			p_oCurrOperation.operator = sOperator;
		}
		
		updateOperation();
		
	}
	
	public function cancel():Void{
		
		p_oSoundPlayer.playASound("bouton",0);
		
		if(p_oCurrOperation.B != null){
			p_oCurrOperation.B = null;
			p_oCurrOperation.mcB.setActive(true);			
		}else if(p_oCurrOperation.operator != null){
			p_oCurrOperation.operator = null;
		}else if (p_oCurrOperation.A != null){
			p_oCurrOperation.A = null;
			p_oCurrOperation.mcA.setActive(true);	
		}
		
		updateOperation();		
		
	}
	
	public function reset():Void{	
		p_oSoundPlayer.playASound("bouton",0);
		startNewPart()		
	}	
	
	
	public function egal():Void{
		
		p_oSoundPlayer.playASound("egal",0);
		
		// calcule l'opération
		if(p_oCurrOperation.operator != null && p_oCurrOperation.A != null && p_oCurrOperation.B != null){		
			// calcul du resultat
			switch (p_oCurrOperation.operator){
				case "+":
					p_oCurrOperation.result = p_oCurrOperation.A + p_oCurrOperation.B;
					break;
				case "-":
					p_oCurrOperation.result = p_oCurrOperation.A - p_oCurrOperation.B;
					break;
				case "x":
					p_oCurrOperation.result = p_oCurrOperation.A * p_oCurrOperation.B;
					break;
				case "÷":
					if(p_oCurrOperation.A % p_oCurrOperation.B == 0){
						p_oCurrOperation.result = p_oCurrOperation.A / p_oCurrOperation.B;
					}else{
						return;
					}
					break;
			}
			
			// affichage du résultat
			this["p_mcResult"+p_iCurrNbTries].setNumber(p_oCurrOperation.result);
			
			// check si gagné
			if(p_oCurrOperation.result == p_iNumberToFind){
				// gagné				
				this["mcPlanet"+p_iNbPartsWon]._visible = true;
				p_iNbPartsWon++;
				if(p_iNbPartsWon == p_iNbPartsToWin){
					setAllButtonsEnabled(false);
					won();
				}else{
					this["txtDepartSuite"].text = "Suite";
					p_btDepartSuite.onRelease = function(){
						_parent.startNewPart();			
					}
					this["txtBravo"].text = "Bravo, tu as trouvé !";
				}
			}else{
				p_oCurrOperation = getOperationObject();
				p_iCurrNbTries++;
				if(p_iCurrNbTries==p_iNbMaxTries){
					setAllButtonsEnabled(false);
					this["txtBravo"].text = "Tu n'as pas trouvé !";
					loose();
				}
			}			
			
		}
		
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){		
		
		p_iNbMaxTries = 5;
		p_iCurrNbTries = 0;
		p_iNbPartsToWin = 3;
		p_iNbPartsWon = 0;
		
		// initialisation des propriétés
		p_aQuestions = [		
			[2, 3, 7, 4, 25, 25, 135],
			[6, 6, 9, 3, 50, 25, 142],
			[3, 6, 2, 2, 25, 10, 168],
			[3, 8, 5, 9, 25, 100, 174],
			[4, 9, 3, 2, 50, 100, 178],
			[6, 6, 1, 2, 50, 10, 180],
			[2, 3, 7, 2, 50, 50, 186],
			[6, 5, 7, 6, 25, 10, 196],
			[6, 2, 6, 2, 25, 100, 203],
			[4, 3, 3, 6, 50, 10, 216],
			[5, 2, 1, 5, 50, 25, 218],
			[6, 4, 6, 2, 25, 50, 233],
			[2, 1, 3, 6, 100, 25, 249],
			[6, 6, 6, 3, 10, 25, 268],
			[4, 8, 6, 8, 100, 50, 292],
			[5, 9, 3, 6, 50, 25, 334],
			[4, 3, 9, 4, 10, 50, 340],
			[3, 4, 3, 3, 25, 25, 347],
			[4, 2, 3, 9, 25, 50, 348],
			[5, 4, 2, 6, 100, 25, 369],
			[1, 6, 4, 1, 50, 25, 396],
			[2, 5, 1, 3, 25, 25, 513],
			[9, 6, 8, 2, 50, 10, 519],
			[5, 4, 7, 1, 50, 50, 535],
			[1, 4, 6, 6, 100, 50, 556],
			[9, 5, 3, 2, 100, 25, 573],
			[5, 6, 6, 3, 100, 25, 574],
			[3, 1, 4, 6, 100, 25, 597],
			[2, 4, 2, 5, 100, 10, 655],
			[3, 6, 2, 3, 25, 100, 672]			
		];
		
		// initialisation de l'interface
		initInterface();
		
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		
		
		setAllButtonsEnabled(false);
		
		Application.useLibCursor(p_btPlus, "cursor-finger");
		p_btPlus.onRelease = function(){
			_parent.setOperator("+");
		}
		Application.useLibCursor(p_btMoins, "cursor-finger");
		p_btMoins.onRelease = function(){
			_parent.setOperator("-");
		}
		Application.useLibCursor(p_btFois, "cursor-finger");
		p_btFois.onRelease = function(){
			_parent.setOperator("x");
		}
		Application.useLibCursor(p_btDivise, "cursor-finger");
		p_btDivise.onRelease = function(){
			_parent.setOperator("÷");
		}
		Application.useLibCursor(p_btEgal, "cursor-finger");
		p_btEgal.onRelease = function(){
			_parent.egal();
		}
		Application.useLibCursor(p_btCancel, "cursor-finger");
		p_btCancel.onRelease = function(){
			_parent.cancel();
		}
		Application.useLibCursor(p_btReset, "cursor-finger");
		p_btReset.onRelease = function(){
			_parent.reset();
		}		
		
		this["mcPlanet0"]._visible = this["mcPlanet1"]._visible = this["mcPlanet2"]._visible = false;
		
		Application.useLibCursor(p_btDepartSuite, "cursor-finger");
		p_btDepartSuite.onRelease = function(){
			_parent.startNewPart();			
		}
		
	}	
	
	/**
	 * démarrage d'une partie
	 */
	private function startNewPart(bRestart:Boolean){
		
		this["txtBravo"].text = "";
		this["txtDepartSuite"].text = "Recommencer";
		p_btDepartSuite.onRelease = function(){
			_parent.startNewPart(true);			
		}
		
		p_iCurrNbTries = 0;
		
		var aCurrQuestion = p_aQuestions[Math.floor(Math.random()*p_aQuestions.length)];
		
		// init des nombres
		for(var i=0;i<6;i++){			
			this["p_mcNumber"+i].setNumber(aCurrQuestion[i]);			
			this["p_mcNumber"+i].setActive(true);
			this["txtOperation"+i].text = "";
			this["p_mcResult"+i].init();
		}
		
		p_iNumberToFind = aCurrQuestion[6];		
		this["txtNumberToFind"].text = String(p_iNumberToFind);
		p_oCurrOperation = getOperationObject();
		
		setAllButtonsEnabled(true);
		
	}
	
	/** update l'affichage de l'opération en cours $*/
	private function updateOperation():Void{
		
		var sOperation:String = "";
		if(p_oCurrOperation.A != null) sOperation += p_oCurrOperation.A + " ";
		if(p_oCurrOperation.operator != null) sOperation += p_oCurrOperation.operator + " ";
		if(p_oCurrOperation.B != null) sOperation += p_oCurrOperation.B + " ";

		this["txtOperation"+p_iCurrNbTries].text = sOperation;
		
	}
	
	private function setAllButtonsEnabled(bVal:Boolean):Void{
		p_btPlus.enabled = p_btMoins.enabled = p_btFois.enabled = p_btDivise.enabled = p_btEgal.enabled = p_btCancel.enabled = bVal;
	}
	
	/**
	 * fabrique un objet operation vide
	 */
	private function getOperationObject():Object{		
		var o:Object = new Object();
		o.A = null;
		o.B = null;
		o.operator = null;
		o.result = null;
		o.mcA = null;
		o.mcB = null;
		return o;		
	}
	
	
}

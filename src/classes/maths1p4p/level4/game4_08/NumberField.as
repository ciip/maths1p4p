﻿/**
 * class com.maths1p4p.level4.game4_08.NumberField
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level4.game4_08.NumberField extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_iNumber:Number;
	private var p_mcBG:MovieClip;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------

	public function NumberField(){		
		init();
		Application.useLibCursor(this, "cursor-finger");
	}

	public function init(){
		p_mcBG._visible = false;
		enabled = false;
		this["txtNumber"].text = "";
	}
	
	public function setNumber(iNumber:Number):Void{
		p_iNumber = iNumber;
		this["txtNumber"].text = String(iNumber);
		setActive(true);
	}
	
	public function onRelease():Void{
		_parent.addNumber(p_iNumber, this);
	}
	
	public function setActive(bActive:Boolean):Void{
		enabled = bActive;
		p_mcBG._visible = !bActive;		
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	
	
	
}

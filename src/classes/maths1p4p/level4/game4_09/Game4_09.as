﻿/**
 * class com.maths1p4p.level4.game4_09.Game4_09
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import flash.geom.Point;
import maths1p4p.application.Application;
import maths1p4p.Maths.Operations.OperationMaker;
import maths1p4p.utils.OutilsTableaux;

class maths1p4p.level4.game4_09.Game4_09 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
		
	public var p_mcPlayer1:MovieClip;
	public var p_mcPlayer2:MovieClip;
	public var p_mcPlayer1Win:MovieClip;
	public var p_mcPlayer2Win:MovieClip;
	
	public var p_mcBars:MovieClip;


	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** joueurs */
	private var p_oPlayer1, p_oPlayer2;

	/** tour en cours */
	private var p_iCurrTurn:Number;
	
	/** l'operation en cours */
	private var p_oCurrOperation:Object;	
	/** l'element de l'opération à trouver */
	private var p_iCurrItemToFind:String;	
	
	/** bar en cours */
	private var p_mcCurrBar:MovieClip;
	
	/** les maps des 2 joueurs */
	private var p_aMapPlayer1:Array;
	private var p_aMapPlayer2:Array;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_09()	{		
		super();	
	}	
	
	/**
	 * secondPlayerSelected
	 * appellé depuis level3
	 */
	public function secondPlayerSelected(oPlayer2:Object):Void{
		
		p_oPlayer1 = new Object();
		p_oPlayer1.name = _parent._parent.getPlayer().getName();
		p_oPlayer1.id = _parent._parent.getPlayer().getId();
		p_oPlayer2 = oPlayer2;
		
		initInterface();
		
	}	
	
		
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		/** le nombre de parties à gagner */
		p_iNbPartsToWin = 3;
		/** le nombre de parties gagnées*/
		p_iNbPartsWon = 0;		
		
		
		/** init map player1 */
		p_aMapPlayer1 = new Array(11);
		for(var i=0;i<p_aMapPlayer1.length;i++){
			p_aMapPlayer1[i] = new Array(9);
			if(i%2==0){
				for(var j=0; j<p_aMapPlayer1[i].length;j+=2){
					p_aMapPlayer1[i][j] = 1;
				}
			}
		}
		/** init map player2 */
		p_aMapPlayer2 = new Array(9);
		for(var i=0;i<p_aMapPlayer2.length;i++){
			p_aMapPlayer2[i] = new Array(11);
			if(i%2==0){
				for(var j=0; j<p_aMapPlayer2[i].length;j+=2){
					p_aMapPlayer2[i][j] = 1;
				}
			}
		}		
		
		// affichage de l'écran de sélection du joueur 2
		if(_parent._parent != null){
			_parent._parent.showSecondPlayerSelectionScreen();
		}else{
			// mode autonome
			p_oPlayer1 = new Object();
			p_oPlayer1.name = "joueur 1";
			p_oPlayer1.id = 1;
			
			p_oPlayer2 = new Object();
			p_oPlayer2.name = "joueur 2";
			p_oPlayer2.id = 2;
			
			// init de l'interface
			initInterface();
			
		}
		
	}	



	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	

		p_mcPlayer1._visible = p_mcPlayer2._visible = false;		
		p_mcPlayer1Win._visible = p_mcPlayer2Win._visible = false;
		this["mcPlanet0"]._visible = this["mcPlanet1"]._visible = false;		
		
		this["txtPlayer1"].text = p_oPlayer1.name;
		this["txtPlayer2"].text = p_oPlayer2.name;
		
		
		for(var i in p_mcBars){
			
			p_mcBars[i]._alpha = 0;
			
			var sMcName:String = p_mcBars[i]._name;
			var a:Array = sMcName.split("_");
			// ajustement des ids...
			for(var ii =1; ii<a.length; ii++){
				a[ii] = a[ii]*2;
			}			
			
			if(a[0] == "mcPlayer1"){
				p_mcBars[i]["playerId"] = 1;
			}else{
				p_mcBars[i]["playerId"] = 2;
			}
			p_mcBars[i]["pt1"] = new Point(a[1], a[2]);
			p_mcBars[i]["pt2"] = new Point(a[3], a[4]);		
			if(a[1] != a[3]){
				p_mcBars[i]["x"] = a[1]+1;
			}else{
				p_mcBars[i]["x"] = a[1];
			}
			if(a[2] != a[4]){
				p_mcBars[i]["y"] = a[2]+1;
			}else{
				p_mcBars[i]["y"] = a[2];
			}
			
			p_mcBars[i]["finished"] = false;
			Application.useLibCursor(p_mcBars[i], "cursor-finger");
			p_mcBars[i].onRelease = function(){
				_parent._parent.clickOnBar(this);
			}
			
		}
		
		newPart();
		
	}	
	
	/**
	 * Nouvelle question
	 */
	private function newPart():Void{
		
		p_iCurrTurn = Math.floor(Math.random()*2);
		nextTurn();

	}
	
	private function nextTurn(){
		
		if(p_iCurrTurn==0){
			p_iCurrTurn = 1;
		}else{
			p_iCurrTurn = 0;
		}			
				
		var iCurrPlayer:Number = p_iCurrTurn+1;
		setPlayerActive(iCurrPlayer);
		
		for(var i in p_mcBars){
			// inactive les bars de l'autre joueur
			if(p_mcBars[i].playerId == iCurrPlayer){
				p_mcBars[i].enabled = !p_mcBars[i].finished;
				p_mcBars[i].swapDepths(p_mcBars.getNextHighestDepth());
			}else{
				p_mcBars[i].enabled = false;
			}			
		}

	}
	
	
	private function setPlayerActive(iPlayerId:Number){
		p_mcPlayer1._visible = false;
		p_mcPlayer2._visible = false;
		this["p_mcPlayer"+String(iPlayerId)]._visible = true;
	}
	
	
	private function clickOnBar(mcBar:MovieClip){
		
		p_mcCurrBar = mcBar;
		p_oSoundPlayer.playASound("barre",0);
		
				
		// vérifie si la barre n'en croise pas une autre
		var barOk:Boolean = true;
		for(var i in p_mcBars){
			if(p_mcCurrBar.hitTest(p_mcBars[i])){
				if(p_mcBars[i].finished && p_mcBars[i].playerId!=p_mcCurrBar.playerId){
					barOk = false;
				}
			}
		}
		
		if(barOk){
		
			freezeBars();
			p_mcCurrBar._alpha = 50;
			
			// nouvelle opération
			var iPlayerId:Number = p_mcCurrBar.playerId;
			var typeOperation:Number = Math.floor(Math.random()*2);
			p_iCurrItemToFind = ["A","B","R"][Math.floor(Math.random()*3)];
			
			if(typeOperation == 0){
				// addition
				p_oCurrOperation = OperationMaker.getAddition(50,700);
			}else{
				// soustraction
				p_oCurrOperation = OperationMaker.getSoustraction(50,700);
			}
			this["p_mcPlayer"+iPlayerId].mcOperation.init(p_oCurrOperation, p_iCurrItemToFind);
			
			Key.addListener(this);
			
		}

	}
	
	/** onKeyDown */
	public function onKeyDown():Void{
		if(Key.getCode() == Key.ENTER){
			checkCurrResult();
		}
	}	
	
	private function checkCurrResult():Boolean{
		
		//Key.removeListener(this);		
		
		if(this["p_mcPlayer"+(p_iCurrTurn+1)].mcOperation.getResponse() == p_oCurrOperation[p_iCurrItemToFind]){
			// bonne réponse
			p_oSoundPlayer.playASound("ok",0);
			p_mcCurrBar.finished = true;
			p_mcCurrBar._alpha = 100;
			delete p_mcCurrBar.onRelease;
			
			// ajout au tableau du joueur en cours
			this["p_aMapPlayer"+(p_iCurrTurn+1)] [p_mcCurrBar["x"]][p_mcCurrBar["y"]] = 1;
			if(checkPath(p_iCurrTurn+1)){
				setVictory(this["p_oPlayer"+(p_iCurrTurn+1)]);
			}
			
			this["p_mcPlayer"+(p_iCurrTurn+1)].mcOperation.hide();
			nextTurn();			
			
		}else{
			// mauvaise réponse
			p_oSoundPlayer.playASound("error",0);
			this["p_mcPlayer"+(p_iCurrTurn+1)].mcOperation.selectCurrTextField();
			return false;
		}		
		

		
	}
	
	private function freezeBars(){
		for(var i in p_mcBars){
			p_mcBars[i].enabled = false;
		}
	}
	
	private function setVictory(oPlayer:Object){
		
		if(oPlayer == p_oPlayer1){
			// c'est le premier joueur qui gagne
			p_mcPlayer1Win._visible = true;
			this["mcPlanet0"]._visible = true;
			won(1);
		}else{
			// c'est le 2e joueur qui gagne
			p_mcPlayer2Win._visible = true;
			p_mcLoots._x = 513;
			this["mcPlanet1"]._visible = true;
			p_mcLoots.gotoAndStop("objsuppl");
			won(2);
		}		
		
	}	
	
	
	private function checkPath(iPlayerId:Number):Boolean{
		
		var aPlayer:Array = this["p_aMapPlayer"+iPlayerId];
		
		if(iPlayerId==1){
			for(var i=0; i<aPlayer[0].length; i+=2){
				var aPath:Array = getPath(aPlayer,0,i, [new Point(0,i)]);
				for(var j=0; j<aPlayer.length;j++){
					if(isInArray(aPath, new Point(aPlayer.length-1, j))){
						return true;
					}
				}
			}
		}else{
			for(var i=0; i<aPlayer.length; i+=2){
				var aPath:Array = getPath(aPlayer,i,0, [new Point(i,0)]);
				for(var j=0; j<aPlayer[0].length;j++){
					if(isInArray(aPath, new Point(j, aPlayer[0].length-1))){
						return true;
					}
				}
			}
		}
						
		return false;		
		
	}
	
	private function getPath(aArr:Array, iBegin:Number, jBegin:Number, aTilesOk:Array):Array{
		
		aTilesOk = OutilsTableaux.getCopie(aTilesOk);
		
		// test en haut
		if(aArr[iBegin][jBegin-1]==1 && !isInArray(aTilesOk, new Point(iBegin, jBegin-1))){
			aTilesOk.push(new Point(iBegin, jBegin-1));
			var aNewPath:Array = getPath(aArr, iBegin, jBegin-1, aTilesOk);
			for(var k=0; k<aNewPath.length;k++){
				aTilesOk.push(aNewPath[k]);
			}			
		}
		// test en bas
		if(aArr[iBegin][jBegin+1]==1 && !isInArray(aTilesOk, new Point(iBegin, jBegin+1))){
			aTilesOk.push(new Point(iBegin, jBegin+1));
			var aNewPath:Array = getPath(aArr, iBegin, jBegin+1, aTilesOk)
			for(var k=0; k<aNewPath.length;k++){
				aTilesOk.push(aNewPath[k]);
			}			
		}
		// test à gauche
		if(aArr[iBegin-1][jBegin]==1 && !isInArray(aTilesOk, new Point(iBegin-1, jBegin))){
			aTilesOk.push(new Point(iBegin-1, jBegin));
			var aNewPath:Array = getPath(aArr, iBegin-1, jBegin, aTilesOk);
			for(var k=0; k<aNewPath.length;k++){
				aTilesOk.push(aNewPath[k]);
			}			
		}
		// test à droite
		if(aArr[iBegin+1][jBegin]==1 && !isInArray(aTilesOk, new Point(iBegin+1, jBegin))){
			aTilesOk.push(new Point(iBegin+1, jBegin));
			var aNewPath:Array = getPath(aArr, iBegin+1, jBegin, aTilesOk);
			for(var k=0; k<aNewPath.length;k++){
				aTilesOk.push(aNewPath[k]);
			}			
		}		
		
		return aTilesOk;
		
	}
	
	private function isInArray(arr:Array, pt:Point):Boolean{
		for(var i=0;i<arr.length;i++){
			if(arr[i].x==pt.x && arr[i].y==pt.y){
				return true;
			}
		}
		return false;
	}

}

﻿/**
 * class com.maths1p4p.level3.game3_10.Operation
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import flash.geom.Point;

class maths1p4p.level4.game4_09.Operation extends MovieClip {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** l'éléments A */
	public var p_txtA:TextField;
	/** l'éléments B */
	public var p_txtB:TextField;
	/** l'éléments R, resultat */
	public var p_txtR:TextField;
	
	/** un tableau de lien vers les textfields */
	public var p_oFields:Object;
	
	/** l'opérateur */
	public var p_txtOperator:TextField;
	
		
		
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	/** l'objet opération */
	private var p_oOperation:Object;
	/** l'item a trouver */
	private var p_sItemToFind:String;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Operation(){	
		
		hide();
		
		p_txtA = this["p_txtA"];
		p_txtB = this["p_txtB"];
		p_txtR = this["p_txtR"];
		p_txtOperator = this["p_txtOperator"];
		
		p_oFields = new Object();
		p_oFields.A = p_txtA;
		p_oFields.B = p_txtB;
		p_oFields.R = p_txtR;
		p_oFields.operator = p_txtOperator;

	}
	
	/** initialisations */
	public function init(oOperation:Object, sItem:String):Void{

		p_oOperation = oOperation;
		p_sItemToFind = sItem;
		
		// affichage de l'opération
		for (var i in p_oOperation){
			p_oFields[i].text = p_oOperation[i];
			p_oFields[i].type = "dynamic";
			p_oFields[i].selectable = false;
		}
		
		// masque le champs p_sItemToFind et le rend editable

		p_oFields[p_sItemToFind].text = "";
		p_oFields[p_sItemToFind].type = "input";
		p_oFields[p_sItemToFind].selectable = true;
		p_oFields[p_sItemToFind].restrict = "0123456789";
		p_oFields[p_sItemToFind].maxChars = 4;

		Selection.setFocus(p_oFields[p_sItemToFind]);
		
		Key.addListener(this);
		
		show();
		
	}

	
	/**
	 * Renvois le nombre saisi
	 */
	public function getResponse():Number{
		return Number(p_oFields[p_sItemToFind].text);
	}
	
	
	/** Affiche l'opération */
	public function show():Void{
		_visible = true;
	}
	
	/** Cache l'opération */
	public function hide():Void{
		_visible = false;
	}
	
	public function selectCurrTextField(){
		trace("sel");
		Selection.setFocus(p_oFields[p_sItemToFind]);
		Selection.setSelection(0, p_oFields[p_sItemToFind].text.length);
	}
	
	/**
	 * On tape Enter dans le champs de saisie
	 */
	private function onKeyDown(){
		
		if(Key.ENTER == Key.getCode()){
			trace("kydown");
			var iReponse:Number = getResponse();
			if(_parent.checkResponse(iReponse)){
				trace("remove listener");
				Key.removeListener(this);
			}
		}
		
	}
	
}

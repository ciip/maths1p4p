﻿/**
 * class com.maths1p4p.level4.game4_10.Cle
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;

class maths1p4p.level4.game4_10.Cle extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var p_btUp:Button;
	public var p_btDown:Button;
	public var p_btLeft:Button;
	public var p_btRight:Button;
	
	public var p_sType:String;
	public var p_iLength:Number;
	
	public var p_iX:Number;
	public var p_iY:Number;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Cle()	{		

		Application.useLibCursor(p_btUp, "cursor-finger");		
		p_btUp.onRelease = function(){
			_parent.moveDown();
		}
		Application.useLibCursor(p_btDown, "cursor-finger");
		p_btDown.onRelease = function(){
			_parent.moveUp();
		}
		Application.useLibCursor(p_btLeft, "cursor-finger");
		p_btLeft.onRelease = function(){
			_parent.moveRight();
		}
		Application.useLibCursor(p_btRight, "cursor-finger");
		p_btRight.onRelease = function(){
			_parent.moveLeft();
		}
			
	}	
	
		
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	private function moveDown(){
		if(_parent._parent.isTileFree(p_iX, p_iY + p_iLength)){
			_parent._parent.moveCle(this, 0, 1);
		}
	}
	private function moveUp(){
		if(_parent._parent.isTileFree(p_iX, p_iY -1)){
			_parent._parent.moveCle(this, 0, -1);
		}
	}
	private function moveRight(){
		if(_parent._parent.isTileFree(p_iX + p_iLength, p_iY)){
			_parent._parent.moveCle(this, 1, 0);
		}
	}
	private function moveLeft(){
		if(_parent._parent.isTileFree(p_iX -1, p_iY)){
			_parent._parent.moveCle(this, -1, 0);
		}
	}
	
	
}

﻿/**
 * class com.maths1p4p.level4.game4_10.Game4_10
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;

class maths1p4p.level4.game4_10.Game4_10 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
		
	/** bouton suite/départ */
	public var p_btDepartSuite
	
	private var p_btAntiClic:Button;
	
	


	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** alignements x,y */
	private var p_iTopX, p_iTopY:Number;	
	/** offset x et y */
	private var p_iOffsetX, p_iOffsetY:Number;
	
	/** la grille du jeu */
	private var p_aGrid:Array;
	
	/** liste des solutions */
	private var p_aSoluces:Array;
	
	/** id de la partie en cours */
	private var p_iCurrPartId:Number;
	
	/** clip de la grille */
	private var p_mcGrid:MovieClip;
	
	private var p_iFrameCount:Number

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_10()	{		
		super();	
	}	
	
		
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		/** le nombre de parties à gagner */
		p_iNbPartsToWin = 3;
		/** le nombre de parties gagnées*/
		p_iNbPartsWon = 0;		
		
		p_iTopX = 65;
		p_iTopY = 51;
		p_iOffsetX = 39;
		p_iOffsetY = 40;
		

				
		p_aSoluces = new Array(3);		
		
		p_aSoluces[0] = [];
		p_aSoluces[0][0] = [];
		p_aSoluces[0][0][0] = {type:"mcCleOr" , x:0 , y:3};
		p_aSoluces[0][0][1] = {type:"mcCleVerticale3" , x:2 , y:2};
		p_aSoluces[0][0][2] = {type:"mcCleVerticale2" , x:2 , y:5};
		p_aSoluces[0][0][3] = {type:"mcCleHorizontale3" , x:3 , y:2};
		p_aSoluces[0][0][4] = {type:"mcCleHorizontale3" , x:3 , y:6};
		p_aSoluces[0][0][5] = {type:"mcCleVerticale3" , x:5 , y:3};
		p_aSoluces[0][1] = [];
		p_aSoluces[0][1][0] = {type:"mcCleOr" , x:0 , y:3};
		p_aSoluces[0][1][1] = {type:"mcCleVerticale3" , x:2 , y:2};
		p_aSoluces[0][1][2] = {type:"mcCleHorizontale3" , x:2 , y:6};
		p_aSoluces[0][1][3] = {type:"mcCleHorizontale3" , x:3 , y:0};
		p_aSoluces[0][1][4] = {type:"mcCleVerticale3" , x:5 , y:1};
		p_aSoluces[0][1][5] = {type:"mcCleVerticale2" , x:5 , y:4};
		p_aSoluces[0][2] = [];
		p_aSoluces[0][2][0] = {type:"mcCleOr" , x:0 , y:3};
		p_aSoluces[0][2][1] = {type:"mcCleHorizontale2" , x:0 , y:0};
		p_aSoluces[0][2][2] = {type:"mcCleHorizontale3" , x:2 , y:0};
		p_aSoluces[0][2][3] = {type:"mcCleVerticale3" , x:2 , y:2};
		p_aSoluces[0][2][4] = {type:"mcCleHorizontale3" , x:1 , y:6};
		p_aSoluces[0][2][5] = {type:"mcCleHorizontale2" , x:4 , y:6};
		p_aSoluces[0][2][6] = {type:"mcCleVerticale3" , x:5 , y:2};
		
		p_aSoluces[1] = [];
		p_aSoluces[1][0] = [];
		p_aSoluces[1][0][0] = {type:"mcCleOr" , x:0 , y:3};
		p_aSoluces[1][0][1] = {type:"mcCleHorizontale3" , x:1 , y:0};
		p_aSoluces[1][0][2] = {type:"mcCleHorizontale2" , x:4 , y:0};
		p_aSoluces[1][0][3] = {type:"mcCleVerticale3" , x:2 , y:1};
		p_aSoluces[1][0][4] = {type:"mcCleHorizontale3" , x:3 , y:1};
		p_aSoluces[1][0][5] = {type:"mcCleVerticale2" , x:5 , y:2};
		p_aSoluces[1][0][6] = {type:"mcCleVerticale3" , x:5 , y:4};
		p_aSoluces[1][0][7] = {type:"mcCleHorizontale2" , x:0 , y:6};
		p_aSoluces[1][0][8] = {type:"mcCleHorizontale3" , x:2 , y:6};
		p_aSoluces[1][1] = [];
		p_aSoluces[1][1][0] = {type:"mcCleOr" , x:0 , y:3};
		p_aSoluces[1][1][1] = {type:"mcCleVerticale3" , x:0 , y:4};
		p_aSoluces[1][1][2] = {type:"mcCleVerticale3" , x:1 , y:0};
		p_aSoluces[1][1][3] = {type:"mcCleVerticale3" , x:3 , y:2};
		p_aSoluces[1][1][4] = {type:"mcCleHorizontale3" , x:2 , y:0};
		p_aSoluces[1][1][5] = {type:"mcCleHorizontale3" , x:2 , y:5};
		p_aSoluces[1][2] = [];
		p_aSoluces[1][2][0] = {type:"mcCleOr" , x:2 , y:3};
		p_aSoluces[1][2][1] = {type:"mcCleHorizontale2" , x:0 , y:3};
		p_aSoluces[1][2][2] = {type:"mcCleHorizontale3" , x:0 , y:0};
		p_aSoluces[1][2][3] = {type:"mcCleHorizontale3" , x:3 , y:0};
		p_aSoluces[1][2][4] = {type:"mcCleHorizontale2" , x:2 , y:2};
		p_aSoluces[1][2][5] = {type:"mcCleHorizontale3" , x:2 , y:4};
		p_aSoluces[1][2][6] = {type:"mcCleVerticale2" , x:4 , y:2};
		p_aSoluces[1][2][7] = {type:"mcCleVerticale3" , x:5 , y:2};
		p_aSoluces[1][2][8] = {type:"mcCleVerticale2" , x:2 , y:5};
		p_aSoluces[1][2][9] = {type:"mcCleHorizontale3" , x:3 , y:5};
		p_aSoluces[1][2][10] = {type:"mcCleHorizontale3" , x:3 , y:6};
		p_aSoluces[1][3] = [];
		p_aSoluces[1][3][0] = {type:"mcCleOr" , x:0 , y:3};
		p_aSoluces[1][3][1] = {type:"mcCleVerticale3" , x:0 , y:0};
		p_aSoluces[1][3][2] = {type:"mcCleVerticale3" , x:1 , y:0};
		p_aSoluces[1][3][3] = {type:"mcCleVerticale3" , x:2 , y:3};
		p_aSoluces[1][3][4] = {type:"mcCleVerticale3" , x:3 , y:3};
		p_aSoluces[1][3][5] = {type:"mcCleVerticale3" , x:4 , y:3};
		p_aSoluces[1][3][6] = {type:"mcCleVerticale3" , x:5 , y:3};
		p_aSoluces[1][3][7] = {type:"mcCleVerticale2" , x:0 , y:5};
		p_aSoluces[1][3][8] = {type:"mcCleVerticale2" , x:1 , y:5};
		p_aSoluces[1][3][9] = {type:"mcCleHorizontale2" , x:2 , y:6};
		p_aSoluces[1][3][10] = {type:"mcCleHorizontale2" , x:4 , y:6};
		p_aSoluces[1][3][11] = {type:"mcCleHorizontale3" , x:3 , y:0};
		
		p_aSoluces[2] = [];
		p_aSoluces[2][0] = [];
		p_aSoluces[2][0][0] = {type:"mcCleOr" , x:0 , y:3};
		p_aSoluces[2][0][1] = {type:"mcCleVerticale3" , x:0 , y:0};		
		p_aSoluces[2][0][2] = {type:"mcCleHorizontale3" , x:2 , y:0};		
		p_aSoluces[2][0][3] = {type:"mcCleHorizontale3" , x:2 , y:1};		
		p_aSoluces[2][0][4] = {type:"mcCleHorizontale3" , x:2 , y:6};	
		p_aSoluces[2][0][5] = {type:"mcCleVerticale3" , x:2 , y:3};
		p_aSoluces[2][0][6] = {type:"mcCleVerticale3" , x:3 , y:3};
		p_aSoluces[2][0][7] = {type:"mcCleVerticale3" , x:5 , y:4};
		p_aSoluces[2][0][8] = {type:"mcCleVerticale2" , x:1 , y:5};
		p_aSoluces[2][0][9] = {type:"mcCleVerticale2" , x:5 , y:0};
		p_aSoluces[2][1] = [];
		p_aSoluces[2][1][0] = {type:"mcCleOr" , x:0 , y:3};
		p_aSoluces[2][1][1] = {type:"mcCleVerticale3" , x:2 , y:1};	
		p_aSoluces[2][1][2] = {type:"mcCleVerticale3" , x:5 , y:4};	
		p_aSoluces[2][1][3] = {type:"mcCleHorizontale3" , x:1 , y:0};	
		p_aSoluces[2][1][4] = {type:"mcCleHorizontale3" , x:3 , y:1};	
		p_aSoluces[2][1][5] = {type:"mcCleHorizontale3" , x:2 , y:6};	
		p_aSoluces[2][1][6] = {type:"mcCleHorizontale2" , x:0 , y:6};	
		p_aSoluces[2][1][7] = {type:"mcCleHorizontale2" , x:4 , y:0};	
		p_aSoluces[2][1][8] = {type:"mcCleVerticale2" , x:5 , y:2};	
		p_aSoluces[2][2] = [];
		p_aSoluces[2][2][0] = {type:"mcCleOr" , x:0 , y:3};
		p_aSoluces[2][2][1] = {type:"mcCleVerticale3" , x:0 , y:0};	
		p_aSoluces[2][2][2] = {type:"mcCleVerticale3" , x:1 , y:0};	
		p_aSoluces[2][2][3] = {type:"mcCleVerticale3" , x:2 , y:3};	
		p_aSoluces[2][2][4] = {type:"mcCleVerticale3" , x:3 , y:3};	
		p_aSoluces[2][2][5] = {type:"mcCleVerticale3" , x:4 , y:3};	
		p_aSoluces[2][2][6] = {type:"mcCleVerticale3" , x:5 , y:3};	
		p_aSoluces[2][2][7] = {type:"mcCleVerticale2" , x:0 , y:5};	
		p_aSoluces[2][2][8] = {type:"mcCleVerticale2" , x:1 , y:5};	
		p_aSoluces[2][2][9] = {type:"mcCleHorizontale2" , x:2 , y:6};	
		p_aSoluces[2][2][10] = {type:"mcCleHorizontale2" , x:4 , y:6};	
		p_aSoluces[2][2][11] = {type:"mcCleHorizontale3" , x:3 , y:0};	
		
		initInterface();
		
	}	


	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		p_btAntiClic.useHandCursor = false;
		p_btAntiClic._visible = false;
		p_btAntiClic.onRelease = function(){};
		
		this["mcCle0"]._visible = this["mcCle1"]._visible = this["mcCle2"]._visible = false;
		nextPart();
		
	}	
	
	/**
	 * Nouvelle question
	 */
	private function nextPart():Void{
		
		p_btAntiClic.useHandCursor = false;
	
		p_iCurrPartId = 0;
		p_iFrameCount = 0;
		
		// init de la grille
		p_mcGrid = createEmptyMovieClip("mcGrid", 10);
		p_mcGrid._x = p_iTopX;
		p_mcGrid._y = p_iTopY;
		
		// creation de la grille
		p_aGrid = [];
		for(var i=0; i<7; i++){
			p_aGrid[i] = [];
			for (var j=0; j<7; j++){
				if(i<6){
					p_aGrid[i][j] = 0;
				}else{
					if(j!=3){
						p_aGrid[i][j] = 1;
					}else{
						p_aGrid[i][j] = 0;
					}
				}
			}			
		}
		
		p_iCurrPartId = Math.floor(Math.random()*p_aSoluces[p_iNbPartsWon].length);
		
		for (var i=0; i<p_aSoluces[p_iNbPartsWon][p_iCurrPartId].length; i++){
			
			var mcCle = p_mcGrid.attachMovie(p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].type, p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].type+i, i);
			if(p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].type == "mcCleOr"){
				mcCle._name = "mcCleOr";
				mcCle.p_iLength = 2;
			}
			mcCle._x = p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].x * p_iOffsetX;
			mcCle._y = p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].y * p_iOffsetY;
			
			mcCle.p_iX = p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].x;
			mcCle.p_iY = p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].y;
			
			if(mcCle._name.indexOf("3")!=-1){
				mcCle.p_iLength = 3;
			}else if(mcCle._name.indexOf("2")!=-1){
				mcCle.p_iLength = 2;
			}				
			
			if(mcCle._name.indexOf("Horizontale")!=-1){
				mcCle.p_sType = "horizontale";
				
				for(var j=p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].x; j<p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].x + mcCle.p_iLength; j++){
					p_aGrid[j][p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].y] = 1;
				}
				
			}else if(mcCle._name.indexOf("Verticale")!=-1){
				mcCle.p_sType = "verticale";
				
				for(var j=p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].y; j<p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].y + mcCle.p_iLength; j++){
					p_aGrid[p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].x][j] = 1;
				}
				
			}else{
				mcCle.p_sType = "or";				
				for(var j=p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].x; j<p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].x + 2; j++){
					p_aGrid[j][p_aSoluces[p_iNbPartsWon][p_iCurrPartId][i].y] = 1;
				}
				
			}
	
			
		}		

	}
	
	private function isTileFree(iX:Number, iY:Number):Boolean{
		if(iX == 7 && iY == 3){
			return true;
		}else{
			return p_aGrid[iX][iY]==0;
		}
	}
	
	private function moveCle(mcCle:MovieClip, iX:Number, iY:Number){
		
		// clear de p_aGrid
		if(iX != 0){
			// mouvement horizontal
			for(var i=mcCle.p_iX; i<mcCle.p_iX+mcCle.p_iLength; i++){
				p_aGrid[i][mcCle.p_iY] = 0;
			}			
		}else{
			// mouvement vertical
			for(var i=mcCle.p_iY; i<mcCle.p_iY+mcCle.p_iLength; i++){
				p_aGrid[mcCle.p_iX][i] = 0;
			}	
		}
		
		// re ajout dans p_aGrid
		if(iX != 0){			
			// mouvement horizontal
			mcCle.p_iX += iX;
			for(var i=mcCle.p_iX; i<mcCle.p_iX+mcCle.p_iLength; i++){
				p_aGrid[i][mcCle.p_iY] = 1;
			}			
		}else{
			// mouvement vertical
			mcCle.p_iY += iY;
			for(var i=mcCle.p_iY; i<mcCle.p_iY+mcCle.p_iLength; i++){
				p_aGrid[mcCle.p_iX][i] = 1;
			}	
		}
		
		// mouvement de la  clé
		mcCle._x = mcCle.p_iX * p_iOffsetX;
		mcCle._y = mcCle.p_iY * p_iOffsetY;
		
		if(mcCle.p_iX == 6 && mcCle.p_iY == 3 && mcCle._name == "mcCleOr"){
			p_oSoundPlayer.playASound("ok",0);
			p_btAntiClic._visible = true;
			this["mcCle"+p_iNbPartsWon]._visible = true;
			p_mcGrid["mcCleOr"]._visible = false;
			p_iNbPartsWon++;
			this.onEnterFrame = function(){
				p_iFrameCount++;
				if(p_iFrameCount >= 30){
					if(p_iNbPartsWon == p_iNbPartsToWin){
						p_mcGrid.removeMovieClip();
						won();
					}else{
						nextPart();
					}
					delete this.onEnterFrame;
				}
			}
		}else{
			p_oSoundPlayer.playASound("avance",0);
		}
		
	}
	
}

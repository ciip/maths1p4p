﻿/**
 * class com.maths1p4p.level4.game4_11.Game4_11
 * 
 * @author Grégory Lardon
 * @version 0.2
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * TODO :
 * integrer la gestion des sons
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import flash.geom.Point;
import maths1p4p.application.Application;

class maths1p4p.level4.game4_11.Game4_11 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var p_mcTube0, p_mcTube1, p_mcTube2:MovieClip;
	public var p_btRes0, p_btRes, p_btRes2:Button;

	public var p_bt0to1, p_bt0to2:Button;
	public var p_bt1to0, p_bt1to2:Button;
	public var p_bt2to0, p_bt2to1:Button;
	
	public var p_btChange:Button;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** liste des questions possibles */
	private var p_aQuestions:Array;
	
	/** id de la question en cours */
	private var p_iCurrQuestion:Number;
	
	


	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_11()	{		
		super();				
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){		
		
		// initialisation des propriétés
		p_iNbPartsToWin = 3;
		p_iNbPartsWon = 0;
		
		p_aQuestions = [
			[[14,0],[5,5],[3,3],2],
			[[11,0],[7,7],[4,4],5],
			[[13,13],[9,0],[4,0],2],
			[[11,11],[5,0],[4,0],3],
			[[14,14],[5,0],[3,0],12]
		];		
		
		p_iCurrQuestion = Math.floor(Math.random()*p_aQuestions.length);
		
		initInterface();
		
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		
		
		this["mcPlanet0"]._visible = this["mcPlanet1"]._visible = this["mcPlanet2"]._visible = false;
		
		//boutons réponses
		for (var i=0; i<3; i++){
			this["p_btRes"+i]["id"] = i;
			Application.useLibCursor(this["p_btRes"+i], "cursor-finger");
			this["p_btRes"+i].onRelease = function(){
				_parent.setAnswer(this["id"]);
			}
		}
		
		Application.useLibCursor(p_btChange, "cursor-finger");
		p_btChange.onRelease = function(){
			_parent.newPart();
		}
		
		// bouton de transvasage
		p_bt0to1["from"] = 0;
		p_bt0to1["to"] = 1;
		Application.useLibCursor(p_bt0to1, "cursor-finger");
		p_bt0to1.onRelease = function(){_parent.transvase(this["from"], this["to"])};
		p_bt0to2["from"] = 0;
		p_bt0to2["to"] = 2;
		Application.useLibCursor(p_bt0to2, "cursor-finger");
		p_bt0to2.onRelease = function(){_parent.transvase(this["from"], this["to"])};
		p_bt1to0["from"] = 1;
		p_bt1to0["to"] = 0;
		Application.useLibCursor(p_bt1to0, "cursor-finger");
		p_bt1to0.onRelease = function(){_parent.transvase(this["from"], this["to"])};
		p_bt1to2["from"] = 1;
		p_bt1to2["to"] = 2;
		Application.useLibCursor(p_bt1to2, "cursor-finger");
		p_bt1to2.onRelease = function(){_parent.transvase(this["from"], this["to"])};
		p_bt2to0["from"] = 2;
		p_bt2to0["to"] = 0;
		Application.useLibCursor(p_bt2to0, "cursor-finger");
		p_bt2to0.onRelease = function(){_parent.transvase(this["from"], this["to"])};
		p_bt2to1["from"] = 2;
		p_bt2to1["to"] = 1;
		Application.useLibCursor(p_bt2to1, "cursor-finger");
		p_bt2to1.onRelease = function(){_parent.transvase(this["from"], this["to"])};
		
		newPart();
		
	}	
	
	
	/**
	 * démarrage d'une partie
	 */
	private function newPart(){
	
		p_iCurrQuestion++;
		if(p_iCurrQuestion == p_aQuestions.length) p_iCurrQuestion = 0;
		
		// init des tubes
		for(var i=0; i<3; i++){
			this["p_mcTube"+i].mcCouvercle.gotoAndStop(p_aQuestions[p_iCurrQuestion][i][0]);
			this["p_mcTube"+i].mcCouvercle.mcColor.gotoAndStop(i+1);
			this["p_mcTube"+i].mcVerre.gotoAndStop(p_aQuestions[p_iCurrQuestion][i][0]+1);
			this["p_mcTube"+i].gotoAndStop(p_aQuestions[p_iCurrQuestion][i][1] +1);	
			this["txtTube"+i].text = p_aQuestions[p_iCurrQuestion][i][0] + " l";
		}
		
		this["txtConsigne"].text = "Isole " + p_aQuestions[p_iCurrQuestion][3] + " litres d'eau dans l'un des réservoirs";
		
	}
	
	private function transvase(iFrom:Number, iTo:Number){
		
		var iWaterFrom:Number = this["p_mcTube"+iFrom]._currentframe-1;
		var iWaterTo:Number = this["p_mcTube"+iTo]._currentframe-1;
		
		var iNbWaterOk:Number = p_aQuestions[p_iCurrQuestion][iTo][0] - iWaterTo;
		iNbWaterOk = Math.min(iNbWaterOk, iWaterFrom);
		
		p_oSoundPlayer.playASound("eau",0);
		
		this["p_mcTube"+iFrom].frameTo( this["p_mcTube"+iFrom]._currentframe - iNbWaterOk, 1);
		this["p_mcTube"+iTo].frameTo( this["p_mcTube"+iTo]._currentframe + iNbWaterOk, 1, "linear", 0, stopSound);
		
	}
	
	private function stopSound(){
		p_oSoundPlayer.stopAsound("eau");
	}
	
	
	private function setAnswer(id:Number){
		
		if(this["p_mcTube"+id]._currentframe-1 == p_aQuestions[p_iCurrQuestion][3]){
			this["mcPlanet"+p_iNbPartsWon]._visible = true;
			p_iNbPartsWon++;
			if(p_iNbPartsWon == p_iNbPartsToWin){
				won();
			}else{
				newPart();
			}			
		}
		
	}
	
	
	
}

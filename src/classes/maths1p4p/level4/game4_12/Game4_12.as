﻿/**
 * class com.maths1p4p.level4.game4_11.Game4_11
 * 
 * @author Grégory Lardon
 * @version 0.2
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * TODO :
 * integrer la gestion des sons
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.Maths.Operations.OperationMaker;
import maths1p4p.application.Application;

class maths1p4p.level4.game4_12.Game4_12 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public var p_mcSuite:MovieClip;
	
	public var p_mcOperation1:MovieClip;
	public var p_mcOperation2:MovieClip;
	
	public var p_mcFormePlayer1_0, p_mcFormePlayer1_1, p_mcFormePlayer1_2:MovieClip;
	public var p_mcFormePlayer2_0, p_mcFormePlayer2_1, p_mcFormePlayer2_2:MovieClip;
	
	public var p_mcTiles:MovieClip;
	
	public var p_btAntiClic:MovieClip;
	
	public var p_mcTete1, p_mcTete2:MovieClip;

	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var p_aMap:Array;

	private var p_oPlayer1, p_oPlayer2:Object;
	
	private var p_iCurrTurn:Number;
	
	private var p_oCurrOperation:Object;
	private var p_iCurrItemToFind:String;
	
	private var p_mcCurrForme:MovieClip;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_12()	{		
		super();				
	}
	
	/**
	 * secondPlayerSelected
	 * appellé depuis level3
	 */
	public function secondPlayerSelected(oPlayer2:Object):Void{
		
		p_oPlayer1 = new Object();
		p_oPlayer1.name = _parent._parent.getPlayer().getName();
		p_oPlayer1.id = _parent._parent.getPlayer().getId();
		p_oPlayer1.win = 0;
		p_oPlayer2 = oPlayer2;
		p_oPlayer2.win = 0;
		
		initInterface();
		
	}	
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){		
		
		// initialisation des propriétés
		p_iNbPartsToWin = 3;
		p_iNbPartsWon = 0;
		
		// affichage de l'écran de sélection du joueur 2
		if(_parent._parent != null){
			_parent._parent.showSecondPlayerSelectionScreen();
		}else{
			// mode autonome
			p_oPlayer1 = new Object();
			p_oPlayer1.name = "joueur 1";
			p_oPlayer1.id = 1;
			p_oPlayer1.win = 0;
			
			p_oPlayer2 = new Object();
			p_oPlayer2.name = "joueur 2";
			p_oPlayer2.id = 2;
			p_oPlayer2.win = 0;
			
			// init de l'interface
			initInterface();			
		}
		
	}	

	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		
		
		this["mcPlanet11"]._visible = this["mcPlanet12"]._visible = this["mcPlanet21"]._visible = this["mcPlanet22"]._visible = false;
		
		p_mcTete1._visible = p_mcTete2._visible = false;
		
		p_btAntiClic._visible = true;
		p_btAntiClic.onRelease = function(){};
		p_btAntiClic.useHandCursor = false;
		
		this["txtPlayer1"].text = p_oPlayer1.name;
		this["txtPlayer2"].text = p_oPlayer2.name;
				
		p_mcFormePlayer1_0["player"] = p_mcFormePlayer1_1["player"] = p_mcFormePlayer1_2["player"] = 1;
		p_mcFormePlayer2_0["player"] = p_mcFormePlayer2_1["player"] = p_mcFormePlayer2_2["player"] = 2;
		p_mcFormePlayer1_0["forme"] = 0;
		p_mcFormePlayer1_1["forme"] = 1;
		p_mcFormePlayer1_2["forme"] = 2;
		p_mcFormePlayer2_0["forme"] = 0;
		p_mcFormePlayer2_1["forme"] = 1;
		p_mcFormePlayer2_2["forme"] = 2;	
		p_mcFormePlayer1_0._alpha = p_mcFormePlayer1_1._alpha = p_mcFormePlayer1_2._alpha = 
		p_mcFormePlayer2_0._alpha = p_mcFormePlayer2_1._alpha = p_mcFormePlayer2_2._alpha = 0;		
		Application.useLibCursor(p_mcFormePlayer1_0, "cursor-finger");
		Application.useLibCursor(p_mcFormePlayer1_1, "cursor-finger");
		Application.useLibCursor(p_mcFormePlayer1_2, "cursor-finger");
		Application.useLibCursor(p_mcFormePlayer2_0, "cursor-finger");
		Application.useLibCursor(p_mcFormePlayer2_1, "cursor-finger");
		Application.useLibCursor(p_mcFormePlayer2_2, "cursor-finger");		
		p_mcFormePlayer1_0.onRelease = p_mcFormePlayer1_1.onRelease = p_mcFormePlayer1_2.onRelease = 
		p_mcFormePlayer2_0.onRelease = p_mcFormePlayer2_1.onRelease = p_mcFormePlayer2_2.onRelease = function(){
			_parent.clickOnForme(this);
		}
		
		activateFormes(false, 1);
		activateFormes(false, 2);
		
		p_mcSuite._visible = false;
		Application.useLibCursor(p_mcSuite.btSuite, "cursor-finger");
		p_mcSuite.btSuite.onRelease = function(){
			_parent._parent.suite();
		}
		
		// init des tiles
		for(var i=0;i<5; i++){
			for(var j=0;j<5; j++){
				p_mcTiles["mcTile_"+i+"_"+j]["i"] = i;
				p_mcTiles["mcTile_"+i+"_"+j]["j"] = j;
				Application.useLibCursor(p_mcTiles["mcTile_"+i+"_"+j], "cursor-finger");
				p_mcTiles["mcTile_"+i+"_"+j].onRelease = function(){
					_parent._parent.clickOnTile(this);
				}
			}			
		}
		
		newPart();
		
	}	
	
	
	/**
	 * démarrage d'une partie
	 */
	private function newPart(){
	
		p_aMap = [];
		p_aMap[0] = [2,null,null,null,null];
		p_aMap[1] = [null,null,null,null,null];
		p_aMap[2] = [null,null,null,null,null];
		p_aMap[3] = [null,null,null,null,null];
		p_aMap[4] = [null,null,null,null,2];
		
		for(var i in p_mcTiles){
			p_mcTiles[i].gotoAndStop(1);
			p_mcTiles[i].enabled = true;
		}
		
		p_iCurrTurn = Math.floor(Math.random()*2);
		p_mcSuite._visible = false;
		nextTurn();
		
	}
	
	private function nextTurn(){
		
		if(p_iCurrTurn==0){
			p_iCurrTurn = 1;
		}else{
			p_iCurrTurn = 0;
		}			
				
		var iCurrPlayer:Number = p_iCurrTurn+1;
		
		p_oCurrOperation = OperationMaker.getAddition(21,99);
		p_iCurrItemToFind = ["A","B","R"][Math.floor(Math.random()*3)];
		this["p_mcOperation"+iCurrPlayer].init(p_oCurrOperation, p_iCurrItemToFind);

	}	
	
	
	private function clickOnTile(mcTile:MovieClip){
	
		// test si la tile a des voisines
		if(isNearATile(mcTile)){
			
			p_oSoundPlayer.playASound("piece",0);
			
			// test si c la première tile
			if(!p_mcTete1._visible && !p_mcTete2._visible){
				if(mcTile["i"]==0 && mcTile["j"]==1){
					p_mcTete2._visible = true;
				}
				if(mcTile["i"]==1 && mcTile["j"]==0){
					p_mcTete1._visible = true;
				}			
			}
	
			activateFormes(false, p_mcCurrForme["player"]);
			mcTile.gotoAndStop(p_mcCurrForme["forme"]+2);
			mcTile.enabled = false;
			p_aMap[mcTile["i"]][mcTile["j"]] = p_mcCurrForme["forme"];
			checkPath();
			p_btAntiClic._visible = true;
			
			p_mcCurrForme._alpha = 0;
			
		}

	}
	private function clickOnForme(mcForme:MovieClip){
		p_mcCurrForme = mcForme;
		p_btAntiClic._visible = false;
		this["p_mcFormePlayer"+ mcForme["player"]+"_0"]._alpha = 0;
		this["p_mcFormePlayer"+ mcForme["player"]+"_1"]._alpha = 0;
		this["p_mcFormePlayer"+ mcForme["player"]+"_2"]._alpha = 0;		
		mcForme._alpha = 100;	
	}	
	
	private function activateFormes(state:Boolean, playerId:Number){
		this["p_mcFormePlayer"+playerId+"_0"].enabled = state;
		this["p_mcFormePlayer"+playerId+"_1"].enabled = state;
		this["p_mcFormePlayer"+playerId+"_2"].enabled = state;
	}
	
	private function suite(){
		this.newPart();
	}
	
	private function checkResponse(iResponse:Number){
		
		if(this["p_mcOperation"+(p_iCurrTurn+1)].getResponse() == p_oCurrOperation[p_iCurrItemToFind]){
			// bonne réponse
			p_oSoundPlayer.playASound("ok",0);
			activateFormes(true, p_iCurrTurn+1);
			this["p_mcOperation"+(p_iCurrTurn+1)].hide();
			activateFormes(true, p_iCurrTurn+1);
			this["p_mcOperation"+(p_iCurrTurn+1)].hide();
			return true;
		}else{
			// mauvaise réponse, on sélectionne le text
			p_oSoundPlayer.playASound("error",0);
			this["p_mcOperation"+(p_iCurrTurn+1)].selectCurrTextField();
			return false;
		}				
		
	}
	
	private function checkPath(){
		
		if(p_mcTete2._visible){
			var from:String = "top";
		}else{
			var from:String = "left";
		}
		
		var res = getNextTileFrom(0, 0, from, 2) ;
		//while (p_aMap[res.i][res.j] != null && ( res.i!=4 && res.j!=4 )){
		while (p_aMap[res.i][res.j] != null){
			res = getNextTileFrom(res.i, res.j, res.from, p_aMap[res.i][res.j]) ;
		}

		if (res.i > 4 || res.j > 4){
			if ( (res.i == 5 && res.j == 4) || (res.i == 4 && res.j == 5) ){
				winPart();
			}else{
				loosePart();
			}
		}else{
			if (res.i < 0 || res.j < 0){
				loosePart();
			}else{
				nextTurn();
			}
		}
		
	}
	
	private function isNearATile(mcTile:MovieClip):Boolean{
		
		var i:Number = mcTile["i"];
		var j:Number = mcTile["j"];		
		
		if (p_aMap[i-1][j]!=null && !(i-1==4 && j==4) ) return true;
		if (p_aMap[i+1][j]!=null && !(i+1==4 && j==4) ) return true;
		if (p_aMap[i][j-1]!=null && !(i==4 && j-1==4) ) return true;
		if (p_aMap[i][j+1]!=null && !(i==4 && j+1
		==4) ) return true;

		return false;
		
	}
	
	private function getNextTileFrom(i:Number, j:Number, from:String, tileType:Number):Object{
		
		var oRes:Object = new Object();
		switch(tileType){
			
			case 0:
				switch(from){				
					case "left":
						oRes.i = i;
						oRes.j = j-1;
						oRes.from = "bottom";
						break;
					case "right":
						oRes.i = i;
						oRes.j = j+1;
						oRes.from = "top";
						break;
					case "top":
						oRes.i = i-1;
						oRes.j = j;
						oRes.from = "right";
						break;
					case "bottom":
						oRes.i = i+1;
						oRes.j = j;
						oRes.from = "left";
						break;							
				}
				break;
				
			case 1:
				switch(from){				
					case "left":
						oRes.i = i;
						oRes.j = j+1;
						oRes.from = "top";
						break;
					case "right":
						oRes.i = i;
						oRes.j = j-1;
						oRes.from = "bottom";
						break;
					case "bottom":
						oRes.i = i-1;
						oRes.j = j;
						oRes.from = "right";
						break;
					case "top":
						oRes.i = i+1;
						oRes.j = j;
						oRes.from = "left";
						break;							
				}
				break;
				
			case 2:
				switch(from){				
					case "left":
						oRes.i = i+1;
						oRes.j = j;
						oRes.from = "left";
						break;
					case "right":
						oRes.i = i-1;
						oRes.j = j;
						oRes.from = "right";
						break;
					case "bottom":
						oRes.i = i;
						oRes.j = j-1;
						oRes.from = "bottom";
						break;
					case "top":
						oRes.i = i;
						oRes.j = j+1;
						oRes.from = "top";
						break;							
				}
				break;
				
			default:
				trace("ne doit jamais arriver : "+tileType);
			
			
		}
		
		return oRes;
		
	}
	
	private function winPart(){
		this["p_oPlayer" + String(p_iCurrTurn+1)].win ++;
		this["mcPlanet" + String(p_iCurrTurn+1) + String(this["p_oPlayer" + String(p_iCurrTurn+1)].win)]._visible = true;
		if(this["p_oPlayer" + String(p_iCurrTurn+1)].win == 2){
			setVictory(this["p_oPlayer" + String(p_iCurrTurn+1)]);
		}else{
			p_mcSuite._visible = true;
		}
	}
	
	private function loosePart(){
		if(p_iCurrTurn==0){
			var iCurrTurn = 1;
		}else{
			var iCurrTurn = 0;
		}	
		this["p_oPlayer" + String(iCurrTurn+1)].win ++;
		this["mcPlanet" + String(iCurrTurn+1) + String(this["p_oPlayer" + String(iCurrTurn+1)].win)]._visible = true;

		if(this["p_oPlayer" + String(iCurrTurn+1)].win == 2){
			setVictory(this["p_oPlayer" + String(iCurrTurn+1)]);
		}else{
			p_mcSuite._visible = true;
		}
	}	
	
	private function setVictory(oPlayer:Object){
		trace("win "+oPlayer.name);
		if(oPlayer == p_oPlayer1){
			// c'est le premier joueur qui gagne
			won(1);
		}else{
			// c'est le 2e joueur qui gagne
			p_mcLoots.gotoAndStop("objsuppl");
			won(2);
		}		
		
	}		
	
	/*
	public function won(idPlayer):Void{
		gotoAndStop("win");
		if(idPlayer == 1){
			p_mcLoots.gotoAndStop("objsuppl");
		}
		p_mcLoots.show();
	}
	*/
	
}

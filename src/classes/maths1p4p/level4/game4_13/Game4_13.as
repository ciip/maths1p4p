﻿/**
 * class com.maths1p4p.level4.game4_13.Game4_13
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * TODO :
 * integrer la gestion des sons
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.Maths.Operations.OperationMaker;
import maths1p4p.utils.OutilsTableaux;
import maths1p4p.application.Application;

class maths1p4p.level4.game4_13.Game4_13 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	
	public var p_mcOperation:MovieClip;
	
	public var p_btAntiClic:MovieClip;
	
	public var p_mcSens:MovieClip;
	
	public var p_mcCurrTile:MovieClip;

	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var p_aMap:Array;
	
	private var p_oCurrOperation:Object;
	private var p_iCurrItemToFind:String;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_13()	{		
		super();				
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){		
		
		// initialisation des propriétés
		initInterface();
		
	}	
	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		
		
		p_btAntiClic._visible = false;
		p_btAntiClic.onRelease = function(){};
		p_btAntiClic.useHandCursor = false;
		
		//Application.useLibCursor(p_mcSens, "cursor-finger");
		p_mcSens.onRelease = function(){
			if(this._currentframe == 1){
				this.gotoAndStop(2);
			}else{
				this.gotoAndStop(1);
			}
		}

		
		// init des tiles
		for(var i=0;i<7; i++){
			for(var j=0;j<7; j++){
				this["mcTile_"+i+"_"+j]["i"] = i;
				this["mcTile_"+i+"_"+j]["j"] = j;
				this["mcTile_"+i+"_"+j].gotoAndStop(2);
				//Application.useLibCursor(this["mcTile_"+i+"_"+j], "cursor-finger");
				this["mcTile_"+i+"_"+j].onRelease = function(){
					_parent.clickOnTile(this);
				}
			}			
		}
		
		newPart();
		
	}	
	
	/**
	 * renvois le sens de rotation actuel
	 */
	private function getSens():String{
		if(p_mcSens._currentframe == 1){
			return "droite";
		}else{
			return "gauche";
		}
	}
	
	/**
	 * démarrage d'une partie
	 */
	private function newPart(){
	
		p_aMap = [];
		p_aMap[0] = [3,null,null,null,null];
		p_aMap[1] = [null,null,null,null,null];
		p_aMap[2] = [null,null,null,null,null];
		p_aMap[3] = [null,null,null,null,null];
		p_aMap[4] = [null,null,null,null,3];
		
		//nextQuestion();
		
	}
	
	private function nextQuestion(){
				
		p_btAntiClic._visible = true;
		//p_oCurrOperation = OperationMaker.getAddition(21,99);
		p_oCurrOperation = OperationMaker.getMultiplication(1,10,1,10);
		//p_iCurrItemToFind = ["A","B","R"][Math.floor(Math.random()*3)];
		p_iCurrItemToFind = "R";
		p_mcOperation.init(p_oCurrOperation, p_iCurrItemToFind);

	}	
	
	
	private function clickOnTile(mcTile:MovieClip){
		
		p_mcCurrTile = mcTile;
		p_mcCurrTile.btTileOver._visible = true;
		p_oSoundPlayer.playASound("clic",0);
		nextQuestion();				

	}

	private function checkResponse(iResponse:Number):Boolean{
		
		if(p_mcOperation.getResponse() == p_oCurrOperation[p_iCurrItemToFind]){
			// bonne réponse
			p_oSoundPlayer.playASound("ok",0);
			switch(getSens()){			
				case "droite":
					if(p_mcCurrTile._currentframe==3){
						p_mcCurrTile.gotoAndStop(1);
					}else{
						p_mcCurrTile.nextFrame();
					}
					break;
				case "gauche":
					if(p_mcCurrTile._currentframe==1){
						p_mcCurrTile.gotoAndStop(3);
					}else{
						p_mcCurrTile.prevFrame();
					}
					break;			
			}

			checkPath();			
			
			p_mcCurrTile.btTileOver._visible = false;
			
			p_mcOperation.hide();
			p_btAntiClic._visible = false;
			return true;
		}else{
			// mauvaise réponse, on sélectionne le text
			p_oSoundPlayer.playASound("error",0);
			p_mcOperation.selectCurrTextField();
			return false;
		}				
		
	}
	
	private function checkPath(){
		var aPaths:Array = ["so","s","se","no","n","ne"];
		var bWon:Boolean = false;
		
		for(var dir=0; dir<aPaths.length; dir++){
			var startTile:Object = {i:5, j:5, from:aPaths[dir]};
			var res = getNextTileFrom(5, 5, aPaths[dir], 2) ;
			var aPath:Array = [];
			aPath.push(startTile);
			var exit:Boolean = false;
			while (this["mcTile_"+res.i+"_"+res.j] != null && !exit){
				res = getNextTileFrom(res.i, res.j, res.from, this["mcTile_"+res.i+"_"+res.j]._currentframe) ;				
				if(isInArray(aPath, res)){
					if(res.i == startTile.i && res.j == startTile.j && res.from == startTile.from){
						exit = true;
						if(checkForThe4(aPath)){
							bWon = true;							
						}
					}else{
						if (isReallyInArray(aPath, res)){
							// loop infinie... on coupe
							exit = true;
						}
					}
				}
				aPath.push(res);
				
			}
			
		}

		if( bWon ){			
			p_btAntiClic._visible = true;
			won();
		}else{
			//nextQuestion();
			p_btAntiClic._visible = false;
		}
		
	}
	
	private function checkForThe4(aPath:Array):Boolean{
		if(
			isInArray(aPath, {i:5, j:5}) &&
			isInArray(aPath, {i:3, j:3}) &&
			isInArray(aPath, {i:3, j:1}) &&
			isInArray(aPath, {i:1, j:3})			
		) return true;
		return false;
	}
	
	private function isInArray(arr:Array, tile:Object):Boolean{
		for (var i in arr){
			if (arr[i].i == tile.i && arr[i].j == tile.j) return true;
		}
		return false;
	}
	
	private function isReallyInArray(arr:Array, tile:Object):Boolean{
		for (var i in arr){
			if (arr[i].i == tile.i && arr[i].j == tile.j && arr[i].from == tile.from) return true;
			
		}
		return false;
	}
	
	
	private function getNextTileFrom(i:Number, j:Number, from:String, tileType:Number):Object{

		var oRes:Object = new Object();
		switch(tileType){
			
			case 1:
				switch(from){				
					case "no":
						oRes.i = i;
						oRes.j = j+1;
						oRes.from = "s";
						break;
					case "n":
						oRes.i = i-1;
						oRes.j = j;
						oRes.from = "so";
						break;
					case "ne":
						oRes.i = i+1;
						oRes.j = j;
						oRes.from = "ne";
						break;
					case "so":
						oRes.i = i+1;
						oRes.j = j+1;
						oRes.from = "n";
						break;	
					case "s":
						oRes.i = i-1;
						oRes.j = j-1;
						oRes.from = "no";
						break;		
					case "se":
						oRes.i = i;
						oRes.j = j-1;
						oRes.from = "se";
						break;								
				}
				break;
				
			case 2:
				switch(from){				
					case "no":
						oRes.i = i+1;
						oRes.j = j;
						oRes.from = "ne";
						break;
					case "n":
						oRes.i = i+1;
						oRes.j = j+1;
						oRes.from = "n";
						break;
					case "ne":
						oRes.i = i;
						oRes.j = j+1;
						oRes.from = "no";
						break;
					case "so":
						oRes.i = i;
						oRes.j = j-1;
						oRes.from = "se";
						break;	
					case "s":
						oRes.i = i-1;
						oRes.j = j-1;
						oRes.from = "s";
						break;		
					case "se":
						oRes.i = i-1;
						oRes.j = j;
						oRes.from = "so";
						break;		
				}
				break;
				
			case 3:
				switch(from){				
					case "no":
						oRes.i = i-1;
						oRes.j = j-1;
						oRes.from = "s";
						break;
					case "n":
						oRes.i = i;
						oRes.j = j-1;
						oRes.from = "se";
						break;
					case "ne":
						oRes.i = i+1;
						oRes.j = j;
						oRes.from = "s";
						break;
					case "so":
						oRes.i = i-1;
						oRes.j = j;
						oRes.from = "so";
						break;	
					case "s":
						oRes.i = i;
						oRes.j = j+1;
						oRes.from = "no";
						break;		
					case "se":
						oRes.i = i+1;
						oRes.j = j+1;
						oRes.from = "n";
						break;		
				}
				break;
			
				
				
		}
		return oRes;
		
	}
	
}

﻿/**
 * class com.maths1p4p.level4.game4_14.Game4_14
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import maths1p4p.application.Application;
import maths1p4p.Maths.Operations.OperationMaker;

class maths1p4p.level4.game4_14.Game4_14 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** les loots */
	public var p_mcLoots:MovieClip;
	
	/** boutons des flips */
	public var p_btLeft, p_btRight;
	
	/** bouton suite/départ */
	public var p_btDepartSuite
	
	/** champs texte opération */
	public var p_txtOperation:TextField;
	
	/** champs départ/suite */
	private var p_txtDepartSuite:TextField;
	
	/** champs texte des erreurs */
	public var p_txtErrors:TextField;
	
	/** le sablier */
	public var p_mcSablier:MovieClip;
	
	/** le fond vert */
	public var p_mcBgNumber:MovieClip;

	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** liste des opérations */
	private var p_aOperations:Array;
	private var p_iCurrOperation:Number;
		
	
	/** colonne de gauche */
	private var p_iLeftXAlign:Number = 57;
	
	/** colonne de droite */
	private var p_iRightXAlign:Number = 465;
	
	/** largeur et hauteur des textfields */
	private var p_iTxtWidth:Number = 113;
	private var p_iTxtHeight:Number = 20;
	
	/** nombre de réponses passées */
	private var p_iAnswersCount:Number;
	
	/** nombre d'erreurs */
	private var p_iErrorsCount:Number = 0;
	
	private var p_iLeftCount, p_iRightCount:Number;	
	private var p_aLeftColumn, p_aRightColumn:Array;
	
	/** le temps écoulé du sablier */
	private var p_iTimeElapsed:Number;
	
	/** le tps max du sablier */
	private var p_iMaxTime:Number;
	
	/** compteur des frames */
	private var p_iFrameCount:Number;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_14()	{		
		super();	
	}	
	
	public function sablierReady(){
		this.onEnterFrame = this.loop;
	}
	
	public function showSuite(){
		p_mcBgNumber._visible = false;
		this["txtDepartSuite"]._visible = true;
		p_btDepartSuite.enabled = true;
	}
		
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		p_txtOperation = this["txtOperation"];
		p_txtDepartSuite = this["txtDepartSuite"];
		p_txtErrors = this["txtErrors"];
		
		p_iAnswersCount = 0;	
		p_iLeftCount = 0;
		p_iRightCount = 0;		
		
		p_aLeftColumn = [];
		p_aRightColumn = [];
		
		// random des opérations
		p_iCurrOperation = -1;
		p_aOperations = new Array();
		
		for(var i=0; i<15; i++){
			
			// addition ? soustraction ? multiplication ?
			var iOperationType:Number = Math.floor(Math.random()*3);
			switch (iOperationType){				
				case 0:
					var oOperation:Object = OperationMaker.getAddition2(50,1000,750,1250);
					break;
				case 1:
					var oOperation:Object = OperationMaker.getSoustraction2(1000,1500,750,1250);
					break;
				case 2:
					var oOperation:Object = OperationMaker.getMultiplication2(10,300,5,10, 750, 1250);
					break;
			}
			
			p_aOperations.push(oOperation);
			
		}
		
		p_iTimeElapsed = 0;
		p_iMaxTime = 19;
		p_iFrameCount = 0;
		
		initInterface();
		
	}	


	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		p_mcBgNumber._visible = false;
		
		// bouton depart
		Application.useLibCursor(p_btDepartSuite, "cursor-finger");
		p_btDepartSuite.onRelease = function(){			
			_parent.p_txtDepartSuite.text = "Suite";
			_parent.nextTurn();
			this.enabled = false;	
		}
		
		// boutons flips
		p_btLeft.enabled = false;
		Application.useLibCursor(p_btLeft, "cursor-finger");
		p_btLeft.onPress = function(){
			_parent.flip("left");
		}
		p_btRight.enabled = false;
		Application.useLibCursor(p_btRight, "cursor-finger");
		p_btRight.onPress = function(){
			_parent.flip("right");
		}	
		
		// txtfields ...
		p_txtOperation.text = "";
		p_txtErrors.text = "";
		var txtForm:TextFormat = new TextFormat();
		txtForm.size = 14;
		txtForm.bold = true;
		p_txtOperation.setTextFormat(txtForm);		
		p_txtErrors.setTextFormat(txtForm);		
		
	}	
	
	/**
	 * Nouvelle question
	 */
	private function nextTurn():Void{
		
		p_iTimeElapsed = 0;
		p_iMaxTime = 19;
		p_iFrameCount = 0;		
		p_mcSablier.gotoAndPlay(2);
		
		p_oSoundPlayer.playASound("sablier",0);
		
		this["txtDepartSuite"]._visible = false;
		p_btDepartSuite.enabled = false;
		
		p_btLeft.enabled = true;
		p_btRight.enabled = true;
		
		p_iCurrOperation++;
		var oOperation:Object = p_aOperations[p_iCurrOperation];
		
		p_txtOperation._visible = true;
		p_txtOperation.text = oOperation.A + " " + oOperation.operator + " " + oOperation.B;
		
		p_mcBgNumber._visible = true;

		

	}
	
	
	/**
	 * loop
	 */
	private function loop():Void{
		
		p_iFrameCount++;		
		var oOperation:Object = p_aOperations[p_iCurrOperation];
		
		if(p_iFrameCount == 10){
			p_iTimeElapsed++;
			p_iFrameCount = 0;			
			if(p_iTimeElapsed >= p_iMaxTime){				
				// le joeur n'a pas fait de choix, tombe à l'eau	
				addOperationToErrors(oOperation);			
				p_mcSablier.gotoAndPlay("lost");
				delete this.onEnterFrame;				
			}else{
				p_mcSablier.nextFrame();
			}
		}
		
	}
	
	/**
	 * flip
	 * flip à gauche ou droite
	 */
	private function flip(sDir:String):Void{
		
		p_oSoundPlayer.playASound("clic",0);
				
		p_aOperations[p_iCurrOperation].column = sDir;
		
		if(sDir == "left"){
			addOperationToColumn(p_aOperations[p_iCurrOperation].column, p_aOperations[p_iCurrOperation]);
		}else{
			addOperationToColumn(p_aOperations[p_iCurrOperation].column, p_aOperations[p_iCurrOperation]);
		}
		
		
		
	}
	
	/**
	 * addOperationToColumn
	 * ajoute une operation dans une colonne
	 * @param sColumn left ou right
	 * @param oOperation l'object operation
	 */
	private function addOperationToColumn(sColumn:String, oOperation:Object):Void{
		
		p_btLeft.enabled = false;
		p_btRight.enabled = false;
		
		if (sColumn == "left"){
			p_iLeftCount ++;
			var yCount:Number = p_iLeftCount;
			var iXAlign = p_iLeftXAlign;
			p_aLeftColumn.push(oOperation);
		}else{
			p_iRightCount ++;
			var yCount:Number = p_iRightCount;
			var iXAlign = p_iRightXAlign;
			p_aRightColumn.push(oOperation);
		}
		
		p_iAnswersCount++;
		p_mcBgNumber._visible = false;
		
		createTextField("txtOperation_"+p_iAnswersCount, getNextHighestDepth(), iXAlign, 320-18*yCount,p_iTxtWidth, p_iTxtHeight);
		var txtField:TextField = this["txtOperation_"+p_iAnswersCount];
		txtField.text += oOperation.A + " " + oOperation.operator + " " + oOperation.B;
		txtField.selectable = false;
		
		oOperation.txtField = txtField;
	
		var txtForm:TextFormat = new TextFormat();
		txtForm.align = "center";
		txtForm.font = "arial";
		txtForm.size = 13;
		txtForm.bold = true;
		txtField.setTextFormat(txtForm);
		
		delete this.onEnterFrame;
				
		p_txtOperation._visible = false;
		if(p_iCurrOperation < p_aOperations.length-1){		
			p_btDepartSuite.enabled = true;
			this["txtDepartSuite"]._visible = true;
		}else{
			// partie terminée
			checkEnd();
		}
	}
	
	/**
	 * addOperationToErrors
	 * ajoute une operation aux erreurs
	 * @param sColumn left ou right
	 * @param oOperation l'object operation
	 */
	private function addOperationToErrors(oOperation:Object):Void{
		
		p_btLeft.enabled = false;
		p_btRight.enabled = false;
		
		p_iErrorsCount ++;
		p_mcBgNumber._visible = true;
		
		p_txtErrors.text += oOperation.A + " " + oOperation.operator + " " + oOperation.B + " / ";
		p_txtOperation._visible = false;
		if(p_iCurrOperation < p_aOperations.length-1){		
			//showSuite();
		}else{
			// partie terminée
			checkEnd();
		}
		
	}
	
	/**
	 * checkEnd
	 * test si la partie est gagnée par le joueur en cours
	 */
	private function checkEnd(iTurn:Number){		

		for(var i=0; i<p_aLeftColumn.length; i++){
			p_aLeftColumn[i].txtField.background = true;
			if(p_aLeftColumn[i].R < 1000){				
				p_aLeftColumn[i].txtField.backgroundColor = 0x00FF00;
			}else{
				p_aLeftColumn[i].txtField.backgroundColor = 0xFF0000;
				p_iErrorsCount++;
			}
		}

		for(var i=0; i<p_aRightColumn.length; i++){
			p_aRightColumn[i].txtField.background = true;
			if(p_aRightColumn[i].R > 1000){
				p_aRightColumn[i].txtField.backgroundColor = 0x00FF00;
			}else{
				p_aRightColumn[i].txtField.backgroundColor = 0xFF0000;
				p_iErrorsCount++;
			}
		}	
		
		if (p_iErrorsCount > 2){
			// perdu
			loose();
		}else{
			// gagné
			won();
		}
		
	}


	
	
	/**
	 * la partie est gagnée
	 */
	private function won(){
		
		super.won();		
				
	}
	
	/**
	 * la partie est gagnée, par l'adversaire
	 */
	private function loose(){

		gotoAndStop("win");
		
	}
}

﻿/**
 * class com.maths1p4p.level4.game4_15.Game4_15
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 
import flash.display.BitmapData;
import maths1p4p.application.Application;
import maths1p4p.Maths.Operations.OperationMaker;
import maths1p4p.utils.OutilsTableaux;

class maths1p4p.level4.game4_15.Game4_15 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** boutons des flips */
	public var p_btLeft, p_btRight;
	
	/** bouton suite/départ */
	public var p_btDepartSuite:Button;
	
	/** bouton undo */
	public var p_btUndo:Button;
	
	/** bouton valider */
	public var p_btFleche:Button;
	
	/** boutons couleurs */
	public var p_mcColor0:MovieClip;
	public var p_mcColor1:MovieClip;
	public var p_mcColor2:MovieClip;
	
	/** le clip de la forme résultat */
	public var p_mcAnswer:MovieClip;

	/** bouton changement de forme */
	public var p_btChange:Button;
	
	/** le clip contenant les pochoirs */
	public var p_mcPochoirs:MovieClip;
	
	public var p_aFormeAldreayDone:Array;
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** liste des couleurs */
	private var p_aColors:Array;
	
	/** couleurs courante */
	private var p_iCurrColor:Number;
	
	/** forme en cours */
	private var p_mcCurrForme:MovieClip;
	
	/** id de la forme en cours */
	private var p_iCurrForme:Number;
	
	/** description de la forme en cours */
	private var p_oCurrForme:Object;
	
	/** le tableau réponse */
	private var p_aAnswer:Array;
	
	/** le tableau des bonnes réponses */
	private var p_aGoodAnswers:Array;
	
	/** l'id de la réponse courante */
	private var p_iCurrAnswerId:Number;
	
	/** true si on doit cliquer sur suite pour continuer */
	private var p_bNeedSuite:Boolean;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_15()	{		
		super();	
	}	

		
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		p_iNbPartsToWin = 3;
		p_iNbPartsWon = 0;
		
		p_iCurrForme = -1;
		
		p_aColors = [0xFF0000, 0xFFCE00, 0x0000FF];
		
		p_aGoodAnswers = [

			//forme 1
			[
				{forme:6, color:0, rotation:0},
				{forme:0, color:2, rotation:180},
				{forme:2, color:0, rotation:90},
				{forme:5, color:2, rotation:90},
				{forme:1, color:2, rotation:0}
			],

			//forme 2
			[
				{forme:6, color:0, rotation:0},
				{forme:3, color:1, rotation:-90},
				{forme:2, color:0, rotation:90},
				{forme:5, color:2, rotation:90},
				{forme:1, color:2, rotation:0}
			],

			//forme 3
			[
				{forme:6, color:2, rotation:0},
				{forme:0, color:1, rotation:0},
				{forme:1, color:0, rotation:0},
				{forme:3, color:2, rotation:-90},
				{forme:4, color:0, rotation:-180}
			],

			//forme 5
			[
				{forme:6, color:0, rotation:0},
				{forme:2, color:1, rotation:90},
				{forme:4, color:1, rotation:-90},
				{forme:0, color:2, rotation:180},
				{forme:1, color:2, rotation:180}
			],

			//forme 6
			[
				{forme:6, color:0, rotation:0},
				{forme:4, color:1, rotation:0},
				{forme:2, color:2, rotation:180},
				{forme:5, color:1, rotation:180},
				{forme:1, color:0, rotation:0}
			],

			//forme 7
			[
				{forme:6, color:1, rotation:0},
				{forme:1, color:2, rotation:0},
				{forme:2, color:0, rotation:-90},
				{forme:3, color:0, rotation:-90},
				{forme:4, color:2, rotation:-90}
			],

			//forme 8
			[
				{forme:6, color:2, rotation:0},
				{forme:2, color:1, rotation:180},
				{forme:1, color:1, rotation:0},
				{forme:3, color:1, rotation:90},
				{forme:4, color:2, rotation:90},
				{forme:0, color:1, rotation:90}
			],

			//forme 9
			[
				{forme:6, color:2, rotation:0},
				{forme:3, color:1, rotation:0},
				{forme:2, color:0, rotation:90},
				{forme:0, color:2, rotation:-90},
				{forme:1, color:2, rotation:180}
			],

			//forme 11
			[
				{forme:6, color:0, rotation:0},
				{forme:3, color:1, rotation:0},
				{forme:1, color:0, rotation:0},
				{forme:5, color:1, rotation:90},
				{forme:0, color:1, rotation:-180}
			],

			//forme 12
			[
				{forme:6, color:2, rotation:0},
				{forme:3, color:0, rotation:-90},
				{forme:2, color:0, rotation:-90},
				{forme:5, color:1, rotation:-90},
				{forme:0, color:0, rotation:0}
			],

			//forme 13
			[
				{forme:6, color:0, rotation:0},
				{forme:3, color:1, rotation:0},
				{forme:1, color:0, rotation:0},
				{forme:5, color:1, rotation:180},
				{forme:0, color:1, rotation:0}
			],

			//forme 14
			[
				{forme:6, color:0, rotation:0},
				{forme:2, color:2, rotation:0},
				{forme:4, color:0, rotation:0},
				{forme:0, color:2, rotation:0},
				{forme:3, color:2, rotation:180},
				{forme:5, color:0, rotation:180},
				{forme:1, color:2, rotation:-90}
			],

			//forme 15
			[
				{forme:6, color:1, rotation:0},
				{forme:2, color:0, rotation:0},
				{forme:3, color:1, rotation:-90},
				{forme:1, color:0, rotation:0},
				{forme:0, color:1, rotation:0}	
			]
			
		];		
		
		p_aFormeAldreayDone = [];

		initInterface();
		
	}	


	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){	
		
		// planetes
		this["mcPlanet0"]._visible = this["mcPlanet1"]._visible = this["mcPlanet2"]._visible = false;	
		
		// boutons gauche/droite
		Application.useLibCursor(p_btLeft, "cursor-finger");
		p_btLeft.onRelease = function(){
			_parent.rotateLeft();
		}
		Application.useLibCursor(p_btRight, "cursor-finger");
		p_btRight.onRelease = function(){
			_parent.rotateRight();
		}
		
		// bouton valider
		Application.useLibCursor(p_btFleche, "cursor-finger");
		p_btFleche.onRelease = function(){
			_parent.valideForme();
		}
		
		// bouton undo
		Application.useLibCursor(p_btUndo, "cursor-finger");
		p_btUndo.onRelease = function(){
			_parent.undo();
		}
		
		Application.useLibCursor(p_btDepartSuite, "cursor-finger");
		p_btDepartSuite.onRelease = function(){
			_parent.newPart();
		}
		
		Application.useLibCursor(p_btChange, "cursor-finger");
		p_btChange.onRelease = function(){
			_parent.reset();
		}
		
		newPart();
		
	}	
	
	/**
	 * Nouvelle question
	 */
	private function newPart():Void{

		p_bNeedSuite = false;
		
		p_btDepartSuite.enabled = false;
		this["txtSuite"]._visible = false;
		
		// init des boutons couleurs
		for (var i=0; i<3; i++){
			this["p_mcColor"+i]["id"] = i;
			Application.useLibCursor(this["p_mcColor"+i], "cursor-finger");
			this["p_mcColor"+i].onRelease = function(){
				_parent.selectColor(this["id"]);
			}
		}
		
		// init des boutons formes
		for (var i=0; i<7; i++){
			this["mcForme"+i]["id"] = i;
			this["mcForme"+i]._alpha = 0;
			this["mcForme"+i].enabled = true;
			Application.useLibCursor(this["mcForme"+i], "cursor-finger");
			this["mcForme"+i].onRelease = function(){
				_parent.selectForme(this["id"]);
			}
		}			
		
		for (var i in p_mcAnswer){
			p_mcAnswer[i].removeMovieClip();
		}
		
		selectColor(0);
		p_iCurrColor = 0;
		p_aAnswer = [];				
		p_iCurrAnswerId = 0;
		
		reset();

	}
	
	private function selectColor(iColor:Number){
		if (p_bNeedSuite) return;
		p_iCurrColor = iColor;
		p_mcColor0._alpha = p_mcColor1._alpha = p_mcColor2._alpha = 0;
		this["p_mcColor"+p_iCurrColor]._alpha = 100;
		setColor(p_iCurrColor);
	}
	
	private function setColor(iColor:Number){
		if (p_bNeedSuite) return;
		var maColor:Color = new Color(p_mcCurrForme.forme);
		maColor.setRGB(p_aColors[p_iCurrColor]);
		p_oCurrForme.color = iColor;
	}
	
	private function selectForme(iForme:Number){
		
		if (p_bNeedSuite) return;
		p_oSoundPlayer.playASound("selectforme",0);
		clearCurrForme();
		
		var mcForme:MovieClip = p_mcCurrForme.attachMovie("mcForme"+iForme, "forme", 1);
		mcForme._x = mcForme._y = 41;
		setColor(p_iCurrColor);

		p_oCurrForme = new Object();
		p_oCurrForme.forme = iForme;
		p_oCurrForme.color = p_iCurrColor;
		p_oCurrForme.rotation = 0;
				
	}	
	
	private function rotateLeft(){
		if (p_bNeedSuite) return;
		p_oSoundPlayer.playASound("rotate",0);
		p_mcCurrForme.forme._rotation -= 90;
		p_oCurrForme.rotation = p_mcCurrForme.forme._rotation;
	}
	private function rotateRight(){
		if (p_bNeedSuite) return;
		p_oSoundPlayer.playASound("rotate",0);
		p_mcCurrForme.forme._rotation += 90;
		p_oCurrForme.rotation = p_mcCurrForme.forme._rotation;
	}
	
	private function valideForme(){
		
		if (p_bNeedSuite) return;
		p_oSoundPlayer.playASound("add",0);
		if(p_oCurrForme.forme != null){
		
			p_mcAnswer.cacheAsBitmap = true;
			var level = p_mcAnswer.getNextHighestDepth();
			var mcLayer:MovieClip = p_mcAnswer.attachMovie("mcForme"+p_oCurrForme.forme, "layer"+level, level);
			mcLayer._x = mcLayer._y = 41;
			mcLayer.cacheAsBitmap = true;
			
			var maColor:Color = new  Color(mcLayer);
			maColor.setRGB(p_aColors[p_oCurrForme.color]);
			
			mcLayer._rotation = p_oCurrForme.rotation;
			
			// freeze du bouton
			this["mcForme"+p_oCurrForme.forme]._alpha = 100;
			this["mcForme"+p_oCurrForme.forme].enabled = false;
					
			// mémorisation
			p_aAnswer.push( {forme:p_oCurrForme.forme, color:p_oCurrForme.color, rotation:p_oCurrForme.rotation} );
					
			// clear 
			clearCurrForme();
			
			// check si bonne réponse
			chechAnswer();
			
		}
		
	}
	
	private function clearCurrForme(){
		if (p_bNeedSuite) return;
		p_oSoundPlayer.playASound("reset",0);
		p_oCurrForme = new Object();
		for(var i in p_mcCurrForme){
			p_mcCurrForme[i].removeMovieClip();
		}
	}
	
	private function undo(){
		
		if (p_bNeedSuite) return;
		p_oSoundPlayer.playASound("reset",0);
		if(p_oCurrForme.forme != null){
			// supprime la forme en cours de construction
			clearCurrForme()
		}else{
			// supprime la dernière forme ajoutée
			var formeId:Number = p_aAnswer.pop().forme;
			this["mcForme"+formeId]._alpha = 0;
			this["mcForme"+formeId].enabled = true;			
			var mcToDelete = p_mcAnswer.getInstanceAtDepth(p_mcAnswer.getNextHighestDepth()-1).removeMovieClip();
		}
		
	}
	
	private function chechAnswer(){
		
		var bmp1:BitmapData = new BitmapData(82,82);
		var bmp2:BitmapData = new BitmapData(82,82);

		bmp1.draw(p_mcAnswer);
		bmp2.draw(p_mcPochoirs);
		
		var ok:Boolean = true;
		
		for(var i=1; i<82; i+=4){
			for(var j=2; j<82; j+=3){
				if(Math.abs(i-j)>4 && Math.abs(82-(i+j)) > 5){
					var pix1 = bmp1.getPixel(i,j);
					var pix2 = bmp2.getPixel(i,j);
					if(pix1 != pix2){
						ok = false;
					}
				}
			}
		}
		
		if(ok){			
			p_aFormeAldreayDone.push(p_iCurrForme);
			this["mcPlanet"+p_iNbPartsWon]._visible = true;
			p_iNbPartsWon++;
			if(p_iNbPartsWon < p_iNbPartsToWin){
				this["txtSuite"]._visible = true;
				p_bNeedSuite = true;
				p_btDepartSuite.enabled = true;
			}else{
				won();
			}
		}
		
	}
	
	private function reset(){
		if (p_bNeedSuite) return;
		p_oSoundPlayer.playASound("reset",0);
		var ok:Boolean = false;
		while(!ok){
			p_iCurrForme = Math.floor(Math.random()*p_aGoodAnswers.length);
			if(OutilsTableaux.getPosition(p_aFormeAldreayDone, p_iCurrForme)==undefined){
				ok = true;
			}else{
				trace("déjà fait !");
			}
		}
		p_mcPochoirs.cacheAsBitmap = true;
		var mcFormes:MovieClip = p_mcPochoirs.createEmptyMovieClip("mcForme",1);
		for(var i=0; i<p_aGoodAnswers[p_iCurrForme].length;i++){
			var mcFormeX:MovieClip = mcFormes.attachMovie("mcForme"+p_aGoodAnswers[p_iCurrForme][i].forme, "forme"+i, i);
			mcFormeX.cacheAsBitmap = true;
			mcFormeX._x = mcFormeX._y = 41;
			mcFormeX._rotation = p_aGoodAnswers[p_iCurrForme][i].rotation;
			var col:Color = new Color(mcFormeX);
			col.setRGB(p_aColors[p_aGoodAnswers[p_iCurrForme][i].color]);
		}
	}
	
}

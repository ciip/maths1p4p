﻿/**
 * class com.maths1p4p.level4.game4_16.Game4_16
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * semble bien fonctionner
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.utils.OutilsTableaux;
import maths1p4p.application.Application;
import com.prossel.utils.CursorManager;

class maths1p4p.level4.game4_16.Game4_16 extends maths1p4p.level4.Level4AbstractGame{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	/** les objets */
	public var p_mcRondJaune, p_mcRondRouge, p_mcRondBleu:MovieClip;
	public var p_mcCarreJaune, p_mcCarreRouge, p_mcCarreBleu:MovieClip;
	public var p_mcCroixJaune, p_mcCroixRouge, p_mcCroixBleu:MovieClip;

	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	/** les solutions */
	private var p_aSoluces:Array;
	
	/** liste des objets */
	private var p_aObjects:Array;
	
	/** positions courantes */
	private var p_aCurrPos:Array;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function Game4_16()	{		
		super();				
	}


	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	/**
	 * démmarage du jeu, implémentée
	 */
	private function startGame(){	
		
		p_iNbPartsToWin = 2;
		p_iNbPartsWon = 0;

		p_aSoluces = [
			[
				[p_mcRondRouge, p_mcCarreRouge, p_mcCroixRouge],
				[p_mcRondBleu, p_mcCarreBleu, p_mcCroixBleu],
				[p_mcRondJaune, p_mcCarreJaune, p_mcCroixJaune]
			],
			[
				[p_mcCroixJaune, p_mcCroixBleu, p_mcCroixRouge],
				[p_mcCarreJaune, p_mcCarreBleu, p_mcCarreRouge],
				[p_mcRondJaune, p_mcRondBleu, p_mcRondRouge]
			]
		];
		
		p_aObjects = [p_mcRondRouge, p_mcCarreRouge, p_mcCroixRouge, p_mcRondBleu, p_mcCarreBleu, p_mcCroixBleu, p_mcRondJaune, p_mcCarreJaune, p_mcCroixJaune];
		
		// planetes
		this["mcPlanet0"]._visible = this["mcPlanet1"]._visible = false;
		
		initInterface();
		
	}	
	
	/**
	 * initialisation de l'interface
	 */
	private function initInterface(){		
		
		// random des objest
		p_aObjects = OutilsTableaux.shuffle(p_aObjects);
		// init des positions
		p_aCurrPos = [];
		var count:Number = 0;
		for(var i=0; i<4; i++){
			p_aCurrPos[i] = [];;
			for(var j=0; j<3; j++){
				p_aCurrPos[i].push(p_aObjects[count]);
				p_aObjects[count]["i"] = i;
				p_aObjects[count]["j"] = j;
				this["mcSlot"+String(i)+String(j)]["i"] = i;
				this["mcSlot"+String(i)+String(j)]["j"] = j;
				p_aObjects[count]._x = this["mcSlot"+String(i)+String(j)]._x;
				p_aObjects[count]._y = this["mcSlot"+String(i)+String(j)]._y;
				Application.useLibCursor(p_aObjects[count], "cursor-finger");
				p_aObjects[count].onPress = function(){
					_parent.drag(this);
				}
				p_aObjects[count].onRelease = p_aObjects[count].onReleaseOutside = function(){
					CursorManager.hide();
					_parent.drop(this);
					CursorManager.show();
				}
				
				count++;				
			}
			
		}
	}	
	
	
	private function drag(mcObj:MovieClip){
		// test si l'objet peut bouger
		var bCanMove:Boolean;
		if(mcObj["j"]==0){
			bCanMove = true;
		}else if(p_aCurrPos[mcObj["i"]][mcObj["j"]-1] == null){
			bCanMove = true;		
		}else{
			bCanMove = false;
		}
		if(bCanMove){
			mcObj._x = _xmouse - mcObj._width/2;
			mcObj._y = _ymouse - mcObj._height/2;
			mcObj.swapDepths(getNextHighestDepth());
			mcObj.startDrag();
			mcObj["draging"] = true;
		}
	}
	
	
	private function drop(mcObj:MovieClip){
		
		mcObj.stopDrag();
		var mcTarget:MovieClip = eval(mcObj._droptarget);		
		trace("target " + mcTarget);
		if(mcObj["draging"] && mcObj._droptarget.indexOf("mcSlot")!=-1 && mcTarget["i"] != mcObj["i"]){
			
			// test la colonne
			var dropId:Number=-1;
			for(var i=0; i<3;i++){
				if(p_aCurrPos[mcTarget["i"]][i] == null){
					dropId = i;
				}
			}
			
			if(dropId!=-1){
				var mcDest:MovieClip =  this["mcSlot"+String(mcTarget["i"])+String(dropId)];
				mcObj._x = mcDest._x;
				mcObj._y = mcDest._y;
				p_aCurrPos[mcObj["i"]][mcObj["j"]] = null;
				p_aCurrPos[mcTarget["i"]][dropId] = mcObj;
				mcObj["i"] = mcTarget["i"];
				mcObj["j"] = dropId;
			}else{
				mcObj._x = this["mcSlot"+String(mcObj["i"])+String(mcObj["j"])]._x;
				mcObj._y = this["mcSlot"+String(mcObj["i"])+String(mcObj["j"])]._y;
			}
			
			mcObj["draging"] = false;
			
			if(checkResponse()){
				this["mcPlanet"+p_iNbPartsWon]._visible = true;
				
				p_iNbPartsWon++;
				if(p_iNbPartsWon == p_iNbPartsToWin){
					won();
				}else{
					initInterface();
					gotoAndStop("suite");
				}
			}
			
		}else{
			mcObj._x = this["mcSlot"+String(mcObj["i"])+String(mcObj["j"])]._x;
			mcObj._y = this["mcSlot"+String(mcObj["i"])+String(mcObj["j"])]._y;
			stopDrag();
		}
		
	}
	
	private function checkResponse(){
		
		for(var i=0; i<p_aSoluces[p_iNbPartsWon].length; i++){
			for(var j=0; j<p_aSoluces[p_iNbPartsWon][i].length; j++){
				if(p_aCurrPos[i][j] != p_aSoluces[p_iNbPartsWon
				][i][j]){
					return false;
				}
			}
		}
		return true;		
	}
	
}

﻿/**
 * class maths1p4p.level4.specialStages.mcScreen_010
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level4.specialStages.mcScreen_010 extends maths1p4p.level4.Level4AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** anticlic */
	public var p_btBG:Button;

	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_aCode:Array;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_010()	{		
		
		super();			
	
		p_btBG.onPress = function(){};
		p_btBG.useHandCursor = false;
		
		p_aCode = _parent._parent.getPlayer().getChemineeCode();
		
		// boutons et textfields
		for(var i=0;i<3;i++){
			this["bt_txt"+i]["id"] = i;
			Application.useLibCursor(this["bt_txt"+i], "cursor-finger");
			this["bt_txt"+i].onRelease = function(){
				_parent.clickOnNum(this["id"]);
			}			
		}
		
		Application.useLibCursor(this["bt_Dir0"], "cursor-finger");
		this["bt_Dir0"].onRelease = function(){
			_parent.checkPass();
		}		

	}


	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	private function clickOnNum(iNum:Number){
		var currNum:Number = Number(this["txt_num"+iNum].text);
		currNum++;
		if (currNum==10)currNum=0;
		this["txt_num"+iNum].text = currNum;
		_parent._parent.playSound("porteCheminee");
	}
	
	private function checkPass(){		
		var ok:Boolean = true;
		for(var i=0;i<3;i++){
			if(Number(this["txt_num"+i].text) != p_aCode[i]){
				ok = false;
			}
		}		
		if(ok){
			gotoAndPlay(2);
			_parent._parent.playSound("openMachine1");
		}		
	}
	
	private function initMachine(){
		this["mcMachine"].init();		
	}
	
	
}

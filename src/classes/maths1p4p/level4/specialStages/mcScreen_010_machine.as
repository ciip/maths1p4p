﻿/**
 * class maths1p4p.level4.specialStages.mcScreen_010_machine
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level4.specialStages.mcScreen_010_machine extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	public var loot0inited:Boolean = false;


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------


	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_010_machine()	{	
	}
	
	public function init(){
		
			
		
		this["loot_0_0"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[0].$loot[0].__text == "1");
		this["loot_0_1"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[0].$loot[1].__text == "1");
		this["loot_0_2"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[0].$loot[2].__text == "1");
		this["loot_0_3"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[0].$loot[3].__text == "1");
		
		initButtons();
		
		if(isMachineReady()){
			enableButtons(true);
		}else{
			enableButtons(false);	
		}
			
	}

	public function addLoot(i:Number, j:Number){
		if(this["loot_"+i+"_"+j] != null){
			this["loot_0_"+j]._visible = true;
			_parent._parent._parent.hideLoot("loot_0_"+j);
			_parent._parent._parent.getPlayerData().state.$machine[0].$loot[j].__text = 1;
			if(isMachineReady()){
				enableButtons(true);
			}
		}
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	private function enableButtons(val:Boolean):Void{
		this["bt_start0"].enabled = val;
		this["bt_start1"].enabled = val;
		this["bt_0_0"].enabled = val;
		this["bt_0_1"].enabled = val;
		this["bt_1_0"].enabled = val;
		this["bt_1_1"].enabled = val;
		this["bt_2_0"].enabled = val;
		this["bt_2_1"].enabled = val;
		this["bt_3_0"].enabled = val;
		this["bt_3_1"].enabled = val;		
	}
	
	private function initButtons():Void{
		
		Application.useLibCursor(this["bt_start0"], "cursor-finger")
		this["bt_start0"].onRelease = function(){
			_parent["loot_0_2"].gotoAndStop(2);
			_parent._parent._parent._parent.playSound("machine1_bt1");
		}
		Application.useLibCursor(this["bt_start1"], "cursor-finger")
		this["bt_start1"].onRelease = function(){
			if(_parent["loot_0_2"]._currentframe == 2){
				if(_parent["loot_0_0"]._currentframe == 1 && _parent["loot_0_1"]._currentframe == 1){
					_parent["boule1"].gotoAndPlay("anim2");
				}else{
					_parent["boule1"].gotoAndPlay("anim1");
				}
				_parent["loot_0_2"].gotoAndStop(1);
				_parent["loot_0_2"]._visible = false;
			}
			_parent._parent._parent._parent.playSound("machine1_bt2");
		}	
		Application.useLibCursor(this["bt_0_0"], "cursor-finger")
		this["bt_0_0"].onRelease = function(){
			_parent["loot_0_1"].gotoAndStop(1);
			_parent._parent._parent._parent.playSound("machine1_bt3");
		}
		Application.useLibCursor(this["bt_0_1"], "cursor-finger")
		this["bt_0_1"].onRelease = function(){
			_parent["loot_0_1"].gotoAndStop(2);
			_parent._parent._parent._parent.playSound("machine1_bt3");
		}
		Application.useLibCursor(this["bt_1_0"], "cursor-finger")
		this["bt_1_0"].onRelease = function(){
			_parent["loot_0_0"].nextFrame();
			_parent._parent._parent._parent.playSound("machine1_bt4");
		}
		Application.useLibCursor(this["bt_1_1"], "cursor-finger")
		this["bt_1_1"].onRelease = function(){
			_parent["loot_0_0"].prevFrame();
			_parent._parent._parent._parent.playSound("machine1_bt4");
		}
		Application.useLibCursor(this["bt_2_0"], "cursor-finger")
		this["bt_2_0"].onRelease = function(){
			if(_parent["loot_0_0"]._currentframe==3 && _parent["mcManette"]._visible==false){
				_parent["loot_0_3"].gotoAndStop(1);
				_parent._parent._parent._parent.playSound("machine1_bt5");
			}
		}
		Application.useLibCursor(this["bt_2_1"], "cursor-finger")
		this["bt_2_1"].onRelease = function(){
			if(_parent["loot_0_0"]._currentframe==3 && _parent["mcManette"]._visible==false){
				_parent["loot_0_3"].gotoAndStop(2);
				_parent._parent._parent._parent.playSound("machine1_bt5");
			}
		}
		Application.useLibCursor(this["bt_3_0"], "cursor-finger")
		this["bt_3_0"].onRelease = function(){
			if(_parent["mcManette"]._visible){
				_parent["mcManette"]._visible = false;
				_parent["eau"].gotoAndPlay(18);
				if(_parent["boule2"]._currentframe == 83){
					_parent["boule2"].gotoAndPlay("anim4");
				}
				_parent._parent._parent._parent.playSound("machine1_bt6");
				_parent._parent._parent._parent.playSound("machine1_eau");
			}
		}
		Application.useLibCursor(this["bt_3_1"], "cursor-finger")
		this["bt_3_1"].onRelease = function(){
			if(!_parent["mcManette"]._visible){
				_parent["mcManette"]._visible = true;
				_parent["eau"].gotoAndPlay(2);
				if(_parent["boule2"]._currentframe == 67){
					_parent["boule2"].gotoAndPlay("anim3");
				}
				_parent._parent._parent._parent.playSound("machine1_bt6");
				_parent._parent._parent._parent.playSound("machine1_eau");
			}
		}		
		
	}

	private function isMachineReady():Boolean{
		return (this["loot_0_0"]._visible==true && this["loot_0_1"]._visible==true && this["loot_0_2"]._visible==true && this["loot_0_3"]._visible==true);
	}
	
	private function won(){
		_parent._parent._parent.getPlayerData().state.$machine[0].done.__text = "1";
		_parent._parent._parent.gotoScene("011");
	}
	
}

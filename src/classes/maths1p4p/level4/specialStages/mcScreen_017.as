﻿/**
 * class maths1p4p.level4.specialStages.mcScreen_010
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level4.specialStages.mcScreen_017 extends maths1p4p.level4.Level4AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_017()	{		
		
		super();			
		
		if(_global.telescope_lumiere != null){
			gotoAndStop(_global.telescope_lumiere);
			this["fenetre"].gotoAndStop(_global.telescope_lumiere);
		}
		if(_global.telescope_fenetre != null){
			this["fenetre"]._visible = _global.telescope_fenetre;
		}else{
			this["fenetre"]._visible = false;
		}
		
		Application.useLibCursor(this["bt_console"], "cursor-finger");
		this["bt_console"].onRelease = function(){
			_parent._parent._parent.gotoScene("018");
		}
		
		Application.useLibCursor(this["bt_fenetre"], "cursor-finger");
		this["bt_fenetre"].onRelease = function(){
			_global.telescope_fenetre = !_parent.fenetre._visible;	
			_parent.fenetre._visible = !_parent.fenetre._visible;	
			_parent._parent._parent.playSound("telescope_open");
		}
		
		Application.useLibCursor(this["bt_light"], "cursor-finger");
		this["bt_light"].onRelease = function(){			
			if(_parent._currentframe==1){
				_parent.gotoAndStop(2);
				_parent.fenetre.gotoAndStop(2);
				_global.telescope_lumiere = 2;
			}else{
				_parent.gotoAndStop(1);
				_parent.fenetre.gotoAndStop(1);
				_global.telescope_lumiere = 1;
			}
			_parent._parent._parent.playSound("telescope_light");
		}		

	}


	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	
	
	
}

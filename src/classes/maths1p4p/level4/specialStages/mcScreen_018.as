﻿/**
 * class maths1p4p.level4.specialStages.mcScreen_010
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level4.specialStages.mcScreen_018 extends maths1p4p.level4.Level4AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	private var ready:Boolean;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_018()	{		
		
		super();	
		
		ready = false;
		
		this["bg"].useHandCursor = false;
		this["bg"].onRelease = function(){};		
		
		Application.useLibCursor(this["bt_telescope"], "cursor-finger");
		this["bt_telescope"].onRelease = function(){
			_parent.telescope();
		}
		
		Application.useLibCursor(this["bt_contact"], "cursor-finger");
		this["bt_contact"].onRelease = function(){
			_parent.contact();
		}
		
		for(var i=0; i<4; i++){
			Application.useLibCursor(this["rond_"+i], "cursor-finger");
			this["rond_"+i].onRelease = function(){
				if(this._currentframe==this._totalframes){
					this.gotoAndStop(1);
				}else{
					this.nextFrame();
				}
			}
		}
		

	}


	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	private function telescope(){
		
		if(_global.telescope_lumiere == 2 && _global.telescope_fenetre == true){

			var oState:Object = _parent._parent.getPlayerData().state;
			var oResults:Object = {
				bleu:1,
				jaune:2,
				vert:3			
			}
			
			var ok:Boolean = true;
			for(var i=0; i<4; i++){
				if(this["rond_"+i]._currentframe != oResults[oState.colors.$color[i].__text]){
					ok = false;
				}
			}

			if(ok){
				this["mcAnim"].gotoAndStop("ok");
				ready = true;
			}else{
				var iFrame = Math.floor(Math.random()*4);
				this["mcAnim"].gotoAndStop("mauvais_"+iFrame);
				ready = false;
			}
			
		}else{
			if(_global.telescope_lumiere == 2){
				trace("eteint la lumiere");
			}else{
				trace("ouvre la fenetre");
			}
		}
		
		_parent._parent.playSound("telescope_locate");
		
	}
	
	private function contact(){
		if(ready){
			this["mcAnim"].gotoAndPlay("contact");
			_parent._parent.playSound("telescope_locate");
		}
	}
	
	public function animationFinished(){
		_parent._parent.gotoScene("071");
	}
	
	
}

﻿/**
 * class maths1p4p.level4.specialStages.mcScreen_010
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level4.specialStages.mcScreen_035 extends maths1p4p.level4.Level4AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------


	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_035()	{				
		super();	
		this.onEnterFrame = function(){
			this["mcMachine"].init();	
			delete this.onEnterFrame;
		}
	}
	
	private function animationFinished(){
		_parent._parent.gotoScene("036");
	}
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	
}

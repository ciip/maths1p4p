﻿/**
 * class maths1p4p.level4.specialStages.mcScreen_035_machine
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level4.specialStages.mcScreen_035_machine extends maths1p4p.level4.Level4AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_nNumberToFind:Number;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_035_machine()	{				
		super();	
	}

	public function init(){

		this["loot_1_0"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[1].$loot[0].__text == "1");
		this["loot_1_1"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[1].$loot[1].__text == "1");
		this["loot_1_2"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[1].$loot[2].__text == "1");
		this["loot_1_3"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[1].$loot[3].__text == "1");
		
		p_nNumberToFind = getNumberToFind();
		this["txt"].text = String(p_nNumberToFind);
		
		Application.useLibCursor(this["loot_1_0"], "cursor-finger");
		this["loot_1_0"]["val"] = 8;
		this["loot_1_0"].onRelease = function(){
			if (this._currentframe == this._totalframes){
				gotoAndStop(1);
			}else{
				nextFrame();
			}
			_parent._parent._parent.playSound("machine2_3");
		}
		Application.useLibCursor(this["loot_1_1"], "cursor-finger");
		this["loot_1_1"]["val"] = 5;
		this["loot_1_1"].onRelease = function(){
			if (this._currentframe == this._totalframes){
				gotoAndStop(1);
			}else{
				nextFrame();
			}
			_parent._parent._parent.playSound("machine2_3");
		}		
		Application.useLibCursor(this["loot_1_2"], "cursor-finger");
		this["loot_1_2"]["val"] = 3;
		this["loot_1_2"].onRelease = function(){
			if (this._currentframe == this._totalframes){
				gotoAndStop(1);
			}else{
				nextFrame();
			}
			_parent._parent._parent.playSound("machine2_3");
		}		
		Application.useLibCursor(this["loot_1_3"], "cursor-finger");
		this["loot_1_3"].onPress = function(){
			gotoAndStop(2);
			_parent._parent._parent.playSound("machine2_3");
		}
		this["loot_1_3"].onRelease = function(){
			gotoAndStop(1);
			_parent.check();
		}
			
	}

	public function addLoot(i:Number, j:Number){
		if(this["loot_"+i+"_"+j] != null){
			this["loot_1_"+j]._visible = true;
			_parent._parent._parent.hideLoot("loot_1_"+j);
			_parent._parent._parent.getPlayerData().state.$machine[1].$loot[j].__text = 1;			
		}
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	private function isMachineReady():Boolean{
		return (this["loot_1_0"]._visible==true && this["loot_1_1"]._visible==true && this["loot_1_2"]._visible==true && this["loot_1_3"]._visible==true);
	}
	
	private function check(){
		
		var res:Number = this["loot_1_0"]["val"]*this["loot_1_0"]._currentframe + this["loot_1_1"]["val"]*this["loot_1_1"]._currentframe + this["loot_1_2"]["val"]*this["loot_1_2"]._currentframe;
		trace("res : "+res);
		trace("ro find : " + p_nNumberToFind);
		
		if(p_nNumberToFind == res){
			won();
		}
		
	}
	
	private function getNumberToFind():Number{
		
		var res:Number=0;
		res += (Math.floor(Math.random()*5)+1)*8;
		res += (Math.floor(Math.random()*5)+1)*5;
		res += (Math.floor(Math.random()*5)+1)*3;
		return res;
		
	}
	
	private function won(){
		_parent._parent._parent.getPlayerData().state.$machine[1].done.__text = "1";
		_parent.gotoAndPlay(2);
		this["loot_1_0"].enabled = false;
		this["loot_1_1"].enabled = false;
		this["loot_1_2"].enabled = false;
		this["loot_1_3"].enabled = false;
	}
	
}

﻿/**
 * class maths1p4p.level4.specialStages.mcScreen_010
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level4.specialStages.mcScreen_052_machine extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var __mcGotBall:MovieClip;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_052_machine()	{				
		super();	
	}

	public function init(){

		this["loot_2_0"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[2].$loot[0].__text == "1");
		this["loot_2_1"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[2].$loot[1].__text == "1");
		this["loot_2_2"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[2].$loot[2].__text == "1");
		this["loot_2_3"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[2].$loot[3].__text == "1");
		
		__mcGotBall = this["loot_2_1"];
		
		this["bouton2"]._visible = false;
		this["ressort"]._visible = false;
		
		Application.useLibCursor(this["loot_2_0"], "cursor-finger");
		Application.useLibCursor(this["loot_2_1"], "cursor-finger");
		Application.useLibCursor(this["loot_2_2"], "cursor-finger");
		Application.useLibCursor(this["loot_2_3"], "cursor-finger");
				
		initButtons();
		
		if(isMachineReady()){
			enableButtons(true);
		}else{
			enableButtons(false);	
		}
			
	}

	public function addLoot(i:Number, j:Number){
		if(this["loot_"+i+"_"+j] != null){
			this["loot_2_"+j]._visible = true;
			_parent._parent._parent.hideLoot("loot_2_"+j);
			_parent._parent._parent.getPlayerData().state.$machine[2].$loot[j].__text = 1;			
			if (isMachineReady()){
				__mcGotBall = this["loot_2_1"];
				enableButtons(true);
			}			
		}
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	private function isMachineReady():Boolean{
		return (this["loot_2_0"]._visible==true && this["loot_2_1"]._visible==true && this["loot_2_2"]._visible==true && this["loot_2_3"]._visible==true);
	}
	
	private function enableButtons(val:Boolean):Void{
		this["loot_2_0"].enabled = val;
		this["bouton"].enabled = val;
		this["bt_restart"].enabled = val;
		this["bt_start0"].enabled = val;
		this["bt_start1"].enabled = val;
		this["bt0"].enabled = val;
		this["bt1"].enabled = val;
		this["machine1"]["bt0"].enabled = val;
		this["machine1"]["bt1"].enabled = val;
		this["bouton2"].enabled = val;
		this["bt_end"].enabled = val;		
	}	
	
	private function initButtons():Void{
		
		// roue
		Application.useLibCursor(this["loot_2_0"], "cursor-finger")
		this["loot_2_0"].onRelease = function(){
			if(_currentframe==1){
				gotoAndStop(2);
			}else{
				gotoAndStop(1);
			}
			if(_parent["loot_2_2"].rond._currentframe==_parent["loot_2_2"].rond._totalframes){
				_parent["loot_2_2"].rond.gotoAndStop(1);
			}else{
				_parent["loot_2_2"].rond.nextFrame();
			}	
			if(_parent["loot_2_2"].rond.boule._visible){
				_parent["loot_2_2"].rond.boule.gotoAndPlay("anim");
			}
			
			if(_parent["loot_2_2"]._currentframe==2){
				if(_parent["loot_2_3"].rond._currentframe==_parent["loot_2_3"].rond._totalframes){
					_parent["loot_2_3"].rond.gotoAndStop(1);
				}else{
					_parent["loot_2_3"].rond.nextFrame();
				}		
				if(_parent["loot_2_3"].rond.boule._visible){
					_parent["loot_2_3"].rond.boule.gotoAndPlay("anim");
				}
			}
			_parent._parent._parent._parent.playSound("machine3_roue");
		}
		
		// bouton
		Application.useLibCursor(this["bouton"], "cursor-finger")
		this["bouton"].onRelease = function(){
			if(_parent["loot_2_2"]._currentframe==1){
				_parent["loot_2_2"].gotoAndStop(2);
				gotoAndStop(2);
				_parent["cache"]._visible = true;
				_parent["tube1"].gotoAndStop(2);;
			}else{
				_parent["loot_2_2"].gotoAndStop(1);
				gotoAndStop(1);
				_parent["cache"]._visible = false;
				_parent["tube1"].gotoAndStop(1);
			}
		}
		
		// bouton départ
		Application.useLibCursor(this["bt_start0"], "cursor-finger")
		this["bt_start0"].onRelease = function(){
			if(!_parent["bouton2"]._visible){
				_parent["bouton2"]._visible = true;
				_parent["ressort"]._visible = true;
				_parent["loot_2_1"].gotoAndStop(2);
				_parent["loot_2_1"].mcBoule._visible = (_parent.__mcGotBall == _parent["loot_2_1"]);			
				_parent._parent._parent._parent.playSound("machine3_bt1");
			}
		}
		Application.useLibCursor(this["bt_start1"], "cursor-finger")
		this["bt_start1"].onRelease = function(){
			if(_parent["bouton2"]._visible){
				_parent["ressort"]._visible = false;
				_parent["bouton2"]._visible = false;
				if(!_parent["cache"]._visible){
					_parent["loot_2_1"].gotoAndPlay("anim1");
				}else{
					if(_parent["loot_2_2"]._currentframe == 2 && ( _parent["loot_2_2"]["rond"]._currentframe==4 || _parent["loot_2_2"]["rond"]._currentframe==6 )){
						_parent["loot_2_1"].gotoAndPlay("anim3");
					}else{
						_parent["loot_2_1"].gotoAndPlay("anim2");
					}						
				}
				_parent["loot_2_1"].mcBoule._visible = (_parent.__mcGotBall == _parent["loot_2_1"]);
				_parent._parent._parent._parent.playSound("machine3_bt2");
			}
		}
		
		// boutons machine1
		Application.useLibCursor(this["machine1"]["bt0"], "cursor-finger")
		Application.useLibCursor(this["machine1"]["bt1"], "cursor-finger")
		this["machine1"]["bt0"].onRelease = function(){
			if(_parent._currentframe == 1){
				_parent.truc._visible = false;
			}
			_parent._parent._parent._parent._parent.playSound("machine3_bt3");
		}	
		this["machine1"]["bt1"].onRelease = function(){

			if(_parent._parent.__mcGotBall == _parent){
				_parent._parent.__mcGotBall = null;
				_parent._parent["animLooseBall2"].gotoAndPlay(2);
			}			
			_parent.gotoAndStop(1);
			_parent.truc._visible = true;
			_parent._parent._parent._parent._parent.playSound("machine3_bt3");

		}	
		
		// boutons machine 2
		Application.useLibCursor(this["bt0"], "cursor-finger")
		this["bt0"].onRelease = function(){
			_parent.machine2.prevFrame();
			_parent._parent._parent._parent.playSound("machine3_bt4");
		}	
		Application.useLibCursor(this["bt1"], "cursor-finger")
		this["bt1"].onRelease = function(){
			if(_parent.machine1._currentframe == 3){
				_parent.machine1.obj = 0;
				_parent.machine2.obj = 0;
			}
			_parent.machine2.nextFrame();
			_parent._parent._parent._parent.playSound("machine3_bt4");
		}		
		
		// bouton bleu
		Application.useLibCursor(this["bouton3"], "cursor-finger")
		this["bouton3"].enabled = false;
		this["bouton3"].onRelease = function(){
			if(_parent.bidule._currentframe >= 4){
				_parent.final._visible = true;
				_parent.porte._visible = true;
				_parent._parent._parent._parent.playSound("machine3_bt5");
			}
		}
		
		// bouton porte
		Application.useLibCursor(this["porte"].bt, "cursor-finger")
		this["porte"].bt.onRelease = function(){
			_parent.gotoAndPlay(2);
			_parent._parent._parent._parent.playSound("machine3_bt6");
		}
		
		// reset
		Application.useLibCursor(this["bt_restart"], "cursor-finger")
		this["bt_restart"].onRelease = function(){
			_parent._parent.gotoFrame(2);
		}
		
	}
	
	public function looseBall(){		
		__mcGotBall = this["loot_2_1"];
		this["loot_2_1"].gotoAndStop(1);
		this["loot_2_1"].mcBoule._visible = true;		
	}
	
	public function addBallToWheel1():Void{
		__mcGotBall = this["loot_2_2"];
		this["loot_2_1"].mcBoule._visible = false;
		this["loot_2_2"].rond.boule.gotoAndPlay("anim2");		
		this["loot_2_2"].rond.boule._visible = true;		
	}
	
	
	public function addBallToWheel2():Void{

		if (this["loot_2_2"]._currentframe==2){
			if (this["loot_2_3"].rond._currentframe==2 || this["loot_2_3"].rond._currentframe==4){
				__mcGotBall = this["loot_2_3"];
				this["loot_2_2"].rond.boule._visible = false;		
				this["loot_2_3"].rond.boule._visible = true;					
				if(this["loot_2_3"].rond._currentframe==4){
					this["loot_2_3"].rond.boule.gotoAndPlay("anim2");
				}else{
					this["loot_2_3"].rond.boule.gotoAndPlay("anim");
				}
			}
		}
		
		
	}
		
	public function addBallToTruc():Void{
		this["loot_2_3"].rond.boule._visible = false;
		
		if(this["machine1"].truc._visible == false){
			__mcGotBall = this["machine1"];
			switch(this["machine2"]._currentframe){
				case 1:
					this["machine1"].gotoAndStop(3);
					break;
				case 2:
					this["machine1"].gotoAndStop(1);
					this["machine1"].boule._visible = true;
					break;
				case 3:
					this["machine1"].gotoAndStop(2);
					break;
			}
		}else{
			this["loot_2_3"].rond.boule._visible = false;
			this["animLooseBall3"].gotoAndPlay(2);
			//looseBall();
		}
	}
	
	
	private function won(){
		_parent._parent._parent.getPlayerData().state.$machine[2].done.__text = "1";
		_parent.gotoFrame(4);
	}
	
}

﻿/**
 * class maths1p4p.level4.specialStages.mcScreen_059
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level4.specialStages.mcScreen_059 extends maths1p4p.level4.Level4AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_059()	{		
		
		super();	
		frame1();

	}
	
	public function clickOnPotion():Void{
		trace("clickOnPotion");
	}


	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	private function frame1():Void{
		
		gotoAndStop(1);
		Application.useLibCursor(this["bt_subDir0"], "cursor-finger");
		this["bt_subDir0"].onRelease = function(){
			if (_parent._parent._parent.hasObject("potion")){
				if(_parent._parent._parent.getLastMessage() >= 5){
					_parent.frameEnd();
				}else{
					_parent.frame2(false);
				}
			}else{
				_parent.frame2(true);
			}
		}
	}
	
	private function frame2(freezed:Boolean){
		gotoAndStop(2);
		Application.useLibCursor(this["bt_subDir3"], "cursor-finger");
		this["bt_subDir3"].onRelease = function(){
			_parent.frame3();
		}
		this["bt_subDir3"]._visible = !freezed;
	}
	
	private function frame3(){
		gotoAndPlay(3);
		_parent._parent.deleteObject("potion");
	}
	
	private function frame35(){
		gotoAndStop(35);
		Application.useLibCursor(this["bt_subDir1"], "cursor-finger");
		this["bt_subDir1"].onRelease = function(){
			_parent.frame36();
		}
		_parent._parent.addMessage(5);
	}
	private function frame36(){
		gotoAndStop(36);
		Application.useLibCursor(this["bt_subDir2"], "cursor-finger");
		this["bt_subDir2"].onRelease = function(){
			_parent.frameEnd();
		}
		_parent._parent.addMessage(6);
	}	
	
	private function frameEnd():Void{
		gotoAndStop(37);
	}
	
	
}

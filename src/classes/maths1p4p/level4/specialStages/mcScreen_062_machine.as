﻿/**
 * class maths1p4p.level4.specialStages.mcScreen_062_machine
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level4.specialStages.mcScreen_062_machine extends MovieClip{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	


	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var __mcGotBall:MovieClip;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_062_machine()	{				
		super();	
		init();
	}

	public function init(){

		this["loot_3_0"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[3].$loot[0].__text == "1");
		this["loot_3_1"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[3].$loot[1].__text == "1");
		this["loot_3_2"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[3].$loot[2].__text == "1");
		this["loot_3_3"]._visible = (_parent._parent._parent.getPlayerData().state.$machine[3].$loot[3].__text == "1");
				
		initButtons();
		
		if(isMachineReady()){
			enableButtons(true);
		}else{
			enableButtons(false);	
		}
			
	}

	public function addLoot(i:Number, j:Number){
		if(this["loot_"+i+"_"+j] != null){
			this["loot_3_"+j]._visible = true;
			_parent._parent._parent.hideLoot("loot_3_"+j);
			_parent._parent._parent.getPlayerData().state.$machine[3].$loot[j].__text = 1;			
			if (isMachineReady()){
				__mcGotBall = this["loot_3_1"];
				enableButtons(true);
			}			
		}
	}
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	private function isMachineReady():Boolean{
		return (this["loot_3_0"]._visible==true && this["loot_3_1"]._visible==true && this["loot_3_2"]._visible==true && this["loot_3_3"]._visible==true);
	}
	
	private function enableButtons(val:Boolean):Void{
		this["bt1"].enabled = val;
		this["bt2"].enabled = val;
		this["bt3"].enabled = val;
		this["btBarre"].enabled = val;		
	}	
	
	private function initButtons():Void{
		
		Application.useLibCursor(this["bt1"], "cursor-finger");
		this["bt1"].onRelease = function(){
			_parent.NextFrame(_parent["loot_3_0"]);
			_parent.NextFrame(_parent["loot_3_1"]);
			_parent._parent._parent._parent.playSound("machine4_bt1");
		}
		Application.useLibCursor(this["bt2"], "cursor-finger");
		this["bt2"].onRelease = function(){
			_parent.NextFrame(_parent["loot_3_1"]);
			_parent.PrevFrame(_parent["loot_3_2"]);
			_parent._parent._parent._parent.playSound("machine4_bt1");
		}
		Application.useLibCursor(this["bt3"], "cursor-finger");
		this["bt3"].onRelease = function(){
			_parent.PrevFrame(_parent["loot_3_0"]);
			_parent.NextFrame(_parent["loot_3_1"]);
			_parent.PrevFrame(_parent["loot_3_2"]);
			_parent._parent._parent._parent.playSound("machine4_bt1");
		}
		
		Application.useLibCursor(this["btBarre"], "cursor-finger");
		this["btBarre"].onRelease = function(){
			if(_parent["loot_3_0"]._currentframe==6 && _parent["loot_3_1"]._currentframe==1 && _parent["loot_3_2"]._currentframe==5){
				_parent.won();
			}
		}		
		
	}
		
	private function NextFrame(mc:MovieClip){
		if(mc._currentframe==mc._totalframes){
			mc.gotoAndStop(1);
		}else{
			mc.nextFrame();
		}
	}
	
	private function PrevFrame(mc:MovieClip){
		if(mc._currentframe==1){
			mc.gotoAndStop(mc._totalframes);
		}else{
			mc.prevFrame();
		}
	}	
	
	private function won(){
		_parent._parent._parent.getPlayerData().state.$machine[3].done.__text = "1";
		_parent.gotoFrame(2);
		_parent._parent._parent.playSound("machine4_bt2");
	}
	
}

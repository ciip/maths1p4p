﻿/**
 * class maths1p4p.level4.specialStages.mcScreen_Messages
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level4.specialStages.mcScreen_Messages extends maths1p4p.level4.Level4AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	/** le bouton retour */
	public var p_btBack:Button;
	
	/** ls boutons gauche droite */
	public var p_btLeft:Button;
	public var p_btRight:Button;
	
	/** anticlic */
	public var p_btBG:Button;

	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	private var p_iLastMessage:Number;

	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_Messages()	{		
		
		super();
		
		Application.useLibCursor(p_btBack, "cursor-finger");
		p_btBack.onRelease = function(){
			_parent._parent._parent.hideSpecialScreen();
		}		
		
		p_iLastMessage = _parent._parent.getPlayer().getLastMessage();
		trace("l::::::" + p_iLastMessage);
		
		if(p_iLastMessage>1){
		
			Application.useLibCursor(p_btLeft, "cursor-finger");
			p_btLeft.onRelease = function(){
				_parent.left();
			}	
			Application.useLibCursor(p_btRight, "cursor-finger");
			p_btRight.onRelease = function(){
				_parent.right();
			}		
			
			right();
			
		}else{
			p_btLeft._visible = p_btRight._visible = false;			
		}
	
		p_btBG.onPress = function(){};
		p_btBG.useHandCursor = false;		
		
		switch(_parent._parent.p_sCurrScene){			
			case "010":
				gotoAndStop(9);
				break;
			case "035":
				gotoAndStop(10);
				break;
			case "052":
				gotoAndStop(11);
				break;
			case "062":
				gotoAndStop(12);
				break;					
		}
		
		Application.useLibCursor(this["bt_close"], "cursor-finger");
		this["bt_close"].onRelease = function(){
			_parent._parent._parent.hideSpecialScreen();
		}
	
	}

	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	private function right(){
		if(this._currentframe-1 < p_iLastMessage){
			nextFrame();
		}
	}
	
	private function left(){	
		if(this._currentframe>2){
			prevFrame();
		}
	}
	
	
	
}

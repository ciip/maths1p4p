﻿/**
 * class maths1p4p.level3.specialStages.mcScreen_204
 * 
 * @author Grégory Lardon
 * @version 0.1
 * 
 * ETAT : 
 * fonctionne bien
 * 
 * Copyright (c) 2008 Grégory Lardon
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.application.Application;

class maths1p4p.level4.specialStages.mcScreen_Resultats extends maths1p4p.level4.Level4AbstractStage{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	
	/** anticlic */
	public var p_btBG:Button;
	
	/** le bouton retour */
	public var p_btBack:Button;
	
	/** le bouton impression */
	public var p_btPrint:Button;
	
	

	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	private var p_sPrintableResults:String;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function mcScreen_Resultats()	{		
		
		super();		
		
		p_sPrintableResults = _parent._parent.getPlayer().getName() + "\n\n";
	
		p_btBG.onPress = function(){};
		p_btBG.useHandCursor = false;
		
		Application.useLibCursor(p_btBack, "cursor-finger");
		p_btBack.onRelease = function(){
			_parent._parent._parent.hideSpecialScreen();
		}		
			
		// résultat des jeux
		var aGames:Array = _parent._parent.getGames();	
		for(var i=0;i<aGames.length;i++){
			p_sPrintableResults += aGames[i].name.__text + " : ";			
			this["mcGame"+i]._visible = (aGames[i].done.__text == 1);			
			if(aGames[i].done.__text == 1){
				p_sPrintableResults += "gagné \n";
			}else{
				p_sPrintableResults += "\n";
			}
		}
		
		p_sPrintableResults += "\nTrésor supplémentaire : ";
		
		// argent
		this["txtObjSuppl"].text = _parent._parent.getPlayerData().state.objsuppl.__text;
		p_sPrintableResults += this["txtObjSuppl"].text;
		
		// affichage du nom du joueur
		this["txtPlayer"].text = _parent._parent.getPlayer().getName();
		
		// impression
		Application.useLibCursor(p_btPrint, "cursor-finger");
		p_btPrint.onRelease = function(){
			_parent.printResults();
		}
				
	}
	

	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	private function printResults():Void{
		
		var mc:MovieClip = createEmptyMovieClip("mcPrint",500);
		mc._x = 2000;
		mc.createTextField("txtResults", 1, 0, 0, 400, 500);
		mc["txtResults"].text = p_sPrintableResults;
		
		var pjResults:PrintJob = new PrintJob();
		if(pjResults.start()){
			pjResults.addPage(mc);
			pjResults.send();
			delete pjResults;	
		}
		
		delete mc;
		
	}
	
	
	
}

﻿/**
 * class maths1p4p.utils.ClipLoader
 * 
 * @author Pierre Rossel
 * @version 0.1
 * 
 * Copyright (c) 2008 Pierre Rossel.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import mx.utils.Delegate;
import mx.managers.DepthManager;
import mx.managers.PopUpManager;
import mx.containers.Window;
import mx.controls.ProgressBar;
import mx.controls.Alert;
import mx.transitions.Tween;
import mx.transitions.easing.*;


class maths1p4p.utils.ClipLoader extends mx.containers.Window {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------

	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var p_asUrl:Array;
	private var p_oTarget:MovieClip;
	private var p_sLabel:String;
	private var p_oListener:Object;  // client listener
	
	private var p_mcl:MovieClipLoader;
	private var p_oMclSink:Object;
	private var p_amcBar;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Charge un fichier externe dans clip, puis saute à un repère dans le clip chargé
	 * 
	 * @param   sUrl  Chaine ou tableau de chaines. Si c'est un tableau, les urls sont préchargés dans
	 *                l'ordre dans un clip invisible, puis le dernier est chargé dans oTarget
	 * @param   oTarget 
	 * @param   sLabel  
	 * @return  
	 */
	public static function load(arrUrl:Object, oTarget:MovieClip, sLabel:String, oListener:Object){
		trace('ClipLoader.load(arrUrl: ' + arrUrl + ', oTarget: ' + oTarget + ', sLabel: ' + sLabel + ")")

		if (!(arrUrl instanceof Array)) {
			arrUrl = [arrUrl];
		}

		var oInit = {};
		oInit.title = "Chargement en cours...";
		//oInit._alpha = 0;

		// crée la barre de progression au top niveau
		var mcWnd = PopUpManager.createPopUp(_root, mx.containers.Window, true, oInit);

		mcWnd.p_asUrl = arrUrl;
		mcWnd.p_oTarget = oTarget;
		mcWnd.p_sLabel = sLabel;
		mcWnd.p_oListener = oListener;

		// puis change sa classe pour la nôtre
		mcWnd.__proto__ = ClipLoader.prototype;
		ClipLoader["apply"](mcWnd);
	}

	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
		
	/**
	 * Constructeur
	 */
	private function ClipLoader(){
		
		var content = createEmptyMovieClip("content", this.getNextHighestDepth());
		
		// enlève les url locales (inutile de précharger) sauf la dernière
		for (var iUrl = p_asUrl.length - 2; iUrl >= 0; iUrl-- ) {
			if (p_asUrl[iUrl].indexOf("file://") == 0)
				p_asUrl.splice(iUrl, 1);
		}
		
		
		// crée les barres de progression
		p_amcBar = new Array();
		for (var iUrl = 0; iUrl < p_asUrl.length; iUrl++ ) {
			var mcBar:ProgressBar = ProgressBar(content.createClassObject(ProgressBar, "mcBar" + iUrl, content.getNextHighestDepth()));
			mcBar.mode = "manual";
			mcBar.label = "";
			mcBar.move(0, 10 * iUrl);
			p_amcBar.push(mcBar);
		}

		//content.createTextField("txtDebug", content.getNextHighestDepth(), 0, 10 * p_asUrl.length, 200, 20);
		//content.txtDebug.text = "hello";
		
		// pas terrible, pas eu le temps de trouver comment faire mieux
		var iWndWidth = 220;
		var iWndHeight = 70 + 10 * iUrl;
		setSize(iWndWidth, iWndHeight);
		content._x = 35;
		content._y = 50;

		// centre sur la scène
		_x = (Stage.width - iWndWidth) / 2;
		_y = (Stage.height - iWndHeight) / 2;
		

		p_oMclSink = new Object();
		p_oMclSink.onLoadInit = Delegate.create(this, function(mcTarget:MovieClip) {
			//trace("onLoadInit(), bytes loaded: " + mcTarget.getBytesLoaded() + " / " + mcTarget.getBytesTotal());
			//Alert.show("onLoadInit: " + p_asUrl.length);
			
			clearInterval(p_oMclSink.p_iTimer);
			p_oMclSink.p_iTimer = undefined;
			
			p_amcBar[0].setProgress(100, 100);
			
			if (p_asUrl.length > 1) {
				p_asUrl.shift();
				p_amcBar.shift();
				loadNextUrl();
			}
			else {
				mcTarget.gotoAndPlay(p_sLabel);
				this.deletePopUp();
			}
			
		});
		p_oMclSink.onLoadError = function (target_mc:MovieClip, errorCode:String, httpStatus:Number) {
			trace("loadError");
			Alert.show("Erreur de chargement: " + errorCode + " httpStatus: " + httpStatus);
		}
		p_oMclSink.onLoadProgress = Delegate.create(this, function (target_mc:MovieClip, loadedBytes:Number, totalBytes:Number)  {
			p_amcBar[0].setProgress(loadedBytes, totalBytes);
			//content.txtDebug.text = loadedBytes + " / " + totalBytes;
			if (loadedBytes == totalBytes && p_oMclSink.p_iTimer == undefined) {
				//Alert.show("onLoadProgress " + loadedBytes + " / " + totalBytes);
				// Workaroung IE bug. onLoadInit n'est pas appelé si le fichier était déjà dans le cache
				// on se laisse 1 seconde pour le recevoir, sinon on l'appelle manuellement
				p_oMclSink.p_iTimer = setInterval(p_oMclSink, "onCompleteTimeout", 1000);
				//content.txtDebug.text += "IE bug: waiting 1s to fix"
			}
		});
		p_oMclSink.onCompleteTimeout = Delegate.create(this, function() {
			trace("IE bug workaround, 1s delay expired...");
			p_oMclSink.onLoadInit(p_oTarget);
		});

		p_mcl = new MovieClipLoader();
		p_mcl.addListener(p_oMclSink);

		// On commence un peu plus bas que le bas de la scène pour laisser le temps aux fichier locaux de se charger 
		// sans laisser apparaître la fenêtre de progression. Elle n'est utile que pour des longs temps d'attente.
		var oTween = new Tween(this, "_y", Regular.easeOut, Stage.height + 50, _y, 5, true);
		
		loadNextUrl();
	}


	private function loadNextUrl() {
		// last clip, time to register client listener
		if (p_asUrl.length == 1 && p_oListener != undefined) {
			p_mcl.addListener(p_oListener);
		}
		p_oTarget._visible = p_asUrl.length == 1;

		p_mcl.loadClip(p_asUrl[0], p_oTarget);
	}
	
}

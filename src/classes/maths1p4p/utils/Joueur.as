﻿/**
 * class maths1p4p.utils.Joueur
 * 
 * @author Rémi Gay
 * @version 1.0
 *
 * ETAT : a finir
 *
 * TO DO : 
 * - gerer l'interaction avec le root (memorisation des scores pour chaque mini-jeu notamment)
 *
 * Copyright (c) 2008 Rémi Gay.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

class maths1p4p.utils.Joueur {
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	public static var listeJoueurs:Array = new Array();;
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------

	/***** variables privées de la classe *****/
	private var id:Number;
	private var nom:String;
	private var score:Number;
	private var nbreCoupJoues:Number;
	
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * s'occupe de l'initialisation graphique et l'attribution des evenements
	 */
	public function Joueur(nom:String, id:Number)	{
		
		//ATTENTION A FAIRE EN AMONT
		//la classe ne verifie pas que le nom n'existe pas dejà ! On le testera avant
		Joueur.listeJoueurs.push(this);
		
		//id => soit on fait manuellement soit en fonction de la taille de la liste si pas de param
		if(id == null)
		{
			this.id = listeJoueurs.length;
		}
		else
		{
			this.id = id;
		}
		this.nom = nom;
		this.score = 0;
		this.nbreCoupJoues = 0;
	}
	
	public function getId():Number {
		
		return this.id;
	}
	
	public function getNom():String {
		
		return this.nom;
	}
	
	public function setNom(nom:String) {
		
		this.nom = nom;
	}
	
	public function getScore():Number {
		
		return this.score;
	}
	
	public function addScore() {
		
		this.score++;
	}
	
	public function setScore(score:Number) {
		
		this.score = score;
	}
	
	public function getNbreCoupJoues():Number {
		
		return this.nbreCoupJoues;
	}
	
	public function addNbreCoupJoues() {
		
		this.nbreCoupJoues ++;
	}
	
	public function setNbreCoupJoues(nbreCoupJoues:Number) {
		
		this.nbreCoupJoues = nbreCoupJoues;
	}
	
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------

	 

	 
	
}

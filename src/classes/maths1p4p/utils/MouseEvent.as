﻿/**
 * class maths1p4p.utils.MouseEvent
 * 
 * @author Rémi Gay
 * @version 1.0
 *
 * Copyright (c) 2008 Rémi Gay.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

import maths1p4p.utils.OutilsTableaux;

class maths1p4p.utils.MouseEvent
{

	public static var listeMouseUpEvent:Array = new Array();
	public static var listeMouseDownEvent:Array = new Array();

	/*
	* fonction ajoutant un écouteur sur un click souris (et lui associe l'execution d'une fonction
	* donnée en paramètre
	*/
	public static function ajouterMouseUp(nomEcouteur:String, fonctionAuClick:Function)
	{
		_root[nomEcouteur] = new Object();
		_root[nomEcouteur].onMouseUp = function ()
		{ 
			trace("declenchement d'un ecouteur mouseUp")
			fonctionAuClick.apply() ;
		}
		Mouse.addListener(_root[nomEcouteur]);
		
		MouseEvent.listeMouseUpEvent.push(_root[nomEcouteur]);
		trace("MouseEvent.listeMouseUpEvent[0] => "+MouseEvent.listeMouseUpEvent[0])
	}
	
	/*
	* fonction retirant l'écouteur sur un click souris
	*/
	public static function removeMouseUp(nomEcouteur:String)
	{
		Mouse.removeListener(_root[nomEcouteur]);
		OutilsTableaux.retirerElement(MouseEvent.listeMouseUpEvent, _root[nomEcouteur]);
		_root[nomEcouteur] = null;
	}

	/*
	* retire tous les écouteurs de mouseUp
	*/
	public static function removeAllMouseUp()
	{
		var ecouteur:Object;
		while(ecouteur = MouseEvent.listeMouseUpEvent.pop())
		{
			Mouse.removeListener(ecouteur);
			ecouteur = null;
		}
	}
	
	
	
	/*
	* fonction ajoutant un écouteur sur un relachement souris (et lui associe l'execution d'une fonction
	* donnée en paramètre
	*/
	public static function ajouterMouseDown(nomEcouteur:String, fonctionAuClick:Function)
	{
		_root[nomEcouteur] = new Object();
		_root[nomEcouteur].onMouseDown = function ()
		{ 
			trace("declenchement d'un ecouteur mouseDown")
			fonctionAuClick.apply() ;
		}
		Mouse.addListener(_root[nomEcouteur]);
		
		MouseEvent.listeMouseDownEvent.push(_root[nomEcouteur]);
		trace("MouseEvent.listeMouseDownEvent[0] => "+MouseEvent.listeMouseDownEvent[0])
	}
	
	
	/*
	* fonction retirant l'écouteur sur un relachement souris
	*/
	public static function removeMouseDown(nomEcouteur:String)
	{
		Mouse.removeListener(_root[nomEcouteur]);
		OutilsTableaux.retirerElement(MouseEvent.listeMouseDownEvent, _root[nomEcouteur]);
		_root[nomEcouteur] = null;
	}

	/*
	* retire tous les écouteurs de mouseDown
	*/
	public static function removeAllMouseDown()
	{
		var ecouteur:Object;
		while(ecouteur = MouseEvent.listeMouseDownEvent.pop())
		{
			Mouse.removeListener(ecouteur);
			ecouteur = null;
		}
	}


}


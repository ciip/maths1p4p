﻿/**
 * Boîte à outils qui contient des fonctions utiles lors de l'utilisation des clips.
 * 
 * Copyright (c) 2008 Rémi Gay.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
class maths1p4p.utils.OutilsClips {
	
	
	/**
	 * Effectue un test de collision avec chacun des clips cibles et, si le clip de départ est au moins en collision
	 * avec l'un d'eux alors on prend le clip avec lequel on a le plus de recouvrement
	 * condition : les 2 clips doivent se trouver dans le même conteneur
	 * @param clipDepart Le clip draggué
	 * @param listClipCible La liste des clips qui subiront les tests de recouvrement
	 * @return le clip retenu pour la collision ou null si aucun clip n'est en collision avec le clip de départ
	 */
	public static function collisionPlacee(clipDepart:MovieClip, listClipCible:Array):MovieClip{
		
		var clipChoisi = null;
		var clipProv1 = clipDepart.createTextField("prov1", 100, 0, 0, 1, 1);
		clipProv1.localToGlobal()
		
		
		
		
		var xClipDepart = clipDepart._x;
		var yClipDepart = clipDepart._y;
		var widthClipDepart = clipDepart._width;
		var heightClipDepart = clipDepart._height;
		//on parcourt tous les clips choisi et on retient la taille de recouvrement pour chacun afin de retourner
		//le meilleur
		for (var clef in listClipCible) {
			
			listClipCible[clef]._x
		}
			if (unTableau[clef] == elementRecherche) {
		
	}
	
	
}

﻿/**
 * Boîte à outils qui contient des fonctions utiles lors de l'utilisation des tableaux.
 * 
 * Copyright (c) 2008 Rémi Gay.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
class maths1p4p.utils.OutilsTableaux {
	// ------------------------------------------------------------------------------
	// I N T E R F A C E   P U B L I Q U E   ( u t i l i s a t e u r s )
	// ------------------------------------------------------------------------------
	/**
		 * Effectue une opération assez proche du toString() d'Array, mais ajoute l'affichage des index,
		 * ainsi que l'affichage des valeurs dont les index sont négatifs ou non-numériques.<br /><br />
		 * 
		 * Note : les valeurs ne sont pas affichées dans l'ordre des index, mais dans l'ordre inverse
		 * de celui d'insertion des éléments dans le tableau.
		 */
	public static function toString(unTableau:Array, avecSautsDeLigne:Boolean):String {
		// valeur par défaut du paramètre facultatif
		if (typeof (avecSautsDeLigne) != 'boolean') {
			// s'il vaut undefined notamment
			avecSautsDeLigne = false;
		}
		// go
		return OutilsTableaux._toString(unTableau, avecSautsDeLigne, 0);
	}
	private static function _toString(unTableau:Array, avecSautsDeLigne:Boolean, profondeur:Number):String {
		// limitations à l'appel récursif de la fonction
		if (profondeur == 1) {
			avecSautsDeLigne = false;
		} else if (profondeur>1) {
			return unTableau.toString();
		}
		// constantes
		var tab:String = '\t';
		var separateur:String = (avecSautsDeLigne ? ',' : ', ');
		// initialisation de la chaîne à retourner
		var str:String = '[Array ';
		// ajout du contenu du tableau dans la chaîne
		var estVide:Boolean = true;
		for (var clef in unTableau) {
			if (estVide) {
				estVide = false;
			}
			if (avecSautsDeLigne) {
				str += ''+newline+tab;
			}
			var element = unTableau[clef];
			str += clef+'=>';
			if (element instanceof Array) {
				str += OutilsTableaux._toString(element, avecSautsDeLigne, profondeur+1);
			} else {
				str += element;
			}
			str += separateur;
		}
		if (estVide) {
			str += '(vide)';
		} else {
			str = str.substr(0, str.length-separateur.length);
		}
		// fin de la chaîne
		str += ']';
		return (str);
	}
	/**
		 * Retourne une copie d'un tableau.
		 * 
		 * @param tableauEstClassique Facultatif (défaut : false). Si le tableau ne comporte que des
		 *    clefs numériques consécutives et commençant à zéro, il est avantageux de mettre ce
		 *    paramètre à true.
		 * 
		 */
	public static function getCopie(unTableau:Array, tableauEstClassique:Boolean):Array {
		if (typeof (tableauEstClassique) != 'boolean') {
			// s'il vaut undefined notamment
			tableauEstClassique = false;
		}
		if (tableauEstClassique) {
			return Array.apply(null, unTableau);
		} else {
			var copie:Array = new Array();
			for (var clef in unTableau) {
				copie[clef] = unTableau[clef];
			}
			return copie;
		}
	}
	/**
		 * Retourne la position d'un élément dans un tableau classique (c'est-à-dire la clef numérique
		 * qui permet, à l'aide de l'opérateur [], d'accéder à cette valeur dans le tableau, tableau
		 * qui ne doit avoir que des clefs numériques consécutives commençant à zéro).<br />
		 *  - Si l'élément est présent plusieurs fois, seul la première position trouvée est retournée.<br />
		 *  - Si l'élément n'est pas trouvé, la fonction retourne la valeur undefined.<br />
		 * L'opérateur de comparaison utilisé est celui d'égalité (==), non celui d'égalité stricte (===).
		 * <br /><br />
		 * 
		 * Note : a la même utilité que getPos dans fonctions.as
		 */
	public static function getPosition(unTableau:Array, elementRecherche):Number {
		var positionTrouvee:Number = undefined;
		var nbElements:Number = unTableau.length;
		for (var position = 0; position<nbElements; position++) {
			if (unTableau[position] == elementRecherche) {
				positionTrouvee = position;
				break;
			}
		}
		return positionTrouvee;
	}
	/**
		 * Dans un tableau classique (i.e. qui ne doit avoir que des clefs numériques consécutives
		 * commençant à zéro), retire la valeur associée à une position donnée (sa position est la clef
		 * numérique qui permet, à l'aide de l'opérateur [], d'accéder à cette valeur dans le tableau).
		 * Les positions supérieures sont décrémentées (ex. : si on retire la valeur qui se trouve à la
		 * position 2, alors la valeur qui était en position 3 se retrouve en position 2, celle en 4 se
		 * retrouve en 3, et ainsi de suite).<br /><br />
		 *
		 * Note : utilisée avec getPosition, elle a la même utilité que arrayRemove dans fonctions.as
		 * 
		 * @return Si la position donnée est négative ou supérieure au nombre d'éléments du tableau, la
		 *    fonction retourne false. Elle retourne true sinon.
		 */
	public static function retirerPosition(unTableau:Array, position:Number):Boolean {
		var retraitEffectue:Boolean = false;
		if ((position>=0) and (position<unTableau.length)) {
			unTableau.splice(position, 1);
			retraitEffectue = true;
		}
		return retraitEffectue;
	}
	/**
		 * Retourne l'index d'un élément dans un tableau (c'est-à-dire la clef numérique
		 * permettant d'y accéder à l'aide de l'opérateur []).<br />
		 *  - Si l'élément est présent plusieurs fois, seul le premier index trouvé est retourné.<br />
		 *  - Si l'élément n'est pas trouvé, la fonction retourne la valeur undefined.<br />
		 *  - Si la seule clef trouvée qui permet d'y accéder n'est pas numérique, le fonction
		 *    retourne la valeur NaN.<br />
		 * Fonctionne avec des index négatifs et/ou non-consécutifs.<br />
		 * L'opérateur de comparaison utilisé est celui d'égalité (==), non celui d'égalité stricte (===).
		 * <br /><br />
		 * 
		 * Si le tableau visé est un tableau classique (où les index commencent à zéro et sont
		 * consécutifs), utilisez préférablement getPosition.
		 * 
		 * Note : a la même utilité que getPos dans fonctions.as
		 */
	public static function getIndex(unTableau:Array, elementRecherche):Number {
		var indexTrouve:Number = undefined;
		for (var clef in unTableau) {
			if (unTableau[clef] == elementRecherche) {
				indexTrouve = Number(clef);
				if (not isNaN(indexTrouve)) {
					break;
				}
			}
		}
		return indexTrouve;
	}
	/**
	 * Retourne le nombre de copies de elementRecherche dans unTableau
	 */
	public static function getNbIndex(unTableau:Array, elementRecherche):Number {
		var nbIndex:Number = 0;
		for (var clef in unTableau) {
			if (unTableau[clef] == elementRecherche) {
				nbIndex++;
			}
		}
		return nbIndex;
	}
	/**
		 * Dans un tableau quelconque, retire la valeur associée à l'index donné (c'est-à-dire la clef
		 * numérique permettant d'y accéder à l'aide de l'opérateur []). Les index des autres valeurs
		 * restent inchangés.<br /><br />
		 * 
		 * Si le tableau visé est un tableau classique (où les index commencent à zéro et sont 
		 * consécutifs), utilisez préférablement retirerPosition.
		 * 
		 * @return Si l'index donné n'est pas trouvé, la fonction retourne false. Elle retourne true
		 *    sinon.
		 */
	public static function retirerIndex(unTableau:Array, index:Number):Boolean {
		if (index == undefined) {
			return false;
		}
		var strIndex:String = ''+index;
		// inutile en réalité
		return delete unTableau[strIndex];
	}
	/**
		 * Retourne la clef associée à un élément dans un tableau (c'est-à-dire la chaîne de caractère
		 * qui permet d'y accéder à l'aide de l'opérateur []).<br />
		 *  - Si l'élément est présent plusieurs fois, seule la première clef trouvée est retournée.<br />
		 *  - Si l'élément n'est pas trouvé, la fonction retourne la valeur undefined.<br />
		 * L'opérateur de comparaison utilisé est celui d'égalité (==), non celui d'égalité stricte (===).
		 */
	public static function getClef(unTableau:Array, elementRecherche):String {
		var clefTrouvee:String = undefined;
		for (var clef in unTableau) {
			if (unTableau[clef] == elementRecherche) {
				clefTrouvee = clef;
				break;
			}
		}
		return clefTrouvee;
	}
	/**
		 * Dans un tableau quelconque, retire la valeur associée à la clef donnée (c'est-à-dire la
		 * chaîne de caractère qui permet d'y accéder à l'aide de l'opérateur []). Les clefs des autres
		 * valeurs restent inchangées.
		 * 
		 * @return Si la clef donnée n'est pas trouvée, la fonction retourne false. Elle retourne true
		 *    sinon.
		 */
	public static function retirerClef(unTableau:Array, clef:String):Boolean {
		return delete unTableau[clef];
	}
	/**
		 * A partir d'un tableau associatif, retourne la liste des valeurs dans un tableau classique
		 * (indexé numériquement à partir de 0 avec des clefs consécutives). L'ordre d'insertion dans
		 * tableau passé en argument est conservé dans le tableau résultat.
		 */
	public static function getValeurs(unTableau:Array):Array {
		var tab_valeurs:Array = [];
		for (var clef in unTableau) {
			// l'ordre de parcours d'un for..in étant inversé, on ajoute au début et non à la fin
			tab_valeurs.unshift(unTableau[clef]);
		}
		return tab_valeurs;
	}
	/**
		 * supprime un élément d' un tableau simple uni ou multidimensionnel
		 * si l'élément est plusieurs fois présent, il est supprimé à chaque emplacement
		 *
		 */
	public static function retirerElement(pArray:Array, pElement):Boolean {
		var vok=false;
		for (var i = 0; i<pArray.length; i++) {
			if (pArray[i] == pElement) {
				pArray.splice(i, 1);
				vok=true;
			} else if (pArray[i].length>0) {
				 var vok2=retirerElement(pArray[i],pElement);
				if (vok2) vok=true;
			}
		}
		return vok;
	}
	/**
		 * supprime un élément d' un dataProvider à partir de son label
		 * si l'élément est plusieurs fois présent, il est supprimé à chaque emplacement
		 *
		 */
	public static function retirerElementDataProvider(pDataProvider, pNomElement):Boolean {
		var vok=false;
		for (var i = 0; i<pDataProvider.length; i++) {
			if (pDataProvider.getItemAt(i).label == pNomElement) {
				pDataProvider.removeItemAt(i);
				vok=true;
			} 
		}
		return vok;
	}

	
	/**
	 * Mélange les éléments d'un tableau
	 * @param pArray Le tableau
	 * @return Une nouvelle référence d'un tableau avec les éléments d'origine mélangés
	 */
	public static function shuffle(pArray:Array):Array{
		var vResult:Array = getCopie(pArray);
		var tmp:Object;
		var i:Number;
		var j:Number;
		var l:Number = pArray.length;
		
		for(i=0; i<l; i++){
			j = Math.round(Math.random()*(vResult.length - 1));
			tmp = vResult[j];
			vResult[j] = vResult[i];
			vResult[i] = tmp;
		}
		return vResult;
	}

	
	// ------------------------------------------------------------------------------
	// D O N N E E S   E T   F O N C T I O N S   P R I V E E S
	// ------------------------------------------------------------------------------
	/**
		 * Constructeur inaccessible, n'a pas à être utilisé. Toutes les fonctions utiles de la classe
		 * sont statiques.
		 */
	private function OutilsTableaux() {
		//
	}
	// ------------------------------------------------------------------------------
	// F O N C T I O N   D E   T E S T   ( e x e m p l e   d ' u t i l i s a t i o n )
	// ------------------------------------------------------------------------------
	/**
	 * Fonction de démonstration.
	 */
	public static function test() {
		// ····································
		// création d'un tableau illustrant tout ce qui est possible
		var tableau:Array = new Array();
		tableau = [0, 1, 2];
		tableau[3] = 3;
		tableau[undefined] = 4;
		// revient à utiliser 'undefined' comme clef
		tableau[null] = 5;
		// revient à utiliser 'null' comme clef
		tableau[false] = 6;
		// revient à utiliser 'false' comme clef
		tableau[true] = 7;
		// revient à utiliser 'true' comme clef
		tableau['8'] = 8;
		tableau[10] = 10;
		tableau[-1] = -1;
		/*
				// ····································
				// divers résultats de base bons à connaître
				
				// un simple trace d'un tableau ne montre pas tous les éléments, et a quelques défauts
				// (il est fait pour des tableaux indexés numériquement, avec des valeurs consécutives)
				trace(tableau);
					// - les index non-numériques ou négatifs ne sont pas visibles lors d'un trace(),
					// - de plus il écrit undefined pour tous les index intermédiaires qui n'ont pas de
					//   valeur associée (comme c'est le cas pour l'index 4, ici, par exemple)
				
				// mais avec un for..in, on voit toutes les clefs d'un tableau
				trace(''); // ligne vide
				for(var clef:String in tableau) // attention, un for..in parcourt en ordre inverse de l'insertion
				{
					trace("tableau['" + clef + "']=" + tableau[clef]);
				}
				
				// quelques remarques intéressantes
				trace(''); // ligne vide
				tableau[NaN] = 'zero'; // attention, utiliser NaN comme index correspond à 0, et non à 'NaN' !
				trace(tableau);
				tableau['0'] = '0'; // strictement identique au chiffre 0 comme index (écrase l'ancienne valeur)
				trace(tableau);
				*/
		// ····································
		// illustration de la classe avec tableau complexe
		trace('');
		// ligne vide
		trace(OutilsTableaux.toString(tableau));
		for (var clef in tableau) {
			// attention, un for..in parcourt en ordre inverse de l'insertion
			trace("tableau['"+clef+"']="+tableau[clef]);
		}
		trace('');
		// ligne vide
		function testerGetIndexEtGetClef(valeurCherchee):Void {
			trace('getIndex('+valeurCherchee+') = '+OutilsTableaux.getIndex(tableau, valeurCherchee));
			var clefTrouvee:String = OutilsTableaux.getClef(tableau, valeurCherchee);
			trace('getClef('+valeurCherchee+") = "+(clefTrouvee == undefined ? 'undefined' : ("'"+clefTrouvee+"'")));
		}
		testerGetIndexEtGetClef(44);
		testerGetIndexEtGetClef(4);
		testerGetIndexEtGetClef(1);
		trace('test avec la chaîne "1", et non le nombre 1...');
		testerGetIndexEtGetClef('1');
		// fonctionne car get[Index|Clef] testent l'égalité et non l'identité
		trace('fin du test avec une chaîne au lieu du nombre');
		testerGetIndexEtGetClef(-1);
		trace('');
		trace(OutilsTableaux.toString(tableau));
		trace('');
		function testerRetirerIndex(indexCherche:Number):Void {
			trace('retirerIndex('+indexCherche+') = '+OutilsTableaux.retirerIndex(tableau, indexCherche));
			trace(OutilsTableaux.toString(tableau));
		}
		testerRetirerIndex(3);
		testerRetirerIndex(8);
		testerRetirerIndex(44);
		trace('');
		function testerRetirerClef(clefCherchee:String):Void {
			trace('retirerClef('+clefCherchee+') = '+OutilsTableaux.retirerClef(tableau, clefCherchee));
			trace(OutilsTableaux.toString(tableau));
		}
		testerRetirerClef(undefined);
		testerRetirerClef('false');
		testerRetirerClef('pouet');
		trace('');
		var unObjet:Object = new Object();
		tableau[unObjet] = 'test object';
		trace(OutilsTableaux.toString(tableau));
		var unAutreObjet:Object = new Object();
		tableau[unAutreObjet] = 'test object 2';
		trace(OutilsTableaux.toString(tableau));
		var unObjetAvecToString:Object = new Object();
		unObjetAvecToString.toString = function() {
			return '[unObjetAvecToString]';
		};
		tableau[unObjetAvecToString] = 'test object avec toString';
		trace(OutilsTableaux.toString(tableau));
		tableau.push('poc');
		// se base sur la clef numérique non-négative la plus grande
		trace(OutilsTableaux.toString(tableau));
		// ····································
		// illustration de la classe avec tableau classique
		trace(newline);
		// deux sauts de ligne
		var tableauClassique:Array = new Array();
		tableauClassique = ['zéro', 'ein', 'ni', 'tres', 'four'];
		trace('');
		function testerGetPosition(valeurCherchee) {
			trace('getPosition('+valeurCherchee+') = '+OutilsTableaux.getPosition(tableauClassique, valeurCherchee));
		}
		trace(OutilsTableaux.toString(tableauClassique));
		testerGetPosition(1);
		testerGetPosition('ni');
		OutilsTableaux.retirerPosition(tableauClassique, 3);
		trace(OutilsTableaux.toString(tableauClassique));
	}
}

﻿/**
 * class maths1p4p.utils.SoundPlayer
 * 
 * @author Grégory Lardon / Fred FAUQUETTE
 * @version 0.1
	* playASound(id:String)
    * stopAsound(id:String)
    * setVolumeOfAsound(id:String)
    * setGlobalVolume()
    * checkIfSoundExists(id:String)
    * addASound(id:String,url:String) (pour ajouter un son dans le
      tableau. L'url servira pour le check lors du loading)
    * deleteASound(id:String) (pour supprimer un son dans le tableau et
      dans le swf)
	  
	  
	  p_arrayOfSounds (qui stockera tous les sons créés dans un tableau
	  notamment pour la méthode setGlobalVolume)

 * Copyright (c) 2008 Grégory Lardon / Fred FAUQUETTE.
 * 
 * This file is part of Maths 1P-4P.
 * 
 * Maths 1P-4P is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
 

class maths1p4p.utils.SoundPlayer{
	
	//-------------------------------------------------------------------------
	// Public Properties
	//-------------------------------------------------------------------------
	
	
	
	
	//-------------------------------------------------------------------------
	// Private properties
	//-------------------------------------------------------------------------
	
	private var p_arrayOfSounds:Array;
	private var p_mcSoundContainer:MovieClip;
	static private var p_volumeGlobal:Number = 40;
	static private var p_numberOfSounds:Number=0;
	
	//-------------------------------------------------------------------------
	// Public methods
	//-------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	public function SoundPlayer(){
		this.p_arrayOfSounds = new Array();
		this.p_mcSoundContainer = _level0.createEmptyMovieClip("p_mcSoundContainer",499);
	}
	//Pour jouer un son ; id = id du son présent dans le XML ; begin : l'endroit, en ms, où commence la lecture
	public function playASound(id:String,begin:Number):Boolean{
		var i = findASoundFromId(id);
		if(i>=0){
			setVolumeOfAsound(id,this.p_arrayOfSounds[i].level);
			this.p_arrayOfSounds[i].mySound.start(begin);
			return true;
		}else{
			return false;
		}				
	}
	//Pour stopper un son ; id = id du son présent dans le XML
    public function stopAsound(id:String):Boolean{
		var i = findASoundFromId(id);
		if(i>=0){
			this.p_arrayOfSounds[i].mySound.stop();
			return true;
		}else{
			return false;
		}		
	}
	//Pour régler le volume d'un son ; id = id du son présent dans le XML ; level : le volume
    public function setVolumeOfAsound(id:String,level:Number):Void{
		var i = findASoundFromId(id);
		level = Math.min(Math.max(0,level),100);
		if(i>=0){
			this.p_arrayOfSounds[i].level = level;
			this.p_arrayOfSounds[i].mySound.setVolume(level);
		}	
	}
	//Pour régler le volume de tous les sons ; level : le volume
    public function setGlobalVolume(level):Void{
		p_volumeGlobal = Math.min(Math.max(0,level),100);
		for(var i in this.p_arrayOfSounds){
			this.p_arrayOfSounds[i].level = level;
			this.p_arrayOfSounds[i].mySound.setVolume(level);
		}		
	}
	//Pour récupérer le volume de tous les sons
	 public function get getGlobalVolume():Number{
		return p_volumeGlobal;
	}
	//méthode publique pour connaitre l'existence d'un son
    public function checkIfSoundExists(id:String):Boolean{
		if(findASoundFromId(id)>=0){
			return true;
		}else{
			return false;
		}				
	}
	//méthode publique pour ajouter un son
    public function addASound(id:String,url:String):Sound{
		var mySound = undefined;
		if(!checkIfSoundExists(id)){
			p_numberOfSounds++;
			var mc = this.p_mcSoundContainer.createEmptyMovieClip("newSound"+p_numberOfSounds,p_numberOfSounds);
			mySound = new Sound(mc);
			this.p_arrayOfSounds.push({id:id,mySound:mySound,url:url,level:p_volumeGlobal});
			//à envoyer au loader ensuite
			mySound.loadSound(url,false);
		}
		return mySound;		
	}
    public function deleteASound(id:String):Boolean{
		//TODO
		return true;		
	}
	//méthode publique pour obtenir la valeur de la propriété privée p_mcSoundContainer
	public function get mcSoundContainer():MovieClip{
		return this.p_mcSoundContainer;
	}
	
	
	//-------------------------------------------------------------------------
	// Private methods
	//-------------------------------------------------------------------------
	//méthode privée qui retourne le numéro d'un son dans le tableau en fonction de son id
	private function findASoundFromId(id:String):Number{
		var te = false;
		for(var i in this.p_arrayOfSounds){
			if(id == this.p_arrayOfSounds[i].id && !te){
				te = true;
				return i;
			}
		}
		if(!te){
			return -1
		}
	}

	
}

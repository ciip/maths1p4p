<?php
/*
 * Fonction générales
 * 
 * @author Pierre Rossel
 * 
 */

	// Parse array of data supposed to have been received from a form and prepare data
	// for different usages
	// $data: 		associative array of values (values can be arrays as well) or single value
	// $sContext:	'html': strip any slashes and encode special values for use in html context
	//              'db':   add slashes if needed
	//				'text': strip any slashes
	// $default:    optional value to return if $data is not set
	function PrepareFormData($data, $sContext = 'html', $default)
	{
		if (!isset($data))
			return $default;
	
		$bMagicQuotes = get_magic_quotes_gpc();
		
		$bStripSlashes = $bMagicQuotes && in_array($sContext, array('html', 'text'));
		$bAddSlashes = !$bMagicQuotes && in_array($sContext, array('db'));
		$bHTMLEncode = in_array($sContext, array('html'));
		
		if (is_array($data))
		{
			foreach ($data as $key => $value)
			{
				$data[$key] = PrepareFormData($value, $sContext);
			}
		}
		else
		{
			if ($bStripSlashes)
				$data = stripslashes($data);

			if ($bAddSlashes)
				$data = addslashes($data);

			if ($bHTMLEncode)
				$data = htmlspecialchars($data);
		}
		return $data;
	}
	
	
	// Prepare form data for db query, ready to incorporate in INSERT or UPDATE query
	//  - add slashes if not already done by PHP
	//  - quote values with '
	//  - format a string with fields and values depending on $sOperation
	//  - NULL values are not quoted, an object's scalar member will be litteraly used (ex: 'logDate' => (object)'NOW()')
	// $sOperation: 'SET' or 'VALUES'
	// $asFields: (optional) If set, can be a coma or space separated string or an array 
	// 	of field names to use in $data. If not set, all key->values of $data will be used
	//  a field name not matching a key in $data will be ignored
	//
	// return: 
	//	if $sOperation = 'SET' will return a string like:
	//		SET field1='value1', field2='value2', ...
	//	if $sOperation = 'SET' will return a string like:
	//		(field1, field2, ...) VALUES ('value1', 'value2', ...)
	// 
	function PrepareFormDataForDb(&$data, $sOperation, $asFields = '')
	{
		// convert the fields restriction param
		if ($asFields == '')
			$asFields = array_keys($data);
		if (is_string($asFields))
			$asFields = preg_split("/\W+/", $asFields);
	
		$sValues = '';
		$sSet = '';
		$sUsedFields = '';
		
		foreach($asFields as $sField)
		{
			// skip field if not present in data to avoid setting it to NULL
			if (!array_key_exists($sField, $data))
				continue;
			
			$sValue = $data[$sField];
			
			if (is_null($sValue))
				$sValue = 'NULL';
			elseif (is_object($sValue))
				$sValue = $sValue->scalar;
			else
			{
				if (!get_magic_quotes_gpc())
					$sValue = addslashes($sValue);
				$sValue = "'$sValue'";
			}

			switch($sOperation)
			{
				case 'VALUES' : 
					
					if ($sValues != '')
						$sValues.= ', ';
					$sValues.= $sValue;

					if ($sUsedFields != '')
						$sUsedFields.= ', ';
					$sUsedFields.= $sField;
					break;
					
				case 'SET' : 
					
					if($sSet != '')
						$sSet.= ', ';
					$sSet.= "$sField=$sValue";
					break;
					
				default: 
					die("Invalid \$sOperation ($sOperation) in PrepareFormDataForDb");
			}
		}
		
		switch($sOperation)
		{
			case 'VALUES' :	return " ($sUsedFields) VALUES ($sValues) ";
			case 'SET' : 	return " SET $sSet ";
		}
		return '';
	}
?>
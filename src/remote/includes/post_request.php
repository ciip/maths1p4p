<?php
	function do_post_request($url, $data, $optional_headers = null) {
		$start = strpos($url,'//')+2;
		$end = strpos($url,'/',$start);
		$host = substr($url, $start, $end-$start);
		$domain = substr($url,$end);
		$fp = pfsockopen($host, 80);
		if(!$fp) return null;
		fputs ($fp,"POST $domain HTTP/1.1\n");
		fputs ($fp,"Host: $host\n");
		if ($optional_headers) {
			fputs($fp, $optional_headers);
		}
		fputs ($fp,"Content-type: application/x-www-form-urlencoded\n");
		fputs ($fp,"Content-length: ".strlen($data)."\n\n");
		fputs ($fp,"$data\n\n");
		$response = "";
		while(!feof($fp)) {
			$response .= fgets($fp, 1024);
		}
		fclose ($fp);
		return $response;
	} 
?>
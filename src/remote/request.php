<?php

/*
 * Implémente le stockage distant (sur un serveur PHP-MySQL) des resultats des jeux Maths 1P-4P
 * 
 * Extensions nécessaires (pas exhaustif):
 *   - DOM XML
 *   - 
 * 
 * @author Pierre Rossel
 * 
 * @param   action  Action à effectuer (ClassesList, PlayersList...)
 * @param   ...     autres paramètres selon l'action. Voir doc dans le code.
 * @return la réponse sous forme de document XML standard (code d'erreur et description) ou selon doc de l'action demandée 
 */

require 'includes/config.php';
require 'includes/general.inc.php';
if (version_compare(PHP_VERSION,'5','>='))
	require_once('includes/domxml-php4-to-php5.php');

mysql_connect(DB_SERVER, DB_USER, DB_PASS);
if (!mysql_select_db(DB_DATABASE))
	die("Error selecting database");

$sAction = isset ($_REQUEST['action']) ? $_REQUEST['action'] : '';

mysql_query("SET NAMES 'UTF8'");

// Test à supprimer quand on sera sûr de ne plus en avoir besoin
function my_encode($str) {
	//return utf8_encode($str);
	return $str;
}

/*
 * Termine le script en renvoyant une réponse XML standard:
 * 
 * <response>
 *   <result value="0" />
 *   <error>Message d'erreur optionnel</error>
 * </response>
 * 
 * @param iResultCode Code de retour. En général < 0 pour les erreurs. 0 ou plus grand si OK.
 * @param sError      (optionnel) Description de l'erreur 
 * 
 */
function XMLResponse($iResultCode, $sError = '') {
	$sRet = '<'.'?xml version="1.0" encoding="utf-8"?'.'>'."\n" .
	"<response><result value=\"$iResultCode\" />";
	if ($sError != '')
		//		$sRet .= '<error>' . htmlentities(utf8_encode($sError)) . '</error>'; // propri&Atilde;�&Acirc;&copy;taire
		//		$sRet .= '<error>' . utf8_encode(htmlentities($sError)) . '</error>'; // propri&Atilde;&copy;taire
		//		$sRet .= '<error>' . utf8_encode($sError) . '</error>';
		//		$sRet .= '<error>' . htmlentities($sError) . '</error>';
		$sRet .= '<error>' . htmlspecialchars($sError) . '</error>';
	//		$sRet .= '<error>' . $sError . '</error>';
	$sRet .= '</response>';
	die($sRet);
}

switch ($sAction) {

	case 'ClassesList' :

		/*
		 * Renvoie la liste des classes dans un document XML du genre:
		 * 
		 * <response>
		 *   <classes>
		 *     <class idClass="2" name="ma classe" level="2" idTeacher="2" description="bla bla bla" passHash="" idSchool="1" school="Rousseau" />
		 *     <class idClass="3" name="classe 2" level="2" idTeacher="2" description="" passHash="" idSchool="2" school=""/>
		 *   </classes>
		 * </response>
		 * 
		 * Paramètres supplémentaires:
		 * level   (1..4, optionnel) Renvoie uniquement les classe du niveau spécifié
		 * teacher (string, optionel) Renvoie uniquement les classes de l'enseignant spécifié
		 * 
		 */

		$iLevel = isset ($_REQUEST['level']) ? (int) ($_REQUEST['level']) : 0;
		$sTeacher = PrepareFormData(@ $_REQUEST['teacher'], 'db', '');

		// si un teacher est donné, récupère son id
		$idTeacher = 0;
		if ($sTeacher != '') {
			$sSQL = " SELECT idTeacher FROM Teacher WHERE login='$sTeacher'";
			$res = mysql_query($sSQL);
			if (!$res)
				die('Error querying database');
			if ($row = mysql_fetch_assoc($res)) 
				$idTeacher = $row['idTeacher'];
			else
				$idTeacher = -1;
		}
		$sSQL = ' SELECT Class.*, School.name as SchoolName FROM Class LEFT JOIN School ON school=idSchool';

		// where clause
		$arrWhere = array();
		if ($iLevel)
			$arrWhere[] = ' level = ' . $iLevel;
		if ($idTeacher)
			$arrWhere[] = ' teacher = ' . $idTeacher;
		if (count($arrWhere))
			$sSQL .= ' WHERE '. implode(' AND ', $arrWhere);

		$sSQL .= ' ORDER BY School.name, Class.name ';
		$res = mysql_query($sSQL);
		if (!$res)
			die('Error querying database');

		// create a new XML document
		$doc = domxml_new_doc('1.0');

		// create root node
		$response = $doc->create_element('response');
		$doc->append_child($response);
		$classes = $doc->create_element('classes');
		$response->append_child($classes);

		// process one row at a time
		while ($row = mysql_fetch_assoc($res)) {
			$class = $doc->create_element('class');
			$classes->append_child($class);
			$class->set_attribute('idClass', $row['idClass']);
			$class->set_attribute('level', $row['level']);
			$class->set_attribute('idTeacher', $row['teacher']);
			$class->set_attribute('name', my_encode($row['name']));
			$class->set_attribute('description', my_encode($row['description']));
			$class->set_attribute('passHash', ($row['pass'] == '') ? '' : MD5($row['pass']));
			$class->set_attribute('idSchool', my_encode($row['school']));
			$class->set_attribute('school', my_encode($row['SchoolName']));
		}

		// get completed xml document
		$sXml = $doc->dump_mem(true, 'utf-8');

		echo $sXml;

		break;

	case 'PlayersList' :

		/*
		 * Renvoie la liste des players dans un document XML du genre:
		 * 
		 * <response>
		 *   <players>
		 *     <player name="Roger" idPlayer="2" passwordSet="1" idClass="3" level="2" idTeacher="1"/>
		 *     <player name="Dominique" idPlayer="3" idClass="3" level="1" idTeacher="1" idSchool="1">
		 *       <results>...
		 *       </results>
		 *     </player>
		 *   </players>
		 * </response>
		 * 
		 * Paramètres supplémentaires:
		 * level       (1..4, optionnel) Renvoie uniquement les players dans les classe du niveau spécifié
		 * idClass     (int, optionnel) ID de la classe dans laquelle chercher les players
		 * idTeacher   (int, optionnel) ID de l'enseignant propriétaire des classes dans lesquelles chercher les players
		 * idSchool    (int, optionnel) ID de l'école dans lasquelle chercher les players
		 * withResults (bool, optionnel, 0 par défaut) Renvoie les résultats des joueurs
		 * 
		 */

		$iLevel = isset ($_REQUEST['level']) ? (int) ($_REQUEST['level']) : 0;
		$iClassId = isset ($_REQUEST['idClass']) ? (int) ($_REQUEST['idClass']) : 0;
		$iTeacherId = isset ($_REQUEST['idTeacher']) ? (int) ($_REQUEST['idTeacher']) : 0;
		$iSchoolId = isset ($_REQUEST['idSchool']) ? (int) ($_REQUEST['idSchool']) : 0;
		$bWithResults = isset ($_REQUEST['withResults']) ? (int) ($_REQUEST['withResults']) : 0;

		$arrWhere = array();
		if ($iLevel)
			$arrWhere[] = ' level = ' . $iLevel;
		if ($iClassId)
			$arrWhere[] = ' idClass = ' . $iClassId;
		if ($iTeacherId)
			$arrWhere[] = ' teacher = ' . $iTeacherId;
		if ($iSchoolId)
			$arrWhere[] = ' school = ' . $iSchoolId;
		if (count($arrWhere))
			$sWhere = ' WHERE '. implode(' AND ', $arrWhere);
		else
			$sWhere = '';

		$sFields = 'idPlayer, idClass, teacher, school, Player.name, level, Player.pass != "" AS passwordSet';
		if ($bWithResults) {
			$sFields.= ', results';
		}
		$sSQL = ' SELECT '.$sFields.' FROM Player JOIN Class ON class=idClass ';
		$sSQL .= $sWhere;
		$sSQL .= ' ORDER BY idClass, name ';
		$res = mysql_query($sSQL);
		if (!$res)
			die('Error querying database');

		// create a new XML document
		$doc = domxml_new_doc('1.0');

		// create root node
		$response = $doc->create_element('response');
		$doc->append_child($response);
		$players = $doc->create_element('players');
		$response->append_child($players);

		// process one row at a time
		while ($row = mysql_fetch_assoc($res)) {
			$player = $doc->create_element('player');
			$players->append_child($player);
			$player->set_attribute('idPlayer', $row['idPlayer']);
			$player->set_attribute('idClass', $row['idClass']);
			$player->set_attribute('name', my_encode($row['name']));
			$player->set_attribute('passwordSet', $row['passwordSet']);
			$player->set_attribute('level', $row['level']);
			$player->set_attribute('idTeacher', $row['teacher']);
			$player->set_attribute('idSchool', $row['school']);
			if ($bWithResults){
				// on doit parser la chaine XML pour récupérer un noeud XML valide
				$docCheck = domxml_open_mem($row['results']);
				if (!$docCheck)
					$docCheck = domxml_open_mem('<results/>');
				$results = $docCheck->first_child();
				$player->append_child($results->clone_node(true));
			}
		}

		// get completed xml document
		$sXml = $doc->dump_mem(true, 'utf-8');

		echo $sXml;

		break;

	case 'SchoolsList' :

		/*
		 * Renvoie la liste des écoles dans un document XML du genre:
		 * 
		 * <response>
		 *   <schools>
		 *     <school idSchool="2" name="mon école" />
		 *     <school idSchool="3" name="Ecole primaire de Trucmuche" />
		 *   </schools>
		 * </response>
		 * 
		 */

		$sSQL = ' SELECT * FROM School ';
		$sSQL .= ' ORDER BY name ';
		$res = mysql_query($sSQL);
		if (!$res)
			die('Error querying database');

		// create a new XML document
		$doc = domxml_new_doc('1.0');

		// create root node
		$response = $doc->create_element('response');
		$doc->append_child($response);
		$schools = $doc->create_element('schools');
		$response->append_child($schools);

		// process one row at a time
		while ($row = mysql_fetch_assoc($res)) {
			$school = $doc->create_element('school');
			$schools->append_child($school);
			$school->set_attribute('idSchool', $row['idSchool']);
			$school->set_attribute('name', my_encode($row['name']));
		}

		// get completed xml document
		$sXml = $doc->dump_mem(true, 'utf-8');

		echo $sXml;

		break;

	case 'GetPlayer' :

		/*
		 * Renvoie les données d'un Player dans un document XML du genre:
		 * 
		 * <response>
		 *   <player name="Roger" idPlayer="2" idClass="3" level="2" passwordSet="1">
		 *     <results>
		 *     </results>
		 *     <state>
		 *     </state>
		 *   </player>
		 * </response>
		 * 
		 * Paramètres supplémentaires:
		 * idPlayer (int) ID du player à charger
		 * 
		 */

		$idPlayer = isset ($_REQUEST['idPlayer']) ? (int) ($_REQUEST['idPlayer']) : 0;

		$sSQL = ' SELECT idPlayer, class, Player.name, level, results, state, Player.pass != "" AS passwordSet FROM Player join Class on class=idClass ';
		$sSQL .= ' WHERE idPlayer = ' . $idPlayer;
		$res = mysql_query($sSQL);
		if (!$res)
			//die('Error querying database');
			die('Error querying database '. mysql_error());
		
		// create a new XML document
		$doc = domxml_new_doc('1.0');

		// create root node
		$response = $doc->create_element('response');
		$doc->append_child($response);
		$player = $doc->create_element('player');
		$response->append_child($player);

		// process one row at a time
		if ($row = mysql_fetch_assoc($res)) {

			$player->set_attribute('idPlayer', $row['idPlayer']);
			$player->set_attribute('idClass', $row['class']);
			$player->set_attribute('name', my_encode($row['name']));
			$player->set_attribute('level', $row['level']);
			$player->set_attribute('passwordSet', $row['passwordSet']);

			// on doit parser la chaine XML pour récupérer un noeud XML valide
			$docCheck = domxml_open_mem($row['results']);
			if (!$docCheck)
				$docCheck = domxml_open_mem('<results/>');
			$results = $docCheck->first_child();
			$player->append_child($results->clone_node(true));

			// on doit parser la chaine XML pour récupérer un noeud XML valide
			$docCheck = domxml_open_mem($row['state']);
			if (!$docCheck)
				$docCheck = domxml_open_mem('<state/>');
			$state = $docCheck->first_child();
			$player->append_child($state->clone_node(true));
		}

		// get completed xml document
		$sXml = $doc->dump_mem(true, 'utf-8');

		echo $sXml;

		break;

	case 'LoginPlayer' :
		/*
		 * Vérifie le mot de passe d'un player
		 * 
		 * Réponse XML Standard (voir XMLResponse): 
		 * 0: ok
		 * -1: erreur 
		 * 
		 * Paramètres supplémentaires:
		 * idPlayer   (int)    ID du player à charger
		 * playerHash (string) md5 hash du mot de passe du player
		 * 
		 */

		//		$idPlayer = isset ($_REQUEST['idPlayer']) ? (int) ($_REQUEST['idPlayer']) : 0;
		//		$sState = isset ($_REQUEST['state']) ? $_REQUEST['state'] : 0;
		//		$sResults = isset ($_REQUEST['results']) ? $_REQUEST['results'] : 0;
		$idPlayer = (int) PrepareFormData(@ $_REQUEST['idPlayer'], 'db', 0);
		$sPlayerHash = PrepareFormData(@ $_REQUEST['playerHash'], 'db', 0);

		if (!$idPlayer)
			XMLResponse(-1, 'Paramètre idPlayer manquant');

		// Vérifie le mot de passe
		$bEmptyPass = $sPlayerHash == MD5('') ? 1 : 0;
		$sSQL = ' SELECT idPlayer FROM Player ' .
		" WHERE idPlayer=$idPlayer AND (pass='$sPlayerHash' OR pass='' AND $bEmptyPass)";
		$res = mysql_query($sSQL);
		if (!$res)
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		if (!mysql_num_rows($res))
			XMLResponse(-1, 'Login incorrect.');

		XMLResponse(0);

		break;

	case 'ChangePlayerPassword' :
		/*
		 * Change le mot de passe d'un player
		 * 
		 * Réponse XML Standard (voir XMLResponse): 
		 * 0: ok
		 * -1: erreur 
		 * 
		 * Paramètres supplémentaires:
		 * idPlayer      (int)    ID du player à charger
		 * playerHash    (string) md5 hash du mot de passe du player
		 * playerNewHash (string) md5 hash du nouveau mot de passe du player
		 * 
		 */

		//		$idPlayer = isset ($_REQUEST['idPlayer']) ? (int) ($_REQUEST['idPlayer']) : 0;
		//		$sState = isset ($_REQUEST['state']) ? $_REQUEST['state'] : 0;
		//		$sResults = isset ($_REQUEST['results']) ? $_REQUEST['results'] : 0;
		$idPlayer = (int) PrepareFormData(@ $_REQUEST['idPlayer'], 'db', 0);
		$sPlayerHash = PrepareFormData(@ $_REQUEST['playerHash'], 'db', '');
		$sPlayerNewHash = PrepareFormData(@ $_REQUEST['playerNewHash'], 'db', '');

		if (!$idPlayer)
			XMLResponse(-1, 'Paramètre idPlayer manquant');

		// Change le mot de passe
		$sSQL = ' UPDATE Player ' .
		" SET pass='$sPlayerNewHash' " .
		" WHERE idPlayer=$idPlayer AND (pass='$sPlayerHash' OR pass='')";
		$res = mysql_query($sSQL);
		if (!$res)
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		if (mysql_affected_rows() == 1)
			XMLResponse(0);

		XMLResponse(-1, 'Login incorrect.');
		break;

	case 'SavePlayer' :
		/*
		 * Sauve les données d'un player
		 * 
		 * Réponse XML Standard (voir XMLResponse): 
		 * 0: ok
		 * -1: erreur 
		 * 
		 * Paramètres supplémentaires:
		 * idPlayer (int) ID du player à charger
		 * name     (opt, string) nom du player
		 * state    (opt, string) donnée de l'état du player
		 * results  (opt, string) donnée des résultats
		 * 
		 */

		//		$idPlayer = isset ($_REQUEST['idPlayer']) ? (int) ($_REQUEST['idPlayer']) : 0;
		//		$sState = isset ($_REQUEST['state']) ? $_REQUEST['state'] : 0;
		//		$sResults = isset ($_REQUEST['results']) ? $_REQUEST['results'] : 0;
		$idPlayer = (int) PrepareFormData(@ $_REQUEST['idPlayer'], 'db', 0);
		$sName = PrepareFormData(@ $_REQUEST['name'], 'db', 0);
		$sState = PrepareFormData(@ $_REQUEST['state'], 'db', 0);
		$sResults = PrepareFormData(@ $_REQUEST['results'], 'db', 0);

		if (!$idPlayer)
			XMLResponse(-1, 'Paramètre idPlayer manquant');

		$arrSet = array ();

		if ($sName !== 0) {
			$arrSet[] = "name='$sName'";
		}

		if ($sState !== 0) {
			// vérifie que la chaîne XML fournie est valide
			$doc = domxml_open_mem(PrepareFormData(@ $_REQUEST['state'], 'text', ''));
			if (!$doc)
				XMLResponse(-1, 'Invalid XML for state: ' . PrepareFormData(@ $_REQUEST['state'], 'html', ''));
			//			else
			//				XMLResponse(1, 'Valid XML for state: ' . $sState);
			$arrSet[] = "state='$sState'";
		}

		if ($sResults !== 0) {
			// vérifie que la chaîne XML fournie est valide
			$doc = domxml_open_mem(PrepareFormData(@ $_REQUEST['results'], 'text', ''));
			if (!$doc)
				XMLResponse(-1, 'Invalid XML for results: ' . PrepareFormData(@ $_REQUEST['results'], 'html', ''));
			$arrSet[] = "results='$sResults'";
		}

		if (!count($arrSet))
			XMLResponse(-1, 'Pas de données à sauver');

		$sSQL = 'UPDATE Player SET ' . implode(', ', $arrSet) . " WHERE idPlayer=$idPlayer";
		$res = mysql_query($sSQL);
		if ($res)
			XMLResponse(0);
		else
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		break;

	case 'NewPlayer' :
		/*
		 * Crée un nouveau player
		 * 
		 * Réponse XML Standard (voir XMLResponse): 
		 * -1: erreur
		 * >0: id du nouveau Player 
		 * 
		 * Paramètres supplémentaires:
		 * name     (string) nom du nouveau joueur
		 * idClass  (int) ID de la classe dans laquelle créer le nouveau player
		 * teacherLogin (string) nom de login du Teacher
		 * teacherHash  (string) md5 hash du mot de passe du Teacher
		 * 
		 */

		$idClass = (int) PrepareFormData(@ $_REQUEST['idClass'], 'db', 0);
		$sName = PrepareFormData(@ $_REQUEST['name'], 'db', 0);
		$sTeacherLogin = PrepareFormData(@ $_REQUEST['teacherLogin'], 'db', 0);
		$sTeacherHash = PrepareFormData(@ $_REQUEST['teacherHash'], 'db', 0);

		// Vérifie la classe et l'autorisation d'accès
		$sSQL = ' SELECT idClass FROM Class INNER JOIN Teacher ON teacher=idTeacher ' .
		" WHERE idClass=$idClass AND login='$sTeacherLogin' AND Teacher.pass='$sTeacherHash'";
		$res = mysql_query($sSQL);
		if (!$res)
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		if (!mysql_num_rows($res))
			XMLResponse(-1, 'Login incorrect ou ne correspondant pas au propriétaire de la classe.');

		$sSQL = "INSERT INTO Player (class, name) VALUES ($idClass, '$sName')";
		$res = mysql_query($sSQL);
		if ($res)
			XMLResponse(mysql_insert_id());
		else
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		break;

	case 'RenamePlayer' :
		/*
		 * Renomme un player
		 * 
		 * Réponse XML Standard (voir XMLResponse): 
		 * 0: ok
		 * -1: erreur 
		 * 
		 * Paramètres supplémentaires:
		 * idPlayer (int) ID du player à charger
		 * name     (string) nom du player
		 * 
		 */

		$idPlayer = (int) PrepareFormData(@ $_REQUEST['idPlayer'], 'db', 0);
		$sName = PrepareFormData(@ $_REQUEST['name'], 'db', 0);

		if (!$idPlayer)
			XMLResponse(-1, 'Paramètre idPlayer manquant');
		if (!$sName)
			XMLResponse(-1, 'Paramètre name manquant');

		$arrSet = array ();

		if ($sName !== 0) {
			$arrSet[] = "name='$sName'";
		}

		if (!count($arrSet))
			XMLResponse(-1, 'Pas de données à sauver');

		$sSQL = 'UPDATE Player SET ' . implode(', ', $arrSet) . " WHERE idPlayer=$idPlayer";
		$res = mysql_query($sSQL);
		if ($res)
			XMLResponse(0);
		else
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		break;

	case 'DeletePlayer' :
		/*
		 * Supprime un player
		 * 
		 * Réponse XML Standard (voir XMLResponse): 
		 * -1: erreur
		 * 0: OK 
		 * 
		 * Paramètres supplémentaires:
		 * idPlayer (int) ID du player
		 * teacherLogin (string) nom de login du Teacher
		 * teacherHash  (string) md5 hash du mot de passe du Teacher
		 * 
		 */

		$idPlayer = (int) PrepareFormData(@ $_REQUEST['idPlayer'], 'db', 0);
		$sTeacherLogin = PrepareFormData(@ $_REQUEST['teacherLogin'], 'db', 0);
		$sTeacherHash = PrepareFormData(@ $_REQUEST['teacherHash'], 'db', 0);

		if (!$idPlayer)
			XMLResponse(-1, 'Paramètre idPlayer manquant');

		// Vérifie la classe et l'autorisation d'accès
		$sSQL = ' SELECT idPlayer FROM Player INNER JOIN Class INNER JOIN Teacher ON teacher=idTeacher AND class=idClass ' .
		" WHERE idPlayer=$idPlayer AND login='$sTeacherLogin' AND Teacher.pass='$sTeacherHash'";
		$res = mysql_query($sSQL);
		if (!$res)
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		if (!mysql_num_rows($res))
			XMLResponse(-1, 'Login incorrect ou ne correspondant pas au propriétaire de la classe.');

		$sSQL = "DELETE FROM Player WHERE idPlayer=$idPlayer";
		$res = mysql_query($sSQL);
		if ($res)
			XMLResponse(0);
		else
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		break;

	case 'UnlockPlayer' :
		/*
		 * Réinitialise le mot de passe d'un player
		 * 
		 * Réponse XML Standard (voir XMLResponse): 
		 * -1: erreur
		 * 0: OK 
		 * 
		 * Paramètres supplémentaires:
		 * idPlayer (int) ID du player
		 * teacherLogin (string) nom de login du Teacher
		 * teacherHash  (string) md5 hash du mot de passe du Teacher
		 * 
		 */

		$idPlayer = (int) PrepareFormData(@ $_REQUEST['idPlayer'], 'db', 0);
		$sTeacherLogin = PrepareFormData(@ $_REQUEST['teacherLogin'], 'db', 0);
		$sTeacherHash = PrepareFormData(@ $_REQUEST['teacherHash'], 'db', 0);

		if (!$idPlayer)
			XMLResponse(-1, 'Paramètre idPlayer manquant');

		// Vérifie la classe et l'autorisation d'accès
		$sSQL = ' SELECT idPlayer FROM Player INNER JOIN Class INNER JOIN Teacher ON teacher=idTeacher AND class=idClass ' .
		" WHERE idPlayer=$idPlayer AND login='$sTeacherLogin' AND Teacher.pass='$sTeacherHash'";
		$res = mysql_query($sSQL);
		if (!$res)
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		if (!mysql_num_rows($res))
			XMLResponse(-1, 'Login incorrect ou ne correspondant pas au propriétaire de la classe.');

		$sSQL = "UPDATE Player SET pass='' WHERE idPlayer=$idPlayer";
		$res = mysql_query($sSQL);
		if ($res)
			XMLResponse(0);
		else
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		break;

	case 'NewClass' :
		/*
		 * Crée une nouvelle classe
		 * 
		 * Réponse XML Standard (voir XMLResponse): 
		 * -1: erreur
		 * >0: id de la nouvelle classe 
		 * 
		 * Paramètres supplémentaires:
		 * name         (string) nom du nouveau joueur
		 * level        (int) 1..4 niveau de la classe
		 * teacherLogin (string) nom de login du Teacher
		 * teacherHash  (string) md5 hash du mot de passe du Teacher
		 * 
		 */

		$sName = PrepareFormData(@ $_REQUEST['name'], 'db', 0);
		$iLevel = isset ($_REQUEST['level']) ? (int) ($_REQUEST['level']) : 0;
		$sTeacherLogin = PrepareFormData(@ $_REQUEST['teacherLogin'], 'db', 0);
		$sTeacherHash = PrepareFormData(@ $_REQUEST['teacherHash'], 'db', 0);

		if ($iLevel < 1 || $iLevel > 4) XMLResponse(-1, 'level invalide.');


		// Vérifie la classe et l'autorisation d'accès
		$sSQL = ' SELECT idTeacher FROM Teacher ' .
		" WHERE login='$sTeacherLogin' AND Teacher.pass='$sTeacherHash'";
		$res = mysql_query($sSQL);
		if (!$res)
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		if (!mysql_num_rows($res))
			XMLResponse(-1, 'Login incorrect.');
			
		$row = mysql_fetch_assoc($res);
		$idTeacher = $row['idTeacher'];

		$sSQL = "INSERT INTO Class (teacher, level, name) VALUES ($idTeacher, $iLevel,  '$sName')";
		$res = mysql_query($sSQL);
		if ($res)
			XMLResponse(mysql_insert_id());
		else
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		break;

	case 'DeleteClass' :
		/*
		 * Supprime une classe
		 * 
		 * Réponse XML Standard (voir XMLResponse): 
		 * -1: erreur
		 * 0: OK 
		 * 
		 * Paramètres supplémentaires:
		 * idClass (int) ID de la classe
		 * teacherLogin (string) nom de login du Teacher
		 * teacherHash  (string) md5 hash du mot de passe du Teacher
		 * 
		 */

		$idClass = (int) PrepareFormData(@ $_REQUEST['idClass'], 'db', 0);
		$sTeacherLogin = PrepareFormData(@ $_REQUEST['teacherLogin'], 'db', 0);
		$sTeacherHash = PrepareFormData(@ $_REQUEST['teacherHash'], 'db', 0);

		// Vérifie la classe et l'autorisation d'accès
		$sSQL = ' SELECT idClass FROM Class INNER JOIN Teacher ON teacher=idTeacher ' .
		" WHERE idClass=$idClass AND login='$sTeacherLogin' AND Teacher.pass='$sTeacherHash'";
		$res = mysql_query($sSQL);
		if (!$res)
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		if (!mysql_num_rows($res))
			XMLResponse(-1, 'Login incorrect ou ne correspondant pas au propriétaire de la classe.');

		// Supprime les joueurs
		$sSQL = "DELETE FROM Player WHERE class=$idClass";
		$res = mysql_query($sSQL);
		if (!$res)
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		// supprime la classe
		$sSQL = "DELETE FROM Class WHERE idClass=$idClass";
		$res = mysql_query($sSQL);
		if (!$res)
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		XMLResponse(0);

		break;

	case 'GetClass' :

		/*
		 * Renvoie les données d'une classe dans un document XML du genre:
		 * 
		 * <response>
		 *   <classe idClass="2" name="1ère primaire" pass="" description="" level="1" disabled="0" idSchool="13" idTeacher="22"/>
		 *   <schools>
		 *     <school idSchool="1" name="Ecole primaire de Cheyres">
		 *     <school idSchool="2" name="Ecole primaire d'Yverdon'">
		 *   </schools>
		 * </response>
		 * 
		 * Paramètres supplémentaires:
		 * idClass (int) ID du la classe à charger
		 * optionWithSchools (opt, bool, 0) Renvoie la liste des écoles
		 * 
		 */

		$idClass = (int) PrepareFormData(@ $_REQUEST['idClass'], 'db', 0);
		$bWithSchools = isset ($_REQUEST['optionWithSchools']) ? (int) ($_REQUEST['optionWithSchools']) : 0;

		$sSQL = ' SELECT idClass, school, teacher, level, name, description, pass, disabled FROM Class ';
		$sSQL .= ' WHERE idClass = ' . $idClass;
		$res = mysql_query($sSQL);
		if (!$res)
			die('Error querying database: ' . mysql_error());

		// create a new XML document
		$doc = domxml_new_doc('1.0');

		// create root node
		$response = $doc->create_element('response');
		$doc->append_child($response);
		$class = $doc->create_element('classe');
		$response->append_child($class);

		// process one row at a time
		if ($row = mysql_fetch_assoc($res)) {

			$class->set_attribute('idClass', $row['idClass']);
			$class->set_attribute('school', $row['school']);
			$class->set_attribute('teacher', $row['teacher']);
			$class->set_attribute('level', $row['level']);
			$class->set_attribute('name', my_encode($row['name']));
			$class->set_attribute('description', my_encode($row['description']));
			$class->set_attribute('pass', my_encode($row['pass']));
			$class->set_attribute('disabled', $row['disabled']);
		}

		if ($bWithSchools) {
			$sSQL = ' SELECT idSchool, name FROM School ';
			$res = mysql_query($sSQL);
			if (!$res)
				die('Error querying database: ' . mysql_error());

			$schools = $doc->create_element('schools');
			$response->append_child($schools);

			// process one row at a time
			while ($row = mysql_fetch_assoc($res)) {
	
				$school = $doc->create_element('school');
				$schools->append_child($school);
				
				$school->set_attribute('idSchool', $row['idSchool']);
				$school->set_attribute('name', my_encode($row['name']));
			}
		}

		// get completed xml document
		$sXml = $doc->dump_mem(true, 'utf-8');
		echo $sXml;

		break;


	case 'SaveClass' :
		/*
		 * Sauve les données d'une classe
		 * 
		 * Réponse XML Standard (voir XMLResponse): 
		 * 0: ok
		 * -1: erreur 
		 * 
		 * Paramètres supplémentaires:
		 * idClass  (int) ID de la classe à sauver
		 * name     (opt, string) nom de la classe
		 * pass     (opt, string) mot de passe de la classe (en clair)
		 * school   (opt, int) id de school ou 0 pour ne pas en utiliser
		 * createSchool (opt, string) si défini, crée une nouvelle école et ignore school
		 * 
		 */

		$idClass = (int) PrepareFormData(@ $_REQUEST['idClass'], 'db', 0);
		$sName = PrepareFormData(@ $_REQUEST['name'], 'db', 0);
		$sPass = PrepareFormData(@ $_REQUEST['pass'], 'db', 0);
		$iSchool = (int) PrepareFormData(@ $_REQUEST['school'], 'db', 0);
		$sCreateSchool = PrepareFormData(@ $_REQUEST['createSchool'], 'db', 0);

		if (!$idClass)
			XMLResponse(-1, 'Paramètre idClass manquant');

		$arrSet = array ();

		if ($sName !== 0) {
			$arrSet[] = "name='$sName'";
		}

		if ($sPass !== 0) {
			$arrSet[] = "pass='$sPass'";
		}
		
		if ($sCreateSchool !== 0) {
			$sSQL = 'INSERT INTO School (name) VALUES ("' . $sCreateSchool . '")';
			$res = mysql_query($sSQL);
			if ($res)
				$iSchool = mysql_insert_id(); 
			else
				XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);
		}

		$arrSet[] = "school='$iSchool'";

		if (!count($arrSet))
			XMLResponse(-1, 'Pas de données à sauver');

		$sSQL = 'UPDATE Class SET ' . implode(', ', $arrSet) . " WHERE idClass=$idClass";
		$res = mysql_query($sSQL);
		if ($res)
			XMLResponse(0);
		else
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		break;


	case 'GetTeacher' :

		/*
		 * Renvoie les données d'un Teacher dans un document XML du genre:
		 * 
		 * <response>
		 *   <teacher idTeacher="2" login="rmont" firstName="Roger" lastName="Montandon" email="roger@email.com"/>
		 * </response>
		 * 
		 * Paramètres supplémentaires:
		 * idTeacher (int) ID du Teacher à charger
		 * 
		 */

		$idTeacher = (int) PrepareFormData(@ $_REQUEST['idTeacher'], 'db', 0);

		$sSQL = ' SELECT idTeacher, login, firstName, lastName, email FROM Teacher ';
		$sSQL .= ' WHERE idTeacher = ' . $idTeacher;
		$res = mysql_query($sSQL);
		if (!$res)
			die('Error querying database: ' . mysql_error());

		// create a new XML document
		$doc = domxml_new_doc('1.0');

		// create root node
		$response = $doc->create_element('response');
		$doc->append_child($response);
		$teacher = $doc->create_element('teacher');
		$response->append_child($teacher);

		// process one row at a time
		if ($row = mysql_fetch_assoc($res)) {

			$teacher->set_attribute('idTeacher', $row['idTeacher']);
			$teacher->set_attribute('login', my_encode($row['login']));
			$teacher->set_attribute('firstName', my_encode($row['firstName']));
			$teacher->set_attribute('lastName', my_encode($row['lastName']));
			$teacher->set_attribute('email', my_encode($row['email']));
		}

		// get completed xml document
		$sXml = $doc->dump_mem(true, 'utf-8');
		echo $sXml;

		break;

	case 'LoginTeacher' :
		/*
		 * Vérifie le mot de passe d'un enseignant
		 * 
		 * Réponse XML Standard (voir XMLResponse): 
		 * > 0: ID Teacher
		 * -1: erreur 
		 * 
		 * Paramètres supplémentaires:
		 * name   (string)    nom de l'enseignant
		 * hash   (string)    md5 hash du mot de passe
		 * 
		 */

		$sName = PrepareFormData(@ $_REQUEST['name'], 'db', 0);
		$sPassHash = PrepareFormData(@ $_REQUEST['passHash'], 'db', 0);

		if ($sName == '') XMLResponse(-1, 'Paramètre name manquant');
		if ($sPassHash == '') XMLResponse(-1, 'Paramètre passHash manquant');

		// Special login for educanet2.ch names
		// Post login and pass to login page on educanet2 and analize result to determine success 
		if (preg_match('/educanet2\.ch$/', $sName)){
			require_once('includes/post_request.php');
			$sResp = do_post_request("http://www.educanet2.ch/ww3ee/100001.php", "login_login=$sName&login_password=$sPassHash");
			// assuming correct login if login name is in the page title (verified to be correct on 2008-09-03)
			if (strpos($sResp, "<title>$sName</title>")){
				// login successful
							
				// hash and update password
				$sPassHash = MD5($sPassHash); 
				$sSQL = " UPDATE Teacher SET pass='$sPassHash'" .
				" WHERE login='$sName' ";
				$res = mysql_query($sSQL);
				if (!$res)
					XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

				// get teacher id
				$sSQL = ' SELECT idTeacher FROM Teacher ' .
				" WHERE login='$sName'";
				$res = mysql_query($sSQL);
				if (!$res)
					XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);
				if ($row = mysql_fetch_assoc($res))
					XMLResponse($row['idTeacher']);

				// Teacher not yet in database, create it
				$sSQL = " INSERT INTO Teacher (login, pass) VALUES ('$sName', '$sPassHash')";
				$res = mysql_query($sSQL);
				if (!$res)
					XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);
				else
					XMLResponse(mysql_insert_id());
			}
			else{
				// Login failed
				XMLResponse(-1, 'Login incorrect.');
			}
		}

		// Vérifie le mot de passe
		$sSQL = ' SELECT idTeacher FROM Teacher ' .
		" WHERE login='$sName' AND pass='$sPassHash'";
		$res = mysql_query($sSQL);
		if (!$res)
			XMLResponse(-1, mysql_error() . "\n\n" . $sSQL);

		if ($row = mysql_fetch_assoc($res))
			XMLResponse($row['idTeacher']);

		//if (!mysql_num_rows($res))
			XMLResponse(-1, 'Login incorrect.');

		break;

	case 'PhpInfo' :
		phpinfo();
		break;

	case 'test' :
		XMLResponse(-1, 'Test avec accent égu et <balises> qui feront peut-être tout foirer l\'truc');
		break;

	default :
		XMLResponse(-1, 'Action non définie');
}
?>

-- phpMyAdmin SQL Dump
-- version 2.11.2.1
-- http://www.phpmyadmin.net
--
-- Serveur: mysql.prossel.info
-- Généré le : Lun 03 Décembre 2007 à 13:28
-- Version du serveur: 5.0.24
-- Version de PHP: 4.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de données: `maths_prosselinfo_dev`
--

-- --------------------------------------------------------

--
-- Structure de la table `Class`
--

CREATE TABLE IF NOT EXISTS `Class` (
  `idClass` int(11) NOT NULL auto_increment,
  `school` int(11) NOT NULL default '0',
  `teacher` int(11) NOT NULL default '0',
  `level` int(1) NOT NULL default '0',
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `pass` varchar(64) NOT NULL,
  `disabled` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`idClass`),
  KEY `Class_FKIndex1` (`teacher`),
  KEY `Class_FKIndex2` (`school`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Player`
--

CREATE TABLE IF NOT EXISTS `Player` (
  `idPlayer` int(11) NOT NULL auto_increment,
  `class` int(11) NOT NULL default '0',
  `name` varchar(64) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `results` text NOT NULL,
  `state` text NOT NULL,
  PRIMARY KEY  (`idPlayer`),
  KEY `Player_FKIndex1` (`class`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `School`
--

CREATE TABLE IF NOT EXISTS `School` (
  `idSchool` int(11) NOT NULL auto_increment,
  `name` varchar(64) NOT NULL default '',
  PRIMARY KEY  (`idSchool`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Teacher`
--

CREATE TABLE IF NOT EXISTS `Teacher` (
  `idTeacher` int(11) NOT NULL auto_increment,
  `login` varchar(32) character set latin1 NOT NULL,
  `pass` varchar(32) character set latin1 NOT NULL,
  `firstName` varchar(64) character set latin1 NOT NULL,
  `lastName` varchar(64) character set latin1 NOT NULL,
  `email` varchar(64) character set latin1 NOT NULL,
  PRIMARY KEY  (`idTeacher`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
